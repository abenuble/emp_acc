<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{app_config('AppName')}} - {{$job->position_name->designation}}</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    {!! Html::style("assets/libs/bootstrap/css/bootstrap.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-toggle/css/bootstrap-toggle.min.css") !!}
    {!! Html::style("assets/libs/font-awesome/css/font-awesome.min.css") !!}
    {!! Html::style("assets/libs/alertify/css/alertify.css") !!}
    {!! Html::style("assets/libs/alertify/css/alertify-bootstrap-3.css") !!}
    {!! Html::style("assets/libs/bootstrap-select/css/bootstrap-select.min.css") !!}
    {!! Html::style("assets/css/style.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}

</head>
<body class="has-top-bar">

<main id="wrapper" class="wrapper">

    <div class="top-bar">

        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><i class="fa fa-bars"></i></button>
                <a class="navbar-brand" href="#">
                    <img src="<?php echo asset(app_config('AppLogo')); ?>" alt="logo" class="bar-logo">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{url('/')}}">{{language_data('Home')}}</a></li>
                    <li class="active"><a href="{{url('apply-job')}}">{{language_data('Jobs')}}</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div>

    </div>

    <section class="wrapper-bottom-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <h2 class="p-t-30 m-b-30 page-title">{{$job->position_name->designation}} ({{$job->job_location}})</h2>

                    @include('notification.notify')

                    <!-- Job Card Start -->
                    <div class="panel panel-30">
                        <div class="panel-body p-t-30">
                            <div class="row">

                                <div class="col-md-12">
                                    <table width="100%" cellpadding="10">
                                        <thead>
                                            <tr>
                                                <th colspan="2"><h4 class="m-b-20">{{language_data('Job Summary')}}</h4></th>
                                                <th><button class="btn btn-success pull-right" data-toggle="modal" data-target="#apply-now">{{language_data('Apply Now')}}</button></th>
                                                <th rowspan="7">{!!$job->description!!}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <span class="info-list-title">{{language_data('Quota')}}</span>
                                                </td>
                                                <td>:</td>
                                                <td>
                                                    <span class="info-list-des">{{$job->quota}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="info-list-title">{{language_data('Job Type')}}</span>
                                                </td>
                                                <td>:</td>
                                                <td>
                                                    <span class="info-list-des">{{$job->job_type}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="info-list-title">{{language_data('Experience')}}</span>
                                                </td>
                                                <td>:</td>
                                                <td>
                                                    <span class="info-list-des">{{$job->experience}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="info-list-title">{{language_data('Job Location')}}</span>
                                                </td>
                                                <td>:</td>
                                                <td>
                                                    <span class="info-list-des">{{$job->job_location}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="info-list-title">{{language_data('Salary Range')}}</span>
                                                </td>
                                                <td>:</td>
                                                <td>
                                                    <span class="info-list-des">{{$salary_range}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="info-list-title">{{language_data('Application Deadline')}}</span>
                                                </td>
                                                <td>:</td>
                                                <td>
                                                    <span class="info-list-des">{{get_date_format($job->close_date)}}</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Job Card End -->


                    <!-- Modal -->
                    <div class="modal fade" id="apply-now" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">{{language_data('Apply For')}} {{$job->position_name->designation}}</h4>
                                </div>
                                <form class="form-some-up" role="form" method="post" id="form" action="{{url('apply-job/post-applicant-resume')}}" enctype="multipart/form-data">

                                    <div class="modal-body">

                                        <div class="form-group a">
                                            <label>{{language_data('Name')}}</label>
                                            <input type="text" class="form-control name" name="name" value="">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Email')}}</label>
                                            <span class="help">e.g. "coderpixel@gmail.com" ({{language_data('Unique For every User')}})</span>
                                            <input type="text" class="form-control" name="email" value="">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>No. KTP</label>
                                            <input type="text" class="form-control"  name="ktp" id="ktp" onchange="checkktp()" value="">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Permanent Address')}}</label>
                                            <textarea placeholder="Alamat" class="form-control" rows="5" name="address" value=""></textarea>
                                        </div>
                                        <div class="form-group a">
                                            <div class="input-group">
                                                <span class="input-group-addon"> RT </span>
                                                <input type="text" class="form-control" name="rt" required value="">
                                                <span class="input-group-addon"> RW </span>
                                                <input type="text" class="form-control" name="rw" required value=""> 
                                            </div>
                                        </div>


                                        <div class="form-group a">
                                            <label>{{language_data('Permanent Village')}}</label>
                                            <input type="text" class="form-control"  name="village" value="">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Permanent District')}}</label>
                                            <input type="text" class="form-control"  name="district" value="">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Permanent City')}}</label>
                                            <input type="text" class="form-control"  name="city" value="">
                                        </div>

                                        <div class="form-group a">
                                            <label>{{language_data('Present Address')}}</label>
                                            <textarea placeholder="Alamat" class="form-control" rows="5" name="address_domisili" value=""></textarea>
                                        </div>

                                        <div class="form-group a">
                                            <label>{{language_data('Present Village')}}</label>
                                            <input type="text" class="form-control"  name="village_domisili" value="">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Present District')}}</label>
                                            <input type="text" class="form-control"  name="district_domisili" value="">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Present City')}}</label>
                                            <input type="text" class="form-control"  name="city_domisili" value="">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Phone')}}</label>
                                            <span class="help">e.g. "xxx-xxx-xxxx, xxxx-xxx-xxxx, xxxx-xxxx-xxxx, xxxx-xxxx-xxx"</span>
                                            <input type="text" class="form-control" name="phone" value="">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Place of Birth')}}</label>
                                            <input type="text" class="form-control"  name="birth_place" value="">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Date Of Birth')}}</label>
                                            <input type="text" class="form-control" required=""  name="birth_date" id="birth_date">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Gender')}}</label>
                                            <select name="gender" class="form-control selectpicker">
                                                <option value="male">{{language_data('male')}}</option>
                                                <option value="female">{{language_data('female')}}</option>
                                            </select>
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Marital Status')}}</label>
                                            <select name="marital_status" class="form-control selectpicker">
                                                <option value="Lajang">{{language_data('Single')}}</option>
                                                <option value="Menikah (K1)">{{language_data('Married (K1)')}}</option>
                                                <option value="Menikah (K2)">{{language_data('Married (K2)')}}</option>
                                                <option value="Menikah (K3)">{{language_data('Married (K3)')}}</option>
                                                <option value="Menikah (K4)">{{language_data('Married (K4)')}}</option>
                                                <option value="Duda">{{language_data('Widower')}}</option>
                                                <option value="Janda">{{language_data('Widow')}}</option>
                                            </select>
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Religion')}}</label>
                                            <select name="religion" class="form-control selectpicker">
                                                <option value="Islam">{{language_data('Islam')}}</option>
                                                <option value="Kristen">{{language_data('Christian')}}</option>
                                                <option value="Hindu">{{language_data('Hindu')}}</option>
                                                <option value="Budha">{{language_data('Buddha')}}</option>
                                            </select>
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data("Biological Mother's Name")}}</label>
                                            <input type="text" class="form-control name" required=""  name="ibu_kandung">
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Job title proposed')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="designation">
                                                <option value="{{$job->position_name->id}}">{{$job->position_name->designation}}</option>
                                            </select>
                                        </div>
        
                                        <div class="col-lg-6 a">
                                            <div class="form-group">
                                                <label>{{language_data('Experience')}}</label>
                                                <input type="text" class="form-control num" name="experience">
                                            </div>
                                        </div>
        
                                        <div class="col-lg-6 a">
                                            <div class="form-group">
                                                <label>{{language_data('Last Education')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true" name="last_education">
                                                    @foreach($last_education as $l)
                                                        <option value="{{$l->id}}">{{$l->education}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
        
                                        
                                        <div class="form-group a">
                                            <label>{{language_data('Curricullum Vitae')}}</label>
                                            <div class="input-group">
                                                <input id="resume" name="resume" class="form-control" type="file" accept="application/pdf">
                                            </div>
                                        </div>
        
                                        <div class="form-group a">
                                            <label>{{language_data('Photo')}}</label>
                                            <div class="input-group">
                                                <input id="photo" name="photo" class="form-control" type="file"  accept="image/*">
                                            </div>
                                        </div>


                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input type="hidden" value="{{$job->id}}" name="cmd" id="cmd">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{language_data('Apply')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>
</main>

{!! Html::script("assets/libs/jquery-1.10.2.min.js") !!}
{!! Html::script("assets/libs/jquery.slimscroll.min.js") !!}
{!! Html::script("assets/libs/smoothscroll.min.js") !!}
{!! Html::script("assets/libs/bootstrap/js/bootstrap.min.js") !!}
{!! Html::script("assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js") !!}
{!! Html::script("assets/libs/alertify/js/alertify.js") !!}
{!! Html::script("assets/libs/bootstrap-select/js/bootstrap-select.min.js") !!}
{!! Html::script("assets/js/scripts.js") !!}
{!! Html::script("assets/libs/moment/moment.min.js")!!}
{!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
{!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
{!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
{!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
{!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
{!! Html::script("assets/js/form-elements-page.js")!!}
{!! Html::script("assets/libs/data-table/datatables.min.js")!!}
{!! Html::script("assets/js/bootbox.min.js")!!}
{!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

<script>
    $(document).ready(function () {
        $('.num').number( true, 0, '.', ',' );

        $('#birth_date').datetimepicker({
            useCurrent: false,
            format: 'DD-MM-YYYY',
        });
        $('#resume').on('change', function(evt) {
            console.log(this.files[0].type);
            console.log(this.files[0].size);
            if(this.files[0].type!='application/pdf'){
                bootbox.alert("File must be a PDF file");
                var data = "";
                $('#resume').val(data);
            }
            if(this.files[0].size>500000){
                alert("File must not more than 500 kb");
                var data = "";
                $('#resume').val(data);
            }
        });
        $('#photo').on('change', function(evt) {
            console.log(this.files[0].size);
            if(this.files[0].size>500000){
                alert("File must not more than 500 kb");
                var data = "";
                $('#photo').val(data);
            }
        });

    });
    function checkktp(){
        var id = $('#ktp').val();
        var job = $('#cmd').val();
        $.ajax
        ({
            type: "GET",
            url: '/sss/apply-job/checkktp',
            data: {ktp : id, job : job},
            cache: false,
            success: function ( data ) {
                if(data=='blocked'){
                    alert("KTP Telah Terblokir");
                    $('#ktp').val('');
                } else if(data=='blocked'){
                    alert("KTP Telah Terdaftar");
                    $('#ktp').val('');
                }
            }
        });
    }
</script>

</body>
</html>
