<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{app_config('AppTitle')}}</title>
    <link rel="icon" type="image/x-icon"  href="<?php echo asset(app_config('AppFav')); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{--Global StyleSheet Start--}}
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    {!! Html::style("assets/libs/bootstrap/css/bootstrap.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-toggle/css/bootstrap-toggle.min.css") !!}
    {!! Html::style("assets/libs/font-awesome/css/font-awesome.min.css") !!}
    {!! Html::style("assets/libs/alertify/css/alertify.css") !!}
    {!! Html::style("assets/libs/alertify/css/alertify-bootstrap-3.css") !!}
    {!! Html::style("assets/libs/bootstrap-select/css/bootstrap-select.min.css") !!}

    {{--Custom StyleSheet Start--}}
    <style> 
        .borderless td, .borderless th {
            border: none;
            
        }
        .color{
            color: #5FFFFD;
        }
        .scrollable-menu {
            height: auto;
            max-height: 200px;
            overflow-x: hidden;
        }
        .scrollable-navbar {
            height: auto;
            width: 50px;
            overflow-x: hidden;
        }
        .nav-bottom-sec::-webkit-scrollbar{
        width: 50px;
        border-radius: 20px;
        }
        .panel-body {
            overflow-x: auto;
            overflow-y: auto;
        }
        .sembunyi {
            display: none;
        }

        .block {
            display: block;
        }
        .center {
            text-align: center;
        }
    </style>
    @yield('style')

    {{--Global StyleSheet End--}}

    {!! Html::style("assets/css/style.css") !!}
    {!! Html::style("assets/css/admin.css") !!}
    {!! Html::style("assets/css/responsive.css") !!}


</head>



<body class="has-left-bar has-top-bar">

<nav id="left-nav" class="left-nav-bar">
    <div class="nav-top-sec">
        <div class="app-logo">
            <img src="<?php echo asset(app_config('AppLogo')); ?>" alt="logo" class="bar-logo">
        </div>

        <a href="#" id="bar-setting" class="bar-setting"><i class="fa fa-bars"></i></a>
    </div>
    <div class="nav-bottom-sec">
        <ul class="left-navigation" id="left-navigation">
            <?php 
            use App\Menu;
            $menu = Menu::Where('menu_type','=',0)->Where('status','=','y')->orderBy('indexs','ASC')->get();
            ?>

            @foreach($menu as $m)
            @if(menu_access($m->id))
                <?php 
                    $has_sub = Menu::Where('menu_parent','=',$m->id)->Where('status','=','y')->first();
                    $sub_menus = Menu::Where('menu_parent','=',$m->id)->Where('status','=','y')->orderBy('indexs','ASC')->get();
                ?>
                @if($has_sub)
                <li class="has-sub">
                    <a href="#"><span class="menu-text color">{{language_data($m->name)}}</span> <span class="arrow"></span><span class="menu-thumb"><i class="{{$m->icon}}"></i></span></a>
                    <ul class="sub">
                        @foreach($sub_menus as $sm)
                            @if(menu_access($sm->id))
                            <li ><a href="{{url($sm->url)}}"><span class="menu-text">{{language_data($sm->name)}}</span> <span class="menu-thumb"><i class="{{$sm->icon}}"></i></span></a></li>
                            @endif
                        @endforeach
                        {{-- @if($m->id==17)
                            <li><a href="http://103.12.31.58/accounting-patra/home/login"><span class="menu-text">Akuntansi</span> <span class="menu-thumb"><i class="fa fa-money"></i></span></a></li>
                        @endif --}}
                    </ul>
                </li>
                @else
                <li><a href="{{url($m->url)}}"><span class="menu-text color">{{language_data($m->name)}}</span> <span class="menu-thumb"><i class="{{$m->icon}}"></i></span></a>
                @endif
            @endif
            @endforeach

        </ul>
    </div>
</nav>

<main id="wrapper" class="wrapper">

    <div class="top-bar clearfix">
        <ul class="top-info-bar">
            {{-- <li class="dropdown bar-notification @if(count(latest_five_leave_application())>0) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-bed"></i></a>
                <ul class="dropdown-menu user-dropdown arrow" role="menu">
                    <li class="title">{{language_data('Recent 5 Leave Applications')}}</li>

                    @foreach(latest_five_leave_application() as $l)
                    <li>
                        <a href="{{url('leave/edit/'.$l->id)}}">

                            <div class="user-name">{{$l->employee_id->fname}} {{$l->employee_id->lname}}</div>
                        </a>
                    </li>

                    @endforeach

                    <li class="footer"><a href="{{url('leave')}}">{{language_data('See All Applications')}}</a></li>
                </ul>
            </li>

            <li class="dropdown bar-notification @if(count(latest_five_tasks())>0) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cloud-download"></i></a>
                <ul class="dropdown-menu arrow" role="menu">
                    <li class="title">{{language_data('Recent 5 Pending Tasks')}}</li>

                    @foreach(latest_five_tasks() as $t)
                        <li><a href="{{url('task/view/'.$t->id)}}">{{$t->task}}</a></li>
                    @endforeach

                    <li class="footer"><a href="{{url('task')}}">{{language_data('See All Tasks')}}</a></li>
                </ul>
            </li>

            <li class="dropdown bar-notification @if(count(latest_five_tickets())>0) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-envelope"></i></a>
                <ul class="dropdown-menu arrow message-dropdown" role="menu">
                    <li class="title">{{language_data('Recent 5 Pending Tickets')}}</li>
                    @foreach(latest_five_tickets() as $st)
                    <li>
                        <a href="{{url('support-tickets/view-ticket/'.$st->id)}}">
                            <div class="name">{{$st->name}} <span>{{get_date_format($st->date)}}</span></div>
                            <div class="message">{{$st->subject}}</div>
                        </a>
                    </li>
                    @endforeach

                    <li class="footer"><a href="{{url('support-tickets/all')}}">{{language_data('See All Tickets')}}</a></li>
                </ul>
            </li> --}}

            <li class="dropdown bar-notification @if(count(latest_five_events())>0) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-calendar"></i></a>
                <ul class="dropdown-menu arrow message-dropdown scrollable-menu" role="menu">
                    @foreach(latest_five_events() as $st)
                    <li>
                        <a href="{{url('calendar/view-calendar/'.$st->id)}}">
                            <div class="occasion">{{$st->occasion}} <span>{{get_date_format($st->start_date)}}</span></div>
                            <div class="message">{{$st->description}}</div>
                        </a>
                    </li>
                    @endforeach
                    <li class="footer"><a href="{{url('calendar')}}">See All Events</a></li>
                </ul>
            </li>

            <li class="dropdown bar-notification @if(count(notifications())>0) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-bell"></i></a>
                <ul class="dropdown-menu arrow message-dropdown scrollable-menu" role="menu">
                    @foreach(notifications() as $st)
                    @if($st->show_date==date('Y-m-d'))
                    <li>
                        <a href="{{url('notifications/all')}}">
                            <div class="title">{{$st->title}} <span>{{get_date_format($st->show_date)}}</span></div>
                            <div class="message">{{$st->description}}</div>
                        </a>
                    </li>
                    @endif
                    @endforeach

                    <li class="footer"><a href="{{url('notifications/all')}}">See All Notifications</a></li>
                </ul>
            </li>

        </ul>


        <div class="navbar-right">

            <div class="clearfix">


                <div class="dropdown user-profile pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <span class="user-info">{{Auth::user()->fname}} {{Auth::user()->lname}}</span>

                        @if(Auth::user()->avatar!='')
                            <img class="user-image" src="<?php echo asset('assets/employee_pic/'.Auth::user()->avatar); ?>" alt="{{Auth::user()->fname}} {{Auth::user()->lname}}">
                        @else
                            <img class="user-image" src="<?php echo asset('assets/employee_pic/user.png'); ?>" alt="{{Auth::user()->fname}} {{Auth::user()->lname}}">
                        @endif

                    </a>
                    <ul class="dropdown-menu arrow right-arrow" role="menu">
                        <li><a href="{{url('user/edit-profile')}}"><i class="fa fa-edit"></i>{{language_data('Update Profile')}}</a></li>
                        <li><a href="{{url('user/change-password')}}"><i class="fa fa-lock"></i>{{language_data('Change Password')}}</a></li>
                        <li class="bg-dark">
                            <a href="{{url('user/logout')}}" class="clearfix">
                                <span class="pull-left">{{language_data('Logout')}}</span>
                                <span class="pull-right"><i class="fa fa-power-off"></i></span>
                            </a>
                        </li>
                    </ul>
                </div>



                @if(Auth::user()->role_id!='1')
                <div class="top-info-bar m-r-10">

                    <div class="pull-right">
                        <a href="{{url('employee/dashboard')}}" target="_blank" class="btn btn-success btn-sm" aria-expanded="false">{{language_data('My Portal')}}</a>
                    </div>
                </div>
                @endif

                <div class="top-info-bar m-r-10">

                    
                    <div class="dropdown pull-right bar-notification">
                        <a href="#" class="dropdown-toggle text-success" data-toggle="dropdown" role="button" aria-expanded="false">
                            <img src="<?php echo asset('assets/country_flag/'.\App\Language::find(app_config('Language'))->icon); ?>" alt="Language">
                        </a>
                        <ul class="dropdown-menu lang-dropdown arrow right-arrow" role="menu">
                            @foreach(get_language() as $lan)
                                <li>
                                    <a href="{{url('language/change/'.$lan->id)}}" @if($lan->id==app_config('Language')) class="text-complete" @endif>
                                        <img class="user-thumb" src="<?php echo asset('assets/country_flag/'.$lan->icon); ?>" alt="user thumb">
                                        <div class="user-name">{{$lan->language}}</div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    
                </div>



            </div>

        </div>
    </div>

    {{--Content File Start Here--}}

    @yield('content')

    {{--Content File End Here--}}

    <input type="hidden" id="_url" value="{{url('/')}}">
    <input type="hidden" id="_DatePicker" value="{{app_config('DateFormat')}}">
</main>

{{--Global JavaScript Start--}}
{!! Html::script("assets/libs/jquery-1.10.2.min.js") !!}
{!! Html::script("assets/libs/jquery.slimscroll.min.js") !!}
{!! Html::script("assets/libs/smoothscroll.min.js") !!}
{!! Html::script("assets/libs/bootstrap/js/bootstrap.min.js") !!}
{!! Html::script("assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js") !!}
{!! Html::script("assets/libs/alertify/js/alertify.js") !!}
{!! Html::script("assets/libs/bootstrap-select/js/bootstrap-select.min.js") !!}
{!! Html::script("assets/js/scripts.js") !!}
{!! Html::script("assets/libs/moment/moment.min.js")!!}
{!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
{!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
{!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
{!! Html::script("application/vendor/ckeditor/ckeditor/ckeditor.js")!!}
{!! Html::script("application/vendor/ckeditor/ckeditor/adapters/jquery.js")!!}
{!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
{!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
{!! Html::script("assets/js/form-elements-page.js")!!}
{!! Html::script("assets/libs/data-table/datatables.min.js")!!}
{!! Html::script("assets/js/bootbox.min.js")!!}
{{--Global JavaScript End--}}

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        }
    });
</script>

{{--Custom JavaScript Start--}}

@yield('script')

{{--Custom JavaScript End Here--}}
</body>

</html>
