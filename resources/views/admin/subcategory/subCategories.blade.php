@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Sub Category')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">
                @if($permcheck->C==1)
                <div class="col-lg-4">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('subcategories/add')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> {{language_data('Add Sub Category')}}</h3>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Name')}}</label>
                                    <input type="text" class="form-control" required name="subcategory">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Status')}}</label>
                                    <select name="status" class="form-control selectpicker">
                                        <option value="active">{{language_data('active')}}</option>
                                        <option value="inactive">{{language_data('inactive')}}</option>
                                    </select>
                                </div>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add')}} </button>
                            </form>
                        </div>
                    </div>
                </div>
                @endif

                <div class="col-lg-8">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('All Sub Category')}}</h3>
                            @if($permcheck->C==1)
                            <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload">{{language_data('Upload')}}</button></a><br>     
                            @endif                  
                            
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 10%;">No</th>
                                    <th style="width: 35%;">{{language_data('Sub Category')}}</th>
                                    <th style="width: 25%;">{{language_data('Status')}}</th>
                                    <th style="width: 35%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($subcategories as $ctr => $d)
                                <tr>
                                    <td data-label="no">{{$ctr+1}}</td>
                                    <td data-label="Sub Category"><p>{{$d->subcategory}}</p></td>
                                    @if($d->status=='active')
                                        <td data-label="status"><p class="btn btn-complete btn-xs">{{language_data('Active')}}</p></td>
                                    @elseif($d->status=='inactive')
                                        <td data-label="status"><p class="btn btn-danger btn-xs">{{language_data('Inactive')}}</p></td>
                                    @endif
                                    <td>
                                        <a class="btn btn-success btn-xs" href="{{url('subcategories/edit/'.$d->id)}}"><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>

                                    </td>
                                </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('subcategories/importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file" accept="application/vnd.ms-excel"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
                                
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('subcategories/downloadExcel/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function(){
            $('.data-table').DataTable();

        });
    </script>
@endsection
