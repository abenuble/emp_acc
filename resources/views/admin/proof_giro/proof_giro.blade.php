@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Bukti Giro</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Bukti Giro</h3>
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 5%;">Tipe</th>
                                    <th style="width: 20%;">{{language_data('Date')}}</th>
                                    <th style="width: 25%;">{{language_data('Proof Number')}}</th>
                                    <th style="width: 20%;">{{language_data('Information')}}</th>
                                    <th style="width: 15%;">Nominal CEK/BG</th>
                                    <th style="width: 25%;">Nominal Saldo</th>
                                    <th style="width: 15%;">{{language_data('Action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr=0; ?>
                                @foreach($bkp as $b)
                                    @if($b->nomor_giro!='' &&$b->nomor_giro!=null)
                                <?php $ctr++; ?>
                                    <tr>
                                    <form name="form" role="form" method="post" action="{{url('accountings/update-giro')}}">
                                    <input type="hidden" name="tipe" value="bkp"/>
                                    <input  type="hidden" name="id" value="{{$b->id}}"/>
                                        <td data-label="No"><?php echo $ctr; ?></td>
                                        <td data-label="No">BKP</td>
                                        <td data-label="Date"><p>{{get_date_format($b->tanggal_giro)}}</p></td>
                                        <td data-label="Proof Number"><p>{{$b->nomor_giro}}</p></td>
                                        <td data-label="Information"><p>{{$b->keterangan_giro}}</p></td>
                                        <td data-label="Value" class='nominal'><p>{{number_format($b->expenditure_nominal,0)}}</p>
                                        
                                        </td>
                                        <td data-label="Information" class='nominal'><input type='text' class="form-control" value="{{number_format($b->saldo,0)}}" name="saldo"/></td>
                                        <td data-label="Information"><input type='submit' class="btn btn-warning"  value="update"/></td>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                                        </form>
                                        
                                    </tr>
                                    @endif
                                @endforeach
                                @foreach($bkm as $b)
                                 @if($b->nomor_giro!='' &&$b->nomor_giro!=null)
                                <?php $ctr++; ?>
                                    <tr>
                                    <form name="form" role="form" method="post" action="{{url('accountings/update-giro')}}">
                                    <input type="hidden" name="tipe" value="bkm"/>
                                    <input  type="hidden" name="id" value="{{$b->id}}"/>

                                        <td data-label="No"><?php echo $ctr; ?></td>
                                        <td data-label="No">BKM</td>
                                        <td data-label="Date"><p>{{get_date_format($b->tanggal_giro)}}</p></td>
                                        <td data-label="Proof Number"><p>{{$b->nomor_giro}}</p></td>
                                        <td data-label="Information"><p>{{$b->keterangan_giro}}</p></td>
                                        <td data-label="Value" ><p>{{number_format($b->income_nominal,0)}}</p>
                                        </td>
                                        <td data-label="Information" class='nominal'><input type='text' class="form-control" value="{{number_format($b->saldo,0)}}" name="saldo"/></td>
                                        <td data-label="Information" class='nominal'><input type='submit' class="btn btn-warning" value="update"/></td>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                                        </form>
                                    </tr>
                                    @endif
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}
    <script>
        $(document).ready(function () {
            $('.nominal').number( true, 0, '.', ',' );
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/banks/delete/" + id;
                    }
                });
            });


        });
    </script>
@endsection
