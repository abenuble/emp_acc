@extends('master')

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Component Detail')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

            {{--View Detail Components--}}
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">

                            <form class="form" role="form" action="{{url('unit-components/set-unit-component-status')}}" method="post">
                                
                                <div class="form-group">
                                    <label>{{language_data('Component Name')}}</label>
                                    <input type="text" class="form-control" required="" name="unit"  readonly value="{{$unit->unit}}">
                                </div>

                                <div class="form-group m-none">
                                    <label for="e20">{{language_data('Status')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="status">
                                        <option value="inactive" @if($unit->status=='inactive') selected @endif >{{language_data('Inactive')}}</option>
                                        <option value="active" @if($unit->status=='active') selected @endif >{{language_data('Active')}}</option>
                                    </select>
                                </div>
                                <br>
                                @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$unit->id}}">
                                <button type="submit" class="btn pull-right btn-sm btn-primary">{{language_data('Update')}}</button>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}

    <script>
        
    </script>
@endsection
