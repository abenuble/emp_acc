@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Cashflow')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-10">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('cashflow/update')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> {{language_data('View Cashflow')}}</h3>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Code')}}</label>
                                    <input type="text" class="form-control" required="" value="{{$cashflow->cashflow_code}}" name="cashflow_code">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Cashflow Name')}}</label>
                                    <input type="text" class="form-control" required="" value="{{$cashflow->cashflow}}" name="cashflow">
                                </div>
                                <div class="form-group">
                                    <label>{{language_data('Debit/Credit')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="debit_credit">
                                        <option value="debit" @if($cashflow->debit_credit=='debit') selected @endif>{{language_data('Debit')}}</option>
                                        <option value="credit" @if($cashflow->debit_credit=='credit') selected @endif>{{language_data('Credit')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{{language_data('Information')}}</label>
                                    <textarea type="text" class="form-control" rows="8" name="information">{{$cashflow->information}}</textarea>
                                </div>
                                            
                                <div class="form-group">
                                    <label>{{language_data('Status')}} : </label>
                                    <select class="selectpicker form-control" data-live-search="true" name="status">
                                        <option value="active" @if($cashflow->status=='active') selected @endif >{{language_data('Active')}}</option>
                                        <option value="inactive" @if($cashflow->status=='inactive') selected @endif >{{language_data('Inactive')}}</option>
                                    </select>
                                </div>

                                @if($permcheck->U==1)
                                <div class="row">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" value="{{$cashflow->id}}" name="cmd">
                                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                                </div>
                                @endif
                                
                            </form>
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();


        });

    </script>
@endsection
