<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - Surat Pengantar</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        body {
            width: 210mm;
            min-height: 297mm;
            margin: 0;
            padding: 0;
            font: 12pt "Tahoma";
        }
        tbody {
            display: table-row-group;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-25 p-t-none p-b-none">
            <table>
                <tbody>
                    <tr>
                        <td>{!!$message!!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</main>
{!! Html::script("assets/libs/qrcode/qrcode.js") !!}
<script type='text/javascript'>

   
</script>

</body>
</html>