<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - Surat Pengantar</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        body {
            width: 210mm;
            min-height: 297mm;
            margin: 0;
            padding: 0;
            font: 12pt "Tahoma";
        }
        thead {
            display: table-header-group;
        }
        tbody {
            display: table-row-group;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-30 p-t-none p-b-none">
            <?php $ctr=0; ?>
            @foreach($message as $m)
                <?php $ctr++; ?>
                <table>
                    <tbody>
                        <tr>
                            <td>{!!$m['message']!!}</td>
                        </tr>
                    </tbody>
                </table>
                <p style="page-break-after: always;">&nbsp;</p>
            @endforeach

            <div class="m-b-20"></div>

        </div>
    </div>
</main>
{!! Html::script("assets/libs/qrcode/qrcode.js") !!}
<script type='text/javascript'>

   
</script>

</body>
</html>