@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Notifications</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Notifications</h3>
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 25%;">Title</th>
                                    <th style="width: 20%;">Description</th>
                                    {{-- <th style="width: 10%;">Perusahaan</th> --}}
                                    <th style="width: 10%;">{{language_data('Start Date')}}</th>
                                    <th style="width: 10%;">{{language_data('End Date')}}</th>
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($notifications as $a)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Title"><p>{{$a->title}}</p></td>
                                        <td data-label="Description"><p>{{$a->description}}</p></td>
                                        {{-- <td data-label="Perusahaan"><p>{{notif_company($a->tag,$a->id_tag)}}</p></td> --}}
                                        @if($a->start_date=='')
                                            <td data-label="Start Date"><p>{{get_date_format($a->show_date)}}</p></td>
                                        @else
                                            <td data-label="Start Date"><p>{{get_date_format($a->start_date)}}</p></td>
                                        @endif
                                        <td data-label="End Date"><p>{{get_date_format($a->end_date)}}</p></td>
                                        <td data-label="Actions" class="">
                                        @if($a->route != '')
                                            <a class="btn btn-complete btn-xs" href="{{url($a->route)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        @endif
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();
        });
    </script>
@endsection
