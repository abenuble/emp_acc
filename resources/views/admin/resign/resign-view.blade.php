@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Resign')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> View Resign
                                @if($resigns->status=='draft')
                                    <a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a>
                                @elseif($resigns->status=='accepted')
                                    <a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a>
                                @else
                                    <a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a>
                                @endif
                                </h3>
                            </div>

                            <form class="form-some-up" role="form" method="post" id="form" action="{{url('resigns/update')}}" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Resign Letter Number')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$resigns->letter_number}}" name="letter_number">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Letter Draft')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$resigns->draft_letter_info->tplname}}" name="draft_letter">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker" readonly required="" value="{{get_date_format($resigns->date)}}" name="date">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Resign Date')}}</label>
                                            <input type="text" class="form-control datePicker" readonly required="" value="{{get_date_format($resigns->resign_date)}}" name="resign_date">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$resigns->project_info->project_number}}" name="project_number">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Company')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$resigns->company_info->company}}" name="company_name">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-none" >
                                            <label for="e20">{{language_data('Employee Code')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$resigns->employee_code}}" name="employee_code">
                                        </div>
                                    
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Employee Name')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$resigns->employee_info->fname}} {{$resigns->employee_info->lname}}" name="employee_name">
                                        </div>
                                    
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-none" >
                                            <label for="e20">{{language_data('Resign Type')}}</label>
                                            <select name="resign_type" class="form-control selectpicker" data-live-search="true">
                                                <option value="RSG" @if($resigns->resign_type=='RSG') selected @endif>{{language_data('Resign')}}</option>
                                                <option value="PHK" @if($resigns->resign_type=='PHK') selected @endif>{{language_data('Termination')}}</option>
                                            </select>
                                        </div>
                                    
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Date of Join')}}</label>
                                            <input type="text" class="form-control datePicker" readonly value="{{get_date_format($resigns->employee_info->doj)}}" name="doj" id="doj">
                                        </div>
                                    
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                        <label>{{language_data('Reason')}}</label>
                                            <textarea type="text" rows="5" readonly class="form-control" name="reason" >{{$resigns->reason}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Insurance Provider</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$resigns->insurance_provider}}" name="insurance_provider">
                                        </div>
                                    </div>
                                </div>
                                @if($resigns->status!='draft')
                                <div class="row">
                                    <div class="col-lg-12">
                                <div class="form-group">
                                        <label for="message">{{language_data('Approval Information')}}</label>
                                        <input type="text" class="form-control"   @if($resigns->status!='draft') readonly @endif value="{{$resigns->keterangan}}" name="keterangan"/>
                                    </div>
                                    </div>
                                    </div>
                                    @endif
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>File Resign Letter</label>
                                            <br>
                                            <span><a href="{{url('resigns/download-file-resign-letter/'.$resigns->id)}}">{{$resigns->file_resign_letter}}</a></span>
                                        </div>
                                    </div>
                                </div>
                                
                            @if($resigns->status=='accepted' && $resigns->file_acceptance_resign=='')
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>File Acceptance Resign Letter</label>
                                            <div class="input-group">
                                                <input name="file_acceptance_resign" class="form-control" type="file" accept="image/*">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @elseif($resigns->file_acceptance_resign!='')
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>File Acceptance Resign Letter</label>
                                            <br>
                                            <span><a href="{{url('resigns/download-file-acceptance-resign-letter/'.$resigns->id)}}">{{$resigns->file_acceptance_resign}}</a></span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <!-- <div class="row">
                                    <div class="col-lg-12">
                                <div class="form-group">
                                    <label>{{language_data('Copies')}}</label>
                                    <input type="text" class="form-control" value="{{$resigns->tembusan}}" @if($resigns->status!='draft') readonly @endif  name="tembusan"/>
                                </div>
                                </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Copies')}}</label>
                                            @foreach($copies as $c)
                                            <div class="container1">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" readonly value="{{$c->copies}}" name="copies[]">
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div><br>

                            @if($resigns->status!='draft')
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="cmd" value="{{$resigns->id}}">
                                @if($resigns->status=='accepted')
                                <a href="{{url('resigns/previewPdf/'.$resigns->id)}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate PDF')}}</a>
                                @endif
                                <br>    
                            @else
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="cmd" value="{{$resigns->id}}">
                            <button type="submit" class="btn btn-xs btn-primary pull-right">{{language_data('Update')}}</button><br>    
                            @endif
                            </form>
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();


        });

    </script>
@endsection
