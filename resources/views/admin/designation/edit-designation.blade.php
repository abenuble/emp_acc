@extends('master')

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Edit Designation')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <form method="POST" action="{{ url('designations/update') }}">

                                <div class="form-group">
                                    <label>{{language_data('Designation Name')}} :</label>
                                    <input type="text" class="form-control" required="" name="designation" value="{{$designations->designation}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Division')}} : </label>
                                    <select class="selectpicker form-control" required=""  data-live-search="true" name="department">
                                        @foreach($departments as $d)
                                            <option value="{{$d->id}}" @if($designations->did==$d->id) selected @endif >{{$d->department}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Career Path')}}</label>
                                    <select class="selectpicker form-control"  required="" data-live-search="true" name="career_path">
                                        @foreach($career as $c)
                                            <option value="{{$c->id}}" @if($designations->career==$c->id) selected @endif >{{$c->designation}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Status')}} :</label>
                                    <select name="status" class="form-control selectpicker">
                                        <option value="active"   @if($designations->status=='active') selected @endif>{{language_data('active')}}</option>
                                        <option value="inactive" @if($designations->status=='inactive') selected @endif>{{language_data('inactive')}}</option>
                                    </select>
                                </div>


                                <div class="hr-line-dashed"></div>
                                @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="cmd" value="{{$designations->id}}">
                                <a class="btn btn-danger btn-sm pull-left" href="{{url('designations')}}">{{language_data('Back')}}</a>
                                <button type="submit" class="btn btn-sm pull-right btn-success"><i class="fa fa-edit"></i> {{language_data('Update')}}</button>
                                @endif
                            </form>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
@endsection
