@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Karyawan Resign</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Karyawan Resign</h3><br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 10%;">NIK</th>
                                    <th style="width: 20%;">{{language_data('Name')}}</th>
                                    <th style="width: 15%;">{{language_data('Company')}}</th>
                                    <th style="width: 15%;">{{language_data('Designation')}}</th>
                                    <th style="width: 15%;">{{language_data('Date of Join')}}</th>
                                    <th style="width: 15%;">{{language_data('Date of Leave')}}</th>
                                    <th style="width: 5%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($employee_resigns as $d)
                                <?php $ctr++; ?>
                                <tr>
                                    <td data-label="No">{{$ctr}}</td>
                                    <td data-label="NIK"><p>{{$d->employee_code}}</p></td>
                                    <td data-label="Name"><p>{{$d->employee_info->fname}}</p></td>                                  
                                    <td data-label="Company"><p>{{$d->company_name->company}}</p></td>                             
                                    <td data-label="Designation"><p>{{$d->employee_info->designation_name->designation}}</p></td>         
                                    <td data-label="Date of join"><p>{{get_date_format($d->employee_info->doj)}}</p></td>        
                                    <td data-label="Date of leave"><p>{{get_date_format($d->dol)}}</p></td>                       
                                    <td data-label="Actions">
                                        <a href="#" class="btn btn-danger btn-xs cblock" id="{{$d->id}}"><i class="fa fa-ban"></i> Blokir</a>
                                    </td>
                                </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>           

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){
            $('.data-table').DataTable();
            $(".cblock").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/employee-resigns/block/" + id;
                    }
                });
            });

        });
    </script>
@endsection
