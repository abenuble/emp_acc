@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Account Setting</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Account Setting</h3>
                            <br>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('accountings/account-setting/update-post')}}">
                            <div class="panel-body p-none">
                                <table class="table table-hover table-ultra-responsive" id="default-table">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 25%"> Nama Menu </th>
                                            <th class="text-center" style="width: 25%"> Transaksi </th>
                                            <th class="text-center" style="width: 25%"> Debit </th>
                                            <th class="text-center" style="width: 25%"> Kredit </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($account_setting as $s)
                                        <tr>
                                            <td class="text-center"> 
                                                {{$s->menu}}
                                                <input type="hidden" name="setting_akun_id[]" value="{{$s->id}}">
                                            </td>
                                            <td class="text-center">
                                                {{$s->activities}}
                                            </td>
                                            <td>
                                                <?php
                                                    $m_coa_id_debit =  json_decode($s->debit);
                                                    for ($i = 0; $i < sizeof($m_coa_id_debit); $i++) { 
                                                ?>
                                                <select class="form-control selectpicker" name="m_coa_id_debit{{$s->id}}[]" data-live-search="true" style="width: 100%" required>
                                                @foreach($coa as $c)
                                                    <option value="{{$c->id}}" @if($c->id==$m_coa_id_debit[$i]) selected @endif>({{$c->coa_code}}) {{$c->coa}}</option>
                                                @endforeach
                                                </select><br><br>
                                                <?php
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $m_coa_id_kredit =  json_decode($s->credit);
                                                    for ($i = 0; $i < sizeof($m_coa_id_kredit); $i++) { 
                                                ?>
                                                <select class="form-control selectpicker" name="m_coa_id_kredit{{$s->id}}[]" data-live-search="true" style="width: 100%" required>
                                                @foreach($coa as $c)
                                                    <option value="{{$c->id}}" @if($c->id==$m_coa_id_kredit[$i]) selected @endif>({{$c->coa_code}}) {{$c->coa}}</option>
                                                @endforeach
                                                </select><br><br>
                                                <?php
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <a type="button" class="btn btn-default pull-right" href="{{url('accountings/journal-manual')}}">{{language_data('Back')}}</a>
                                    @if($permcheck->U==1)
                                    <button type="submit" class="btn btn-primary pull-right">{{language_data('Save')}}</button>
                                    @endif
                                </div> 
                            </div>  
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/banks/delete/" + id;
                    }
                });
            });


        });
    </script>
@endsection
