@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">View Expense</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">

                            <form class="" role="form" action="{{url('approvals/set-expense-status')}}" method="post">
                                <div class="row">
                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label>Expense Number</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$expense->expense_number}}" name="expense_number">
                                        </div>

                                        <div class="form-group">
                                            <label>Post date</label>
                                            <input type="text" class="form-control datePicker"  readonly required="" value="{{get_date_format($expense->date)}}" name="post_date">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Company')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$expense->company_info->company}}" required="">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$expense->project_info->project}}" required="" name="project">
                                        </div>

                                        <div class="form-group">
                                            <label>Expense</label>
                                            <input type="text" class="form-control" required="" readonly value="{{$expense->about}}" name="about">
                                        </div>

                                        <div class="form-group">
                                            <label>Total</label>
                                            <input type="text" readonly class="form-control alltotal" value="{{number_format($expense->total,0)}}" required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="col-sm-12">
                                            <div class="panel">
                                                <div id="mydata2">
                                                    <table class="table table-hover table-ultra-responsive" id="myTableData2"  >
                                                        <thead>
                                                            <tr>
                                                            <th style="width: 30%;">{{language_data('Item')}}</th>
                                                            <th style="width: 10%;">{{language_data('QTY')}}</th>
                                                            <th style="width: 20%;">{{language_data('Unit Price')}}</th>
                                                            <th style="width: 10%;">PPN</th>
                                                            <th style="width: 30%;">{{language_data('Total Price')}}</th>
                                                            </tr>
                                                        </thead>
                                                        
                                                        <tbody>
                                                        @foreach($expense_detail as $ed)
                                                            <tr>
                                                                <td><input type="text" name="item[]" readonly value="{{ $ed->item,2}}" class="form-control"></td>
                                                                <td><input type="text" name="qty[]" readonly value="{{$ed->qty,2}}" class="form-control qty"></td>
                                                                <td><input type="text" name="unit[]" readonly value="{{$ed->unit_price,2}}" class="form-control unit"></td>
                                                                <td><input type="text"  name="ppnper[]" value="{{$ed->ppn,2}}"  readonly class="form-control persenz"></td>

                                                                <td><input type="text" name="total[]" readonly value="{{$ed->total_price,2}}" class="form-control total"></td>
                                                            
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="col-sm-12">
                                            <div class="panel">
                                                <div id="mydata2">
                                                    <table class="table table-hover table-ultra-responsive" id="myTableData3">
                                                        <tr>
                                                            <td style="width: 30%;"></td>
                                                            <td style="width: 20%;"></td>
                                                            <td style="width: 20%;"><center><label>Sub Total :</label></center></td>
                                                            <td style="width: 20%;"><input type="text" name="subtotal" readonly class="form-control subtotal"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 30%;"></td>
                                                            <td style="width: 20%;"></td>
                                                            <td style="width: 20%;"><center><label>Pembulatan :</label></center></td>
                                                            <td style="width: 20%;"><input type="text" readonly name="pembulatan" value="{{number_format($expense->pembulatan,0)}}" class="form-control feee"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 30%;"></td>
                                                            <td style="width: 20%;"></td>
                                                            <td style="width: 20%;"><center><label>PPN :</label></center></td>
                                                            <td style="width: 20%;"><input type="text" name="ppn" readonly value="{{number_format($expense->ppn_tax,0)}}" class="form-control ppn"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 30%;"></td>
                                                            <td style="width: 20%;"></td>
                                                            <td style="width: 20%;"><center><label>Total :</label></center></td>
                                                            <td style="width: 20%;"><input type="text" name="alltotal" value="{{number_format($expense->total,0)}}" readonly class="form-control alltotal"></td>
                                                        </tr>
                                                        <tr>
                                                        <td style="width: 30%;"></td>
                                                        <td style="width: 30%;"></td>
                                                        <td style="width: 30%;"><center><input type="checkbox" name="keterangan" @if($expense->keterangan==1) checked @endif />&nbsp;<label style="color:red;margin-right:0;text-align:right">*Biaya Tagihan Sudah Termasuk PPN</label></center></td>
                                                    </tr>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </div>

                            
                                @if($permcheck->U==1)
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="cmd" value="{{$expense->id}}">
                                    @if($expense->status=='Pending' || $expense->status=='Edited')
                                        <input type="submit" name="status" value="Approved" class="btn btn-success btn-xs pull-right">
                                        <input type="submit" name="status" value="Rejected" class="btn btn-danger btn-xs pull-right">
                                    @endif
                                @endif
                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/projects/delete/" + id;
                    }
                });
            });

            /*For Delete Project Doc*/
            $(".deleteProjectDoc").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/projects/delete-project-doc/" + id;
                    }
                });
            });

            /*For components Loading*/
            $("#payment_type").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'pay_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/projects/get-component',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#components").html( data);
                    }
                });
            });

            $('#myTableData2').ready(function() {
                var item_quantity = parseInt($(this).parents("tr").find(".qty").val());
                var unit_price = parseInt($(this).parents("tr").find(".unit").val());
                var total_value = unit_price * item_quantity;
                console.info(item_quantity, unit_price, total_value);
                if(!isNaN(total_value))
                    $(this).parents("tr").find(".total").val(total_value.toFixed(2)).number( true, 0, '.', ',' );
                    total();
            });

            $('#myTableData3').on("change",".fee, .ppn", function() {
                var fee = parseInt($(".fee").val());
                var ppn = parseInt($(".ppn").val());
                var subtotal = parseInt($(".subtotal").val());
                var alltotal = subtotal + fee - (fee*ppn/100);
                console.info(fee, ppn, alltotal);
                if(!isNaN(alltotal))
                    $(".alltotal").val(alltotal.toFixed(2)).number( true, 0, '.', ',' );
            });

        });       

        function addRow() {
         
            var designation = document.getElementById("designation");
            var textdesg = getSelectedText('designation');
            var location = document.getElementById("location");
            var open_date = document.getElementById("open_date");
            var close_date = document.getElementById("close_date");

            var desg = document.getElementById("desg");
            var length = document.getElementsByName("loc").length;
            var sama = length+1;
            var loc = document.getElementsByName("loc");

            var i = 0;

            //lokasi.push(location.value);

            var total = document.getElementById("total");
            var table = document.getElementById("myTableData");

            if(open_date.value==='' || close_date.value==='' || total.value===''){
                return false;
            } 

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            row.insertCell(0).innerHTML= '<input type="hidden" class="form-control" value="'+designation.value+'" name="designation[] id="desg"><input type="text" readonly class="form-control" value="'+textdesg+'">';
            row.insertCell(1).innerHTML= '<input type="hidden" readonly class="form-control" value="'+location.value+'" name="location[]"><input type="text" readonly class="form-control" value="'+location.value+'" name="loc">';
            row.insertCell(2).innerHTML= '<input type="text" readonly class="form-control" value="'+open_date.value+'" name="open_date[]">';
            row.insertCell(3).innerHTML= '<input type="text" readonly class="form-control" value="'+close_date.value+'" name="close_date[]">';
            row.insertCell(4).innerHTML= '<input type="text" class="form-control" value="'+total.value+'" name="total[]">';
            row.insertCell(5).innerHTML= '<input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';


        }


        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData");
            table.deleteRow(index);
        
        }

        function total() {
            var total1 = 0;
            $('input.total').each(function () {
                var n = parseInt($(this).val());
                total1 += isNaN(n) ? 0 : n;
            });
            $('.subtotal').val(total1.toFixed(2)).number( true, 0, '.', ',' );
        }

        function addRow2() {
         
            $('#myTableData2').append('<tr><td><input type="text" name="item[]" class="form-control"></td><td><input type="text" name="qty[]" class="form-control qty"></td><td><input type="text" name="unit[]" class="form-control unit"></td><td><input type="text" name="total[]" class="form-control total"></td><td data-label="Action"><input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow2(this)"></td></tr>');

        }


        function deleteRow2(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData2");
            table.deleteRow(index);
        
        }

        function load() {
        
            console.log("Page load finished");

        }
        

        
    </script>

@endsection