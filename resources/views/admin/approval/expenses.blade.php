@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Expenses</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Expenses</h3>
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 10%;">Expense Number</th>
                                    <th style="width: 15%;">Expense</th>
                                    <th style="width: 20%;">Post Date</th>
                                    <th style="width: 15%;">Total</th>
                                    <th style="width: 20%;">Status</th>
                                    <th style="width: 15%;">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($expenses as $e)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Expense Number">{{$e->expense_number}}</td>
                                        <td data-label="Expense">{{$e->about}}</td>
                                        <td data-label="Post Date">{{get_date_format($e->date)}}</td>
                                        <td data-label="Total">{{app_config('CurrencyCode')}} {{number_format($e->total,0)}}</td>
                                        @if($e->status=='Approved')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Approved')}}</p></td>
                                        @elseif($e->status=='Pending')
                                        
                                            <td data-label="Status"><p class="btn btn-warning btn-xs">Pending</p></td>
                                            @elseif($e->status=='Edited')
                                            <td data-label="Status"><p class="btn btn-info btn-xs">Edited</p></td>
                                        @elseif($e->status=='Rejected')
                                            <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Rejected')}}</p></td>
                                        @endif
                                        <td data-label="Actions">
                                            <a class="btn btn-complete btn-xs" href="{{url('approvals/view-expense/'.$e->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>

            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });

    
    </script>
@endsection
