@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
                <h2 class="page-title">{{language_data('Client Contract')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                                <h3 class="panel-title">{{language_data('Client Contract')}}</h3>
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 15%;">{{language_data('Client Contract Number')}}</th>
                                    <th style="width: 25%;">{{language_data('Client Contract')}}</th>
                                    <th style="width: 15%;">{{language_data('Company')}}</th>
                                    <th style="width: 10%;">{{language_data('Start Date')}}</th>
                                    <th style="width: 15%;">{{language_data('Facility')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 5%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($projects as $p)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Project Number"><p>{{$p->project_number}}</p></td>
                                        <td data-label="Project"><p>{{$p->project}}</p></td>
                                        <td data-label="Company"><p>{{$p->company_name->company}}</p></td>
                                        <td data-label="Start Date"><p>{{get_date_format($p->start_date)}}</p></td>
                                        <td data-label="Facility"><p>{{json_decode($p->facility)[0]}}@if(isset(json_decode($p->facility)[1])), {{json_decode($p->facility)[1]}} @endif</p></td>
                                        @if($p->status=='opening')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">Open</p></td>
                                            <td data-label="Actions">
                                                <div class="btn-group btn-mini-group dropdown-default">
                                                    <a class="btn btn-success btn-sm dropdown-toggle btn-animated from-top fa fa-caret-down" data-toggle="dropdown" href="#" aria-expanded="false"><span><i class="fa fa-bars"></i></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{url('approvals/view-project/'.$p->id)}}" data-toggle="tooltip" data-placement="right" title="{{language_data('View')}}" class="text-success"><i class="fa fa-eye"></i></a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        @elseif($p->status=='closed')
                                            <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Closed')}}</p></td>
                                            <td data-label="Actions">
                                            </td>
                                        @else
                                            <td data-label="Status"><p class="btn btn-warning btn-xs">{{language_data('Drafted')}}</p></td>
                                            <td data-label="Actions">
                                                <div class="btn-group btn-mini-group dropdown-default">
                                                    <a class="btn btn-success btn-sm dropdown-toggle btn-animated from-top fa fa-caret-down" data-toggle="dropdown" href="#" aria-expanded="false"><span><i class="fa fa-bars"></i></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{url('approvals/view-project/'.$p->id)}}" data-toggle="tooltip" data-placement="right" title="{{language_data('View')}}" class="text-success"><i class="fa fa-eye"></i></a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        @endif
                                        

                                        
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            
        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();


        });
        
    </script>
@endsection
