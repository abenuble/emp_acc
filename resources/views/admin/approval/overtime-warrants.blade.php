@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Overtime Warrants</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Overtime Warrants</h3>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 15%;">{{language_data('Company')}}</th>
                                    <th style="width: 15%;">{{language_data('Employee')}}</th>
                                    <th style="width: 20%;">{{language_data('Date')}}</th>
                                    <th style="width: 20%;">Time</th>
                                    <th style="width: 20%;">{{language_data('Information')}}</th>
                                    <th style="width: 5%;">{{language_data('Status')}}</th>
                                    <th style="width: 5%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($warrant as $w)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Company"><p>{{$w->employee_info->company_name->company}}</p></td>
                                        <td data-label="Employee"><p>{{$w->employee_info->fname}} {{$w->employee_info->lname}}</p></td>
                                        <td data-label="Date"><p>{{get_date_format($w->date_from)}} - {{get_date_format($w->date_to)}}</p></td>
                                        <td data-label="Time"><p>{{$w->overtime_from}} - {{$w->overtime_to}}</p></td>
                                        <td data-label="Information"><p>{{$w->information}}</p></td>
                                        @if($w->status=='rejected')
                                            <td data-label="status"><p class="btn btn-danger btn-xs">{{language_data('Rejected')}}</p></td>
                                        @elseif($w->status=='accepted')
                                            <td data-label="status"><p class="btn btn-success btn-xs">{{language_data('Accepted')}}</p></td>
                                        @else
                                            <td data-label="status"><p class="btn btn-warning btn-xs">{{language_data('Drafted')}}</p></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-success btn-xs" href="{{url('approvals/view-overtime-warrant/'.$w->id)}}"><i class="fa fa-edit"></i> {{language_data('View')}}</a>
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });
    </script>
@endsection
