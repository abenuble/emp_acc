@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Mutation')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Mutation')}}</h3>
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 10%;">{{language_data('Client Contract Number')}}</th>
                                    <th style="width: 10%;">{{language_data('Company')}}</th>
                                    <th style="width: 15%;">{{language_data('Employee')}}</th>
                                    <th style="width: 5%;">{{language_data('Area Code')}}</th>
                                    <th style="width: 20%;">{{language_data('Department')}}/{{language_data('Designation')}}</th>
                                    <th style="width: 20%;">{{language_data('Employee')}} {{language_data('Status')}}</th>
                                    <th style="width: 5%;">{{language_data('Status')}}</th>
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($mutations as $a)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Project Number"><p>{{$a->project_info->project_number}}</p></td>
                                        <td data-label="Company"><p>{{$a->company_info->company}}</p></td>
                                        <td data-label="Employee"><p>{{$a->employee_info->fname}} {{$a->employee_info->lname}}</p></td>
                                        <td data-label="Area Code"><p>{{$a->company_info->company_code}} > {{$a->company_info_new->company_code}}</p></td>
                                        <td data-label="Department/Designation"><p>{{$a->designation_old_info->designation}} > {{$a->designation_new_info->designation}}</p></td>
                                        <td data-label="Employee Status"><p>{{$a->employee_status_old}} > {{$a->employee_status_new}}</p></td>
                                        @if($a->status=='draft')
                                            <td data-label="status"><a class="btn btn-warning btn-xs" href="#" data-toggle="modal" data-target=".modal_set_status_{{$a->id}}">{{language_data('Drafted')}}</a></td>
                                        @elseif($a->status=='accepted')
                                            <td data-label="status"><a class="btn btn-success btn-xs" href="#" data-toggle="modal" data-target=".modal_set_status_{{$a->id}}">{{language_data('Accepted')}}</a></td>
                                        @else
                                            <td data-label="status"><a class="btn btn-danger btn-xs" href="#" data-toggle="modal" data-target=".modal_set_status_{{$a->id}}">{{language_data('Rejected')}}</a></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                        @if($a->status=='draft')
                                            <a class="btn btn-complete btn-xs" href="{{url('approvals/view-mutation/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        @elseif($a->status=='accepted')
                                            <a class="btn btn-complete btn-xs" href="{{url('approvals/view-mutation/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        @endif
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();
            
        });
    </script>
@endsection
