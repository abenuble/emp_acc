@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Termination')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> View Termination
                                @if($terminations->status=='draft')
                                    <a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a>
                                @elseif($terminations->status=='accepted')
                                    <a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a>
                                @else
                                    <a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a>
                                @endif
                                </h3>
                            </div>

                            <form class="form-some-up" role="form" method="post" id="form" action="{{url('approvals/set-termination-status')}}" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Termination Letter Number')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$terminations->letter_number}}" name="letter_number">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Letter Draft')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$terminations->draft_letter_info->tplname}}" name="draft_letter">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker" readonly required="" value="{{get_date_format($terminations->date)}}" name="date">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Effective Date')}}</label>
                                            <input type="text" class="form-control datePicker" readonly required="" value="{{get_date_format($terminations->effective_date)}}" name="effective_date">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$terminations->project_info->project_number}}" name="project_number">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Company')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$terminations->company_info->company}}" name="company_name">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-none" >
                                            <label for="e20">{{language_data('Employee Code')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$terminations->employee_code}}" name="employee_code">
                                        </div>
                                    
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Employee Name')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$terminations->employee_info->fname}} {{$terminations->employee_info->lname}}" name="employee_name">
                                        </div>
                                    
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group m-none">
                                            <label for="e20">Grounds</label>
                                            @foreach($grounds as $g)
                                            <div class="container1">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" readonly value="{{$g->grounds}}" name="grounds[]">
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Insurance Provider</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$terminations->insurance_provider}}" name="insurance_provider">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Copies')}}</label>
                                            @foreach($copies as $c)
                                            <div class="container1">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" readonly value="{{$c->copies}}" name="copies[]">
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="message">{{language_data('Information')}}</label>
                                        <input type="text" class="form-control"   @if($terminations->status!='draft') readonly @endif value="{{$terminations->keterangan}}"  name="keterangan"/>
                                    </div>  
                                </div><br>

                                @if($permcheck->U==1)
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="cmd" value="{{$terminations->id}}">
                                    @if($terminations->status=='draft')
                                        <input type="submit" name="status" value="accepted" class="btn btn-success btn-xs pull-right">
                                        <input type="submit" name="status" value="rejected" class="btn btn-danger btn-xs pull-right">
                                    @endif
                                @endif
                                @if($terminations->status=='accepted')
                                <a href="{{url('terminations/downloadPdf/'.$terminations->id)}}" class="btn btn-primary btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> Preview</a><br>    
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();


        });

    </script>
@endsection
