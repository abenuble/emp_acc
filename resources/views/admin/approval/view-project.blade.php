@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View contract client')}}</h2>
        </div>

        <div class="p-30 p-t-none p-b-none">
            <div class="row">

                <div class="col-lg-8">
                    <div class="panel">
                        <div class="panel-body p-t-20">
                            <div class="clearfix">
                                <h3 class="bold font-color-1">{{$projectview->project}}</h3>
                                <span>{{$projectview->company_name->company}}</span><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8">
                    <form class="form-some-up form-block" role="form" action="{{url('approvals/set-project-status')}}" method="post">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label>{{language_data('Client Contract Number')}}</label>
                                    <input type="text" class="form-control" readonly value="{{$projectview->project_number}}" name="id">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Client Contract')}}</label>
                                    <input type="text" class="form-control" readonly value="{{$projectview->project}}" name="projects">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <input type="text" class="form-control" readonly value="{{$projectview->company_name->company}}" name="company">
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Facility')}}</label>
                                            <div class="checkbox">
                                                <label><input type="checkbox" disabled id="facility1" class="facility" name="facility[]" @if(in_array('kesehatan', json_decode($projectview->facility))) checked @endif value="kesehatan">{{language_data('Kesehatan')}}</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" disabled id="facility2" class="facility" name="facility[]" @if(in_array('ketenagakerjaan', json_decode($projectview->facility))) checked @endif value="ketenagakerjaan">{{language_data('ketenagakerjaan')}}</label>
                                            </div>  
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group kesehatan" style="display:none;">
                                            <label>Jenis Fasilitas Kesehatan</label>
                                            <input type="text" class="form-control" readonly name="health_facility_type" id="health_facility_type" value="{{$projectview->health_facility_type}}">
                                        </div>

                                        <div class="form-group ketenagakerjaan" disabled style="display:none;">
                                            <label>Jenis Fasilitas Ketenagakerjaan</label><br>
                                            <input type="checkbox" name="accident_insurance" value="1" @if($projectview->accident_insurance=='1') checked @endif> Jaminan Kecelakaan Kerja (JKK)<br>
                                            <input type="checkbox" name="life_insurance" value="1" @if($projectview->life_insurance=='1') checked @endif> Jaminan Kematian (JKM)<br>
                                            <input type="checkbox" name="jaminan_hari_tua" value="1" @if($projectview->jaminan_hari_tua=='1') checked @endif> Jaminan Hari Tua (JHT)<br>
                                            <input type="checkbox" name="jaminan_pensiun" value="1" @if($projectview->jaminan_pensiun=='1') checked @endif> Jaminan Pensiun (JP)<br>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group kesehatan" style="display:none;">
                                            <label>Nama Asuransi Kesehatan</label>
                                            <input type="text" class="form-control" readonly name="insurance_name" id="insurance_name" value="{{$projectview->insurance_name}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('Start Date')}}</label>
                                        <input type="text" class="form-control datePicker" name="start_date" readonly value="{{get_date_format($projectview->start_date)}}" required="">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('End Date')}}</label>
                                        <input type="text" class="form-control datePicker" name="end_date" readonly value="{{get_date_format($projectview->end_date)}}" required="">
                                    </div>
                                </div>
                                
                                {{-- <div class="form-group" style="display:none">
                                    <label>{{language_data('Client Contract Value')}}</label>
                                    <input type="text" class="form-control" readonly value="{{app_config('Currency')}} {{number_format($projectview->value,0)}}" name="value">
                                </div> --}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                            <h3 class="panel-title">{{language_data('Client Contract')}}</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-hover table-ultra-responsive" id="table_detail">
                                            <thead>
                                            <tr>
                                                <th style="width: 5%;">No</th>
                                                <th style="width: 25%;">{{language_data('Designation')}}</th>
                                                <th style="width: 20%;">{{language_data('Location')}}</th>
                                                <th style="width: 15%;">{{language_data('Open Date')}}</th>
                                                <th style="width: 15%;">{{language_data('Close Date')}}</th>
                                                <th style="width: 15%;">{{language_data('Quota')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($projectneeds as $pro)
                                                <tr>
                                                    <td data-label="No">{{$pro->id_needs}}</td>
                                                    <td data-label="Designation"> <p><a href="{{url('jobs/view-applicant/'.$pro->id_job)}}">{{$pro->designation_name->designation}}</a></p></td>
                                                    <td data-label="Location">{{$pro->location}}</td>
                                                    <td data-label="Open Date">{{get_date_format($pro->open_date)}}</td>
                                                    <td data-label="Close Date">{{get_date_format($pro->close_date)}}</td>
                                                    <td data-label="Total">{{$pro->total}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        
                                    @if($permcheck->U==1)
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input type="hidden" name="cmd" value="{{$projectview->id}}">
                                        @if($projectview->status=='drafted')
                                            <input type="submit" name="status" value="opening" class="btn btn-success btn-xs pull-right">
                                            <input type="submit" name="status" value="closed" class="btn btn-danger btn-xs pull-right">
                                        @endif
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            $('.facility').ready(function() {
                if(document.getElementById("facility1").checked == true){
                    $('.kesehatan').show();
                    var asuransi = $('#health_facility_type').val();
                    $('#insurance_name').val(asuransi);
                } else {
                    $('.kesehatan').hide();
                }
                if(document.getElementById("facility2").checked == true){
                    $('.ketenagakerjaan').show();
                } else {
                    $('.ketenagakerjaan').hide();
                }
            });
            $('#health_facility_type').ready(function() {
                var asuransi = $('#health_facility_type').val();
                $('#insurance_name').val(asuransi);
                    
            });
        });

       
    </script>

@endsection
