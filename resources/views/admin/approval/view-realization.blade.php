@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">View Procurement</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> View Procurement
                                @if($realizations->realization_status=='draft')
                                    <a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a>
                                @elseif($realizations->realization_status=='approved')
                                    <a class="btn btn-success btn-xs">{{language_data('Approved')}}</a>
                                @else
                                    <a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a>
                                @endif
                                </h3>
                            </div>

                            <form class="form-some-up" role="form" method="post" id="form" action="{{url('approvals/set-realization-status')}}" enctype="multipart/form-data">                                    
                                <div class="col-lg-12">
                            
                                    <div class="form-group">
                                        <label>Realization Number</label>
                                        <input type="text" readonly class="form-control" required="" value="{{$realizations->realization_number}}" name="realization_number">
                                    </div>

                                    <div class="form-group">
                                        <label>Procurement Number</label>
                                        <input type="text" readonly class="form-control" required="" value="{{$realizations->procurement_number}}">
                                    </div>

                                    <div class="form-group">
                                        <label>Realization Date</label>
                                        <input type="text" readonly class="form-control datePicker" required="" value="{{get_date_format($realizations->realization_date)}}" name="realization_date">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Supplier</label>
                                        <input type="text" readonly class="form-control" required="" value="{{$realizations->supplier}}" name="supplier">
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Perihal</label>
                                            <input type="text" class="form-control " value="{{$realizations->perihal}}" name="perihal">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="col-sm-12">
                                            <div class="panel">
                                                <div id="mydata2">
                                                    <table class="table table-hover table-ultra-responsive" id="myTableData3" >
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 5%;">No</th>
                                                                <th style="width: 30%;">Item</th>
                                                                <th style="width: 10%;">QTY</th>
                                                                <th style="width: 10%;">QTY Bought</th>
                                                                <th style="width: 15%;">Unit Price</th>
                                                                <th style="width: 15%;">Total Price</th>
                                                                <th style="width: 15%;">Difference</th>
                                                            </tr>
                                                        </thead>
                                                        
                                                        <tbody>
                                                        <?php $tot=0; $ctr=0;?>
                                                        @foreach($procurement_goods as $i)
                                                            <?php $ctr++; ?>
                                                            <tr>
                                                                <td data-label="No">{{$ctr}}</td>
                                                                    <td data-label="Item">
                                                                        @if($realizations->type=='project')
                                                                            @if($i->item_info)
                                                                            <input type="text" name="item[]" readonly value=" {{$i->item_info->item}}" class="form-control">
                                                                         
                                                                            @else
                                                                            <input type="text" name="item[]" readonly value="-" class="form-control">
                                                                            @endif
                                                                        @else
                                                                        <input type="text" name="item[]" readonly value="{{$i->goods}}" class="form-control">
                                                                        @endif
                                                                        </td>
                                                                <td data-label="QTY"><input type="text" name="qty[]" readonly value="{{$i->quantity}}" class="form-control text-right"></td>
                                                                <td data-label="QTY Bought"><input type="text" name="realization_quantity[]" readonly value="{{$i->realization_quantity}}" class="form-control text-right"></td>
                                                                <td data-label="Unit Price"><input type="text" name="realization_uni_price[]" readonly value="{{number_format($i->realization_unit_price,0)}}" class="form-control text-right"></td>
                                                                <td data-label="Total Price"><input type="text" name="realization_total_price[]" readonly value="{{number_format(($i->realization_unit_price*$i->realization_quantity),0)}}" class="form-control text-right"></td>
                                                                <td data-label="Difference"></td>
                                                            </tr>
                                                            <?php $tot+=($i->realization_unit_price*$i->realization_quantity); ?>
                                                        @endforeach
                                                            <tr>
                                                                <td style="width: 5%;"></td>
                                                                <td style="width: 30%;"></td>
                                                                <td style="width: 10%;"></td>
                                                                <td style="width: 10%;"></td>
                                                                <td style="width: 15%;"><center><label>Total :</label></center></td>
                                                                <td style="width: 15%;"><input type="text" name="realization_total" readonly value="{{number_format($tot,0)}}" class="form-control text-right"></td>
                                                                <td style="width: 15%;"><input type="text" name="realization_differences" readonly value="{{number_format($realizations->realization_differences,0)}}" class="form-control text-right"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                                
                                @if($permcheck->U==1)
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="cmd" value="{{$realizations->id}}">
                                    @if($realizations->realization_status=='draft')
                                        <input type="submit" name="status" value="approved" class="btn btn-success btn-xs pull-right">
                                        <input type="submit" name="status" value="rejected" class="btn btn-danger btn-xs pull-right">
                                    @endif
                                @endif
                            <form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });       


        
    </script>

@endsection