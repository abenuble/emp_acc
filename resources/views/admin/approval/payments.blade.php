@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Payment</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Payment</h3>
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 15%;">{{language_data('Client Contract Number')}}</th>
                                    <th style="width: 10%;">Month</th>
                                    <th style="width: 25%;">{{language_data('Company')}}</th>
                                    <th style="width: 25%;">{{language_data('Payroll Type')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($payments as $a)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Project Number"><p>{{$a->project_info->project_number}}</p></td>
                                        <td data-label="Month"><p>{{get_date_month($a->month)}}</p></td>
                                        <td data-label="Company"><p>{{$a->company_info->company}}</p></td>
                                        <td data-label="Payroll Type"><p>{{$a->payroll_info->payroll_name}}</p></td>
                                        @if($a->status=='draft')
                                            <td data-label="status"><a class="btn btn-warning btn-xs" href="#">{{language_data('Drafted')}}</a></td>
                                        @elseif($a->status=='approved')
                                            <td data-label="status"><a class="btn btn-success btn-xs" href="#">{{language_data('Approved')}}</a></td>
                                        @else
                                            <td data-label="status"><a class="btn btn-danger btn-xs" href="#">{{language_data('Rejected')}}</a></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                        @if($a->status=='draft')
                                            <a class="btn btn-complete btn-xs" href="{{url('approvals/view-payment/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        @elseif($a->status=='approved')
                                            <a class="btn btn-complete btn-xs" href="{{url('approvals/view-payment/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        @endif
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>



        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();
            
        });
    </script>
@endsection
