@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">View Out Letter</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> View Out Letter
                                @if($pkbletters->status=='draft')
                                    <a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a>
                                @elseif($pkbletters->status=='approved')
                                    <a class="btn btn-success btn-xs">{{language_data('Approved')}}</a>
                                @else
                                    <a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a>
                                @endif
                                </h3>
                            </div>

                            <form class="form-some-up" role="form" method="post" id="form" action="{{url('approvals/set-out-letter-status')}}" enctype="multipart/form-data">
                                <div class="col-lg-12">
                                    

                                    <div class="form-group">
                                        <label for="letter_number">{{language_data('Letter Number')}}</label>
                                        <input type="text" class="form-control" readonly name="letter_number" value="{{$pkbletters->letter_number}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="subject">{{language_data('Subject')}}</label>
                                        <input type="text" readonly class="form-control" id="subject" name="subject" value="{{$pkbletters->subject}}">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="date">{{language_data('Date')}}</label>
                                        <input type="text" readonly class="form-control datePicker" required name="date" value="{{get_date_format($pkbletters->date)}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="message">{{language_data('Message')}}</label>
                                        <textarea id="textarea-wysihtml5" class="textarea-wysihtml5 form-control" name="message"  readonly>{!! $pkbletters->message !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="message">{{language_data('Copies')}}</label>
                                        <input type="text" class="form-control"  readonly  value="{{$pkbletters->tembusan}}"  name="tembusan"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="message">{{language_data('Information')}}</label>
                                        <input type="text" class="form-control"   @if($pkbletters->status!='draft') readonly @endif value="{{$pkbletters->information}}" name="information"/>
                                    </div>
                                    
                                </div>
                                @if($permcheck->U==1)
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="cmd" value="{{$pkbletters->id}}">
                                    @if($pkbletters->status=='draft')
                                        <input type="submit" name="status" value="approved" class="btn btn-success btn-xs pull-right">
                                        <input type="submit" name="status" value="rejected" class="btn btn-danger btn-xs pull-right">
                                    @endif
                                @endif
                                </div>
                               
                                
                               
                            <form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });       


        
    </script>

@endsection