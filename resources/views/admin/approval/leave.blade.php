@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Leave Application')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Leave Application')}}</h3>
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">{{language_data('SL')}}#</th>
                                    <th style="width: 20%;">{{language_data('Client Contract Number')}}</th>
                                    <th style="width: 10%;">{{language_data('Company')}}</th>
                                    <th style="width: 10%;">{{language_data('Employee')}}</th>
                                    <th style="width: 10%;">{{language_data('Leave Type')}}</th>
                                    <th style="width: 10%;">{{language_data('Leave From')}}</th>
                                    <th style="width: 10%;">{{language_data('Leave To')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($leave as $d)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="project_number">{{$d->employee_id->project_name->project_number}}</td>
                                        <td data-label="company"> {{$d->employee_id->company_name->company}}</td>
                                        <td data-label="employee"><p>
                                            @if($employee_permission->R==1) <a href="{{url('employees/view/'.$d->employee_id->id)}}"> {{$d->employee_id->fname}} {{$d->employee_id->lname}}</a> @else {{$d->employee_id->fname}} {{$d->employee_id->lname}} @endif
                                        </p></td>
                                        <td data-label="leaveType"><p>{{$d->leave_type->leave}}</p></td>
                                        <td data-label="LeaveFrom"><p>{{get_date_format($d->leave_from)}}</p></td>
                                        <td data-label="LeaveTo"><p>{{get_date_format($d->leave_to)}}</p></td>
                                        @if($d->status=='approved')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Approved')}}</p></td>
                                        @elseif($d->status=='pending')
                                            <td data-label="Status"><p class="btn btn-warning btn-xs">{{language_data('Pending')}}</p></td>
                                        @else
                                            <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Rejected')}}</p></td>
                                        @endif

                                        <td data-label="Actions">
                                        @if($d->status=='approved')
                                            <a class="btn btn-success btn-xs" href="{{url('approvals/view-leave/'.$d->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        @elseif($d->status=='pending')
                                            <a class="btn btn-success btn-xs" href="{{url('approvals/view-leave/'.$d->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        @else
                                        @endif
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>

            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });

    
    </script>
@endsection
