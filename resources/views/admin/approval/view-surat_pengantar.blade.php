@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Surat Pengantar')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
        
            <form class="form-some-up form-block" role="form" action="{{url('approvals/set-surat-pengantar-status')}}" method="post">
            @include('notification.notify')
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> {{language_data('View Contract')}}
                                @if($surat_pengantar->status=='draft')
                                    <a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a>
                                @elseif($surat_pengantar->status=='accepted')
                                    <a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a>
                                @else
                                    <a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a>
                                @endif
                                </h3>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Company')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$surat_pengantar->company_info->company}}" name="company_name">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Surat Pengantar Type')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$surat_pengantar->surat_pengantar_types}}" name="surat_pengantar_types">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Letter Draft')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$surat_pengantar->draft_letter_info->tplname}}" name="draft_letter">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Date')}}</label>
                                        <input type="text" class="form-control datePicker" readonly required="" value="{{get_date_format($surat_pengantar->date)}}" name="date">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Client Contract')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$surat_pengantar->project_info->project_number}}" name="project_number">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Client Contract Name')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$surat_pengantar->project_name}}" name="project_name">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>{{language_data('Surat Pengantar Recipients')}}</label><br>
                                        <label class="radio-inline">
                                            <input type="radio" disabled name="share_with" id="share_with_all" value="all" class="toggle_specific" @if($surat_pengantar->share_with=='all') checked @endif>{{language_data('All Employee')}}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" disabled name="share_with" id="share_with_specific_radio_button" value="specific" class="toggle_specific" @if($surat_pengantar->share_with=='specific') checked @endif>{{language_data('Specific Employee')}}
                                        </label>
                                        <div class="specific_dropdown" style="display: none;">
                                            <input type="text" value="" name="share_with_specific" id="share_with_specific" class="w100p validate-hidden" placeholder="choose members and or teams"  />
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>   
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    <label>{{language_data('Information')}}</label>
                                    <input type="text" class="form-control"  value="{{$surat_pengantar->keterangan}}" name="keterangan">

                                    </div>
                                    </div>
                                    </div>               
                            <br>  
                            @if($surat_pengantar->status=='accepted')
                            <a href="{{url('surat_pengantar/downloadAllPdf/'.$surat_pengantar->id)}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate All PDF')}}</a><br>  
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> {{language_data('Surat Pengantar Recipients List')}}</h3>
                            </div>
                            <div class="panel-body p-none">
                                <table class="table data-table table-hover table-ultra-responsive">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 20%;">{{language_data('Contract Number')}}</th>
                                        <th style="width: 20%;">{{language_data('Employee')}}</th>
                                        <th style="width: 20%;">{{language_data('Address')}}</th>
                                        <th style="width: 25%;">{{language_data('Date of Birth')}}</th>
                                        <th style="width: 10%;">{{language_data('Actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($recipients as $a)
                                        <tr>
                                            <td data-label="No">{{$a->id}}</td>
                                            <td data-label="Contract Number"><p>{{$a->letter_number}}</p></td>
                                            <td data-label="Employee"><p>{{$a->employee_info->fname}} {{$a->employee_info->lname}}</p></td>
                                            <td data-label="Address"><p>{{$a->employee_info->pre_address}}</p></td>
                                            <td data-label="Date of Birth"><p>{{$a->employee_info->birth_place}}, {{get_date_format($a->employee_info->dob)}}</p></td>
                                            <td data-label="Actions" class="">
                                            @if($surat_pengantar->status=='accepted')
                                                <a href="{{url('surat-pengantar/downloadPdf/'.$a->id)}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate PDF')}}</a><br>  
                                           @endif
                                            </td>

                                            
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>

                @if($permcheck->U==1)
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="Row"><br>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$surat_pengantar->id}}">
                                @if($surat_pengantar->status=='draft')
                                <input type="submit" name="status" value="accepted" class="btn btn-success btn-xs pull-right">
                                <input type="submit" name="status" value="rejected" class="btn btn-danger btn-xs pull-right">
                                @endif
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
            </form>
        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();


        });

    </script>
@endsection
