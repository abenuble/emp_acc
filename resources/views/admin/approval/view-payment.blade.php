@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Payment</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                        <form class="form-some-up" role="form" action="{{url('approvals/set-payment-status')}}" method="post" enctype="multipart/form-data">
                      
                        <div class="form-group">
                                        <label>{{language_data('Letter Number')}}</label>
                                        <input type="text" readonly class="form-control " required=""  value="{{$payments->letter_number}}">
                                    </div>
                                 
                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Month</label>
                                        <input type="text" readonly class="form-control monthPicker" required="" value="{{get_date_month($payments->month)}}" name="month">
                                    </div>
                                </div>

                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Company')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$payments->company_info->company}}" name="company">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Client Contract')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$payments->project_info->project}}" name="project">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Client Contract Number')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$payments->project_info->project_number}}" name="project_number">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Payroll Type')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$payments->payroll_info->payroll_name}}" name="payroll_type">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label>{{language_data('Copies')}}</label>
                                        <input type="text" readonly class="form-control "   value="{{$payments->tembusan}}">
                                    </div>
                                    <div class="form-group">
                                        <label>{{language_data('Information')}}</label>
                                        <input type="text"  class="form-control "  name="keterangan" value="{{$payments->keterangan}}">
                                    </div>
                            </div>
                            
                            @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$payments->id}}">
                                @if($payments->status=='draft')
                                    <input type="submit" name="status" value="approved" class="btn btn-success btn-xs pull-right">
                                    <input type="submit" name="status" value="rejected" class="btn btn-danger btn-xs pull-right">
                                @endif
                            @endif
                        </form>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Payment
                            @if($payments->status=='draft')
                                <a class="btn btn-warning btn-xs" href="#">{{language_data('Drafted')}}</a>
                            @elseif($payments->status=='approved')
                                <a class="btn btn-success btn-xs" href="#">{{language_data('Approved')}}</a>
                                <a class="btn btn-complete btn-xs" href="{{url('payments/print-all-payslip/'.$payments->id)}}"><i class="fa fa-download"></i> Print</a>
                            @else
                                <a class="btn btn-danger btn-xs" href="#">{{language_data('Rejected')}}</a>
                            @endif
                            </h3>
                            <br>  
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 10%;">{{language_data('Employee')}}</th>
                                        @foreach($payroll_components as $comp)
                                            <th style="width: 10%;">{{$comp->component_name->component}}</th>
                                        @endforeach
                                        <th style="width: 10%;">Total</th>
                                        @if($payments->status=='approved')
                                        <th style="width: 5%;">{{language_data('Actions')}}</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $ctr1 = 0;$ctr = 0; ?>
                                @foreach($payment_components as $a)
                                    <?php $ctr1++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr1}}</td>
                                        <td data-label="Employee">{{$employee_name[$ctr]}}</td>
                                        @foreach($payroll_components as $comp)
                                            <td data-label="{{$comp->component_name->component}}" class="text-right">{{number_format($a[str_replace(' ','_',$comp->component_name->component)],0)}}</td>
                                        @endforeach
                                        <td data-label="Total"  class="text-right">{{number_format($a['total'],0)}}</td>
                                        @if($payments->status=='approved')
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-complete btn-xs" href="{{url('payments/print-payslip/'.$a['id'])}}"><i class="fa fa-download"></i> Print</a>
                                        </td>
                                        @endif
                                    </tr>
                                    <?php $ctr++; ?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {

            /*For DataTable*/
            $('.data-table').DataTable();

        });
    </script>


@endsection
