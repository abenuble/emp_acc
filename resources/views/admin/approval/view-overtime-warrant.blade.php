@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">View Overtime Warrant</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> View Overtime Warrant</h3>
                                </div>

                            <form class="form-some-up form-block" role="form" action="{{url('approvals/set-overtime-status')}}" method="post">
                                <div class="form-group">
                                    <label for="letter_number">{{language_data('Letter Number')}}</label>
                                 
                                    <input type="text" class="form-control" required readonly name="letter_number" value="{{$overtime_warrant->letter_number}}">
                                  
                                </div>
                                <div class="form-group">
                                    <label>{{language_data('Employee Code')}}</label>
                                    <select name="employee" class="form-control selectpicker" id="employee_id">
                                    @foreach($employee as $e)
                                        <option value="{{$e->id}}" @if($e->id==$overtime_warrant->emp_id) selected @endif>{{$e->employee_code}}</option>
                                    @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Employee Code')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" id="employee_name">
                                        @foreach($employee as $e)
                                            <option value='{{$e->id}}' @if($e->id==$overtime_warrant->emp_id) selected @endif>{{$e->fname}} {{$e->lname}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Date From</label>
                                            <input type="text" class="form-control datePicker" require="" value="{{get_date_format($overtime_warrant->date_from)}}" name="date_from">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Date To</label>
                                            <input type="text" class="form-control datePicker" require="" value="{{get_date_format($overtime_warrant->date_to)}}" name="date_to">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Overtime From</label>
                                            <input type="text" class="form-control timePicker" require="" value="{{get_time_format($overtime_warrant->overtime_from)}}" name="overtime_from">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Overtime To</label>
                                            <input type="text" class="form-control timePicker" require="" value="{{get_time_format($overtime_warrant->overtime_to)}}" name="overtime_to">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Information')}}</label>
                                    <textarea class="form-control" rows="6" name="information"> {{$overtime_warrant->information}} </textarea>
                                </div>
                                <div class="form-group">
                                    <label>{{language_data('Copies')}}</label>
                                    <input type="text" class="form-control" value="{{$overtime_warrant->tembusan}}" name="tembusan"/>
                                </div>
                                <div class="form-group">
                                    <label >{{language_data('Information')}}</label>
                                    <input type="text" class="form-control" name="keterangan"  @if($overtime_warrant->status!='draft') readonly @endif value="{{$overtime_warrant->keterangan}}"  />
                                </div>
                                @if($permcheck->U==1)
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="cmd" value="{{$overtime_warrant->id}}">
                                    @if($overtime_warrant->status=='draft')
                                        <input type="submit" name="status" value="accepted" class="btn btn-success btn-xs pull-right">
                                        <input type="submit" name="status" value="rejected" class="btn btn-danger btn-xs pull-right">
                                    @endif
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });
    </script>
@endsection
