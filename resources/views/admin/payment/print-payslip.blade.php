<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - {{language_data('Print Payslip')}}</title>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
{!! Html::style("assets/libs/bootstrap/css/bootstrap.min.css") !!}
{!! Html::style("assets/libs/bootstrap-toggle/css/bootstrap-toggle.min.css") !!}
{!! Html::style("assets/libs/font-awesome/css/font-awesome.min.css") !!}
{!! Html::style("assets/libs/alertify/css/alertify.css") !!}
{!! Html::style("assets/libs/alertify/css/alertify-bootstrap-3.css") !!}
{!! Html::style("assets/css/style.css") !!}

<head>
    <style>
        .help-split {
            display: inline-block;
            width: 30%;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-30 p-t-none p-b-none">

            <div class="p-t-30"></div>
            <table width="100%">
                <tbody>
                <tr>
                    <td style="border: 0;  pull-align: center" width="100%">
                        <center><span style="font-size: 14px;"><strong>
                            @if($payslip['company_id']==1) PT. USAHA PATRA LIMA JAYA @elseif($payslip['company_id']==2) PT. USAHA YEKAPEPE @else company @endif
                        </strong></span></center>
                        <center><span style="font-size: 14px;"><strong>
                        {{$payslip['company_address']}}/ Telp : {{$payslip['company_phone']}}
                        </strong></span></center>
                        <center><span>SLIP UPAH PEKERJA</span></center>
                        <center><span><strong>{{language_data('Month')}}:</strong> {{date('M Y')}}</span></center>
                    </td>
                    <td style="border: 0;  pull-align: right" width="62%">
                      
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="m-b-20"></div>

            <table width="100%">
                <tbody>
                <tr>
                
                    <td>
                    <div class="row">
                            <div class="col-xs-12">
                                <strong>I. IDENTITAS</strong>
                            </div>
                        </div>
                        <div class="row">
                        
                        <div class="col-xs-6">
                             &nbsp; 1. {{language_data('Employee Name')}} :
                            </div>
                            <div class="col-xs-6">
                            <span>{{$payslip['fname']}} {{$payslip['lname']}}</span>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                             &nbsp; 2. {{language_data('Employee Code')}} :
                            </div>
                            <div class="col-xs-6">
                            <span>{{$payslip['employee_code']}}</span>
                            </div>

                             <div class="col-xs-6">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                             &nbsp; 3. {{language_data('Designation')}} :
                            </div>
                            <div class="col-xs-6">
                            <span>{{$payslip['designation']}}</span>
                            </div>

                             <div class="col-xs-6">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                             &nbsp; 3. {{language_data('Address')}} :
                            </div>
                            <div class="col-xs-6">
                            <span>{{$payslip['pre_address']}}</span>
                            </div>

                             <div class="col-xs-6">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <strong>II. UPAH SEBULAN</strong>
                            </div>
                        </div>
                        <div class="row">
                        
                        <div class="col-xs-8">
                              UPAH :
                            </div>
                            <div class="col-xs-2">
                            <span class="pull-left">Rp.</span>
                            <span class="pull-right">{{number_format($payslip['Salary'],0)}}</span>

                            </div>
                            <div class="col-xs-2">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>III. TUNJANGAN</strong>
                            </div>
                        </div>
                        <?php 
                        $ctr=1;
                        $tun=0;
                            foreach($payment_component_tunjangan as $tunjangan){
                        $field=str_replace(' ','_',$tunjangan->component_name->component);
                        ?>
                         <div class="row">
                        
                        <div class="col-xs-8">
                        &nbsp;{{$ctr}}. <?php echo $tunjangan->component_name->component; ?>
                            </div>
                            <div class="col-xs-2">
                            <span class="pull-left">Rp.</span>
                            <span class="pull-right">{{number_format($payslip[$field],0)}}</span>

                            </div>
                            <div class="col-xs-2">
                            </div>
                        </div>
                        </div>
                        <?php
                        $tun+=$payslip[$field];
                          $ctr++;  }
                        $totala=$payslip['Salary']+$tun;
                        ?>
                      
                    
                     
                        <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-6">
                            <strong> A. JUMLAH(II+III) :</strong> 
                            </div>
                            <div class="col-xs-3">
                            <span><hr ></span>

                            </div>
                            <div class="col-xs-2 ">
                            <span class="pull-left">Rp.</span>
                            <span class="pull-right">{{number_format($totala,0)}}</span>

                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>IV. IURAN</strong>
                            </div>
                        </div>
                        <?php 
                        $ctr=1;
                        $iur=0;
                            foreach($payment_component_iuran as $tunjangan){
                        $field=str_replace(' ','_',$tunjangan->component_name->component);
                        ?>
                         <div class="row">
                        
                        <div class="col-xs-8">
                        &nbsp;{{$ctr}}. <?php echo $tunjangan->component_name->component; ?>
                            </div>
                            <div class="col-xs-2">
                            <span class="pull-left">Rp.</span>
                            <span class="pull-right">{{number_format($payslip[$field],0)}}</span>

                            </div>
                            <div class="col-xs-2">
                            </div>
                        </div>
                        </div>
                        <?php
                           $iur+=$payslip[$field];
                          $ctr++;  }
                        ?>
                        <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-6">
                            <strong> B. JUMLAH POTONGAN(IV) :</strong> 
                            </div>
                            <div class="col-xs-3">
                            <span><hr ></span>

                            </div>
                            <div class="col-xs-2 ">
                            <span class="pull-left">Rp.</span>
                            <span class="pull-right">{{number_format($iur,0)}}</span>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7">
                                <strong>V. JUMLAH UPAH DITERIMA (A-B)</strong>
                            </div>
                            <div class="col-xs-3">
                            

                            </div>
                            <div class="col-xs-2 ">
                            <span class="pull-left">Rp.</span>
                            <span class="pull-right">{{number_format($totala-$iur,0)}}</span>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                            </div>
                            <div class="col-xs-4">
                            {{$payslip['pre_city']}}


                            </div>
                            <div class="col-xs-4 ">
                            <span>{{get_date_format(date('Y-m-d'))}}</span>

                            </div>
                        </div>
                        
                    </td>
                    
                </tr>
                </tbody>
            </table>
    </div>
</main>

{!! Html::script("assets/libs/jquery-1.10.2.min.js") !!}
{!! Html::script("assets/libs/jquery.slimscroll.min.js") !!}
{!! Html::script("assets/libs/bootstrap/js/bootstrap.min.js") !!}
{!! Html::script("assets/libs/qrcode/qrcode.js") !!}

{!! Html::script("assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js") !!}
{!! Html::script("assets/libs/alertify/js/alertify.js") !!}
{!! Html::script("assets/js/scripts.js") !!}
<script type='text/javascript'>

        var qrcode = new QRCode("qrcode");

function makeCode () {      
  
    
    qrcode.makeCode("aaa");
}

makeCode();
   
</script>
</body>
</html>