@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Pembayaran</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Pembayaran</h3>
                            <button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload">download</button> 

                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#payment"><i class="fa fa-plus"></i> Tambah Pembayaran</button>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 15%;">Nomor Proyek</th>
                                    <th style="width: 10%;">Bulan</th>
                                    <th style="width: 25%;">Perusahaan</th>
                                    <th style="width: 25%;">Tipe Gaji</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($payments as $a)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Project Number"><p>{{$a->project_info->project_number}}</p></td>
                                        <td data-label="Month"><p>{{get_date_month($a->month)}}</p></td>
                                        <td data-label="Company"><p>{{$a->company_info->company}}</p></td>
                                        <td data-label="Payroll Type"><p>{{$a->payroll_info->payroll_name}}</p></td>
                                        @if($a->status=='draft')
                                            <td data-label="status"><a class="btn btn-warning btn-xs" href="#">{{language_data('Drafted')}}</a></td>
                                        @elseif($a->status=='approved')
                                            <td data-label="status"><a class="btn btn-success btn-xs" href="#">{{language_data('Approved')}}</a></td>
                                        @else
                                            <td data-label="status"><a class="btn btn-danger btn-xs" href="#">{{language_data('Rejected')}}</a></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                        @if($a->status=='draft')
                                            @if($permcheck->U==1)
                                                <a class="btn btn-success btn-xs" href="{{url('payments/edit/'.$a->id)}}"><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>
                                            @endif
                                            @if($permcheck->D==1)
                                                <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$a->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                            @endif
                                        @elseif($a->status=='approved')
                                            <a class="btn btn-complete btn-xs" href="{{url('payments/view/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        @endif
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Tambah Pembayaran</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('payments/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">
                            <div class="form-group">
                                    <label>Nomor Surat</label>
                                    <input type="text" class="form-control" required readonly name="letter_number" value="{{$number}}/SG/SSS/{{get_roman_letters($month)}}/{{$year}}">
                                </div>
                                <div class="form-group">
                                    <label>Bulan</label>
                                    <input type="text" class="form-control monthPicker" required=""  name="month">
                                </div>

                                <div class="form-group">
                                    <label for="el3">Perusahaan</label>
                                    <select class="selectpicker form-control" data-live-search="true"  required="" name="company" id="company_id">
                                        <option value="0">Pilih Perusahaan</option>
                                        @foreach($company as $p)
                                            <option value="{{$p->id}}"> {{$p->company}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Proyek</label>
                                    <select class="selectpicker form-control" data-live-search="true" required=""  @if( $comp_id == '' ) disabled @endif name="project" id="project_id">
                                        <option value="0">Pilih Proyek</option>
                                    </select>
                                </div>
                                
                                <div class="form-group" id="project_number">
                                    <label>Nomor Proyek</label>
                                    <input type="text" readonly class="form-control" required=""  name="project_number" >
                                </div>

                                <div class="form-group">
                                    <label for="el3">Tipe Gaji</label>
                                    <select class="selectpicker form-control" data-live-search="true"  required="" name="payroll_type">
                                        <option value="0">Pilih Tipe Gaji</option>
                                        @foreach($payroll as $p)
                                            <option value="{{$p->id}}"> {{$p->payroll_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                        <label for="el3">{{language_data('Copies')}}</label>
                                        <input type="text" class="form-control"   name="tembusan">
                                    </div>
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- download -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Download</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('payments/DownloadExcelAll') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="el3">Tipe Gaji</label>
                                    <select class="selectpicker form-control" data-live-search="true"  required="" name="payroll_type">
                                        <option value="0">Pilih Tipe Gaji</option>
                                        @foreach($payroll as $p)
                                            <option value="{{$p->id}}"> {{$p->payroll_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                              
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">Download</button>
                            </div>
                        </form>
                       
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
          

            /*For Project Loading*/
            $("#company_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'comp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/payments/get-project',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#project_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For Project_number Loading*/
            $("#project_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/payments/get-project-number',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#project_number").html( data);
                    }
                });
            });
            
        });
        $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/payments/delete/" + id;
                    }
                });
            });
    </script>
@endsection
