@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Payment</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="form-group">
                                <label>{{language_data('Letter Number')}}</label>
                                <input type="text" readonly class="form-control " required=""  value="{{$payments->letter_number}}">
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                               
                                    <div class="form-group">
                                        <label>Month</label>
                                        <input type="text" readonly class="form-control monthPicker" required="" value="{{get_date_month($payments->month)}}" name="month">
                                    </div>
                                </div>

                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Company')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$payments->company_info->company}}" name="company">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Client Contract')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$payments->project_info->project}}" name="project">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Client Contract Number')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$payments->project_info->project_number}}" name="project_number">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Payroll Type')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$payments->payroll_info->payroll_name}}" name="payroll_type">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label for="el3">{{language_data('Copies')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$payments->tembusan}}" name="tembusan">
                                    </div>
                                    @if($payments->status!='draft')
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Information')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$payments->keterangan}}" name="keterangan">
                                    </div>
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Payment
                            @if($payments->status=='draft')
                                <a class="btn btn-warning btn-xs" href="#">{{language_data('Drafted')}}</a>
                            @elseif($payments->status=='approved')
                                <a class="btn btn-success btn-xs" href="#">{{language_data('Approved')}}</a>
                                <button class="btn btn-primary btn-xs " data-toggle="modal" data-target="#downloadcomponent">download</button> 

                                <a class="btn btn-complete btn-xs" href="{{url('payments/print-all-payslip/'.$payments->id)}}"><i class="fa fa-download"></i> Print</a>
                            @else

                                <a class="btn btn-danger btn-xs" href="#">{{language_data('Rejected')}}</a>
                                <button class="btn btn-primary btn-xs " data-toggle="modal" data-target="#downloadcomponent">download</button> 

                            @endif
                            </h3>
                            @if($permcheck->U==1)
                            <a><button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#upload">Upload</button></a> 
                            @endif
                            <br>  
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 10%;">{{language_data('Employee')}}</th>
                                        @foreach($payroll_components as $comp)
                                            <th style="width: 10%;">{{$comp->component_name->component}}</th>
                                        @endforeach
                                        <th style="width: 10%;">Total</th>
                                        @if($payments->status=='approved')
                                        <th style="width: 5%;">{{language_data('Actions')}}</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $ctr1 = 0;$ctr = 0; ?>
                                    @foreach($payment_components as $a)
                                        <?php $ctr1++; ?>
                                        <tr>
                                            <td data-label="No">{{$ctr1}}</td>
                                            <td data-label="Employee">{{$employee_name[$ctr]}}</td>
                                            @foreach($payroll_components as $comp)
                                                <td data-label="{{$comp->component_name->component}}" class="text-right">{{number_format($a[str_replace(' ','_',$comp->component_name->component)],0)}}</td>
                                            @endforeach
                                            <td data-label="Total"  class="text-right">{{number_format($a['total'],0)}}</td>
                                            @if($payments->status=='approved')
                                            <td data-label="Actions" class="">
                                                <a class="btn btn-complete btn-xs" href="{{url('payments/print-payslip/'.$a['id'])}}"><i class="fa fa-download"></i> Print</a>
                                            </td>
                                            @endif
                                        </tr>
                                    <?php $ctr++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal fade" id="downloadcomponent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Download</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('payments/DownloadComponent') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                            @foreach($payroll_components as $p)
                            <label><input type="checkbox" name="variable[]" value="{{$p->component_name->component}}">  {{$p->component_name->component}}</label><br>
                            @endforeach
                            <label><input type="checkbox" id="checkalll" >Check All</label><br>

                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="payment" value="{{$payments->id}}">
                                <input type="hidden" name="namapayroll" value="{{$payments->payroll_info->payroll_name}}">

                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">Download</button>
                            </div>
                        </form>
                       
                    </div>
                </div>
            </div>

            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Upload File</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('payments/importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file" accept="application/vnd.ms-excel"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
                                
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="payment" value="{{$payments->id}}">
                                <input type="hidden" name="month" value="{{$payments->month}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">Upload File</button>
                            </div>
                        </form>
                        <div class="modal-body">
                            <form class="form-some-up" role="form" action="{{ URL::to('payments/downloadExcel') }}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="payment" value="{{$payments->id}}">
                                *.xls (Excel 2003), Download template <button class="btn btn-xs btn-primary" type="submit">here</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            $('#checkalll').change(function() {
        if(this.checked) {
            $('input:checkbox[name="variable[]"]').prop('checked',true);
        }
        else{
            $('input:checkbox[name="variable[]"]').prop('checked',false);

        }
            
    });
            /*For DataTable*/
            $('.data-table').DataTable();

        });
    </script>


@endsection
