@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Assessment Forms')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Assessment Forms')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#form"><i class="fa fa-plus"></i> {{language_data('Add New Form')}}</button>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 40%;">{{language_data('Assessment')}}</th>
                                    <th style="width: 15%;">{{language_data('Date')}}</th>
                                    <th style="width: 20%;">{{language_data('Status')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($forms as $f)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Assessment"><p>{{$f->assessment}}</p></td>
                                        <td data-label="Date"><span style="display:none;">{{$f->date}}</span><p>{{get_date_format($f->date)}}</p></td>
                                        @if($f->status=='active')
                                            <td data-label="status"><p class="btn btn-complete btn-xs" data-toggle="modal" data-target=".modal_set_status_{{$f->id}}">{{language_data('Active')}}</p></td>
                                        @else
                                            <td data-label="status"><p class="btn btn-success btn-xs" data-toggle="modal" data-target=".modal_set_status_{{$f->id}}">{{language_data('Inactive')}}</p></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-success btn-xs" href="{{url('assessments/view-form/'.$f->id)}}"><i class="fa fa-edit"></i> {{language_data('View')}}</a>
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Form')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('assessments/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{language_data('Form Name')}}</label>
                                            <input type="text" class="form-control" name="assessment">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker" name="date">
                                        </div>
                                    </div>                                        
                                </div>                                

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For download pdf Info*/
            $(".download").click(function (e) {
                var id = this.id;
                var _url = $("#_url").val();
                window.location.href = _url + "/assessments/download-pdf/" + id;
            });

        });

    </script>
@endsection
