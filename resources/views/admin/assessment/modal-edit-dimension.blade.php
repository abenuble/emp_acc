<div class="modal fade modal_dimension_{{$dimension->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{language_data('Change Status')}}</h4>
            </div>
            <form class="form-some-up form-block" role="form" action="{{url('assessments/add-aspect')}}" method="post">

                <div class="modal-body">

                    <div class="row">
                        <div class="col-xs-8">
                            <div class="form-group">
                                <label>{{language_data('Dimension')}}</label>
                                <input type="text" class="form-control" require="" value="{{$dimension->dimension}}" name="dimension">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>{{language_data('Weight')}}</label>
                                <input type="text" class="form-control" require="" value="{{$dimension->dimension_weight}}" name="dimension_weight">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="Add" onclick="Javascript:addRow2()">
                    </div>    
                    <br>
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="col-sm-12">
                                <div class="panel">
                                    <div id="mydata">
                                        <table class="table table-hover table-ultra-responsive" id="myTableData2"  >
                                            <thead>
                                                <tr>
                                                    <th style="width: 80%;">{{language_data('Aspect')}}</th>
                                                    <th style="width: 10%;">{{language_data('Weight')}}</th>
                                                    <th style="width: 10%;">{{language_data('Action')}}</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($assessment_aspect as $aspect)
                                            @if($aspect->dimension==$dimension->id)
                                                <tr>
                                                    <td data-label="Aspect">{{$aspect->aspect}}</td>
                                                    <td data-label="Weight"><p>{{$aspect->weight}} %</p></td>
                                                    <td data-label="Actions" class="">
                                                        <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$aspect->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                                    </td>
                                                </tr>
                                            @endif
                                            @endforeach

                                            </tbody> 
                                        </table>

                                    </div>

                                </div>
                            </div>

                            
                            

                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" value="{{$assessment->id}}" name="cmd">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                    <button type="submit" class="btn btn-primary">{{language_data('Update')}}</button>
                </div>

            </form>
        </div>
    </div>

</div>

