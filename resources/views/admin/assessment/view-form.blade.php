@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Assessment Form')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('assessments/edit-form')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> {{language_data('View Assessment Form')}}</h3>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{language_data('Form Name')}}</label>
                                            <input type="text" class="form-control" value="{{$assessment->assessment}}" name="assessment">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker" value="{{get_date_format($assessment->date)}}" name="date">
                                        </div>
                                    </div>                                        
                                </div> 

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{{language_data('Status')}} : </label>
                                            <select class="selectpicker form-control" data-live-search="true" name="status">
                                                <option value="active" @if($assessment->status=='active') selected @endif >{{language_data('Active')}}</option>
                                                <option value="inactive" @if($assessment->status=='inactive') selected @endif >{{language_data('Inactive')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">{{language_data('Assessment Aspects')}}</h3>
                                                <a href="#" class="btn btn-success btn-xs pull-right" data-toggle="modal" data-target="#dimension"><i class="fa fa-plus"></i> {{language_data('Add Dimension')}}</a>
                                                <br>
                                            </div>
                                            <div class="panel-body p-none">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 20%;">{{language_data('Dimension')}}</th>
                                                        <th style="width: 10%;">{{language_data('Weight')}}</th>
                                                        <th style="width: 50%;">{{language_data('Aspect')}}</th>
                                                        <th style="width: 10%;">{{language_data('Weight')}}</th>
                                                        <th style="width: 10%;">{{language_data('Actions')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($assessment_dimension as $dimension)
                                                        <tr>
                                                            <td data-label="Dimension">{{$dimension->dimension}}</td>
                                                            <td data-label="Weight">
                                                                <div class="form-group">
                                                                    <input type="text" readonly class="form-control" value="{{$dimension->dimension_weight}} %">
                                                                </div>
                                                            </td>
                                                            
                                                            <td data-label="Aspect">
                                                            @foreach($assessment_aspect as $aspect)
                                                                @if($aspect->dimension==$dimension->id)
                                                                    <div class="form-group">
                                                                        <input type="text" readonly class="form-control" value="{{$aspect->aspect}}">
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                            </td>
                                                                    
                                                            <td data-label="Weight">
                                                            @foreach($assessment_aspect as $aspect)
                                                                @if($aspect->dimension==$dimension->id)
                                                                    <div class="form-group">
                                                                        <input type="text" readonly class="form-control" value="{{$aspect->weight}} %">
                                                                    </div>
                                                                @endif
                                                            @endforeach    
                                                            </td>
                                                            <td data-label="Actions" class="">
                                                                <a href="{{url('assessments/edit-dimension/'.$dimension->id)}}" class="btn btn-success btn-xs"><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                    <tr>
                                                        <td data-label="Dimension">{{language_data('Dimensions Weight Total')}} :</td>
                                                        <td data-label="Weight">
                                                            <div class="form-group">
                                                                <input type="text" readonly class="form-control" value="{{$total_weight_dimension}} %">
                                                            </div>
                                                        </td>
                                                        <td data-label="Aspect">{{language_data('Aspects Weight Total')}} :</td>
                                                        <td data-label="Weight">
                                                            <div class="form-group">
                                                                <input type="text" readonly class="form-control" value="{{$total_weight_aspect}} %">
                                                            </div>
                                                        </td>
                                                        <td data-label="Actions" class="">
                                                        </td>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                @if($permcheck->U==1)
                                <div class="row">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" value="{{$assessment->id}}" name="cmd">
                                    <button type="submit" class="btn btn-success btn-xs pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                                    <a href="{{url('assessments/download-pdf/'.$assessment->id)}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate PDF')}}</a><br>  
                                </div>
                                @endif
                                
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal fade " id="dimension" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add Dimension')}}</h4>
                        </div>
                        <form class="form-some-up form-block" role="form" action="{{url('assessments/add-dimension')}}" method="post">

                            <div class="modal-body">

                                <div class="row">
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <label>{{language_data('Dimension')}}</label>
                                            <input type="text" class="form-control" require="" value="" name="dimension">
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label>{{language_data('Weight')}}</label>
                                            <input type="text" class="form-control" require="" value="" name="dimension_weight">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="Add" onclick="Javascript:addRow()">
                                </div>    
                                <br>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="col-sm-12">
                                            <div class="panel">
                                                <div id="mydata">
                                                    <table class="table table-hover table-ultra-responsive" id="myTableData"  >
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 80%;">{{language_data('Aspect')}}</th>
                                                                <th style="width: 10%;">{{language_data('Weight')}}</th>
                                                                <th style="width: 10%;">{{language_data('Action')}}</th>
                                                            </tr>
                                                        </thead>
                                                        
                                                    </table>

                                                </div>

                                            </div>
                                        </div>

                                        
                                        

                                    </div>

                                </div>

                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" value="{{$assessment->id}}" name="cmd">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Update')}}</button> 
                            </div>

                        </form>
                    </div>
                </div>

            </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/assessments/delete-aspect/" + id;
                    }
                });
            });

        });

        function addRow() {
         
            var table = document.getElementById("myTableData");

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            row.insertCell(0).innerHTML= '<input type="text" class="form-control" value="" name="aspect[]">';
            row.insertCell(1).innerHTML= '<input type="text" class="form-control" value="" name="aspect_weight[]">';
            row.insertCell(2).innerHTML= '<input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';

        }

        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData");
            table.deleteRow(index);
        
        }

    </script>
@endsection
