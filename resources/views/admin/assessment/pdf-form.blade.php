<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - assessment Form</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        .help-split {
            display: inline-block;
            width: 30%;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-30 p-t-none p-b-none">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-body">
                    <form class="" role="form" action="{{url('assessments/edit-form')}}" method="post">
                            <div class="panel-heading">
                                <h2 align="center" class="panel-title">{{$assessment->assessment}}</h2>
                            </div>

                            <div class="row">
                              
                              
                                    <div class="form-group" align="center">
                                       {{get_date_format($assessment->date)}}
                                    </div>
                                                                   
                            </div> 
                            
                            <div class="row">
                              
                            <div class="col-xs-6">
                              
                                    <div class="form-group">
                                    <label>Periode Awal</label>
                                     <input type='text' class="form-control"/>
                                    </div>
                                    </div>
                                    <div class="col-xs-6">
                                    <label>Periode Akhir</label>
                                    <input type='text' class="form-control"/>

                                    </div>
                                                                   
                            </div> 

                         <div class="row">
                              
                              <div class="col-xs-6">
                                
                                      <div class="form-group">
                                      <label>{{language_data('Client Contract')}}</label>
                                       <input type='text' class="form-control"/>
                                      </div>
                                      </div>
                                      <div class="col-xs-6">
                                      <label>{{language_data('Company')}}</label>
                                      <input type='text' class="form-control"/>
  
                                      </div>
                                                                     
                              </div> 
                              <div class="row">
                              
                              <div class="col-xs-12">
                                
                                      <div class="form-group">
                                      <label>{{language_data('Employee Code')}}</label>
                                       <input type='text' class="form-control"/>
                                      </div>
                                      </div>
                                   
                                                                     
                              </div> 
                              <div class="row">
                              
                              <div class="col-xs-6">
                                
                                      <div class="form-group">
                                      <label>{{language_data('Designation')}}</label>
                                       <input type='text' class="form-control"/>
                                      </div>
                                      </div>
                                      <div class="col-xs-6">
                                
                                <div class="form-group">
                                <label>{{language_data('Location')}}</label>
                                 <input type='text' class="form-control"/>
                                </div>
                                </div>
                                   
                                                                     
                              </div> 
                            <hr>
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Assessment Aspects</h3>
                                        </div>
                                        <div class="panel-body p-none">
                                            <table class="table">
                                              
                                                <tr>
                                                    <th style="width: 20%;">Dimension</th>
                                                
                                                    <th style="width: 50%;">Aspect</th>
                                                    <th style="width: 5%;">Weight</th>
                                                    <th style="width: 5%;">Grade</th>
                                                    <th style="width: 5%;">Weighted Grade</th>
                                                </tr>
                                               
                                                <tbody>
                                                @foreach($assessment_aspect as $aspect)
                                                        <tr>
                                                            <td data-label="Dimension">{{$aspect->dimension_info->dimension}}</td>
                                                            <td data-label="Aspect">
                                                                <div class="form-group">
                                                                {{$aspect->aspect}}                                                                </div>
                                                            </td>
                                                                    
                                                            <td data-label="Weight">
                                                                <div class="form-group">
                                                                {{$aspect->weight}} %                                                                </div>
                                                            </td>

                                                            <td data-label="Grade">
                                                                <div class="form-group">
                                                                
                                                                    <input type="text" class="form-control qty"  value="" data-cubics="{{$aspect->weight}}" >
                                                              
                                                                </div>
                                                            </td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control cubics"  name="weighted_grade[]">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                             

                                                </tbody>
                                            </table>
                                            <hr>
                                            <div class="row">
                                            <div class="col-xs-6">
                                
                                <div class="form-group pull-right">
                                Subtotal
                                </div>
                                </div>
                              <div class="col-xs-6">
                                
                                      <div class="form-group pull-right">
                                       <input type='text' class="form-control"/>
                                      </div>
                                      </div>
                                  
                                   
                                                                     
                              </div> 
                              <div class="row">
                              <div class="col-xs-3">
                                
                                <div class="form-group pull-left">
                                Violation
                                </div>
                                </div>
                                            <div class="col-xs-3">
                                
                                <div class="form-group pull-right">
                                <input type='text' class="form-control"/>
                                </div>
                                </div>
                                <div class="col-xs-3">
                                
                                      <div class="form-group pull-right">
                                            Value
                                          </div>
                                      </div>
                              <div class="col-xs-3">
                                
                                      <div class="form-group pull-right">
                                       <input type='text' class="form-control"/>
                                      </div>
                                      </div>
                                  
                                   
                                                                     
                              </div> 
                              <div class="row">
                              <div class="col-xs-6">
                                
                                <div class="form-group pull-right">
                                Total
                                </div>
                                </div>
                               
                              <div class="col-xs-6">
                                
                                      <div class="form-group pull-right">
                                       <input type='text' class="form-control"/>
                                      </div>
                                      </div>
                                  
                                   
                                                                     
                              </div> 
                              <hr>
                              <div class="row">
                              <div class="col-xs-12">
                                
                                <div class="form-group">
                                <label>Grade Information</label>
                                <input type='text' class="form-control" style="display:table-cell; width:100%"/>

                                </div>
                                </div>
                               
                            
                                  
                                   
                                                                     
                              </div> 
                              <div class="row">
                              <div class="col-xs-12">
                                
                                <div class="form-group">
                                <label>Note</label>
                          <input type='text' class="form-control" style="display:table-cell; width:100%"/>
                                </div>
                                </div>
                              </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>


</body>
</html>