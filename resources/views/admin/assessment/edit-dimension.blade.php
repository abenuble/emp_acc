@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Update Assessment')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('assessments/post-edit-dimension')}}" method="post">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <label>{{language_data('Dimension')}}</label>
                                            <input type="text" class="form-control" require="" value="{{$dimension->dimension}}" name="dimension">
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label>{{language_data('Weight')}}</label>
                                            <input type="text" class="form-control" require="" value="{{$dimension->dimension_weight}}" name="dimension_weight">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="Add" onclick="Javascript:addRow()">
                                </div>    
                                <br>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="col-sm-12">
                                            <div class="panel">
                                                <div id="mydata">
                                                    <table class="table table-hover table-ultra-responsive" id="myTableData"  >
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 80%;">{{language_data('Aspect')}}</th>
                                                                <th style="width: 10%;">{{language_data('Weight')}}</th>
                                                                <th style="width: 10%;">{{language_data('Action')}}</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                        @foreach($assessment_aspect as $aspect)
                                                        @if($aspect->dimension==$dimension->id)
                                                            <tr>
                                                                <td data-label="Aspect">{{$aspect->aspect}}</td>
                                                                <td data-label="Weight"><p>{{$aspect->weight}} %</p></td>
                                                                <td data-label="Actions" class="">
                                                                    <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$aspect->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @endforeach

                                                        </tbody> 
                                                    </table>

                                                </div>

                                            </div>
                                        </div>

                                        
                                        

                                    </div>

                                </div>


                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" value="{{$dimension->assessment}}" name="cmd">
                            <input type="hidden" value="{{$dimension->id}}" name="dimension_id">
                            <button type="submit" class="btn btn-primary">{{language_data('Update')}}</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/assessments/delete-aspect/" + id;
                    }
                });
            });

        });

        function addRow() {
         
            var table = document.getElementById("myTableData");

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            row.insertCell(0).innerHTML= '<input type="text" class="form-control" value="" name="aspect[]">';
            row.insertCell(1).innerHTML= '<input type="text" class="form-control" value="" name="aspect_weight[]">';
            row.insertCell(2).innerHTML= '<input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';

        }

        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData");
            table.deleteRow(index);
        
        }

    </script>
@endsection