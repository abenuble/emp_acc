@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Legal Document</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Legal Document')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#new-legal-document"><i class="fa fa-plus"></i> {{language_data('New')}} {{language_data('Legal Document')}}
                            </button>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 10%;">{{language_data('ID Document')}} </th>
                                    <th style="width: 30%;">{{language_data('Document')}}</th>
                                    <th style="width: 25%;">{{language_data('About')}}</th>
                                    <th style="width: 10%;">{{language_data('Due Date')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($legaldocuments as $d)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="no" >{{$ctr}}</td>
                                        <td data-label="ID Document">{{$d->id_document}}</td>
                                        <td data-label="Document"> {{$d->document}}</td>
                                        <td data-label="About"><p>{{$d->about}}</p></td>
                                        <td data-label="Due Date"><p>{{get_date_format($d->due_date)}}</p></td>
                                        <td data-label="Actions">
                                            <a class="btn btn-success btn-xs" href="{{url('legaldocuments/view/'.$d->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            @if($permcheck->D==1)
                                            <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$d->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                            @endif
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>


                        <!-- Modal -->
                        <div class="modal fade" id="new-legal-document" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">{{language_data('Add')}} {{language_data('Legal Document')}}</h4>
                                    </div>
                                    <form class="form-some-up" id='addlegal' role="form" method="post" action="{{url('legaldocuments/add')}}" enctype="multipart/form-data">

                                        <div class="modal-body">

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>{{language_data('ID Document')}}</label>
                                                        <input type="text" class="form-control" name="id_document" required="">
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>{{language_data('Document')}}</label>
                                                        <input type="text" class="form-control" name="document" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>{{language_data('About')}}</label>
                                                        <input type="text" class="form-control" name="about" required="">
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>{{language_data('Due Date')}}</label>
                                                        <input type="text" class="form-control" name="due_date" id='duedate' required="">
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>File</label>
                                                        <input name="file" class="form-control" type="file" accept="image/*">
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="modal-footer">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                            <button type="submit" class="btn btn-primary">{{language_data('Send')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>

            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            $('#addlegal').submit(function(){
                alert('a');
                $('#duedate').datetimepicker({ lang: 'uk' });
               
                        return true;
            });
            /*For DataTable*/
            $('.data-table').DataTable();

                     $('#duedate').datetimepicker({
                            locale: 'id',
                            useCurrent: false,
                            format: 'DD MMMM YYYY'
                        });
            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/legaldocuments/delete/" + id;
                    }
                });
            });

        });

        
    </script>
@endsection
