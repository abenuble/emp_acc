@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View')}} {{language_data('Legal Document')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('legaldocuments/update')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('View')}} {{language_data('Legal Document')}}</h3>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{language_data('ID Document')}}</label>
                                            <input type="text" class="form-control" name="id_document" value="{{$d->id_document}}" readonly required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{language_data('Document')}}</label>
                                            <input type="text" class="form-control" name="document" value="{{$d->document}}" required="">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{language_data('About')}}</label>
                                            <input type="text" class="form-control" name="about" value="{{$d->about}}" required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{language_data('Due Date')}}</label>
                                            <input type="text" class="form-control" value="{{get_date_format($d->due_date)}}" id='duedate' name="due_date" required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>File</label>
                                            <a type="text" href="{{url('legaldocuments/download-file/'.$d->id)}}" readonly>{{$d->file}}</a>
                                        </div>
                                    </div>
                                </div>


                                @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" value="{{$d->id}}" name="cmd">
                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
{!! Html::script("assets/libs/moment/moment.min.js")!!}
{!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
{!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
{!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
{!! Html::script("assets/js/form-elements-page.js")!!}
{!! Html::script("assets/libs/data-table/datatables.min.js")!!}
{!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
        

                     $('#duedate').datetimepicker({
                            locale: 'id',
                            useCurrent: false,
                            format: 'DD MMMM YYYY'
                        });
          
});
        
    </script>
@endsection
