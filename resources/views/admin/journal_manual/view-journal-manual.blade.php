@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Journal Manual</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>Kode Jurnal (Auto)</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" required="" value="{{$journal_manual->number}}" readonly name="jurnal_manual_nomor"> 
                                    </div> 
                                </div>       
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Date')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control datePicker" required="" value="{{get_date_format($journal_manual->date)}}" name="date"> 
                                    </div> 
                                </div>  
                                <br>    
                                <div class="row">
                                    <div class="col-lg-2"><input type="checkbox" class="tagihann" value='1' @if($journal_manual->tagihan==1) checked @endif disabled name="tagihan" id="tagihann"/>
                                        <label>Tagihan</label>
                                    </div>
                                    <div class="col-lg-6">
                                        <select name="id_tagihan"  class="form-control selectpicker id_tagihan"  data-live-search="true"  id="id_tagihan">
                                            <option value="0" >Select Expense</option>
                                            @foreach($expense as $e)
                                                <option  value="{{$e->id}}" @if($journal_manual->tagihan_id==$e->id) selected @endif>{{$e->expense_number}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                    <input type="text" class="form-control money text-right nominal_tagihan" id="nominal_tagihan" name="nominal_tagihan" value="{{number_format($journal_manual->tagihan_nominal,0)}}" readonly />
                                    </div>
                                </div>  
                                <br>
                                <br>
                                <div class="form-group">
                                        <div class="col-md-12 table-scroll">
                                            <input type="hidden" name="jml_detail" id="jml_detail" value="0" />
                                            <table class="table table-hover table-ultra-responsive" id="default-table">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="5%"> No </th>
                                                        <th class="text-center" width="45%"> Perkiraan </th>
                                                        <th class="text-center" width="15%"> D / K </th>
                                                        <th class="text-center" width="15%"> Debet </th>
                                                        <th class="text-center" width="15%"> Kredit </th>
                                                        <th class="text-center" width="5%"> Action </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tableTbody">
                                                    <?php $no = 1; ?>
                                                    @foreach($detail as $d)
                                                    <tr> 
                                                        <td> <?= $no ?>
                                                        </td> 
                                                        <td> 
                                                        <select class="selectpicker form-control" disabled name="m_coa_id[]" data-live-search="true" style="width: 100%"> 
                                                            @foreach($coa as $c)
                                                                <option value="{{$c->id}}" @if($d->coa_id==$c->id) selected @endif>({{$c->coa_code}}) {{$c->coa}}</option>
                                                            @endforeach
                                                        </select> 
                                                        </td> 
                                                        <td> 
                                                          <div class="form-group ">
                                                              <div class="col-md-6 text-center">
                                                                  <div class="input-icon right">
                                                                      <i class="fa"></i>
                                                                      <label class="mt-radio"> d 
                                                                          <input type="radio" value="d" name="jurnal_manual_debit_kredit<?= $no ?>" @if($d->debit_credit == 'd') checked @endif id="jurnal_manual_debit_kredit1<?= $no ?>"/>
                                                                          <span></span>
                                                                      </label>
                                                                      <label class="mt-radio"> k 
                                                                          <input type="radio" value="k" name="jurnal_manual_debit_kredit<?= $no ?>" @if($d->debit_credit == 'k') checked @endif id="jurnal_manual_debit_kredit2<?= $no ?>"/>
                                                                          <span></span>
                                                                      </label> 
                                                                  </div>
                                                                <input type="hidden" name="jurnal_manual_debit_kredit[]" id="jurnal_manual_debit_kredit3<?= $no ?>" value="{{$d->debit_credit}}">
                                                              </div>
                                                          </div> 
                                                        </td> 
                                                        <td> 
                                                        <input type="text" class="form-control money text-right" name="jurnal_manualdet_nominal_debet[]" readonly value="{{number_format($d->nominal_debit,0)}}" required>
                                                        </td> 
                                                        <td> 
                                                        <input type="text" class="form-control money text-right" name="jurnal_manualdet_nominal_kredit[]" readonly value="{{number_format($d->nominal_kredit,0)}}" required>
                                                        </td> 
                                                        <td class="text-center"> 
                                                        <button class="btn btn btn-danger" type="button" title="Remove Detail" disabled >
                                                            <i class="fa fa-times"></i>
                                                        </button> 
                                                        </td> 
                                                    </tr> 
                                                    <?php $no++; ?>
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                      <th colspan="3" class="text-right">Total</th>
                                                      <th>
                                                          <div class="input-group">
                                                              <input type="text" class="form-control money text-right" id="jurnal_manual_total_debet" name="jurnal_manual_total_debet" value="{{number_format($journal_manual->total_debit,0)}}" required readonly />
                                                          </div>
                                                      </th>
                                                      <th>
                                                          <div class="input-group">
                                                              <input type="text" class="form-control money text-right" id="jurnal_manual_total_kredit" name="jurnal_manual_total_kredit" value="{{number_format($journal_manual->total_kredit,0)}}" required readonly />
                                                          </div>
                                                      </th>
                                                      <th class="text-center"> 
                                                          &nbsp;<button type="button" id="btnAddDetail" class="btn btn-success" disabled onclick="addDetail()"><i class="fa fa-plus"></i></button>&nbsp;
                                                      </th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label>{{language_data('Information')}}</label>
                                            <label>{{language_data('Information')}}</label>
                                        </div> 
                                        <div class="col-lg-8">
                                            <textarea class="form-control" rows="3" name="jurnal_manual_keterangan" readonly required>{{$journal_manual->information}}</textarea>
                                        </div> 
                                    </div>  
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <a type="button" class="btn btn-default pull-right" href="{{url('accountings/journal-manual')}}">{{language_data('Back')}}</a>
                                    </div> 
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();


        });       

    </script>

@endsection