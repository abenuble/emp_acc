@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Journal Manual</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <form class="form-some-up" role="form" method="post" id="form" action="{{url('accountings/journal-manual/add-post')}}">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>Kode Jurnal (Auto)</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" required="" value="" readonly name="jurnal_manual_nomor"> 
                                    </div> 
                                </div>       
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Date')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control datePicker" required="" value="" name="date"> 
                                    </div> 
                                </div>  
                                <br>
                                <div class="row">
                                    <div class="col-lg-2"><input type="checkbox" checked class="tagihann" value='1' name="tagihan" id="tagihann"/>
                                        <label>Tagihan</label>
                                    </div>
                                    <div class="col-lg-6">
                                        <select name="id_tagihan"  class="form-control selectpicker id_tagihan"  data-live-search="true"  id="id_tagihan">
                                            <option value="0" >Select Expense</option>
                                            @foreach($expense as $e)
                                                <option  value="{{$e->id}}">{{$e->expense_number}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control money text-right nominal_tagihan" id="nominal_tagihan" name="nominal_tagihan" value="0" readonly />
                                    </div>
                                </div>     
                                <br>
                                <br>
                                <div class="form-group">
                                    <div class="col-md-12 table-scroll">
                                        <input type="hidden" name="jml_detail" id="jml_detail" value="0" />
                                        <table class="table table-hover table-ultra-responsive" id="default-table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" width="5%"> No </th>
                                                    <th class="text-center" width="45%"> Perkiraan </th>
                                                    <th class="text-center" width="15%"> D / K </th>
                                                    <th class="text-center" width="15%"> Debet </th>
                                                    <th class="text-center" width="15%"> Kredit </th>
                                                    <th class="text-center" width="5%"> Action </th>
                                                </tr>
                                            </thead>
                                            <tbody id="tableTbody">
                                                <tr>
                                                    <td colspan="6">
                                                      No matching records found
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                  <th colspan="3" class="text-right">Total</th>
                                                  <th>
                                                      <div class="input-group">
                                                          <input type="text" class="form-control money text-right" id="jurnal_manual_total_debet" name="jurnal_manual_total_debet" value="0" required readonly />
                                                      </div>
                                                  </th>
                                                  <th>
                                                      <div class="input-group">
                                                          <input type="text" class="form-control money text-right" id="jurnal_manual_total_kredit" name="jurnal_manual_total_kredit" value="0" required readonly />
                                                      </div>
                                                  </th>
                                                  <th class="text-center"> 
                                                      &nbsp;<button type="button" id="btnAddDetail" class="btn btn-success" onclick="addDetail()"><i class="fa fa-plus"></i></button>&nbsp;
                                                  </th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Information')}}</label>
                                        <label>{{language_data('Information')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <textarea class="form-control" rows="3" name="jurnal_manual_keterangan" required></textarea>
                                    </div> 
                                </div>  
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <a type="button" class="btn btn-default pull-right" href="{{url('accountings/journal-manual')}}">{{language_data('Back')}}</a>
                                    <a type="button" onclick="submits()" class="btn btn-primary pull-right">{{language_data('Save')}}</a>
                                </div> 
                            </div>  
                        </div>
                    </div>
                </div>
                
            </form>

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

    <script>
        $(document).ready(function () {
          $('.money').number( true, 0, '.', ',' );
          $('.money').css('text-align', 'right');
          $(".money").css('width', 'auto');
            itemDetail = 0;
            /*For DataTable*/
            $('.data-table').DataTable();


        });       
        $("#id_tagihan").change(function () {
            var id = $(this).val();
            var _url = $("#_url").val();
            var dataString = 'id=' + id;
            // alert(_url);
            $.ajax
            ({
                type: "POST",
                url: _url + '/accountings/journal-manual/get-expense-detail',
                data: dataString,
                cache: false,
                success: function ( data ) {
                // alert(data);
                    $("#nominal_tagihan").val( data);
                },
                
                error:function( errorThrown){
                    console.log(errorThrown);
                }
            });
            $('.money').number( true, 0, '.', ',' );
        });
        $("#tagihann").change(function () {
            
            var check=this.checked;
            $(".id_tagihan").toggle(check);
            $(".nominal_tagihan").toggle(check);
        });
        function resetDetail() {
            itemDetail = 0;
            $("#jml_detail").val(itemDetail);
            $("#default-table tbody").empty();
            $("#default-table tbody").append(' \
                <tr> \
                    <td colspan="5"> \
                    No matching records found \
                    </td> \
                </tr> \
            ');
        }

        function addDetail() {
          if (itemDetail == 0) {
            $("#default-table tbody").empty();
          }
          itemDetail++;
          $("#jml_detail").val(itemDetail);
          $("#default-table tbody").append(' \
                <tr id="detail'+itemDetail+'"> \
                  <td> \
                    '+itemDetail+' \
                    <input type="hidden" id="jurnal_manualdet_id'+itemDetail+'" name="jurnal_manualdet_id[]" readonly /> \
                  </td> \
                  <td> \
                    <div class="input-icon right"> \
                        <select class="form-control" id="m_coa_id'+itemDetail+'" name="m_coa_id[]" aria-required="true" aria-describedby="select-error" data-live-search="true" style="width: 100%"> \
                        </select> \
                    </div> \
                  </td> \
                  <td> \
					<div class="form-group ">\
						<div class="col-md-6 text-center">\
							<div class="input-icon right">\
								<i class="fa"></i>\
								<label class="mt-radio"> d \
									<input type="radio" value="d" name="jurnal_manual_debit_kredit'+itemDetail+'" id="jurnal_manual_debit_kredit1'+itemDetail+'" onclick="sumSubTotal(),debit('+itemDetail+')"/>\
									<span></span>\
								</label>\
								<label class="mt-radio"> k \
									<input type="radio" value="k" name="jurnal_manual_debit_kredit'+itemDetail+'" id="jurnal_manual_debit_kredit2'+itemDetail+'" onclick="sumSubTotal(),debit('+itemDetail+')"/>\
									<span></span>\
								</label> \
							</div>\
							<input type="hidden" name="jurnal_manual_debit_kredit[]" id="jurnal_manual_debit_kredit3'+itemDetail+'" value="">\
						</div>\
					</div> \
                  </td> \
                  <td> \
                    <div class="input-icon right"> \
                        <input type="text" class="form-control money text-right" id="jurnal_manualdet_nominal_debet'+itemDetail+'" name="jurnal_manualdet_nominal_debet[]" value="0" onchange="sumSubTotal()" required> \
                    </div> \
                  </td> \
                  <td> \
                    <div class="input-icon right"> \
                        <input type="text" class="form-control money text-right" id="jurnal_manualdet_nominal_kredit'+itemDetail+'" name="jurnal_manualdet_nominal_kredit[]" value="0" onchange="sumSubTotal()" required> \
                    </div> \
                  </td> \
                  <td class="text-center"> \
                    <button class="btn btn btn-danger" type="button" title="Remove Detail" onclick="removeItemDetail('+itemDetail+')">\
                        <i class="fa fa-times"></i>\
                    </button> \
                  </td> \
                </tr> \
              ');
          $('.money').number( true, 0, '.', ',' );
          $('.money').css('text-align', 'right');
          $(".money").css('width', 'auto');
          $(".num2").css('width', 'auto');
          var _url = $("#_url").val();
          $.ajax
          ({
              type: "POST",
              url: _url + '/accountings/journal-manual/get-coa',
              cache: false,
              success: function ( data ) {
                $("#m_coa_id"+itemDetail).html( data).selectpicker('refresh');
              }
          });
        }

        function debit(i) {
            if($('#jurnal_manual_debit_kredit1'+i).is(':checked')) { 
                $("#jurnal_manualdet_nominal_kredit"+i).val('0');
                document.getElementById("jurnal_manual_debit_kredit3"+i).value = 'd';
                document.getElementById("jurnal_manualdet_nominal_kredit"+i).readOnly = true;
                document.getElementById("jurnal_manualdet_nominal_debet"+i).readOnly = false;
            } else if($('#jurnal_manual_debit_kredit2'+i).is(':checked')) { 
                $("#jurnal_manualdet_nominal_debit"+i).val('0');
                document.getElementById("jurnal_manual_debit_kredit3"+i).value = 'k';
                document.getElementById("jurnal_manualdet_nominal_kredit"+i).readOnly = false;
                document.getElementById("jurnal_manualdet_nominal_debet"+i).readOnly = true;
            }
            $('.money').number( true, 0, '.', ',' );
            $('.money').css('text-align', 'right');
            sumSubTotal();
        }

        function removeItemDetail(itemSeq) {
            var parent = document.getElementById("tableTbody");
            for (var i = 1; i <= itemDetail; i++) {
                if (i >= itemSeq && i < itemDetail) {
                    document.getElementById("jurnal_manualdet_id"+i).value = document.getElementById("jurnal_manualdet_id"+(i+1)).value;
                    $("#m_coa_id"+i).empty();
                    $("#m_coa_id"+i).append(document.getElementById("m_coa_id"+(i+1)).innerHTML);
                    var _url = $("#_url").val();
                    $.ajax
                    ({
                        type: "POST",
                        url: _url + '/accountings/journal-manual/get-coa',
                        cache: false,
                        success: function ( data ) {
                            $("#m_coa_id"+i).html( data).selectpicker('refresh');
                        }
                    });
                };
            };
            for (var i = 1; i <= itemDetail; i++) {
                if (i==itemDetail) {
                var child = document.getElementById("detail"+i);
                parent.removeChild(child);
                };
            };
            itemDetail--;
            sumSubTotal();
            $("#jml_detail").val(itemDetail);
            $('.num2').number( true, 0, '.', ',' );
            $('.num2').css('text-align', 'right');
            if (itemDetail == 0) {
                $("#default-table tbody").empty();
                $("#default-table tbody").append(' \
                <tr> \
                    <td colspan="8"> \
                        No matching records found \
                    </td> \
                </tr> \
                ');
            }
        }
        function sumSubTotal() {
            subTotal_debet        = 0;
            subTotal_kredit       = 0;
            itemRefrensi    = [];
            document.getElementsByName('jurnal_manual_total_debet')[0].value  = subTotal_debet;
            document.getElementsByName('jurnal_manual_total_kredit')[0].value = subTotal_kredit;
            for (var i = 1; i <= itemDetail; i++) {
                nominal_debet = parseFloat(document.getElementById('jurnal_manualdet_nominal_debet'+i).value.replace(/\,/g, ""));
                nominal_kredit = parseFloat(document.getElementById('jurnal_manualdet_nominal_kredit'+i).value.replace(/\,/g, ""));
                subTotal_debet += nominal_debet;
                subTotal_kredit += nominal_kredit;
            }
            document.getElementById('jurnal_manual_total_debet').value  = subTotal_debet;
            document.getElementById('jurnal_manual_total_kredit').value = subTotal_kredit;
            $('.money').number( true, 0, '.', ',' );
            $('.money').css('text-align', 'right');

        }

        function submits(){
            if (document.getElementById("tagihann").checked == true) {
            // alert("masuk");
                var subTotal_debet = document.getElementById('jurnal_manual_total_debet').value;
                var subTotal_kredit = document.getElementById('jurnal_manual_total_kredit').value;
                var tagihan = document.getElementById('nominal_tagihan').value;
                alert(subTotal_debet);alert(subTotal_kredit);alert(tagihan);
                if (tagihan == subTotal_debet && tagihan == subTotal_kredit){
                    $('#form').submit();
                } else {
                    bootbox.alert("Nominal debit dan nominal kredit tidak sama dengan nominal tagihan");
                    return false;
                }
            } else {
                var subTotal_debet = document.getElementById('jurnal_manual_total_debet').value;
                var subTotal_kredit = document.getElementById('jurnal_manual_total_kredit').value;
                if (subTotal_debet == subTotal_kredit){
                    $('#form').submit();
                } else {
                    bootbox.alert("Nominal debit dan nominal kredit tidak sama");
                    return false;
                }
            }
        }
    </script>

@endsection