@extends('master')

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Edit Division')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <form method="POST" action="{{ url('departments/update') }}">

                                <div class="form-group">
                                    <label>{{language_data('Division Name')}} :</label>
                                    <input type="text" class="form-control" required="" name="department" value="{{$departments->department}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Status')}} :</label>
                                    <select name="status" class="form-control selectpicker">
                                        <option value="active"   @if($departments->status=='active') selected @endif>{{language_data('active')}}</option>
                                        <option value="inactive" @if($departments->status=='inactive') selected @endif>{{language_data('inactive')}}</option>
                                    </select>
                                </div>


                                <div class="hr-line-dashed"></div>
                                @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="cmd" value="{{$departments->id}}">
                                <a class="btn btn-danger btn-sm pull-left" href="{{url('departments')}}">{{language_data('Back')}}</a>
                                <button type="submit" class="btn btn-sm pull-right btn-success"><i class="fa fa-edit"></i> {{language_data('Update')}}</button>
                                @endif
                            </form>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
@endsection
