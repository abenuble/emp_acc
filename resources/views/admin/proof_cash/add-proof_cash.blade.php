@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Cash Proof Form')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <form class="form-some-up" role="form" method="post" id="form" action="{{url('accountings/cash/add-post')}}">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Proof Number')}} (Auto)</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" required="" value="" readonly name="proof_number"> 
                                    </div> 
                                </div>       
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Date')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control datePicker" required="" value="" name="date"> 
                                    </div> 
                                </div>    
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Attachment')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" required="" value="" name="attachment"> 
                                    </div> 
                                </div>      
                                <br> 
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Proof Type')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <label class="radio-inline">
                                            <input type="radio" name="proof_type" id="cash_in" value="cash_in" class="toggle_specific">{{language_data('Cash In')}}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="proof_type" id="cash_out" value="cash_out" class="toggle_specific">{{language_data('Cash Out')}}
                                        </label>
                                    </div> 
                                </div>       
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Cash COA')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <select class="selectpicker form-control" data-live-search="true" name="cash_coa">
                                        @foreach($coa as $c)
                                            <option value="{{$c->id}}">{{$c->coa_code}} ({{$c->coa}})</option>
                                        @endforeach
                                        </select>
                                    </div> 
                                </div>  
                                <input class="btn btn-success btn-xs pull-right" type="hidden" id="add" value="Add" onclick="addRow2();">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <br>
                                <table class="table table-hover table-ultra-responsive" id="myTableData2">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;">No</th>
                                            <th style="width: 35%;">{{language_data('Description')}}</th>
                                            <th style="width: 30%;">{{language_data('Amount')}}</th>
                                            <th style="width: 30%;"><center>{{language_data('In?')}}</center></th>
                                        </tr>
                                    </thead>
                                    
                                    <?php $ctr = 0; ?>
                                    <tbody>
                                        <?php $ctr++; ?>
                                        <tr>
                                            <td data-label="No">{{$ctr}}</td>
                                            <td data-label="Description">
                                                <div class="specific_dropdown_in" style="display: none;">
                                                    <select class="selectpicker form-control" data-live-search="true" name="coa1" id="bkm_id">
                                                        <option value="0">Select Income</option>
                                                    @foreach($bkm as $m)
                                                        <option value="{{$m->id}}">{{$m->bkm_code}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                                <div class="specific_dropdown_out" style="display: none;">
                                                    <select class="selectpicker form-control" data-live-search="true" name="coa2" id="bkp_id">
                                                        <option value="0">Select Payment</option>
                                                    @foreach($bkp as $p)
                                                        <option value="{{$p->id}}">{{$p->bkp_code}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </td>
                                            <td data-label="Amount"><input type="text" name="amount[]" class="form-control amount" id="amount"></td>
                                            <td data-label="In?"><center><input type="checkbox" name="in[]" value="1"></center></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5%;"></td>
                                            <td style="width: 35%;"><center><label>Total :</label></center></td>
                                            <td style="width: 30%;"><input type="text" name="total" readonly class="form-control alltotal"></td>
                                            <td style="width: 30%;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Note')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <textarea class="form-control" rows="5" name="note"></textarea>
                                    </div> 
                                </div>  
                                <br>
                                <div class="row">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <a type="button" class="btn btn-default pull-right" href="{{url('accountings/cash')}}">{{language_data('Back')}}</a>
                                    <button type="submit" class="btn btn-primary pull-right">{{language_data('Save')}}</button>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                
            </form>

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            $('#amount').number( true, 0, '.', ',' );

            $("#amount").blur(function() {
                var total = $('#amount').val();
                console.log(total);
                 $('.alltotal').val(total).number( true, 0, '.', ',' );
            });

            /*For bkm new Loading*/
            $("#bkm_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/accountings/cash/get-bkm-detail',
                    data: {id : id},
                    cache: false,
                    success: function ( data ) {
                        $("#amount").val(data.income_nominal);
                        total()
                    }
                });
            });

            /*For bkp new Loading*/
            $("#bkp_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/accountings/cash/get-bkp-detail',
                    data: {id : id},
                    cache: false,
                    success: function ( data ) {
                        $("#amount").val(data.expenditure_nominal);
                        total()
                    }
                });
            });

        });   

        $(".toggle_specific").click(function () {
            //alert("masuk");
            toggle_specific_dropdown();
        });
        toggle_specific_dropdown();
    
        function toggle_specific_dropdown() {
            var $element = $(".toggle_specific:checked");
            if ($element.val() === "cash_in") {
                $(".specific_dropdown_in").show().find("input").addClass("form-control").addClass("validate-hidden");
                $(".specific_dropdown_out").hide().find("input").removeClass("validate-hidden");
            } else if ($element.val() === "cash_out") {
                $(".specific_dropdown_out").show().find("input").addClass("form-control").addClass("validate-hidden");
                $(".specific_dropdown_in").hide().find("input").removeClass("validate-hidden");
            }
        }    


        function total() {
            var total1 = 0;
            $('input.amount').each(function () {
                var n = parseFloat($(this).val());
                total1 += isNaN(n) ? 0 : n;
            });
            $('.alltotal').val(total1.toFixed(2)).number( true, 0, '.', ',' );
        }

        
        function addRow2() {
         
            $('#myTableData2').append('<tr><td data-label="No" class="number"></td><td data-label="Description"><select class="selectpicker form-control" data-live-search="true" name="coa">@foreach($coa as $c)<option value="{{$c->id}}">{{$c->coa}}</option>@endforeach</select></td><td data-label="Amount"><input type="number" min="0" name="amount[]" class="form-control amount" id="amount"></td><td data-label="In?"><input type="check-button" class="form-control"></td></tr>');

        }


        function deleteRow2(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData2");
            table.deleteRow(index);
        
        }
    </script>

@endsection