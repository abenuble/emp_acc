@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Cash Proof Form')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Proof Number')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" readonly value="{{$proof_cash->proof_number}}" name="proof_number"> 
                                    </div> 
                                </div>       
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Date')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control datePicker" readonly value="{{get_date_format($proof_cash->date)}}" name="date"> 
                                    </div> 
                                </div>    
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Attachment')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" readonly value="{{$proof_cash->attachment}}" name="attachment"> 
                                    </div> 
                                </div>      
                                <br> 
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Proof Type')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <label class="radio-inline">
                                            <input type="radio" name="proof_type" disabled value="cash_in" @if($proof_cash->proof_type=='cash_in') checked @endif class="toggle_specific">{{language_data('Cash In')}}
                                        </label>
                                        <label class="radio-inline"> 
                                            <input type="radio" name="proof_type" disabled value="cash_out" @if($proof_cash->proof_type=='cash_out') checked @endif class="toggle_specific">{{language_data('Cash Out')}}
                                        </label>
                                    </div> 
                                </div>       
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Cash COA')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <select class="selectpicker form-control" disabled data-live-search="true" name="cash_coa">
                                        @foreach($coa as $c)
                                            <option value="{{$c->id}}"  @if($proof_cash->cash_coa==$c->id) selected @endif>{{$c->coa}}</option>
                                        @endforeach
                                        </select>
                                    </div> 
                                </div>  
                                <input class="btn btn-success btn-xs pull-right" type="hidden" id="add" value="Add" onclick="addRow2();">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <br>
                                <table class="table table-hover table-ultra-responsive" id="myTableData2">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;">No</th>
                                            <th style="width: 35%;">{{language_data('Description')}}</th>
                                            <th style="width: 30%;">{{language_data('Amount')}}</th>
                                            <th style="width: 30%;"><center>Verifikasi</center></th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                    <?php $ctr = 0; ?>
                                    @foreach($detail as $d)
                                        <?php $ctr++; ?>
                                        <tr>
                                            <td data-label="No">{{$ctr}}</td>
                                            <td data-label="Description">
                                                <div class="specific_dropdown_in" style="display: none;">
                                                    <select class="selectpicker form-control" disabled data-live-search="true" name="coa[]" id="bkm_id">
                                                        <option value="0">Select Income</option>
                                                    @foreach($bkm as $m)
                                                        <option value="{{$m->id}}" @if($d->coa==$m->id) selected @endif>{{$m->bkm_code}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                                <div class="specific_dropdown_out" style="display: none;">
                                                    <select class="selectpicker form-control" disabled data-live-search="true" name="coa[]" id="bkp_id">
                                                        <option value="0">Select Payment</option>
                                                    @foreach($bkp as $p)
                                                        <option value="{{$p->id}}" @if($d->coa==$p->id) selected @endif>{{$p->bkp_code}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </td>
                                            <td data-label="Amount"><input type="text" name="amount[]" readonly class="form-control amount" value="{{$d->amount}}" id="amount"></td>
                                            <td data-label="In?"><center><input type="checkbox" name="in[]" readonly value="1" @if($d->in=='1') checked @endif></center></td>
                                        </tr>
                                    @endforeach
                                        <tr>
                                            <td style="width: 5%;"></td>
                                            <td style="width: 35%;"><center><label>Total :</label></center></td>
                                            <td style="width: 30%;"><input type="text" name="total" readonly value="{{number_format($proof_cash->total,0)}}" class="form-control alltotal"></td>
                                            <td style="width: 30%;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Note')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <textarea class="form-control" readonly rows="5" name="note">{{$proof_cash->note}}</textarea>
                                    </div> 
                                </div>  
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            $('#amount').number( true, 0, '.', ',' );

            $("#amount").blur(function() {
                var total = $('#amount').val();
                console.log(total);
                 $('.alltotal').val(total).number( true, 0, '.', ',' );
            });

        });       


        $(".toggle_specific").click(function () {
            //alert("masuk");
            toggle_specific_dropdown();
        });
        $(".toggle_specific").ready(function () {
            //alert("masuk");
            toggle_specific_dropdown();
        });
        toggle_specific_dropdown();
    
        function toggle_specific_dropdown() {
            var $element = $(".toggle_specific:checked");
            if ($element.val() === "cash_in") {
                $(".specific_dropdown_in").show().find("input").addClass("form-control").addClass("validate-hidden");
                $(".specific_dropdown_out").hide().find("input").removeClass("validate-hidden");
            } else if ($element.val() === "cash_out") {
                $(".specific_dropdown_out").show().find("input").addClass("form-control").addClass("validate-hidden");
                $(".specific_dropdown_in").hide().find("input").removeClass("validate-hidden");
            }
        }

        function total() {
            var total1 = 0;
            $('input.amount').each(function () {
                var n = parseFloat($(this).val());
                total1 += isNaN(n) ? 0 : n;
            });
            $('.alltotal').val(total1.toFixed(2)).number( true, 0, '.', ',' );
        }

        
        function addRow2() {
         
            $('#myTableData2').append('<tr><td data-label="No" class="number"></td><td data-label="Description"><select class="selectpicker form-control" data-live-search="true" name="coa">@foreach($coa as $c)<option value="{{$c->id}}">{{$c->coa}}</option>@endforeach</select></td><td data-label="Amount"><input type="number" min="0" name="amount[]" class="form-control amount" id="amount"></td><td data-label="In?"><input type="check-button" class="form-control"></td></tr>');

        }


        function deleteRow2(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData2");
            table.deleteRow(index);
        
        }
    </script>

@endsection