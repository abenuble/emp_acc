@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Chart of Accounts')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Chart of Accounts')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#coa"><i class="fa fa-plus"></i> {{language_data('Add New Chart of Accounts')}}</button>
                            <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload">{{language_data('Upload')}}</button></a><br>    
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 25%;">{{language_data('Company')}}</th>
                                    <th style="width: 10%;">{{language_data('Code')}}</th>
                                    <th style="width: 15%;">{{language_data('Type')}}</th>
                                    <th style="width: 20%;">{{language_data('COA Name')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 15%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($coa as $c)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Company"><p>{{$c->company_info['company']}}</p></td>
                                        <td data-label="Code"><p>{{$c->coa_code}}</p></td>
                                        <td data-label="Type"><p>{{$c->coa_type}}</p></td>
                                        <td data-label="COA Name"><p>{{$c->coa}}</p></td>
                                        @if($c->status=='active')
                                            <td data-label="status"><p class="btn btn-complete btn-xs">{{language_data('Active')}}</p></td>
                                        @else
                                            <td data-label="status"><p class="btn btn-danger btn-xs">{{language_data('Inactive')}}</p></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-complete btn-xs" href="coa/view/{{$c->id}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal add-->
            <div class="modal fade" id="coa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Chart of Accounts')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('coa/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="company">
                                        @foreach($company as $c)
                                            <option value="{{$c->id}}">{{$c->company}}</option>
                                         @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Code')}}</label>
                                    <input type="text" class="form-control" required="" name="code">
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label>{{language_data('Type')}} COA</label><br>
                                        <label class="radio-inline">
                                            <input type="radio" name="type" id="type_header" value="header" >Header
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="type" id="type_sub_header" value="sub header" >Sub Header
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="type" id="type_approximate_name" value="approximate name" >{{language_data('Approximate Name')}}
                                        </label>                                        
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Header</label>
                                    <span class="help">{{language_data('Leave blank if there is no Header')}}</span>
                                    <select class="selectpicker form-control" disabled data-live-search="true" id="header" name="header">
                                            <option value="0">{{language_data('Select')}} Header</option>
                                        @foreach($header as $h)
                                            <option value="{{$h->id}}">{{$h->coa}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Sub Header</label>
                                    <span class="help">{{language_data('Leave blank if there is no Sub Header')}}</span>
                                    <select class="selectpicker form-control" disabled data-live-search="true" id="sub_header" name="sub_header">
                                            <option value="0">{{language_data('Select')}} Sub Header</option>
                                        @foreach($sub_header as $sh)
                                            <option value="{{$sh->id}}">{{$sh->coa}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('COA Name')}}</label>
                                    <input type="text" class="form-control" required="" name="coa">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Cashflow')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="cashflow">
                                        @foreach($cashflow as $cf)
                                            <option value="{{$cf->id}}">{{$cf->cashflow}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>Tipe Akun</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="account_type">
                                        <option value="1">Aktiva Lancar</option>
                                        <option value="2">Aktiva Tetap</option>
                                        <option value="3">Hutang</option>
                                        <option value="4">Modal</option>
                                        <option value="5">Pendapatan</option>
                                        <option value="6">Beban</option>
                                        <option value="7">Pendapatan Lain-lain</option>
                                        <option value="8">Beban Lain-lain</option>
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Debit/Credit')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="debit_credit">
                                        <option value="debit">{{language_data('Debit')}}</option>
                                        <option value="credit">{{language_data('Credit')}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Information')}}</label>
                                    <textarea type="text" class="form-control" rows="8" name="information"></textarea>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('coa/importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file" accept="application/vnd.ms-excel"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
 
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('coa/downloadExcel/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/coa/delete/" + id;
                    }
                });
            });

            /*For header*/
            $("#type_header").click(function () {
                $('#header').attr("disabled","disabled").selectpicker('refresh');
                $('#sub_header').attr("disabled","disabled").selectpicker('refresh');
            });

            /*For sub header*/
            $("#type_sub_header").click(function () {
                $('#header').removeAttr("disabled").selectpicker('refresh');
                $('#sub_header').attr("disabled","disabled").selectpicker('refresh');
            });
            
            /*For approximate name*/
            $("#type_approximate_name").click(function () {
                $('#header').removeAttr("disabled").selectpicker('refresh');
                $('#sub_header').removeAttr("disabled").selectpicker('refresh');
            });


        });
        
    </script>
@endsection
