@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Chart of Accounts')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-10">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('coa/update')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('View Chart of Accounts')}}</h3>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="company">
                                        @foreach($company as $c)
                                            <option value="{{$c->id}}" @if($coas->company==$c->id) selected @endif>{{$c->company}}</option>
                                         @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Code')}}</label>
                                    <input type="text" class="form-control" required="" value="{{$coas->coa_code}}" name="code">
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label>{{language_data('Type')}}</label><br>
                                        <label class="radio-inline">
                                            <input type="radio" name="type" id="type_header" value="header" @if($coas->coa_type=='header') checked @endif >Header
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="type" id="type_sub_header" value="sub header" @if($coas->coa_type=='sub header') checked @endif >Sub Header
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="type" id="type_approximate_name" value="approximate name" @if($coas->coa_type=='approximate name') checked @endif >{{language_data('Approximate Name')}}
                                        </label>                                        
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Header</label>
                                    <span class="help">{{language_data('Leave blank if there is no Header')}}</span>
                                    <select class="selectpicker form-control" disabled data-live-search="true" id="header" name="header">
                                            <option value="0">{{language_data('Select')}} Header</option>
                                        @foreach($header as $h)
                                            <option value="{{$h->id}}" @if($coas->header==$h->id) selected @endif>{{$h->coa}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Sub Header</label>
                                    <span class="help">{{language_data('Leave blank if there is no Sub Header')}}</span>
                                    <select class="selectpicker form-control" disabled data-live-search="true" id="sub_header" name="sub_header">
                                            <option value="0">{{language_data('Select')}} Sub Header</option>
                                        @foreach($sub_header as $sh)
                                            <option value="{{$sh->id}}" @if($coas->sub_header==$sh->id) selected @endif>{{$sh->coa}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('COA Name')}}</label>
                                    <input type="text" class="form-control" required="" value="{{$coas->coa}}" name="coa">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Cashflow')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="cashflow">
                                        @foreach($cashflow as $cf)
                                            <option value="{{$cf->id}}" @if($coas->cashflow==$cf->id) selected @endif>{{$cf->cashflow}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Tipe Akun</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="account_type">
                                        <option value="1" @if($coas->account_type=='1') selected @endif>Aktiva Lancar</option>
                                        <option value="2" @if($coas->account_type=='2') selected @endif>Aktiva Tetap</option>
                                        <option value="3" @if($coas->account_type=='3') selected @endif>Hutang</option>
                                        <option value="4" @if($coas->account_type=='4') selected @endif>Modal</option>
                                        <option value="5" @if($coas->account_type=='5') selected @endif>Pendapatan</option>
                                        <option value="6" @if($coas->account_type=='6') selected @endif>Beban</option>
                                        <option value="7" @if($coas->account_type=='7') selected @endif>Pendapatan Lain-lain</option>
                                        <option value="8" @if($coas->account_type=='8') selected @endif>Beban Lain-lain</option>
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Debit/Credit')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="debit_credit">
                                        <option value="debit" @if($coas->debit_credit=='debit') selected @endif>{{language_data('Debit')}}</option>
                                        <option value="credit" @if($coas->debit_credit=='credit') selected @endif>{{language_data('Credit')}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Information')}}</label>
                                    <textarea type="text" class="form-control" rows="8" name="information">{{$coas->information}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Status')}} : </label>
                                    <select class="selectpicker form-control" data-live-search="true" name="status">
                                        <option value="active" @if($coas->status=='active') selected @endif >{{language_data('Active')}}</option>
                                        <option value="inactive" @if($coas->status=='inactive') selected @endif >{{language_data('Inactive')}}</option>
                                    </select>
                                </div>


                                @if($permcheck->U==1)
                                <div class="row">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" value="{{$coas->id}}" name="cmd">
                                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For header*/
            $("#type_header").click(function () {
                $('#header').attr("disabled","disabled").selectpicker('refresh');
                $('#sub_header').attr("disabled","disabled").selectpicker('refresh');
            });

            /*For sub header*/
            $("#type_sub_header").click(function () {
                $('#header').removeAttr("disabled").selectpicker('refresh');
                $('#sub_header').attr("disabled","disabled").selectpicker('refresh');
            });
            
            /*For approximate name*/
            $("#type_approximate_name").click(function () {
                $('#header').removeAttr("disabled").selectpicker('refresh');
                $('#sub_header').removeAttr("disabled").selectpicker('refresh');
            });

        });

    </script>
@endsection
