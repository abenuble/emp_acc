<table style="width:100%">
	<tbody>
		<tr>
			<td>No</td>
			<td>:</td>
			<td>{{letter_number}}</td>
			<td rowspan="2" style="text-align:right">Surabaya, {{date}}</td>
		</tr>
		<tr>
			<td>Perihal</td>
			<td>:</td>
			<td>Surat Pengantar<br />
			{{types}}</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>Kepada Yth.</p>

<p>&nbsp;</p>

<p><strong>{{company_name}}</strong></p>

<table width="20%!important">
	<tr>
		<td><p>{{company_address}}</p></td>
	</tr>
</table>

<p>{{company_address}}</p>

<p>&nbsp;</p>

<p>Up : {{pic}}</p>

<p>&nbsp;</p>

<p>Dengan Hormat,</p>

<p>&nbsp;</p>

<p style="text-align:justify">Sebelumnya kami ucapkan terima kasih atas kepercayaan yang diberikan pada kami PT Sentra Support Service, dengan adanya permintaan untuk <strong>{{types}}</strong> dibagian <strong>{{job}}</strong>, maka dengan ini kami datangkan tenaga kerja pengganti untuk pelaksanaan pekerjaan di area dan data tenaga kerja adalah sebagai berikut :</p>

<p>&nbsp;</p>

<table style="width:100%">
	<tbody>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td><strong>{{employee_name}}</strong></td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td>:</td>
			<td>{{address}}</td>
		</tr>
		<tr>
			<td>No. Telp</td>
			<td>:</td>
			<td>{{phone}}</td>
		</tr>
		<tr>
			<td>Keterangan</td>
			<td>:</td>
			<td>{{types}}</td>
		</tr>
	</tbody>
</table>

<p>Demikian surat pengantar ini kami sampaikan dan atas perhatiannya kami ucapkan terima kasih</p>

<table style="width:100%">
	<tbody>
		<tr>
			<td>Hormat Kami<br />
			PT. Sentra Support Service</td>
		</tr>
		<tr>
			<td>
			<p>&nbsp;</p>

			<p>&nbsp;</p>

			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td><strong>Riyadi</strong></td>
		</tr>
		<tr>
			<td>Recruitment dan Personalia Officer</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>
