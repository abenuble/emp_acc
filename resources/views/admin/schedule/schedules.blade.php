@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Schedules')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Schedules')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#schedules"><i class="fa fa-plus"></i> {{language_data('Add New Schedule')}}</button>
                            @endif
                            {{--  <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload">{{language_data('Upload')}}</button></a><br>                  --}}
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 45%;">{{language_data('Schedule')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr=0; ?>
                                @foreach($schedules as $s)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No" >{{$ctr}}</td>
                                        <td data-label="Schedule"><p>{{$s->schedule}}</p></td>
                                        @if($s->status=='active')
                                            <td data-label="status"><a class="btn btn-complete btn-xs">{{language_data('Active')}}</a></td>
                                        @elseif($s->status=='inactive')
                                            <td data-label="status"><a class="btn btn-danger btn-xs">{{language_data('Inactive')}}</a></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-complete btn-xs" href="{{url('schedules/view/'.$s->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>
                                        
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade" id="schedules" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Schedule')}}</h4>
                        </div>
                        <form class="form-some-up" name="form" role="form" method="post" action="{{url('schedules/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Schedule Name')}}</label>
                                    <input type="text" class="form-control" required="" value="schedule name" name="schedule">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Type')}}</label><br>
                                    <label class="radio-inline">
                                        <input type="radio" name="type" value="fixed" id="type" class="toggle_specific">{{language_data('Fixed')}}
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="type" value="shift" id="type" class="toggle_specific">{{language_data('Shift')}}
                                    </label>
                                </div>
                                
                                <div class="specific_fixed" style="display: none;">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th style="width: 3%;"></th>
                                            <th style="width: 27%;">{{language_data('Date')}}</th>
                                            <th style="width: 35%;">{{language_data('Starts At')}}</th>
                                            <th style="width: 35%;">{{language_data('Ends At')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>                                        
                                        <tr>
                                            <td data-label=""><input type="checkbox" value="enabled" class="disabling_input_monday"></td>
                                            <td data-label="Date"><input type="hidden" value="Monday" name="component[]"> {{language_data('Monday')}}</td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="monday form-control timePicker" value=" " name="start_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="monday form-control timePicker" value=" "  name="end_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td data-label=""><input type="checkbox" value="enabled" class="disabling_input_tuesday"></td>
                                            <td data-label="Date"><input type="hidden" value="Tuesday" name="component[]"> {{language_data('Tuesday')}}</td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="tuesday form-control timePicker"  value=" " name="start_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="tuesday form-control timePicker"  value=" " name="end_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-label=""><input type="checkbox" value="enabled" class="disabling_input_wednesday"></td>
                                            <td data-label="Date"><input type="hidden" value="Wednesday" name="component[]"> {{language_data('Wednesday')}}</td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="wednesday form-control timePicker"  value=" " name="start_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="wednesday form-control timePicker"  value=" " name="end_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-label=""><input type="checkbox" value="enabled" class="disabling_input_thursday"></td>
                                            <td data-label="Date"><input type="hidden" value="Thursday" name="component[]"> {{language_data('Thursday')}}</td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="thursday form-control timePicker" value=" "  name="start_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="thursday form-control timePicker"  value=" " name="end_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-label=""><input type="checkbox" value="enabled" class="disabling_input_friday"></td>
                                            <td data-label="Date"><input type="hidden" value="Friday" name="component[]"> {{language_data('Friday')}}</td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="friday form-control timePicker" value=" "  name="start_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="friday form-control timePicker"  value=" " name="end_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-label=""><input type="checkbox" value="enabled" class="disabling_input_saturday"></td>
                                            <td data-label="Date"><input type="hidden" value="Saturday" name="component[]"> {{language_data('Saturday')}}</td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="saturday form-control timePicker"  value=" " name="start_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="saturday form-control timePicker" value=" "  name="end_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-label=""><input type="checkbox" value="enabled" class="disabling_input_sunday"></td>
                                            <td data-label="Date"><input type="hidden" value="Sunday" name="component[]"> {{language_data('Sunday')}}</td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="sunday form-control timePicker"  value=" " name="start_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="sunday form-control timePicker"  value=" " name="end_time[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                                <div class="specific_shift" style="display: none;">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th style="width: 3%;"></th>
                                            <th style="width: 31%;">{{language_data('Shift')}}</th>
                                            <th style="width: 33%;">{{language_data('Starts At')}}</th>
                                            <th style="width: 33%;">{{language_data('Ends At')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>                                        
                                        <tr>
                                            <td data-label=""><input type="checkbox" value="enabled" class="disabling_input1"></td>
                                            <td data-label="Shift">
                                                <select class="schedule_select1 selectpicker form-control" data-live-search="true" name="component_shift[]">
                                                    <option value="0">{{language_data('Select Shift')}}</option>
                                                    <option value="Shift-1">Shift-1</option>
                                                    <option value="Shift-2">Shift-2</option>
                                                    <option value="Shift-3">Shift-3</option>
                                                </select>
                                            </td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="schedule1 form-control timePicker"  name="start_time_shift[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="schedule1 form-control timePicker"  name="end_time_shift[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td data-label=""><input type="checkbox" value="enabled" class="disabling_input2"></td>
                                            <td data-label="Shift">
                                                <select class="schedule_select2 selectpicker form-control" data-live-search="true" name="component_shift[]">
                                                    <option value="0">{{language_data('Select Shift')}}</option>
                                                    <option value="Shift-1">Shift-1</option>
                                                    <option value="Shift-2">Shift-2</option>
                                                    <option value="Shift-3">Shift-3</option>
                                                </select>
                                            </td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="schedule2 form-control timePicker"  name="start_time_shift[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="schedule2 form-control timePicker"  name="end_time_shift[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td data-label=""><input type="checkbox" value="enabled" class="disabling_input3"></td>
                                            <td data-label="Shift">
                                                <select class="schedule_select3 selectpicker form-control" data-live-search="true" name="component_shift[]">
                                                    <option value="0">{{language_data('Select Shift')}}</option>
                                                    <option value="Shift-1">Shift-1</option>
                                                    <option value="Shift-2">Shift-2</option>
                                                    <option value="Shift-3">Shift-3</option>
                                                </select>
                                            </td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="schedule3 form-control timePicker"  name="start_time_shift[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="schedule3 form-control timePicker"  name="end_time_shift[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                    
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>{{language_data('Repeat')}}</label>
                                                <div class='input-group'>
                                                    <input type='number' class="form-control" min="1" max="2" name="repeat"/>
                                                    <span class="input-group-addon">
                                                        <span>{{language_data('Day')}}</span>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>{{language_data('Holiday')}}</label><br>
                                                <select class="selectpicker form-control" data-live-search="true" name="holiday">
                                                    <option value="0">{{language_data('Select Holiday')}}</option>
                                                    <option value="sabtu">{{language_data('Saturday')}}</option>
                                                    <option value="minggu">{{language_data('Sunday')}}</option>
                                                    <option value="sabtu dan minggu">{{language_data('Saturday and Sunday')}}</option>
                                                    <option value="urutan shift 1">{{language_data('Shift Order')}} 1</option>
                                                    <option value="urutan shift 2">{{language_data('Shift Order')}} 2</option>
                                                </select>
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('companies/importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
 
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('companies/downloadExcel/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/js/phoneno-+international-format.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            $(".toggle_specific").click(function () {
                toggle_specific_dropdown();
            });
            toggle_specific_dropdown();
        
            function toggle_specific_dropdown() {
                var $element = $(".toggle_specific:checked");
                if ($element.val() === "fixed") {
                    $(".specific_fixed").show().find("input").addClass("validate-hidden");
                    $(".specific_shift").hide().find("input").removeClass("validate-hidden");
                } else if($element.val() === "shift") {
                    $(".specific_fixed").hide().find("input").removeClass("validate-hidden");
                    $(".specific_shift").show().find("input").addClass("validate-hidden");
                }
            }

            $(".disabling_input1").click(function () {
                toggle_enabling1();
            });
            toggle_enabling1();
        
            function toggle_enabling1() {
                var $element = $(".disabling_input1:checked");
                var data = '';
                if ($element.val() === "enabled") {
                    $(".schedule1").prop("disabled", false);
                    $(".schedule_select1").removeAttr('disabled').selectpicker('refresh');
                } else {
                    $(".schedule1").prop("disabled", true).val(data);
                    $(".schedule_select1").prop("disabled", true).selectpicker('refresh');
                }
            }

            $(".disabling_input2").click(function () {
                toggle_enabling2();
            });
            toggle_enabling2();
        
            function toggle_enabling2() {
                var $element = $(".disabling_input2:checked");
                var data = '';
                if ($element.val() === "enabled") {
                    $(".schedule2").prop("disabled", false);
                    $(".schedule_select2").removeAttr('disabled').selectpicker('refresh');
                } else {
                    $(".schedule2").prop("disabled", true).val(data);
                    $(".schedule_select2").prop("disabled", true).selectpicker('refresh');
                }
            }

            $(".disabling_input3").click(function () {
                toggle_enabling3();
            });
            toggle_enabling3();
        
            function toggle_enabling3() {
                var $element = $(".disabling_input3:checked");
                var data = '';
                if ($element.val() === "enabled") {
                    $(".schedule3").prop("disabled", false);
                    $(".schedule_select3").removeAttr('disabled').selectpicker('refresh');
                } else {
                    $(".schedule3").prop("disabled", true).val(data);
                    $(".schedule_select3").prop("disabled", true).selectpicker('refresh');
                }
            }

            $(".disabling_input_monday").click(function () {
                toggle_enabling_monday();
            });
            toggle_enabling_monday();
        
            function toggle_enabling_monday() {
                var $element = $(".disabling_input_monday:checked");
                var data = '';
                if ($element.val() === "enabled") {
                    $(".monday").prop("disabled", false);
                } else {
                    $(".monday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_tuesday").click(function () {
                toggle_enabling_tuesday();
            });
            toggle_enabling_tuesday();
        
            function toggle_enabling_tuesday() {
                var $element = $(".disabling_input_tuesday:checked");
                var data = '';
                if ($element.val() === "enabled") {
                    $(".tuesday").prop("disabled", false);
                } else {
                    $(".tuesday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_wednesday").click(function () {
                toggle_enabling_wednesday();
            });
            toggle_enabling_wednesday();
        
            function toggle_enabling_wednesday() {
                var $element = $(".disabling_input_wednesday:checked");
                var data = '';
                if ($element.val() === "enabled") {
                    $(".wednesday").prop("disabled", false);
                } else {
                    $(".wednesday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_thursday").click(function () {
                toggle_enabling_thursday();
            });
            toggle_enabling_thursday();
        
            function toggle_enabling_thursday() {
                var $element = $(".disabling_input_thursday:checked");
                var data = '';
                if ($element.val() === "enabled") {
                    $(".thursday").prop("disabled", false);
                } else {
                    $(".thursday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_friday").click(function () {
                toggle_enabling_friday();
            });
            toggle_enabling_friday();
        
            function toggle_enabling_friday() {
                var $element = $(".disabling_input_friday:checked");
                var data = '';
                if ($element.val() === "enabled") {
                    $(".friday").prop("disabled", false);
                } else {
                    $(".friday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_saturday").click(function () {
                toggle_enabling_saturday();
            });
            toggle_enabling_saturday();
        
            function toggle_enabling_saturday() {
                var $element = $(".disabling_input_saturday:checked");
                var data = '';
                if ($element.val() === "enabled") {
                    $(".saturday").prop("disabled", false);
                } else {
                    $(".saturday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_sunday").click(function () {
                toggle_enabling_sunday();
            });
            toggle_enabling_sunday();
        
            function toggle_enabling_sunday() {
                var $element = $(".disabling_input_sunday:checked");
                var data = '';
                if ($element.val() === "enabled") {
                    $(".sunday").prop("disabled", false);
                } else {
                    $(".sunday").prop("disabled", true).val(data);
                }
            }


        });
    </script>
@endsection
