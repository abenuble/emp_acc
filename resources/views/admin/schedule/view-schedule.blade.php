@extends('master')
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Schedule Detail')}}</h2>
        </div>
        <div class="col-md-6">
            <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

            {{--View Detail Schedule--}}
                <div class="panel">
                    <div class="panel-body">
                        <div class="col-lg-12">
                        <form class="form-some-up form-block" role="form" action="{{url('schedules/set-schedule-status')}}" method="post"><br>
                            <div class="row">
                                <div class="form-group">
                                    <label>{{language_data('Schedule Name')}}</label>
                                    <input type="text" class="form-control" readonly required="" value="{{$schedule->schedule}}" name="schedule">
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Status')}} : </label>
                                    <select class="selectpicker form-control" data-live-search="true" name="status">
                                        <option value="inactive" @if($schedule->status=='inactive') selected @endif >{{language_data('Inactive')}}</option>
                                        <option value="active" @if($schedule->status=='active') selected @endif >{{language_data('Active')}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Type')}}</label><br>
                                    <label class="radio-inline">
                                        <input type="radio" name="type" value="fixed" id="type" disabled class="toggle_specific" @if($schedule->type=='fixed') checked @endif>{{language_data('Fixed')}}
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="type" value="shift" id="type" disabled class="toggle_specific" @if($schedule->type=='shift') checked @endif>{{language_data('Shift')}}
                                    </label>
                                </div> 

                                <div class="specific_fixed" @if($schedule->type=='shift') style="display: none;" @endif >
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th style="width: 3%;"></th>
                                            <th style="width: 27%;">{{language_data('Date')}}</th>
                                            <th style="width: 35%;">{{language_data('Starts At')}}</th>
                                            <th style="width: 35%;">{{language_data('Ends At')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>                           
                                        @foreach($schedule_component as $s)             
                                        <tr>
                                            <td data-label=""><input type="checkbox" @if($s->OfficeInTime!='') checked @endif value="enabled" class="disabling_input_{{strtolower($s->component)}}"></td>
                                            <td data-label="Date"><input type="hidden" value="{{$s->component}}" class="{{$s->component}} form-control" name="component_fixed[]">{{$s->component}}</td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="{{strtolower($s->component)}} form-control timePicker" value="{{$s->OfficeInTime}}" name="start_time_fixed[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="{{strtolower($s->component)}} form-control timePicker" value="{{$s->OfficeOutTime}}" name="end_time_fixed[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        

                                        </tbody>
                                    </table>
                                </div>

                                <div class="specific_shift" @if($schedule->type=='fixed') style="display: none;" @endif>
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th style="width: 3%;">{{language_data('No')}}</th>
                                            <th style="width: 31%;">{{language_data('Shift')}}</th>
                                            <th style="width: 33%;">{{language_data('Starts At')}}</th>
                                            <th style="width: 33%;">{{language_data('Ends At')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>                               
                                        <?php $i=1;?>      
                                        @foreach($schedule_component as $s)   
                                        <tr>
                                            <td data-label=""><input type="checkbox" @if($s->OfficeInTime!='') checked @endif value="enabled" class="disabling_input{{$i}}"></td>
                                            <td data-label="Shift">
                                                <select class="schedule_select{{$i}} selectpicker form-control" data-live-search="true" name="component_shifted[]">
                                                    <option value="0">Select Shift</option>
                                                    <option value="Shift-1" @if($s->component=='Shift-1') selected @endif>Shift-1</option>
                                                    <option value="Shift-2" @if($s->component=='Shift-2') selected @endif>Shift-2</option>
                                                    <option value="Shift-3" @if($s->component=='Shift-3') selected @endif>Shift-3</option>
                                                </select>
                                            </td>
                                            <td data-label="Starts At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="schedule{{$i}} form-control timePicker" value="{{$s->OfficeInTime}}" name="start_time_shifted[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-label="Ends At">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <input type='text' class="schedule{{$i}} form-control  timePicker" value="{{$s->OfficeOutTime}}" name="end_time_shifted[]"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $i++;?>
                                        @endforeach


                                        </tbody>
                                    </table>
                                    
                                    <div class="form-group">
                                        <label>{{language_data('Repeat')}}</label>
                                        <div class='input-group'>
                                            <input type='number' min="0" max="2" class="form-control" value="{{$schedule->repeat}}" name="repeat"/>
                                            <span class="input-group-addon">
                                                <span>{{language_data('Day')}}</span>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Holiday')}}</label><br>
                                        <select class="selectpicker form-control" data-live-search="true" name="holiday">
                                            <option value="0">{{language_data('Select Holiday')}}</option>
                                            <option value="sabtu" @if($schedule->holiday=='sabtu') selected @endif>{{language_data('Saturday')}}</option>
                                            <option value="minggu" @if($schedule->holiday=='minggu') selected @endif>{{language_data('Sunday')}}</option>
                                            <option value="sabtu dan minggu" @if($schedule->holiday=='sabtu dan minggu') selected @endif>{{language_data('Saturday and Sunday')}}</option>
                                            <option value="urutan shift 1" @if($schedule->holiday=='urutan shift 1') selected @endif>{{language_data('Shift Order')}} 1</option>
                                            <option value="urutan shift 2" @if($schedule->holiday=='urutan shift 2') selected @endif>{{language_data('Shift Order')}} 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            @if($permcheck->U==1)
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="cmd" value="{{$schedule->id}}">
                            <button type="submit" class="btn btn-primary btn-sm pull-right">{{language_data('Update')}}</button>
                            @endif
                        </form>
                        </div>
                    </div>
                </div>
                   
        
            </div>
         
        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/js/phoneno-+international-format.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            $(".disabling_input1").click(function () {
                toggle_enabling1();
            });
            toggle_enabling1();
        
            function toggle_enabling1() {
                var $element = $(".disabling_input1:checked");
                var data = ' ';
                if ($element.val() === "enabled") {
                    $(".schedule1").prop("disabled", false);
                    $(".schedule_select1").removeAttr('disabled').selectpicker('refresh');
                } else {
                    $(".schedule1").prop("disabled", true).val(data);
                    $(".schedule_select1").prop("disabled", true).selectpicker('refresh');
                }
            }

            $(".disabling_input2").click(function () {
                toggle_enabling2();
            });
            toggle_enabling2();
        
            function toggle_enabling2() {
                var $element = $(".disabling_input2:checked");
                var data = ' ';
                if ($element.val() === "enabled") {
                    $(".schedule2").prop("disabled", false);
                    $(".schedule_select2").removeAttr('disabled').selectpicker('refresh');
                } else {
                    $(".schedule2").prop("disabled", true).val(data);
                    $(".schedule_select2").prop("disabled", true).selectpicker('refresh');
                }
            }

            $(".disabling_input3").click(function () {
                toggle_enabling3();
            });
            toggle_enabling3();
        
            function toggle_enabling3() {
                var $element = $(".disabling_input3:checked");
                var data = '';
                if ($element.val() === "enabled") {
                    $(".schedule3").prop("disabled", false);
                    $(".schedule_select3").removeAttr('disabled').selectpicker('refresh');
                } else {
                    $(".schedule3").prop("disabled", true).val(data);
                    $(".schedule_select3").prop("disabled", true).selectpicker('refresh');
                }
            }

            $(".disabling_input4").click(function () {
                toggle_enabling4();
            });
            toggle_enabling4();
        
            function toggle_enabling4() {
                var $element = $(".disabling_input4:checked");
                var data = ' ';
                if ($element.val() === "enabled") {
                    $(".schedule4").prop("disabled", false);
                    $(".schedule_select4").removeAttr('disabled').selectpicker('refresh');
                } else {
                    $(".schedule4").prop("disabled", true).val(data);
                    $(".schedule_select4").prop("disabled", true).selectpicker('refresh');
                }
            }

            $(".disabling_input5").click(function () {
                toggle_enabling5();
            });
            toggle_enabling5();
        
            function toggle_enabling5() {
                var $element = $(".disabling_input5:checked");
                var data = ' ';
                if ($element.val() === "enabled") {
                    $(".schedule5").prop("disabled", false);
                    $(".schedule_select5").removeAttr('disabled').selectpicker('refresh');
                } else {
                    $(".schedule5").prop("disabled", true).val(data);
                    $(".schedule_select5").prop("disabled", true).selectpicker('refresh');
                }
            }

            $(".disabling_input6").click(function () {
                toggle_enabling6();
            });
            toggle_enabling6();
        
            function toggle_enabling6() {
                var $element = $(".disabling_input6:checked");
                var data = ' ';
                if ($element.val() === "enabled") {
                    $(".schedule6").prop("disabled", false);
                    $(".schedule_select6").removeAttr('disabled').selectpicker('refresh');
                } else {
                    $(".schedule6").prop("disabled", true).val(data);
                    $(".schedule_select6").prop("disabled", true).selectpicker('refresh');
                }
            }

            $(".disabling_input7").click(function () {
                toggle_enabling7();
            });
            toggle_enabling7();
        
            function toggle_enabling7() {
                var $element = $(".disabling_input7:checked");
                var data = ' ';
                if ($element.val() === "enabled") {
                    $(".schedule7").prop("disabled", false);
                    $(".schedule_select7").removeAttr('disabled').selectpicker('refresh');
                } else {
                    $(".schedule7").prop("disabled", true).val(data);
                    $(".schedule_select7").prop("disabled", true).selectpicker('refresh');
                }
            }

            $(".disabling_input8").click(function () {
                toggle_enabling8();
            });
            toggle_enabling8();
        
            function toggle_enabling8() {
                var $element = $(".disabling_input8:checked");
                var data = ' ';
                if ($element.val() === "enabled") {
                    $(".schedule8").prop("disabled", false);
                    $(".schedule_select8").removeAttr('disabled').selectpicker('refresh');
                } else {
                    $(".schedule8").prop("disabled", true).val(data);
                    $(".schedule_select8").prop("disabled", true).selectpicker('refresh');
                }
            }

            $(".disabling_input_monday").click(function () {
                toggle_enabling_monday();
            });
            toggle_enabling_monday();
        
            function toggle_enabling_monday() {
                var $element = $(".disabling_input_monday:checked");
                var data = '';
                var monday = 'Monday';
                if ($element.val() === "enabled") {
                    $(".monday").prop("disabled", false);
                    $(".Monday").prop("disabled", false).val(monday);
                } else {
                    $(".monday").prop("disabled", true).val(data);
                    $(".Monday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_tuesday").click(function () {
                toggle_enabling_tuesday();
            });
            toggle_enabling_tuesday();
        
            function toggle_enabling_tuesday() {
                var $element = $(".disabling_input_tuesday:checked");
                var data = '';
                var tuesday = 'Tuesday';
                if ($element.val() === "enabled") {
                    $(".tuesday").prop("disabled", false);
                    $(".Tuesday").prop("disabled", false).val(tuesday);
                } else {
                    $(".tuesday").prop("disabled", true).val(data);
                    $(".Tuesday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_wednesday").click(function () {
                toggle_enabling_wednesday();
            });
            toggle_enabling_wednesday();
        
            function toggle_enabling_wednesday() {
                var $element = $(".disabling_input_wednesday:checked");
                var data = '';
                var wednesday = 'Wednesday';
                if ($element.val() === "enabled") {
                    $(".wednesday").prop("disabled", false);
                    $(".Wednesday").prop("disabled", false).val(wednesday);
                } else {
                    $(".wednesday").prop("disabled", true).val(data);
                    $(".Wednesday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_thursday").click(function () {
                toggle_enabling_thursday();
            });
            toggle_enabling_thursday();
        
            function toggle_enabling_thursday() {
                var $element = $(".disabling_input_thursday:checked");
                var data = '';
                var thursday = 'Thursday';
                if ($element.val() === "enabled") {
                    $(".thursday").prop("disabled", false);
                    $(".Thursday").prop("disabled", false).val(thursday);
                } else {
                    $(".thursday").prop("disabled", true).val(data);
                    $(".Thursday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_friday").click(function () {
                toggle_enabling_friday();
            });
            toggle_enabling_friday();
        
            function toggle_enabling_friday() {
                var $element = $(".disabling_input_friday:checked");
                var data = '';
                var friday = 'Friday';
                if ($element.val() === "enabled") {
                    $(".friday").prop("disabled", false);
                    $(".Friday").prop("disabled", false).val(friday);
                } else {
                    $(".friday").prop("disabled", true).val(data);
                    $(".Friday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_saturday").click(function () {
                toggle_enabling_saturday();
            });
            toggle_enabling_saturday();
        
            function toggle_enabling_saturday() {
                var $element = $(".disabling_input_saturday:checked");
                var data = '';
                var saturday = 'Saturday';
                if ($element.val() === "enabled") {
                    $(".saturday").prop("disabled", false);
                    $(".Saturday").prop("disabled", false).val(saturday);
                } else {
                    $(".saturday").prop("disabled", true).val(data);
                    $(".Saturday").prop("disabled", true).val(data);
                }
            }

            $(".disabling_input_sunday").click(function () {
                toggle_enabling_sunday();
            });
            toggle_enabling_sunday();
        
            function toggle_enabling_sunday() {
                var $element = $(".disabling_input_sunday:checked");
                var data = '';
                var sunday = 'Sunday';
                if ($element.val() === "enabled") {
                    $(".sunday").prop("disabled", false);
                    $(".Sunday").prop("disabled", false).val(sunday);
                } else {
                    $(".sunday").prop("disabled", true).val(data);
                    $(".Sunday").prop("disabled", true).val(data);
                }
            }



        });
    </script>
@endsection
