<div class="modal fade modal_edit_schedule_{{$s->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{language_data('Edit Schedule')}}</h4>
            </div>
            <form class="form-some-up form-block" role="form" action="{{url('schedules/edit')}}" method="post">

                <div class="modal-body">
                    <div class="form-group">
                        <label>{{language_data('Employee Code')}}</label>
                        <input type="text" class="form-control" required="" readonly value="{{$s->employee_info->employee_code}}">
                    </div>

                    <div class="form-group">
                        <label>{{language_data('Employee Name')}}</label>
                        <input type="text" class="form-control" required="" readonly value="{{$s->employee_info->fname}} {{$s->employee_info->lname}}">
                    </div>

                    <div class="form-group">
                        <label>{{language_data('Date')}}</label>
                        <input type="text" class="form-control" required="" readonly value="{{get_date_format($s->date)}}" name="date">
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>{{language_data('Starts At')}}</label>
                            <input type="text" class="form-control timePicker" value="{{$s->starts_at}}" name="starts_at" required="">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>{{language_data('Ends At')}}</label>
                            <input type="text" class="form-control timePicker" value="{{$s->ends_at}}" name="ends_at" required="">
                        </div>
                    </div>
                                
                    <div class="form-group">
                        <label>{{language_data('Information')}}</label>
                        <textarea type="text" class="form-control" rows="6"  name="information"></textarea>
                    </div>
                </div>
                
                
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="cmd" value="{{$s->id}}">
                    <input type="hidden" name="emp_id" value="{{$s->emp_id}}">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>{{language_data('Update')}}</button>
                </div>

            </form>
        </div>
    </div>

</div>
