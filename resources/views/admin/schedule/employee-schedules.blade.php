@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Employee Schedules')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Search Condition')}}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="" role="form" method="post" action="{{url('schedules/post-custom-search')}}">

                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Employee')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="employee">
                                                <option value="0">{{language_data('Select Employee')}}</option>
                                                @foreach($employee as $e)
                                                    @if($e->employee_type=="Internal")
                                                        <option value="{{$e->id}}" @if($e->id==$emp_id) selected @endif>{{$e->fname}} {{$e->lname}} ({{$e->employee_code}})</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Company')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="company" id="company">
                                                <option value="0">{{language_data('Select Company')}}</option>
                                                @foreach($company as $c)
                                                    <option value="{{$c->id}}"> {{$c->company}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Division')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="department" id="department_id">
                                                <option value="">{{language_data('Select Division')}}</option>
                                                @foreach($department as $e)
                                                <option value="{{$e->id}}" @if($dep_id==$e->id) selected @endif> {{$e->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Designation')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $des_id == '' ) disabled @endif name="designation" id="designation">
                                                <option value="0">{{language_data('Select Designation')}}</option>
                                                @if($des_id!=0)
                                                @foreach($designation as $des)
                                                    <option value="{{$des->id}}" @if($des_id==$des->id) selected @endif> {{$des->designation}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-search"></i> {{language_data('Search')}}</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Employee Schedules')}}</h3>

                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 40%;">{{language_data('Employee Name')}}</th>
                                    <th style="width: 35%;">{{language_data('Schedule')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $id=1; ?>

                                @foreach($employee as $e)
                                
                                <tr>
                                    <td data-label="No"> {{$id++}} </td>
                                    <td data-label="Employee Name">
                                        @if($employee_permission->R==1) <a href="{{url('employees/view/'.$e->id)}}">{{$e->fname}} {{$e->lname}}</a> @else {{$e->fname}} {{$e->lname}} @endif
                                    </td>
                                    <td data-label="Schedule">{{$e->schedule_info->schedule}}</td>
                                    <td data-label="Actions"><a class="btn btn-complete btn-xs" href="{{url('schedules/view-employee/'.$e->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a></td>
                                </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {

            /*For DataTable*/
            $('.data-table').DataTable();


            /*Linked Date*/

            $("#date_from").on("dp.change", function (e) {
                $('#date_to').data("DateTimePicker").minDate(e.date);
            });

            $("#date_to").on("dp.change", function (e) {
                $('#date_from').data("DateTimePicker").maxDate(e.date);
            });


            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/attendance/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });


            /*For Delete Attendance*/

            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/attendance/delete-attendance/" + id;
                    }
                });
            });

        });
    </script>


@endsection
