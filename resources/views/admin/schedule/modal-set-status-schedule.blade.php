<div class="modal fade modal_set_status_{{$s->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{language_data('Change Status')}}</h4>
            </div>
            <form class="form-some-up form-block" role="form" action="{{url('schedules/set-schedule-status')}}" method="post">

                <div class="modal-body">

                    <div class="form-group">
                        <label>{{language_data('Status')}} : </label>
                        <select class="selectpicker form-control" data-live-search="true" name="status">
                            <option value="inactive" @if($s->status=='inactive') selected @endif >{{language_data('Inactive')}}</option>
                            <option value="active" @if($s->status=='active') selected @endif >{{language_data('Active')}}</option>
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="cmd" value="{{$s->id}}">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                    <button type="submit" class="btn btn-primary">{{language_data('Update')}}</button>
                </div>

            </form>
        </div>
    </div>

</div>

