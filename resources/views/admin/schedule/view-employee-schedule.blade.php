@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Employee Schedules Detail')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Search Condition')}}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="" role="form" method="post" action="{{url('schedules/edit-schedule-type')}}">

                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Year')}}</label>
                                            <input type="text" id="month" class="form-control yearPicker" required="" name="month" value="{{$month_now}}">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Schedule')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="schedule" id="schedule">
                                                <option value="0">{{language_data('Select schedule')}}</option>
                                                @foreach($schedule_type as $c)
                                                    <option value="{{$c->id}}" @if($c->id==$employee->schedule) selected @endif> {{$c->schedule}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Employee Code')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$employee->employee_code}}">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Employee Name')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$employee->fname}} {{$employee->lname}}">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="emp_id" value="{{$employee->id}}">
                                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-edit"></i> {{language_data('Edit')}}</button>

                            </form>

                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Employee Schedules')}}</h3>

                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 10%;">{{language_data('Date')}}</th>
                                    <th style="width: 20%;">{{language_data('Day')}}</th>
                                    <th style="width: 15%;">{{language_data('Office In Time')}}</th>
                                    <th style="width: 15%;">{{language_data('Office Out Time')}}</th>
                                    <th style="width: 25%;">{{language_data('Information')}}</th>
                                    @if($permcheck->U==1)
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no = 1;?>
                                @foreach($schedule as $s)
                                
                                <tr>
                                    <td data-label="No"> <?php echo $no ?> </td>
                                    <td data-label="Date"><span style="display:none;">{{$s->date}}</span>{{get_date_format($s->date)}}</td>
                                    <td data-label="Day">{{$s->day}}</td>
                                    @if($s->starts_at!='' && $s->ends_at!='')
                                    <td data-label="Office In Time">{{$s->starts_at}}</td>
                                    <td data-label="Office Out Time">{{$s->ends_at}}</td>
                                    @else
                                    <td data-label="Office In Time">{{language_data('Holiday')}}</td>
                                    <td data-label="Office Out Time">{{language_data('Holiday')}}</td>
                                    @endif

                                    <td data-label="Information">{{$s->information}}</td>
                                    @if($permcheck->U==1)
                                        @if($days_before<strtotime($s->date))
                                            <td data-label="Actions">
                                                <a class="btn btn-success btn-xs" href="#" data-toggle="modal" data-target=".modal_edit_schedule_{{$s->id}}"><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>
                                                @include('admin.schedule.modal-edit-schedule')
                                            </td>
                                        @else
                                            <td data-label="Actions">
                                            </td>
                                        @endif
                                    @endif
                                </tr>
                                <?php $no++; ?>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
  
        $(document).ready(function () {
          
          
       
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Attendance*/

            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/attendance/delete-attendance/" + id;
                    }
                });
            });

        });
    </script>


@endsection
