<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - END CONTRACT</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        @page {
            size: 21cm 29.7cm;
            margin-left: 1cm;
            margin-right: 1cm;
            font: 14pt "Tahoma";
        }
        thead {
            display: table-header-group;
        }
        tbody {
            display: table-row-group;
            font: 14pt "Tahoma";
        }
        p {
            font: 14pt "Tahoma";
        }
        td {
            font: 14pt "Tahoma";
        }
        ul {
            font: 14pt "Tahoma";
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-30 p-t-none p-b-none">
            <?php $ctr=0; ?>
            @foreach($message as $m)
                <?php $ctr++; ?>
                {!!$m['message']!!}
                <p style="page-break-after: always;">&nbsp;</p>
            @endforeach

            <div class="m-b-20"></div>

        </div>
    </div>
</main>
</body>
</html>