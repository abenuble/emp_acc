<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - RESIGN</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        @page {
            size: 21cm 29.7cm;
            margin-left: 1cm;
            margin-right: 1cm;
            font: 14pt "Tahoma";
        }
        thead {
            display: table-header-group;
        }
        tbody {
            display: table-row-group;
            font: 14pt "Tahoma";
        }
        p {
            font: 14pt "Tahoma";
        }
        td {
            font: 14pt "Tahoma";
        }
        ul {
            font: 14pt "Tahoma";
        }
    </style>
</head>
<body class="printable-page">
<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-30 p-t-none p-b-none">
            {!!$message!!}
        </div>
    </div>
</main>
</body>
</html>