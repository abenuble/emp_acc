@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Contract')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> {{language_data('View Contract')}}
                                @if($endcontracts->status=='draft')
                                    <a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a>
                                @elseif($endcontracts->status=='accepted')
                                    <a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a>
                                @else
                                    <a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a>
                                @endif
                                </h3>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Date')}}</label>
                                        <input type="text" class="form-control datePicker" readonly required="" value="{{get_date_format($endcontracts->date)}}" name="date">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Effective Date')}}</label>
                                        <input type="text" class="form-control datePicker" readonly required="" value="{{get_date_format($endcontracts->effective_date)}}" name="effective_date">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Letter Draft')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$endcontracts->draft_letter_info->tplname}}" name="draft_letter">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Client Contract')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$endcontracts->project_info->project_number}}" name="project_number">
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Company')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$endcontracts->company_info->company}}" name="company_name">
                                    </div>
                                </div>
                            </div>
                            

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group m-none">
                                        <label for="e20">Concerns</label>
                                        <div class="container2">
                                            <div class="col-xs-12">
                                            @foreach($concerns as $c)
                                                <input type="text" class="form-control" readonly required="" value="{{$c->concerns}}" name="concerns[]">
                                            @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Copies')}}</label>
                                        <div class="container1">
                                            <div class="col-xs-12">
                                            @foreach($copies as $c)
                                                <input type="text" class="form-control" readonly required="" value="{{$c->copies}}" name="copies[]">
                                            @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>End Contract Recipients</label><br>
                                        <label class="radio-inline">
                                            <input type="radio" disabled name="share_with" id="share_with_all" value="all" class="toggle_specific" @if($endcontracts->share_with=='all') checked @endif>{{language_data('All Employee')}}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" disabled name="share_with" id="share_with_specific_radio_button" value="specific" class="toggle_specific" @if($endcontracts->share_with=='specific') checked @endif>{{language_data('Specific Employee')}}
                                        </label>
                                        <div class="specific_dropdown" style="display: none;">
                                            <input type="text" value="" name="share_with_specific" id="share_with_specific" class="w100p validate-hidden" placeholder="choose members and or teams"  />
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>                   
                            <br>  
                            @if($endcontracts->status=='accepted' && count($recipients)>0)
                            <a href="{{url('endcontracts/downloadAllPdf/'.$endcontracts->id)}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate All PDF')}}</a><br>  
                                @endif
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> {{language_data('Contract Recipients List')}}</h3>
                            </div>
                            <div class="panel-body p-none">
                                <table class="table data-table table-hover table-ultra-responsive">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 20%;">End Contract Number</th>
                                        <th style="width: 20%;">{{language_data('Employee')}}</th>
                                        <th style="width: 20%;">{{language_data('Address')}}</th>
                                        <th style="width: 25%;">{{language_data('Date of Birth')}}</th>
                                        <th style="width: 10%;">{{language_data('Actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $ctr=1; ?>
                                    @foreach($recipients as $a)
                                        <tr>
                                            <td data-label="No">{{$ctr}}</td>
                                            <td data-label="Contract Number"><p>{{$a->letter_number}}</p></td>
                                            <td data-label="Employee"><p>{{$a->employee_info->fname}}</p></td>
                                            <td data-label="Address"><p>{{$a->employee_info->per_address}}</p></td>
                                            <td data-label="Date of Birth"><p>{{$a->employee_info->birth_place}}, {{get_date_format($a->employee_info->dob)}}</p></td>
                                            <td data-label="Actions" class="">
                                            @if($endcontracts->status=='accepted')
                                                <a href="{{url('endcontracts/downloadPdf/'.$a->id)}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate PDF')}}</a><br>  
                                            @endif
                                            </td>

                                            <?php $ctr++; ?>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();


        });

    </script>
@endsection
