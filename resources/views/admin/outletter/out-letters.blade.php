@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Out Letter')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Out Letter')}}</h3>
                            @if($permcheck->C==1)
                            <a href="{{url('outgoing-letter/out-letters/templates-add')}}"><button class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add New Letter')}}</button></a><br>
                            @endif
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 25%;">{{language_data('Letter')}}</th>
                                    <th style="width: 35%;">{{language_data('Subject')}}</th>
                                    <th style="width: 10%;">{{language_data('Date')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($outletters as $et)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Letter"><p><a href="{{url('outgoing-letter/out-letters/manage/'.$et->id)}}"> {{$et->letter_number}}</a></p></td>
                                        <td data-label="Subject"><p>{{$et->subject}}</p></td>
                                        <td data-label="Date"><span style="display:none;">{{$et->date}}</span><p>{{get_date_format($et->date)}}</p></td>
                                        @if($et->status=='approved')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Approved')}}</p></td>
                                        @elseif($et->status=='draft')
                                            <td data-label="Status"><p class="btn btn-warning btn-xs">{{language_data('Drafted')}}</p></td>
                                        @else
                                            <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Rejected')}}</p></td>
                                        @endif
                                        <td data-label="Actions">
                                        @if($et->status!='draft')
                                        <a class="btn btn-info btn-xs" href="{{url('outgoing-letter/out-letters/manage/'.$et->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        @if($et->status=='approved')
                                            <a href="{{url('outgoing-letter/out-letters/downloadPdf/'.$et->id)}}" class="btn btn-success btn-xs"><i class="fa fa-file-pdf-o"></i> Print</a><br>   
                                        @endif
                                        @else
                                        <a class="btn btn-success btn-xs" href="{{url('outgoing-letter/out-letters/manage/'.$et->id)}}"><i class="fa fa-edit"></i> {{language_data('Manage')}}</a>

                                        @endif
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    <script>
        $(document).ready(function () {
            $('.data-table').DataTable();
        });
    </script>
@endsection
