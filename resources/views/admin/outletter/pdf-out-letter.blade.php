<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - OUT LETTER</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        .help-split {
            display: inline-block;
            width: 30%;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-25 p-t-none p-b-none">

            {!!$message!!}
            <br><br>
                        <div class="row">

                            <div class="col-xs-4">

                            </div>
                            <div class="col-xs-4">
                       
                            <div id="qrcode" ></div>
                            </div>
                            <div class="col-xs-4">
                            </div>
                            </div>
            <div class="m-b-20"></div>
       
        </div>
    </div>
</main>
{!! Html::script("assets/libs/qrcode/qrcode.js") !!}
<script type='text/javascript'>

        var qrcode = new QRCode("qrcode",{
    text: "<?php echo $letter_number; ?>",
    width: 150,
    height: 150,
    colorDark : "#000000",
    colorLight : "#ffffff"
});

function makeCode () {      
  
    
    qrcode.makeCode( "<?php echo $letter_number; ?>");
}

makeCode();
   
</script>

</body>
</html>