@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Edit Company')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel" role="document">
                        <div class="panel-body">
                            <form method="POST" name="form" role="form" action="{{ url('companies/update') }}">

                                <div class="form-group">
                                    <label>{{language_data('Company Code Area')}}</label>
                                    <input type="text" class="form-control" required="" name="company_code" value="{{$company->company_code}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Company Name')}}</label>
                                    <input type="text" class="form-control" required="" name="company" value="{{$company->company}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Address')}}</label>
                                    <input type="text" class="form-control" required="" name="address" value="{{$company->address}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Phone Number')}}</label>
                                    <span class="help">e.g. "xxx-xxx-xxxx, xxxx-xxx-xxxx, xxxx-xxxx-xxxx, xxxx-xxxx-xxx"</span>
                                    <input type="text" class="form-control" required="" name="phone_number" value="{{$company->phone_number}}">
                                </div>

                                <div class="form-group">
                                    <label>PIC</label>
                                    <input type="text" class="form-control" required="" placeholder="pic name" name="pic" value="{{$company->pic}}">
                                </div>
                                
                                <div class="form-group">
                                    <label>PIC Jabatan</label>
                                    <input type="text" class="form-control" required="" placeholder="Jabatan" name="pic_jabatan" value="{{$company->pic_jabatan}}">
                                </div>

                                <div class="form-group">
                                    <label>PIC {{language_data('Phone Number')}}</label>
                                    <span class="help">e.g. "xxx-xxx-xxxx, xxxx-xxx-xxxx, xxxx-xxxx-xxxx, xxxx-xxxx-xxx"</span>
                                    <input type="text" class="form-control" required="" name="pic_phone" value="{{$company->pic_phone}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Status')}} :</label>
                                    <select name="status" class="form-control selectpicker">
                                        <option value="active"   @if($company->status=='active') selected @endif>{{language_data('active')}}</option>
                                        <option value="inactive" @if($company->status=='inactive') selected @endif>{{language_data('inactive')}}</option>
                                    </select>
                                </div>

                                <div class="hr-line-dashed"></div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="cmd" value="{{$company->id}}">
                                <a class="btn btn-danger btn-sm pull-left" href="{{url('companies')}}">{{language_data('Back')}}</a>
                                <button type="submit" class="btn btn-sm pull-right btn-success"><i class="fa fa-edit"></i> {{language_data('Update')}}</button>
                            </form>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
      $(document).ready(function () {
      });

    </script>
@endsection
