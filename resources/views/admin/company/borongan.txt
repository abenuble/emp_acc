
            <h1 style="text-align:center"><strong><u>PERJANJIAN KERJA WAKTU TERTENTU</u></strong></h1>

            <p style="text-align:center">Nomor : {{letter_number}}</p>
            
            <p>Pada hari <span >{{day}}</span> ini tanggal <span >{{date}}</span> telah dilakukan kesepakatan dan penandatanganan Kontrak Kerja antara :</p>
            
            <table width="100%">
                <tr>
                    <td width="20%">Nama</td>
                    <td width="1%">:</td>
                    <td width="79%"><strong>Riyadi</strong></td>
                </tr>
                <tr>
                    <td width="20%">Jabatan</td>
                    <td width="1%">:</td>
                    <td width="79%">Recruitment &amp; Personalia Officer</td>
                </tr>
            </table>

            <p>Dalam hal ini bertindak atas nama PT. Sentra Support Service yang berkedudukan di Jl. Jemur Andayani 19 No. 2 Surabaya dan selanjutnya disebut <strong>Pihak Pertama.</strong></p>
            
            <table width="100%">
                <tr>
                    <td width="20%">Nama Lengkap</td>
                    <td width="1%">:</td>
                    <td width="79%"><span >{{employee_name}}</span></td>
                </tr>
                <tr>
                    <td width="20%">Tempat/Tgl. Lahir</td>
                    <td width="1%">:</td>
                    <td width="79%"><span >{{date_of_birth}}</span></td>
                </tr>
                <tr>
                    <td width="20%">Jenis Kelamin</td>
                    <td width="1%">:</td>
                    <td width="79%"><span >{{gender}}</span></td>
                </tr>
                <tr>
                    <td width="20%">Alamat</td>
                    <td width="1%">:</td>
                    <td width="79%"><span >{{address}}</span></td>
                </tr>
                <tr>
                    <td width="20%">Pendidikan</td>
                    <td width="1%">:</td>
                    <td width="79%"><span >{{last_education}}</span></td>
                </tr>
                <tr>
                    <td width="20%">No. KTP</td>
                    <td width="1%">:</td>
                    <td width="79%"><span >{{ktp}}</span></td>
                </tr>
                <tr>
                    <td width="20%">No. Telp. / Hp.</td>
                    <td width="1%">:</td>
                    <td width="79%"><span >{{phone}}</span></td>
                </tr>
            </table>
            
            <p>Dalam hal ini bertindak untuk dan atas nama diri pribadi dan selanjutnya disebut <strong>Pihak Kedua.</strong></p>
            
            <h3 style="text-align:center"><strong>PASAL 1</strong></h3>
            <h3 style="text-align:center"><strong>Penempatan Dan Lokasi Kerja</strong></h3>  
            <p>Pihak Pertama bersedia dan setuju untuk menerima Pihak Kedua sebagai karyawan dengan status karyawan kontrak untuk waktu tertentu, dengan penempatan kerja di {{company_name}}, sebagai tenaga Kerja borongan untuk pekerjaan {{job}}</p>
            
            <h3 style="text-align:center"><strong>PASAL 2</strong></h3>
            <h3 style="text-align:center"><strong>Jangka Waktu Perjanjian</strong></h3>     
            <p>Perjanjian kerja untuk waktu tertentu ini berlaku sejak tanggal : {{effective_date}} dan akan berakhir dengan sendirinya pada tanggal : {{end_date}}</p>
            <p>Bilamana perjanjian waktu tertentu ini telah berakhir sesuai dengan jangka waktu yang telah ditetapkan, maka hubungan hukum akan putus dengan sendirinya dan Pihak Pertama tidak dibebani kewajiban untuk memberikan kompensasi atau kebijaksanaan dalam bentuk apapun kepada Pihak Kedua dan atau apabila terjadi penurunan produksi di area kerja maka Pihak Kedua bersedia untuk dilakukan pengurangan karyawan.</p>
            
            <h3 style="text-align:center"><strong>PASAL 3</strong></h3>
            <h3 style="text-align:center"><strong>Perpanjangan Kontrak</strong></h3>    
            <p>Apabila setelah perpanjangan kontrak atau setelah penandatanganan kontrak kerja, baik sehari, seminggu, atau sebulan masa kerja dst, sebelum kontrak kerja berakhir pihak kedua tidak melakukan pekerjaan dengan baik, tidak disiplin atau tidak mematuhi peraturan perusahaan yang telah ditetapkan maka pihak pertama akan mengadakan pemutusan hubungan kerja dengan pihak kedua. Dan pihak pertama terbebas dari segala macam tuntutan oleh pihak kedua.</p>
            
            <h3 style="text-align:center"><strong>PASAL 4</strong></h3>
            <h3 style="text-align:center"><strong>Upah Dan Tunjangan Atau Fasilitas</strong></h3>    
            <p>{{payroll_types}}</p>
            <p>Asuransi:</p>
            <p>
                <table border="1">
                    <tr>
                        <td rowspan="2">No</td>
                        <td rowspan="2">Fasilitas</td>
                        <td colspan="2">Premi</td>
                    </tr>
                    <tr>
                        <td>Perusahaan</td>
                        <td>Tenaga Kerja</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>JKK (Jaminan Kecelakaan Kerja)</td>
                        <td>0,24%</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>JKM (Jaminan Kematian)</td>
                        <td>0,30%</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>JHT (Jaminan Hari Tua)</td>
                        <td>3,75%</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>JP (Jaminan Pensiun)</td>
                        <td>2%</td>
                        <td>1%</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Jaminan Kesehatan</td>
                        <td>4%</td>
                        <td>1%</td>
                    </tr>
                </table>  
            </p>
            <p><strong>THR</strong> (Tunjangan Hari Raya) diberikan bila masa kerja lebih dari 1 (satu) bulan dengan perhitungan dengan perhitungan pro rata. Pihak Kedua yang berhenti bekerja kurang dari 30 (tiga puluh) hari menjelang hari besar keagamaan (Hari Raya) maka tidak berhak atas Tunjangan Hari Raya (THR).</p>  

            <h3 style="text-align:center"><strong>PASAL 5</strong></h3>
            <h3 style="text-align:center"><strong>Jam Kerja</strong></h3>    
            <p>Pihak Kedua bersedia bekerja untuk 6 (enam) hari kerja dalam seminggu dan dengan pengaturan jam kerja disesuaikan dengan situasi dan kebutuhan Perusahaan dilokasi kerja.</p>
            <p>Pihak Kedua menyatakan bersedia untuk kerja lembur dan kerja Shift bilamana situasi dan kebutuhan dimana Pihak Kedua ditempatkan membutuhkan untuk itu.</p>
            
            <h3 style="text-align:center"><strong>PASAL 6</strong></h3>
            <h3 style="text-align:center"><strong>Tata Tertib Dan Disiplin Kerja</strong></h3>    
            <p>Pihak Kedua wajib melaksanakan pekerjaan yang diberikan oleh Pihak Pertama sesuai dengan standar kinerja perusahaan dan taat pada peraturan perusahaan yang meliputi :</p>
            
            <ol>
                <li>Peraturan Kerja PT Sentra Support Service.</li>
                <li>Peraturan Perusahaan Pihak Pemberi Jasa </li>
            </ol>
            
            <p>Pihak Kedua wajib menjaga rahasia Pihak Pertama dan Pihak Pemberi Jasa dalam menjalankan pekerjaan berdasarkan perjanjian ini, baik selama maupun sesudah berakhirnya perjanjian.</p>

            <h3 style="text-align:center"><strong>PASAL 7</strong></h3>
            
            <h3 style="text-align:center"><strong>Sanksi</strong></h3>    
            <p>Pelanggaran terhadap tata tertib dan disiplin kerja akan mendapatkan sanksi sebagaimana yang telah diatur dalam Peraturan Perusahaan Pihak Pertama dan peraturan perusahaan dimana Pihak Kedua ditempatkan ({{company_name}}).</p>
            
            <ol>
                <li>Pihak Pertama dapat memberikan Surat Peringatan Terakhir kepada Pihak Kedua karena kesalahan Pihak Kedua melakukan perbuatan-perbuatan sbb :
                    <ol>
                        <li>Tetap menolak untuk mentaati perintah atau penugasan yang layak diberikan kepadanya oleh Pihak Pertama.</li>
                        <li>Dengan sengaja atau karena lalai yang mengakibatkan diri Pihak Kedua tidak dapat menjalankan pekerjaannya</li>
                        <li>Apabila dikemudian hari Pihak Kedua tidak melaksanakan pekerjaannya sesuai dengan ketentuan yang sudah diperjanjikan</li>
                        <li>Melanggar ketentuan yang telah ditetapkan oleh Perusahaan, dan Pihak Pertama telah diberikan peringatan lisan, surat peringatan pertama atau kedua yang masih berlaku</li>
                    </ol>
                </li>
                <li>Setelah surat peringatan terakhir Pihak Kedua tetap melakukan perbuatan sebagaimana dimaksud dalam ayat 1, maka Pihak Pertama dapat mengakhiri Perjanjian Kontrak Kerja.</li>
            </ol>
            
            <h3 style="text-align:center"><strong>PASAL 8</strong></h3>
            
            <h3 style="text-align:center"><strong>Pengakhiran Hubungan Kerja</strong></h3>    
            
            <p>Sewaktu-waktu tanpa harus menunggu berakhirnya masa kontrak kerja, Pihak Kedua dapat dikenakan sanksi Pemutusan Hubungan Kerja bilamana :</p>

            <ol>
                <li>Melakukan pencurian, penggelapan atau segala perbuatan yang melanggar hukum</li>
                <li>Melakukan penganiayaandan mengancam terhadap pimpinan dan atau keluarganya serta sesama pekerja.</li>
                <li>Berkelahi dengan sesama pekerja, melakukan pemukulan terlebih dahulu terhadap yang lainnya.</li>
                <li>Merusak dengan sengaja atau dengan kecerobohannya, barang dan atau benda lainnya yang mengakibatkan kerugian materi dan non materi bagi Perusahaan atau pihak lain.</li>
                <li>Mabok, minum-minuman keras yang memabukkan, madat, memakai obat bius atau menyalahgunakan obat-obatan perangsang lainnya yang dilarang oleh peraturan perundang-undangan, ditempat kerja, dan ditempat-tempat yang ditetapkan perusahaan</li>
                <li>Memberikan keterangan palsu, menghasut sesama pekerja atau pihak lainnya untuk merencanakan dan atau melaksanakan perbuatan-perbuatan yang dapat menggangu ketentraman, keamanan dan kelangsungan dalam menjalankan tugas pekerjaan.</li>
                <li>Membongkar atau membocorkan rahasia perusahaan atau mencemarkan nama baik perusahaan</li>
                <li>Menghina, mecemarkan nama baik atau kewibawaan pimpinan maupun keluarganya serta sesaman pekerja dan Perusahaan.</li>
                <li>Menolak atau dengan sengaja melalaikan tugas atau perintah yang diberikan oleh Perusahaan dimana Pihak Kedua ditempatkan.</li>
                <li>Berulangkali terlambat masuk kerja atau pulang sebelum waktunya (lebih awal) meskipun sudah diberikan surat peringatan lisan maupun tertulis.</li>
                <li>Tidak masuk kerja selama 3 (tiga) hari berturut-turut tanpa keterangan tertulis atau alasan yang dapat dipertanggungjawabkan.</li>
                <li>Menyalahgunakan jabatan atau fasilitas yang tersedia dimana Pihak Kedua ditempatkan untuk kepentingan Pribadi.</li>
                <li>Pelanggaran-pelanggaran berat lainnya yang diatur dalam peraturan Perusahaan dimana Pihak Kedua ditempatkan.</li>
                <li>Bila mana Pihak Kedua menurut penilaian perusahaan kurang atau tidak memenuhi kualifikasi yang telah di tetapkan, maka Pihak Kedua di akhiri hubungan kerjanya dengan Pihak Pertama.</li>
                <li>Terjadinya pemutusan hubungan kontrak antara Pihak Penerima Jasa dan Pihak Pemberi Jasa.</li>
            </ol>

            <h3 style="text-align:center"><strong>PASAL 9</strong></h3>
            <h3 style="text-align:center"><strong>Ketentuan PHK (Pemutusan Hubungan Kerja)</strong></h3>  
            <p>Dalam hal ini Pihak Kedua melakukan pelanggaran sebagaimana disebut dalam pasal 8 perjanjian ini, Pihak Pertama akan melakukan tindakan Pemutusan Hubungan Kerja atas diri Pihak Kedua dan Pihak Pertama dibebaskan untuk memberikan kompensasi atau kebijakan dalam bentuk apapun sebagai akibat Pemutusan Hubungan Kerja.</p>

            <h3 style="text-align:center"><strong>PASAL 10</strong></h3>
            <h3 style="text-align:center"><strong>Mutasi</strong></h3>  
            <p>Pihak Kedua bersedia dimutasi kelokasi kerja manapun sesuai kebutuhan Pihak Pertaman, dan apabila Pihak Kedua tidak bersedia maka dianggap mengundurkan diri.</p>

            <h3 style="text-align:center"><strong>PASAL 11</strong></h3>
            <h3 style="text-align:center"><strong>Lain-Lain</strong></h3>  

            <ol>
                <li>Apabila terjadi perubahan upah sewaktu-waktu sesuai dengan persetujuan dari pihak pember jasa (PT Essentra) maka upah akan diubah dan disesuaikan dengan upah terbaru yang sudah disetujui dari pihak pemberi jasa (PT Essentra), serta diterbitkan addendum untuk merubah penyesuaian upah terbaru.</li>
                <li>Bilamana dikemudian hari timbul perselisihan sebagai akibat dari perjanjian ini, maka Pihak Pertama dan Pihak Kedua sepakat untuk menyelesaikannya secara musyawarah kekeluargaan, tanpa mengesampingkan kemungkinan penyelesaian melalui prosedur dan ketentuan hukum yang berlaku..</li>
                <li>Hal-hal yang tidak diatur dalam perjanjian kerja ini, berlaku ketentuan-ketentuan yang tercantum dalam peraturan perusahaan Pihak Pertama dan Pihak Pemberi Jasa.</li>
                <li>Perjanjian Kerja ini akan diubah dan atau ditambah sebagaimana mestinya apabila ternyata terdapat kekeliruan dan atau kekurangan dikemudian hari.</li>
                <li>Selama proses melamar dan ditempatkan bekerja tidak dikenakan biaya sepersenpun atau dimintai imbalan / barang oleh Management Perusahaan ataupun perwakilan yang ada dilokasi kerja (Supervisor / Team leader Area kerja).</li>
            </ol>
            
            <p>Demikian perjanjian kerja waktu tertentu ini dibuat oleh kedua belah pihak dalam keadaan sehat jasmani dan rohani, tanpa tekanan atau paksaan dari pihak manapun dan akan dilaksanakan dengan penuh tanggung jawab.</p>
   
            <table width="100%">
                <tr>
                    <td width="50%">Pihak Pertama</td>
                    <td width="50%">Pihak Kedua</td>
                </tr>
                <tr>
                    <td width="50%" height="70"></td>
                    <td width="50%" height="70"></td>
                </tr>
                <tr>
                    <td width="50%"><strong><u>R I y a d i</u></strong></td>
                    <td width="50%"><strong><u>{{employee_name}}</u></strong></td>
                </tr>
                <tr>
                    <td width="50%">Recruitment &amp; Personalia Officer</td>
                    <td width="50%">Karyawan</td>
                </tr>
            </table>  