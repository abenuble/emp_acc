@extends('master')
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Assessment Detail')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">

                            <div class="form-group">
                                <label>{{language_data('Dimensions')}}</label>
                                <input type="text" name="dimensions" class="form-control" readonly value="{{$assessment->dimensions}}">
                            </div>
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata">
                                                <table class="table table-hover table-ultra-responsive" id="myTableData"  >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 15%;">{{language_data('Aspect')}}</th>
                                                            <th style="width: 15%;">KS</th>
                                                            <th style="width: 15%;">K</th>
                                                            <th style="width: 15%;">C</th>
                                                            <th style="width: 15%;">B</th>
                                                            <th style="width: 15%;">BS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($aspect as $a)
                                                        <tr>
                                                            <td>{{$a->aspect}}</td>
                                                            <td>{{$a->KS}}</td>
                                                            <td>{{$a->K}}</td>
                                                            <td>{{$a->C}}</td>
                                                            <td>{{$a->B}}</td>
                                                            <td>{{$a->BS}}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                    
                                    

                                </div>

                            </div>
                            <a class="btn btn-danger btn-sm pull-right" href="{{url('company-profile')}}">{{language_data('Back')}}</a>
                        </div>
                    </div>
                </div>
            </div>

            


            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
@endsection
