@extends('master')
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Edit Violation')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('company-profile/edit-violation-post')}}" method="post">

                                <div class="form-group">
                                    <label>{{language_data('Violation')}}</label>
                                    <input type="text" name="violation" class="form-control" required="" value="{{$violation->violation}}">
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Weight')}}</label>
                                    <input type="text" name="weight" class="form-control" required="" value="{{$violation->weight}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Information')}}</label>
                                    <textarea type="text" rows="6" name="information" class="form-control" >{{$violation->information}}</textarea>
                                </div>
                                

                                <div class="row">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" value="{{$violation->id}}" name="cmd">
                                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                                    <a class="btn btn-danger btn-sm pull-right" href="{{url('company-profile')}}">{{language_data('Back')}}</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            


            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
@endsection
