@extends('master')
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Edit Job Desc')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('company-profile/edit-jobdesc-post')}}" method="post">

                                <div class="form-group">
                                    <label>{{language_data('Position')}}</label>
                                    <select name="division" class="form-control selectpicker" data-live-search="true" id="division">
                                        @foreach($division as $d)
                                            <option value='{{$d->id}}' @if($d->id==$jobdesc->division) selected @endif>{{$d->division}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Division')}}</label>
                                    <select name="position" class="form-control selectpicker" data-live-search="true" id="position">
                                        @foreach($position as $p)
                                            <option value='{{$p->id}}' @if($d->id==$jobdesc->position) selected @endif>{{$p->position}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Report To')}}</label>
                                    <select name="report_to" class="form-control selectpicker" data-live-search="true" id="report_to">
                                        @foreach($division as $d)
                                            <option value='{{$d->id}}' @if($d->id==$jobdesc->division) selected @endif>{{$d->division}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Detail')}}</label>
                                    <textarea type="text" rows="6" name="detail" class="form-control" >{{$jobdesc->detail}}</textarea>
                                </div>
                                

                                <div class="row">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" value="{{$jobdesc->id}}" name="cmd">
                                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                                    <a class="btn btn-danger btn-sm pull-right" href="{{url('company-profile')}}">{{language_data('Back')}}</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            


            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
@endsection
