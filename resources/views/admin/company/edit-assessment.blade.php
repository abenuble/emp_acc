@extends('master')
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Assessment Detail')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('company-profile/edit-assessment-post')}}" method="post">

                                <div class="form-group">
                                    <label>{{language_data('Dimensions')}}</label>
                                    <input type="text" name="dimensions" class="form-control" value="{{$assessment->dimensions}}">
                                </div>
                                <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="+" onclick="Javascript:addRow()">
                                <br>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="col-sm-12">
                                            <div class="panel">
                                                <div id="mydata">
                                                    <table class="table table-hover table-ultra-responsive" id="myTableData"  >
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 15%;">{{language_data('Aspect')}}</th>
                                                                <th style="width: 15%;">KS</th>
                                                                <th style="width: 15%;">K</th>
                                                                <th style="width: 15%;">C</th>
                                                                <th style="width: 15%;">B</th>
                                                                <th style="width: 15%;">BS</th>
                                                                <th style="width: 10%;">{{language_data('Action')}}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($aspect as $a)
                                                            <tr>
                                                                <td><input type="text" class="form-control" value="{{$a->aspect}}" name="aspect[]"></td>
                                                                <td><input type="text" class="form-control" value="{{$a->KS}}" name="KS[]"></td>
                                                                <td><input type="text" class="form-control" value="{{$a->K}}" name="K[]"></td>
                                                                <td><input type="text" class="form-control" value="{{$a->C}}" name="C[]"></td>
                                                                <td><input type="text" class="form-control" value="{{$a->B}}" name="B[]"></td>
                                                                <td><input type="text" class="form-control" value="{{$a->BS}}" name="BS[]"></td>
                                                                <td><input class="btn btn-danger btn-xs pull-right" type="button" value = "-" onClick="Javacsript:deleteRow(this)"></td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>

                                        
                                        

                                    </div>

                                </div>
                                <div class="row">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" value="{{$assessment->id}}" name="cmd">
                                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                                    <a class="btn btn-danger btn-sm pull-right" href="{{url('company-profile')}}">{{language_data('Back')}}</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            


            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    <script>
        function addRow() {
         
            var table = document.getElementById("myTableData");

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            row.insertCell(0).innerHTML= '<input type="text" class="form-control" value="" name="aspect[]">';
            row.insertCell(1).innerHTML= '<input type="text" class="form-control" value="" name="KS[]">';
            row.insertCell(2).innerHTML= '<input type="text" class="form-control" value="" name="K[]">';
            row.insertCell(3).innerHTML= '<input type="text" class="form-control" value="" name="C[]">';
            row.insertCell(4).innerHTML= '<input type="text" class="form-control" value="" name="B[]">';
            row.insertCell(5).innerHTML= '<input type="text" class="form-control" value="" name="BS[]">';
            row.insertCell(6).innerHTML= '<input class="btn btn-danger btn-xs pull-right" type="button" value = "-" onClick="Javacsript:deleteRow(this)">';

        }

        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData");
            table.deleteRow(index);
        
        }
    </script>
@endsection
