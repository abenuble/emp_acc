@extends('master')
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Career Path Detail')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('company-profile/edit-career-post')}}" method="post">

                                <div class="form-group">
                                    <label>{{language_data('Level')}}</label>
                                    <input type="number" name="level" class="form-control" value="{{$careerpath->level}}" required="">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Position')}}</label>
                                    <select name="position" class="form-control selectpicker" data-live-search="true" id="position">
                                        @foreach($division as $d)
                                            <option @if($careerpath->position==$d->id) selected @endif value='{{$d->id}}'>{{$d->division}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Education')}}</label>
                                    <select name="education" class="form-control selectpicker" data-live-search="true" id="education">
                                        @foreach($education as $d)
                                            <option @if($careerpath->education==$d->id) selected @endif  value='{{$d->id}}'>{{$d->education}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Major')}}</label>
                                    <textarea id="textarea-wysihtml5" class="textarea-wysihtml5 form-control" name="major"required="">{{$careerpath->major}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Working Period')}}</label>
                                    <span class="help">e.g. "15 Tahun, 20 Tahun"</span>
                                    <input type="text" name="working_period" class="form-control" value="{{$careerpath->working_period}}" required="">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Competences')}}</label>
                                    <textarea id="textarea-wysihtml5" class="textarea-wysihtml5 form-control" name="competences" required="">{{$careerpath->competences}}</textarea>
                                </div>
                                                        
                                <div class="row">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" value="{{$careerpath->id}}" name="cmd">
                                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                                    <a class="btn btn-danger btn-sm pull-right" href="{{url('company-profile')}}">{{language_data('Back')}}</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            


            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
@endsection
