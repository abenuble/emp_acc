@extends('master')
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Company Detail')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

        <div class="row">

            <div class="col-lg-12">
                <div class="panel-body">
                    <div class="row text-center">

                        <div class="col-sm-3 m-b-15">
                            <div class="z-shad-1">
                                <div class="bg-success text-white p-15 clearfix">
                                    <span class="pull-left font-45 m-l-10"><i class="fa fa-book"></i></span>

                                    <div class="pull-right text-right m-t-15">
                                        <font size="4"><span class="m-b-5">{{language_data('Client Contract')}}</span></font><br>
                                        <font size="4"><span class="m-b-5">{{$projects}} </span></font>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3 m-b-15">
                            <div class="z-shad-1">
                                <div class="bg-complete-darker text-white p-15 clearfix">
                                    <span class="pull-left font-45 m-l-10"><i class="fa fa-users"></i></span>

                                    <div class="pull-right text-right m-t-15">
                                        <font size="4"><span class="m-b-5">{{language_data('Employees')}}</span></font><br>
                                        <font size="4"><span class="m-b-5">{{$employees}}</span></font>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

            {{--View Detail Company--}}
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <br>
                            <div class="pull-left">
                                <h3 class="bold font-color-1">{{$company->company}}</h3>
                                    @if($company->address!='')
                                        <span>{{$company->address}}</span><br>
                                    @endif
                                    @if($company->phone_number!='')
                                        <span>{{$company->phone_number}}</span><br>
                                    @endif
                                    @if($company->pic!='')
                                        <span>{{$company->pic}} - {{$company->pic_phone}} - {{$company->pic_jabatan}}</span><br>
                                    @endif
                            </div>                                
                        </div>
                    </div>
                </div>
            </div>

            {{--View Project Company--}}
            <div class="row">

                <div class="col-lg-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#project" aria-controls="home" role="tab" data-toggle="tab">{{language_data('Client Contract')}}</a></li>
                        <li role="presentation"><a href="#employee" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Employee')}}</a></li>
                    </ul>
                    
                    <!-- Tab panes -->
                    <div class="tab-content panel p-20">

                    {{--project--}}
                        <div role="tabpanel" class="tab-pane active" id="project">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('Client Contract')}}</h3>
                                    <br>
                                </div>
                                <div class="panel-body p-none">
                                    <table class="table data-table table-hover table-ultra-responsive">
                                        <thead>
                                            <tr>
                                                <th style="width: 5%;">No</th>
                                                <th style="width: 10%;">{{language_data('Client Contract Number')}}</th>
                                                <th style="width: 15%;">{{language_data('Client Contract')}}</th>
                                                <th style="width: 15%;">{{language_data('Date Start')}}</th>
                                                <th style="width: 15%;">{{language_data('Date End')}}</th>
                                                <th style="width: 10%;">{{language_data('Value')}}</th>
                                                <th style="width: 10%;">{{language_data('Status')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $ctr = 0; ?>
                                        @foreach($project as $pro)
                                            <?php $ctr++; ?>
                                            <tr>
                                                <td data-label="No">{{$ctr}}</td>
                                                <td data-label="project_number">{{$pro->project_number}}</td>
                                                <td data-label="project">
                                                    @if($project_permission->R==1) <a href="{{url('projects/view/'.$pro->id)}}">{{$pro->project}}</a> @else {{$pro->project}} @endif
                                                </td>
                                                <td data-label="date_start">{{get_date_format($pro->start_date)}}</td>
                                                <td data-label="date_end">{{get_date_format($pro->end_date)}}</td>
                                                <td data-label="value">{{app_config('Currency')}} {{number_format($pro->value,0)}}</td>
                                                @if($pro->status=='opening')
                                                    <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Open')}}</p></td>
                                                @elseif($pro->status=='closed')
                                                    <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Closed')}}</p></td>
                                                @else
                                                    <td data-label="Status"><p class="btn btn-warning btn-xs">{{language_data('Drafted')}}</p></td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    {{--employee--}}
                        <div role="tabpanel" class="tab-pane" id="employee">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('Employees')}}</h3>
                                    <br>
                                </div>
                                <div class="panel-body p-none">
                                    <table class="table data-table table-hover table-ultra-responsive">
                                        <thead>
                                        <tr>
                                            <th style="width: 5%;">No</th>
                                            <th style="width: 10%;">NIK</th>
                                            <th style="width: 20%;">{{language_data('Name')}}</th>
                                            <th style="width: 20%;">{{language_data('Location')}}</th>
                                            <th style="width: 20%;">{{language_data('Address')}}</th>
                                            <th style="width: 20%;">{{language_data('Phone Number')}}</th>
                                            <th style="width: 5%;">{{language_data('Status')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $ctr = 0; ?>
                                        @foreach($employee as $d)
                                        <?php $ctr++; ?>
                                        <tr>
                                            <td data-label="No">{{$ctr}}</td>
                                            <td data-label="NIK">{{$d->employee_code}}</td>
                                            <td data-label="Name"><p>
                                                @if($employee_permission->R==1) <a href="{{url('employees/view/'.$d->id)}}"> {{$d->fname}} {{$d->lname}}</a> @else {{$d->fname}} {{$d->lname}} @endif
                                            </p></td>
                                            <td data-label="Lokasi"><p> {{$d->location}} </p></td>   
                                            <td data-label="Address"><p> {{$d->address}} </p></td>                                    
                                            <td data-label="Phone Number"><p>{{$d->phone}}</p></td>
                                            @if($d->status=='active')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Active')}}</p></td>
                                            @else
                                            <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Inactive')}}</p></td>
                                            @endif
                                        </tr>
        
                                        @endforeach
        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    <script>
    $(document).ready(function () {
        $('#anniversary').datetimepicker({
            locale: 'id',
            useCurrent: false,
            format: 'DD MMMM YYYY'
        });
  });
</script>
@endsection
