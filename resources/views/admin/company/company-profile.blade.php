@extends('master')
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Company Profile')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')    
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body p-t-20">
                            <div class="clearfix">
                                <div class="col-md-12">
                                    <div class="pull-left m-r-30">
                                        <div class="thumbnail m-b-none">
                                        @if($sss->avatar!='')
                                            <img src="<?php echo asset('assets/company_pic/'.$sss->avatar); ?>" alt="Profile Page" width="150px" height="150px">
                                        @else
                                            <img src="<?php echo asset('assets/company_pic/company.png');?>" alt="Profile Page" width="150px" height="150px">
                                        @endif
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <h3 class="bold font-color-1">{{$sss->company}}</h3>
                                            @if($sss->address!='')
                                                <span>{{language_data('address')}} : {{$sss->address}}</span><br>
                                            @endif
                                            @if($sss->phone_number!='')
                                                <span>{{language_data('Phone')}} : {{$sss->phone_number}}</span><br>
                                            @endif
                                            @if($sss->fax!='')
                                                <span>{{language_data('Fax')}} : {{$sss->fax}}</span><br>
                                            @endif
                                            @if($sss->email!='')
                                                <span>{{language_data('Email')}} : {{$sss->email}}</span><br>
                                            @endif
                                    </div>
                                    @if($permcheck->U==1)
                                    <a data-toggle="modal" data-target="#sss"><i class="fa fa-edit"></i></a>
                                    @endif
                                </div>
                                
                                {{-- <button class="btn btn-success btn-sm pull-right" href="{{ url('cetakWord') }}" ><i class="fa fa-download"></i> Word</button>
                                <form class="form-some-up" role="form" action="{{ url('cetakWord') }}" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <button type="submit" class="btn btn-primary">Word</button>
                                </form> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="p-30 p-t-none p-b-none">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body p-t-20">
                            <div class="clearfix">
                                @if($permcheck->U==1)
                                <center><span>{{$vision_mission->slogan}} <a data-toggle="modal" data-target="#slogan"><i class="fa fa-edit"></i></a></span></center>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="p-30 p-t-none p-b-none">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#vision_mission" aria-controls="home" role="tab" data-toggle="tab">{{language_data('Vision Mission')}}</a></li>
                        <li role="presentation"><a href="#organizational_structure" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Organizational Structure')}}</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content panel p-20">

                    {{--Vision Mission--}}
                        <div role="tabpanel" class="tab-pane active" id="vision_mission">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <label>{{language_data('Vision')}} 
                                                @if($permcheck->U==1) <a data-toggle="modal" data-target="#vision"><i class="fa fa-edit"></i></a> @endif
                                            </label><br>
                                            <span>{{$vision_mission->vision}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <label>{{language_data('Mission')}} 
                                                @if($permcheck->U==1) <a data-toggle="modal" data-target="#mission"><i class="fa fa-edit"></i></a> @endif
                                            </label>
                                            <div class="col-lg-12">
                                                <span>{!! $vision_mission->mission !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                    {{--Organizational Structure--}}
                        <div role="tabpanel" class="tab-pane" id="organizational_structure">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label>{{language_data('Organizational Structure')}}</label><br>
                                    @if($permcheck->U==1)
                                    <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#upload" ><i class="fa fa-plus"></i> {{language_data('Upload Picture')}}</button>
                                    @endif
                                    <br>
                                    <img src="<?php echo asset('assets/company_profile_files/'.$organizational_structure->organizational_structure);?>" width="1173" height="615"><br>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('company-profile/organizational-structure-upload') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="file" accept="image/*"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal edit sss-->
            <div class="modal fade" id="sss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Edit')}}</h4>
                        </div>
                        <form class="form-some-up" name="form" role="form" action="{{ url('company-profile/edit-sss') }}" method="post" enctype="multipart/form-data" onsubmit="return phonenumber(document.form.phone);">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>Logo</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="file" accept="image/*"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('address')}}</label>
                                    <textarea type="text" rows="6" name="address" class="form-control" required="">{{$sss->address}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Phone Number')}}</label>
                                    <span class="help">e.g. "xxx-xxx-xxxx, xxxx-xxx-xxxx, xxxx-xxxx-xxxx, xxxx-xxxx-xxx"</span>
                                    <input type="text" class="form-control" required="" value="{{$sss->phone_number}}" name="phone">
                                </div>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$sss->id}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal add-data-career -->
            {{-- <div class="modal fade" id="add_data_career" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add Career Path')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('company-profile/career-add') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Level')}}</label>
                                    <input type="number" min="1" name="level" class="form-control" required="">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Position')}}</label>
                                    <select name="position" class="form-control selectpicker" data-live-search="true" id="position">
                                        @foreach($division as $d)
                                            <option value='{{$d->id}}'>{{$d->division}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Education')}}</label>
                                    <select name="education" class="form-control selectpicker" data-live-search="true" id="education">
                                        @foreach($education as $d)
                                            <option value='{{$d->id}}'>{{$d->education}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Major')}}</label>
                                    <textarea id="textarea-wysihtml5" class="textarea-wysihtml5 form-control" name="major"required=""></textarea>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Working Period')}}</label>
                                    <span class="help">e.g. "15 Tahun, 20 Tahun"</span>
                                    <input type="text" name="working_period" class="form-control" value="" required="">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Competences')}}</label>
                                    <textarea id="textarea-wysihtml5" class="textarea-wysihtml5 form-control" name="competences" required=""></textarea>
                                </div>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}

            <!-- Modal add-data-jobdesc -->
            {{-- <div class="modal fade" id="add_data_jobdesc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add Job Description')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('company-profile/jobdesc-add') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Position')}}</label>
                                    <select name="division" class="form-control selectpicker" data-live-search="true" id="division">
                                        @foreach($division as $d)
                                            <option value='{{$d->id}}'>{{$d->division}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Division')}}</label>
                                    <select name="position" class="form-control selectpicker" data-live-search="true" id="position">
                                        @foreach($position as $p)
                                            <option value='{{$p->id}}'>{{$p->position}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Report To')}}</label>
                                    <select name="report_to" class="form-control selectpicker" data-live-search="true" id="report_to">
                                        @foreach($division as $d)
                                            <option value='{{$d->id}}'>{{$d->division}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Detail')}}</label>
                                    <textarea type="text" rows="6" name="detail" class="form-control" ></textarea>
                                </div>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}

            <!-- Modal add-data-violation -->
            {{-- <div class="modal fade" id="add_data_violation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add Violation')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('company-profile/violation-add') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Violation')}}</label>
                                    <input type="text" name="violation" class="form-control" required="">
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Weight')}}</label>
                                    <input type="text" name="weight" class="form-control" required="">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Information')}}</label>
                                    <textarea type="text" rows="6" name="information" class="form-control" ></textarea>
                                </div>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}

            <!-- Modal add-data-assessment -->
            {{-- <div class="modal fade" id="add_data_assessment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add Assessment')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('company-profile/assessment-add') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Dimensions')}}</label>
                                    <input type="text" name="dimensions" class="form-control" required="">
                                </div>
                                
                                <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="+" onclick="Javascript:addRow()">
                                <br>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="col-sm-12">
                                            <div class="panel">
                                                <div id="mydata">
                                                    <table class="table table-hover table-ultra-responsive" id="myTableData"  >
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 15%;">{{language_data('Aspect')}}</th>
                                                                <th style="width: 15%;">KS</th>
                                                                <th style="width: 15%;">K</th>
                                                                <th style="width: 15%;">C</th>
                                                                <th style="width: 15%;">B</th>
                                                                <th style="width: 15%;">BS</th>
                                                                <th style="width: 10%;">{{language_data('Action')}}</th>
                                                            </tr>
                                                        </thead>

                                                    </table>

                                                </div>

                                            </div>
                                        </div>

                                        
                                        

                                    </div>

                                </div>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}

            <!-- Modal Mission -->
            <div class="modal fade" id="mission" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Edit Mission')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('company-profile/edit-mission') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Mission')}}</label>
                                    <textarea id="textarea-wysihtml5" class="textarea-wysihtml5 form-control" name="mission">{{$vision_mission->mission}}</textarea>
                                </div>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$vision_mission->id}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal vision -->
            <div class="modal fade" id="vision" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Edit Vision')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('company-profile/edit-vision') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Vision')}}</label>
                                    <textarea class="form-control" name="vision">{{$vision_mission->vision}}</textarea>
                                </div>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$vision_mission->id}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal slogan -->
            <div class="modal fade" id="slogan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Edit Slogan')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('company-profile/edit-slogan') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Slogan')}}</label>
                                    <textarea class="form-control" name="slogan">{{$vision_mission->slogan}}</textarea>
                                </div>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$vision_mission->id}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>

    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    <script>
        function addRow() {
         
            var table = document.getElementById("myTableData");

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            row.insertCell(0).innerHTML= '<input type="text" class="form-control" value="" name="aspect[]">';
            row.insertCell(1).innerHTML= '<input type="text" class="form-control" value="" name="KS[]">';
            row.insertCell(2).innerHTML= '<input type="text" class="form-control" value="" name="K[]">';
            row.insertCell(3).innerHTML= '<input type="text" class="form-control" value="" name="C[]">';
            row.insertCell(4).innerHTML= '<input type="text" class="form-control" value="" name="B[]">';
            row.insertCell(5).innerHTML= '<input type="text" class="form-control" value="" name="BS[]">';
            row.insertCell(6).innerHTML= '<input class="btn btn-danger btn-xs pull-right" type="button" value = "-" onClick="Javacsript:deleteRow(this)">';

        }

        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData");
            table.deleteRow(index);
        
        }
        function phonenumber(inputtxt)
        {
            // var phoneno = /^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;
            // var phoneno2 = /^\(?([0-9]{4})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;  
            // var phoneno3 = /^\(?([0-9]{4})\)?[-]?([0-9]{4})[-]?([0-9]{4})$/;  
            // var phoneno4 = /^\(?([0-9]{4})\)?[-]?([0-9]{4})[-]?([0-9]{3})$/;
            // if(inputtxt.value.match(phoneno)){
            //     return true;
            // } else if(inputtxt.value.match(phoneno2)){
            //     return true;
            // } else if(inputtxt.value.match(phoneno3)){
            //     return true;
            // } else if(inputtxt.value.match(phoneno4)){
            //     return true;
            // } else{
            //     alert("Not a valid Phone Number");
            //     return false;
            // }
        }

        $('#textarea').wysihtml5({
            "html": true,
            "toolbar": "toolbar"
        });
    </script>
@endsection
