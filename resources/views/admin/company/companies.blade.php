@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Companies')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Companies')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#companies"><i class="fa fa-plus"></i> {{language_data('Add New Company')}}</button>
                            <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload">{{language_data('Upload')}}</button></a>
                            <br><br>
                            @endif
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 5%;">{{language_data('Company area code')}}</th>
                                    <th style="width: 20%;">{{language_data('Company')}}</th>
                                    <th style="width: 15%;">{{language_data('Address')}}</th>
                                    <th style="width: 15%;">PIC</th>
                                    <th style="width: 10%;">{{language_data('Phone Number')}}</th>
                                    <th style="width: 5%;">{{language_data('Category')}}</th>
                                    <th style="width: 5%;">{{language_data('Status')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($companies as $d)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Company Code Area"><p>{{$d->company_code}}</p></td>
                                        <td data-label="Company"><p>{{$d->company}}</p></td>
                                        <td data-label="Address"><p>{{$d->address}}</p></td>
                                        <td data-label="PIC"><p>{{$d->pic}}</p></td>
                                        <td data-label="Phone Number"><a href="tel:{{$d->phone_number}}">{{$d->phone_number}}</a></td>
                                        <td data-label="Category"><p>{{$d->category}}</p></td>
                                        @if($d->status=='active')
                                            <td data-label="status"><p class="btn btn-complete btn-xs">{{language_data('Active')}}</p></td>
                                        @elseif($d->status=='inactive')
                                            <td data-label="status"><p class="btn btn-danger btn-xs">{{language_data('Inactive')}}</p></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-complete btn-xs" href="{{url('companies/view/'.$d->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            @if($permcheck->U==1)
                                            <a class="btn btn-success btn-xs" href="{{url('companies/edit/'.$d->id)}}" ><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>
                                            @endif
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Modal -->
            <div class="modal fade" id="companies" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Company')}}</h4>
                        </div>
                        <form class="form-some-up" name="form" role="form" method="post" action="{{url('companies/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Company Code Area')}}</label>
                                    <input type="text" class="form-control" required="" name="company_code" >
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Company Name')}}</label>
                                    <input type="text" class="form-control" required="" placeholder="company name" name="company">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Address')}}</label>
                                    <input type="text" class="form-control" required="" placeholder="company address" name="address">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Phone Number')}}</label>
                                    <span class="help">e.g. "xxx-xxx-xxxx, xxxx-xxx-xxxx, xxxx-xxxx-xxxx, xxxx-xxxx-xxx"</span>
                                    <input type="text" class="form-control" required="" name="phone">
                                </div>

                                <div class="form-group">
                                    <label>PIC</label>
                                    <input type="text" class="form-control" required="" placeholder="pic name" name="pic">
                                </div>

                                <div class="form-group">
                                    <label>PIC Jabatan</label>
                                    <input type="text" class="form-control" required="" placeholder="Jabatan" name="pic_jabatan">
                                </div>

                                <div class="form-group">
                                    <label>PIC {{language_data('Phone Number')}}</label>
                                    <span class="help">e.g. "xxx-xxx-xxxx, xxxx-xxx-xxxx, xxxx-xxxx-xxxx, xxxx-xxxx-xxx"</span>
                                    <input type="text" class="form-control" required="" name="pic_phone">
                                </div>

                                <div class="form-group m-none">
                                    <label for="e20">{{language_data('Status')}}</label>
                                    <select name="status" class="form-control selectpicker">
                                        <option value="active">{{language_data('Active')}}</option>
                                        <option value="inactive">{{language_data('Inactive')}}</option>
                                    </select>
                                </div><br>

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('companies/importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file" accept="application/vnd.ms-excel"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
 
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('companies/downloadExcel/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            $('#anniversary').datetimepicker({
                locale: 'id',
                useCurrent: false,
                format: 'DD MMMM YYYY'
            });
            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/companies/delete/" + id;
                    }
                });
            });

        });

        function phonenumber(inputtxt)
        {
            var phoneno = /^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;
            var phoneno2 = /^\(?([0-9]{4})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;  
            var phoneno3 = /^\(?([0-9]{4})\)?[-]?([0-9]{4})[-]?([0-9]{4})$/;  
            var phoneno4 = /^\(?([0-9]{4})\)?[-]?([0-9]{4})[-]?([0-9]{3})$/;
            if(inputtxt.value.match(phoneno)){
                return true;
            } else if(inputtxt.value.match(phoneno2)){
                return true;
            } else if(inputtxt.value.match(phoneno3)){
                return true;
            } else if(inputtxt.value.match(phoneno4)){
                return true;
            } else{
                alert("Not a valid Phone Number");
                return false;
            }
        }

    </script>
@endsection
