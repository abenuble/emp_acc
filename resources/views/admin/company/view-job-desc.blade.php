@extends('master')
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Job Desc Detail')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="form-group">
                                <label>{{language_data('Position')}}</label>
                                <input type="text" class="form-control" name="position" readonly value="{{$jobdesc->position_info->position}}">
                            </div>
                            
                            <div class="form-group">
                                <label>{{language_data('Division')}}</label>
                                <input type="text" class="form-control" name="division" readonly value="{{$jobdesc->division_info->division}}">
                            </div>

                            <div class="form-group">
                                <label>{{language_data('Report To')}}</label>
                                <input type="text" class="form-control" name="Report To" readonly value="{{$jobdesc->division_info->division}}">
                            </div>

                            <div class="form-group">
                                <label>{{language_data('Detail')}}</label>
                                <textarea type="text" rows="6" readonly name="detail" class="form-control" >{{$jobdesc->detail}}</textarea>
                            </div>
                            
                            <a class="btn btn-danger btn-sm pull-right" href="{{url('company-profile')}}">{{language_data('Back')}}</a>
                        </div>
                    </div>
                </div>
            </div>

            


            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
@endsection
