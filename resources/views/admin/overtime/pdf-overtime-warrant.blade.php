<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - {{language_data('Print Payslip')}}</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        .help-split {
            display: inline-block;
            width: 30%;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-30 p-t-none p-b-none">
        
            <div class="col-lg-6">
                <div class="panel">
                    <div class="panel-body">
                        <div class="panel-heading">
                            <h3 class="panel-title"> View Overtime Warrant</h3>
                        </div>
                        <div class="form-group">
                                    <label for="letter_number">{{language_data('Letter Number')}}</label>
                                 
                                    <input type="text" class="form-control" required readonly name="letter_number" value="{{$overtime_warrant->letter_number}}">
                                  
                                </div>
                        <div class="form-group">
                        
                            <label>{{language_data('Employee Code')}}</label>
                            <select name="employee" class="form-control selectpicker" id="employee_id">
                            @foreach($employee as $e)
                                <option value="{{$e->id}}" @if($e->id==$overtime_warrant->emp_id) selected @endif>{{$e->employee_code}}</option>
                            @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>{{language_data('Employee Code')}}</label>
                            <select class="selectpicker form-control" data-live-search="true" id="employee_name">
                                @foreach($employee as $e)
                                    <option value='{{$e->id}}' @if($e->id==$overtime_warrant->emp_id) selected @endif>{{$e->fname}} {{$e->lname}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Date From</label>
                                    <input type="text" class="form-control datePicker" require="" value="{{get_date_format($overtime_warrant->date_from)}}" name="date_from">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Date To</label>
                                    <input type="text" class="form-control datePicker" require="" value="{{get_date_format($overtime_warrant->date_to)}}" name="date_to">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Overtime From</label>
                                    <input type="text" class="form-control timePicker" require="" value="{{get_time_format($overtime_warrant->overtime_from)}}" name="overtime_from">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Overtime To</label>
                                    <input type="text" class="form-control timePicker" require="" value="{{get_time_format($overtime_warrant->overtime_to)}}" name="overtime_to">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>{{language_data('Information')}}</label>
                            <textarea class="form-control" rows="6" name="information"> {{$overtime_warrant->information}} </textarea>
                        </div>

                    </div>
                </div>
            </div>


            
        </div>
    </div>
</main>


</body>
</html>