@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Lihat Surat Perintah Lembur</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('overtime/edit-warrant')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> Lihat Surat Perintah Lembur</h3>
                                </div>
                                <div class="form-group">
                                    <label for="letter_number">Nomor Surat</label>
                                 
                                    <input type="text" class="form-control" required readonly name="letter_number" value="{{$overtime_warrant->letter_number}}">
                                  
                                </div>
                                <div class="form-group">
                                    <label>NIK</label>
                                    <select name="employee" class="form-control selectpicker" id="employee_id">
                                    @foreach($employee as $e)
                                        <option value="{{$e->id}}" @if($e->id==$overtime_warrant->emp_id) selected @endif>{{$e->employee_code}}</option>
                                    @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Nama Karyawan</label>
                                    <select class="selectpicker form-control" data-live-search="true" id="employee_name">
                                        @foreach($employee as $e)
                                            <option value='{{$e->id}}' @if($e->id==$overtime_warrant->emp_id) selected @endif>{{$e->fname}} {{$e->lname}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tanggal Mulai</label>
                                            <input type="text" class="form-control datePicker" require="" value="{{get_date_format($overtime_warrant->date_from)}}" name="date_from">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tanggal Selesai</label>
                                            <input type="text" class="form-control datePicker" require="" value="{{get_date_format($overtime_warrant->date_to)}}" name="date_to">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Mulai Dari</label>
                                            <input type="text" class="form-control timePicker" require="" value="{{get_time_format($overtime_warrant->overtime_from)}}" name="overtime_from">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Selesai</label>
                                            <input type="text" class="form-control timePicker" require="" value="{{get_time_format($overtime_warrant->overtime_to)}}" name="overtime_to">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>{{language_data('Copies')}}</label>
                                    <input type="text" class="form-control" value="{{$overtime_warrant->tembusan}}"  name="tembusan"/>
                                </div>
                                <div class="form-group">
                                    <label>{{language_data('Information')}}</label>
                                    <textarea class="form-control" rows="6" name="information"> {{$overtime_warrant->information}} </textarea>
                                </div>
                               
                                @if($overtime_warrant->status!='draft')
                                <div class="form-group">
                                        <label for="message">Status Persetujuan</label>
                                        <input type="text" class="form-control"   @if($overtime_warrant->status!='draft') readonly @endif value="{{$overtime_warrant->keterangan}}" name="keterangan"/>
                                    </div>
                                    @endif
                                @if($permcheck->U==1)
                                    @if($overtime_warrant->status=='draft')
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" value="{{$overtime_warrant->id}}" name="cmd">
                                        <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                                    @endif
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/overtime/delete/" + id;
                    }
                });
            });

            /*For career Loading*/
            $("#employee_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/overtime/get-employee',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#employee_name").html( data).removeAttr('disabled').selectpicker('refresh');;
                    }
                });
            });


        });
    </script>
@endsection
