@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Surat Perintah Lembur</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Surat Perintah Lembur</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#warrant"><i class="fa fa-plus"></i> Tambah SPL</button>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 15%;">{{language_data('Company')}}</th>
                                    <th style="width: 15%;">{{language_data('Employee')}}</th>
                                    <th style="width: 20%;">Tanggal</th>
                                    <th style="width: 15%;">Total Lembur</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($warrant as $w)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Company"><p>{{$w->employee_info->company_name->company}}</p></td>
                                        <td data-label="Employee"><p>{{$w->employee_info->fname}} {{$w->employee_info->lname}}</p></td>
                                        <td data-label="Overtime Date"><p>{{get_date_format($w->date_from)}} - {{get_date_format($w->date_to)}}</p></td>
                                        <td data-label="Overtime Total"><p>{{$w->overtime_total}} H</p></td>
                                        @if($w->status=='rejected')
                                            <td data-label="status"><p class="btn btn-danger btn-xs">{{language_data('Rejected')}}</p></td>
                                        @elseif($w->status=='accepted')
                                            <td data-label="status"><p class="btn btn-success btn-xs">{{language_data('Accepted')}}</p></td>
                                        @else
                                            <td data-label="status"><p class="btn btn-warning btn-xs">{{language_data('Drafted')}}</p></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                        @if($w->status=='accepted')
                                            <a class="btn btn-success btn-xs" href="{{url('overtime/view-warrant/'.$w->id)}}"><i class="fa fa-edit"></i> {{language_data('View')}}</a>
                                            <a href="#" class="btn btn-info btn-xs download" id="{{$w->id}}"><i class="fa fa-print"></i> Print</a>
                                        @else
                                            <a class="btn btn-success btn-xs" href="{{url('overtime/view-warrant/'.$w->id)}}"><i class="fa fa-edit"></i> {{language_data('View')}}</a>
                                            @if($permcheck->D==1)
                                            <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$w->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                            @endif
                                        @endif
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade" id="warrant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Tambah SPL</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('overtime/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">
                            <div class="form-group">
                                    <label for="letter_number">Nomor Surat</label>
                                    <input type="text" class="form-control" required readonly name="letter_number" value="{{$number}}/SPL/SSS/{{get_roman_letters($month)}}/{{$year}}">
                                </div>
                                <div class="form-group">
                                    <label>{{language_data('Employee')}}</label>
                                    <select name="employee" class="form-control selectpicker" id="employee_id">
                                        <option value="0">Pilih Karyawan</option>
                                        @foreach($employee as $e)
                                            <option value="{{$e->id}}">{{$e->fname}} {{$e->lname}} ({{$e->employee_code}})</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tanggal Dari</label>
                                            <input type="text" class="form-control datePicker" require="" name="date_from">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tanggal Selesai</label>
                                            <input type="text" class="form-control datePicker" require="" name="date_to">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Mulai Dari</label>
                                            <input type="text" class="form-control timePicker" require="" name="overtime_from">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Selesai</label>
                                            <input type="text" class="form-control timePicker" require="" name="overtime_to">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>{{language_data('Copies')}}</label>
                                    <input type="text" class="form-control"  name="tembusan"/>
                                </div>
                                <div class="form-group">
                                    <label>{{language_data('Information')}}</label>
                                    <textarea class="form-control" rows="6"  name="information"></textarea>
                                </div>
                                
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/overtime/delete/" + id;
                    }
                });
            });

            /*For download pdf Info*/
            $(".download").click(function (e) {
                var id = this.id;
                var _url = $("#_url").val();
                window.location.href = _url + "/overtime/download-pdf/" + id;
            });


        });
    </script>
@endsection
