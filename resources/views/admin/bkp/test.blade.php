
            $(document).ready(function(){
                rules();
                itemPo = 0;
                itemRefrensi = [];
                $("#formAdd").submit(function(event){
                  if ($("#formAdd").valid() == true) {
                    subTotalitem        = parseFloat(document.getElementById('payment_request_subtotal').value.replace(/\,/g, ""));
                    subTotalPembayaran  = parseFloat(document.getElementById('payment_request_giro_nominal').value.replace(/\,/g, "")) + parseFloat(document.getElementById('payment_request_pengeluaran_nominal').value.replace(/\,/g, "")); 
                    if (subTotalitem != subTotalPembayaran) {
                        swal({
                            title: "Alert!",
                            text: "Nominal tidak sesuai!",
                            type: "error",
                            confirmButtonClass: "btn-raised btn-danger",
                            confirmButtonText: "OK",
                        });
                    } else {
                        actionData2();   
                    }
                  }
                  return false;
                });
                $('#m_partner_id').css('width', '100%');
                selectList_supplier("#m_partner_id");
                $('#pengeluaran_id').css('width', '100%');
                $('#pengeluaran_id').select2();
                $('#payment_request_giro_refrensi_id').css('width', '100%');
                checkTipe(0);
                var refrensi = [];
                if (document.getElementsByName("kode")[0].value.length >0) {
                    editData(document.getElementsByName("kode")[0].value);
                }
            });

            function resetDetail() {
                itemPo = 0;
                $('#jml_itempo').val(itemPo);
                $("#default-table tbody").empty();
                document.getElementById('payment_request_subtotal').value = 0;
                document.getElementsByName('payment_request_jumlah_bayar')[0].value = 0;
                $('.money').number( true, 0, '.', ',' );
                generateItemDetailNotaDebet();
                $('#pengeluaran_id').select2();
                $('#pengeluaran_id').select2('destroy');
                $('#pengeluaran_id').empty();
                $('#pengeluaran_id').select2();
                document.getElementById('bank').checked = false;
                document.getElementById('kas').checked = false;
            }

            function checkTipe(tipe = null) {
                if (document.getElementById("bank").checked == true) {
                    tipe = 1;
                } else if (document.getElementById("kas").checked == true) {
                    tipe = 2;
                } else if (document.getElementById("tdkada").checked == true) {
                    tipe = 3;
                }

                $('#pengeluaran_id').select2();
                $('#pengeluaran_id').select2('destroy');
                $('#pengeluaran_id').empty();
                $('#pengeluaran_id').select2();
                if (tipe == 1) {
                    selectList_masterBank('#pengeluaran_id');
                } else if (tipe == 2) {
                    selectList_masterKas('#pengeluaran_id');
                } else if (tipe == 3) {
                    $('#pengeluaran_id').select2('destroy');
                    $('#pengeluaran_id').empty();
                    document.getElementById('pengeluaran_id').disabled = true;
                }
                checkGiro();
            }

            function checkGiro() {
                if (document.getElementById('payment_request_giro_checkbox').checked == true) {
                    document.getElementById('pengeluaran_id').required = false;
                    $('#payment_request_giro_refrensi_id').select2();
                    $('#payment_request_giro_refrensi_id').select2('destroy');
                    $('#payment_request_giro_refrensi_id').empty();
                    $('#payment_request_giro_refrensi_id').select2();
                    var refrensi = [];
                    var refrensi_temp = "";
                    for (var i = 1; i <= itemPo; i++) {
                        if (document.getElementById('payment_requestdet_type'+i).value == 0) {
                            if (document.getElementById('payment_requestdet_typecheck'+i).checked == true) {
                                refrensi_temp = document.getElementById('payment_requestdet_detail'+i).value;
                                refrensi.push(refrensi_temp);
                            }   
                        }
                    }
                    var parameter = {
                        'm_partner_id'  : document.getElementById('m_partner_id').value,
                        'refrensi'      : refrensi,
                    };
                    select2MultipleList('#payment_request_giro_refrensi_id', 'Accounting/Bukti-BG-Cek/loadDataSelect/1', 'Pilih Nomor Giro', parameter);
                    document.getElementById('payment_request_giro').value = 1;
                } else {
                    document.getElementById('pengeluaran_id').required = true;
                    $('#payment_request_giro_refrensi_id').select2();
                    $('#payment_request_giro_refrensi_id').select2('destroy');
                    $('#payment_request_giro_refrensi_id').empty();
                    $('#payment_request_giro_refrensi_id').select2();
                    var refrensi = [];
                    var refrensi_temp = "";
                    document.getElementById('payment_request_giro').value = 0;
                }
                checkNominal();
                $('.money').number( true, 0, '.', ',' );
            }

            function checkNominal(giroValue = null) {
                if (giroValue == null) {
                    var giroValue = $('#payment_request_giro_refrensi_id').val();   
                }
                document.getElementById('payment_request_giro_nominal').value = 0;
                if (giroValue != null) {
                    $.ajax({
                        type : "GET",
                        url  : 'http://jasasoftware.co/kelapa/Accounting/Bukti-BG-Cek/loadDataNominalGiro',
                        data : { giroid : giroValue },
                        dataType : "json",
                        success:function(data){
                            document.getElementById('payment_request_giro_nominal').value = data.nominal;
                            $('.money').number( true, 0, '.', ',' );
                        }
                    });
                    sumSubTotal();   
                }
            }

            function checkPembayaran(tipe) {
                $('#m_partner_id').select2();
                $('#m_partner_id').select2('destroy');
                $('#m_partner_id').empty();
                $('#m_partner_id').select2();
                $('#m_partner_id').css('width', '100%');
                if (tipe == 1) {
                    selectList_supplier("#m_partner_id");
                } else if (tipe == 2) {
                    select2List('#m_partner_id', 'Master-Data/Partner/loadDataSelect3', 'Pilih Nama');
                }
                resetDetail();
            }

            function addPo() {
                generateItemDetail();
            }

            function generateItemDetail() {
                itemPo++;
                $('#jml_itempo').val(itemPo);
                $("#default-table tbody").append('\
                    <tr id="detail'+itemPo+'">\
                        <td id="td0'+itemPo+'" class="text-center"> '+itemPo+' </td>\
                        <td id="td1'+itemPo+'"> <select class="form-control" name="m_coa_id[]" id="m_coa_id'+itemPo+'" required></select> </td>\
                        <td id="td2'+itemPo+'"> \
                            <div class="row">\
                                <div class="col-md-2">\
                                    <input type="hidden" value="0" id="payment_requestdet_type'+itemPo+'" name="payment_requestdet_type[]"/>\
                                    <label class="mt-checkbox"> BPB \
                                        <input type="checkbox" value="1" id="payment_requestdet_typecheck'+itemPo+'" onclick="checkRefresi('+itemPo+'),checkTipe()" checked />\
                                        <span></span>\
                                    </label>\
                                </div>\
                                <div class="col-md-1">\
                                </div>\
                                <div id="payment_requestdet_detail_div'+itemPo+'" class="col-md-9">\
                                    <select class="form-control" name="payment_requestdet_detail[]" id="payment_requestdet_detail'+itemPo+'" onchange="getDetailBpb('+itemPo+'),checkTipe()" required></select>\
                                </div>\
                            </div>\
                        </td>\
                        <td id="td3'+itemPo+'"> \
                            <div class="input-group">\
                                <span class="input-group-addon" style="">\
                                    RP\
                                </span>\
                                <input type="text" class="form-control money text-right" id="payment_requestdet_kekurangan'+itemPo+'" name="payment_requestdet_kekurangan[]" value="0" required readonly>\
                            </div>\
                        </td>\
                        <td id="td4'+itemPo+'"> \
                            <div class="input-group">\
                                <span class="input-group-addon" style="">\
                                    RP\
                                </span>\
                                <input type="text" class="form-control money text-right" id="payment_requestdet_jumlah'+itemPo+'" name="payment_requestdet_jumlah[]" value="0" onchange="sumSubTotal()" required>\
                            </div>\
                        </td>\
                        <td id="td5'+itemPo+'"> \
                            <button class="btn red-thunderbird" type="button" title="Remove Detail" onclick="removeItemDetail('+itemPo+')">\
                                <i class="icon-close"></i>\
                            </button>\
                        </td>\
                    </tr>\
                ');
                $('#m_coa_id'+itemPo).css('width', '100%');
                selectList_masterCOA('#m_coa_id'+itemPo, 4);
                selectList_penerimaanBarangPembayaran('#payment_requestdet_detail'+itemPo, document.getElementById('m_partner_id').value);
                $('#payment_requestdet_detail'+itemPo).css('width', '100%');
                $('.money').number( true, 0, '.', ',' );
            }

            function generateItemDetailNotaDebet() {
                $("#default-table tbody").empty();
                $.ajax({
                    type : "GET",
                    url  : 'http://jasasoftware.co/kelapa/Pembelian/Nota-Debet/loadDataPembayaran',
                    data : "id="+document.getElementById('m_partner_id').value,
                    dataType : "json",
                    success:function(data){
                        for(var i=0; i<data.val.length;i++){
                            itemPo++;
                            $('#jml_itempo').val(itemPo);
                            $("#default-table tbody").append('\
                                <tr id="detail'+itemPo+'" class="text-center">\
                                    <td id="td0'+itemPo+'"> '+itemPo+' </td>\
                                    <td id="td1'+itemPo+'"> <select class="form-control" name="m_coa_id[]" id="m_coa_id'+itemPo+'"></select> </td>\
                                    <td id="td2'+itemPo+'"> \
                                        <div class="row">\
                                            <div id="payment_requestdet_detail_div'+itemPo+'" class="col-md-12">\
                                                <input type="hidden" value="1" id="payment_requestdet_type'+itemPo+'" name="payment_requestdet_type[]"/>\
                                                <input type="hidden" name="payment_requestdet_detail[]" id="payment_requestdet_detail'+itemPo+'" value="'+data.val[i].id+'">\
                                                <input type="text" class="form-control" name="payment_requestdet_detail_nomor[]" id="payment_requestdet_detail_nomor'+itemPo+'" value="'+data.val[i].nomor+'" required readonly>\
                                            </div>\
                                        </div>\
                                    </td>\
                                    <td id="td3'+(i+1)+'"> \
                                        <div class="input-group">\
                                            <span class="input-group-addon" style="">\
                                                RP\
                                            </span>\
                                            <input type="text" class="form-control money text-right" id="payment_requestdet_kekurangan'+itemPo+'" name="payment_requestdet_kekurangan[]" value="'+(-Math.abs(data.val[i].jumlah))+'" required readonly>\
                                        </div>\
                                    </td>\
                                    <td id="td4'+(i+1)+'"> \
                                        <div class="input-group">\
                                            <span class="input-group-addon" style="">\
                                                RP\
                                            </span>\
                                            <input type="text" class="form-control money text-right" id="payment_requestdet_jumlah'+itemPo+'" name="payment_requestdet_jumlah[]" value="'+(-Math.abs(data.val[i].jumlah))+'" onchange="sumSubTotal()" required readonly>\
                                        </div>\
                                    </td>\
                                    <td id="td5'+(i+1)+'"> \
                                        <button class="btn red-thunderbird" type="button" title="Remove Detail" onclick="removeItemDetail('+itemPo+')">\
                                            <i class="icon-close"></i>\
                                        </button>\
                                    </td>\
                                </tr>\
                            ');
                            $('#m_coa_id'+itemPo).css('width', '100%');
                            selectList_masterCOA('#m_coa_id'+itemPo, 4);
                            $('.money').number( true, 0, '.', ',' );
                            sumSubTotal();
                        }
                    }
                });
            }

            function removeItemDetail(itemSeq) {
                var parent = document.getElementById("tableTbody");
                for (var i = 1; i <= itemPo; i++) {
                  if (i >= itemSeq && i < itemPo) {
                    $("#m_coa_id"+i).select2('destroy');
                    $("#m_coa_id"+i).empty();
                    $("#m_coa_id"+i).append(document.getElementById("m_coa_id"+(i+1)).innerHTML);
                    selectList_masterCOA('#m_coa_id'+i, 4);
                    document.getElementById("payment_requestdet_kekurangan"+i).value = document.getElementById("payment_requestdet_kekurangan"+(i+1)).value;
                    document.getElementById("payment_requestdet_jumlah"+i).value = document.getElementById("payment_requestdet_jumlah"+(i+1)).value;
                    if (document.getElementById("payment_requestdet_type"+(i+1)).value == 1) {
                        document.getElementById("payment_requestdet_type"+i).value = document.getElementById("payment_requestdet_type"+(i+1)).value;
                        document.getElementById("payment_requestdet_detail"+i).value = document.getElementById("payment_requestdet_detail"+(i+1)).value;
                        document.getElementById("payment_requestdet_detail_nomor"+i).value = document.getElementById("payment_requestdet_detail_nomor"+(i+1)).value;
                    } else if (document.getElementById("payment_requestdet_type"+(i+1)).value == 0) {
                        $("#td2"+i).empty();
                        $("#td2"+i).append('\
                            <div class="row">\
                                <div class="col-md-2">\
                                    <input type="hidden" value="0" id="payment_requestdet_type'+i+'" name="payment_requestdet_type[]"/>\
                                    <label class="mt-checkbox"> BPB \
                                        <input type="checkbox" value="1" id="payment_requestdet_typecheck'+i+'" onclick="checkRefresi('+i+'),checkTipe()" checked />\
                                        <span></span>\
                                    </label>\
                                </div>\
                                <div class="col-md-1">\
                                </div>\
                                <div id="payment_requestdet_detail_div'+i+'" class="col-md-9">\
                                    <select class="form-control" name="payment_requestdet_detail[]" id="payment_requestdet_detail'+i+'" onchange="getDetailBpb('+i+'),checkTipe()" required></select>\
                                </div>\
                            </div>\
                        ');
                        $("#payment_requestdet_detail"+i).append(document.getElementById("payment_requestdet_detail"+(i+1)).innerHTML);
                        selectList_penerimaanBarangPembayaran('#payment_requestdet_detail'+i, document.getElementById('m_partner_id').value);
                        document.getElementById("payment_requestdet_jumlah"+i).readOnly = false;
                    } else if (document.getElementById("payment_requestdet_type"+(i+1)).value == 2) {
                        $("#td2"+i).empty();
                        $("#td2"+i).append('\
                            <div class="row">\
                                <div class="col-md-2">\
                                    <input type="hidden" value="2" id="payment_requestdet_type'+i+'" name="payment_requestdet_type[]"/>\
                                    <label class="mt-checkbox"> BPB \
                                        <input type="checkbox" value="1" id="payment_requestdet_typecheck'+i+'" onclick="checkRefresi('+i+'),checkTipe()" />\
                                        <span></span>\
                                    </label>\
                                </div>\
                                <div class="col-md-1">\
                                </div>\
                                <div id="payment_requestdet_detail_div'+i+'" class="col-md-9">\
                                    <input type="text" class="form-control" name="payment_requestdet_detail[]" id="payment_requestdet_detail'+i+'" required>\
                                </div>\
                            </div>\
                        ');
                        document.getElementById("payment_requestdet_jumlah"+i).readOnly = false;
                    }
                  };
                };
                for (var i = 1; i <= itemPo; i++) {
                  if (i==itemPo) {
                    var child = document.getElementById("detail"+i);
                    parent.removeChild(child);
                  };
                };
                itemPo--;
                $("#jml_itempo").val(itemPo);
                $('.money').number( true, 0, '.', ',' );
                sumSubTotal();
                checkTipe();
            }

            function getDetailBpb(idx) {
                $.ajax({
                    type : "GET",
                    url  : 'http://jasasoftware.co/kelapa/Gudang/Penerimaan-Barang/loadDataWhere',
                    data : "id="+document.getElementById('payment_requestdet_detail'+idx).value,
                    dataType : "json",
                    success:function(data){
                        for(var i=0; i<data.val.length;i++){
                            document.getElementById('payment_requestdet_kekurangan'+idx).value = data.val[i].penerimaan_barang_kekurangan;
                            document.getElementById('payment_requestdet_jumlah'+idx).value = data.val[i].penerimaan_barang_kekurangan;
                            $('.money').number( true, 0, '.', ',' );
                            sumSubTotal();
                        }
                    }
                });
            }

            function checkRefresi(idx) {
                document.getElementById('payment_requestdet_jumlah'+idx).value = 0;
                if (document.getElementById('payment_requestdet_typecheck'+idx).checked == true) {
                    document.getElementById('payment_requestdet_type'+idx).value = 0;
                    $('#payment_requestdet_detail_div'+idx).empty();
                    $('#payment_requestdet_detail_div'+idx).append('\
                        <select class="form-control" name="payment_requestdet_detail[]" id="payment_requestdet_detail'+idx+'" onchange="getDetailBpb('+idx+'),checkTipe()" required></select>\
                    ');
                    selectList_penerimaanBarangPembayaran('#payment_requestdet_detail'+idx, document.getElementById('m_partner_id').value);
                } else {
                    document.getElementById('payment_requestdet_type'+idx).value = 2;
                    $('#payment_requestdet_detail_div'+idx).empty();
                    $('#payment_requestdet_detail_div'+idx).append('\
                        <input type="text" class="form-control" name="payment_requestdet_detail[]" id="payment_requestdet_detail'+idx+'" required>\
                    ');
                }
            }

            function sumSubTotal() {
                subTotal        = 0;
                subTotalplus    = 0;
                subTotalmin     = 0;
                document.getElementsByName('payment_request_voucher')[0].value  = subTotalplus;
                document.getElementsByName('payment_request_discount')[0].value = subTotalmin;
                for (var i = 1; i <= itemPo; i++) {
                    nominal = parseFloat(document.getElementById('payment_requestdet_jumlah'+i).value.replace(/\,/g, ""));
                    if (nominal > 0) {
                        subTotalplus += nominal;
                    } else {
                        subTotalmin += nominal;
                    }
                    subTotal += nominal;
                }
                document.getElementsByName('payment_request_voucher')[0].value  = subTotalplus;
                document.getElementsByName('payment_request_discount')[0].value = subTotalmin;
                document.getElementById('payment_request_subtotal').value = subTotal;
                document.getElementsByName('payment_request_jumlah_bayar')[0].value = subTotal;
                $('.money').number( true, 0, '.', ',' );
                checkStatusPemasukkan();
            }

            function sumSubTotal2() {
                subTotal        = 0;
                subTotalplus    = 0;
                subTotalmin     = 0;
                document.getElementsByName('payment_request_voucher')[0].value  = subTotalplus;
                document.getElementsByName('payment_request_discount')[0].value = subTotalmin;
                for (var i = 1; i <= itemPo; i++) {
                    nominal = parseFloat(document.getElementById('payment_requestdet_jumlah'+i).value.replace(/\,/g, ""));
                    if (nominal > 0) {
                        subTotalplus += nominal;
                    } else {
                        subTotalmin += nominal;
                    }
                    subTotal += nominal;
                }
                document.getElementsByName('payment_request_voucher')[0].value  = subTotalplus;
                document.getElementsByName('payment_request_discount')[0].value = subTotalmin;
                document.getElementById('payment_request_subtotal').value = subTotal;
                document.getElementsByName('payment_request_jumlah_bayar')[0].value = subTotal;
                $('.money').number( true, 0, '.', ',' );
            }

            function checkStatusPemasukkan(){
                if (document.getElementById('payment_request_giro_checkbox').checked == true) {
                    if (document.getElementById("bank").checked == true) {
                        document.getElementById('payment_request_pengeluaran_nominal').value = 0;
                        document.getElementById('payment_request_pengeluaran_nominal').readOnly = false;
                    } else if (document.getElementById("kas").checked == true) {
                        document.getElementById('payment_request_pengeluaran_nominal').value = 0;
                        document.getElementById('payment_request_pengeluaran_nominal').readOnly = false;
                    } else if (document.getElementById("tdkada").checked == true) {
                        document.getElementById('payment_request_pengeluaran_nominal').value = 0;
                        document.getElementById('payment_request_pengeluaran_nominal').readOnly = true;
                    }
                } else {
                    document.getElementById('payment_request_pengeluaran_nominal').readOnly = true;
                    document.getElementById('payment_request_pengeluaran_nominal').value = document.getElementsByName('payment_request_jumlah_bayar')[0].value;
                }
            }

            function editData(id) {
                $.ajax({
                  type : "GET",
                  url  : 'http://jasasoftware.co/kelapa/Accounting/Payment-Request/loadDataWhere/',
                  data : "id="+id,
                  dataType : "json",
                  success:function(data){
                    for(var i=0; i<data.val.length;i++){
                        document.getElementById("submit").disabled = true;
                        document.getElementById("btnAddPo").disabled = true;
                        document.getElementsByName("kode")[0].value = data.val[i].kode;
                        document.getElementsByName("payment_request_nomor")[0].value = data.val[i].payment_request_nomor;
                        document.getElementsByName("payment_request_tanggal")[0].value = data.val[i].payment_request_tanggal;
                        document.getElementsByName("payment_request_status")[0].value = data.val[i].payment_request_status;
                        document.getElementsByName("payment_request_lampiran")[0].value = data.val[i].payment_request_lampiran;
                        document.getElementsByName("payment_request_lampiran")[0].readOnly = true;
                        $("#m_partner_id").select2('destroy');
                        for(var j=0; j<data.val[i].m_partner_id.val2.length; j++){
                        $("#m_partner_id").append('<option value="'+data.val[i].m_partner_id.val2[j].id+'" selected>'+data.val[i].m_partner_id.val2[j].text+'</option>');
                        }
                        $("#m_partner_id").select2();
                        document.getElementById("m_partner_id").disabled = true;
                        document.getElementsByName("payment_request_keperluan")[0].value = data.val[i].payment_request_keperluan;
                        document.getElementsByName("payment_request_keperluan")[0].readOnly = true;
                        document.getElementsByName("payment_request_voucher")[0].value = data.val[i].payment_request_voucher;
                        document.getElementsByName("payment_request_voucher")[0].readOnly = true;
                        document.getElementsByName("payment_request_discount")[0].value = data.val[i].payment_request_discount;
                        document.getElementsByName("payment_request_discount")[0].readOnly = true;
                        document.getElementsByName("payment_request_jumlah_bayar")[0].value = data.val[i].payment_request_jumlah_bayar;
                        if (data.val[i].payment_request_tipe_pengeluaran == 1) {
                            document.getElementById("bank").checked = true;
                            $("#pengeluaran_id").select2();
                            $("#pengeluaran_id").select2('destroy');
                            for(var j=0; j<data.val[i].pengeluaran_id.val2.length; j++){
                            $("#pengeluaran_id").append('<option value="'+data.val[i].pengeluaran_id.val2[j].id+'" selected>'+data.val[i].pengeluaran_id.val2[j].text+'</option>');
                            }
                            $("#pengeluaran_id").select2();
                            document.getElementById("pengeluaran_id").disabled = true;
                        } else if (data.val[i].payment_request_tipe_pengeluaran == 2) {
                            document.getElementById("kas").checked = true;
                            $("#pengeluaran_id").select2();
                            $("#pengeluaran_id").select2('destroy');
                            for(var j=0; j<data.val[i].pengeluaran_id.val2.length; j++){
                            $("#pengeluaran_id").append('<option value="'+data.val[i].pengeluaran_id.val2[j].id+'" selected>'+data.val[i].pengeluaran_id.val2[j].text+'</option>');
                            }
                            $("#pengeluaran_id").select2();
                            document.getElementById("pengeluaran_id").disabled = true;
                        } else if (data.val[i].payment_request_tipe_pemasukkan == 3) {
                            document.getElementById("tdkada").checked = true;
                            $("#pemasukkan_id").select2();
                            $("#pemasukkan_id").select2('destroy');
                            $("#pemasukkan_id").select2();
                            document.getElementById("pemasukkan_id").disabled = true;
                        }
                        document.getElementById("bank").disabled = true;
                        document.getElementById("kas").disabled = true;
                        document.getElementById("tdkada").disabled = true;
                        if (data.val[i].payment_request_giro == 1) {
                            document.getElementById("payment_request_giro_checkbox").checked = true;
                            $("#payment_request_giro_refrensi_id").select2();
                            $("#payment_request_giro_refrensi_id").select2('destroy');
                            for(var j=0; j<data.val[i].payment_request_giro_refrensi_id.val2.length; j++){
                            $("#payment_request_giro_refrensi_id").append('<option value="'+data.val[i].payment_request_giro_refrensi_id.val2[j].id+'" selected>'+data.val[i].payment_request_giro_refrensi_id.val2[j].text+'</option>');
                            }
                            $("#payment_request_giro_refrensi_id").select2();
                            checkNominal();
                        }
                        document.getElementsByName("payment_request_pengeluaran_nominal")[0].value = data.val[i].payment_request_pengeluaran_nominal;
                        document.getElementsByName("payment_request_pengeluaran_nominal")[0].readOnly = true;
                        document.getElementById("payment_request_giro_checkbox").disabled = true;
                        document.getElementById("payment_request_giro_refrensi_id").disabled = true;
                        $("#payment_request_nomor").attr("hidden", false);
                        $('.money').number( true, 0, '.', ',' );
                    }

                    itemPo = data.val2.length;
                    $("#jml_itempo").val(itemPo);

                    for(var i = 0; i < data.val2.length; i++){
                        if (data.val2[i].payment_requestdet_type == 0) {
                            $("#default-table tbody").append('\
                                <tr id="detail'+(i+1)+'">\
                                    <td id="td0'+(i+1)+'" class="text-center"> '+(i+1)+' </td>\
                                    <td id="td1'+(i+1)+'"> \
                                        <select class="form-control" name="m_coa_id[]" id="m_coa_id'+(i+1)+'" required disabled>\
                                            <option value="'+data.val2[i].m_coa_id.val2[0].id+'" selected>'+data.val2[i].m_coa_id.val2[0].text+'</option>\
                                        </select>\
                                    </td>\
                                    <td id="td2'+(i+1)+'"> \
                                        <div class="row">\
                                            <div class="col-md-2">\
                                                <input type="hidden" value="0" id="payment_requestdet_type'+(i+1)+'" name="payment_requestdet_type[]"/>\
                                                <label class="mt-checkbox"> BPB \
                                                    <input type="checkbox" value="1" id="payment_requestdet_typecheck'+(i+1)+'" onclick="checkRefresi('+(i+1)+'),checkTipe()" checked disabled />\
                                                    <span></span>\
                                                </label>\
                                            </div>\
                                            <div class="col-md-1">\
                                            </div>\
                                            <div id="payment_requestdet_detail_div'+(i+1)+'" class="col-md-9">\
                                                <select class="form-control" name="payment_requestdet_detail[]" id="payment_requestdet_detail'+(i+1)+'" onchange="getDetailBpb('+(i+1)+'),checkTipe()" required disabled>\
                                                    <option value="'+data.val2[i].payment_requestdet_detail.val2[i].id+'" selected>'+data.val2[i].payment_requestdet_detail.val2[i].text+'</option>\
                                                </select>\
                                            </div>\
                                        </div>\
                                    </td>\
                                    <td id="td3'+(i+1)+'"> \
                                        <div class="input-group">\
                                            <span class="input-group-addon" style="">\
                                                RP\
                                            </span>\
                                            <input type="text" class="form-control money text-right" id="payment_requestdet_kekurangan'+(i+1)+'" name="payment_requestdet_kekurangan[]" value="'+data.val2[i].payment_requestdet_kekurangan+'" onchange="sumSubTotal()" required disabled>\
                                        </div>\
                                    </td>\
                                    <td id="td4'+(i+1)+'"> \
                                        <div class="input-group">\
                                            <span class="input-group-addon" style="">\
                                                RP\
                                            </span>\
                                            <input type="text" class="form-control money text-right" id="payment_requestdet_jumlah'+(i+1)+'" name="payment_requestdet_jumlah[]" value="'+data.val2[i].payment_requestdet_jumlah+'" onchange="sumSubTotal()" required disabled>\
                                        </div>\
                                    </td>\
                                    <td id="td5'+(i+1)+'"> \
                                        <button class="btn red-thunderbird" type="button" title="Remove Detail" onclick="removeItemDetail('+(i+1)+')" disabled>\
                                            <i class="icon-close"></i>\
                                        </button>\
                                    </td>\
                                </tr>\
                            ');
                        } else if (data.val2[i].payment_requestdet_type == 1) {
                            $("#default-table tbody").append('\
                                <tr id="detail'+(i+1)+'" class="text-center">\
                                    <td id="td0'+(i+1)+'"> '+(i+1)+' </td>\
                                    <td id="td1'+(i+1)+'"> \
                                        <select class="form-control" name="m_coa_id[]" id="m_coa_id'+(i+1)+'" required disabled>\
                                            <option value="'+data.val2[i].m_coa_id.val2[0].id+'" selected>'+data.val2[i].m_coa_id.val2[0].text+'</option>\
                                        </select>\
                                    </td>\
                                    <td id="td2'+(i+1)+'"> \
                                        <div class="row">\
                                            <div class="col-md-12">\
                                                <input type="hidden" value="1" id="payment_requestdet_type'+(i+1)+'" name="payment_requestdet_type[]"/>\
                                                <input type="hidden" name="payment_requestdet_detail[]" id="payment_requestdet_detail'+(i+1)+'" value="'+data.val2[i].payment_requestdet_detail.val2[i].id+'">\
                                                <input type="text" class="form-control" name="payment_requestdet_detail_nomor[]" id="payment_requestdet_detail_nomor'+(i+1)+'" value="'+data.val2[i].payment_requestdet_detail.val2[i].text+'" required disabled>\
                                            </div>\
                                        </div>\
                                    </td>\
                                    <td id="td3'+(i+1)+'"> \
                                        <div class="input-group">\
                                            <span class="input-group-addon" style="">\
                                                RP\
                                            </span>\
                                            <input type="text" class="form-control money text-right" id="payment_requestdet_kekurangan'+(i+1)+'" name="payment_requestdet_kekurangan[]" value="'+data.val2[i].payment_requestdet_kekurangan+'" onchange="sumSubTotal()" required disabled>\
                                        </div>\
                                    </td>\
                                    <td id="td4'+(i+1)+'"> \
                                        <div class="input-group">\
                                            <span class="input-group-addon" style="">\
                                                RP\
                                            </span>\
                                            <input type="text" class="form-control money text-right" id="payment_requestdet_jumlah'+(i+1)+'" name="payment_requestdet_jumlah[]" value="'+data.val2[i].payment_requestdet_jumlah+'" onchange="sumSubTotal()" required disabled>\
                                        </div>\
                                    </td>\
                                    <td id="td5'+(i+1)+'"> \
                                        <button class="btn red-thunderbird" type="button" title="Remove Detail" onclick="removeItemDetail('+(i+1)+')" disabled>\
                                            <i class="icon-close"></i>\
                                        </button>\
                                    </td>\
                                </tr>\
                            ');
                        } else if (data.val2[i].payment_requestdet_type == 2) {
                            $("#default-table tbody").append('\
                                <tr id="detail'+(i+1)+'">\
                                    <td id="td0'+(i+1)+'" class="text-center"> '+(i+1)+' </td>\
                                    <td id="td1'+(i+1)+'"> \
                                        <select class="form-control" name="m_coa_id[]" id="m_coa_id'+(i+1)+'" required disabled>\
                                            <option value="'+data.val2[i].m_coa_id.val2[0].id+'" selected>'+data.val2[i].m_coa_id.val2[0].text+'</option>\
                                        </select>\
                                    </td>\
                                    <td id="td2'+(i+1)+'"> \
                                        <div class="row">\
                                            <div class="col-md-2">\
                                                <input type="hidden" value="2" id="payment_requestdet_type'+(i+1)+'" name="payment_requestdet_type[]"/>\
                                                <label class="mt-checkbox"> BPB \
                                                    <input type="checkbox" value="1" id="payment_requestdet_typecheck'+(i+1)+'" onclick="checkRefresi('+(i+1)+'),checkTipe()" disabled />\
                                                    <span></span>\
                                                </label>\
                                            </div>\
                                            <div class="col-md-1">\
                                            </div>\
                                            <div id="payment_requestdet_detail_div'+(i+1)+'" class="col-md-10">\
                                                <input type="text" class="form-control" name="payment_requestdet_detail[]" id="payment_requestdet_detail'+(i+1)+'" value="'+data.val2[i].payment_requestdet_detail.val2[i].text+'" required disabled>\
                                            </div>\
                                        </div>\
                                    </td>\
                                    <td id="td3'+(i+1)+'"> \
                                        <div class="input-group">\
                                            <span class="input-group-addon" style="">\
                                                RP\
                                            </span>\
                                            <input type="text" class="form-control money text-right" id="payment_requestdet_kekurangan'+(i+1)+'" name="payment_requestdet_kekurangan[]" value="'+data.val2[i].payment_requestdet_kekurangan+'" onchange="sumSubTotal()" required disabled>\
                                        </div>\
                                    </td>\
                                    <td id="td4'+(i+1)+'"> \
                                        <div class="input-group">\
                                            <span class="input-group-addon" style="">\
                                                RP\
                                            </span>\
                                            <input type="text" class="form-control money text-right" id="payment_requestdet_jumlah'+(i+1)+'" name="payment_requestdet_jumlah[]" value="'+data.val2[i].payment_requestdet_jumlah+'" onchange="sumSubTotal()" required disabled>\
                                        </div>\
                                    </td>\
                                    <td id="td5'+(i+1)+'"> \
                                        <button class="btn red-thunderbird" type="button" title="Remove Detail" onclick="removeItemDetail('+(i+1)+')" disabled>\
                                            <i class="icon-close"></i>\
                                        </button>\
                                    </td>\
                                </tr>\
                            ');
                        }
                        $('.money').number( true, 0, '.', ',' );
                    }
                    sumSubTotal2();
                  }
                });
            }
        