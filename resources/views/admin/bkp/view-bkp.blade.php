@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Cash Proof of Payment')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <form class="form-some-up" role="form" method="post" id="form" action="{{url('accountings/bkp/update')}}">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('BKP Code')}} (Auto)</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" value="{{$bkp->bkp_code}}" readonly name="bkp_code"> 
                                    </div> 
                                </div>       
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Client Contract')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <select class="selectpicker form-control" data-live-search="true" disabled name="project" id="project" onchange="change()">
                                            @foreach($project as $p)
                                                <option value="{{$p->id}}" @if($p->id===$bkp->project) selected @endif> {{$p->project}}</option>
                                            @endforeach
                                        </select>
                                    </div> 
                                </div>    
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Date')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control datePicker" readonly required value="{{get_date_format($bkp->date)}}" name="date"> 
                                    </div> 
                                </div>    
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Attachment')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" required value="{{$bkp->attachment}}" readonly name="attachment"> 
                                    </div> 
                                </div>      
                                <br> 
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Payment to')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <label class="radio-inline">
                                            <input type="radio" name="payment_to" id="payment_to1" disabled value="client_company" @if($bkp->payment_to==='client_company') checked @endif onclick="pay()">{{language_data('Client Company')}}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="payment_to" id="payment_to2" disabled value="others" @if($bkp->payment_to==='others') checked @endif onclick="pay()">{{language_data('Others')}}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="payment_to" id="payment_to3" disabled value="giro_check" @if($bkp->payment_to==='giro_check') checked @endif onclick="pay()">{{language_data('Giro/Check')}}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="payment_to" id="payment_to4" disabled value="payment" @if($bkp->payment_to==='payment') checked @endif onclick="pay()">Penggajian
                                        </label>
                                    </div> 
                                </div>       
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Paid To')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        @if($bkp->paid_to != null)
                                        <select class="selectpicker form-control" data-live-search="true" disabled name="paid_to" id="paid_to">
                                            <option value="{{$bkp->paid_to}}" selected> {{$bkp->company_info->company}}</option>
                                        </select>
                                        @else
                                        <input type="text" class="form-control" required value="{{$bkp->paid_to}}" readonly name="paid_to"> 
                                        @endif
                                    </div> 
                                </div>    
                                {{--  <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="Add" onclick="addRow2();">  --}}

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <br>
                                <table class="table table-hover table-ultra-responsive" id="myTableData2">
                                    <thead>
                                        <tr>
                                            <th class="text-center"> No </th>
                                            <th class="text-center"> 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                {{language_data('Approximate Name')}}
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                            </th>
                                            <th class="text-center"> 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                {{language_data('Description')}} 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                            </th>
                                            <th class="text-center"> 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                {{language_data('Deficiency')}} 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                            </th>
                                            <th class="text-center"> 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                {{language_data('Payment')}} 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                            </th>
                                            <th class="text-center" width="5%"> {{language_data('Action')}} </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tableTbody">

                                        @foreach($bkpdet as $det)
                                        <?php $no++ ?>
                                        <tr id="detail{{$no}}">
                                            <td id="td0{{$no}}" class="text-center"> {{$no}} </td>
                                            <td id="td1{{$no}}"> 
                                                <select class="selectpicker form-control" data-live-search="true" name="m_coa_id[]" id="m_coa_id{{$no}}" required>
                                                    <option value="{{$det->coa}}" selected> {{$det->coa_info->coa}}</option>
                                                </select> 
                                            </td>
                                            <td id="td2{{$no}}"> 
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="hidden" value="3" id="bkpdet_type{{$no}}" name="bkpdet_type[]"/>
                                                        <label class="mt-checkbox"> Pengadaan 
                                                            <input type="checkbox" value="3" name="bkpdet_typecheck2[]" id="bkpdet_typecheck21{{$no}}" onclick="checkRefresi({{$no}}),checkTipe()" @if($det->procurement_type=='3') checked @endif/>
                                                        </label>
                                                        <label class="mt-checkbox"> Gaji 
                                                            <input type="checkbox" value="4" name="bkpdet_typecheck2[]" id="bkpdet_typecheck22{{$no}}" onclick="checkRefresi({{$no}}),checkTipe()" @if($det->procurement_type=='4') checked @endif/>
                                                        </label>
                                                    </div>
                                                    <div id="bkpdet_detail_div{{$no}}" class="col-md-8">
                                                        <select class="selectpicker form-control"  data-live-search="true" name="bkpdet_detail[]" id="bkpdet_detail{{$no}}" onchange="getDetailPengadaan({{$no}}),checkTipe()" required>
                                                            @if($det->procurement_type=='3')
                                                            <option value="{{$det->procurement}}" selected> {{$det->procurement_info->procurement_number}}</option>
                                                            @elseif($det->procurement_type=='4')
                                                            <option value="{{$det->procurement}}" selected> {{$det->procurement}}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                            <td id="td3{{$no}}"> 
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            Rp.
                                                        </span>
                                                        <input type="text" class="form-control money text-right" id="bkpdet_kekurangan{{$no}}" name="bkpdet_kekurangan[]" value="{{$det->deficiency}}" required readonly>
                                                    </div>
                                                </div>
                                            </td>
                                            <td id="td4{{$no}}"> 
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            Rp.
                                                        </span>
                                                        <input type="text" class="form-control money text-right" id="bkpdet_jumlah{{$no}}" name="bkpdet_jumlah[]" value="{{$det->payment}}" onchange="sumSubTotal()" required>
                                                    </div>
                                                </div>
                                            </td>
                                            <td id="td5{{$no}}"> 
                                                <button class="btn btn-danger" type="button" title="Remove Detail" disabled onclick="deleteRow2(this)">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"> 
                                                &nbsp;<button type="button" id="btnAddPo" class="btn btn-success" disabled onclick="addPo()"><i class="fa fa-plus"></i></button>&nbsp;
                                            </th>
                                            <th colspan="3" class="text-right"> Total </th>
                                            <th>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            Rp.
                                                        </span>
                                                        <input type="text" class="form-control money text-right" id="bkp_subtotal" name="bkp_subtotal" value="0" required readonly />
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                            </th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>{{language_data('Note')}}</label>
                                    </div> 
                                    <div class="col-lg-8">
                                        <textarea class="form-control" rows="5" name="note" id="note">{{$bkp->note}}</textarea>
                                    </div> 
                                </div>  
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <br>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <table class="table table-hover table-ultra-responsive" id="myTableData3">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" colspan="2"> Kolom Pembayaran </th>
                                                    <th class="text-center" colspan="2" width="50%"> Kolom Pengeluaran </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td width="25%"> <label class="control-label">Hutang Voucher</label> </td>
                                                    <td class="text-right" width="25%">
                                                        <div class="input-group">
                                                            <span class="input-group-addon" style="">
                                                                RP
                                                            </span>
                                                            <input type="text" class="form-control money text-right" name="bkp_voucher" value="{{$bkp->voucher_payment}}" readonly>
                                                        </div>
                                                    </td>
                                                    <td class="text-left" width="25%">
                                                        <div class="mt-radio-inline">
                                                            <label class="mt-radio"> 
                                                                <input type="radio" value="1" name="bkp_pengeluaran" id="bank" @if($bkp->expenditure_type=='1') checked @endif onclick="checkTipe(1)" />Bank
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-radio"> 
                                                                <input type="radio" value="2" name="bkp_pengeluaran" id="kas" @if($bkp->expenditure_type=='2') checked @endif onclick="checkTipe(2)" />Kas
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-radio" style="visibility: hidden"> 
                                                                <input type="radio" value="3" name="bkp_pengeluaran" id="giro" @if($bkp->expenditure_type=='3') checked @endif onclick="checkTipe(3)" />Giro
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td width="25%">
                                                        <select class="form-control selectpicker"  data-live-search="true" name="pengeluaran_id" id="pengeluaran_id">
                                                            <option value="{{$bkp->expenditure_id}}" selected>@if($bkp->expenditure_type=='1') {{$bkp->bank_info->bank}} ({{$bkp->bank_info->branch}}-{{$bkp->bank_info->company_info->company}}-{{$bkp->bank_info->account_number}})
                                                            @elseif($bkp->expenditure_type=='2') 
                                                            {{$bkp->cash_info->cash}}
                                                            @endif
                                                            </option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%"> <label class="control-label">Discount / Adjst</label> </td>
                                                    <td class="text-right" width="25%">
                                                        <div class="input-group">
                                                            <span class="input-group-addon" style="">
                                                                RP
                                                            </span>
                                                            <input type="text" class="form-control money text-right" name="bkp_discount" value="{{$bkp->discount_payment}}" readonly>
                                                        </div>
                                                    </td>
                                                    <td width="25%"> <label class="control-label">Nominal Bank/Kas/Giro</label> </td>
                                                    <td class="text-right" width="25%">
                                                        <div class="input-group">
                                                            <span class="input-group-addon" style="">
                                                                RP
                                                            </span>
                                                            <input type="text" class="form-control money text-right" name="bkp_pengeluaran_nominal" id="bkp_pengeluaran_nominal" value="{{$bkp->income_nominal}}" required readonly>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%"> <label class="control-label">Jumlah Dibayarkan</label> </td>
                                                    <td class="text-right" width="25%">
                                                        <div class="input-group">
                                                            <span class="input-group-addon" style="">
                                                                RP
                                                            </span>
                                                            <input type="text" class="form-control money text-right" name="bkp_jumlah_bayar" value="{{$bkp->paid_amount}}" required readonly>
                                                        </div>
                                                    </td>
                                                    <td class="text-left" width="25%">
                                                        <input type="hidden" value="0" id="bkm_giro" name="bkm_giro"/>
                                                        <label class="mt-checkbox"> Nomor giro
                                                        </label>
                                                    </td>
                                                    <td width="25%">
                                                        <input  type="text"  class="form-control nomor_giro" name="nomor_giro" value="{{$bkp->nomor_giro}}"value=""/>            
                                                    </td>
                                                </tr>
                                                <!-- <tr>
                                                    <td width="25%"> </td>
                                                    <td class="text-right" width="25%">
                                                    </td>
                                                    <td width="25%"> <label class="control-label">Nominal Giro</label> </td>
                                                    <td class="text-right" width="25%">
                                                        <div class="input-group">
                                                            <span class="input-group-addon" style="">
                                                                RP
                                                            </span>
                                                            <input type="text" class="form-control money text-right" name="bkp_giro_nominal" id="bkp_giro_nominal" value="{{$bkp->giro}}" required readonly>
                                                        </div>
                                                    </td>
                                                </tr> -->
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row" >
                                <div class="col-md-7 "><label class="pull-right" style="font-weight: normal;">Tanggal Giro</label></div>
                                <div class="col-md-2 "></div>
                                                       <div class="col-md-3 pull-right">
                                                            <div class="form-group">
                                                            <input type="text" class="form-control datePicker"  name="tanggal_giro" value="{{get_date_format($bkp->tanggal_giro)}}"   >
                                                       </div>
                                                       </div>
                                                       </div>
                                                       <div class="row" >
                                                       <div class="col-md-7 "><label class="pull-right" style="font-weight: normal;">Keterangan Giro</label></div>
                                                       <div class="col-md-2 "></div>
                                                       <div class="col-md-3 pull-right">

                                                         <div class="input-group" style="width:100%">
                                                            
                                                            <textarea class="form-control" rows="5" name="keterangan_giro"  >{{$bkp->keterangan_giro}}</textarea>    
                                                                                                                </div></div>   </div>
                                                                                                                
                                @if($permcheck->U==1)
                                <div class="row">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="cmd" value="{{$bkp->id}}">
                                    <a type="button" class="btn btn-default pull-right" href="{{url('accountings/bkp')}}">{{language_data('Back')}}</a>
                                    <button type="submit" class="btn btn-primary pull-right">{{language_data('Save')}}</button>
                                </div> 
                                @endif 
                            </div>
                        </div>
                    </div>
                </div>
                
            </form>

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

    <script>
        function addPo() {
            generateItemDetail();
        }
        $(document).ready(function () {
            itemPo = 0 + {{$no}};
            /*For DataTable*/
            $('.data-table').DataTable();

            $('#amount').number( true, 0, '.', ',' );

            $("#amount").blur(function() {
                var total = $('#amount').val();
                console.log(total);
                 $('.alltotal').val(total).number( true, 0, '.', ',' );
            });

            sumSubTotal();
            //checkTipe({{$bkp->income_type}});
        });       

        function change(){
            $('#tableTbody').empty();
            itemPo = 0;
        }

        function pay() {
            if(document.getElementById("payment_to1").checked == true){
                $(".nomor_giro").attr("required", false);

                var _url = $("#_url").val();
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/accountings/bkp/get-client',
                    cache: false,
                    success: function ( data ) {
                        $("#paid_to").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            } else if(document.getElementById("payment_to2").checked == true){
                $(".nomor_giro").attr("required", false);

                $("#paid_to").empty();
                $("#paid_to").prop("disabled", true).selectpicker('refresh');
            } else if(document.getElementById("payment_to3").checked == true){
                $(".nomor_giro").attr("required", true);

                $("#paid_to").empty();
                $("#paid_to").prop("disabled", true).selectpicker('refresh');
            }
        }


        function generateItemDetail() {
            itemPo++;
            $('#jml_itempo').val(itemPo);
            $("#tableTbody").append('\
                <tr id="detail'+itemPo+'">\
                    <td id="td0'+itemPo+'" class="text-center"> '+itemPo+' </td>\
                    <td id="td1'+itemPo+'"> <select class="selectpicker form-control" data-live-search="true" name="m_coa_id[]" id="m_coa_id'+itemPo+'" required></select> </td>\
                    <td id="td2'+itemPo+'"> \
                        <div class="row">\
                            <div class="col-md-4">\
                                <input type="hidden" value="3" id="bkpdet_type'+itemPo+'" name="bkpdet_type[]"/>\
                                <label class="mt-checkbox"> Pengadaan \
                                    <input type="checkbox" value="3" name="bkpdet_typecheck2[]" id="bkpdet_typecheck2'+itemPo+'" onclick="checkRefresi('+itemPo+'),checkTipe()" />\
                                </label>\
                            </div>\
                            <div id="bkpdet_detail_div'+itemPo+'" class="col-md-8">\
                                <select class="selectpicker form-control" name="bkpdet_detail[]"  data-live-search="true" id="bkpdet_detail'+itemPo+'" onchange="getDetailPengadaan('+itemPo+'),checkTipe()" required></select>\
                            </div>\
                        </div>\
                    </td>\
                    <td id="td3'+itemPo+'"> \
                        <div class="form-group">\
                            <div class="input-group">\
                                <span class="input-group-addon">\
                                    Rp.\
                                </span>\
                                <input type="text" class="form-control money text-right" id="bkpdet_kekurangan'+itemPo+'" name="bkpdet_kekurangan[]" value="0" required readonly>\
                            </div>\
                        </div>\
                    </td>\
                    <td id="td4'+itemPo+'"> \
                        <div class="form-group">\
                            <div class="input-group">\
                                <span class="input-group-addon">\
                                    Rp.\
                                </span>\
                                <input type="text" class="form-control money text-right" id="bkpdet_jumlah'+itemPo+'" name="bkpdet_jumlah[]" value="0" onchange="sumSubTotal()" required>\
                            </div>\
                        </div>\
                    </td>\
                    <td id="td5'+itemPo+'"> \
                        <button class="btn btn-danger" type="button" title="Remove Detail" onclick="deleteRow2(this)">\
                            <i class="fa fa-times"></i>\
                        </button>\
                    </td>\
                </tr>\
            ');
            $('#m_coa_id'+itemPo).css('width', '100%');
            var _url = $("#_url").val();
            $.ajax
            ({
                type: "POST",
                url: _url + '/accountings/bkp/get-coa',
                cache: false,
                success: function ( data ) {
                    $("#m_coa_id"+itemPo).html( data).selectpicker('refresh');
                }
            });
            // selectList_masterCOA('#m_coa_id'+itemPo, 4);
            // selectList_penerimaanBarangPembayaran('#bkpdet_detail'+itemPo, document.getElementById('m_supplier_id').value);
            // selectList_purchaseOrderPembayaran('#bkpdet_detail'+itemPo, document.getElementById('m_supplier_id').value);
            $('#bkpdet_detail'+itemPo).css('width', '100%');
            $('.money').number( true, 0, '.', ',' );
        }
        
        function checkTipe(tipe = null) {
            if (document.getElementById("bank").checked == true) {
                tipe = 1;
            } else if (document.getElementById("kas").checked == true) {
                tipe = 2;
            } else if (document.getElementById("giro").checked == true) {
                tipe = 3;
            }

            var _url = $("#_url").val();
            //$('#pengeluaran_id').select2();
            //$('#pengeluaran_id').select2('destroy');
            //$('#pengeluaran_id').empty();
            //$('#pengeluaran_id').select2();
            if (tipe == 1) {
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/accountings/bkp/get-bank',
                    cache: false,
                    success: function ( data ) {
                        $("#pengeluaran_id").html( data).selectpicker('refresh');
                    }
                });
                
                document.getElementsByName('bkp_pengeluaran_nominal')[0].value = document.getElementsByName('bkp_jumlah_bayar')[0].value;
                
            } else if (tipe == 2) {
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/accountings/bkp/get-cash',
                    cache: false,
                    success: function ( data ) {
                        $("#pengeluaran_id").html( data).selectpicker('refresh');
                    }
                });
                
                document.getElementsByName('bkp_pengeluaran_nominal')[0].value = document.getElementsByName('bkp_jumlah_bayar')[0].value;
                
            } else if (tipe == 3) {
                //$('#pengeluaran_id').select2('destroy');
                $('#pengeluaran_id').empty();
            }
            checkGiro();
        }

        function checkGiro() {
            if (document.getElementById('bkp_giro_checkbox').checked == true) {
                document.getElementById('pengeluaran_id').required = false;
                //$('#bkp_giro_refrensi_id').select2();
                //$('#bkp_giro_refrensi_id').select2('destroy');
                //$('#bkp_giro_refrensi_id').empty();
                //$('#bkp_giro_refrensi_id').select2();
                var refrensi = [];
                var refrensi_temp = "";
                for (var i = 1; i <= itemPo; i++) {
                    if (document.getElementById('bkpdet_type'+i).value == 0 || document.getElementById('bkpdet_type'+i).value == 3) {
                        if (document.getElementById('bkpdet_typecheck'+i).checked == true || document.getElementById('bkpdet_typecheck2'+i).checked == true) {
                            refrensi_temp = document.getElementById('bkpdet_detail'+i).value;
                            refrensi.push(refrensi_temp);
                        }   
                    }
                }
                var parameter = {
                    'm_supplier_id'  : document.getElementById('m_supplier_id').value,
                    'refrensi'      : refrensi,
                };
                //select2MultipleList('#bkp_giro_refrensi_id', 'Accounting/Bukti-BG-Cek/loadDataSelect/1', 'Pilih Nomor Giro', parameter);
                document.getElementById('bkp_giro').value = 1;
            } else {
                document.getElementById('pengeluaran_id').required = true;
                //$('#bkp_giro_refrensi_id').select2();
                //$('#bkp_giro_refrensi_id').select2('destroy');
                //$('#bkp_giro_refrensi_id').empty();
                //$('#bkp_giro_refrensi_id').select2();
                var refrensi = [];
                var refrensi_temp = "";
                document.getElementById('bkp_giro').value = 0;
            }
            checkNominal();
            $('.money').number( true, 0, '.', ',' );
        }

        function checkNominal(giroValue = null) {
            if (giroValue == null) {
                var giroValue = $('#bkp_giro_refrensi_id').val();   
            }
        }
        
        function checkRefresi(idx) {
            document.getElementById('bkpdet_jumlah'+idx).value = 0;
            if (document.getElementById('bkpdet_typecheck2'+idx).checked == true) {
                // Pengadaan
                document.getElementById('bkpdet_type'+idx).value = 3;
                $('#bkpdet_detail_div'+idx).empty();
                $('#bkpdet_detail_div'+idx).append('\
                    <select class="form-control" name="bkpdet_detail[]"  data-live-search="true" id="bkpdet_detail'+idx+'" onchange="getDetailPengadaan('+idx+'),checkTipe()" required></select>\
                ');
                var _url = $("#_url").val();
                var id = $("#project").val();
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/accountings/bkp/get-expense',
                    data: {id : id},
                    cache: false,
                    success: function ( data ) {
                        $("#bkpdet_detail"+idx).html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
                //selectList_purchaseOrderPembayaran('#bkpdet_detail'+idx, document.getElementById('m_supplier_id').value);
            } else {
                $('#bkpdet_detail_div'+idx).empty();
                $("#bkpdet_detail_div"+idx).prop("disabled", true).selectpicker('refresh');

            }
        }

        
        function getDetailPengadaan(idx) {
            var _url = $("#_url").val();
            var id = document.getElementById('bkpdet_detail'+idx).value;
            // Pengadaan
            $.ajax({
                type : "POST",
                url  : _url+'/accountings/bkp/get-detail-expense',
                data : {id : id},
                dataType : "json",
                success:function(data){
                    document.getElementById('bkpdet_kekurangan'+idx).value = data.total;
                    document.getElementById('bkpdet_jumlah'+idx).value = data.total;
                    keperluan = document.getElementById("note").innerHTML;
                    keperluan += "Pemasukan Pengadaan "+ data.expense_number + ", ";
                    document.getElementById("note").innerHTML = keperluan;
                    $('.money').number( true, 0, '.', ',' );
                    sumSubTotal();
                    //for(var i=0; i<data.val.length;i++){
                        //keperluan = document.getElementsByName("bkp_keperluan")[0].innerHTML;
                        //if(parseFloat(data.val[i].order_dp) <= 0){
                            // JIKA TIDAK PUNYA DP
                            //document.getElementById('bkpdet_kekurangan'+idx).value = 0;
                            //document.getElementById('bkpdet_jumlah'+idx).value = 0;
                        //} else if(data.val[i].order_dp > 0){
                            // JIKA PUNYA DP, BAYAR DP DULU
                            //if(parseFloat(data.val[i].order_nominal_pembayaran) < parseFloat(data.val[i].order_dp)){
                                // MASIH ADA DP
                                //document.getElementById('bkpdet_kekurangan'+idx).value = data.val[i].order_total - data.val[i].order_dp;
                                //document.getElementById('bkpdet_jumlah'+idx).value = data.val[i].order_total - data.val[i].order_dp;
                                //keperluan += "DP "+ data.val[i].order_nomor;
                            //}
                        //}
                        //document.getElementsByName("bkp_keperluan")[0].innerHTML = keperluan;
                        //$('.money').number( true, 0, '.', ',' );
                    //}
                }
            });
        }

        function sumSubTotal() {
            subTotal        = 0;
            subTotalplus    = 0;
            subTotalmin     = 0;
            document.getElementsByName('bkp_voucher')[0].value  = subTotalplus;
            document.getElementsByName('bkp_discount')[0].value = subTotalmin;
            for (var i = 1; i <= itemPo; i++) {
                nominal = parseFloat(document.getElementById('bkpdet_jumlah'+i).value.replace(/\,/g, ""));
                dp = 0;
                if(document.getElementById("bkpdet_type"+i).value == 0){
                    dp = parseFloat(document.getElementById('order_pakai_dp'+i).value.replace(/\,/g, ""));
                }
                if (nominal > 0) {
                    subTotalplus += nominal;
                    subTotalplus += dp;
                } else {
                    subTotalmin += nominal;
                    subTotalmin += dp;
                }
                subTotal += nominal;
                subTotal += dp;
            }
            document.getElementsByName('bkp_voucher')[0].value  = subTotalplus;
            document.getElementsByName('bkp_discount')[0].value = subTotalmin;
            document.getElementById('bkp_subtotal').value = subTotal;
            document.getElementsByName('bkp_jumlah_bayar')[0].value = subTotal;
            document.getElementsByName('bkp_pengeluaran_nominal')[0].value = subTotal;
            $('.money').number( true, 0, '.', ',' );
            //checkStatuspengeluaran();
        }

        function total() {
            var total1 = 0;
            $('input.amount').each(function () {
                var n = parseFloat($(this).val());
                total1 += isNaN(n) ? 0 : n;
            });
            $('.alltotal').val(total1.toFixed(2)).number( true, 0, '.', ',' );
        }


        function deleteRow2(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData2");
            table.deleteRow(index);
            itemPo--;
        }
    </script>

@endsection