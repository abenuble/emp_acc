@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Proof of Cash Payment')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Proof of Cash Payment')}}</h3>
                            @if($permcheck->C==1)
                            <a href="{{url('accountings/bkp/add')}}" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add')}}</a>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 20%;">{{language_data('Number')}}</th>
                                    <th style="width: 10%;">{{language_data('Date')}}</th>
                                    <th style="width: 20%;">{{language_data('Payment to')}}</th>
                                    <th style="width: 25%;">{{language_data('Regarding')}}</th>
                                    <th style="width: 15%;">{{language_data('Action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($bkp as $b)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Number"><p>{{$b->bkp_code}}</p></td>
                                        <td data-label="Date"><span style="display:none;">{{$b->date}}</span><p>{{get_date_format($b->date)}}</p></td>
                                        @if(is_int($b->paid_to)&& $b->paid_to!=0)
                                        <td data-label="Payment to"><p>{{$b->company_info->company}}</p></td>
                                        @else
                                        <td data-label="Payment to"><p>{{$b->paid_to}}</p></td>
                                        @endif
                                        <td data-label="Regarding"><p>{{$b->note}}</p></td>
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-complete btn-xs" href="bkp/view/{{$b->id}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/accountings/bkm/delete/" + id;
                    }
                });
            });


        });
    </script>
@endsection
