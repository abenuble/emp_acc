@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Warning')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> View Warning
                                @if($warnings->status=='draft')
                                    <a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a>
                                @elseif($warnings->status=='accepted')
                                    <a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a>
                                @else
                                    <a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a>
                                @endif
                                </h3>
                            </div>
                            
                            <form class="form-some-up form-block" role="form" action="{{url('warnings/update')}}" method="post">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Warning Letter Number')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$warnings->letter_number}}" name="letter_number">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker"  required="" value="{{get_date_format($warnings->date)}}" name="date">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Letter Draft')}}</label>
                                            @if($warnings->draft_letter_info)
                                            <input type="text" class="form-control" readonly required="" value="{{$warnings->draft_letter_info->tplname}}" name="draft_letter">
                                            @else
                                            <input type="text" class="form-control" readonly required="" value="-" name="draft_letter">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Warning Type')}}</label>
                                            <select name="type" class="form-control selectpicker"  id="warning_type" >
                                            <option value='0'>Select type</option>
                                              <?php foreach($type as $t){
                                                  ?>
                                                <option @if($t== $warnings->type) selected @endif value="{{$t}}">{{$t}}</option>

                                             <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$warnings->project_info->project_number}}" name="project_number">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Company')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$warnings->company_info->company}}" name="company_name">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-none" >
                                            <label for="e20">{{language_data('Employee Code')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$warnings->employee_code}}" name="employee_code">
                                        </div>
                                    
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Employee Name')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$warnings->employee_info->fname}} {{$warnings->employee_info->lname}}" name="employee_name">
                                        </div>
                                    
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group m-none" >
                                            <label for="e20">{{language_data('Official Report Number')}}</label>
                                            <input type="text" class="form-control"  required="" value="{{$warnings->official_report_number}}" name="official_report_number">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker"  required="" value="{{get_date_format($warnings->official_report_date)}}" name="official_report_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Location')}}</label>
                                            <input type="text" class="form-control" readonly  value="{{$warnings->lokasi}}" >

                                        </div>
                                    </div>
                                    
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Violate')}}</label>
                                            <textarea type="text" rows="6" class="form-control"  required="" name="violate">{{$warnings->violate}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Effective Date')}}</label>
                                            <input type="text" class="form-control datePicker"  required="" id='start_date' value="{{get_date_format($warnings->effective_date)}}" name="effective_date">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('End Date')}}</label>
                                            <input type="text" class="form-control datePicker"  required="" id='end_date' value="{{get_date_format($warnings->end_date)}}" name="end_date">
                                        </div>
                                    </div>
                                </div>  
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Copies')}}</label>
                                            @foreach($copies as $c)
                                            <div class="container1">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" readonly value="{{$c->copies}}" name="copies[]">
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>                     
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Information')}}</label>
                                            <textarea type="text" rows="6" class="form-control"  required="" name="warning_information">{{$warnings->warning_information}}</textarea>
                                        </div>
                                    </div>
                                </div>           
                                <br>

                                
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$warnings->id}}">
                                <button type="submit" class="btn btn-sm btn-primary pull-right">{{language_data('Update')}}</button>
                            </form>
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            $('#start_date').datetimepicker().on('dp.change', function (e) {
            var incrementDay = moment(new Date(e.date));
           
            $('#end_date').data('DateTimePicker').minDate(incrementDay);
            $(this).data("DateTimePicker").hide();
        });

        $('#end_date').datetimepicker().on('dp.change', function (e) {
            var decrementDay = moment(new Date(e.date));
        
            $('#start_date').data('DateTimePicker').maxDate(decrementDay);
                $(this).data("DateTimePicker").hide();
        });
            $('.data-table').DataTable();

            /*For Location new Loading*/
            $("#project_id").ready(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + {{$warnings->project_number}};
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/warnings/get-location-new',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#location_new").html( data).selectpicker('refresh');
                    }
                });
            });

            /*For designation_new Loading*/
            $("#employee_id").ready(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + {{$warnings->employee_name}};
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/warnings/get-designation-new',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation_new").html( data).selectpicker('refresh');
                    }
                });
            });


        });

    </script>
@endsection
