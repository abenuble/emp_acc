@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Edit Contract')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> {{language_data('Edit Contract')}}
                                @if($contracts->status=='draft')
                                    <a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a>
                                @elseif($contracts->status=='accepted')
                                    <a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a>
                                @else
                                    <a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a>
                                @endif
                                </h3>
                            </div>

                            <form class="form-some-up form-block" role="form" action="{{url('contracts/update')}}" method="post">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Company')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$contracts->company_info->company}}" name="company_name">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Contract Type')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$contracts->contract_types}}" name="contract_types">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Letter Draft')}}</label>
                                            <select name="draft_letter" class="form-control selectpicker">
                                                <option value="0">{{language_data('Select Draft Letter')}}</option>
                                            @foreach($email_template as $em)
                                                <option value="{{$em->id}}" @if($contracts->draft_letter==$em->id) selected @endif>{{$em->tplname}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$contracts->project_info->project_number}}" name="project_number">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Client Contract Name')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$contracts->project_name}}" name="project_name">
                                        </div>
                                    </div>
                                </div>
                                    
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker" readonly required="" value="{{get_date_format($contracts->date)}}" name="date">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Designation')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$contracts->designation_info->designation}}" name="designation">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Location')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$contracts->location}}" name="location">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Effective Date')}}</label>
                                            <input type="text" class="form-control datePicker" readonly required="" value="{{get_date_format($contracts->effective_date)}}" name="effective_date">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('End Date')}}</label>
                                            <input type="text" class="form-control datePicker" readonly required="" value="{{get_date_format($contracts->end_date)}}" name="end_date">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Salary')}}</label>
                                            <input type="text" class="form-control" readonly name="salary" value="{{number_format($contracts->salary,0)}}" id="salary">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Payroll Type')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$contracts->payroll_info->payroll_name}}" name="payroll_types">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Contract Recipients')}}</label><br>
                                            <label class="radio-inline">
                                                <input type="radio" disabled name="share_with" id="share_with_all" value="all" class="toggle_specific" @if($contracts->share_with=='all') checked @endif>{{language_data('All Employee')}}
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" disabled name="share_with" id="share_with_specific_radio_button" value="specific" class="toggle_specific" @if($contracts->share_with=='specific') checked @endif>{{language_data('Specific Employee')}}
                                            </label>
                                            <div class="specific_dropdown" style="display: none;">
                                                <input type="text" value="" name="share_with_specific" id="share_with_specific" class="w100p validate-hidden" placeholder="choose members and or teams"  />
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>                     
                                <br>  

                            
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$contracts->id}}">
                                <button type="submit" class="btn btn-sm btn-primary pull-right">{{language_data('Update')}}</button>
                            </form>

                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> {{language_data('Contract Recipients List')}}</h3>
                            </div>
                            <div class="panel-body p-none">
                                <table class="table data-table table-hover table-ultra-responsive">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 20%;">{{language_data('Contract Number')}}</th>
                                        <th style="width: 20%;">{{language_data('Employee')}}</th>
                                        <th style="width: 20%;">{{language_data('Address')}}</th>
                                        <th style="width: 25%;">{{language_data('Date of Birth')}}</th>
                                        <th style="width: 10%;">{{language_data('Actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($recipients as $a)
                                        <tr>
                                            <td data-label="No">{{$a->id}}</td>
                                            <td data-label="Contract Number"><p>{{$a->letter_number}}</p></td>
                                            <td data-label="Employee"><p>{{$a->employee_info->fname}} {{$a->employee_info->lname}}</p></td>
                                            <td data-label="Address"><p>{{$a->employee_info->pre_address}}</p></td>
                                            <td data-label="Date of Birth"><p>{{$a->employee_info->birth_place}}, {{get_date_format($a->employee_info->dob)}}</p></td>
                                            <td data-label="Actions" class="">
                                                <a href="{{url('contracts/downloadPdf/'.$a->id)}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate PDF')}}</a><br>  
                                            </td>

                                            
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();


        });

    </script>
@endsection
