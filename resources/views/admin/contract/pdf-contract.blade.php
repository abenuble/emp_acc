<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - CONTRACT</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        @page {
            size: 21cm 29.7cm;
            margin-left: 1cm;
            margin-right: 1cm;
            font: 12pt "Tahoma";
        }
        thead {
            display: table-header-group;
        }
        tbody {
            display: table-row-group;
        }
        table {
            width: 100%;
            page-break-inside:avoid;
        }
        td, th {
            padding: 3pt;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-25 p-t-none p-b-none">
            {!!$message!!}
        </div>
    </div>
</main>
{!! Html::script("assets/libs/qrcode/qrcode.js") !!}
<script type='text/javascript'>

   
</script>

</body>
</html>