@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/select2/select2.css")!!}
    {!! Html::style("assets/css/style.css")!!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Contract')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Contract')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#contract"><i class="fa fa-plus"></i> {{language_data('Add New Contract')}}</button>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 25%;">{{language_data('Client Contract Number')}}</th>
                                    <th style="width: 30%;">{{language_data('Company')}}</th>
                                    {{-- <th style="width: 20%;">{{language_data('Payroll Type')}}</th> --}}
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php  $ctr = 0; ?>
                                @foreach($contracts as $a)
                                <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Project Number"><p>{{$a->project_info->project_number}}</p></td>
                                        <td data-label="Company"><p>{{$a->company_info->company}}</p></td>
                                        {{-- <td data-label="Payroll Type"><p>{{$a->payroll_info->payroll_name}}</p></td> --}}
                                        @if($a->status=='draft')
                                            <td data-label="status"><a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a></td>
                                        @elseif($a->status=='accepted')
                                            <td data-label="status"><a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a></td>
                                        @else
                                            <td data-label="status"><a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                        @if($a->status=='draft')
                                            @if($permcheck->U==1)
                                            <a class="btn btn-success btn-xs" href="{{url('contracts/edit/'.$a->id)}}"><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>
                                            @endif
                                            @if($permcheck->D==1)
                                            <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$a->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                            @endif
                                        @else
                                            <a class="btn btn-complete btn-xs" href="{{url('contracts/view/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                     
                                        @endif
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade" id="contract" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Contract')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('contracts/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Company')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" required name="company_name" id="company_id">
                                                <option value="0"> {{language_data('Select Company')}}</option>
                                                @foreach($company as $p)
                                                    <option value="{{$p->id}}"> {{$p->company}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Contract Type')}}</label>
                                            <select name="contract_types" required class="form-control selectpicker">
                                                {{-- <option value="PKWT1">{{language_data('Monthly PKWT')}}</option>
                                                <option value="KKHL">{{language_data('Daily PKWT')}}</option>
                                                <option value="PKWT2">{{language_data('Wholesale PKWT')}}</option> --}}
                                                <option value="PKWT">PKWT</option>
                                                <option value="PKWTT">PKWTT</option>
                                                <option value="TETAP">TETAP</option>
                                                <option value="HARIAN">HARIAN</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Letter Draft')}}</label>
                                            <select name="draft_letter" required class="form-control selectpicker">
                                            @foreach($email_template as $em)
                                                <option value="{{$em->id}}">{{$em->tplname}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $comp_id == '' ) disabled @endif required name="project_number" id="project_id">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Client Contract Name')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $proj_id == '' ) disabled @endif required name="project_name" id="project_name">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker" required="" name="date">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Designation')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $proj_id == '' ) disabled @endif required name="designation" id="designation">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Location')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $proj_id == '' ) disabled @endif required name="location" id="location">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Effective Date')}}</label>
                                            <input type="text" class="form-control datePicker" required="" name="effective_date">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('End Date')}}</label>
                                            <input type="text" class="form-control datePicker" required="" name="end_date">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Salary')}}</label>
                                            <input type="text" class="form-control" readonly name="salary" id="salary">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group m-none">
                                            <label for="e20"> {{language_data('Payroll Type')}}</label>
                                            <select name="payroll_types" class="form-control selectpicker" required id="payroll_id">
                                                <option value="0"> {{language_data('Select Payroll Type')}}</option>
                                            @foreach($payroll_types as $pay)
                                                <option value="{{$pay->id}}">{{$pay->payroll_name}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Contract Recipients')}}</label><br>
                                            <label class="radio-inline">
                                                <input type="radio" name="share_with" id="share_with_all" required value="all" class="toggle_specific">{{language_data('All Employee')}}
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="share_with" id="share_with_specific_radio_button" required value="specific" class="toggle_specific">{{language_data('Specific Employee')}}
                                            </label>
                                            <div class="specific_dropdown" style="display: none;">
                                                <input type="text" required value="" name="share_with_specific" id="share_with_specific" class="w100p validate-hidden" placeholder="choose members and or teams"  />
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>                     
                                
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/select2/select2.js")!!}

    <script>
        $(document).ready(function () {

            /*For DataTable*/
            $('.data-table').DataTable();


            /*For Project Loading*/
            $("#company_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'comp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/contracts/get-project',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#project_id").html( data).removeAttr('disabled').selectpicker('refresh');

                        
                        var id = $('#project_id').val();
                        var _url = $("#_url").val();
                        var dataString = 'proj_id=' + id;
                        $.ajax
                        ({
                            type: "POST",
                            url: _url + '/contracts/get-project-name',
                            data: dataString,
                            cache: false,
                            success: function ( data ) {
                                $("#project_name").html( data).removeAttr('disabled').selectpicker('refresh');
                            }
                        });
                        $.ajax
                        ({
                            type: "POST",
                            url: _url + '/contracts/get-designation',
                            data: dataString,
                            cache: false,
                            success: function ( data ) {
                                $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                            }
                        });
                        $.ajax
                        ({
                            type: "POST",
                            url: _url + '/contracts/get-location',
                            data: dataString,
                            cache: false,
                            success: function ( data ) {
                                $("#location").html( data).removeAttr('disabled').selectpicker('refresh');
                            }
                        });
                        $.ajax
                        ({
                            type: "POST",
                            url: _url + '/contracts/get-employee',
                            data: dataString,
                            cache: false,
                            success: function ( data ) {
                                
                                $("#share_with_specific").select2({
                                    tags: true,
                                    tokenSeparators: [',', ' '],
                                    multiple: true,
                                    data: data
                                });
                            }
                        });
                    }
                });
            });


            /*For Project Name Loading*/
            $("#project_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/contracts/get-project-name',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#project_name").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });


            /*For Project Name Loading*/
            $("#project_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/contracts/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });


            /*For Project Name Loading*/
            $("#project_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/contracts/get-location',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#location").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For Project Name Loading*/
            $("#payroll_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'payroll_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/contracts/get-salary',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#salary").val( data);
                    }
                });
            });

            /*For employee Name Loading*/
            $("#project_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/contracts/get-employee',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        
                        $("#share_with_specific").select2({
                            tags: true,
                            tokenSeparators: [',', ' '],
                            multiple: true,
                            data: data
                        });
                    }
                });
            });

            $(".toggle_specific").click(function () {
                toggle_specific_dropdown();
            });
            toggle_specific_dropdown();
        
            function toggle_specific_dropdown() {
                var $element = $(".toggle_specific:checked");
                if ($element.val() === "specific") {
                    $(".specific_dropdown").find("input").prop('required',false);
                    $(".specific_dropdown").show().find("input").addClass("form-control").addClass("validate-hidden");
                } else {
                   
                    $(".specific_dropdown").find("input").prop('required',false);
                    $(".specific_dropdown").hide().find("input").removeClass("form-control").removeClass("validate-hidden");
                }
            }


            
        });
        $(".cdelete").click(function (e) {
            e.preventDefault();
            var id = this.id;
            bootbox.confirm("Are you sure?", function (result) {
                if (result) {
                    var _url = $("#_url").val();
                    window.location.href = _url + "/contracts/delete/" + id;
                }
            });
        });

    </script>
@endsection
