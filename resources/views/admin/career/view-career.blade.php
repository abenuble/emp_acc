@extends('master')
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Career Detail')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
        <form class="form-some-up" name="form" role="form" method="post" action="{{url('careers/update')}}" enctype="multipart/form-data">

            @include('notification.notify')

            {{--View Detail Career--}}
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel-body">
                        <div class="row">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{language_data('Division')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$career->department_info->department}}" name="">
                                    </div>
                                </div>
                                        
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>{{language_data('Designation')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$career->designation_info->designation}}" name="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{language_data('Minimum Working Period')}}</label>
                                        <input type="number" class="form-control" required="" value="{{$career->working_period}}" name="working_period">
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>{{language_data('Minimum Education')}}</label>
                                        <select class="selectpicker form-control" required="" data-live-search="true" name="education">
                                            <option value="3" @if($career->education=='3') selected @endif>SMA/SMK</option>
                                            <option value="5" @if($career->education=='5') selected @endif>D3</option>
                                            <option value="7" @if($career->education=='7') selected @endif>S1</option>
                                            <option value="8" @if($career->education=='8') selected @endif>S2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">                          
                                    <div class="form-group">
                                        <label>{{language_data('Status')}} : </label>
                                        <select class="selectpicker form-control" data-live-search="true" name="status">
                                            <option value="inactive" @if($career->status=='inactive') selected @endif >{{language_data('Inactive')}}</option>
                                            <option value="active" @if($career->status=='active') selected @endif >{{language_data('Active')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            

                        </div>
                    </div>
                </div>
            </div>    

             {{--View Competences--}}
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Competences')}}</h3>
                            <div id="myform">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" name="" id="competence" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select name="assessment_type" class="form-control selectpicker" id="assessment_type" data-live-search="true">
                                                <option value="Manual">{{language_data('Manual')}}</option>
                                                <option value="With Proof">{{language_data('With Proof')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="{{language_data('Add')}}" onclick="Javascript:addRow()">
                            </div><br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive" id="myTableData">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">No</th>                                        
                                        <th style="width: 40%;">{{language_data('Competence')}}</th>
                                        <th style="width: 40%;">{{language_data('Assessment Type')}}</th>
                                        <th style="width: 10%;">{{language_data('Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($competence as $c)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Competence">{{$c->competence}}</td>
                                        <td data-label="Assessment Type">{{$c->assessment_type}}</td>
                                        <td data-label="Action">
                                            <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$c->id}}">{{language_data('Delete')}}</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <input type="hidden" id="no" value="<?php echo $ctr; ?>">
                            </table>
                            @if($permcheck->U==1)
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="cmd" value="{{$career->id}}">
                            <button type="submit" class="btn btn-primary btn-xs pull-right" style="margin-right:10px">{{language_data('Update')}}</button><br><br>
                            @endif
                        </div>
                    
                       
                    
                   </div>
                    
                </div>
                
            </div>
        
        
   
        </form>    
        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    <script>
        $(document).ready(function () {
            var no = $("#no").val();

            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/careers/competence-delete/" + id;
                    }
                });
            });

            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/careers/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });
        });

        function addRow() {
            var no = $("#no").val();
            no++;
            $("#no").val(no);
            var competence = document.getElementById("competence");
            var assessment_type = document.getElementById('assessment_type');
            
            var table = document.getElementById("myTableData");

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);
            var addNo = rowCount;
            
            row.insertCell(0).innerHTML= no;
            row.insertCell(1).innerHTML= '<input type="text" class="form-control" value="'+competence.value+'" name="competence[]">';
            row.insertCell(2).innerHTML= '<input type="text" class="form-control" value="'+assessment_type.value+'" name="assessment_type[]">';
            row.insertCell(3).innerHTML= '<input class="btn btn-danger btn-xs" type="button" value ="{{language_data("Delete")}}" onClick="Javacsript:deleteRow(this)">';

        }
        

        function deleteRow(obj) {
            var no = $("#no").val();
            no--;
            $("#no").val(no);
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData");
            table.deleteRow(index);
        
        }
    </script>
@endsection
