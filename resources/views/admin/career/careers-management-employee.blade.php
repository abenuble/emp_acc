@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Career Path')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Search Condition')}}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="" role="form" method="post" action="{{url('careers/management-employee/post-custom-search')}}">

                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Company')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="company" id="company">
                                                <option value="0">{{language_data('Select Company')}}</option>
                                                @foreach($company as $p)
                                                    <option value="{{$p->id}}" @if($p->id==$comp_id) selected @endif> {{$p->company}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Division')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="department" id="department_id">
                                                <option value="0">{{language_data('Select Division')}}</option>
                                                @foreach($department as $d)
                                                <option value="{{$d->id}}" @if($d->id==$dep_id) selected @endif> {{$d->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Designation')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $des_id == '' ) disabled @endif name="designation" id="designation">
                                                <option value="0">{{language_data('Select Designation')}}</option>
                                                @if($des_id!=0)
                                                @foreach($designation as $des)
                                                    <option value="{{$des->id}}" @if($des_id==$des->id) selected @endif> {{$des->designation}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-search"></i> {{language_data('Search')}}</button>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Career Path')}}</h3>         
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 20%;">{{language_data('Employee Name')}}</th>
                                    <th style="width: 20%;">{{language_data('Designation')}}</th>
                                    <th style="width: 20%;">{{language_data('Career Path')}}</th>
                                    <th style="width: 25%;">{{language_data('Progress')}}</th>
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                <?php $ctr = 0; ?>
                                @foreach($employee as $e)
                                <?php $ctr++; ?>
                                <tr>
                                    <td data-label="No">{{$ctr}}</td>
                                    <td data-label="Employee Name">
                                        @if($employee_permission->R==1) <a href="{{url('employees/view/'.$e->id)}}">{{$e->fname}} {{$e->lname}}</a> @else {{$e->fname}} {{$e->lname}} @endif
                                    </td>
                                    <td data-label="Designation">{{$e->designation_name->designation}}</td>
                                    @if($e->designation_name->career_info!='')
                                    <td data-label="Career Path">{{$e->designation_name->career_info->designation}}</td>
                                    @else
                                    <td data-label="Career Path"></td>
                                    @endif
                                    <td data-label="Progress">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{$e->career_path_progress}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$e->career_path_progress}}%">
                                            {{$e->career_path_progress}}%
                                            </div>
                                        </div>
                                    </td>
                                    <td data-label="Actions">


                                        <div class="btn-group btn-mini-group dropdown-default">
                                            <a class="btn btn-complete btn-xs" href="{{url('careers/view-employee/'.$e->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </div>

                                    </td>
                                </tr>
                                
                                @endforeach
                                

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>




        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/js/phoneno-+international-format.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/careers/delete/" + id;
                    }
                });
            });

            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/careers/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });


        });

        function addRow() {
         
            var competence = document.getElementById("competence");
            var assessment_type = document.getElementById('assessment_type');
            
            var table = document.getElementById("myTableData");

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);
            
            row.insertCell(0).innerHTML= '<input type="text" class="form-control" value="'+competence.value+'" name="competence[]">';
            row.insertCell(1).innerHTML= '<input type="text" class="form-control" value="'+assessment_type.value+'" name="assessment_type[]">';
            row.insertCell(2).innerHTML= '<input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';

        }
        

        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData");
            table.deleteRow(index);
        
        }
    </script>
@endsection
