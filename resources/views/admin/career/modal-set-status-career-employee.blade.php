<div class="modal fade modal_set_status_assessment{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{language_data('Change Status')}}</h4>
            </div>
            <form class="form-some-up form-block" role="form" action="{{url('careers/set-career-assessment-status')}}" method="post">

                <div class="modal-body">

                    <div class="form-group">
                        <label>{{language_data('Status')}} : </label>
                        <select class="selectpicker form-control" data-live-search="true" name="status">
                            <option value="Pass" @if($c->status=='Pass') selected @endif >{{language_data('Qualify')}}</option>
                            <option value="Not Pass" @if($c->status=='Not Pass') selected @endif >{{language_data('Unqualify')}}</option>
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="cmd" value="{{$c->id}}">
                    <input type="hidden" name="emp_id" value="{{$c->emp_id}}">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                    <button type="submit" class="btn btn-primary">{{language_data('Update')}}</button>
                </div>

            </form>
        </div>
    </div>

</div>

