@extends('master')
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Career Detail')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

            {{--View Detail Career--}}
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel-body">
                        <div class="row text-center">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{language_data('Employee Code')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$employee->employee_code}}" name="">
                                    </div>
                                </div>
                                        
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>{{language_data('Employee Name')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$employee->fname}}" name="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>{{language_data('Designation Currently')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$employee->designation_name->designation}}" name="">
                                    </div>
                                </div>
                                        
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>{{language_data('Career Path')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$employee->designation_name->career_info->designation}}" name="">
                                    </div>
                                </div>
                            </div>         

                        </div>
                    </div>
                </div>
            </div>    

             {{--View Competences--}}
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Competences')}}</h3>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table table-hover table-ultra-responsive" id="myTableData">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>                                        
                                        <th style="width: 65%;">{{language_data('Competence')}}</th>
                                        <th style="width: 20%;">{{language_data('Upload File')}}</th>
                                        <th style="width: 10%;">{{language_data('Qualification')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                            
                                @foreach($competence as $c)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Competence">{{$c->competence_info->competence}}</td>
                                        @if($c->competence_info->assessment_type=='With Proof' && $c->status=='Pass')
                                            <td data-label="Upload File">
                                                <a href="{{url('careers/download-assessment-document/'.$c->id)}}">{{$c->proof_file}}</a>
                                            </td>
                                        @elseif($c->competence_info->assessment_type=='With Proof' && $c->status=='Not Pass')
                                            <td data-label="Upload File"><a><button class="btn btn-primary btn-sm" data-toggle="modal" data-target=".modal_upload{{$c->id}}">{{language_data('Upload')}}</button></a></td>
                                            @include('admin.career.modal-upload-proof')
                                        @else
                                            <td data-label="Upload File"></td>
                                        @endif
                                        @if($c->status=='Pass')
                                            <td data-label="Qualification"><a class="btn btn-success btn-xs" @if($permcheck->U==1) data-toggle="modal" data-target=".modal_set_status_assessment{{$c->id}}" @endif>{{language_data('Qualify')}}</a></td>
                                            @include('admin.career.modal-set-status-career-employee')
                                        @else
                                            <td data-label="Qualification"><a class="btn btn-danger btn-xs" @if($permcheck->U==1) data-toggle="modal" data-target=".modal_set_status_assessment{{$c->id}}" @endif>{{language_data('Unqualify')}}</a></td>
                                            @include('admin.career.modal-set-status-career-employee')
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        
        
        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/careers/delete/" + id;
                    }
                });
            });


        });

    </script>
@endsection
