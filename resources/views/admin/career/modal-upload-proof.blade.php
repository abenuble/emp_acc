<div class="modal fade modal_upload{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{language_data('Change Status')}}</h4>
            </div>
            <form class="form-some-up form-block" role="form" action="{{url('careers/upload-assessment-document')}}" method="post" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="form-group">
                        <label>{{language_data('Select file')}}</label>
                        <div class="input-group input-group-file">
                            <span class="input-group-btn">
                                <span class="btn btn-primary btn-file">
                                    {{language_data('Browse')}} <input type="file" class="form-control" name="file"/>
                                </span>
                            </span>
                        <input type="text" class="form-control" readonly="">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="cmd" value="{{$c->id}}">
                    <input type="hidden" name="emp_id" value="{{$c->emp_id}}">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                    <button type="submit" class="btn btn-primary">{{language_data('Update')}}</button>
                </div>

            </form>
        </div>
    </div>

</div>

