@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Careers')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Careers')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#careers"><i class="fa fa-plus"></i> {{language_data('Add New Career')}}</button>
                            @endif
                            {{--  <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload_competent">Upload Competent</button></a><br>                  --}}
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 30%;">{{language_data('Division')}}</th>
                                    <th style="width: 30%;">{{language_data('Designation')}}</th>
                                    <th style="width: 15%;">{{language_data('Status')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($careers as $c)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Department"><p>{{$c->department_info->department}}</p></td>
                                        <td data-label="Designation"><p>{{$c->designation_info->designation}}</p></td>
                                        @if($c->status=='active')
                                            <td data-label="status"><a class="btn btn-complete btn-xs">{{language_data('Active')}}</a></td>
                                        @elseif($c->status=='inactive')
                                            <td data-label="status"><a class="btn btn-danger btn-xs">{{language_data('Inactive')}}</a></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-complete btn-xs" href="{{url('careers/view/'.$c->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade" id="careers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Career')}}</h4>
                        </div>
                        <form class="form-some-up" name="form" role="form" method="post" action="{{url('careers/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{language_data('Division')}}</label>
                                            <select name="department" class="form-control selectpicker" id="department_id" data-live-search="true">
                                                    <option value="0">{{language_data('Select Division')}}</option>
                                                @foreach($department as $d)
                                                    <option value="{{$d->id}}">{{$d->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>{{language_data('Designation')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $des_id == '' ) disabled @endif  name="designation" id="designation">
                                                    <option value="0">{{language_data('Select Designation')}}</option>
                                                @if($des_id!=0)
                                                @foreach($designation as $d)
                                                    <option value='{{$d->id}}' @if($des_id==$designation->id) selected @endif>{{$d->designation}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{language_data('Minimum Working Period')}}</label>
                                            <input type="number" class="form-control" required="" name="working_period">
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>{{language_data('Minimum Education')}}</label>
                                            <select class="selectpicker form-control" required="" data-live-search="true" name="education">
                                                <option value="3">SMA/SMK</option>
                                                <option value="5">D3</option>
                                                <option value="7">S1</option>
                                                <option value="8">S2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div id="myform">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{language_data('Competence')}}</label>
                                                <input type="text" class="form-control" name="" id="competence" value="Competence">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{language_data('Assessment Type')}}</label>
                                                <select name="assessment_type" class="form-control selectpicker" id="assessment_type" data-live-search="true">
                                                    <option value="Manual">{{language_data('Manual')}}</option>
                                                    <option value="With Proof">{{language_data('With Proof')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="Add" onclick="Javascript:addRow()">
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata">
                                                <table class="table table-hover table-ultra-responsive" id="myTableData"  >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 60%;">{{language_data('Competence')}}</th>
                                                            <th style="width: 30%;">{{language_data('Assessment Type')}}</th>
                                                            <th style="width: 10%;">{{language_data('Action')}}</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                    
                                    

                                </div>

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Upload -->
            <div class="modal fade" id="upload_competent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('careers/importExcelCompetent') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
 
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('careers/downloadExcel/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/js/phoneno-+international-format.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/careers/delete/" + id;
                    }
                });
            });

            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/careers/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });


        });

        function addRow() {
         
            var competence = document.getElementById("competence");
            var assessment_type = document.getElementById('assessment_type');
            
            var table = document.getElementById("myTableData");

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);
            
            row.insertCell(0).innerHTML= '<input type="text" class="form-control" value="'+competence.value+'" name="competence[]">';
            row.insertCell(1).innerHTML= '<input type="text" class="form-control"  readonly value="'+assessment_type.value+'" name="assessment_type[]">';
            row.insertCell(2).innerHTML= '<input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';

        }
        

        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData");
            table.deleteRow(index);
        
        }
    </script>
@endsection
