@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Payroll Detail')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

            {{--View Detail Company--}}
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">

                            <form class="form-some-up form-block" role="form" action="{{url('payroll/edit-payroll-post')}}" method="post"><br>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>{{language_data('Company')}}</label>
                                        <select name="company" class="form-control selectpicker w100p validate-hidden" data-live-search="true">
                                                <option value='0'>{{language_data('Select Company')}}</option>
                                            @foreach($company as $d)
                                                <option value='{{$d->id}}'  @if($payroll_types->company==$d->id) selected @endif >{{$d->company}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{language_data('Designation')}}</label>
                                        <select name="designation" class="form-control selectpicker w100p validate-hidden" data-live-search="true">
                                                <option value='0'>{{language_data('Select Designation')}}</option>
                                            @foreach($designation as $d)
                                                <option value='{{$d->id}}'  @if($payroll_types->designation==$d->id) selected @endif>{{$d->designation}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Payroll Name')}}</label>
                                        <input type="text" class="form-control" required="" name="payroll_name" value="{{$payroll_types->payroll_name}}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Status')}} : </label>
                                        <select class="selectpicker form-control" data-live-search="true" name="status">
                                            <option value="inactive" @if($payroll_types->status=='inactive') selected @endif >{{language_data('Inactive')}}</option>
                                            <option value="active" @if($payroll_types->status=='active') selected @endif >{{language_data('Active')}}</option>
                                        </select>
                                    </div>
                                    <div id="myform">
                                        <div class="form-group">
                                            <label>{{language_data('Components')}}</label>
                                            <select name="" class="form-control selectpicker" data-live-search="true" id="component">
                                                @foreach($components as $comp)
                                                    <option value='{{$comp->id}}'>{{$comp->component}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Unit Components')}}</label>
                                            <select name="" class="form-control selectpicker" data-live-search="true" id="unit">
                                                @foreach($units as $u)
                                                    <option value='{{$u->id}}'>{{$u->unit}}</option>
                                                @endforeach
                                            </select>
                                        </div>
    
                                        <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="Add" onclick="Javascript:addRow()"><br>
                                    </div>
    
                                </div>

                                {{--View Detail Components--}}
                                <div class="panel-body">
                                    <table class="table data-table table-hover table-ultra-responsive" id="default-table">
                                        <thead>
                                        <tr>
                                            <th style="width: 5%;">No</th>
                                            <th style="width: 30%;">{{language_data('Component Name')}}</th>
                                            <th style="width: 10%;">{{language_data('Unit Component')}}</th>
                                            <th style="width: 20%;">{{language_data('Unit Nominal')}}</th>
                                            <th style="width: 10%;">{{language_data('Converter')}}</th>
                                            <th style="width: 20%;">{{language_data('Nominal')}}</th>
                                            <th style="width: 5%;">{{language_data('Action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody id="myTable">
                                            <?php $ctr = 0; ?>
                                        @foreach($payroll_component as $p)
                                            @if($p->component_name->status=='active')
                                            <?php $ctr++; ?>
                                                <tr>
                                                    <td data-label="No">{{$ctr}}</td>
                                                    <td data-label="Component Name"><input type="hidden" class="form-control" value="{{$p->component_name->id}}" name="component[]"><input type="text" readonly class="form-control" value="{{$p->component_name->component}}"></td>
                                                    <td data-label="Unit Component"><input type="hidden" class="form-control" value="{{$p->unit_name->id}}" name="unit[]"><input type="text" readonly class="form-control" value="{{$p->unit_name->unit}}"></td>
                                                    <td data-label="Unit Nominal"><input type="text" name="unit_nominal[]" class="form-control num unit" value="{{number_format($p->unit_nominal,0)}}"></td>          
                                                    <td data-label="Converter"><input type="text" name="converter[]" class="form-control num converter" value="{{number_format($p->converter,0)}}"></td>          
                                                    <td data-label="Nominal"><input type="text" name="value[]" class="form-control num nominal" value="{{number_format($p->value,0)}}"></td>          
                                                    <td data-label="Action"><input class="btn btn-danger btn-xs" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)"></td>                                       
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5"><p class="pull-right">Total : </p></td>
                                                <td><input type="text" name="total" class="form-control num total" value="0"></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$payroll_types->id}}">
                                <button type="submit" class="btn btn-primary btn-sm pull-right">{{language_data('Update')}}</button>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            


            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}
    <script>
        $(document).ready(function(){
            $('.data-table').DataTable();
            $('.num').number( true, 0);
            $(".num").css('text-align', 'right');
            $(".num").css('width', '150px');
            Subtotal();
            $('#default-table').on("keyup",".nominal", function() {
                Subtotal();
            });
            $('#default-table').on("keyup",".unit, .converter", function() {
                var unit = parseFloat($(this).parents("tr").find(".unit").val());
                var converter = parseFloat($(this).parents("tr").find(".converter").val());
                var nominal = unit * converter;
                console.info(unit, converter, nominal);
                if(!isNaN(nominal)){
                    $(this).parents("tr").find(".nominal").val(nominal ).number( true, 0);
                    Subtotal();
                }
            });
        });

        function Subtotal() {
            var total = 0;
            $('input.nominal').each(function () {
                var n = parseFloat($(this).val());
                total += isNaN(n) ? 0 : n;
            });
            $('.total').val(total).number( true, 0);

            $('.num').number( true, 0);
            $('.num').css('text-align', 'right');
        }
        function getSelectedText(elementId) {
            var elt = document.getElementById(elementId);

            if (elt.selectedIndex == -1)
                return null;

            return elt.options[elt.selectedIndex].text;
        }

        function addRow() {
         
            var component = document.getElementById("component");
            var unit = document.getElementById("unit");
            var textcomp = getSelectedText('component');
            var textunit = getSelectedText('unit');
            var table = document.getElementById("myTable");

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);
            var converter = 0;

            if(textunit=='Harian'){
                converter = 20;
            }
            if(textunit=='Mingguan'){
                converter = 4;
            }
            if(textunit=='Bulanan'){
                converter = 1;
            }

            row.insertCell(0).innerHTML= rowCount+1;
            row.insertCell(1).innerHTML= '<input type="hidden" class="form-control" value="'+component.value+'" name="component[]"><input type="text" readonly class="form-control" value="'+textcomp+'">';
            row.insertCell(2).innerHTML= '<input type="hidden" class="form-control" value="'+unit.value+'" name="unit[]"><input type="text" readonly class="form-control" value="'+textunit+'">';
            row.insertCell(3).innerHTML= '<input type="text" name="unit_nominal[]" class="form-control num unit" value="0">';
            row.insertCell(4).innerHTML= '<input type="text" name="converter[]" class="form-control num converter" value="'+converter+'">';
            row.insertCell(5).innerHTML= '<input type="text" name="value[]" class="form-control num nominal" value="0">';
            row.insertCell(6).innerHTML= '<input class="btn btn-danger btn-xs" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';

            $('.num').number( true, 0);
            $(".num").css('text-align', 'right');
            $(".num").css('width', '150px');
        }

        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("default-table");
            table.deleteRow(index);
        
            Subtotal();
        }   
    </script>
@endsection
