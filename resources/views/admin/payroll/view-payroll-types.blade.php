@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Payroll Detail')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')

            {{--View Detail Company--}}
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">

                            <form class="form-some-up form-block" role="form" action="{{url('payroll/set-payroll-status')}}" method="post"><br>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>{{language_data('Company')}}</label>
                                        <select name="company" class="form-control selectpicker w100p validate-hidden" disabled data-live-search="true">
                                                <option value='0'>{{language_data('Select Company')}}</option>
                                            @foreach($company as $d)
                                                <option value='{{$d->id}}'  @if($payroll_types->company==$d->id) selected @endif >{{$d->company}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{language_data('Designation')}}</label>
                                        <select name="designation" class="form-control selectpicker w100p validate-hidden" disabled data-live-search="true">
                                                <option value='0'>{{language_data('Select Designation')}}</option>
                                            @foreach($designation as $d)
                                                <option value='{{$d->id}}'  @if($payroll_types->designation==$d->id) selected @endif>{{$d->designation}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{language_data('Payroll Name')}}</label>
                                        <input type="text" class="form-control" required="" name="payroll_name"  readonly value="{{$payroll_types->payroll_name}}">
                                    </div>

                                </div>

                                {{--View Detail Components--}}
                                <div class="panel-body">
                                    <table class="table data-table table-hover table-ultra-responsive">
                                        <thead>
                                        <tr>
                                            <th style="width: 5%;">No</th>
                                            <th style="width: 30%;">{{language_data('Component Name')}}</th>
                                            <th style="width: 10%;">{{language_data('Unit Component')}}</th>
                                            <th style="width: 20%;">{{language_data('Unit Nominal')}}</th>
                                            <th style="width: 10%;">{{language_data('Converter')}}</th>
                                            <th style="width: 20%;">{{language_data('Nominal')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php $ctr = 0; ?>
                                        @foreach($payroll_component as $p)
                                            @if($p->component_name->status=='active')
                                            <?php $ctr++; ?>
                                            <tr>
                                                <td data-label="No">{{$ctr}}</td>
                                                    <td data-label="Component Name"><p>{{$p->component_name->component}}</p></td>
                                                    <td data-label="Unit Component"><p>{{$p->unit_name->unit}}</p></td>
                                                    <td data-label="Unit Nominal"><p class="pull-right">{{number_format($p->unit_nominal,0)}}</p></td>          
                                                    <td data-label="Converter"><p class="pull-right">{{number_format($p->converter,0)}}</p></td>    
                                                    <td data-label="Nominal"><p class="pull-right">Rp. {{number_format($p->value,0)}}</p></td>                                       
                                                </tr>
                                            @endif
                                        @endforeach
                                            <tr>
                                                <td colspan="5"><p class="pull-right">Total : </p></td>
                                                <td><p class="pull-right">Rp. {{number_format($payroll_types->total,0)}}</p></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$payroll_types->id}}">
                                {{-- <button type="submit" class="btn btn-primary btn-sm pull-right">{{language_data('Update')}}</button> --}}
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            


            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    <script>
        $(document).ready(function () {
        });       
    </script>
@endsection
