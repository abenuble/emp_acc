@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Payroll Types')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Payroll Types')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#payroll_type"><i class="fa fa-plus"></i> {{language_data('Add New Payroll')}}</button>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 25%;">{{language_data('Payroll')}}</th>
                                    <th style="width: 15%;">Total</th>
                                    <th style="width: 20%;">{{language_data('Company')}}</th>
                                    <th style="width: 25%;">{{language_data('Designation')}}</th>
                                    <th style="width: 5%;">{{language_data('Status')}}</th>
                                    <th style="width: 5%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($payroll_types as $proll)
                                <?php $ctr++; ?>
                                <tr>
                                    <td data-label="No">{{$ctr}}</td>
                                    <td data-label="Payroll">{{$proll->payroll_name}}</td>
                                    <td data-label="Total">Rp. {{number_format($proll->total,0)}}</td>
                                    <td data-label="Company">{{$proll->company_info->company}}</td>
                                    <td data-label="Designation">{{$proll->designation_info->designation}}</td>
                                    @if($proll->status=='active')
                                        <td data-label="status"><p class="btn btn-complete btn-xs">{{language_data('Active')}}</p></td>
                                    @elseif($proll->status=='inactive')
                                        <td data-label="status"><p class="btn btn-danger btn-xs">{{language_data('Inactive')}}</p></td>
                                    @endif
                                    <td data-label="Actions">
                                        <div class="btn-group btn-mini-group dropdown-default">
                                            <a class="btn btn-success btn-sm dropdown-toggle btn-animated from-top fa fa-caret-down" data-toggle="dropdown" href="#" aria-expanded="false"><span><i class="fa fa-bars"></i></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{url('payroll/view-payroll-types/'.$proll->id)}}" data-toggle="tooltip" data-placement="right" title="{{language_data('View')}}" class="text-success"><i class="fa fa-eye"></i></a></li>
                                                <li><a href="{{url('payroll/edit-payroll-types/'.$proll->id)}}" data-toggle="tooltip" data-placement="right" title="{{language_data('Edit')}}" class="text-success"><i class="fa fa-edit"></i></a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Modal -->
            <div class="modal fade" id="payroll_type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Payroll')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('payroll/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <select name="company" class="form-control selectpicker w100p validate-hidden" data-live-search="true">
                                            <option value='0'>{{language_data('Select Company')}}</option>
                                        @foreach($company as $d)
                                            <option value='{{$d->id}}' >{{$d->company}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{{language_data('Designation')}}</label>
                                    <select name="designation" class="form-control selectpicker w100p validate-hidden" data-live-search="true">
                                            <option value='0'>{{language_data('Select Designation')}}</option>
                                        @foreach($designation as $d)
                                            <option value='{{$d->id}}' >{{$d->designation}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Payroll Type')}}</label>
                                    <input type="text" class="form-control" required="" value="" name="payroll_name">
                                </div>

                                <div class="form-group m-none">
                                    <label for="e20">{{language_data('Status')}}</label>
                                    <select name="status" class="form-control selectpicker">
                                        <option value="active">{{language_data('Active')}}</option>
                                        <option value="inactive">{{language_data('Inactive')}}</option>
                                    </select>
                                </div>

                                <div id="myform">
                                    <div class="form-group">
                                        <label>{{language_data('Components')}}</label>
                                        <select name="" class="form-control selectpicker" data-live-search="true" id="component">
                                            @foreach($components as $comp)
                                                <option value='{{$comp->id}}'>{{$comp->component}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Unit Components')}}</label>
                                        <select name="" class="form-control selectpicker" data-live-search="true" id="unit">
                                            @foreach($units as $u)
                                                <option value='{{$u->id}}'>{{$u->unit}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="Add" onclick="Javascript:addRow()">
                                </div>

                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="col-sm-12">
                                            <div class="panel">
                                                <div id="mydata">
                                                    <table class="table table-hover table-ultra-responsive" id="default-table">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 30%;">{{language_data('Component Name')}}</th>
                                                                <th style="width: 10%;">{{language_data('Unit Component')}}</th>
                                                                <th style="width: 20%;">{{language_data('Unit Nominal')}}</th>
                                                                <th style="width: 10%;">{{language_data('Converter')}}</th>
                                                                <th style="width: 20%;">{{language_data('Nominal')}}</th>
                                                                <th style="width: 5%;">{{language_data('Action')}}</th>
                                                            </tr>
                                                        </thead>
                                                        
                                                    </table>
                                                    <table class="table table-hover table-ultra-responsive" id="default-table2">
                                                        <tfoot>
                                                            <tr>
                                                                <td style="width: 30%;"></td>
                                                                <td style="width: 10%;"></td>
                                                                <td style="width: 20%;"></td>
                                                                <td style="width: 10%;"><p class="pull-right">Total : </p></td>
                                                                <td style="width: 20%;"><input type="text" name="total" class="form-control num total" value="0"></td>
                                                                <td style="width: 5%;"></td>
                                                            </tr>
                                                        </tfoot>
                                                        
                                                    </table>

                                                </div>

                                            </div>
                                        </div>

                                        
                                        

                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

    <script>
        $(document).ready(function(){
            $('.data-table').DataTable();
            $('.num').number( true, 0);
            $(".num").css('text-align', 'right');
            $(".num").css('width', '150px');

            $('#default-table').on("keyup",".nominal", function() {
                Subtotal();
            });
            
            $('#default-table').on("keyup",".unit, .converter", function() {
                var unit = parseFloat($(this).parents("tr").find(".unit").val());
                var converter = parseFloat($(this).parents("tr").find(".converter").val());
                var nominal = unit * converter;
                console.info(unit, converter, nominal);
                if(!isNaN(nominal)){
                    $(this).parents("tr").find(".nominal").val(nominal ).number( true, 0);
                    Subtotal();
                }
            });
        });

        function Subtotal() {
            var total = 0;
            $('input.nominal').each(function () {
                var n = parseFloat($(this).val());
                total += isNaN(n) ? 0 : n;
            });
            $('.total').val(total).number( true, 0);

            $('.num').number( true, 0);
            $('.num').css('text-align', 'right');
        }
        function getSelectedText(elementId) {
            var elt = document.getElementById(elementId);

            if (elt.selectedIndex == -1)
                return null;

            return elt.options[elt.selectedIndex].text;
        }

        function addRow() {
         
            var component = document.getElementById("component");
            var unit = document.getElementById("unit");
            var textcomp = getSelectedText('component');
            var textunit = getSelectedText('unit');
            var table = document.getElementById("default-table");

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            var converter = 0;

            if(textunit=='Harian'){
                converter = 20;
            }
            if(textunit=='Mingguan'){
                converter = 4;
            }
            if(textunit=='Bulanan'){
                converter = 1;
            }

            row.insertCell(0).innerHTML= '<input type="hidden" class="form-control" value="'+component.value+'" name="component[]"><input type="text" readonly class="form-control" value="'+textcomp+'">';
            row.insertCell(1).innerHTML= '<input type="hidden" class="form-control" value="'+unit.value+'" name="unit[]"><input type="text" readonly class="form-control" value="'+textunit+'">';
            row.insertCell(2).innerHTML= '<input type="text" name="unit_nominal[]" class="form-control num unit" value="0">';
            row.insertCell(3).innerHTML= '<input type="text" name="converter[]" class="form-control num converter" value="'+converter+'">';
            row.insertCell(4).innerHTML= '<input type="text" name="value[]" class="form-control num nominal" value="0">';
            row.insertCell(5).innerHTML= '<input class="btn btn-danger btn-xs" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';

            $('.num').number( true, 0);
            $(".num").css('text-align', 'right');
            $(".num").css('width', '150px');
        }

        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("default-table");
            table.deleteRow(index);
        
        }
    </script>
@endsection
