<div class="modal fade modal_edit_project_{{$p->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Project</h4>
            </div>
            <form class="form-some-up form-block" role="form" action="{{url('projects/update')}}" method="post">

                <div class="modal-body">
                    <div class="form-group">
                        <label>{{language_data('Client Contract Number')}}</label>
                            <input type="text" class="form-control" required="" value="{{$p->project_number}}" name="project_number">
                    </div>

                    <div class="form-group">
                        <label>{{language_data('Client Contract Name')}}</label>
                        <input type="text" class="form-control" required="" value="{{$p->project}}" name="projects">
                    </div>

                    <div class="form-group">
                        <label>{{language_data('Company')}}</label>
                        <input type="text" class="form-control" required="" readonly value="{{$p->company_name->company}}" name="company">
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>{{language_data('Start Date')}}</label>
                            <input type="text" class="form-control datepicker" value="{{get_date_format_indonesia($p->start_date)}}" name="start_date" id="start_date" required="">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>{{language_data('End Date')}}</label>
                            <input type="text" class="form-control datepicker" value="{{get_date_format_indonesia($p->end_date)}}" name="end_date" id="end_date" required="">
                        </div>
                    </div>
                                
                    {{-- <div class="form-group">
                        <label>{{language_data('Value')}}</label>
                        <input type="text" class="form-control" required="" value="{{$p->value}}" name="value">
                    </div> --}}

                </div>
                
                
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="cmd" value="{{$p->id}}">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>{{language_data('Update')}}</button>
                </div>

            </form>
        </div>
    </div>

</div>
