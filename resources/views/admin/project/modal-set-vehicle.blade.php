<!-- Modal vehicle -->
<div class="modal fade vehicle_{{$emp->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Set Vehicle</h4>
            </div>
            <form class="form-some-up" role="form" method="post" id="form" action="{{url('projects/set-vehicle')}}" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>{{language_data('Employee Code')}}</label><br>
                                <input type="text" class="form-control" name="" readonly required value="{{$emp->employee_code}}">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Name</label><br>
                                <input type="text" class="form-control" name="" readonly required value="{{$emp->fname}} {{$emp->lname}}">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Phone Number</label><br>
                                <input type="text" class="form-control" name="" readonly required value="{{$emp->phone}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Brand</label><br>
                                <input type="text" class="form-control" name="brand"  required value="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Type</label><br>
                                <input type="text" class="form-control" name="type"  required value="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>TMT</label><br>
                                <input type="text" class="form-control datePicker" name="tmt"  required value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>License Plate</label><br>
                                <input type="text" class="form-control" name="license_plate"  required value="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Vehicle Registration Date</label><br>
                                <input type="text" class="form-control datePicker" name="vehicle_registration_date"  required value="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tax Due Dtae</label><br>
                                <input type="text" class="form-control datePicker" name="tax_due_date"  required value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Insurance Vendor</label><br>
                                <input type="text" class="form-control" name="insurance_vendor"  required value="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Vehicle Vendor</label><br>
                                <input type="text" class="form-control" name="vehicle_vendor"  required value="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Car Rental Value</label><br>
                                <input type="text" class="form-control" name="car_rental_value"  required value="">
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="cmd" value="{{$emp->id}}">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                    <button type="submit" class="btn btn-primary">{{language_data('Save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>        