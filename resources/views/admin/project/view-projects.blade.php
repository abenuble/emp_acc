@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Client Contract')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

            <div class="col-lg-12">
                <div class="panel-body">
                    <div class="row text-center">

                        <div class="col-sm-3 m-b-15">
                            <div class="z-shad-1">
                                <div class="bg-success text-white p-15 clearfix">
                                    <span class="pull-left font-45 m-l-10"><i class="fa fa-users"></i></span>

                                    <div class="pull-right text-right m-t-15">
                                        <font size="4"><span class="m-b-5">{{language_data('Employees')}}</span></font><br>
                                        <font size="4"><span class="m-b-5">{{$employees}}</span></font>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3 m-b-15">
                            <div class="z-shad-1">
                                <div class="bg-complete-darker text-white p-15 clearfix">
                                    <span class="pull-left font-45 m-l-10"><i class="fa fa-envelope"></i></span>

                                    <div class="pull-right text-right m-t-15">
                                        <font size="4"><span class=" m-b-5">{{language_data('Applicants')}}</span></font><br>
                                        <font size="4"><span class=" m-b-5">{{$job_applicants}}</span></font>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col-sm-4 m-b-15">
                            <div class="z-shad-1">
                                <div class="bg-complete text-white p-15 clearfix">
                                    <span class="pull-left font-45 m-l-10"><i class="fa fa-suitcase"></i></span>

                                    <div class="pull-right text-right m-t-15">
                                        <font size="4"><span class=" m-b-5 ">{{language_data('Job App Closed')}}</span></font><br>
                                        <font size="4"><span class=" m-b-5 ">{{$jobs_closed}}/{{$jobs}}</span></font>
                                    </div>

                                </div>
                            </div>
                        </div>


                        {{-- <div class="col-sm-3 m-b-15">
                            <div class="z-shad-1">
                                <div class="bg-primary text-white p-15 clearfix">
                                    <span class="pull-left font-45 m-l-10"><i class="fa fa-bar-chart"></i></span>

                                    <div class="pull-right text-right m-t-15">
                                        <font size="4"><span class=" m-b-5 ">{{language_data('Unpaid Expense')}}</span></font><br>
                                        <font size="4"><span class=" m-b-5 ">{{app_config('Currency')}}{{number_format($expenses,0)}}</span></font>
                                    </div>

                                </div>
                            </div>
                        </div> --}}



                    </div>
                </div>
            </div>
        </div>


        </div>
        <div class="p-30 p-t-none p-b-none">
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body p-t-20">
                            <div class="clearfix">
                                <h3 class="bold font-color-1">{{$projectview->project}}</h3>
                                <span>{{$projectview->company_name->company}}</span><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#overview" aria-controls="home" role="tab" data-toggle="tab">{{language_data('Overview')}}</a></li>
                        <li role="presentation"><a href="#employee" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Employee')}}</a></li>
                        <li role="presentation"><a href="#payment" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Payment')}}</a></li>
                        <li role="presentation"><a href="#project_contract_file" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('client Contract File')}}</a></li>
                        {{-- <li role="presentation"><a href="#expense" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Expense')}}</a></li>
                        <li role="presentation"><a href="#item" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Item')}}</a></li>
                        @if($projectview->category=='transportation')
                            <li role="presentation"><a href="#vehicle" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Vehicle')}}</a></li>
                        @endif --}}
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content panel p-20">

                        {{--Overview--}}
                        <div role="tabpanel" class="tab-pane active" id="overview">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label>{{language_data('Internal Company')}}</label>
                                    <input type="text" class="form-control" readonly @if($projectview->internal_company!=0) value="{{$projectview->internal_company_name->company}}" @endif name="internal_company">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Client Contract Number')}}</label>
                                    <input type="text" class="form-control" readonly value="{{$projectview->project_number}}" name="id">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Client Contract Name')}}</label>
                                    <input type="text" class="form-control" readonly value="{{$projectview->project}}" name="projects">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <input type="text" class="form-control" readonly value="{{$projectview->company_name->company}}" name="company">
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Facility')}}</label>
                                            <div class="checkbox">
                                                <label><input type="checkbox" disabled id="facility1" class="facility" name="facility[]" @if(in_array('kesehatan', json_decode($projectview->facility))) checked @endif value="kesehatan">{{language_data('Kesehatan')}}</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" disabled id="facility2" class="facility" name="facility[]" @if(in_array('ketenagakerjaan', json_decode($projectview->facility))) checked @endif value="ketenagakerjaan">{{language_data('ketenagakerjaan')}}</label>
                                            </div>  
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group kesehatan" style="display:none;">
                                            <label>Jenis Fasilitas Kesehatan</label>
                                            <input type="text" class="form-control" readonly name="health_facility_type" id="health_facility_type" value="{{$projectview->health_facility_type}}">
                                        </div>

                                        <div class="form-group ketenagakerjaan" disabled style="display:none;">
                                            <label>Jenis Fasilitas Ketenagakerjaan</label><br>
                                            <input type="checkbox" name="accident_insurance" value="1" @if($projectview->accident_insurance=='1') checked @endif> Jaminan Kecelakaan Kerja (JKK)<br>
                                            <input type="checkbox" name="life_insurance" value="1" @if($projectview->life_insurance=='1') checked @endif> Jaminan Kematian (JKM)<br>
                                            <input type="checkbox" name="jaminan_hari_tua" value="1" @if($projectview->jaminan_hari_tua=='1') checked @endif> Jaminan Hari Tua (JHT)<br>
                                            <input type="checkbox" name="jaminan_pensiun" value="1" @if($projectview->jaminan_pensiun=='1') checked @endif> Jaminan Pensiun (JP)<br>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group kesehatan" style="display:none;">
                                            <label>Nama Asuransi Kesehatan</label>
                                            <input type="text" class="form-control" readonly name="insurance_name" id="insurance_name" value="{{$projectview->insurance_name}}">
                                        </div>
                                    </div>
                                </div>

                                {{-- <div class="form-group">
                                    <label>{{language_data('Category')}}</label>
                                    <select name="category" disabled class="form-control selectpicker" data-live-search="true">
                                        <option value="support" @if($projectview->category=='support') selected @endif>{{language_data('Support Services')}}</option>
                                        <option value="transportation" @if($projectview->category=='transportation') selected @endif>{{language_data('Transportation Services')}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Sub Category')}}</label>
                                    <select name="category" disabled class="form-control selectpicker" data-live-search="true">
                                        @foreach($subcategories as $c)
                                            <option value="{{$c->id}}" @if($projectview->subcategory==$c->id) selected @endif>{{$c->subcategory}}</option>
                                        @endforeach
                                    </select>
                                </div> --}}

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('Start Date')}}</label>
                                        <input type="text" class="form-control datePicker" name="start_date" readonly value="{{get_date_format($projectview->start_date)}}" required="">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('End Date')}}</label>
                                        <input type="text" class="form-control datePicker" name="end_date" readonly value="{{get_date_format($projectview->end_date)}}" required="">
                                    </div>
                                </div>
                                
                                {{-- <div class="form-group" style="display:none">
                                    <label>{{language_data('Client Contract Value')}}</label>
                                    <input type="text" class="form-control" readonly value="{{app_config('Currency')}} {{number_format($projectview->value,0)}}" name="value">
                                </div> --}}

                                @if($projectview->status=='opening')
                                    @if(strtotime($projectview->end_date.'-30 days')<=strtotime(date('Y-m-d')))
                                        <div class="form-group text-right">
                                            <form class="" role="form" method="post" action="{{url('projects/closing')}}" enctype="multipart/form-data">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" value="{{$projectview->id}}" name="cmd">
                                                <button type="submit" class="btn btn-danger">Closing</button>
                                            </form>
                                        </div>
                                    @endif
                                @endif
                            </div>

                            <div class="row">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('Client Contract')}}</h3>
                                    @if($permcheck->U==1)       
                                    <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#detailProject"><i class="fa fa-plus"></i> {{language_data('Add New Designation')}}</button>
                                    @endif
                                    <br>
                                </div>
                                <table class="table data-table table-hover table-ultra-responsive" id="table_detail">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 25%;">{{language_data('Designation')}}</th>
                                        <th style="width: 20%;">{{language_data('Location')}}</th>
                                        <th style="width: 15%;">{{language_data('Open Date')}}</th>
                                        <th style="width: 15%;">{{language_data('Close Date')}}</th>
                                        <th style="width: 15%;">{{language_data('Quota')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $ctr = 0; ?>
                                    @foreach($projectneeds as $pro)
                                        <?php $ctr++; ?>
                                        <tr>
                                            <td data-label="No" >{{$ctr}}</td>
                                            <td data-label="Designation"> <p><a href="{{url('jobs/view-applicant/'.$pro->id_job)}}">{{$pro->designation_name->designation}}</a></p></td>
                                            <td data-label="Location">{{$pro->location}}</td>
                                            <td data-label="Open Date">{{get_date_format($pro->open_date)}}</td>
                                            <td data-label="Close Date">{{get_date_format($pro->close_date)}}</td>
                                            <td data-label="Total">{{$pro->total}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>

                        {{--Employee--}}
                        <div role="tabpanel" class="tab-pane" id="employee">
                            <div class="row">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('Employee')}}</h3>
                                    <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#download">{{language_data('Download Data')}}</button></a><br>             
                                    <br>
                                </div>
                                <table class="table data-table table-hover table-ultra-responsive">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 10%;">{{language_data('Employee Code')}}</th>
                                        <th style="width: 15%;">{{language_data('Name')}}</th>
                                        <th style="width: 20%;">{{language_data('Location')}}</th>
                                        <th style="width: 15%;">{{language_data('Designation')}}</th>
                                        <th style="width: 20%;">{{language_data('Address')}}</th>
                                        <th style="width: 15%;">{{language_data('Phone Number')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $ctr = 0; ?>
                                    @foreach($employee as $emp)
                                        <?php $ctr++; ?>
                                        <tr>
                                            <td data-label="No" >{{$ctr}}</td>
                                            <td data-label="Employee Code">{{$emp->employee_code}}</td>
                                            <td data-label="Name">
                                                @if($employee_permission->R==1) <a href="{{url('employees/view/'.$emp->id)}}">{{$emp->fname}} {{$emp->lname}}</a> @else {{$emp->fname}} {{$emp->lname}} @endif
                                            </td>
                                            <td data-label="Location">{{$emp->location}}</td>
                                            <td data-label="Designation">{{$emp->designation_name->designation}}</td>
                                            <td data-label="Address">{{$emp->pre_address}}</td>
                                            <td data-label="Phone Number"><a href="tel:{{$emp->phone}}">{{$emp->phone}}</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>

                        {{--Payment--}}
                        <div role="tabpanel" class="tab-pane" id="payment">
                            <div class="row">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('Payment')}}</h3>
                                    <br>
                                </div>
                                <table class="table data-table table-hover table-ultra-responsive">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 20%;">{{language_data('Designation')}}</th>
                                        <th style="width: 20%;">{{language_data('Location')}}</th>
                                        <th style="width: 20%;">{{language_data('Quota')}}</th>
                                        {{-- <th style="width: 15%;">{{language_data('Payment Type')}}</th> --}}
                                        <th style="width: 20%;">{{language_data('Actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $ctr = 0; ?>
                                    @foreach($projectneeds as $pro)
                                        <?php $ctr++; ?>
                                        <tr>
                                            <td data-label="No" >{{$ctr}}</td>
                                            <td data-label="Designation">{{$pro->designation_name->designation}}</td>
                                            <td data-label="Location">{{$pro->location}}</td>
                                            <td data-label="Total">{{$pro->total}}</td>
                                            {{-- <td data-label="Payment Type">
                                                @if($permcheck->U==1)       
                                                <a class="btn btn-success btn-xs" href="{{url('projects/edit-payment/'.$pro->id_needs)}}"><i class="fa fa-edit"></i> {{language_data('Set Payment Type')}}</a>
                                                @endif
                                            </td> --}}
                                            @if($pro->id_payment == '0')
                                            <td data-label="Actions"></td>
                                            @else
                                            <td data-label="Actions">
                                                <a class="btn btn-complete btn-xs" href="{{url('projects/view-payment/'.$pro->id_needs)}}"><i class="fa fa-edit"></i> {{language_data('view')}}</a>
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        {{--Project Contract File--}}
                        <div role="tabpanel" class="tab-pane" id="project_contract_file">
                            <div class="row">
                                <div class="row">

                                    @if($permcheck->U==1)       
                                    <div class="col-lg-3">
                                        <div class="panel-body">
                                            <form class="" role="form" method="post" action="{{url('projects/add-document')}}" enctype="multipart/form-data">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title"> {{language_data('Add Document')}}</h3>
                                                </div>

                                                <div class="form-group">
                                                    <label>{{language_data('Document Name')}}</label>
                                                    <input type="text" class="form-control" required name="document_name" value="Project File">
                                                </div>

                                                <div class="form-group">

                                                    <label>{{language_data('Select Document')}}</label>
                                                    <div class="input-group input-group-file">
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-primary btn-file">
                                                                    {{language_data('Browse')}} <input type="file" class="form-control" name="file">
                                                                </span>
                                                            </span>
                                                        <input type="text" class="form-control" readonly="">
                                                    </div>
                                                </div>

                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" value="{{$projectview->id}}" name="cmd">
                                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add')}} </button>
                                            </form>
                                        </div>
                                    </div>
                                    @endif

                                    <div class="col-lg-9">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">{{language_data('All Documents')}}</h3>
                                        </div>
                                        <div class="panel-body p-none">
                                            <table class="table data-table table-hover table-ultra-responsive">
                                                <thead>
                                                <tr>
                                                    <th style="width: 5%;">No</th>
                                                    <th style="width: 60%;">{{language_data('Document Name')}}</th>
                                                    <th style="width: 35%;" class="text-right">{{language_data('Actions')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $ctr = 1; ?>
                                                @foreach($project_doc as $pd)
                                                    <tr>
                                                        <td data-label="no">{{$ctr++}}</td>
                                                        <td data-label="Document Name">{{$pd->file_title}}</td>
                                                        <td class="text-right">
                                                            <a href="{{url('projects/download-project-document/'.$pd->id)}}" class="btn btn-success btn-xs"><i class="fa fa-download"></i> {{language_data('Download')}}</a>
                                                            <a href="#" class="btn btn-danger btn-xs deleteProjectDoc" id="{{$pd->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                
                            </div>
                        </div>

                        {{--Expense--}}
                        {{-- <div role="tabpanel" class="tab-pane" id="expense">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">{{language_data('Client Contract Expense')}}</h3>
                                        @if($permcheck->U==1)       
                                        <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#expenses"><i class="fa fa-plus"></i> {{language_data('Add New Expense')}}</button>
                                        @endif
                                        <a data-toggle="modal" data-target="#downloadex" class="btn btn-info btn-sm pull-right" ><i class="fa fa-download"></i> Download </a>

                                        <br>
                                    </div>
                                    <table class="table data-table table-hover table-ultra-responsive">
                                        <thead>
                                        <tr>
                                            <th style="width: 5%;">No</th>
                                            <th style="width: 10%;">{{language_data('Expense Number')}}</th>
                                            <th style="width: 15%;">{{language_data('Expense')}}</th>
                                            <th style="width: 20%;">{{language_data('Post Date')}}</th>
                                            <th style="width: 15%;">{{language_data('Total')}}</th>
                                            <th style="width: 20%;">{{language_data('Status')}}</th>
                                            <th style="width: 15%;">{{language_data('Actions')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($expense as $e)
                                            <tr>
                                                <td data-label="No" >{{$ctr}}</td>
                                                <td data-label="Expense Number">{{$e->expense_number}}</td>
                                                <td data-label="Expense">{{$e->about}}</td>
                                                <td data-label="Post Date">{{get_date_format($e->date)}}</td>
                                                <td data-label="Total">{{app_config('CurrencyCode')}} {{number_format($e->total,0)}}</td>
                                                
                                                @if($e->status=='Approved')
                                                    <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Approved')}}</p></td>
                                                @elseif($e->status=='Pending')
                                                    <td data-label="Status"><p class="btn btn-warning btn-xs">{{language_data('Pending')}}</p></td>
                                                    @elseif($e->status=='Edited')
                                                    <td data-label="Status"><p class="btn btn-primary btn-xs">Edited</p></td>
                                                @elseif($e->status=='Rejected')
                                                    <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Rejected')}}</p></td>
                                                @endif

                                                <td data-label="Actions">
                                                @if($e->status=='Approved' ||$e->status=='Rejected' )
                                                <a class="btn btn-info btn-xs" href="{{url('projects/download-expense/'.$e->id)}}"><i class="fa fa-download"></i> {{language_data('Download Data')}}</a>


                                                @endif
                                                    <a class="btn btn-complete btn-xs" href="{{url('projects/view-expense/'.$e->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div> --}}

                        {{--Item--}}
                        {{-- <div role="tabpanel" class="tab-pane" id="item">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">{{language_data('Client Contract Item')}}</h3>
                                        @if($permcheck->U==1)       
                                        <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload">{{language_data('Upload')}}</button></a>             

                                        <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#items"><i class="fa fa-plus"></i> {{language_data('Add New Item')}}</button>
                                        @endif
                                        <br>
                                    </div>
                                    <table class="table data-table table-hover table-ultra-responsive">
                                        <thead>
                                        <tr>
                                            <th style="width: 5%;">No</th>
                                            <th style="width: 25%;">{{language_data('Item')}}</th>
                                            <th style="width: 25%;">{{language_data('Amount')}}</th>
                                            <th style="width: 25%;">{{language_data('Amount Purchased')}}</th>
                                            <th style="width: 20%;">{{language_data('Leftovers')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($item as $i)
                                            <tr>
                                                <td data-label="No" >{{$ctr}}</td>
                                                <td data-label="Item">{{$i->item}}</td>
                                                <td data-label="Amount">{{$i->amount}}</td>
                                                <td data-label="Amount Purchased">{{$i->amount_bought}}</td>
                                                <td data-label="Leftovers">{{$i->leftovers}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div> --}}

                        {{--vehicle--}}
                        {{-- <div role="tabpanel" class="tab-pane" id="vehicle">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">{{language_data('Client Contract Vehicle')}}</h3>
                                    </div>
                                    <table class="table data-table table-hover table-ultra-responsive">
                                        <thead>
                                        <tr>
                                            <th style="width: 5%;">No</th>
                                            <th style="width: 25%;">{{language_data('Employee Code')}}</th>
                                            <th style="width: 25%;">{{language_data('Employee Name')}}</th>
                                            <th style="width: 25%;">{{language_data('Phone Number')}}</th>
                                            <th style="width: 20%;">{{language_data('Actions')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($employee as $emp)
                                            <tr>
                                                <td data-label="No" >{{$ctr}}</td>
                                                <td data-label="Employee Code">{{$emp->employee_code}}</td>
                                                <td data-label="Employee Name">
                                                    @if($employee_permission->R==1) <a href="{{url('employees/view/'.$emp->id)}}">{{$emp->fname}} {{$emp->lname}}</a> @else {{$emp->fname}} {{$emp->lname}} @endif
                                                </td>
                                                <td data-label="Phone Number"><a href="tel:{{$emp->phone}}">{{$emp->phone}}</a></td>
                                                <td data-label="Actions">
                                                @if($emp->vehicle=='')
                                                    @if($permcheck->U==1)       
                                                        <a class="btn btn-success btn-xs" href="{{url('projects/edit-vehicle/'.$emp->id)}}"><i class="fa fa-edit"></i> {{language_data('Set vehicle')}}</a>
                                                    @endif

                                                @else
                                                    <a class="btn btn-complete btn-xs" href="{{url('projects/view-vehicle/'.$emp->vehicle)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                                @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div> --}}

                    </div>
                </div>
            </div>  
            
              

     <!-- Modal Upload -->
     {{-- <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('projects/importExcelItem/'.$projectview->id) }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file" accept="application/vnd.ms-excel"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
 
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('projects/downloadExcel/item') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>  --}}
            <!-- Modal add -->
            <div class="modal fade" id="detailProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Designation')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('projects/updateProjectDetail')}}" enctype="multipart/form-data">
                            <div class="modal-body">

                                <h3 class="panel-title">{{language_data('Client Contract Detail')}}</h3>
                                <div id="myform">

                                    <div class="form-group">
                                        <label>{{language_data('Designation')}}</label>
                                        <select name="" class="form-control selectpicker" data-live-search="true" id="designation">
                                            @foreach($designation as $d)
                                                <option value='{{$d->id}}'>{{$d->designation}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Location')}}</label>
                                        <input type="text" class="form-control" name="" required id="location" value="Jakarta">
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Open Date')}}</label>
                                            <input type="text" class="form-control" required name="" id="open_date">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Close Date')}}</label>
                                            <input type="text" class="form-control" required name="" id="close_date">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Quota')}}</label>
                                        <input type="text" class="form-control" value="12" required name="" id="total">
                                    </div>
                                    <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="Add" onclick="addRow();">
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata">
                                                <table class="table table-hover table-ultra-responsive" id="myTableData"  >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 20%;">{{language_data('Designation')}}</th>
                                                            <th style="width: 20%;">{{language_data('Location')}}</th>
                                                            <th style="width: 20%;">{{language_data('Open Date')}}</th>
                                                            <th style="width: 20%;">{{language_data('Close Date')}}<</th>
                                                            <th style="width: 10%;">{{language_data('Quota')}}</th>
                                                            <th style="width: 10%;">{{language_data('Action')}}</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                    
                                    

                                </div>

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$projectview->id}}">
                                <input type="hidden" name="company_id" value="{{$projectview->company}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

             <!-- Modal Download -->
            <div class="modal fade" id="download" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Download</h4>
                        </div>
                        <form class="form-some-up" role="form" id="downloadEmployee" action="{{url('projects/download-employee-data')}}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="row">
                                    <div class="form-group">
                                        <label>Based on :</label>
                                        <label class="radio-inline"><input type="radio" disabled name="based" value="{{$projectview->company}}">{{language_data('Company')}}</label>
                                        <label class="radio-inline"><input type="radio" disabled name="based" value="{{$projectview->id}}" checked>{{language_data('Client Contract')}}</label>
                                    </div>
                                
                                    <div class="form-group">
                                        <label>{{language_data('Client Contract')}}</label>
                                        <select name="project_name" disabled class="form-control selectpicker" data-live-search="true">
                                            @foreach($projects as $d)
                                                <option value='{{$d->id}}' @if($d->id==$projectview->id) selected @endif>{{$d->project}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Employee Data</label>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="employee_code">NIK</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="doj">Tanggal Masuk</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="dol">Tanggal Berakhir</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="fname">Nama Karyawan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="gender">Jenis Kelamin</label>
                                        </div>         
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="company">Perusahaan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="designation">Jabatan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="location">Lokasi</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="no_ktp">No. KTP</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="dob">Tanggal Lahir</label>
                                        </div>       
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="birth_place">Tempat Lahir</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="per_address">Alamat (KTP)</label>
                                        </div> 
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="per_village">Kelurahan (KTP)</label>
                                        </div>            
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="per_district">Kecamatan (KTP)</label>
                                        </div>                    
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="per_city">Kota (KTP)</label>
                                        </div>                
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="pre_address">Alamat Domisili</label>
                                        </div>                
                                    </div>

                                    <div class="col-md-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="last_education">Pendidikan Terakhir</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="religion">Agama</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="mother_name">Nama Ibu Kandung</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="marital_status">Status Perkawinan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.tax_status">Status Pajak</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="phone">Nomor Telpon</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="email">Email</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.account_number">No Rekening</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.npwp_number">No NPWP</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.employee_bpjs">BPJS Karyawan</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.health_bpjs">BPJS Kesehatan</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.other_insurance_number">No Asuransi Lain</label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="payment_type">Upah Karyawan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.employee_bpjs_cut">Potongan BPJS Ketenagakerjaan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.health_bpjs_cut">Potongan BPJS Ketenagakerjaan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.other_insurance_cut">Potongan Asuransi Lain</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.pph_cut">Potongan PPH 21</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="checkalll" >Check All</label>
                                        </div>   
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$projectview->id}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="button" onclick="downloadEmployee()" class="btn btn-primary">Download</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


  {{-- <div class="modal fade" id="downloadex" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Download</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{url('projects/downloadExcelProjectExpense')}}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                          
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="expense_number">Nomor Tagihan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="about">Nama Tagihan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="created_at">Tanggal</label>
                                        </div>         
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="ppn_tax"><Tarea>PPN</Tarea></label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="purchase_from"><Tarea>Purchase from</Tarea></label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="total"><Tarea>Total</Tarea></label>
                                        </div>
                                      
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="checkalll" >Check All</label>
                                        </div>            
                                    </div>

                                   
                                </div>
                                
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$projectview->id}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">Download</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}
            <!-- Modal expense -->
            {{-- <div class="modal fade" id="expenses" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add New Expense</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('projects/add-expense')}}" enctype="multipart/form-data">
                            <div class="modal-body">

                                 <div class="form-group">
                                    <label>Expense Number</label>
                                    @if($company->id=='1')
                                    <input type="text" class="form-control" required readonly name="expense_number" value="{{$number}}/SSS/{{get_roman_letters($month)}}/{{$year}}">
                                    @elseif($company->id=='2')
                                    <input type="text" class="form-control" required readonly name="expense_number" value="{{$number}}/UY/{{get_roman_letters($month)}}/{{$year}}">
                                    @else
                                    <input type="text" class="form-control" required readonly name="expense_number" value="{{$number}}/Company/{{get_roman_letters($month)}}/{{$year}}">
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Post date</label>
                                    <input type="text" class="form-control datePicker" required="" name="post_date">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <input type="text" class="form-control" readonly value="{{$projectview->company_name->company}}" required="">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Client Contract')}}</label>
                                    <input type="text" class="form-control" readonly value="{{$projectview->project}}" required="" name="project">
                                </div>

                                <div class="form-group">
                                    <label>Expense</label>
                                    <input type="text" class="form-control" required="" name="about">
                                </div>

                                <div class="form-group">
                                    <label>Total</label>
                                    <input type="text" readonly class="form-control alltotal" required="">
                                </div>
                                <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="Add" onclick="addRow2();">
                            </div>

                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata2">
                                                <table class="table table-hover table-ultra-responsive" id="myTableData2"  >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 30%;">Item</th>
                                                            <th style="width: 10%;">QTY</th>
                                                            <th style="width: 20%;">Unit Price</th>
                                                            <th style="width: 10%;">ppn</th>
                                                            <th style="width: 20%;">Total Price</th>
                                                            <th style="width: 10%;">{{language_data('Action')}}</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata2">
                                                <table class="table table-hover table-ultra-responsive" id="myTableData3">
                                                <tr>
                                                    
                                                    <td style="width: 60%;"><center><label>Sub Total :</label></center></td>
                                                    <td style="width: 50%;"><input type="text" name="subtotal" readonly class="form-control subtotal unit pull-right"></td>
                                                </tr>
                                                <tr>
                                                  
                                                    <td style="width: 60%;"><center><label>Pembulatan :</label></center></td>
                                                    <td style="width: 50%;"><input type="text" value="0" class="form-control feee pull-right"></td>
                                                </tr>
                                                <tr>
                                                <input type="hidden" name="fee" value="0" >
                                              
                                                    <td style="width: 60%;"><center><label>PPN :</label></center></td>
                                                    <td style="width: 50%;"><input type="text" name="ppn" value="0" class="form-control ppnz unit pull-right"></td>
                                                </tr>
                                                <tr>
                                                  
                                                    <td style="width: 60%;"><center><label>Total :</label></center></td>
                                                    <td style="width: 50%;"><input type="text" name="alltotal" readonly class="form-control alltotal unit pull-right">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;"></td>
                                                    <td style="width: 50%;"> <center><input type="checkbox" name="keterangan"/>&nbsp;<label style="color:red;margin-right:0;text-align:right">*Biaya Tagihan Sudah Termasuk PPN</label></center></td>
                                                </tr>
                                                    
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" value="{{$projectview->id}}" name="cmd">
                                <input type="hidden" value="{{$projectview->company}}" name="company">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}

            <!-- Modal add item-->
            {{-- <div class="modal fade" id="items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add Item</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('projects/add-item')}}" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>Item</label>
                                    <input type="text" class="form-control" name="item" required value="">
                                </div>

                                <div class="form-group">
                                    <label>Amount</label>
                                    <input type="text" class="form-control" value="" required name="amount">
                                </div>

                            </div>
                            

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$projectview->id}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}


    <script>
        function activaTab(tab){
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };
        $(document).ready(function () {
            activaTab('<?php echo $tabs; ?>');
            $('#checkalll').change(function() {
                if(this.checked) {
                    $('input:checkbox[name="variable[]"]').prop('checked',true);
                    $('input:checkbox[name="banks[]"]').prop('checked',true);
                }
                else{
                    $('input:checkbox[name="variable[]"]').prop('checked',false);
                    $('input:checkbox[name="banks[]"]').prop('checked',false);

                }
                    
            });
            $('.facility').ready(function() {
                if(document.getElementById("facility1").checked == true){
                    $('.kesehatan').show();
                    var asuransi = $('#health_facility_type').val();
                    $('#insurance_name').val(asuransi);
                } else {
                    $('.kesehatan').hide();
                }
                if(document.getElementById("facility2").checked == true){
                    $('.ketenagakerjaan').show();
                } else {
                    $('.ketenagakerjaan').hide();
                }
            });
            $('#health_facility_type').ready(function() {
                var asuransi = $('#health_facility_type').val();
                $('#insurance_name').val(asuransi);
                    
            });
            $('#open_date,#close_date').datetimepicker({
                locale: 'id',
                useCurrent: false,
                format: 'DD MMMM YYYY',
                minDate: moment()
            });
            $('#open_date').datetimepicker().on('dp.change', function (e) {
                var incrementDay = moment(new Date(e.date));
                incrementDay.add(1, 'days');
                $('#close_date').data('DateTimePicker').minDate(incrementDay);
                $(this).data("DateTimePicker").hide();
            });

            $('#close_date').datetimepicker().on('dp.change', function (e) {
                var decrementDay = moment(new Date(e.date));
                decrementDay.subtract(1, 'days');
                $('#open_date').data('DateTimePicker').maxDate(decrementDay);
                    $(this).data("DateTimePicker").hide();
            });
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/projects/delete/" + id;
                    }
                });
            });

            /*For Delete Project Doc*/
            $(".deleteProjectDoc").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/projects/delete-project-doc/" + id;
                    }
                });
            });

            /*For components Loading*/
            $("#payment_type").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'pay_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/projects/get-component',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#components").html( data);
                    }
                });
            });

        //    $('#myTableData2').on("change",".qty, .unit , .persenz", function() {
        //         var item_quantity = parseFloat($(this).parents("tr").find(".qty").val());
        //         var persen = parseFloat($(this).parents("tr").find(".persenz").val());
        //         var unit_price = parseFloat($(this).parents("tr").find(".unit").val());
                
        //         var total_value = (unit_price * item_quantity);
                 
        //         var nominalppn =(unit_price * item_quantity * (persen/100));
        //         console.info(item_quantity, unit_price, total_value);
        //         if(!isNaN(nominalppn))
        //         $(this).parents("tr").find(".nominalppn").val(nominalppn);
        //         if(!isNaN(total_value))
        //             $(this).parents("tr").find(".total").val(total_value);
        //             $('.total').number( true, 0, '.', ',' );
        //             total();
        //     });
            // $('.feee').keyup(function(){
            //       var fee=parseFloat($(".feee").val());
            //       if(isNaN(fee)){fee=0;}
            //     var subtotal = parseFloat($(".subtotal").val());
            //     if(isNaN(subtotal)){subtotal=0;}
            //     var ppn = parseFloat($(".ppnz").val());
            //     if(isNaN(ppn)){ppn=0;}
            //     var alltotal = fee + subtotal+ ppn;
            //     if(!isNaN(alltotal)){
            //         $(".alltotal").val(alltotal);}
            //   });
            // $('#myTableData3').on("change",".feee, .ppn", function() {
                
            //     var fee = parseFloat($(".fee").val());
            
            //     if(isNaN(fee)){fee=0;}
            //     var ppn = parseFloat($(".ppn").val());
            //     if(isNaN(ppn)){ppn=0;}
            //     var subtotal = parseFloat($(".subtotal").val());
            //     if(isNaN(subtotal)){subtotal=0;}
               
            //     var alltotal = subtotal + fee - (fee*ppn/100)+bulat;
            //     console.info( ppn, alltotal);
            //     if(!isNaN(alltotal)){
            //         $(".alltotal").val(alltotal);}
                   
            // });

        });      

        function getSelectedText(elementId) {
            var elt = document.getElementById(elementId);

            if (elt.selectedIndex == -1)
                return null;

            return elt.options[elt.selectedIndex].text;
        } 

        function addRow() {
         
            var designation = document.getElementById("designation");
            var textdesg = getSelectedText('designation');
            var location = document.getElementById("location");
            var open_date = document.getElementById("open_date");
            var close_date = document.getElementById("close_date");

            var desg = document.getElementById("desg");
            var length = document.getElementsByName("loc").length;
            var sama = length+1;
            var loc = document.getElementsByName("loc");

            var i = 0;

            //lokasi.push(location.value);

            var total = document.getElementById("total");
            var table = document.getElementById("myTableData");

            if(open_date.value==='' || close_date.value==='' || total.value===''){
                return false;
            } 

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            row.insertCell(0).innerHTML= '<input type="hidden" class="form-control" value="'+designation.value+'" name="designation[] id="desg"><input type="text" readonly class="form-control" value="'+textdesg+'">';
            row.insertCell(1).innerHTML= '<input type="hidden" readonly class="form-control" value="'+location.value+'" name="location[]"><input type="text" readonly class="form-control" value="'+location.value+'" name="loc">';
            row.insertCell(2).innerHTML= '<input type="text" readonly class="form-control" value="'+open_date.value+'" name="open_date[]">';
            row.insertCell(3).innerHTML= '<input type="text" readonly class="form-control" value="'+close_date.value+'" name="close_date[]">';
            row.insertCell(4).innerHTML= '<input type="text" class="form-control" value="'+total.value+'" name="total[]">';
            row.insertCell(5).innerHTML= '<input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';


        }

        function downloadEmployee(){
            if($('.checks').is(':checked')){
                $("#downloadEmployee").submit();
            } else {
                alert("Silahkan pilih data yang akan di unduh");
                return false;
            }
        } 
        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData");
            table.deleteRow(index);
        
        }

        //    function total() {
            
        //     var bulat = parseFloat($('.feee').val());
        //     if(isNaN(bulat)){bulat=0;}
        //     var total1 =  0;
        //    var sub=0;
        //    var ppntot=0;
        //    var ppntot=0;
        //     $('input.total').each(function () {
              
        //         var n = parseFloat($(this).val());
        //         sub+= isNaN(n) ? 0 : n;
        //         total1 += isNaN(n) ? 0 : n ;
             
        //     });
        //     $('input.nominalppn').each(function () {
        //         var y = parseFloat($(this).val());
        //         total1+= isNaN(y) ? 0 : y;
        //         ppntot+=  isNaN(y) ? 0 : y;
        //     });
        //     total1+=bulat;
        //     $('.ppnz').val(ppntot);
        //     $('.subtotal').val(sub);
        //     $('.alltotal').val(total1);
        // }
        function tandaPemisahTitik(b){
            var _minus = false;
            if (b<0) _minus = true;
            b = b.toString();
            b=b.replace(".","");
            b=b.replace("-","");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--){
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)){
            c = b.substr(i-1,1) + "." + c;
            } else {
            c = b.substr(i-1,1) + c;
            }
            }
            if (_minus) c = "-" + c ;
            return c;
        }
        // function addRow2() {
         
        //     $('#myTableData2').append('<tr>\
        //         <td>\
        //                 <input type="text" name="item[]" required class="form-control">\
        //             </td>\
        //             <td>\
        //                 <input type="text" name="qty[]" required class="form-control qty">\
        //             </td>\
        //             <td>\
        //                 <input type="text" name="unit[]"  required class="form-control unit">\
        //                 <input type="hidden" name="nominalppn[]"  class="form-control nominalppn">\
        //             </td>\
        //             <td>\
        //             <input type="text"  name="ppnper[]" required value="10" class="form-control persenz">\
        //             </td>\
        //             <td>\
        //                 <input type="text" name="total[]" required class="form-control total money">\
        //             </td>\
        //         <td data-label="Action">\
        //             <input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow2(this)">\
        //         </td>\
        //     </tr>');
        //     $('.unit').number( true, 0, '.', ',' );
        //     $('.nominalppn').number( true, 0, '.', ',' );
        //     $('.qty').number( true, 0, '.', ',' );
        //     $('.persenz').number( true, 0, '.', ',' );
            
        // }


        // $('.unit').number( true, 0, '.', ',' );
        // $('.feee').number( true, 0, '.', ',' );

        // function deleteRow2(obj) {
            
        //     var index = obj.parentNode.parentNode.rowIndex;
        //     var table = document.getElementById("myTableData2");

        //     table.deleteRow(index);
        //     total();
        
        // }

        function load() {
        
            console.log("Page load finished");

        }
        

        
    </script>

@endsection
