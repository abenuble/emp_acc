<!-- Modal payment -->
<div class="modal fade payment_type_{{$pro->id_needs}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{language_data('Set Payment Type')}}</h4>
            </div>
            <form class="form-some-up" role="form" method="post" id="form" action="{{url('projects/set-payment-type')}}" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label>{{language_data('Designation')}}</label><br>
                            <input type="text" class="form-control" readonly value="{{$pro->designation_name->designation}}">
                        </div>
                        <div class="form-group">
                            <label>{{language_data('Location')}}</label><br>
                            <input type="text" class="form-control" readonly value="{{$pro->location}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label>{{language_data('Payment Type')}}</label><br>
                            <select name="payment_type" class="form-control selectpicker" id="payment_type" width="200px">
                            @foreach($payment as $pay)
                                <option value="{{$pay->id}}"@if($pro->id_payment==$pay->id) selected @endif>{{$pay->payroll_name}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label>{{language_data('Salary')}}</label><br>
                            <input type="text" class="form-control" value="" name="salary">
                        </div>
                        <div class="form-group">
                            <label>{{language_data('Attendance Allowance')}}</label><br>
                            <input type="text" class="form-control" value="" name="attendance_allowance">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" id="components">
                            <label>{{language_data('Components')}}</label>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="cmd" value="{{$pro->id_needs}}">
                    <input type="hidden" name="proj_id" value="{{$pro->id}}">
                    <input type="hidden" name="des_id" value="{{$pro->id_designation}}">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                    <button type="submit" class="btn btn-primary">{{language_data('Save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>