@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Expense')}}</h2>
        </div>
        <form  role="form" method="post" action="{{url('projects/update-expense')}}" >
        <input type="hidden" value="{{$expense->id}}" name="cmd">
        <input type="hidden" name="_token" value="{{csrf_token()}}">

        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <label>{{language_data('Expense Number')}}</label>
                                        <input type="text" class="form-control" required="" value="{{$expense->expense_number}}" readonly name="expense_number">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Post date')}}</label>
                                        <input type="text" class="form-control datePicker"  @if($expense->status=='Approved' && $expense->total_edit==0) @else readonly @endif  required="" value="{{get_date_format($expense->date)}}" name="post_date">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Company')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$expense->company_info->company}}" required="">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Client Contract')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$expense->project_info->project}}" required="" name="project">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Expense')}}</label>
                                        <input type="text" class="form-control" required="" readonly value="{{$expense->about}}" name="about">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Total')}}</label>
                                        <input type="text" readonly class="form-control alltotal" value="{{number_format($expense->total,0)}}" required="">
                                    </div>
                                    @if($expense->status=='Approved'  && $expense->total_edit==0)
                                    <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="Add" onclick="addRow2();">
                                    @endif
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata2">
                                                <table class="table table-hover table-ultra-responsive" id="myTableData2"  >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 30%;">{{language_data('Item')}}</th>
                                                            <th style="width: 10%;">{{language_data('QTY')}}</th>
                                                            <th style="width: 20%;">{{language_data('Unit Price')}}</th>
                                                            <th style="width: 10%;">PPN</th>
                                                            <th style="width: 20%;">{{language_data('Total Price')}}</th>
                                                            <th style="width: 10%;">{{language_data('Action')}}</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                    <tbody>
                                                    <?php $sub=0; ?>
                                                    @foreach($expense_detail as $ed)
                                                        <tr>
                                                            <td><input type="text" value="{{$ed->item}}"  name="item[]" required class="form-control" @if($expense->status=='Approved' && $expense->total_edit==0) @else readonly @endif ></td>
                                                            <td><input  type="text" name="qty[]" required class="form-control qty" value="{{$ed->qty}}" @if($expense->status=='Approved' && $expense->total_edit==0) @else readonly @endif  ></td>
                                                            <td class="text-right"><input type="text" @if($expense->status=='Approved' && $expense->total_edit==0) @else readonly @endif required name="unit[]" value="{{number_format($ed->unit_price, 2, '.', ',')}}" class="form-control text-right unit">
                                                             <input  type="hidden" name="nominalppn[]"  class="form-control nominalppn" value="{{$ed->ppn*$ed->total_price/100}}" ></td>
                                                             <td><input
                                                             type="text"  name="ppnper[]" required value="{{$ed->ppn}}" class="form-control persenz"  @if($expense->status=='Approved' && $expense->total_edit==0) @else readonly @endif></td>
                                                             <td> <input
                                                             type="text" name="total[]" required value="{{number_format($ed->total_price, 2, '.', ',')}}" class="form-control text-right total money" @if($expense->status=='Approved' && $expense->total_edit==0) @else readonly @endif required ></td>
                                                           
                                                            <td data-label="Action">
                                                                @if($expense->status=='Approved' && $expense->total_edit==0)
                                                                <input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow2(this)">                      
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <?php $sub+=$ed->total_price; ?>
                                                    @endforeach
                                                    
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td style="width: 30%;"></td>
                                                            <td style="width: 10%;"></td>
                                                            <td style="width: 20%;"></td>
                                                            <td style="width: 10%;"><label class="text-right">Sub Total :</label></td>
                                                            <td style="width: 20%;"><input type="text" name="subtotal" value="<?php echo number_format($sub,2); ?>" readonly class="form-control text-right subtotal unit"></td>
                                                            <td style="width: 10%;"></td>
                                                        </tr>
                                  
                                                        <tr>
                                                            <td style="width: 30%;"></td>
                                                            <td style="width: 10%;"></td>
                                                            <td style="width: 20%;"></td>
                                                            <td style="width: 10%;"><label class="text-right">Pembulatan :</label></td>
                                                            <td style="width: 20%;" ><input type="text" name="pembulatan" @if($expense->status=='Approved' && $expense->total_edit==0) @else readonly @endif value="{{number_format($expense->pembulatan,0)}}" class="form-control text-right feee"></td>
                                                            <td style="width: 10%;"></td>
                                                        </tr>
                                                        <tr>
                                                        <input type="hidden" name="fee" readonly value="{{$expense->management_fee}}" class="form-control fee">
                                                            <td style="width: 30%;"></td>
                                                            <td style="width: 10%;"></td>
                                                            <td style="width: 20%;"></td>
                                                            <td style="width: 10%;"><label class="text-right">PPN :</label></td>
                                                            <td style="width: 20%;" ><input type="text" name="ppn" readonly value="{{number_format($expense->ppn_tax,0)}}" class="form-control ppnz text-right unit"></td>
                                                            <td style="width: 10%;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 30%;"></td>
                                                            <td style="width: 10%;"></td>
                                                            <td style="width: 20%;"></td>
                                                            <td style="width: 10%;"><label class="text-right">Total :</label></td>
                                                            <td style="width: 20%;"><input type="text" name="alltotal" value="{{number_format($expense->total,0)}}" readonly class="form-control alltotal text-right unit"></td>
                                                            <td style="width: 10%;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 30%;"></td>
                                                            <td style="width: 10%;"></td>
                                                            <td style="width: 20%;"></td>
                                                            <td style="width: 40%;" colspan="2"><input type="checkbox" class="text-right" name="keterangan" @if($expense->keterangan==1) checked @endif />&nbsp;<label class="text-right" style="color:red;margin-right:0;text-align:right">*Biaya Tagihan Sudah Termasuk PPN</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 30%;"></td>
                                                            <td style="width: 10%;"></td>
                                                            <td style="width: 20%;"></td>
                                                            <td style="width: 10%;"></td>
                                                            <td style="width: 20%;">
                                                            @if($expense->status=='Approved' && $expense->total_edit==0)
                                                            <button type="submit" class="btn btn-success btn-xs pull-right"><i class="fa fa-save"></i> {{language_data('Save')}} </button>
                                                            @endif
                                                            </td>
                                                            <td style="width: 10%;"></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </form>
        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}


    <script>
        $(document).ready(function () {
          
            $('#open_date,#close_date').datetimepicker({
                locale: 'id',
                useCurrent: false,
                format: 'DD MMMM YYYY',
                minDate: moment()
            });
            $('#open_date').datetimepicker().on('dp.change', function (e) {
                var incrementDay = moment(new Date(e.date));
                incrementDay.add(1, 'days');
                $('#close_date').data('DateTimePicker').minDate(incrementDay);
                $(this).data("DateTimePicker").hide();
            });

            $('#close_date').datetimepicker().on('dp.change', function (e) {
                var decrementDay = moment(new Date(e.date));
                decrementDay.subtract(1, 'days');
                $('#open_date').data('DateTimePicker').maxDate(decrementDay);
                    $(this).data("DateTimePicker").hide();
            });
            $("td.number1").each(function(i,v) {
                $(v).text(i + 1);
            });
            $("td.number2").each(function(i,v) {
                $(v).text(i + 1);
            });
            $("td.number3").each(function(i,v) {
                $(v).text(i + 1);
            });
            $("td.number4").each(function(i,v) {
                $(v).text(i + 1);
            });
            $("td.number5").each(function(i,v) {
                $(v).text(i + 1);
            });
            $("td.number6").each(function(i,v) {
                $(v).text(i + 1);
            });
            $("td.number7").each(function(i,v) {
                $(v).text(i + 1);
            });
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/projects/delete/" + id;
                    }
                });
            });

            /*For Delete Project Doc*/
            $(".deleteProjectDoc").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/projects/delete-project-doc/" + id;
                    }
                });
            });

            /*For components Loading*/
            $("#payment_type").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'pay_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/projects/get-component',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#components").html( data);
                    }
                });
            });

           
           $('#myTableData2').on("change",".qty, .unit , .persenz", function() {
                var item_quantity = parseFloat($(this).parents("tr").find(".qty").val());
                var persen = parseFloat($(this).parents("tr").find(".persenz").val());
                var unit_price = parseFloat($(this).parents("tr").find(".unit").val());
                
                var total_value = (unit_price * item_quantity);
                 
                var nominalppn =(unit_price * item_quantity * (persen/100));
                console.info(item_quantity, unit_price, total_value);
                if(!isNaN(nominalppn))
                $(this).parents("tr").find(".nominalppn").val(nominalppn);
             
                if(!isNaN(total_value))
                    $(this).parents("tr").find(".total").val(total_value);
                    $('.total').number( true, 0, '.', ',' );

                    total();
            });
              $('.feee').keyup(function(){
                  var fee=parseFloat($(".feee").val());
                  if(isNaN(fee)){fee=0;}
                var subtotal = parseFloat($(".subtotal").val());
                if(isNaN(subtotal)){subtotal=0;}
                var ppn = parseFloat($(".ppnz").val());
                if(isNaN(ppn)){ppn=0;}
                var alltotal = fee + subtotal+ ppn;
                if(!isNaN(alltotal)){
                    $(".alltotal").val(alltotal);}
              });

            $('#myTableData3').on("change",".fee, .ppn", function() {
                var fee = parseFloat($(".fee").val());
                var ppn = parseFloat($(".ppn").val());
                var subtotal = parseFloat($(".subtotal").val());
                var alltotal = subtotal + fee - (fee*ppn/100);
                console.info(fee, ppn, alltotal);
                if(!isNaN(alltotal))
                    $(".alltotal").val(alltotal.toFixed(2));
            });

        });      

        function getSelectedText(elementId) {
            var elt = document.getElementById(elementId);

            if (elt.selectedIndex == -1)
                return null;

            return elt.options[elt.selectedIndex].text;
        } 
        function tandaPemisahTitik(b){
            var _minus = false;
            if (b<0) _minus = true;
            b = b.toString();
            b=b.replace(".","");
            b=b.replace("-","");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--){
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)){
            c = b.substr(i-1,1) + "." + c;
            } else {
            c = b.substr(i-1,1) + c;
            }
            }
            if (_minus) c = "-" + c ;
            return c;
        }
        function addRow() {
         
            var designation = document.getElementById("designation");
            var textdesg = getSelectedText('designation');
            var location = document.getElementById("location");
            var open_date = document.getElementById("open_date");
            var close_date = document.getElementById("close_date");

            var desg = document.getElementById("desg");
            var length = document.getElementsByName("loc").length;
            var sama = length+1;
            var loc = document.getElementsByName("loc");

            var i = 0;

            //lokasi.push(location.value);

            var total = document.getElementById("total");
            var table = document.getElementById("myTableData");

            if(open_date.value==='' || close_date.value==='' || total.value===''){
                return false;
            } 

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            row.insertCell(0).innerHTML= '<input type="hidden" class="form-control" value="'+designation.value+'" name="designation[] id="desg"><input type="text" readonly class="form-control" value="'+textdesg+'">';
            row.insertCell(1).innerHTML= '<input type="hidden" readonly class="form-control" value="'+location.value+'" name="location[]"><input type="text" readonly class="form-control" value="'+location.value+'" name="loc">';
            row.insertCell(2).innerHTML= '<input type="text" readonly class="form-control" value="'+open_date.value+'" name="open_date[]">';
            row.insertCell(3).innerHTML= '<input type="text" readonly class="form-control" value="'+close_date.value+'" name="close_date[]">';
            row.insertCell(4).innerHTML= '<input type="text" class="form-control" value="'+total.value+'" name="total[]">';
            row.insertCell(5).innerHTML= '<input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';


        }


        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData");
            table.deleteRow(index);
        
        }

          function total() {
            
            var bulat = parseFloat($('.feee').val());
            if(isNaN(bulat)){bulat=0;}
            var total1 =  0;
           var sub=0;
           var ppntot=0;
           var ppntot=0;
            $('input.total').each(function () {
              
                var n = parseFloat($(this).val());
                sub+= isNaN(n) ? 0 : n;
                total1 += isNaN(n) ? 0 : n ;
                console.log(n);
             
            });
            $('input.nominalppn').each(function () {
                var y = parseFloat($(this).val());
                total1+= isNaN(y) ? 0 : y;
                ppntot+=  isNaN(y) ? 0 : y;
                console.log(y);
            });
            total1+=bulat;
            $('.ppnz').val(ppntot);
            $('.subtotal').val(sub);
            $('.alltotal').val(total1);
        }

      function addRow2() {
         
         $('#myTableData2').append('<tr>\
            <td>\
                <input type="text" name="item[]" required class="form-control">\
            </td>\
            <td>\
                <input type="text" name="qty[]" required class="form-control qty">\
            </td>\
            <td>\
                <input type="text" name="unit[]"  required class="form-control text-right unit">\
                <input type="hidden" name="nominalppn[]"  class="form-control nominalppn">\
            </td>\
            <td>\
            <input type="text"  name="ppnper[]" required value="10" class="form-control persenz">\
            </td>\
            <td>\
                <input type="text" name="total[]" required class="form-control text-right total money">\
            </td>\
            <td data-label="Action">\
                <input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow2(this)">\
            </td>\
         </tr>');
         $('.unit').number( true, 0, '.', ',' );
         $('.nominalppn').number( true, 0, '.', ',' );
         $('.qty').number( true, 0, '.', ',' );
         $('.persenz').number( true, 0, '.', ',' );
         
     }

$('.unit').number( true, 0, '.', ',' );
$('.feee').number( true, 0, '.', ',' );


        function deleteRow2(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData2");
            table.deleteRow(index);
            total();
        
        }

        function load() {
        
            console.log("Page load finished");

        }
        

        
    </script>

@endsection