@extends('master')

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Edit Vehicle Type</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <form class="form-some-up" role="form" method="post" id="form" action="{{url('projects/set-vehicle')}}" enctype="multipart/form-data">
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{language_data('Employee Code')}}</label><br>
                                                <input type="text" class="form-control" name="" readonly required value="{{$employee->employee_code}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{language_data('Name')}}</label><br>
                                                <input type="text" class="form-control" name="" readonly required value="{{$employee->fname}} {{$employee->lname}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{language_data('Phone Number')}}</label><br>
                                                <input type="text" class="form-control" name="" readonly required value="{{$employee->phone}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{language_data('Brand')}}</label><br>
                                                <input type="text" class="form-control" name="brand"  required value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{language_data('Type')}}</label><br>
                                                <input type="text" class="form-control" name="type"  required value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>TMT</label><br>
                                                <input type="text" class="form-control datePicker" name="tmt"  required value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{language_data('License Plate')}}</label><br>
                                                <input type="text" class="form-control" name="license_plate"  required value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{language_data('Vehicle Registration Date')}}</label><br>
                                                <input type="text" class="form-control datePicker" name="vehicle_registration_date"  required value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{language_data('Tax Due Date')}}</label><br>
                                                <input type="text" class="form-control datePicker" name="tax_due_date"  required value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{language_data('Insurance Vendor')}}</label><br>
                                                <input type="text" class="form-control" name="insurance_vendor"  required value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{language_data('Vehicle Vendor')}}</label><br>
                                                <input type="text" class="form-control" name="vehicle_vendor"  required value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{language_data('Car Rental Value')}}</label><br>
                                                <input type="text" class="form-control" name="car_rental_value"  required value="">
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="cmd" value="{{$employee->id}}">
                                    <button type="submit" class="btn btn-sm pull-right btn-primary">{{language_data('Save')}}</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
@endsection
