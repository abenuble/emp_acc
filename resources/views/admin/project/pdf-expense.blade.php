<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - EXPENSES</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<head>
    <style>
        .help-split {
            display: inline-block;
            width: 30%;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Expense')}}</h2>
        </div>
        <form >
  

        <div class="p-30 p-t-none p-b-none">

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <label>{{language_data('Expense Number')}} : </label>{{$expense->expense_number}}
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Post date')}} : </label> {{get_date_format($expense->date)}}
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Company')}} :</label>{{$expense->company_info->company}}
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Client Contract')}} : </label>{{$expense->project_info->project}}
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Expense')}} : </label>{{$expense->about}}
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Total')}} : </label> {{number_format($expense->total,0)}}
                                    </div>
                                
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata2">
                                                <table class="table table-hover table-ultra-responsive" id="myTableData2"  >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 30%;">{{language_data('Item')}}</th>
                                                            <th style="width: 20%;">{{language_data('QTY')}}</th>
                                                            <th style="width: 20%;">{{language_data('Unit Price')}}</th>
                                                           
                                                            <th style="width: 20%;">{{language_data('Total Price')}}</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                    <tbody>
                                                    <?php $sub=0; ?>
                                                    @foreach($expense_detail as $ed)
                                                        <tr>
                                                            <td>{{$ed->item}}</td>
                                                            <td> {{$ed->qty}}</td>
                                                            <td>{{$ed->unit_price}}</td>
                                                          
                                                            <td>{{$ed->total_price}}</td>
                                                  
                                                        </tr>
                                                        <?php $sub+=$ed->total_price; ?>
                                                    @endforeach
                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata2">
                                                <table class="table table-hover table-ultra-responsive" id="myTableData3">
                                                    <tr>
                                                        <td style="width: 30%;"></td>
                                                        <td style="width: 20%;"></td>
                                                        <td style="width: 20%;"><center><label>Sub Total :</label></center></td>
                                                        <td style="width: 20%;"><?php echo number_format($sub,2); ?></td>
                                                    </tr>
                              
                                                    <tr>
                                                        <td style="width: 30%;"></td>
                                                        <td style="width: 20%;"></td>
                                                        <td style="width: 20%;"><center><label>PPN :</label></center></td>
                                                        <td style="width: 20%;">{{number_format($expense->ppn_tax,0)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%;"></td>
                                                        <td style="width: 20%;"></td>
                                                        <td style="width: 20%;"><center><label>Total :</label></center></td>
                                                        <td style="width: 20%;">{{number_format($expense->total,0)}}</td>
                                                    </tr>
                                                    @if($expense->keterangan==1)
                                                    <tr>
                                                        <td style="width: 30%;"></td>
                                                        <td style="width: 30%;"></td>
                                                        <td style="width: 30%;"><center><label style="color:red;margin-right:0;text-align:right">*Biaya Tagihan Sudah Termasuk PPN </label></center></td>
                                                    </tr>
                                                    @endif
                                                    <tr>
                                                        <td style="width: 30%;"></td>
                                                        <td style="width: 20%;"></td>
                                                        <td style="width: 20%;"></td>
                                                        <td style="width: 20%;">
                                                     
                                                        </td>
                                                    </tr>
                                                    
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </form>
        </div>
        </main>
        <p style="page-break-after: always;">&nbsp;</p>

        <main id="wrapper" class="wrapper">
                <div class="p-30">
                <img src="<?php echo public_path('assets/company_pic/usaha-patra-lima-jaya-pt.jpg');?>"  style=" display: block;
            margin: 0 auto;" alt="Profile Page" width="200px" height="150px">
                </div>
                <form >
          
        
                <div class="p-30 p-t-none p-b-none">
        
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel">
                                <div class="panel-body">
        
                                    <div class="row">
                                        <div class="col-lg-12">
        
                                            <div class="form-group">
                                                <label>No : </label >&nbsp;{{$expense->id}}<br>
                                                <label>Sudah Terima Dari : </label >&nbsp;{{$expense->purchase_form}}
                                            </div>
        
                                            
                                        
                                        </div>
                                    </div>
        
        
                                   
                                    <div class="row">
                                        <div class="col-xs-4">
        
                                            <div class="form-group">
                                               <input type="text" value="Rp. {{number_format($expense->total,0)}}" class="form-control"/>
                                            </div>
        
                                            
                                        
                                        </div>
                                        <div class="col-xs-8">
        
                                            <div class="form-group">
                                            <input type="text" value="{{$kata}}" class="form-control"/>
                                            </div>
        
                                            
                                        
                                        </div>
                                    </div>
        
                                    <div class="row">
                                        <div class="col-xs-4">
        
                                            <div class="form-group">
                                            <label>Menurut Uraian Disebelah : </label >
                                            </div>
        
                                            
                                        
                                        </div>
                                        <div class="col-xs-8">
        
                                            <div class="form-group">
                                            <input type="text" value="{{$expense->about}}" class="form-control"/>
                                            </div>
        
                                            
                                        
                                        </div>
                                    </div>
        
                                      <div class="row">
                                        <div class="col-xs-8">
        
                                            <div class="form-group">
                                           Tanggal Transaksi  {{get_date_format($expense->purchase_date)}}
                                            </div>
        
                                            
                                        
                                        </div>
                                        <div class="col-xs-4 pull-left">
        
                                            <div class="form-group">
                                        Surabaya,{{get_date_format(date('Y-m-d'))}} <br>
                                        Direktur,
                                            </div>
        
                                            
                                        
                                        </div>
                                    </div>
        <br>
        <br>
        <br>
          <div class="row">
          <div class="col-xs-8">
        
        
        </div>
        <div class="col-xs-4 pull-left">
        
        <div class="form-group">
        MOCHAMMAD MUSI
        </div>
        
        
        
        </div>
          </div>
        
                                    </div>
        
                                </div>
                            </div>
                        </div>
                    </div>
        
                </form>
                </div>
                </main>

</body>
</html>

