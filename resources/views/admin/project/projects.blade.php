@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Client Contract')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                    <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Client Contract')}}</h3><br>
                            <a><button class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#download">{{language_data('Download Data')}}</button></a>
                            @if($permcheck->C==1)       
                            <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload">{{language_data('Upload')}}</button></a>             

                            <a><button class="btn btn-success btn-sm pull-right tambahproyek" data-toggle="modal" data-target="#projects"><i class="fa fa-plus"></i> {{language_data('Add New Client Contract')}}</button></a>
                            @endif
                            <br>
                            <!-- <a class="btn btn-primary btn-sm pull-right"  href="{{url('projects/downloadExcelProject')}}">{{language_data('Download Data')}}</a>     -->

                           
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 15%;">{{language_data('Client Contract Number')}}</th>
                                    <th style="width: 40%;">{{language_data('Company')}}</th>
                                    {{-- <th style="width: 15%;">Kategori</th> --}}
                                    <th style="width: 10%;">{{language_data('Start Date')}}</th>
                                    <th style="width: 10%;">{{language_data('End Date')}}</th>
                                    <th style="width: 10%;">{{language_data('Facility')}}</th>
                                    <th style="width: 5%;">{{language_data('Status')}}</th>
                                    <th style="width: 5%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($projects as $ctr => $p)
                                    <tr>
                                        <td data-label="No">{{$ctr+1}}</td>
                                        <td data-label="Project Number"><p>{{$p->project_number}}</p></td>
                                        <td data-label="Company"><p>{{$p->company_name->company}}</p></td>
                                        {{-- <td data-label="Kategori"><p>{{$p->category}}</p></td> --}}
                                        <td data-label="Start Date"><p>{{get_date_format($p->start_date)}}</p></td>
                                        <td data-label="End Date"><p>{{get_date_format($p->end_date)}}</p></td>
                                        <td data-label="Facility"><p>{{json_decode($p->facility)[0]}}@if(isset(json_decode($p->facility)[1])), {{json_decode($p->facility)[1]}} @endif</p></td>
                                        @if($p->status=='opening')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Open')}}@if($p->end_date<date('Y-m-d')) : {{language_data('End Date Passed')}} @endif</p></td>
                                            <td data-label="Actions">
                                                <div class="btn-group btn-mini-group dropdown-default">
                                                    <a class="btn btn-success btn-sm dropdown-toggle btn-animated from-top fa fa-caret-down" data-toggle="dropdown" href="#" aria-expanded="false"><span><i class="fa fa-bars"></i></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{url('projects/view/'.$p->id)}}" data-toggle="tooltip" data-placement="right" title="{{language_data('View')}}" class="text-success"><i class="fa fa-eye"></i></a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        @elseif($p->status=='closed')
                                            <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Closed')}}</p></td>
                                            <td data-label="Actions">
                                                <div class="btn-group btn-mini-group dropdown-default">
                                                    <a class="btn btn-success btn-sm dropdown-toggle btn-animated from-top fa fa-caret-down" data-toggle="dropdown" href="#" aria-expanded="false"><span><i class="fa fa-bars"></i></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{url('projects/view/'.$p->id)}}" data-toggle="tooltip" data-placement="right" title="{{language_data('View')}}" class="text-success"><i class="fa fa-eye"></i></a></li>
                                                        @if($permcheck->C==1)
                                                        <li><a href="{{url('projects/view/'.$p->id)}}" data-toggle="modal" data-target="#projects" href="#" data-id="{{$p->id}}" data-placement="right" title="Duplicate" class="text-success links"><i class="fa fa-copy"></i></a></li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            </td>
                                        @else
                                            <td data-label="Status"><p class="btn btn-info btn-xs">{{language_data('Drafted')}}</p></td>
                                            <td data-label="Actions">
                                                <div class="btn-group btn-mini-group dropdown-default">
                                                    <a class="btn btn-success btn-sm dropdown-toggle btn-animated from-top fa fa-caret-down" data-toggle="dropdown" href="#" aria-expanded="false"><span><i class="fa fa-bars"></i></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{url('projects/view/'.$p->id)}}" data-toggle="tooltip" data-placement="right" title="{{language_data('View')}}" class="text-success"><i class="fa fa-eye"></i></a></li>
                                                        <li><a href="#" data-toggle="modal" data-target=".modal_edit_project_{{$p->id}}" data-placement="right" title="{{language_data('Edit')}}" class="text-success"><i class="fa fa-edit"></i></a></li>
                                                        <li><a href="#" data-toggle="tooltip" data-placement="right" title="{{language_data('Delete')}}" class="text-success cdelete"  id="{{$p->id}}"><i class="fa fa-trash"></i></a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                            @include('admin.project.modal-edit-project')

                                        @endif
                                        

                                        
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal add -->
            <div class="modal fade" id="projects" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Client Contract')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post"  id="addprojectt" action="{{url('projects/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">
                            <input type="hidden" name="neworduplicate" class="neworduplicate" />
                            <input type="hidden" name="daftarkaryawan" class="daftarkaryawan" />
                                <div class="form-group">
                                    <label>{{language_data('Internal Company')}}</label>
                                    <input type="text" class="form-control" readonly value="{{$internal_company->company_name->company}}"  >
                                    <input type="hidden" class="form-control" readonly value="{{$internal_company->company_name->id}}"   name="internal_company" >
                                </div>
                                <div class="form-group">
                                    <label>{{language_data('Client Contract Type')}}</label>
                                    <div class="radio">
                                        <label><input type="radio" id="project_type1" name="project_type" onclick="pnumber()" value="internal">Internal</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" id="project_type2" name="project_type" onclick="pnumber()" value="external">External</label>
                                    </div>  
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <select name="company" class="form-control selectpicker companyy" id="company_id" data-live-search="true">
                                        @foreach($company as $c)
                                            <option value="{{$c->id}}">{{$c->company}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Client Contract Number')}}</label>
                                    <input type="text" class="form-control" required  name="project_number" id="project_number" value="">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Client Contract Name')}}</label>
                                    <input type="text" class="form-control namaproyek" required="" name="projects" value="">
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Facility')}}</label>
                                            <div class="checkbox">
                                                <label><input type="checkbox" id="facility1" class="facility" name="facility[]" value="kesehatan">{{language_data('Kesehatan')}}</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" id="facility2" class="facility" name="facility[]" value="ketenagakerjaan">{{language_data('ketenagakerjaan')}}</label>
                                            </div>  
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group kesehatan" style="display:none;">
                                            <label>{{language_data('Type of Health Facilities')}}</label>
                                            <select name="health_facility_type" class="form-control selectpicker" id="health_facility_type" data-live-search="true">
                                                <option value="BPJS Kesehatan">{{language_data('BPJS Health')}}</option>
                                                <option value="Inhealth">{{language_data('Inhealth')}}</option>
                                                <option value="other">{{language_data('Other Health Insurance')}}</option>
                                            </select>
                                        </div>

                                        <div class="form-group ketenagakerjaan" style="display:none;">
                                            <label>{{language_data('Type of Employment Facilities')}}</label><br>
                                            <input type="checkbox" name="accident_insurance" value="1"> {{language_data('Work Accident Insurance (JKK)')}}<br>
                                            <input type="checkbox" name="life_insurance" value="1"> {{language_data('Death Insurance (JKM)')}}<br>
                                            <input type="checkbox" name="jaminan_hari_tua" value="1"> {{language_data('Old Age Guarantee (JHT)')}}<br>
                                            <input type="checkbox" name="jaminan_pensiun" value="1"> {{language_data('Pension Guarantee (JP)')}}<br>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group kesehatan" style="display:none;">
                                            <label>{{language_data('Name of Health Insurance')}}</label>
                                            <input type="text" class="form-control" name="insurance_name" id="insurance_name" value="">
                                        </div>
                                    </div>
                                </div>
                                
                                {{-- <div class="form-group">
                                    <label>{{language_data('Sub Category')}}</label>
                                    <select name="category" class="form-control selectpicker subcategoryy" data-live-search="true">
                                        @foreach($subcategories as $c)
                                            <option value="{{$c->id}}">{{$c->subcategory}}</option>
                                        @endforeach
                                    </select>
                                </div> --}}

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Start Date')}}</label>
                                            <input type="text" class="form-control startdate" required="" name="start_date" id="start_date">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('End Date')}}</label>
                                            <input type="text" class="form-control enddate" required="" name="end_date" id="end_date">
                                        </div>
                                    </div>
                                </div>
                                
                                {{-- <div class="form-group">
                                    <label>{{language_data('Value')}}</label>
                                    <input type="text" class="form-control nilaii" name="value" id="value" required="" value="">
                                </div> --}}

                                <div class="form-group">
                                    <label>{{language_data('Contract File')}}</label>
                                    <div class="input-group">
                                        <input name="contract_file" class="form-control" type="file">
                                        <input type="hidden" name="contract_document_name" value="Contract">
                                    </div>
                                </div>

                                {{-- <div class="form-group">
                                    <label>{{language_data('Bid File')}}</label>
                                    <div class="input-group">
                                        <input name="bid_file" class="form-control" type="file">
                                        <input type="hidden" name="bid_document_name" value="Bid">
                                    </div>
                                </div> --}}

                                <div class="form-group">
                                    <label>File Perjanjian</label>
                                    <div class="input-group">
                                        <input name="agreement_file" class="form-control" type="file">
                                        <input type="hidden" name="agreement_document_name" value="Agreement">
                                    </div>
                                </div>

                                <hr>

                                <h3 class="panel-title">{{language_data('Client Contract Detail')}}</h3>
                                <div id="myform">

                                    <div class="form-group">
                                        <label>{{language_data('Designation')}}</label>
                                        <select name="" class="form-control selectpicker" data-live-search="true" id="designation">
                                                <option value=''>{{language_data('Select Designation')}}</option>
                                            @foreach($designation as $d)
                                                <option value='{{$d->id}}'>{{$d->designation}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Location')}}</label>
                                        <input type="text" class="form-control" name="" id="location" value="">
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Open Date')}}</label>
                                            <input type="text" class="form-control" name="" id="open_date">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Close Date')}}</label>
                                            <input type="text" class="form-control" name="" id="close_date">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Quota')}}</label>
                                        <input type="number" min="0" class="form-control" name="" id="total" value="">
                                    </div>
                                    <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="{{language_data('Add')}}" onclick="Javascript:addRow()">
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata">
                                                <table class="table table-hover table-ultra-responsive" id="myTableData"  >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 20%;">{{language_data('Designation')}}</th>
                                                            <th style="width: 20%;">{{language_data('Location')}}</th>
                                                            <th style="width: 20%;">{{language_data('Open Date')}}</th>
                                                            <th style="width: 20%;">{{language_data('Close Date')}}</th>
                                                            <th style="width: 5%;">{{language_data('Quota')}}</th>
                                                            <th style="width: 15%;">{{language_data('Action')}}</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                    
                                    

                                </div>

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" tabindex="-1" role="dialog"  id='mydiv'>
        <form class="form-some-up" role="form" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
            <img src="<?php echo asset('assets/img/loading_spinner.gif');?>" alt="Wait" style='margin:auto;'  />
            </div>
            </div>
            </form>
            </div>
            
             <!-- Modal Upload -->
             <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('projects/importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file" accept="application/vnd.ms-excel"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
 
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('projects/downloadExcel/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" tabindex="-1" role="dialog" id='mydiv'>
            <img src="<?php echo asset('assets/img/loading_spinner.gif');?>" alt="Wait" />
            </div>

                <div class="modal fade" id="download" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Download')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{url('projects/downloadExcelProject')}}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                                      
                                <div class="row">
                                    <div class="col-lg-6" >
                                        <label>{{language_data('Company')}}</label>
                                        <select name="company" class="form-control selectpicker" data-live-search="true">
                                                <option value='0'>{{language_data('All Company')}}</option>
                                            @foreach($company as $d)
                                                <option value='{{$d->id}}' >{{$d->company}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="checkalll" >Check All</label>
                                        </div> 
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="project_number">{{language_data('Client Contract Number')}}</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="project">{{language_data('Client Contract Name')}}</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="company">{{language_data('Company')}}</label>
                                        </div>    
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="start_date">{{language_data('Start Date')}}</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="end_date">{{language_data('End Date')}}</label>
                                        </div>          
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="facility">{{language_data('Facility')}}</label>
                                        </div> 
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="health_facility_type">{{language_data('Type of Health Facilities')}}</label>
                                        </div>  
                                    </div>

                                    <div class="col-lg-4">    
                                        {{-- <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="value">{{language_data('Value')}}</label>
                                        </div>    --}}
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="status">Status</label>
                                        </div>  
                                     
                                    </div>
                                </div>
                                
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Download')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

    <script>
		
        $(document).ready(function () {
            
            $("#addprojectt").submit(function(e) {
                var dialog = bootbox.dialog({
                    message: '<p class="text-center">Checking...</p>',
                    closeButton: false
                });

                var _url = $("#_url").val();
                var dataString = 'project_number=' + $( "input[name='project_number']" ).val();
                event.preventDefault();
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/projects/checkexist',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        // if(data=='0'){
                        //     dialog.modal('hide');
                        //     var dialogz = bootbox.dialog({
                        //         title: 'Failed',
                        //         message: '<p>Data sudah ada</p>'
                        //     });
                        //     dialogz.init(function(){
                        //         setTimeout(function(){
                        //             dialogz.modal('hide');
                        //         }, 1000);
                        //     });
                        // }
                        // else{
                            var form = $('#addprojectt')[0];
                            var _url = $("#_url").val();

                            // Create an FormData object 
                            var data = new FormData(form);
                            $.ajax({
                                type: "POST",
                                enctype: 'multipart/form-data',
                                url: _url + "/projects/add",
                                data: data,
                                processData: false,
                                contentType: false,
                                cache: false,
                                timeout: 600000,
                                success: function (data) {
                                    dialog.modal('hide');
                                    alert('sukses Insert!');
                                    location.reload();

                                }
                            });
                        // }
                    }
                });

            });
            

            /*For type Loading*/
            $("#company_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'comp_id=' + id;
                if(document.getElementById("project_type1").checked == true){
                    $.ajax
                    ({
                        type: "POST",
                        url: _url + '/projects/get-area-code',
                        data: dataString,
                        cache: false,
                        success: function ( data ) {
                            $("#project_number").val( data);
                        }
                    });
                } else {
                    var data = '';
                    $("#project_number").val(data);
                }
            });
            $('#checkalll').change(function() {
                if(this.checked) {
                    $('input:checkbox[name="variable[]"]').prop('checked',true);
                }
                else{
                    $('input:checkbox[name="variable[]"]').prop('checked',false);

                }
                    
            });
            $('.facility').click(function() {
                if(document.getElementById("facility1").checked == true){
                    $('.kesehatan').show();
                    var asuransi = $('#health_facility_type').val();
                    $('#insurance_name').val(asuransi);
                } else {
                    $('.kesehatan').hide();
                }
                if(document.getElementById("facility2").checked == true){
                    $('.ketenagakerjaan').show();
                } else {
                    $('.ketenagakerjaan').hide();
                }
            });
            $('#health_facility_type').change(function() {
                var asuransi = $('#health_facility_type').val();
                $('#insurance_name').val(asuransi);
                    
            });
            $('.links').click(function(){
                $(".neworduplicate").val('duplicate');
                $(".startdate").val("");
                $(".enddate").val("");
                var elem = $(this);
                var _url = $("#_url").val();
                $("#myTableData tr").remove();
                var dataString = 'id=' + elem.attr('data-id');
               
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/projects/get-detail',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        var hsl=JSON.parse(data);
                        $('.daftarkaryawan').val(JSON.stringify(hsl.employee));

                        console.log(hsl);
                        $(".namaproyek").val(hsl.project.project);
                        $(".companyy").val(hsl.project.company).trigger("change");
                        $(".categoryy").val(hsl.project.category).trigger("change");
                        $(".subcategoryy").val(hsl.project.subcategory).trigger("change");
                   
                        $(".nilaii").val(hsl.project.value);
                        console.log(hsl.project_need);
                        for(var i=0; i<hsl.project_need.length;i++){
                            var table = document.getElementById("myTableData");
                            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);
            var namades="";
           for(var j=0;j<hsl.des.length;j++){
               if(hsl.project_need[i].id_designation==hsl.des[j].id){
                   namades=hsl.des[j].designation;
               }
           }
          
                    row.insertCell(0).innerHTML= '<input type="hidden" class="form-control" value="'+hsl.project_need[i].id_designation+'" name="designation[] id="desg"><input type="text" readonly class="form-control" value="'+namades+'">';
            row.insertCell(1).innerHTML= '<input type="hidden" readonly class="form-control" value="'+hsl.project_need[i].location+'" name="location[]"><input type="text" readonly class="form-control" value="'+hsl.project_need[i].location+'" name="loc">';
            row.insertCell(2).innerHTML= '<input type="text" readonly class="form-control" value="'+hsl.project_need[i].open_date+'" name="open_date[]">';
            row.insertCell(3).innerHTML= '<input type="text" readonly class="form-control" value="'+hsl.project_need[i].close_date+'" name="close_date[]">';
            row.insertCell(4).innerHTML= '<input type="number" min="0" class="form-control" value="'+hsl.project_need[i].total+'" name="total[]">';
            row.insertCell(5).innerHTML= '<input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';
                            
                        }
                      
                    }
                });
               
                
              
            });
            $(".tambahproyek").click(function(){
                $('.neworduplicate').val('new');

                $("#myTableData tr").remove();
                $(".namaproyek").val("");
                        $(".companyy").val("0").trigger("change");
                        $(".categoryy").val("0").trigger("change");
                        $(".subcategoryy").val("0").trigger("change");
                        $(".startdate").val("");
                        $(".enddate").val("");
                        $(".nilaii").val("");
            });
            $('#start_date,#end_date').datetimepicker({
                locale: 'id',
                useCurrent: false,
                format: 'DD MMMM YYYY',
            });
            $('#start_date').datetimepicker().on('dp.change', function (e) {
              
                $(this).data("DateTimePicker").hide();
            });

            $('#end_date').datetimepicker().on('dp.change', function (e) {
                var decrementDay = moment(new Date(e.date));
                $('#start_date').data('DateTimePicker').maxDate(decrementDay);
                    $(this).data("DateTimePicker").hide();
            });
            
            $('#open_date,#close_date').datetimepicker({
                locale: 'id',
                useCurrent: false,
                format: 'DD MMMM YYYY'
            });
            $('#open_date').datetimepicker().on('dp.change', function (e) {
            
                $(this).data("DateTimePicker").hide();
            });

            $('#close_date').datetimepicker().on('dp.change', function (e) {
                var decrementDay = moment(new Date(e.date));
                $('#open_date').data('DateTimePicker').maxDate(decrementDay);
                    $(this).data("DateTimePicker").hide();
            });
            
            // Set up the number formatting.
            $('#value').number( true, 0, '.', ',' );
            /*For DataTable*/
            $('.data-table').DataTable();

          


        });
  /*For Delete Job Info*/
  $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/projects/delete/" + id;
                    }
                });
            });
        function getSelectedText(elementId) {
            var elt = document.getElementById(elementId);

            if (elt.selectedIndex == -1)
                return null;

            return elt.options[elt.selectedIndex].text;
        }

        function pnumber(){
            var id = $('#company_id').val();
            var _url = $("#_url").val();
            var dataString = 'comp_id=' + id;
            if(document.getElementById("project_type1").checked == true){
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/projects/get-area-code',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#project_number").val( data);
                    }
                });
            } else {
                var data = '';
                $("#project_number").val(data);
            }
        }

        

        function addRow() {
         
            var designation = document.getElementById("designation");
            var textdesg = getSelectedText('designation');
            var location = document.getElementById("location");
            var open_date = document.getElementById("open_date");
            var close_date = document.getElementById("close_date");

            var desg = document.getElementById("desg");
            var length = document.getElementsByName("loc").length;
            var sama = length+1;
            var loc = document.getElementsByName("loc");

            var lokasi = ["test"];
            var i = 0;

            lokasi.splice(sama,0,loc[length]);

            var total = document.getElementById("total");
            var table = document.getElementById("myTableData");

            if(open_date.value==='' || close_date.value==='' || total.value===''){
                return false;
            } 

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            row.insertCell(0).innerHTML= '<input type="hidden" class="form-control" value="'+designation.value+'" name="designation[] id="desg"><input type="text" readonly class="form-control" value="'+textdesg+'">';
            row.insertCell(1).innerHTML= '<input type="hidden" readonly class="form-control" value="'+location.value+'" name="location[]"><input type="text" readonly class="form-control" value="'+location.value+'" name="loc">';
            row.insertCell(2).innerHTML= '<input type="text" readonly class="form-control" value="'+open_date.value+'" name="open_date[]">';
            row.insertCell(3).innerHTML= '<input type="text" readonly class="form-control" value="'+close_date.value+'" name="close_date[]">';
            row.insertCell(4).innerHTML= '<input type="number" min="0" class="form-control" value="'+total.value+'" name="total[]">';
            row.insertCell(5).innerHTML= '<input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';

        }

        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData");
            table.deleteRow(index);
        
        }

        function load() {
        
            console.log("Page load finished");

        }
        
        
    </script>
@endsection
