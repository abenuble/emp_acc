@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">View Vehicle</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Employee Code')}}</label><br>
                                        <input type="text" class="form-control" name="" readonly required value="{{$vehicle->employee_info->employee_code}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Name')}}</label><br>
                                        <input type="text" class="form-control" name="" readonly required value="{{$vehicle->employee_info->fname}} {{$vehicle->employee_info->lname}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Phone Number')}}</label><br>
                                        <input type="text" class="form-control" name="" readonly required value="{{$vehicle->employee_info->phone}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Brand')}}</label><br>
                                        <input type="text" class="form-control" name="brand"  required value="{{$vehicle->brand}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Type')}}</label><br>
                                        <input type="text" class="form-control" name="type"  required value="{{$vehicle->type}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>TMT</label><br>
                                        <input type="text" class="form-control datePicker" name="tmt"  required value="{{get_date_format($vehicle->tmt)}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('License Plate')}}</label><br>
                                        <input type="text" class="form-control" name="license_plate"  required value="{{$vehicle->license_plate}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Vehicle Registration Date')}}</label><br>
                                        <input type="text" class="form-control datePicker" name="vehicle_registration_date"  required value="{{get_date_format($vehicle->vehicle_registration_date)}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Tax Due Date')}}</label><br>
                                        <input type="text" class="form-control datePicker" name="tax_due_date"  required value="{{get_date_format($vehicle->tax_due_date)}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Insurance Vendor')}}</label><br>
                                        <input type="text" class="form-control" name="insurance_vendor"  required value="{{$vehicle->insurance_vendor}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Vehicle Vendor')}}</label><br>
                                        <input type="text" class="form-control" name="vehicle_vendor"  required value="{{$vehicle->vehicle_vendor}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Car Rental Value')}}</label><br>
                                        <input type="text" class="form-control" name="car_rental_value"  required value="{{$vehicle->car_rental_value}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });       


        
    </script>

@endsection