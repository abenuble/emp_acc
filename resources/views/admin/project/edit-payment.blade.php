@extends('master')

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Edit Payment Type')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <form class="form-some-up" role="form" method="post" id="form" action="{{url('projects/set-payment-type')}}" enctype="multipart/form-data">
                                    <br>

                                    <div class="row">
                                        <div class="form-group">
                                            <label>{{language_data('Designation')}}</label><br>
                                            <input type="text" class="form-control" readonly value="{{$project_needs->designation_name->designation}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{language_data('Location')}}</label><br>
                                            <input type="text" class="form-control" readonly value="{{$project_needs->location}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label>{{language_data('Payment Type')}}</label><br>
                                            <select name="payment_type" class="form-control selectpicker" id="payment_type" width="200px">
                                            <option value="0">{{language_data('Select Payroll Type')}}</option>
                                            @foreach($payroll_type as $pay)
                                                <option value="{{$pay->id}}"@if($project_needs->id_payment==$pay->id) selected @endif>{{$pay->payroll_name}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group" id="components">
                                            <label>{{language_data('Components')}}</label>
                                        </div>
                                    </div>


                                    <hr>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="cmd" value="{{$project_needs->id_needs}}">
                                    <input type="hidden" name="proj_id" value="{{$project_needs->id}}">
                                    <input type="hidden" name="des_id" value="{{$project_needs->id_designation}}">
                                    <button type="submit" class="btn btn-sm pull-right btn-primary">{{language_data('Save')}}</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}
    <script>
        $(document).ready(function () {

            /*For components Loading*/
            $("#payment_type").ready(function () {
                var id = {{$project_needs->id_payment}};
                var _url = $("#_url").val();
                var dataString = 'pay_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/projects/get-component',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#components").html( data);
                    }
                });
            });

            /*For components Loading*/
            $("#payment_type").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'pay_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/projects/get-component',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#components").html( data);
                    }
                });
            });

        });   
    </script>
@endsection
