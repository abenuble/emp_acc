<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - EXPENSES</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<head>
    <style>
        .help-split {
            display: inline-block;
            width: 30%;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
        <div class="p-30">
        <img src="<?php echo public_path('assets/company_pic/usaha-patra-lima-jaya-pt.jpg');?>"  style=" display: block;
    margin: 0 auto;" alt="Profile Page" width="200px" height="150px">
        </div>
        <form >
  

        <div class="p-30 p-t-none p-b-none">

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <label>No : </label >&nbsp;{{$expense->id}}<br>
                                        <label>Sudah Terima Dari : </label >&nbsp;{{$expense->purchase_form}}
                                    </div>

                                    
                                
                                </div>
                            </div>


                           
                            <div class="row">
                                <div class="col-xs-4">

                                    <div class="form-group">
                                       <input type="text" value="Rp. {{number_format($expense->total,0)}}" class="form-control"/>
                                    </div>

                                    
                                
                                </div>
                                <div class="col-xs-8">

                                    <div class="form-group">
                                    <input type="text" value="{{$kata}}" class="form-control"/>
                                    </div>

                                    
                                
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-4">

                                    <div class="form-group">
                                    <label>Menurut Uraian Disebelah : </label >
                                    </div>

                                    
                                
                                </div>
                                <div class="col-xs-8">

                                    <div class="form-group">
                                    <input type="text" value="{{$expense->about}}" class="form-control"/>
                                    </div>

                                    
                                
                                </div>
                            </div>

                              <div class="row">
                                <div class="col-xs-8">

                                    <div class="form-group">
                                   Tanggal Transaksi  {{get_date_format($expense->purchase_date)}}
                                    </div>

                                    
                                
                                </div>
                                <div class="col-xs-4 pull-left">

                                    <div class="form-group">
                                Surabaya,{{get_date_format(date('Y-m-d'))}} <br>
                                Direktur,
                                    </div>

                                    
                                
                                </div>
                            </div>
<br>
<br>
<br>
  <div class="row">
  <div class="col-xs-8">


</div>
<div class="col-xs-4 pull-left">

<div class="form-group">
MOCHAMMAD MUSI
</div>



</div>
  </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </form>
        </div>
        </main>


</body>
</html>

