@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Manage PKB Letter</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-7">
                    <div class="panel">
                        <div class="panel-body">
                            <form method="POST" action="{{ url('outgoing-letter/pkb-letters/templates-update') }}">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> Manage PKB Letter</h3>
                                </div>


                                <div class="form-group">
                                    <label for="letter_number">{{language_data('Letter Number')}}</label>
                                    <input type="text" class="form-control" readonly name="letter_number" value="{{$pkbletters->letter_number}}">
                                </div>

                                <div class="form-group">
                                    <label for="subject">{{language_data('Subject')}}</label>
                                    <input type="text" class="form-control" id="subject" name="subject" value="{{$pkbletters->subject}}">
                                </div>
                                
                                <div class="form-group">
                                    <label for="date">{{language_data('Date')}}</label>
                                    <input type="text" class="form-control datePicker" required name="date" value="{{get_date_format($pkbletters->date)}}">
                                </div>
                                <div class="form-group">
                                    <label for="message">{{language_data('Copies')}}</label>
                                    <input type="text" class="form-control"  value="{{$pkbletters->tembusan}}" name="tembusan"/>
                                </div>
                                <div class="form-group">
                                    <label for="message">{{language_data('Message')}}</label>
                                    <textarea id="textarea-wysihtml5" class="textarea-wysihtml5 form-control" name="message">{!! $pkbletters->message !!}</textarea>
                                </div>
                                @if($pkbletters->status!='draft')
                                <div class="form-group">
                                        <label for="message">{{language_data('Information')}}</label>
                                        <input type="text" class="form-control"   @if($pkbletters->status!='draft') readonly @endif value="{{$pkbletters->information}}" name="information"/>
                                    </div>
                                    @endif
                                <div class="form-group">
                                    <label for="available_variables">Available Variables : </label>
                                    <div id="variables"></div>
                                    
                                </div>

                                @if($permcheck->U==1)
                                    @if($pkbletters->status=='draft')
                                        <input type="hidden" value="{{$pkbletters->id}}" name="cmd">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" name="update" class="btn btn-success"><i class="fa fa-edit"></i> {{language_data('Update')}}</button>
                                    @endif
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    <script>
    $(document).ready(function () {
        /*For variable Loading*/
        $("#variables").ready(function () {
            var id = $(this).val();
            var _url = $("#_url").val();
            $.ajax
            ({
                type: "POST",
                url: _url + '/outgoing-letter/out-letters/get-variable',
                cache: false,
                success: function ( data ) {
                    $("#variables").html( data);
                }
            });
        });

        
    });
    </script>
@endsection
