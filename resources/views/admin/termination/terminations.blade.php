@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Termination')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Termination')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#termination"><i class="fa fa-plus"></i> {{language_data('Add New Termination')}}</button>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 10%;">{{language_data('Client Contract Number')}}</th>
                                    <th style="width: 20%;">{{language_data('Company')}}</th>
                                    <th style="width: 15%;">{{language_data('Employee')}}</th>
                                    <th style="width: 20%;">{{language_data('Number')}}</th>
                                    <th style="width: 10%;">{{language_data('Termination Date')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php  $ctr=0; ?>
                                @foreach($terminations as $a)
                                       <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Project Number"><p>{{$a->project_info->project_number}}</p></td>
                                        <td data-label="Company"><p>{{$a->company_info->company}}</p></td>
                                        <td data-label="Employee"><p>{{$a->employee_info->fname}} {{$a->employee_info->lname}}</p></td>
                                        <td data-label="Number"><p>{{$a->letter_number}}</p></td>
                                        <td data-label="Termination Date"><p>{{get_date_format($a->effective_date)}}</p></td>
                                        @if($a->status=='draft')
                                            <td data-label="status"><a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a></td>
                                        @elseif($a->status=='accepted')
                                            <td data-label="status"><a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a></td>
                                        @else
                                            <td data-label="status"><a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                        @if($a->status=='draft')
                                            @if($permcheck->U==1)
                                                <a class="btn btn-success btn-xs" href="{{url('terminations/edit/'.$a->id)}}"><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>
                                            @endif
                                            @if($permcheck->D==1)
                                                <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$a->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                            @endif
                                        @else
                                            <a class="btn btn-complete btn-xs" href="{{url('terminations/view/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                       
                                        @endif
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade" id="termination" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Termination')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('terminations/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div class="row">
                                    {{-- <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Termination Letter Number')}}</label>
                                            <input type="text" class="form-control" required readonly name="letter_number" value="{{$number}}/PHK-SSS/{{get_roman_letters($month)}}/{{$year}}">
                                        </div>
                                    </div> --}}
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Letter Draft')}}</label>
                                            <select name="draft_letter" class="form-control selectpicker">
                                                <option value="0">{{language_data('Select Draft Letter')}}</option>
                                            @foreach($email_template as $em)
                                                <option value="{{$em->id}}">{{$em->tplname}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker" required="" name="date">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Effective Date')}}</label>
                                            <input type="text" class="form-control datePicker" required="" name="effective_date">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $comp_id == '' ) disabled @endif name="project_number" id="project_id">
                                                <option value="0">{{language_data('Select Client Contract')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Company')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="company_name" id="company_id">
                                                <option value="0">{{language_data('Select Company')}}</option>
                                                @foreach($company as $p)
                                                    <option value="{{$p->id}}"> {{$p->company}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-none" >
                                            <label for="e20">{{language_data('Employee Code')}}</label>
                                            <select name="employee_code" class="form-control selectpicker" data-live-search="true" @if( $emp_id == '' ) disabled @endif id="employee_code">
                                                <option value="0">{{language_data('Select Employee Code')}}</option>
                                            </select>
                                        </div>
                                    
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Employee Name')}}</label>
                                            <select name="employee_name" class="form-control selectpicker" data-live-search="true" @if( $comp_id == '' ) disabled @endif id="employee_id">
                                                <option value="0">{{language_data('Select Employee')}}</option>
                                            </select>
                                        </div>
                                    
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Grounds')}}</label>
                                            <div class="container2">
                                                <div class="col-xs-11">
                                                    <input type="text" class="form-control" name="grounds[]">
                                                </div>
                                                <div class="col-xs-1">
                                                    <button class="btn btn-xs btn-success add_form_field_ground"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>    

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group m-none" >
                                            <label for="e20">Insurance Provider</label>
                                            <input type="text" class="form-control" required="" value="" name="insurance_provider">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Copies')}}</label>
                                            <div class="container1">
                                                <div class="col-xs-11">
                                                    <input type="text" class="form-control" name="copies[]">
                                                </div>
                                                <div class="col-xs-1">
                                                    <button class="btn btn-xs btn-success add_form_field"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                     
                                
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
          

            /*For Project Loading*/
            $("#company_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'comp_id=' + id;
                var data = '';
                var data2 = '';
                if(id!='0'){
                    $.ajax
                    ({
                        type: "POST",
                        url: _url + '/terminations/get-project',
                        data: dataString,
                        cache: false,
                        success: function ( data ) {
                            $("#project_id").html( data).removeAttr('disabled').selectpicker('refresh');
                            $("#employee_id").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#employee_code").html( data2).removeAttr('disabled').selectpicker('refresh');
                        }
                    });
                } else {
                    $("#project_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#employee_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#employee_code").html( data).removeAttr('disabled').selectpicker('refresh');
                }
            });

            /*For Employee Loading*/
            $("#project_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + id;
                var data = '';
                var data2 = '';
                if(id!='0'){
                    $.ajax
                    ({
                        type: "POST",
                        url: _url + '/terminations/get-employee',
                        data: dataString,
                        cache: false,
                        success: function ( data ) {
                            $("#employee_id").html( data).removeAttr('disabled').selectpicker('refresh');
                            $("#employee_code").html( data2).removeAttr('disabled').selectpicker('refresh');
                        }
                    });
                } else {
                    $("#employee_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#employee_code").html( data).removeAttr('disabled').selectpicker('refresh');
                }
            });

            /*For employee code Loading*/
            $("#employee_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/terminations/get-employee-code',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#employee_code").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            var max_fields      = 10;
            var wrapper         = $(".container1"); 
            var add_button      = $(".add_form_field"); 
            
            var x = 1; 
            $(add_button).click(function(e){ 
                e.preventDefault();
                if(x < max_fields){ 
                    x++; 
                    $(wrapper).append('<div class="col-lg-12"><div class="row"><div id="form" class="col-xs-11"><input type="text" class="form-control" name="copies[]"/></div><div class="col-xs-1"><a href="#" class="btn btn-xs btn-danger delete"><i class="fa fa-minus" aria-hidden="true"></i></a></div></div></div>'); //add input box
                }
                else
                {
                alert('You Reached the limits')
                }
            });
            
            $(wrapper).on("click",".delete", function(e){ 
                e.preventDefault(); 
                $(this).parent('div').remove(); x--;
                $("#form").remove(); x--;
            })

            var max_fields2      = 10;
            var wrapper2         = $(".container2"); 
            var add_button2      = $(".add_form_field_ground"); 
            
            var x = 1; 
            $(add_button2).click(function(e){ 
                e.preventDefault();
                if(x < max_fields2){ 
                    x++; 
                    $(wrapper2).append('<div class="col-lg-12"><div class="row"><div id="form2" class="col-xs-11"><input type="text" class="form-control" name="grounds[]"/></div><div class="col-xs-1"><a href="#" class="btn btn-xs btn-danger delete2"><i class="fa fa-minus" aria-hidden="true"></i></a></div></div></div>'); //add input box
                }
                else
                {
                alert('You Reached the limits')
                }
            });
            
            $(wrapper2).on("click",".delete2", function(e){ 
                e.preventDefault(); 
                $(this).parent('div').remove(); x--;
                $("#form2").remove(); x--;
            })


            
        });
        $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/terminations/delete/" + id;
                    }
                });
            });
    </script>
@endsection
