<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - RESIGN</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        .help-split {
            display: inline-block;
            width: 30%;
        }
        .footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: red;
    color: white;
    text-align: center;
}
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-30 p-t-none p-b-none">

            {!!$message!!}
            <footer>
                            <div id="qrcode" style="text-align:left"></div>
                            </footer>
    </div>
    </div>
    </main>
    <p style="page-break-after: always;">&nbsp;</p>
   <main id="wrapper" class="wrapper">
   <div class="p-30 p-t-none p-b-none">
      <div  class='row' style='text-align:center'>
      <div class='col-xs-12'>
     <label style='font-size:15pt;font-weight:bold'>  <u>CERTIFICATE OF EMPLOYMENT</u></label><br>
      <label style='font-size:13pt;font-weight:bold'>SURAT KETERANGAN KERJA</label><br>
      <label style='font-size:13pt;'>No . {{$letter_number}} </label>
      </div>
      </div>
      <br>
      <div  class='row' style='text-align:left'>
      <div class='col-xs-12'>
     <label style='font-size:11pt;font-weight:bold'> <u>This is to Certify that </u></label><br>
      <label style='font-size:11pt;font-weight:bold'>Dengan ini menerangkan bahwa</label><br>
      </div>
      </div>
      <div  class='col-xs-12' style='text-align:left'>
      <div class='col-xs-3' style='font-weight:bold'>
     <label style='font-size:11pt;'> <u>Name</u><br> Nama</label><br>
     </div>
     <div class='col-xs-9'>
      <label style='font-size:11pt;font-weight:bold;'>:{{$employee_name}}</label><br>
      </div>
      </div>
      <div  class='col-xs-12' style='text-align:left'>
      <div class='col-xs-3' style='font-weight:bold'>
     <label style='font-size:11pt;'> <u>{{language_data('Address')}}</u><br> Alamat</label><br>
     </div>
     <div class='col-xs-9'>
      <label style='font-size:11pt;font-weight:bold;'>:{{$address}}</label><br>
      </div>
      </div>
      <div  class='col-xs-12' style='text-align:left'>
      <div class='col-xs-3' style='font-weight:bold'>
     <label style='font-size:11pt;'> <u>Period Service</u><br> Masa Kerja</label><br>
     </div>
     <div class='col-xs-9'>
      <label style='font-size:11pt;font-weight:bold;'>:{{$tgl_msk}} - {{$resign_date}}</label><br>
      </div>
      </div>
      <div  class='col-xs-12' style='text-align:left'>
      <div class='col-xs-3' style='font-weight:bold'>
     <label style='font-size:11pt;'> <u>Reason For Leaving</u><br>Alasan Berhenti</label><br>
     </div>
     <div class='col-xs-9'>
      <label style='font-size:11pt;font-weight:bold;'>:Dipecat</label><br>
      </div>
      </div>
     
      <div  class='col-xs-12' style='text-align:left'>
      <br>
      <div class='col-xs-12' style='font-weight:bold'>
      <label style='font-size:11pt;font-weight:bold;'>We would like to take opportunity to thank you for past efforts and contributions to {{$company}} and wish you every success in the future.</label>
      <br><br>
      <hr>
      <label style='font-size:11pt;font-weight:bold;'>Kami mengucapkan banyak terima kasih atas usaha dan dedikasi yang telah Saudara berikan kepada {{$company}}, dan berharap semoga prestasi dan keberhasilan menyertai

Saudara diwaktu yang akan datang.
</label>
     </div>
      </div>

   <div  class='col-xs-12' style='text-align:right'>
   <br> <br>
      <div class='col-xs-9' style='font-weight:bold'>
  
     </div>
     <div class='col-xs-3'>
      <label style='font-size:11pt;font-weight:bold;'>Surabaya, {{date('D M Y')}}<br>
      {{$company}}
      </label><br>
      </div>
      </div>
      <div  class='col-xs-12' style='text-align:right'>
      <br><br><br><br>
      <div class='col-xs-9' style='font-weight:bold'>
  
     </div>
     <div class='col-xs-3'>
      <label style='font-size:11pt;font-weight:bold;'>MOCHAMMAD MUSI<br>
      <u>Direktur</u>
      </label><br>
      </div>
      <footer>
                            <div id="qrcode" style="text-align:left"></div>
                            </footer>  
      </div>  
                      
</main>
   <p style="page-break-after: always;">&nbsp;</p>
   <main id="wrapper" class="wrapper">
   <div class="p-30 p-t-none p-b-none">
                            <div class="row">

                        <div class="col-xs-8">

                        </div> 
                        <div class="col-xs-4" style='text-align:right'>
                                Lampiran:<br>
                                <b><u><i>Certificate Of Employment</i></u></b><br>
                                Surat Keterangan Kerja<br>
                                {{$letter_number}}
                        </div>
                        </div>

                        <b><u><i>Job History</i></u></b><br> 
                        Riwayat Pekerjaan<br>
                            <div class="row">
                            <div class="col-lg-12">
                                    <div class="panel">
                                      
                                        <div class="panel-body p-none">
                                            <table class="table">
                          
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">No</th>
                                        <th style="width: 20%;">   <b><u><i>Office Location</i></u> </b><br> 
                        Lokasi Kantor</th>
                                        <th style="width: 20%;">   <b><u><i>{{language_data('Company')}}</i></u> </b><br> 
                        Perusahaan</th>
                                        <th style="width: 20%;">   <b><u><i>Classification</i></u> /<b><br> 
                        Jabatan</th>
                                        <th style="width: 10%;">   <b><u><i>Period</i></u> </b><br> 
                       Periode</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $ctr=0; ?>
                                @foreach($employment as $e)
                                <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="no">{{$ctr}}</td>
                                        <td data-label="location">{{$e->location}}</td>
                                        <td data-label="company">{{$e->employee_info->company_name->company}}</td>
                                        <td data-label="department">{{$e->department_info->department}}/{{$e->designation_info->designation}}</td>
                                       
                                        <td data-label="start_date">{{get_date_format($e->start_date)}} s/d {{get_date_format($e->end_date)}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>  
                     
                            </div> 
                            </div> 
                            </div>
                            <footer>
                            <div id="qrcode" style="text-align:left"></div>
                            </footer>  
                        </div>
                     
</main>
{!! Html::script("assets/libs/qrcode/qrcode.js") !!}
<script type='text/javascript'>

        var qrcode = new QRCode("qrcode",{
    text: "<?php echo $letter_number; ?>",
    width: 70,
    height: 70,
    colorDark : "#000000",
    colorLight : "#ffffff"
});

function makeCode () {      
  
    
    qrcode.makeCode( "<?php echo $letter_number; ?>");
}

makeCode();
   
</script>

</body>
</html>