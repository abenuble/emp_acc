@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title"></h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                        <form class="" role="form" method="post" action="{{url('attendance/post-view-outsource-custom-search')}}">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Month</label>
                                        <input type="text" class="form-control monthPicker" required="" @if($month!='') value="{{get_date_month($month)}}" @endif name="month">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>{{language_data('Employee')}} Code</label>
                                        <input type="text" class="form-control" name="employee" required="" readonly value="{{$attendance_id->employee_info->employee_code}}">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>{{language_data('Employee Name')}}</label>
                                        <input type="text" class="form-control" name="employee" required="" readonly value="{{$attendance_id->employee_info->fname}} {{$attendance_id->employee_info->lname}}">
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>{{language_data('Company')}}</label>
                                        <input type="text" class="form-control" name="employee" required="" readonly value="{{$attendance_id->employee_info->company_name->company}}">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>{{language_data('Department')}}/{{language_data('Designation')}}<label>
                                        <input type="text" class="form-control" name="employee" required="" readonly value="{{$attendance_id->employee_info->department_name->department}} / {{$attendance_id->employee_info->designation_name->designation}}">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>{{language_data('Location')}}</label>
                                        <input type="text" class="form-control" name="employee" required="" readonly value="{{$attendance_id->employee_info->location}}">
                                    </div>
                                </div>

                            </div>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="cmd" value="{{$attendance_id->emp_id}}">
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-search"></i> {{language_data('Search')}}</button>

                        </form>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Outsource  {{language_data('Attendance')}}</h3>


                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive" id="dataTable">
                                <thead>
                                <tr>
                                    <th style="width: 10%;">{{language_data('Date')}}</th>
                                    <th style="width: 5%;">{{language_data('Presence')}}</th>
                                    <th style="width: 20%;">{{language_data('Absenteeism')}}</th>
                                    <th style="width: 5%;">{{language_data('Overtime')}}</th>
                                    <th style="width: 20%;">{{language_data('Information')}}</th>
                                    @if($permcheck->U==1) 
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($attendance as $d)
                               
                              
                                <tr>
                                <form role="form" method="post" action="{{url('attendance/outsource-update-detail')}}" >
                                <input type="hidden" name="idem" value="{{$attendance_id->employee_info->id}}" />
                                <input type="hidden" value="{{$d->id}}" name="idatt"/>
                                    <td data-label="Date"><span style="display:none;">{{$d->date}}</span>{{get_date_format_angka($d->date)}}</td>
                                    <td data-label="Presence"><center><input type="checkbox" class="presscheck" name="presence"  @if($d->status=='Present') checked    @endif  @if($d->status=='Present' || $d->leave_id=='3') @else disabled @endif></center></td>
                                    @if($d->leave_id!='')
                                        <td data-label="Absenteeism"><input type="text" class="form-control" value="{{$d->leave->leave_type->leave}}" readonly style="width : 250px"></td>
                                    @elseif($d->leave_id=='' && $d->status=='Absent')
                                        <td data-label="Absenteeism"><input type="text" class="form-control" readonly value="{{language_data('Without Explanation / Skipping')}}" style="width : 250px"></td>
                                    @else
                                        <td data-label="Absenteeism"></td>
                                    @endif
                                    <td data-label="Overtime"><center>{{$d->overtime}} H 
                                        @if($permcheck->U==1) <a href="{{url('attendance/overtime-outsource/'.$d->id)}}" data-toggle="tooltip" data-placement="right" title="{{language_data('Set Overtime')}}"><i class="fa fa-clock-o"></i></a> @endif
                                    </center></td>
                                    
                                    @if($d->leave_id!='')
                                        <td data-label="Information"><input type="text" class="form-control" name="informasi" value="{{$d->leave->leave_reason}}"  style="width : 250px"></td>
                                    @elseif($d->overtime!='0')
                                        <td data-label="Information"><input type="text" class="form-control" name="informasi" value="{{$d->overtime_information}}" style="width : 250px"></td>
                                    @elseif($d->leave_id=='' && $d->status=='Absent')
                                        <td data-label="Information"><input type="text" class="form-control" name="informasi" value="{{$d->leave_information}}"  style="width : 250px"></td>
                                    @else
                                        <td data-label="Information"><input type="text" class="form-control" id="info{{$d->id}}"  style-"display:none" name="informasi"  style="width : 250px"></td>
                                    @endif
                                    @if($permcheck->U==1) 
                                        @if($days_before<strtotime($d->date) && $d->leave_id=='' && $d->status=='Absent')
                                            <td data-label="Actions">
                                                <a class="btn btn-success btn-xs" href="#" data-toggle="modal" data-target=".modal_edit_attendance_{{$d->id}}"><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>
                                                @include('admin.attendance.modal-edit-attendance')
                                            </td>
                                        @else
                                            <td data-label="Actions">
                                            @if( $d->status=='Present'|| $d->leave_id=='3')
                                            
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <input type="submit" value="{{language_data('save')}}" class="btn btn-primary" />
                                            
                                            @endif

                                            </td>
                                        @endif
                                    @endif
                                    </form>
                                </tr>
                               
                                @endforeach

                                </tbody>
                            </table>
                            <table class="table table-hover ">
                                <thead>
                                <tr>
                                    <th style="width: 10%;"></th>
                                    <th style="width: 5%;"></th>
                                    <th style="width: 20%;"></th>
                                    <th style="width: 5%;"></th>
                                    <th style="width: 20%;"></th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td data-label="Date"><center>Total : </center></td>
                                    <td data-label="Presence"><center> {{$attendance_total}} {{language_data('Days')}} </center></td>
                                    <td data-label="Absenteeism"><center> {{$leave_total}} {{language_data('Days')}} </center></td>
                                    <td data-label="Overtime"><center>{{$overtime_total}} H </center></td>
                                    <td data-label="Information"></td>
                                </tr>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
 
        $(document).ready(function () {
    
            /*For DataTable*/
            $('.data-table').DataTable();

            /*Linked Date*/

          
            $("#date_from").on("dp.change", function (e) {
                $('#date_to').data("DateTimePicker").minDate(e.date);
            });

            $("#date_to").on("dp.change", function (e) {
                $('#date_from').data("DateTimePicker").maxDate(e.date);
            });

            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/attendance/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For location Loading*/
            $("#designation_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'des_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/attendance/get-location',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#location").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For Company Loading*/
            $("#project_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/attendance/get-company',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#company").html( data).selectpicker('refresh');
                    }
                });
            });


            /*For Delete Attendance*/

            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/attendance/delete-attendance/" + id;
                    }
                });
            });

            /*Set Overtime*/
            $(".setOvertime").click(function (e) {
                e.preventDefault();

                var id = this.id;
                var _url = $("#_url").val();
                var  overTimeVal= $(this).data('overtime-val');
                var redirectURL=window.location.href;

                bootbox.prompt({
                    title: "Set Overtime (In Hour)?",
                    value: overTimeVal,
                    callback: function(result) {
                        var dataString = 'attend_id=' + id+'&overTimeValue='+result;
                        $.ajax
                        ({
                            type: "POST",
                            url: _url + '/attendance/set-overtime',
                            data: dataString,
                            cache: false,
                            success: function ( data ) {
                                if(data=='success'){
                                    window.location=redirectURL;
                                }else{
                                    alert('Please Try Again');
                                }

                            }
                        });
                    }
                });
            });




        });
    </script>
@endsection
