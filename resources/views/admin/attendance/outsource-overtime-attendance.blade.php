@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Update Attendance')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('attendance/post-overtime-outsource-attendance')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> {{language_data('Update Attendance')}}</h3>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Employee')}}</label>
                                    <input type="text" class="form-control" name="employee" required="" readonly value="{{$attendance->employee_info->fname}} {{$attendance->employee_info->lname}} ({{$attendance->employee_info->employee_code}})">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Date')}}</label>
                                    <input type="text" class="form-control datePicker" name="date" required=""  readonly  value="{{get_date_format($attendance->date)}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Overtime')}} Hour</label>
                                    <input type="number" class="form-control" required name="overtime" value="{{$attendance->overtime}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Information')}}</label>
                                    <textarea type="text" class="form-control" rows="6" name="overtime_information">{{$attendance->overtime_information}}</textarea>
                                </div>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" value="{{$attendance->id}}" name="cmd">
                                <input type="hidden" value="{{$attendance->emp_id}}" name="emp_id">
                                <button type="submit" class="btn btn-danger btn-sm pull-left" href="/attendance/outsource-report">Cancel</button>
                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
@endsection
