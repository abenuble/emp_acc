@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Attendance')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Search Condition')}}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="" role="form" method="post" action="{{url('attendance/post-view-internal-custom-search')}}">

                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>{{language_data('Month')}}</label>
                                            <input type="text" class="form-control monthPicker" required="" @if($month!='') value="{{get_date_month($month)}}" @endif name="month">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Employee')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$employee->fname}} {{$employee->lname}}" name="employee">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Division')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$employee->department_name->department}}" name="department">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Designation')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$employee->designation_name->designation}}" name="designation">
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$employee->id}}">
                                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-search"></i> {{language_data('Search')}}</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Attendance')}}</h3>
                            @if($permcheck->C==1)
                                <a><button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#upload">{{language_data('Upload')}}</button></a>   
                                <a href="{{url('attendance/update')}}" class="btn btn-success btn-xs pull-right"> {{language_data('Update Attendance')}}</a>

                            @endif
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive" id="dataTable">
                                <thead>
                                <tr>
                                    <th style="width: 17%;">{{language_data('Employee Name')}}</th>
                                    <th style="width: 10%;">{{language_data('Date')}}</th>
                                    <th style="width: 10%;">{{language_data('Clock In')}}</th>
                                    <th style="width: 10%;">{{language_data('Clock Out')}}</th>
                                    <th style="width: 5%;">{{language_data('Late')}}</th>
                                    <th style="width: 5%;">{{language_data('Early Leaving')}}</th>
                                    <th style="width: 5%;">{{language_data('Overtime')}}</th>
                                    <th style="width: 10%;">{{language_data('Total Work')}}</th>
                                    <th style="width: 10%;">Overtime {{language_data('Status')}}</th>
                                    @if($permcheck->U==1)
                                    <th style="width: 5%;">{{language_data('Actions')}}</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($attendance as $d)
                                
                                <tr>
                                    <td data-label="employee_name">
                                        @if($employee_permission->R==1) <a href="{{url('employees/view/'.$d->emp_id)}}">{{$d->employee_info->fname}} {{$d->employee_info->lname}}</a> @else {{$d->employee_info->fname}} {{$d->employee_info->lname}} @endif
                                    </td>
                                    <td data-label="Date"><span style="display:none;">{{$d->date}}</span>{{get_date_format($d->date)}}</td>
                                    <td data-label="Clock_In">{{$d->clock_in}}</td>
                                    <td data-label="Clock_out">{{$d->clock_out}}</td>
                                    <td data-label="Late">{{round($d->late/60,2)}} H</td>
                                    <td data-label="Early_leaving">{{round($d->early_leaving/60,2)}} H</td>
                                    <td data-label="Overtime">{{round($d->overtime,2)}} {{language_data('H')}}</td>
                                    <td data-label="Total_Work">{{round($d->total/60,2)+$d->overtime}} H</td>
                                    @if($d->overtime_status=='Rejected')
                                    <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Rejected')}}</p></td>
                                    @elseif($d->overtime_status=='Draft')
                                    <td data-label="Status"><p class="btn btn-warning btn-xs">{{language_data('Drafted')}}</p></td>
                                    @elseif($d->overtime_status=='Accepted')
                                    <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Accepted')}}</p></td>
                                    @else
                                    <td data-label="Status"></td>
                                    @endif
                                    @if($permcheck->U==1)
                                    <td data-label="Actions">


                                        <div class="btn-group btn-mini-group dropdown-default">
                                            <a class="btn btn-success btn-sm dropdown-toggle btn-animated from-top fa fa-caret-down" data-toggle="dropdown" href="#" aria-expanded="false"><span><i class="fa fa-bars"></i></span></a>
                                            <ul class="dropdown-menu">
                                                {{--  <li><a href="{{url('attendance/overtime/'.$d->id)}}" data-toggle="tooltip" data-placement="right" title="{{language_data('Set Overtime')}}"><i class="fa fa-clock-o"></i></a></li>  --}}
                                                <li><a href="{{url('attendance/edit/'.$d->id)}}" data-toggle="tooltip" data-placement="right" title="{{language_data('Edit')}}" class="text-success"><i class="fa fa-edit"></i></a></li>
                                            </ul>
                                        </div>

                                    </td>
                                    @endif
                                </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Upload File</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('attendance/importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
                                
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('attendance/downloadExcel/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">Upload File</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {

            /*For DataTable*/
            $('.data-table').DataTable();


            /*Linked Date*/

            $("#date_from").on("dp.change", function (e) {
                $('#date_to').data("DateTimePicker").minDate(e.date);
            });

            $("#date_to").on("dp.change", function (e) {
                $('#date_from').data("DateTimePicker").maxDate(e.date);
            });


            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/attendance/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });


            /*For Delete Attendance*/

            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/attendance/delete-attendance/" + id;
                    }
                });
            });



            /*Set Overtime*/
            $(".setOvertime").click(function (e) {
                e.preventDefault();

                var id = this.id;
                var _url = $("#_url").val();
                var  overTimeVal= $(this).data('overtime-val');
                var redirectURL=window.location.href;

                bootbox.prompt({
                    title: "Set Overtime (In Hour)?",
                    value: overTimeVal,
                    callback: function(result) {
                        var dataString = 'attend_id=' + id+'&overTimeValue='+result;
                        $.ajax
                        ({
                            type: "POST",
                            url: _url + '/attendance/set-overtime',
                            data: dataString,
                            cache: false,
                            success: function ( data ) {
                                if(data=='success'){
                                    window.location=redirectURL;
                                }else{
                                    alert('Please Try Again');
                                }

                            }
                        });
                    }
                });
            });





        });
    </script>


@endsection
