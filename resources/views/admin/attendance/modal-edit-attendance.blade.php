<div class="modal fade modal_edit_attendance_{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{language_data('Edit Attendance')}}</h4>
            </div>
            <form class="form-some-up form-block" role="form" action="{{url('attendance/edit-leave')}}" method="post">

                <div class="modal-body">
                    <div class="form-group">
                        <label>{{language_data('Employee Code')}}</label>
                        <input type="text" class="form-control" required="" readonly value="{{$d->employee_info->employee_code}}">
                    </div>

                    <div class="form-group">
                        <label>{{language_data('Employee Name')}}</label>
                        <input type="text" class="form-control" required="" readonly value="{{$d->employee_info->fname}} {{$d->employee_info->lname}}">
                    </div>

                    <div class="form-group">
                        <label>{{language_data('Date')}}</label>
                        <input type="text" class="form-control" required="" readonly value="{{get_date_format($d->date)}}" name="date">
                    </div>

                    <div class="form-group">
                        <label>{{language_data('Absenteeism')}}</label>
                        <input type="text" class="form-control" required="" readonly value="{{language_data('Without Explanation / Skipping')}}">
                    </div>
                                
                    <div class="form-group">
                        <label>{{language_data('Information')}}</label>
                        <textarea type="text" class="form-control" rows="6" name="information"></textarea>
                    </div>
                </div>
                
                
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="cmd" value="{{$d->id}}">
                    <input type="hidden" name="emp_id" value="{{$d->emp_id}}">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>{{language_data('Update')}}</button>
                </div>

            </form>
        </div>
    </div>

</div>
