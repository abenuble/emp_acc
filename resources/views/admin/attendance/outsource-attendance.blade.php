@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Outsource {{language_data('Attendance')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Search Condition')}}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="" role="form" method="post" action="{{url('attendance/outsource-post-custom-search')}}">

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Company')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="company" id="company">
                                                <option value="0">{{language_data('Select Company')}}</option>
                                                @foreach($company as $p)
                                                    <option value="{{$p->id}}"> {{$p->company}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Client Contract')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $proj_id == '' ) disabled @endif name="project" id="project_id">
                                                <option value="0">{{language_data('Select Client Contract')}}</option>
                                                @foreach($project as $d)
                                                    <option value="{{$d->id}}" @if($proj_id==$d->id) selected @endif>{{$d->project_number}} {{$d->project}} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Department')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="department" id="department_id">
                                                <option value="0">{{language_data('Select Department')}}</option>
                                                @foreach($department as $d)
                                                <option value="{{$d->id}}" @if($dep_id==$d->id) selected @endif> {{$d->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Designation')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $des_id == '' ) disabled @endif  name="designation" id="designation_id">
                                                <option value="0">{{language_data('Select Designation')}}</option>
                                                @if($des_id!=0)
                                                @foreach($designation as $des)
                                                    <option value="{{$des->id}}" @if($des_id==$des->id) selected @endif> {{$des->designation}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Location')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $des_id == '' ) disabled @endif name="location" id="location">
                                                <option value="0">{{language_data('Select Location')}}</option>
                                                @if($des_id!=0)
                                                @foreach($projectneeds as $pro)
                                                    <option value="{{$pro->location}}" @if($location==$pro->location) selected @endif> {{$pro->location}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-search"></i> {{language_data('Search')}}</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Outsource {{language_data('Attendance')}}</h3>
                            @if($permcheck->C==1)
                                <a><button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#upload">Upload</button></a>  
                                <a href="{{url('attendance/outsource-update')}}" class="btn btn-success btn-xs pull-right"> Update Attendance</a>
                            @endif
                                <a href="{{url('attendance/get-all-pdf-report-outsource')}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate PDF')}}</a><br>


                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 20%;">{{language_data('Employee Name')}}</th>
                                    <th style="width: 20%;">Attendance Total</th>
                                    <th style="width: 20%;">leave Total</th>
                                    <th style="width: 20%;">Overtime Total</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($attendance_outsource_report as $d)
                                
                                <tr>
                                    <td data-label="Employee Name">
                                        @if($employee_permission->R==1) <a href="{{url('employees/view/'.$d->emp_id)}}">{{$d->employee_id->fname}} {{$d->employee_id->lname}}</a> @else {{$d->employee_id->fname}} {{$d->employee_id->lname}} @endif
                                    </td>
                                    <td data-label="Attendance Total">{{$d->total_attendance}} Days</td>
                                    <td data-label="leave Total">{{$d->total_leave}} Days</td>
                                    <td data-label="Overtime Total">{{$d->total_overtime}} H</td>
                                    <td data-label="Actions">
                                        <a href="{{url('attendance/view-outsource/'.$d->emp_id)}}" data-toggle="tooltip" data-placement="right" title="View" class="text-success"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Upload File</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('attendance/outsource-importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file" accept="application/vnd.ms-excel"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
                                
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('attendance/outsource-downloadExcel/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">Upload File</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {

            /*For DataTable*/
            $('.data-table').DataTable();


            /*Linked Date*/

            $("#date_from").on("dp.change", function (e) {
                $('#date_to').data("DateTimePicker").minDate(e.date);
            });

            $("#date_to").on("dp.change", function (e) {
                $('#date_from').data("DateTimePicker").maxDate(e.date);
            });

            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/attendance/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For location Loading*/
            $("#designation_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'des_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/attendance/get-location',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#location").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For Company Loading*/
            $("#company").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'comp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/attendance/get-project',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#project_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });


            /*For Delete Attendance*/

            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/attendance/delete-attendance/" + id;
                    }
                });
            });




        });
    </script>


@endsection
