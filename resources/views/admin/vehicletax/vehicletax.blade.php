@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Vehicle Tax')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Vehicle Tax')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#new-vehicle-tax"><i class="fa fa-plus"></i> {{language_data('New')}} {{language_data('Vehicle Tax')}}
                            </button>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 25%;">{{language_data('Vehicle')}}</th>
                                    <th style="width: 25%;">{{language_data('License Plate')}}</th>
                                    <th style="width: 25%;">{{language_data('Tax Due Date')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($vehicletax as $d)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No" >{{$ctr}}</td>
                                        <td data-label="Vehicle">{{$d->vehicle}}</td>
                                        <td data-label="License Plate"> {{$d->license}}</td>
                                        <td data-label="Tax Due Date"><p>{{get_date_format($d->tax_due_date)}}</p></td>
                                        <td data-label="Actions">
                                            <a class="btn btn-success btn-xs" href="{{url('vehicletax/view/'.$d->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            @if($permcheck->D==1)
                                            <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$d->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                            @endif
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>


                        <!-- Modal -->
                        <div class="modal fade" id="new-vehicle-tax" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">{{language_data('Add')}} {{language_data('Vehicle Tax')}}</h4>
                                    </div>
                                    <form class="form-some-up" role="form" method="post" id="form" action="{{url('vehicletax/add')}}" enctype="multipart/form-data">

                                        <div class="modal-body">

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>{{language_data('Vehicle')}}</label>
                                                        <input type="text" class="form-control" name="vehicle" required="">
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>{{language_data('License Plate')}}</label>
                                                        <input type="text" class="form-control" name="license" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>{{language_data('Tax Due Date')}}</label>
                                                        <input type="text" class="form-control datePicker" name="tax_due_date" required="">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                            <button type="submit" class="btn btn-primary">{{language_data('Send')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>

            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/vehicletax/delete/" + id;
                    }
                });
            });

        });

        
    </script>
@endsection
