@extends('master')
{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View')}} {{language_data('Vehicle Tax')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('vehicletax/update')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('View')}} {{language_data('Vehicle Tax')}} </h3>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{language_data('Vehicle')}}</label>
                                            <input type="text" class="form-control" name="vehicle" value="{{$d->vehicle}}" readonly required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{language_data('License Plate')}}</label>
                                            <input type="text" class="form-control" name="license" value="{{$d->license}}" required="">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{language_data('Tax Due Date')}}</label>
                                            <input type="text" class="form-control datePicker" value="{{get_date_format($d->tax_due_date)}}" name="tax_due_date" required="">
                                        </div>
                                    </div>
                                </div>
                                @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" value="{{$d->id}}" name="cmd">
                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
{!! Html::script("assets/libs/moment/moment.min.js")!!}
{!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
{!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
{!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
{!! Html::script("assets/js/form-elements-page.js")!!}
{!! Html::script("assets/libs/data-table/datatables.min.js")!!}
{!! Html::script("assets/js/bootbox.min.js")!!}
@endsection
