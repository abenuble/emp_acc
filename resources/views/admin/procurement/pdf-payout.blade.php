<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - Pembayaran Pengadaan</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        .help-split {
            display: inline-block;
            width: 30%;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-30 p-t-none p-b-none">
        
            <div class="col-lg-6">
                <div class="panel">
                    <div class="panel-body">
                        <div class="panel-heading">
                            <h3 class="panel-title" align="center" style="font-weight:bold" >  Permintaan Pembayaran</h3>
                        </div>
                        <br>
                        <div class="form-group">
                               No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:  {{$procurement->procurement_number}}  <br>
                               Tanggal&nbsp;&nbsp;&nbsp;&nbsp;:  {{get_date_format($procurement->payout_date)}} <br>
                               Proyek&nbsp;&nbsp;&nbsp; &nbsp;: @if($procurement->project_info) {{$procurement->project_info->project}} @else - @endif <br>
                               Kepada&nbsp;&nbsp;&nbsp; :  {{$procurement->supplier}} <br>
                               Dari &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:  {{$procurement->company_info->company}} <br>


                                </div>
                        
                        <hr style="color: black;
background-color: black;
height: 4px;">
                        <div class="row">
                          
                                <div class="form-group">
                                <div class="col-sm-12">
                                    Permintaan Pembayaran Oleh:
                                    </div>
                            </div><br>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    Nama  &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;:{{$employee->fname}}&nbsp;{{$employee->lname}}
                                    </div>
                            </div> <br>
                            <div class="form-group">
                                <div class="col-sm-12">
                                   No Pegawai  &nbsp;&nbsp;&nbsp; :{{$employee->employee_code}}
                                    </div>
                            </div><br>
                            <div class="form-group">
                                <div class="col-sm-12">
                                   Jabatan &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; :@if($employee->designation_name){{$employee->designation_name->designation}}@endif
                                    </div>
                            </div><br>
                            <div class="form-group">
                                <div class="col-sm-12">
                                   Bagian &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :@if($employee->department_name){{$employee->department_name->department}}@endif
                                    </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <div class="col-sm-12">
                                   <u>Rincian Permintaan Pembayaran</u>
                                    </div>
                            </div>
                        </div>
                        
                        <div class="row">

                                <div class="col-lg-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                           
                                        </div>
                                        <div class="panel-body p-none">
                                            <table class="table">
                                              
                                                <tr>
                                                    <th style="width: 10%;">Item</th>
                                                
                                                    <th style="width: 5%;">Qty</th>
                                                    <th style="width: 5%;">Qty Beli</th>
                                                    <th style="width: 10%;">Unit</th>
                                                    <th style="width: 20%;" align='right'>Unit Price</th>
                                                    <th style="width: 20%;" align='right'>Total Price</th>
                                                    <th style="width: 10%;" align='right'>Sisa</th>
                                                
                                                    @foreach($procurement_goods as $p)
                                                    </tr>
                                                    <td>
                                                    @if($p->goods)
                                                    @if($procurement->type=='project')
                                                                            @if($p->item_info)
                                                                            {{$p->item_info->item}}
                                                                            @else
                                                                              -
                                                                            @endif
                                                                        @else
                                                                        {{$p->goods}}
                                                                        @endif
                                                    @else -
                                                    @endif
                                                    </td>
                                                    <td>
                                                    @if($p->quantity)
                                                        {{$p->quantity}}
                                                    @else -
                                                    @endif
                                                    </td>
                                                    <td>
                                                    @if($p->received_quantity)
                                                    {{$p->received_quantity}}
                                                    @else -
                                                    @endif
                                                    </td>
                                                    <td>
                                                    @if($p->unit)
                                                    {{$p->unit}}
                                                    @else -
                                                    @endif
                                                    </td>
                                                    <td align='right'>
                                                    @if($p->unit_price)
                                                    {{number_format($p->received_unit_price,0)}}
                                                    @else -
                                                    @endif
                                                    </td>
                                                    <td align='right'>
                                                    @if($p->total_price)
                                                    {{number_format($p->received_total_price,0)}}
                                                    @else 0
                                                    @endif
                                                    </td>
                                                    <td align='right'>
                                                    @if($p->realization_difference)
                                                    {{number_format($p->realization_difference,0)}}
                                                    @else 0
                                                    @endif
                                                    </td>
                                                    </tr>
                                                    @endforeach
                                                    <tr>
                                                    <td>   </td>
                                                    <td>   </td>
                                                    <td>   </td>
                                                    <td>   </td>
                                                    <td> TOTAL  </td>
                                                    <td align='right'> {{number_format($total,0)}}  </td>
                                                    <td align='right'>  {{number_format($sisa,0)}} </td>
                                                    </tr>
                                                <tbody>
                                              

                                             

                                                </tbody>
                                            </table>
                                            <hr>
                                            <div class="row">
                                            <div class="col-xs-6">
                                
                                <div class="form-group pull-left">
                                Mengetahui,<br>
                                Manager Keuangan
                                </div>
                                </div>
                              <div class="col-xs-6">
                                
                                      <div class="form-group pull-right">
                                       Pemohon
                                      </div>
                                      </div>
                                  
                                   
                                                                     
                              </div> 
                        <br><br><br>
                              <div class="row">
                                            <div class="col-xs-6">
                                
                                <div class="form-group pull-left">
                               (&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; )
                                </div>
                                </div>
                              <div class="col-xs-6">
                                
                                      <div class="form-group pull-right">
                                      (&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;)
                                      </div>
                                      </div>
                                  
                                   
                                                                     
                              </div> 

                     

                    </div>
                </div>
            </div>


            
        </div>
    </div>
</main>


</body>
</html>