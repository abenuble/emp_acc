@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">View Procurement</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            
                            <div class="col-lg-12">
                                <br>
                                <div class="form-group">
                                    <label>Procurement Number</label>
                                    <input type="text" readonly class="form-control" required="" value="{{$procurements->procurement_number}}" name="procurement_number">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <input type="text" readonly class="form-control" required="" value="{{$procurements->company_info->company}}" name="company">
                                </div>

                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="selectpicker form-control" disabled data-live-search="true" name="type" id="type">
                                        <option value="internal" @if($procurements->type=='internal') selected @endif>Internal</option>
                                        <option value="project" @if($procurements->type=='project') selected @endif>{{language_data('Client Contract')}}</option>
                                    </select>
                                </div>
                                @if($procurements->type=='project')
                                <div class="form-group">
                                    <label>{{language_data('Client Contract')}}</label>
                                    <input type="text" readonly class="form-control" required="" value="{{$procurements->project_info->project}}" name="project">
                                </div>
                                @endif

                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="selectpicker form-control" disabled data-live-search="true" name="category">
                                        <option value="subsist" @if($procurements->category=='subsist') selected @endif>Subsist</option>
                                        <option value="non-subsist" @if($procurements->category=='non-subsist') selected @endif>Non-Subsist</option>
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" readonly class="form-control datePicker" required="" value="{{get_date_format($procurements->date)}}" name="date">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Perihal</label>
                                            <input type="text" class="form-control " value="{{$procurements->perihal}}" name="perihal">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Supplier</label>
                                    <input type="text" readonly class="form-control" required="" value="{{$procurements->supplier}}" name="supplier">
                                </div>
                                
                            </div>
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata2">
                                                <table class="table table-hover table-ultra-responsive" >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 5%;">No</th>
                                                            <th style="width: 30%;">Item</th>
                                                            <th style="width: 15%;">QTY</th>
                                                            <th style="width: 15%;">Unit</th>
                                                            <th style="width: 15%;" class="text-right">Unit Price</th>
                                                            <th style="width: 20%;" class="text-right">Total Price</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                    <tbody>
                                                    <?php $ctr = 0; ?>
                                                    @foreach($procurement_goods as $i)
                                                        <?php $ctr++; ?>
                                                        <tr>
                                                            <td data-label="No" >{{$ctr}}</td>
                                                            <td data-label="Item">
                                                                        @if($procurements->type=='project')
                                                                            @if($i->item_info)
                                                                            {{$i->item_info->item}}
                                                                            @else
                                                                              -
                                                                            @endif
                                                                        @else
                                                                        {{$i->goods}}
                                                                        @endif
                                                                        </td>
                                                            <td data-label="QTY">{{$i->quantity}}</td>
                                                            <td data-label="Unit">{{$i->unit}}</td>
                                                            <td data-label="Unit Price" class="text-right">{{number_format($i->unit_price,0)}}</td>
                                                            <td data-label="Total Price" class="text-right">{{number_format($i->total_price,0)}}</td>
                                                        </tr>
                                                    @endforeach
                                                        <tr>
                                                            <td style="width: 5%;"></td>
                                                            <td style="width: 30%;"></td>
                                                            <td style="width: 15%;"></td>
                                                            <td style="width: 15%;"></td>
                                                            <td style="width: 15%;" class="text-right"><label>Total :</label></td>
                                                            <td style="width: 20%;" class="text-right">{{number_format($procurements->total,0)}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });       


        
    </script>

@endsection