@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/multi-step-form-progress-validation/css/style.css")!!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">View Procurement</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                        <form class="form-some-up form" role="form" method="post" action="{{url('procurements/update')}}" enctype="multipart/form-data">
                        <br>
                            <div class="col-md-12">   	
                                <ul class="nav nav-pills nav-justified steps" disabled>
                                    <li id="step_1" class="active">
                                        <a href="#step1" data-toggle="tab" onclick="setProgress(1)">
                                            <span class="number"> 1 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Procurement </span>
                                        </a>
                                    </li>
                                    <li id="step_2">
                                        <a href="#step2" data-toggle="tab" onclick="setProgress(2)">
                                            <span class="number"> 2 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Reception </span>
                                        </a>
                                    </li>
                                    <li id="step_3">
                                        <a href="#step3" data-toggle="tab" onclick="setProgress(3)">
                                            <span class="number"> 3 </span>
                                            <span class="desc">
                                                @if($procurements->category=='subsist')
                                                <i class="fa fa-check"></i> Realization </span>
                                                @else
                                                <i class="fa fa-check"></i> Payment </span>
                                                @endif
                                        </a>
                                    </li>
                                </ul>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 33%;"></div>
                                </div>
                                
                                <div class="box row-fluid">	
                                    <br>
                                    <div class="step" id="step1">
                                        <div class="col-lg-12">
                                            <h3>{{language_data('Procurement')}}</h3>
                                            <div class="form-group">
                                                <label>Procurement Number</label>
                                                <input type="text" readonly class="form-control" required="" value="{{$procurements->procurement_number}}" name="procurement_number">
                                            </div>

                                            <div class="form-group">
                                                <label>{{language_data('Company')}}</label>
                                                <input type="text" readonly class="form-control" required="" value="{{$procurements->company_info->company}}" name="company">
                                            </div>

                                            <div class="form-group">
                                                <label>Type</label>
                                                <select class="selectpicker form-control" disabled data-live-search="true" name="type" id="type">
                                                    <option value="internal" @if($procurements->type=='internal') selected @endif>Internal</option>
                                                    <option value="project" @if($procurements->type=='project') selected @endif>{{language_data('Client Contract')}}</option>
                                                </select>
                                            </div>
                                            @if($procurements->type=='project')
                                            <div class="form-group" id="project_div">
                                                <label>{{language_data('Client Contract')}}</label>
                                                <select class="selectpicker form-control" disabled data-live-search="true" name="project" id="project">
                                                @foreach($project as $pro)
                                                    <option value="{{$pro->id}}" @if($procurements->project==$pro->id) selected @endif>{{$pro->project}}</option>
                                                @endforeach
                                                </select>
                                                
                                            </div>
                                            @endif

                                            <div class="form-group">
                                                <label>Category</label>
                                                <select class="selectpicker form-control" disabled data-live-search="true" name="category">
                                                    <option value="subsist" @if($procurements->category=='subsist') selected @endif>Subsist</option>
                                                    <option value="non-subsist" @if($procurements->category=='non-subsist') selected @endif>Non-Subsist</option>
                                                </select>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>{{language_data('Date')}}</label>
                                                        <input type="text" readonly class="form-control datePicker" required="" value="{{get_date_format($procurements->date)}}" name="date">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Supplier</label>
                                                <input type="text" readonly class="form-control" required="" value="{{$procurements->supplier}}" name="supplier">
                                            </div>
                                            
                                        </div>
                                        <div class="row">

                                            <div class="col-lg-12">
                                                <div class="col-sm-12">
                                                    <div class="panel">
                                                        <div id="mydata2">
                                                            <table class="table table-hover table-ultra-responsive" >
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 5%;">No</th>
                                                                        <th style="width: 30%;">Item</th>
                                                                        <th style="width: 15%;">QTY</th>
                                                                        <th style="width: 15%;">Unit</th>
                                                                        <th style="width: 15%;" class="text-right">Unit Price</th>
                                                                        <th style="width: 20%;" class="text-right">Total Price</th>
                                                                    </tr>
                                                                </thead>
                                                                
                                                                <tbody>
                                                                <?php $ctr = 0; ?>
                                                                @foreach($procurement_goods as $i)
                                                                    <?php $ctr++; ?>
                                                                    <tr>
                                                                        <td data-label="No" >{{$ctr}}</td>
                                                                        <td data-label="Item">
                                                                        @if($procurements->type=='project')
                                                                            @if($i->item_info)
                                                                            {{$i->item_info->item}}
                                                                            @else
                                                                              -
                                                                            @endif
                                                                        @else
                                                                        {{$i->goods}}
                                                                        @endif
                                                                        </td>
                                                                        <td data-label="QTY">{{$i->quantity}}</td>
                                                                        <td data-label="unit">{{$i->unit}}</td>
                                                                        <td data-label="Unit Price" class="text-right">{{number_format($i->unit_price,0)}}</td>
                                                                        <td data-label="Total Price" class="text-right">{{number_format($i->total_price,0)}}</td>
                                                                    </tr>
                                                                @endforeach
                                                                    <tr>
                                                                        <td colspan="5" class="text-right"><label>Total :</label></td>
                                                                        <td style="width: 20%;" class="text-right">{{number_format($procurements->total,0)}}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="step" id="step2" style="display: none;">
                                        <div class="col-lg-12">
                                            <h3>Reception</h3>
                                            <div class="form-group">
                                                <label>Reception Number</label>
                                                <input type="text" readonly class="form-control" required="" value="{{$number}}/Penerimaan/{{get_roman_letters($month)}}/{{$year}}" name="reception_number">
                                            </div>

                                            <div class="form-group">
                                                <label>Procurement Number</label>
                                                <input type="text" readonly class="form-control" required="" value="{{$procurements->procurement_number}}">
                                            </div>

                                            <div class="form-group">
                                                <label>Reception Date</label>
                                                <input type="text" class="form-control datePicker" required="" name="reception_date">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Supplier</label>
                                                <input type="text" readonly class="form-control" required="" value="{{$procurements->supplier}}" name="supplier">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>File</label>
                                                <div class="input-group">
                                                    <input name="reception_file" class="form-control" type="file" accept="image/*">
                                                </div>
                                            </div>

                                        </div>	  
                                        <div class="row">

                                            <div class="col-lg-12">
                                                <div class="col-sm-12">
                                                    <div class="panel">
                                                        <div id="mydata2">
                                                            <table class="table table-hover table-ultra-responsive" >
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 5%;">No</th>
                                                                        <th style="width: 35%;">Item</th>
                                                                        <th style="width: 30%;">QTY</th>
                                                                        <th style="width: 30%;">QTY Received</th>
                                                                    </tr>
                                                                </thead>
                                                                
                                                                <tbody>
                                                                <?php 
                                                                    $a = 1;
                                                                    $ctr++; 
                                                                ?>
                                                                @foreach($procurement_goods as $i)
                                                                    <?php $ctr++; ?>
                                                                    <tr>
                                                                        <td data-label="No" >{{$ctr}}</td>
                                                                        <td data-label="Item">
                                                                        @if($procurements->type=='project')
                                                                            @if($i->item_info)
                                                                            <input type="text" class="form-control" readonly value=" {{$i->item_info->item}}">
                                                                           
                                                                            @else
                                                                            <input type="text" class="form-control" readonly value="-">
                                                                            @endif
                                                                        @else
                                                                        <input type="text" class="form-control" readonly value="{{$i->goods}}">
                                                                        @endif
                                                                        </td>
                                                                        <td data-label="QTY"><input type="text" class="form-control" readonly value="{{$i->quantity}}"></td>
                                                                        <td data-label="QTY Received"><input type="text" max="{{$i->quantity}}" required="" onkeypress="return isNumber(event)" onchange="received({{$a}})" class="form-control" value="" name="reception_quantity[]" id="reception_quantity{{$a}}"></td>
                                                                    </tr>
                                                                    <?php $a++; ?>
                                                                @endforeach
                                                                </tbody>
                                                            </table>

                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>	
                                    </div>
                                    <div class="step display" id="step3" style="display: none;">
                                        <div class="col-lg-12">
                                            @if($procurements->category=='subsist')
                                            <h3>Realization</h3>
                                            @elseif($procurements->category=='non-subsist')
                                            <h3>Payout</h3>
                                            @endif
                                            <div class="form-group">
                                            @if($procurements->category=='subsist')
                                                <label>Realization Number</label>
                                                <input type="text" readonly class="form-control" required="" value="{{$number_realization}}/Realisasi/{{get_roman_letters($month)}}/{{$year}}" name="realization_number">
                                            @elseif($procurements->category=='non-subsist')
                                                <label>Payout Number</label>
                                                <input type="text" readonly class="form-control" required="" value="{{$number_payout}}/Pembayaran/{{get_roman_letters($month)}}/{{$year}}" name="payout_number">
                                            @endif
                                            </div>

                                            <div class="form-group">
                                                <label>Procurement Number</label>
                                                <input type="text" readonly class="form-control" required="" value="{{$procurements->procurement_number}}">
                                            </div>

                                            <div class="form-group">
                                            @if($procurements->category=='subsist')
                                                <label>Realization Date</label>
                                                <input type="text" class="form-control datePicker" required="" name="realization_date">
                                            @elseif($procurements->category=='non-subsist')
                                                <label>Payout Date</label>
                                                <input type="text" class="form-control datePicker" required="" name="payout_date">
                                            @endif
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Supplier</label>
                                                <input type="text" readonly class="form-control" required="" value="{{$procurements->supplier}}" name="supplier">
                                            </div>
                                            
                                            @if($procurements->category=='non-subsist')
                                            <div class="form-group">
                                                <label>File</label>
                                                <div class="input-group">
                                                    <input name="payout_file" class="form-control" type="file" accept="image/*">
                                                </div>
                                            </div>
                                            @endif

                                            <input type="hidden" class="form-control" value="{{$procurements->category}}" name="category" id="category">

                                        </div>
                                        @if($procurements->category=='subsist')
                                        <div class="row">

                                            <div class="col-lg-12">
                                                <div class="col-sm-12">
                                                    <div class="panel">
                                                        <div id="mydata2">
                                                            <table class="table table-hover table-ultra-responsive" id="myTableData3" >
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 5%;">No</th>
                                                                        <th style="width: 30%;">Item</th>
                                                                        <th style="width: 10%;">QTY</th>
                                                                        <th style="width: 10%;">QTY Bought</th>
                                                                        <th style="width: 15%;">Unit Price</th>
                                                                        <th style="width: 15%;">Total Price</th>
                                                                        <th style="width: 15%;">Difference</th>
                                                                    </tr>
                                                                </thead>
                                                                
                                                                <tbody>
                                                                <?php 
                                                                    $b = 1 ;
                                                                    $ctr = 0 ;
                                                                ?>
                                                                @foreach($procurement_goods as $i)
                                                                    <?php $ctr++; ?>
                                                                    <tr>
                                                                        <td data-label="No" >{{$ctr}}</td>
                                                                        <td data-label="Item">
                                                                        @if($procurements->type=='project')
                                                                            @if($i->item_info)
                                                                            <input type="text" class="form-control" readonly value=" {{$i->item_info->item}}">
                                                                           
                                                                            @else
                                                                            <input type="text" class="form-control" readonly value="-">
                                                                            @endif
                                                                        @else
                                                                        <input type="text" class="form-control" readonly value="{{$i->goods}}">
                                                                        @endif
                                                                        </td>
                                                                        <td data-label="QTY"><input type="text" class="form-control qty" readonly value="{{$i->quantity}}"></td>
                                                                        <td data-label="QTY Bought"><input type="text" onkeypress="return isNumber(event)" max="{{$i->quantity}}" name="realization_quantity[]" id="realization_quantity{{$b}}" class="form-control realization_quantity" value=""></td>
                                                                        <td data-label="Unit Price"><input type="text" onkeypress="return isNumber(event)" name="realization_unit_price[]" class="form-control unit text-right"></td>
                                                                        <td data-label="Total Price">
                                                                            <input type="text" readonly name="realization_total_price[]" class="form-control total text-right">
                                                                            <input type="hidden" class="form-control total_lama" value="{{$i->total_price}}">
                                                                        </td>
                                                                        <td data-label="Difference"><input type="text" readonly name="realization_difference[]" class="form-control difference text-right"></td>
                                                                    </tr>
                                                                    <?php $b++; ?>
                                                                @endforeach
                                                                    <tr>
                                                                        <td colspan="5" ><center><label>Total :</label></center></td>
                                                                        <td style="width: 15%;"><input type="text" name="realization_total" readonly class="form-control alltotal text-right"></td>
                                                                        <td style="width: 15%;"><input type="text" name="realization_differences" readonly class="form-control alldifference text-right"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>	
                                        @elseif($procurements->category=='non-subsist')
                                        <div class="row">

                                            <div class="col-lg-12">
                                                <div class="col-sm-12">
                                                    <div class="panel">
                                                        <div id="mydata2">
                                                            <table class="table table-hover table-ultra-responsive" id="myTableData4">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 5%;">No</th>
                                                                        <th style="width: 30%;">Item</th>
                                                                        <th style="width: 10%;">QTY</th>
                                                                        <th style="width: 10%;">QTY Received</th>
                                                                        <th style="width: 15%;">Unit</th>
                                                                        <th style="width: 15%;">Unit Price</th>
                                                                        <th style="width: 15%;">Total Price</th>
                                                                    </tr>
                                                                </thead>
                                                                
                                                                <tbody>
                                                                <?php 
                                                                    $b = 1 ;
                                                                    $ctr = 0 ;
                                                                ?>
                                                                @foreach($procurement_goods as $i)
                                                                    <?php $ctr++; ?>
                                                                    <tr>
                                                                        <td data-label="No" >{{$ctr}}</td>
                                                                        <td data-label="Item">
                                                                        @if($procurements->type=='project')
                                                                            @if($i->item_info)
                                                                            <input type="text" class="form-control" readonly value=" {{$i->item_info->item}}">
                                                                           
                                                                            @else
                                                                            <input type="text" class="form-control" readonly value="-">
                                                                            @endif
                                                                        @else
                                                                        <input type="text" class="form-control" readonly value="{{$i->goods}}">
                                                                        @endif
                                                                        </td>
                                                                        <td data-label="QTY"><input type="text" class="form-control" readonly value="{{$i->quantity}}"></td>
                                                                        <td data-label="QTY Received"><input type="text" onkeypress="return isNumber(event)" max="{{$i->quantity}}" name="received_quantity[]" id="received_quantity{{$b}}" class="form-control qty" value=""></td>
                                                                        <td data-label="unit text-right"><input type="text" class="form-control" value="{{$i->unit}}"></td>
                                                                        <td data-label="Unit Price"><input type="text" onkeypress="return isNumber(event)" name="received_quantity[]" class="form-control unit text-right"></td>
                                                                        <td data-label="Total Price">
                                                                            <input type="text" readonly name="received_total_price[]" class="form-control total text-right">
                                                                            <input type="hidden" class="form-control total_lama" value="{{$i->total_price}}">
                                                                        </td>
                                                                    </tr>
                                                                    <?php $b++; ?>
                                                                @endforeach
                                                                    <tr>
                                                                        <td  colspan="6" ><center><label>Total :</label></center></td>
                                                                        <td style="width: 15%;"><input type="text" name="received_total" readonly class="form-control alltotal text-right"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        @endif  

                                    </div>

                                    <div class="row">
                                    <div class="col-sm-12">
                                        <div class="pull-right">
                                            <button type="button" class="action btn-sky text-capitalize back btn" style="display: none;">Back</button>
                                            <button type="button" class="action btn-sky text-capitalize next btn" style="display: inline-block;">Next</button>
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <input type="hidden" name="cmd" value="{{$procurements->id}}">
                                            <button type="submit" class="action btn-hot text-capitalize submit btn" style="display: none;">Submit</button>
                                        </div>
                                    </div>
                                    </div>			

                                </div>
                                
                            </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/multi-step-form-progress-validation/js/jquery.validate.js")!!}
    {!! Html::script("assets/libs/multi-step-form-progress-validation/js/jquery-ui.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}
    <script>
	$(document).ready(function(){

		var current = 1;
		
        $('.unit').number( true, 0, '.', ',' );
		widget      = $(".step");
		btnnext     = $(".next");
		btnback     = $(".back"); 
		btnsubmit   = $(".submit");
		// Init buttons and UI
		widget.not(':eq(0)').hide();
		hideButtons(current);
		setProgress(current);
        console.log(widget);
		// Next button click action
		btnnext.click(function(){
			if(current < widget.length){
				// Check validation
                // alert(widget);
				if($(".form").valid()){
					widget.show();
					widget.not(':eq('+(current++)+')').hide();
					setProgress(current);
				}
			}
            hideButtons(current);
		})

		// Back button click action
		btnback.click(function(){
			if(current > 1){
				current = current - 2;
				if(current < widget.length){
					widget.show();
					widget.not(':eq('+(current++)+')').hide();
					setProgress(current);
				}
			}
			hideButtons(current);
		})

	    $('.form').validate({ // initialize plugin
			ignore:":not(:visible)",			
	    });

        $('#myTableData3').on("change",".qty, .unit", function() {
            var item_quantity = parseFloat($(this).parents("tr").find(".realization_quantity").val());
            var unit_price = parseFloat($(this).parents("tr").find(".unit").val());
            var total_value = unit_price * item_quantity;
            var total_lama = parseFloat($(this).parents("tr").find(".total_lama").val());
            var difference = total_lama - total_value;
            console.info(item_quantity, unit_price, total_value, total_lama, difference);
            if(!isNaN(total_value)){
                $(this).parents("tr").find(".total").val(total_value.toFixed(2)).number( true, 0, '.', ',' );
                total();
            }
            if(!isNaN(difference)){
                $(this).parents("tr").find(".difference").val(difference.toFixed(2)).number( true, 0, '.', ',' );
                differences();
            }
        });

        $('#myTableData4').on("change",".qty, .unit", function() {
            var item_quantity = parseFloat($(this).parents("tr").find(".qty").val());
            var unit_price = parseFloat($(this).parents("tr").find(".unit").val());
            var total_value = unit_price * item_quantity;
            var total_lama = parseFloat($(this).parents("tr").find(".total_lama").val());
            if(!isNaN(total_value)){
                $(this).parents("tr").find(".total").val(total_value.toFixed(2)).number( true, 0, '.', ',' );
                total();
            }
        });

	});

	// Change progress bar action
	setProgress = function(currstep){
        if(currstep == '1'){
		    $("#step_1").addClass( "active" );	
		    $("#step_2").removeClass( "active" );	
		    $("#step_3").removeClass( "active" );	
        }
        if(currstep == '2'){
		    $("#step_1").removeClass( "active" );	
		    $("#step_2").addClass( "active" );	
		    $("#step_3").removeClass( "active" );	
        }
        if(currstep == '3'){
		    $("#step_1").removeClass( "active" );	
		    $("#step_2").removeClass( "active" );	
		    $("#step_3").addClass( "active" );	
        }
		var percent = parseFloat(100 / widget.length) * currstep;
		percent = percent.toFixed();
		$(".progress-bar").css("width",percent+"%");		
	}

	// Hide buttons according to the current step
	hideButtons = function(current){
		var limit = parseInt(widget.length); 

		$(".action").hide();

		if(current < limit) btnnext.show();
		if(current > 1) btnback.show();
		if (current == limit) { 
			// Show entered values
			$(".display label.lbl").each(function(){
				$(this).html($("#"+$(this).data("id")).val());	
			});
			btnnext.hide(); 
			btnsubmit.show();
		}
	}

    function changePilihan(idx) {
        if (document.getElementById('bukti_bankdet_check'+idx).checked == true) {
            document.getElementById('bukti_bankdet_pilihan'+idx).value = 1;
        } else {
            document.getElementById('bukti_bankdet_pilihan'+idx).value = 0;
        }
        sumBankTotal();
    }

    function received(idx){
        var received = document.getElementById('reception_quantity'+idx).value;
        var category = document.getElementById('category').value;
        console.log(category);
        if(category=='subsist'){
            document.getElementById('realization_quantity'+idx).value = received;
        } else if(category=='non-subsist'){
            document.getElementById('received_quantity'+idx).value = received;
        }
    }
        
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function total() {
        var total1 = 0;
        $('input.total').each(function () {
            var n = parseFloat($(this).val());
            total1 += isNaN(n) ? 0 : n;
        });
        $('.alltotal').val(total1.toFixed(2)).number( true, 0, '.', ',' );
        var total = 0;
    }

    function differences() {
        var total = 0;
        $('input.difference').each(function () {
            var n = parseFloat($(this).val());
            total += isNaN(n) ? 0 : n;
        });
        $('.alldifference').val(total.toFixed(2)).number( true, 0, '.', ',' );
    }

    </script>

@endsection