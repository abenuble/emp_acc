@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">View Procurement</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            
                            <div class="col-lg-12">
                                <br>
                                <div class="form-group">
                                    <label>Payout Number</label>
                                    <input type="text" readonly class="form-control" required="" value="{{$procurements->payout_number}}" name="payout_number">
                                </div>

                                <div class="form-group">
                                    <label>Procurement Number</label>
                                    <input type="text" readonly class="form-control" required="" value="{{$procurements->procurement_number}}">
                                </div>

                                <div class="form-group">
                                    <label>Payout Date</label>
                                    <input type="text" class="form-control datePicker" required="" name="payout_date" value="{{get_date_format($procurements->payout_date)}}">
                                </div>
                                
                                <div class="form-group">
                                    <label>Supplier</label>
                                    <input type="text" readonly class="form-control" required="" value="{{$procurements->supplier}}" name="supplier">
                                </div>

                                <div class="form-group">
                                    <label>File</label>
                                    <div class="input-group">
                                        <a href="{{url('procurements/download-payout-file/'.$procurements->id)}}">{{$procurements->payout_file}}</a>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata2">
                                                <table class="table table-hover table-ultra-responsive" >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 5%;">No</th>
                                                            <th style="width: 30%;">Item</th>
                                                            <th style="width: 10%;">QTY</th>
                                                            <th style="width: 10%;">QTY Received</th>
                                                            <th style="width: 15%;">Unit</th>
                                                            <th style="width: 15%;" class="text-right">Unit Price</th>
                                                            <th style="width: 15%;" class="text-right">Total Price</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                    <tbody>
                                                    <?php $ctr = 0; ?>
                                                    @foreach($procurement_goods as $i)
                                                        <?php $ctr++; ?>
                                                        <tr>
                                                            <td data-label="No" >{{$ctr}}</td>
                                                            <td data-label="Item">{{$i->goods}}</td>
                                                            <td data-label="QTY">{{$i->quantity}}</td>
                                                            <td data-label="QTY Received">{{$i->quantity}}</td>
                                                            <td data-label="Unit">{{$i->unit}}</td>
                                                            <td data-label="Unit Price" class="text-right">{{number_format($i->unit_price,0)}}</td>
                                                            <td data-label="Total Price" class="text-right">{{number_format($i->total_price,0)}}</td>
                                                        </tr>
                                                    @endforeach
                                                        <tr>
                                                            <td colspan="6" class="text-right"><label>Total :</label></td>
                                                            <td style="width: 15%;" class="text-right">{{number_format($procurements->total,0)}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });       


        
    </script>

@endsection