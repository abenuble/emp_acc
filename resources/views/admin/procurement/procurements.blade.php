@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Procurement')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Procurement')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#procurement"><i class="fa fa-plus"></i> {{language_data('Add New Procurement')}}</button>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 20%;">{{language_data('Procurement Number')}}</th>
                                    <th style="width: 20%;">{{language_data('Company')}}</th>
                                    <th style="width: 15%;">{{language_data('Type')}}</th>
                                    <th style="width: 10%;">{{language_data('Date')}}</th>
                                    <th style="width: 10%;">Total</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($procurements as $a)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No" >{{$ctr}}</td>
                                        <td data-label="Procurement Number"><p>{{$a->procurement_number}}</p></td>
                                        <td data-label="Company"><p>{{$a->company_info->company}}</p></td>
                                        <td data-label="Type"><p>{{$a->type}}</p></td>
                                        <td data-label="Date"><span style="display:none;">{{$a->date}}</span><p>{{get_date_format($a->date)}}</p></td>
                                        <td data-label="Total"><p>{{number_format($a->total,0)}}</p></td>
                                        @if($a->status=='draft')
                                            <td data-label="status"><a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a></td>
                                        @elseif($a->status=='approved')
                                            <td data-label="status"><a class="btn btn-success btn-xs">Approved : {{$a->process_status}}</a></td>
                                        @else
                                            <td data-label="status"><a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                        @if($permcheck->U==1)
                                            @if($a->status=='draft')
                                                <a class="btn btn-complete btn-xs" href="{{url('procurements/view/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            @elseif($a->status=='approved' && $a->process_status=='reception')
                                                <a class="btn btn-complete btn-xs" href="{{url('procurements/wizard/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            @elseif($a->status=='approved' && $a->process_status=='realization' && $a->category=='petty-cash')
                                                <a class="btn btn-complete btn-xs" href="{{url('procurements/wizard2/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            @elseif($a->status=='approved' && $a->process_status=='realization' && $a->category=='subsist')
                                                <a class="btn btn-complete btn-xs" href="{{url('procurements/realization/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            @elseif($a->status=='approved' && $a->process_status=='payout')
                                                <a class="btn btn-complete btn-xs" href="{{url('procurements/payout/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            @else
                                                <a class="btn btn-success btn-xs" href="{{url('procurements/edit/'.$a->id)}}"><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>
                                            @endif
                                        @endif

                                          @if($a->status=='approved' && $a->process_status=='realization' )
                                         <a class="btn btn-info btn-xs" href="{{url('procurements/print/realization/'.$a->id)}}"><i class="fa fa-download"></i> {{language_data('Download Data')}}</a>

                                          @elseif($a->status=='approved' && $a->process_status=='payout' )
                                          <a class="btn btn-info btn-xs" href="{{url('procurements/print/payout/'.$a->id)}}"><i class="fa fa-download"></i> {{language_data('Download Data')}}</a>

                                          @elseif($a->status=='approved' )
                                          <a class="btn btn-info btn-xs" href="{{url('procurements/print/procure/'.$a->id)}}"><i class="fa fa-download"></i> {{language_data('Download Data')}}</a>

                                         @endif
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade" id="procurement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Procurement')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('procurements/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">
                                
                                <div class="form-group">
                                    <label>Procurement Number</label>
                                    <input type="text" class="form-control" required="" value="{{$number}}/PNG/SSS/{{get_roman_letters($month)}}/{{$year}}" name="procurement_number">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <input type="text" readonly class="form-control" required="" value="{{$company->company}}">
                                    <input type="hidden" value="{{$company->id}}" name="company">
                                </div>

                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="type" id="type">
                                        <option value="internal">Internal</option>
                                        <option value="project">{{language_data('Client Contract')}}</option>
                                    </select>
                                </div>

                                <div class="form-group" id="project_div" style="display: none;">
                                    <label>{{language_data('Client Contract')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="project" id="project">
                                    </select>
                                    
                                </div>

                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="category">
                                        <option value="subsist">Subsist</option>
                                        <option value="non-subsist">Non-Subsist</option>
                                        <option value="petty-cash">Petty-Cash</option>
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker" required="" value="" name="date">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Perihal</label>
                                            <input type="text" class="form-control " name="perihal">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Supplier</label>
                                    <input type="text" class="form-control" required="" value="" name="supplier">
                                </div>
                                
                                <input class="btn btn-success btn-xs pull-right" type="button" id="add" value="Add" onclick="addRow2();">
                            </div>
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata2">
                                                <table class="table table-hover table-ultra-responsive" id="myTableData2"  >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 25%;">Item</th>
                                                            <th style="width: 15%;">QTY</th>
                                                            <th style="width: 15%;">Unit</th>
                                                            <th style="width: 15%;">Unit Price</th>
                                                            <th style="width: 20%;">Total Price</th>
                                                            <th style="width: 10%;">{{language_data('Action')}}</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="col-sm-12">
                                        <div class="panel">
                                            <div id="mydata2">
                                                <table class="table table-hover table-ultra-responsive" id="myTableData3">
                                                    <tr>
                                                        <td style="width: 30%;"></td>
                                                        <td style="width: 20%;"></td>
                                                        <td style="width: 20%;"><center><label>Total :</label></center></td>
                                                        <td style="width: 20%;"><input type="text" name="total"  readonly class="form-control alltotal" style="text-align:right;"></td>
                                                        <td style="width: 10%;"></td>
                                                    </tr>
                                                    
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

    <script>
        $(document).ready(function () {
            item = 0;
            /*For DataTable*/
            $('.data-table').DataTable();

            $('#myTableData2').on("change",".qty, .unit", function() {
                var item_quantity = parseFloat($(this).parents("tr").find(".qty").val());
                var unit_price = parseFloat($(this).parents("tr").find(".unit").val());
                var total_value = unit_price * item_quantity;
                console.info(item_quantity, unit_price, total_value);
                if(!isNaN(total_value))
                    $(this).parents("tr").find(".total").val(total_value.toFixed(2)).number( true, 0, '.', ',' );
                    total();
            });


            /*For project Loading*/
            $("#type").change(function () {
                var value = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'type=' + value;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/procurements/get-project',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#project").html( data).selectpicker('refresh');
                    }
                });
            });

            $("#type").change(function () {
                    toggle_specific_dropdown();
                });
                toggle_specific_dropdown();

            function toggle_specific_dropdown() {
            var $element = $("#type");
            if ($element.val() === "project") {
                $("#project_div").show();
            } else {
                $("#project_div").hide();
            }
        }

        });
        function total() {
            var total1 = 0;
            $('input.total').each(function () {
                var n = parseFloat($(this).val());
                total1 += isNaN(n) ? 0 : n;
            });
            $('.alltotal').val(total1.toFixed(2)).number( true, 0, '.', ',' );
        }

        function addRow2() {
            item++;
            $('#myTableData2').append('<tr>\
                <td id="td'+item+'">\
                </td>\
                <td>\
                    <input type="text" name="qty[]" onkeypress="return isNumber(event)" id="qty'+item+'" class="form-control qty">\
                </td>\
                <td>\
                    <input type="text" name="unit[]" class="form-control">\
                </td>\
                <td>\
                    <input type="text" name="unit_price[]" onkeypress="return isNumber(event)" class="form-control unit">\
                </td>\
                <td>\
                    <input type="text" name="total_price[]" class="form-control total" style="text-align:right;" readonly>\
                </td>\
                <td data-label="Action">\
                    <input class="btn btn-danger btn-xs pull-right" type="button" value = "Delete" onClick="Javacsript:deleteRow2(this)">\
                </td>\
            </tr>'
            );

            var value = $('#project').val();
            var _url = $("#_url").val();
            $.ajax({
                type : "POST",
                url  : _url + '/procurements/get-project-item',
                data : "id="+value,
                success:function(data){
                    if(data.length > 0){
                        $("#td"+item).append('\
                            <select class="form-control" data-live-search="true" name="item[]" id="item'+item+'" onchange="getQty('+item+')"></select>\
                        ');
						for(var j=0; j<data.length; j++){
							$("#item"+item).append('<option value="'+data[j].id+'">'+data[j].item+'</option>');
						}
                        $("#item"+item).selectpicker('refresh');
                    } else {
                        $("#td"+item).append('<input type="text" name="item[]" class="form-control" id="item'+item+'">');
                    }
                }
            });

            $('.unit').number( true, 0, '.', ',' );
        }

        function getQty(idx){
            var id = document.getElementById('item'+idx).value;
            var _url = $("#_url").val();
            $.ajax({
                type : "POST",
                url  : _url + '/procurements/get-project-item-detail',
                data : "id="+id,
                success:function(data){
                    document.getElementById('qty'+idx).value = data.leftovers;
                }
            });
        }
        
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }


        function deleteRow2(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("myTableData2");
            table.deleteRow(index);
        
            item--;
        }
    </script>
@endsection
