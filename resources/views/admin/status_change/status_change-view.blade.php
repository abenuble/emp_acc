@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Status Change')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> View Status Change
                                @if($status_change->status=='draft')
                                    <a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a>
                                @elseif($status_change->status=='accepted')
                                    <a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a>
                                @else
                                    <a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a>
                                @endif
                                </h3>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Status Change Letter Number')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$status_change->letter_number}}" name="letter_number">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Date')}}</label>
                                        <input type="text" class="form-control datePicker" readonly value="{{get_date_format($status_change->date)}}" required="" name="date">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Letter Draft')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$status_change->draft_letter_info->tplname}}" required="" name="draft_letter">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Client Contract')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$status_change->project_info->project_number}}" required="" name="project_number">
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Company')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$status_change->company_info->company}}" required="" name="company_name">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group m-none" >
                                        <label for="e20">{{language_data('Employee Code')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$status_change->employee_code}}" required="" name="employee_code">
                                    </div>
                                
                                </div>
                                <div class="col-lg-8">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Employee Name')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$status_change->employee_info->fname}} {{$status_change->employee_info->lname}}" required="" name="employee_name">
                                    </div>
                                
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group m-none" >
                                        <label for="e20">{{language_data('Department')}}/{{language_data('Designation')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$status_change->designation_old_info->department_name->department}} ({{$status_change->designation_old_info->designation}})" required="" name="designation_old">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Location')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$status_change->location_old}}" required="" name="location_old">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Employee Status')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$status_change->employee_status_old}}" required="" name="employee_status_old">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row"><br>
                            <h3 class="panel-title">{{language_data('Status Change')}}</h3>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>{{language_data('Status Change Date')}}</label>
                                        <input type="text" class="form-control datePicker" readonly value="{{get_date_format($status_change->mutation_date)}}" required="" name="mutation_date">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Department')}}/{{language_data('Designation')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$status_change->designation_new_info->department_name->department}} ({{$status_change->designation_new_info->designation}})" required="" name="designation_new">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Location')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$status_change->location_new}}" required="" name="location_new">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Employee Status')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$status_change->employee_status_new}}" required="" name="employee_status_new">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label for="message">{{language_data('Copies')}}</label>
                                        <input type="text" class="form-control"  readonly  value="{{$status_change->tembusan}}"  name="tembusan"/>
                                    </div>
                                    @if($status_change->status!='draft')
                                    <div class="form-group">
                                        <label for="message">{{language_data('Information')}}</label>
                                        <input type="text" class="form-control"   @if($status_change->status!='draft') readonly @endif value="{{$status_change->keterangan}}"  name="keterangan"/>
                                    </div>  
                                    @endif
                            </div>   
                            <br>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @if($status_change->status=='accepted')
                            <a href="{{url('status-change/downloadPdf/'.$status_change->id)}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate PDF')}}</a><br>    
                            @endif
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();


        });

    </script>
@endsection
