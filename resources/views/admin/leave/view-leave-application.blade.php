@extends('master')

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Application')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('leave/post-job-status')}}" method="post" enctype="multipart/form-data">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> {{language_data('View Application')}}</h3>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Employee Name')}}</label>
                                    <input type="text" class="form-control" readonly value="{{$leave->employee_id->fname}} {{$leave->employee_id->lname}} ({{$leave->employee_id->employee_code}}) ">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Leave Type')}}</label>
                                    <input type="text" class="form-control" readonly value="{{$leave->leave_type->leave}}">
                                </div>

                                <div class="form-group">
                                @if($leave->sick_name!='')
                                    <label>Sick Name</label>
                                    <input type="text" class="form-control" readonly value="{{$leave->sick_name}}">
                                @endif
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{language_data('Leave From')}}</label>
                                            <input type="text" class="form-control" readonly value="{{get_date_format($leave->leave_from)}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{language_data('Leave To')}}</label>
                                            <input type="text" class="form-control" readonly value="{{get_date_format($leave->leave_to)}}">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label>{{language_data('Applied On')}}</label>
                                    <input type="text" class="form-control" readonly value="{{get_date_format($leave->applied_on)}}" >
                                </div>


                                <div class="form-group">
                                    <label>{{language_data('Leave Reason')}}</label>
                                    <textarea class="form-control" readonly rows="4">{{$leave->leave_reason}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label>Leave File</label>

                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="file" accept="image/*">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" >
                                    </div>

                                    <a type="text" href="{{url('leave/download-file/'.$leave->id)}}" >{{$leave->leave_file}}</a>
                                </div>

                           
                                <div class="form-group">
                                    <label>{{language_data('Copies')}}</label>
                                    <input type="text" class="form-control"  readonly value="{{$leave->tembusan}}" name="tembusan"/>
                                </div>
                                @if($leave->status!='pending')
                                <div class="form-group">
                                    <label>{{language_data('Remark')}}</label>
                                    <textarea class="form-control" name="remark" readonly rows="4">{{$leave->remark}}</textarea>
                                </div>
                                @endif
                                @if($permcheck->U==1)
                                    @if($leave->status=='pending')

                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="cmd" value="{{$leave->id}}">
                                    <button type="submit" class="btn btn-primary">{{language_data('Send')}}</button>
                                    @endif
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
@endsection
