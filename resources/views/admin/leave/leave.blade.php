@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Leave Application')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Leave Application')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#new-leave"><i class="fa fa-plus"></i> {{language_data('New Leave')}}
                            </button>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">{{language_data('SL')}}#</th>
                                    <th style="width: 20%;">{{language_data('Client Contract Number')}}</th>
                                    <th style="width: 10%;">{{language_data('Company')}}</th>
                                    <th style="width: 10%;">{{language_data('Employee')}}</th>
                                    <th style="width: 10%;">{{language_data('Leave Type')}}</th>
                                    <th style="width: 10%;">{{language_data('Leave From')}}</th>
                                    <th style="width: 10%;">{{language_data('Leave To')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($leave as $d)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="no" >{{$ctr}}</td>
                                        <td data-label="project_number">{{$d->employee_id->project_name->project_number}}</td>
                                        <td data-label="company"> {{$d->employee_id->company_name->company}}</td>
                                        <td data-label="employee"><p>
                                            @if($employee_permission->R==1) <a href="{{url('employees/view/'.$d->employee_id->id)}}"> {{$d->employee_id->fname}} {{$d->employee_id->lname}}</a> @else {{$d->employee_id->fname}} {{$d->employee_id->lname}} @endif
                                        </p></td>
                                        <td data-label="leaveType"><p>{{$d->leave_type->leave}}</p></td>
                                        <td data-label="LeaveFrom"><p>{{get_date_format($d->leave_from)}}</p></td>
                                        <td data-label="LeaveTo"><p>{{get_date_format($d->leave_to)}}</p></td>
                                        @if($d->status=='approved')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Approved')}}</p></td>

                                            <td data-label="Actions">
                                                <a class="btn btn-success btn-xs" href="{{url('leave/edit/'.$d->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            </td>
                                        @elseif($d->status=='pending')
                                            <td data-label="Status"><p class="btn btn-warning btn-xs">{{language_data('Pending')}}</p></td>

                                            <td data-label="Actions">
                                                <a class="btn btn-success btn-xs" href="{{url('leave/edit/'.$d->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                                @if($permcheck->D==1)
                                                <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$d->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                                @endif
                                            </td>
                                        @else
                                            <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Rejected')}}</p></td>

                                            <td data-label="Actions">
                                                <a class="btn btn-success btn-xs" href="{{url('leave/edit/'.$d->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            </td>
                                        @endif
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>


                        <!-- Modal -->
                        <div class="modal fade" id="new-leave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">{{language_data('Add')}} {{language_data('New Leave')}}</h4>
                                    </div>
                                    <form class="form-some-up" role="form" method="post" id="form" action="{{url('leave/post-new-leave')}}" enctype="multipart/form-data">

                                        <div class="modal-body">

                                            <div class="form-group">
                                                <label>{{language_data('Employee')}}</label>
                                                <select name="employee" id="employee_id" class="form-control selectpicker" data-live-search="true">
                                                        <option value="0">{{language_data('Select Employee')}}</option>
                                                    @foreach($employee as $e)
                                                        <option value="{{$e->id}}">{{$e->fname}} {{$e->lname}}</option>
                                                    @endforeach
                                                </select>
                                            </div>


                                            <div class="form-group">
                                                <label>{{language_data('Leave Type')}}</label>
                                                <select name="leave_type" id="leave_id" class="form-control selectpicker" onchange="addFields()" data-live-search="true">
                                                        <option value="0">Select Leave</option>
                                                    @foreach($leave_type as $lt)
                                                        <option value="{{$lt->id}}">{{$lt->leave}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group" id="sickField">
                                            
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>{{language_data('Leave From')}}</label>
                                                    <input type="text" class="form-control datePicker" name="leave_from" required="">
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>{{language_data('Leave To')}}</label>
                                                    <input type="text" class="form-control datePicker" name="leave_to" required="">
                                                </div>
                                            </div>

                                            {{--  <div class="form-group">
                                                <label>{{language_data('Status')}}</label>
                                                <select class="selectpicker form-control" name="status">
                                                    <option value="approved">{{language_data('Approved')}}</option>
                                                    <option value="pending">{{language_data('Pending')}}</option>
                                                    <option value="rejected">{{language_data('Rejected')}}</option>
                                                </select>
                                            </div>  --}}

                                                      <div class="form-group">
                                    <label>{{language_data('Copies')}}</label>
                                    <input type="text" class="form-control"  name="tembusan"/>
                                </div>
                                            <div class="form-group">
                                                <label>{{language_data('Leave Reason')}}</label>
                                                <textarea class="form-control" rows="5" name="leave_reason"></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>Leave File</label>
                                                <div class="input-group input-group-file">
                                                    <span class="input-group-btn">
                                                        <span class="btn btn-primary btn-file">
                                                            {{language_data('Browse')}} <input type="file" class="form-control" name="file" accept="image/*">
                                                        </span>
                                                    </span>
                                                    <input type="text" class="form-control" readonly="">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="el3">Leave Quota</label>
                                                <select class="selectpicker form-control" data-live-search="true" @if( $leave_id == '' ) disabled @endif name="leave_quota" id="leave_quota">
                                                    <option value="0">Leave Quota</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="el3">Remaining Leave</label>
                                                <select class="selectpicker form-control" data-live-search="true" @if( $employee_id == '' ) disabled @endif name="remaining_leave" id="remaining_leave">
                                                    <option value="0">Remaining Leave</option>
                                                </select>
                                            </div>


                                        </div>
                                        <div class="modal-footer">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                            <button type="submit" class="btn btn-primary">{{language_data('Send')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>

            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
           

            /*For quota Loading*/
            $("#leave_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'leave_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/leave/get-leave-quota',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#leave_quota").html( data).selectpicker('refresh');
                    }
                });
            });

            /*For remaining leave Loading*/
            $("#employee_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'employee_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/leave/get-remaining-leave',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#remaining_leave").html( data).selectpicker('refresh');
                    }
                });
            });

        });
        $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/leave/delete-leave-application/" + id;
                    }
                });
            });
        function addFields()
        {
            var id = document.getElementById("leave_id").value;
            // Container <div> where dynamic content will be placed
            var container = document.getElementById("sickField");
            // Clear previous contents of the container
            while (container.hasChildNodes()) {
                container.removeChild(container.lastChild);
            }
                if(id==1){
                    // Append a node with a random text
                    container.appendChild(document.createTextNode("Sick Name "));
                    container.appendChild(document.createElement("br"));
                    // Create an <input> element, set its type and name attributes
                    var input = document.createElement("input");
                    input.type = "text";
                    input.className = "form-control";
                    input.value = "";
                    input.name = "sick_name";
                    container.appendChild(input);
                } else {
                    container.removeChild(container.lastChild);
                }
        }

        function removeFields()
        {
            // Container <div> where dynamic content will be placed
            var container = document.getElementById("sickField");
            // Clear previous contents of the container
            while (container.hasChildNodes()) {
                container.removeChild(container.lastChild);
            }
        }
    </script>
@endsection
