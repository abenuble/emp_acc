@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap/css/bootstrap.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/select2/select2.css")!!}
    {!! Html::style("assets/css/style.css")!!}
    <style>
        /*color tag*/
        .color-tag {
            display: inline-block;
            width: 15px;
            height: 15px;
            margin:2px 10px 0 0;
            transition:all 300ms ease;
        }
        .color-tag:hover{
            opacity: 0.7;
        }

        .color-tag.active {
            border-radius: 50%;
        }

    </style>
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Add New Calendar')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-6" >
                    <div class="panel">
                        <div class="panel-body" onload="hideTagField()">
                            <form class="" role="form" method="post" action="{{url('calendar/post-add-calendar')}}">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('Add New Calendar')}}</h3>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Occasion')}}</label>
                                    <input type="text" class="form-control" required value="" name="occasion">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Description')}}</label>
                                    <textarea class="form-control" rows="6" value="description" name="description"></textarea>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('Start Date')}}</label>
                                        <input type="text" class="form-control" required name="start_date" id="start_date">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('End Date')}}</label>
                                        <input type="text" class="form-control" required name="end_date" id="end_date">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('Starts At')}}</label>
                                        <input type="text" class="form-control timePicker" required name="start_time">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('Ends At')}}</label>
                                        <input type="text" class="form-control timePicker" required name="end_time">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Location')}}</label>
                                    <input type="text" class="form-control" value="" required name="location">
                                </div>
{{--                                 
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="sms_notification" value="1">SMS Notifikasi
                                    </label>
                                </div> --}}

                                <div class="row">
                                    <div class="form-group">
                                        <label for="location" class=" col-md-3"></label>
                                        <div class="color-palet col-md-9">
                                        @foreach($colors as $color)
                                        <span style='background-color: {{$color}}' class='color-tag clickable mr15 {{$active_class}}' data-color='{{$color}}'></span>
                                        @endforeach
                                        <input id="color" type="hidden" name="warna" value="{{$color}}" />
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="form-group">
                                        <label>{{language_data('Shared With')}}</label><br>
                                        <label class="radio-inline">
                                            <input type="radio" name="share_with" id="only_me" value="" class="toggle_specific">{{language_data('Only Me')}}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="share_with" id="share_with_all" value="all" class="toggle_specific">{{language_data('All Team Members')}}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="share_with" id="share_with_specific_radio_button" value="specific" class="toggle_specific">{{language_data('Specific Members and Teams')}}
                                        </label>
                                        <div class="specific_dropdown" style="display: none;">
                                            <input type="text" value="" name="share_with_specific" id="share_with_specific" class="w100p validate-hidden" placeholder="choose members and or teams"  />
                                        </div>

                                        
                                    </div>
                                </div>

                                
                                

                                

                    
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add')}} </button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/bootstrap/js/bootstrap.min.js") !!}
    {!! Html::script("assets/libs/jquery-3.2.1.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/select2/select2.js")!!}

    <script>
    $(document).ready(function () {

       
       $('#start_date,#end_date').datetimepicker({
            locale: 'id',
            useCurrent: false,
            format: 'DD MMMM YYYY',
            minDate: moment().subtract(1, 'days')
        });
        $('#start_date').datetimepicker().on('dp.change', function (e) {
            var incrementDay = moment(new Date(e.date));
           
            $('#end_date').data('DateTimePicker').minDate(incrementDay);
            $(this).data("DateTimePicker").hide();
        });

        $('#end_date').datetimepicker().on('dp.change', function (e) {
            var decrementDay = moment(new Date(e.date));
        
            $('#start_date').data('DateTimePicker').maxDate(decrementDay);
                $(this).data("DateTimePicker").hide();
        });


        var data = <?php echo json_encode($members_and_teams_dropdown) ?>;

        function formatmember (data) {
            if (data.type === "department") {
                return "<i class='fa fa-users info'></i> " + data.text;
            } else {
                return "<i class='fa fa-user'></i> " + data.text;
            }
        }

        $("#share_with_specific").select2({
            tags: true,
            tokenSeparators: [',', ' '],
            multiple: true,
            formatResult: formatmember,
            formatSelection: formatmember,
            data: data
        });

        $(".color-palet span").click(function () {
            $(".color-palet").find(".active").removeClass("active");
            $(this).addClass("active");
            $("#color").val($(this).attr("data-color"));
        });

        $(".toggle_specific").click(function () {
                toggle_specific_dropdown();
            });
            toggle_specific_dropdown();
        
        function toggle_specific_dropdown() {
            var $element = $(".toggle_specific:checked");
            if ($element.val() === "specific") {
                $(".specific_dropdown").show().find("input").addClass("form-control").addClass("validate-hidden");
            } else {
                $(".specific_dropdown").hide().find("input").removeClass("validate-hidden");
            }
        }
    });    
       
    </script>
@endsection
