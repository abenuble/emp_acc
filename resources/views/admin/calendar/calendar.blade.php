@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap-calendar/css/calendar.css") !!}
    {!! Html::style("assets/libs/bootstrap-calendar/css/fullcalendar.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-calendar/css/fullcalendar.print.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Calendar')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <div class="row">
                <div class="panel">
                    <div class="panel-body">

                        <div class="panel-heading">
                            <h3 id="month" class="panel-title"></h3>
                            <div class="pull-right form-inline">
                                <div class="btn-group " id="type">
                                    <button class="btn btn-warning btn-xs" data-calendar-view="month">{{language_data('Month')}}</button>
                                    <button class="btn btn-warning btn-xs" data-calendar-view="week">{{language_data('Week')}}</button>
                                    <button class="btn btn-warning btn-xs" data-calendar-view="day">{{language_data('Day')}}</button>
                                </div>
                                <div class="btn-group" id="time">
                                    <button class="btn btn-success btn-xs" data-calendar-nav="prev"><< {{language_data('Prev')}}</button>
                                    <button class="btn btn-default btn-xs" data-calendar-nav="today">{{language_data('Today')}}</button>
                                    <button class="btn btn-complete btn-xs" data-calendar-nav="next">{{language_data('Next')}} >></button>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>


        </div>
    </section>

    <input type="hidden" value="<?php echo asset('assets/libs/bootstrap-calendar/tmpls/'); ?>" id="_asset_path">

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/bootstrap-calendar/js/underscore.js")!!}
    {!! Html::script("assets/libs/bootstrap-calendar/js/language/id-ID.js")!!}
    {!! Html::script("assets/libs/bootstrap-calendar/js/calendar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-calendar/js/fullcalendar.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    <script>
        $(document).ready(function () {
            var _asset_url = $("#_asset_path").val() + '/';
            var _url = $("#_url").val();
            var calendar = $('#calendar').calendar({
                language: 'id-ID',
                weekbox: false,
                tmpl_path: _asset_url,
                events_source: _url+'/calendar/ajax-event-calendar',
                view: 'month',
                day: 'now',
                // Day Start time and end time with time intervals. Time split 10, 15 or 30.
                time_start: '06:00',
                time_end: '22:00',
                time_split: '30',
                classes: {
                    months: {
                        inmonth: 'cal-day-inmonth',
                        outmonth: 'cal-day-outmonth',
                        saturday: 'cal-day-weekend',
                        sunday: 'cal-day-weekend',
                        calendars: 'cal-day-calendar',
                        today: 'cal-day-today'
                    },
                    week: {
                        workday: 'cal-day-workday',
                        saturday: 'cal-day-weekend',
                        sunday: 'cal-day-weekend',
                        calendars: 'cal-day-calendar',
                        today: 'cal-day-today'
                    }
                },
                views: {
                    month: {
                        slide_events: 1,
                        enable: 1
                    },
                    week: {
                        enable: 1
                    },
                    day: {
                        enable: 1
                    }
                },
                onAfterViewLoad: function(view) {
                    $('#month').text(this.getTitle());
                }
            });

            $('.btn-group button[data-calendar-nav]').each(function () {
                var $this = $(this);
                $this.click(function () {
                    calendar.navigate($this.data('calendar-nav'));
                });
            });

            $('.btn-group button[data-calendar-view]').each(function () {
                var $this = $(this);
                $this.click(function () {
                    calendar.view($this.data('calendar-view'));
                });
            });



        });

    </script>


@endsection
