@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Surat Pengantar')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> {{language_data('View Surat Pengantar')}}
                                @if($surat_pengantar->status=='draft')
                                    <a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a>
                                @elseif($surat_pengantar->status=='accepted')
                                    <a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a>
                                @else
                                    <a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a>
                                @endif
                                </h3>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Surat Pengantar Type')}}</label>
                                        <select name="surat_pengantar_types" id="surat_pengantar_types" disabled class="form-control selectpicker">
                                            {{-- <option value="INT">{{language_data('Interview')}}</option> --}}
                                            <option value="PENP" @if($surat_pengantar->surat_pengantar_types=='PENP') selected @endif>{{language_data('Penempatan Kerja')}}</option>
                                            <option value="TRAI" @if($surat_pengantar->surat_pengantar_types=='TRAI') selected @endif>{{language_data('Training')}}</option>
                                            <option value="PETK" @if($surat_pengantar->surat_pengantar_types=='PETK') selected @endif>{{language_data('Pengganti Tenaga Kerja')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Date')}}</label>
                                        <input type="text" class="form-control datePicker" readonly required="" value="{{get_date_format($surat_pengantar->date)}}" name="date">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Letter Draft')}}</label>
                                        <select name="draft_letter" class="form-control selectpicker">
                                            <option value="0">{{language_data('Select Draft Letter')}}</option>
                                        @foreach($email_template as $em)
                                            <option value="{{$em->id}}" @if($surat_pengantar->draft_letter==$em->id) selected @endif>{{$em->tplname}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('Job Application Number')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$surat_pengantar->job_info->no_position}}" name="job">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('Client Contract')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$surat_pengantar->project_info->project}}" name="project">
                                    </div>
                                </div>
                            </div>
                                
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="el3">{{language_data('Company')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$surat_pengantar->company_info->company}}" name="company">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Designation')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$surat_pengantar->designation_info->designation}}" name="designation">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Location')}}</label>
                                        <input type="text" class="form-control" readonly required="" value="{{$surat_pengantar->location}}" name="location">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>{{language_data('Surat Pengantar Recipients')}}</label><br>
                                        <label class="radio-inline">
                                            <input type="radio" disabled name="share_with" id="share_with_all" value="all" class="toggle_specific" @if($surat_pengantar->share_with=='all') checked @endif>{{language_data('All Employee')}}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" disabled name="share_with" id="share_with_specific_radio_button" value="specific" class="toggle_specific" @if($surat_pengantar->share_with=='specific') checked @endif>{{language_data('Specific Employee')}}
                                        </label>
                                        <div class="specific_dropdown" style="display: none;">
                                            <input type="text" value="" name="share_with_specific" id="share_with_specific" class="w100p validate-hidden" placeholder="choose members and or teams"  />
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>  
                            @if($surat_pengantar->status!='draft')
                            <div class="row">
                                <div class="col-lg-12">
                                <div class="form-group">
                                        <label for="message">{{language_data('Approval Information')}}</label>
                                        <input type="text" class="form-control"   @if($surat_pengantar->status!='draft') readonly @endif value="{{$surat_pengantar->keterangan}}" name="keterangan"/>
                                    </div>
                                    </div>
                                    </div>
                                    @endif                    
                            <br>  
                            @if($surat_pengantar->status=='accepted' && count($recipients)>0)
                            <a href="{{url('surat-pengantar/downloadAllPdf/'.$surat_pengantar->id)}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate All PDF')}}</a><br>  
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> {{language_data('Surat Pengantar Recipients List')}}</h3>
                            </div>
                            <div class="panel-body p-none">
                                <table class="table data-table table-hover table-ultra-responsive">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 20%;">{{language_data('Surat Pengantar Number')}}</th>
                                        <th style="width: 20%;">{{language_data('Employee')}}</th>
                                        <th style="width: 20%;">{{language_data('Address')}}</th>
                                        <th style="width: 25%;">{{language_data('Date of Birth')}}</th>
                                        <th style="width: 10%;">{{language_data('Actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $ctr = 0; ?>
                                    @foreach($recipients as $a)
                                        <?php $ctr++; ?>
                                        <tr>
                                            <td data-label="No">{{$ctr}}</td>
                                            <td data-label="Surat Pengantar Number"><p>{{$a->letter_number}}</p></td>
                                            <td data-label="Employee"><p>{{$a->employee_info->fname}} {{$a->employee_info->lname}}</p></td>
                                            <td data-label="Address"><p>{{$a->employee_info->pre_address}}</p></td>
                                            <td data-label="Date of Birth"><p>{{$a->employee_info->birth_place}}, {{get_date_format($a->employee_info->dob)}}</p></td>
                                            <td data-label="Actions" class="">
                                            @if($surat_pengantar->status=='accepted')
                                                <a href="{{url('surat-pengantar/downloadPdf/'.$a->id)}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate PDF')}}</a><br>  
                                            @endif
                                            </td>

                                            
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });

    </script>
@endsection
