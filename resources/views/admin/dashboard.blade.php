@extends('master')

@section('style')
    {!! Html::style("assets/libs/bootstrap/css/bootstrap.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-daterangepicker/daterangepicker.css")!!}
    {!! Html::style("assets/libs/select2/select2.css")!!}
    {!! Html::style("assets/css/style.css")!!}
@endsection
@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30"></div>
        <div class="p-15 p-t-none p-b-none m-l-10 m-r-10">
        @include('notification.notify')
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel-heading">
                    {{-- <form class="" role="form" id="dashboard" method="post" action="{{url('dashboard')}}">
                    <div class="row">
                        <div class="col-lg-6">
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <input type="text" class="form-control datePicker" value="{{get_date_format($date_from)}}" name="period_from" id="period_from">
                            </div>    
                        </div>
                        <div class="col-lg-1">
                            <center>S/D</center>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <input type="text" class="form-control datePicker" value="{{get_date_format($date_to)}}" name="period_to" id="period_to">
                            </div>                  
                        </div>
                        <div class="col-lg-1">
                            <div class="form-group">
                                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-eye"></i> Show</button>
                            </div>                  
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </form> --}}
                    <form class="" role="form" method="post" action="{{url('dashboard')}}">
                        <h3 class="panel-title">{{language_data('Dashboard')}}</h3>
                        <div class="row center">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>{{language_data('Period From')}}</label>
                                    <input type="text" class="form-control datePicker" required="" value="{{get_date_format($date_from)}}" name="test1">
                                </div>    
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>{{language_data('Period To')}}</label>
                                    <input type="text" class="form-control datePicker" required="" value="{{get_date_format($date_to)}}" name="test2">
                                </div>                  
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" value="1" name="test">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-search"></i> {{language_data('Search')}}</button>
                    </form>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel-body">
                    <div class="row text-center">
                        <h1 class="text-center"><i class="fa fa-user"></i> {{language_data('Employees')}}</h1>
                        <br>
                        <div class="col-sm-3">
                            <div class="z-shad-1">
                                <div class="bg-success text-white p-15 clearfix">
                                    <h2 class="text-center"><i class="fa fa-users"></i> Employee</h2><br>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h3 class="text-center" id="employee_all">{{$employee_all}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="z-shad-1">
                                <div class="bg-info text-white p-15 clearfix">
                                    <h2 class="text-center"><i class="fa fa-user-plus"></i> New Employee</h2><br>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h3 class="text-center" id="employee_new">{{$employee_new}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="z-shad-1">
                                <div class="bg-danger text-white p-15 clearfix">
                                    <h2 class="text-center"><i class="fa fa-user-times"></i> Resign</h2><br>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h3 class="text-center" id="employee_resign">{{$employee_resign}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="z-shad-1">
                                <div class="bg-warning text-white p-15 clearfix">
                                    <h2 class="text-center"><i class="fa fa-user-edit"></i> Mutasi</h2><br>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h3 class="text-center" id="employee_mutasi">{{$employee_mutasi}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <br>
                    <div class="row text-center">

                        <div class="col-sm-12">
                            <div class="z-shad-1">
                                <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                            </div>
                        </div>

                    </div>
                    <br>
                    <div class="row text-center">

                        <div class="col-sm-12">
                            <div class="z-shad-1">
                                <div id="container" style="min-width: 310px; height: 800px; margin: 0 auto"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection


{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/jquery-3.2.1.min.js")!!}
    {!! Html::script("https://code.jquery.com/ui/1.12.1/jquery-ui.js") !!}
    {!! Html::script("assets/libs/bootstrap/js/bootstrap.min.js") !!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-daterangepicker/daterangepicker.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/js/highcharts.js")!!}

<script>
    $(document).ready(function () {
        
        $('#period_from').change(function() {
            alert('masuk');
            var period_from = $('#period_from').val();
            var period_to = $('#period_to').val();
            if (period_from != null && period_to != null){
                var date_from = new Date(period_from); //dd-mm-YYYY
                var date_to = new Date(period_to);
                if(date_from <= date_to){
                    $("#dashboard").submit();
                } else {
                    alert('Silahkan isi periode dengan benar');
                    return false;
                }
            } else {
                alert('Silahkan isi periode yang akan di');
                return false;
            }
        });
        $('#period_to').change(function() {
            alert('masuk');
            var period_from = $('#period_from').val();
            var period_to = $('#period_to').val();
            if (period_from != null && period_to != null){
                var date_from = new Date(period_from); //dd-mm-YYYY
                var date_to = new Date(period_to);
                if(date_from <= date_to){
                    $("#dashboard").submit();
                } else {
                    alert('Silahkan isi periode dengan benar');
                    return false;
                }
            } else {
                alert('Silahkan isi periode dengan benar');
                return false;
            }
        });

        Highcharts.chart('container', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Karyawan per Perusahaan'
            },
            xAxis: {
                categories: [
                    <?php 
                    foreach($employee_percompany as $comp) {
                        echo '"'.$comp['company'].'",';
                    }
                    ?>
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr>' +
                    '<td style="padding:0"><b>{point.y:,.0f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Total',
                data: [
                    <?php 
                    foreach($employee_percompany as $comp) {
                        echo $comp['employee'].',';
                    }
                    ?>
                ]
            }]
        });

        Highcharts.chart('container2', {

            title: {
                text: 'Total Karyawan per Bulan'
            },

            yAxis: {
                title: {
                    text: 'Jumlah Karyawan'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 1
                }
            },
            series: [
            <?php foreach($years as $y) {?>
            {
                name: <?php echo $y; ?>,
                data: [
                    <?php foreach($employee_permonth[$y] as $nom) {
                        echo $nom[0].',';
                    } ?>
                    ]
            }, 
            <?php } ?>
            ],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });

        
    });

    // function dashboard() {
    //     var period_from = $('#period_from').val();
    //     var period_to = $('#period_to').val();
    //     console.log(period_from,period_to);
    //     if (period_from != null && period_to != null){
    //         // var date_from = new Date(period_from); //dd-mm-YYYY
    //         // var date_to = new Date(period_to);
    //         // console.log(date_from,date_to);
    //         if(period_from <= period_to){
    //             $("#dashboard").submit();
    //         } else {
    //             alert('Silahkan isi periode dengan benar');
    //             return false;
    //         }
    //     } else {
    //         alert('Silahkan isi periode dengan benar');
    //         return false;
    //     }
    // }
    </script>
@endsection