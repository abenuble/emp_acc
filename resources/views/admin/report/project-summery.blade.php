@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/select2/select2.css")!!}
    {!! Html::style("assets/libs/chart-horizontal/chartist.min.css") !!}
    {!! Html::style("assets/libs/c3/c3.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
    <div class="p-30"  >
        
        <div class="panel"  style='padding:10px'>
        <div class="panel-body p-none">
        <form role="form-some-up" method="post" action="{{url('reports/project')}}">
            <h2 class="page-title">Laporan Proyek</h2><br>
           <div class="row">
                <div class="col-lg-4">
               
                </div>
            <div class="col-lg-8">
                   
                                    <div class="form-group">
            <div class="specific_dropdown" >
                                    <label><b>{{language_data('Company')}}</b></label>
                                    <input type="text" value="{{$company}}" name="share_with_specific" id="share_with_specific" style="width:100%" placeholder="choose Company"  />
</div>
</div>
                   
            </div>
           </div>
           <div class="row">
           <div class="col-lg-9">
                
                </div>
                <div class="col-lg-2"  style="float: right;">
                <div class="form-group">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <button type="submit" class="btn btn-info " style="float: right;"><i class="fa fa-search"></i>Search</button>
                </div>
                </div>
          
           </div>
           </form>
           </div>
           </div>
           </div>
        <div class="p-30">
        <div class="row">
        <div class="col-lg-12">
        <div class="panel"  style='padding:10px'>
        <div class="panel-body p-none">
                <div class="col-xs-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Project Value (Pie)</h5>
                        </div>
                        <div class="ibox-content">
                            <div id="pie1" class="ct-perfect-fourth"></div>
                        </div>
                    </div>
                </div>
                <div class="row" >
                <div class="col-xs-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Project Value (chart)</h5>
                        </div>
                        <div class="ibox-content" >
                            <div id="ct-chart3" ></div>
                        </div>
                    </div>
                </div>
        </div>
                </div>
</div></div></div>
      
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/select2/select2.js")!!}
    {!! Html::script("assets/libs/chart-horizontal/chartist.min.js")!!}
    {!! Html::script("assets/libs/c3/c3.min.js")!!}
    {!! Html::script("assets/libs/c3/d3.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){
              
            $(".specific_dropdown").show().find("input").addClass("validate-hidden");
                    var _url = $("#_url").val();
                    $.ajax
                ({
                    url: _url + '/reports/get-companies',
                    cache: false,
                    success: function ( data ) {
                    $("#share_with_specific").select2({
                            tags: true,
                            tokenSeparators: [',', ' '],
                            multiple: true,
                            data:data
                        });
                    }
                    });
                    c3.generate({
                bindto: '#pie1',
                data:{
                    columns: [
                        <?php
                            $ct=1;
                        foreach ($data as $d){
                            echo "['".$d['company']."',".$d['projectval']."]";
                            if($ct!=sizeof($data)){echo ",";}
                            $ct++;
                        }
                        ?>
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type : 'pie'
                }
            });

  c3.generate({
                bindto: '#ct-chart3',
                data:{
                    columns: [
                        <?php
                            $ct=1;
                        foreach ($data as $d){
                            echo "['".$d['company']."',".$d['projectval']."]";
                            if($ct!=sizeof($data)){echo ",";}
                            $ct++;
                        }
                        ?>
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type: 'bar',
                    labels:true,
                    groups: [
                        ['data1', 'data2']
                    ]
                },    
                bar: {
    width: {
      ratio:1,
    }
  },                
                tooltip: {
        show: false
    }
            });
             
            $('.data-table').DataTable();
        });
    </script>
@endsection
