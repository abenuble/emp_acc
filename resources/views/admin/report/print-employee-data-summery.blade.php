<!DOCTYPE html>

<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - assessment Form</title>

  

<head>
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
        {!! Html::style("assets/libs/chart-horizontal/chartist.min.css") !!}
        {!! Html::style("assets/libs/c3/c3.min.css") !!}
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">



        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                <div class="p-30"  >
                
                    <div class="panel"  style='padding:10px'>
                        <div class="panel-body p-none">
            
                            <div class="col-xs-5">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Jenis Kelamin</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div id="pie1"></div>
                                    </div>
                                </div>
                            </div>
                
                            <div class="col-xs-5">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Tingkat Pendidikan</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div id="pie2" ></div>
                                    </div>
                                </div>
                            </div>
                    
                
                                <div class="col-xs-5">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Gaji {{$u120}}, {{$u2130}},{{$u3150}},{{$u51}} </h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div id="pie3" ></div>
                                        </div>
                                    </div>
                             
                            
                            
                                <div class="col-xs-5">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Usia </h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div id="pie4" ></div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
        </div>
    </div>  
</main> 
</body>                
    {!! Html::script("assets/libs/jquery-1.10.2.min.js") !!}
{!! Html::script("assets/libs/jquery.slimscroll.min.js") !!}
{!! Html::script("assets/libs/smoothscroll.min.js") !!}
{!! Html::script("assets/libs/bootstrap/js/bootstrap.min.js") !!}
{!! Html::script("assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js") !!}
{!! Html::script("assets/libs/alertify/js/alertify.js") !!}
{!! Html::script("assets/libs/bootstrap-select/js/bootstrap-select.min.js") !!}
{!! Html::script("assets/js/scripts.js") !!}
    {!! Html::script("assets/libs/c3/c3.min.js")!!}
    {!! Html::script("assets/libs/c3/d3.min.js")!!}


<script type='text/javascript'>
        $(document).ready(function(){
            c3.generate({
                bindto: '#pie1',
                data:{
                    columns: [
                        ['male', {{$gendermale}}],
                        ['female',{{$genderfemale}}]
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type : 'pie'
                }
            });
            c3.generate({
                bindto: '#pie2',
                data:{
                    columns: [
                        ['sma', {{$sma}}],
                        ['d3', {{$d3}}],
                        ['s1',{{$s1}}],
                        ['s2', {{$s2}}]
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type : 'pie'
                }
            });
            c3.generate({
                bindto: '#pie3',
                data:{
                    columns: [
                        
                        ['1-5 Juta', {{$s05}}],
                        ['5-10Juta', {{$s510}}],
                        ['10-15Juta', {{$s1015}}],
                        ['>15Juta', {{$s15}}]
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type : 'pie'
                }
            });
            c3.generate({
                bindto: '#pie4',
                data:{
                    columns: [
                        ['1-20 Thn', {{$u120}}],
                        ['21-30Thn', {{$u2130}}],
                        ['31-50Thn', {{$u3150}}],
                        ['>50Thn', {{$u51}}]
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type : 'pie'
                }
            });
            var data = {
                series: [{{$gendermale}}, {{$genderfemale}}]
            };

            var sum = function(a, b) { return a + b };

            new Chartist.Pie('#ct-chart5', data, {
                labelInterpolationFnc: function(value) {
                    return Math.round(value / data.series.reduce(sum) * 100) + '%';
                }
            });
            var data = {
                series: [{{$sma}}, {{$d3}},{{$s1}},{{$s2}}]
            };
 new Chartist.Pie('#ct-chart6', data, {
                labelInterpolationFnc: function(value) {
                    return Math.round(value / data.series.reduce(sum) * 100) + '%';
                }
            });
            var data = {
               
                series: [{{$s05}}, {{$s510}},{{$s1015}},{{$s15}}]
              
            };
 new Chartist.Pie('#ct-chart7', data, {
                labelInterpolationFnc: function(value) {
                    return Math.round(value / data.series.reduce(sum) * 100) + '%';
                }
            });
            var data = {
                labels:['a','b','c','d'],
               series: [{{$u120}}, {{$u2130}},{{$u3150}},{{$u51}}]
               
             
           };
new Chartist.Pie('#ct-chart8', data, {
               labelInterpolationFnc: function(value) {
                   return Math.round(value / data.series.reduce(sum) * 100) + '%';
               }
           });

            $('.data-table').DataTable();
        });
    </script>
</html>

