@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/chart-horizontal/chartist.min.css") !!}
    {!! Html::style("assets/libs/c3/c3.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
    <div class="p-30"  >
        
        <div class="panel"  style='padding:10px'>
        <div class="panel-body p-none">
        <form role="form" method="post" action="{{url('reports/employee-score')}}">
            <h2 class="page-title">Laporan Penilaian Karyawan </h2>
            <div class="row">
          
            <div class="col-lg-4"></div>
                <div class="col-lg-4">
                <div class="form-group">
                        <label><b>Proyek</b></label>
                        <select name="project" class="form-control selectpicker companyy" data-live-search="true" id="project">
                        <option value="0">{{language_data('All contract client')}}</option>
                                        @foreach($projects as $c)
                                        <option value="{{$c->id}}">{{$c->project.'('.$c->project_number.')'}}</option>
                                        @endforeach
                                    </select>
                </div>
                </div>
            <div class="col-lg-4">
                   
                                    <label>Posisi</label>
                                    <select name="designation" class="form-control selectpicker companyy" data-live-search="true" id="designation">
                                    <option value="0">All {{language_data('Designation')}}<option>
                                    @foreach($designations as $c)
                                            <option value="{{$c->id}}">{{$c->designation}}</option>
                                        @endforeach
                                    </select>                   
            </div>
      
           </div>
           <div class="row">
           <div class="col-lg-9">
                
                </div>
                <div class="col-lg-2"  style="float: right;">
                <div class="form-group">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <button type="submit" class="btn btn-info " style="float: right;"><i class="fa fa-search"></i>Search</button>
                </div>
                </div>
          
           </div>
           </form>
        
           <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Top 10 Employees</h5>
                        </div>
                        <div class="ibox-content">
                            <div id="ct-chart4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Ringkasan Penilaian Karyawan</h3>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 10%;">#</th>
                                    <th style="width: 30%;">{{language_data('Name')}}</th>
                                    <th style="width: 30%;">Bulan Penlaian</th>
                                    <th style="width: 30%;">Hasil Penilaian</th>
                                 
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr=0; $idx=0; ?>
                                @foreach($employees as $d)
                                <?php $ctr++; ?>
                                <tr>
                                    <td data-label="SL">{{$ctr}}</td>
                                    <td data-label="Name"><p>{{$d['fname']}} {{$d['lname']}}({{$d['employee_code']}})</p></td>
                                    <td data-label="Date"><p>{{$scores[$idx]['tanggal']}}</p></td>
                                    <td data-label="grade"><p>{{$scores[$idx]['grade']}}</p></td>

                                
                                </tr>
                                <?php $idx++; ?>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/chart-horizontal/chartist.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/c3/c3.min.js")!!}
    {!! Html::script("assets/libs/c3/d3.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){
            c3.generate({
                bindto: '#ct-chart4',
                data:{
                    columns: [
                        <?php
                            $ct=1;
                            if(sizeof($tmpskor)<10){
                                foreach($tmpskor as $s){
                            echo "['".$s['emplo']."',".$s['grade']."]";
                            if($ct!=sizeof($tmpskor)){echo ",";}
                            $ct++;
                        }
                    }
                    else{
                        for($i=0;$i<10;$i++){
                            echo "['".$tmpskor[$i]['emplo']."',".$tmpskor[$i]['grade']."]";
                            if($i!=9){echo ",";}
                        }
                    }
                    ?>
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    labels:true,
                    type: 'bar',
                    order: 'desc',
                    groups: [
                        ['data1', 'data2']
                    ]
                },                    
                tooltip: {
        show: false
    },
    axis: {
        rotated: true
    }
            });
           
            $("#project").val("{{$project}}").trigger("change");
            $("#designation").val("{{$designation}}").trigger("change");
            $('.data-table').DataTable();
            
        });
    </script>
@endsection
