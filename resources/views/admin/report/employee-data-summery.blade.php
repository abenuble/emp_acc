@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/chart-horizontal/chartist.min.css") !!}
    {!! Html::style("assets/libs/c3/c3.min.css") !!}

    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
    <div class="p-30"  >
        
        <div class="panel"  style='padding:10px'>
        <div class="panel-body p-none">
        <form role="form-some-up" method="post" action="{{url('reports/employee-data')}}">
            <h2 class="page-title">Laporan Data Karyawan</h2><br>
           <div class="row">
                <div class="col-lg-4">
                <div class="form-group">
                        <label><b>{{language_data('Location')}}</b></label>
                        <select name="location" class="form-control selectpicker" data-live-search='true' id="location">
                        <option value="0" >all location</option>
                                    @foreach($lokasi as $e)
                                        <option value="{{$e->pre_city}}" >{{$e->pre_city}}</option>
                                    @endforeach
                                    </select>
                </div>
                </div>
            <div class="col-lg-8">
                   
                                    <label>{{language_data('Client Contract')}}</label>
                                    <select name="project" class="form-control selectpicker" data-live-search='true' id="project">
                                    <option value="0" >all Project</option>
                                    @foreach($project as $e)
                                    <option value="{{$e->id}}">{{$e->project.'('.$e->project_number.')'}}</option>
                                    @endforeach
                                    </select>
                   
            </div>
           </div>
           <div class="row">
           <div class="col-lg-7">
                
                </div>
              
                <div class="col-lg-4"  style="float: right;">
                <div class="form-group"  style="float: right;">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                @if($projectq)
                <a href="{{url('reports/download-pdf-data-karyawan/'.$gendermale.'/'.$genderfemale.'/'.$sma.'/'.$d3.'/'.$s1.'/'.$s2.'/'.$s05.'/'.$s510.'/'.$s1015.'/'.$s15.'/'.$u120.'/'.$u2130.'/'.$u3150.'/'.$u51.'/'.$projectq)}}" class="btn btn-success " style='text-align:right' >Generate Pdf</a> &nbsp;
                @else
                    <a href="{{url('reports/download-pdf-data-karyawan/'.$gendermale.'/'.$genderfemale.'/'.$sma.'/'.$d3.'/'.$s1.'/'.$s2.'/'.$s05.'/'.$s510.'/'.$s1015.'/'.$s15.'/'.$u120.'/'.$u2130.'/'.$u3150.'/'.$u51.'/0')}}" class="btn btn-success " style='text-align:right' >Generate Pdf</a> &nbsp;

                
                @endif
                <button type="submit" class="btn btn-info " style='text-align:right'><i class="fa fa-search"></i>Search</button>
                </div>
                </div>
          
           </div>
           </form>
        </div>
        </div>
        </div>
        <div class="p-30"  >
        
        <div class="panel"  style='padding:10px'>
        <div class="panel-body p-none">
        <div class="row">
                <div class="col-lg-5">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Jenis Kelamin</h5>
                        </div>
                        <div class="ibox-content">
                            <div id="pie1" class="ct-perfect-fourth"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-lg-5">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Tingkat Pendidikan</h5>
                        </div>
                        <div class="ibox-content">
                            <div id="pie2" class="ct-perfect-fourth"></div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="p-30">
        <div class="row">
                <div class="col-lg-5">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Gaji {{$u120}}, {{$u2130}},{{$u3150}},{{$u51}} </h5>
                        </div>
                        <div class="ibox-content">
                            <div id="pie3" class="ct-perfect-fourth"></div>
                        </div>
                    </div>
                </div>
              
                <div class="col-lg-5">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Usia </h5>
                        </div>
                        <div class="ibox-content">
                            <div id="pie4" class="ct-perfect-fourth"></div>
                        </div>
                    </div>
             
                </div>
               </div>
               </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/chart-horizontal/chartist.min.js")!!}
    {!! Html::script("assets/libs/c3/c3.min.js")!!}
    {!! Html::script("assets/libs/c3/d3.min.js")!!}

    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){

            $('#location').val("{{$locationq}}").trigger("change");
            $('#project').val("{{$projectq}}").trigger("change");
            c3.generate({
                bindto: '#pie1',
                data:{
                    columns: [
                        ['male', {{$gendermale}}],
                        ['female',{{$genderfemale}}]
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type : 'pie'
                }
            });
            c3.generate({
                bindto: '#pie2',
                data:{
                    columns: [
                        ['sma', {{$sma}}],
                        ['d3', {{$d3}}],
                        ['s1',{{$s1}}],
                        ['s2', {{$s2}}]
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type : 'pie'
                }
            });
            c3.generate({
                bindto: '#pie3',
                data:{
                    columns: [
                        
                        ['1-5 Juta', {{$s05}}],
                        ['5-10Juta', {{$s510}}],
                        ['10-15Juta', {{$s1015}}],
                        ['>15Juta', {{$s15}}]
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type : 'pie'
                }
            });
            c3.generate({
                bindto: '#pie4',
                data:{
                    columns: [
                        ['1-20 Thn', {{$u120}}],
                        ['21-30Thn', {{$u2130}}],
                        ['31-50Thn', {{$u3150}}],
                        ['>50Thn', {{$u51}}]
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type : 'pie'
                }
            });
            var data = {
                series: [{{$gendermale}}, {{$genderfemale}}]
            };

            var sum = function(a, b) { return a + b };

            new Chartist.Pie('#ct-chart5', data, {
                labelInterpolationFnc: function(value) {
                    return Math.round(value / data.series.reduce(sum) * 100) + '%';
                }
            });
            var data = {
                series: [{{$sma}}, {{$d3}},{{$s1}},{{$s2}}]
            };
 new Chartist.Pie('#ct-chart6', data, {
                labelInterpolationFnc: function(value) {
                    return Math.round(value / data.series.reduce(sum) * 100) + '%';
                }
            });
            var data = {
               
                series: [{{$s05}}, {{$s510}},{{$s1015}},{{$s15}}]
              
            };
 new Chartist.Pie('#ct-chart7', data, {
                labelInterpolationFnc: function(value) {
                    return Math.round(value / data.series.reduce(sum) * 100) + '%';
                }
            });
            var data = {
                labels:['a','b','c','d'],
               series: [{{$u120}}, {{$u2130}},{{$u3150}},{{$u51}}]
               
             
           };
new Chartist.Pie('#ct-chart8', data, {
               labelInterpolationFnc: function(value) {
                   return Math.round(value / data.series.reduce(sum) * 100) + '%';
               }
           });

            $('.data-table').DataTable();
        });
    </script>
@endsection
