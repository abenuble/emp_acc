@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Employee Resign')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                        <form class="" role="form" method="post" action="{{url('reports/employee-resign')}}">
                            <h3 class="panel-title">{{language_data('Employee Resign')}}</h3>
                            <div class="row center">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Period From')}}</label>
                                        <input type="text" class="form-control datePicker" required="" @if($period_from!='0') value="{{get_date_format($period_from)}}" @endif name="period_from">
                                    </div>    
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Period To')}}</label>
                                        <input type="text" class="form-control datePicker" required="" @if($period_to!='0') value="{{get_date_format($period_to)}}" @endif name="period_to">
                                    </div>                  
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" id="url" value="{{url('reports/download-excel-employee-resign/')}}">
                            <a onclick="excel()" class="btn btn-info pull-right"><i class="fa fa-download"></i> Excel</a>
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-search"></i> {{language_data('Search')}}</button>
                        </form>
                        <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 10%;">NIK</th>
                                        <th style="width: 20%;">{{language_data('Name')}}</th>
                                        <th style="width: 15%;">{{language_data('Company')}}</th>
                                        <th style="width: 15%;">{{language_data('Designation')}}</th>
                                        <th style="width: 15%;">{{language_data('Date of Join')}}</th>
                                        <th style="width: 15%;">{{language_data('Date of Leave')}}</th>
                                        <th style="width: 5%;">{{language_data('Actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $ctr = 0; ?>
                                    @foreach($employees as $d)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="NIK"><p>{{$d->employee_code}}</p></td>
                                        <td data-label="Name"><p>{{$d->fname}}</p></td>                                  
                                        <td data-label="Company"><p>{{$d->company_name->company}}</p></td>                                    
                                        <td data-label="Designation"><p>{{$d->employee_info->designation_name->designation}}</p></td>         
                                        <td data-label="Date of join"><p>{{get_date_format($d->employee_info->doj)}}</p></td>        
                                        <td data-label="Date of leave"><p>{{get_date_format($d->dol)}}</p></td>     
                                        <td data-label="Actions">
                                            <a class="btn btn-complete btn-xs" href="{{url('employees/view/'.$d->employee_info->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>
                                    </tr>
    
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });
        
        function excel()
        {
            var url = document.getElementById("url").value;
            var period_from = document.getElementsByName("period_from")[0].value;
            var period_to = document.getElementsByName("period_to")[0].value;
            if(period_from == ''){
                period_from = '0';
            }
            if(period_to == ''){
                period_to = '0';
            }
            console.log(period_from, period_to, url);
            // alert(url+'/'+period_from+'/'+period_to);
            window.open(url+'/'+period_from+'/'+period_to);
        }
    </script>
@endsection
