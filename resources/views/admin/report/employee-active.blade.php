@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Employee Active')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                        <form class="" role="form" method="post" action="{{url('reports/employee-active')}}">
                            <h3 class="panel-title">{{language_data('Employee Active')}}</h3>
                            <div class="row center">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Period From')}}</label>
                                        <input type="text" class="form-control datePicker" required="" @if($period_from!='0') value="{{get_date_format($period_from)}}" @endif name="period_from">
                                    </div>    
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Period To')}}</label>
                                        <input type="text" class="form-control datePicker" required="" @if($period_to!='0') value="{{get_date_format($period_to)}}" @endif name="period_to">
                                    </div>                  
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" id="url" value="{{url('reports/download-excel-employee-active/')}}">
                            <a onclick="excel()" class="btn btn-info pull-right"><i class="fa fa-download"></i> Excel</a>
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-search"></i> {{language_data('Search')}}</button>
                        </form>
                        <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">No</th>
                                        <th style="width: 10%;">NIK</th>
                                        <th style="width: 20%;">{{language_data('Name')}}</th>
                                        <th style="width: 20%;">{{language_data('Client Contract Name')}}</th>
                                        <th style="width: 20%;">{{language_data('Employee Type')}}</th>
                                        <th style="width: 20%;">{{language_data('Actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $ctr = 0; ?>
                                    @foreach($employees as $d)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="NIK">{{$d->employee_code}}</td>
                                        <td data-label="Name"><p>
                                            @if($permcheck->R==1) <a href="{{url('employees/view/'.$d->id)}}"> {{$d->fname}} {{$d->lname}}</a></a> @else {{$d->fname}} {{$d->lname}} @endif
                                        </p></td>
                                        <td data-label="Client Contract Name"><p>  @if($d->project_name) {{$d->project_name->project}} @else -@endif </p></td>                                    
                                        <td data-label="Employee Type"><p>{{$d->employee_type}}</p></td>
                                        <td data-label="Actions">
                                        <a class="btn btn-info btn-xs" href="{{url('jobs/download-resume/1/'.$d->id)}}"> Generate CV </a>
                                        </td>
                                    </tr>
    
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });
        function excel()
        {
            var url = document.getElementById("url").value;
            var period_from = document.getElementsByName("period_from")[0].value;
            var period_to = document.getElementsByName("period_to")[0].value;
            if(period_from == ''){
                period_from = '0';
            }
            if(period_to == ''){
                period_to = '0';
            }
            console.log(period_from, period_to, url);
            // alert(url+'/'+period_from+'/'+period_to);
            window.open(url+'/'+period_from+'/'+period_to);
        }
    </script>
@endsection
