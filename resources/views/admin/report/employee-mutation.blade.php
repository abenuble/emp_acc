@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Mutation')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                        <form class="" role="form" method="post" action="{{url('reports/employee-mutation')}}">
                            <h3 class="panel-title">{{language_data('Mutation')}}</h3>
                            <div class="row center">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Period From')}}</label>
                                        <input type="text" class="form-control datePicker" required="" @if($mutation_from!='0') value="{{get_date_format($mutation_from)}}" @endif name="mutation_from">
                                    </div>    
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{language_data('Period To')}}</label>
                                        <input type="text" class="form-control datePicker" required="" @if($mutation_to!='0') value="{{get_date_format($mutation_to)}}" @endif name="mutation_to">
                                    </div>                  
                                </div>
                            </div>
                            <div class="row center">
                                <div class="col-lg-6">
                                    <div class="form-group m-none">
                                        <label for="e20">{{language_data('Mutation Type')}}</label>
                                        <select name="mutation_type" id="type" class="form-control selectpicker">
                                            <option value="0" @if($type=='0') selected @endif>{{language_data('Select Type')}}</option>
                                            <option value="mutasi" @if($type=='mutasi') selected @endif>Mutasi</option>
                                            <option value="demosi" @if($type=='demosi') selected @endif>Demosi</option>
                                            <option value="promosi" @if($type=='promosi') selected @endif>Promosi</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" id="url" value="{{url('reports/download-excel-employee-mutation/')}}">
                            <a onclick="excel()" class="btn btn-info pull-right"><i class="fa fa-download"></i> Excel</a>
                            {{-- <a href="{{url('reports/download-excel-employee-mutation/'.$type.'/'.$mutation_from.'/'.$mutation_to)}}" class="btn btn-info pull-right"><i class="fa fa-download"></i> Excel</a> --}}
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-search"></i> {{language_data('Search')}}</button>
                        </form>
                        <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 10%;">{{language_data('Client Contract Number')}}</th>
                                    <th style="width: 10%;">{{language_data('Company')}}</th>
                                    <th style="width: 15%;">{{language_data('Employee')}}</th>
                                    <th style="width: 5%;">{{language_data('Area Code')}}</th>
                                    <th style="width: 20%;">{{language_data('Department')}}/{{language_data('Designation')}}</th>
                                    <th style="width: 20%;">{{language_data('Employee')}} {{language_data('Status')}}</th>
                                    <th style="width: 5%;">{{language_data('Status')}}</th>
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($mutations as $a)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Project Number"><p>{{$a->project_info->project_number}}</p></td>
                                        <td data-label="Company"><p>{{$a->company_info->company}}</p></td>
                                        <td data-label="Employee"><p>{{$a->employee_info->fname}} {{$a->employee_info->lname}}</p></td>
                                        <td data-label="Area Code"><p>{{$a->company_info->company_code}} > {{$a->company_info_new->company_code}}</p></td>
                                        <td data-label="Department/Designation"><p>{{$a->designation_old_info->designation}} > {{$a->designation_new_info->designation}}</p></td>
                                        <td data-label="Employee Status"><p>{{$a->employee_status_old}} > {{$a->employee_status_new}}</p></td>
                                        @if($a->status=='draft')
                                            <td data-label="status"><a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a></td>
                                        @elseif($a->status=='accepted')
                                            <td data-label="status"><a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a></td>
                                        @else
                                            <td data-label="status"><a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-complete btn-xs" href="{{url('mutations/view/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });
        function excel()
        {
            var url = document.getElementById("url").value;
            var type = document.getElementById("type").value;
            var mutation_from = document.getElementsByName("mutation_from")[0].value;
            var mutation_to = document.getElementsByName("mutation_to")[0].value;
            if(mutation_from == ''){
                mutation_from = '0';
            }
            if(mutation_to == ''){
                mutation_to = '0';
            }
            console.log(mutation_from, mutation_to, url);
            // alert(url+'/'+period_from+'/'+period_to);
            window.open(url+'/'+type+'/'+mutation_from+'/'+mutation_to);
        }
    </script>
@endsection
