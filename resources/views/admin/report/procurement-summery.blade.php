@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/select2/select2.css")!!}
    {!! Html::style("assets/libs/c3/c3.min.css") !!}

@endsection


@section('content')

    <section class="wrapper-bottom-sec">
    <div class="p-30"  >
        
        <div class="panel"  style='padding:10px'>
        <div class="panel-body p-none">
        <form role="form-some-up" method="post" action="{{url('reports/procurement')}}">
            <h2 class="page-title">Laporan Pengadaan</h2><br>
           <div class="row">
                <div class="col-lg-4">
              
                </div>
            <div class="col-lg-8">
                   
                                    <label>{{language_data('Client Contract')}}</label>
                                    <input type="text"value="{{$project}}" name="share_with_specific" id="share_with_specific" style="width:100%" placeholder="choose project"  />
                   
            </div>
           </div>
           <br>
           <div class="row">
           <div class="col-lg-9">
                
                </div>
              
                <div class="col-lg-2"  style="float: right;">
                <div class="form-group">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <button type="submit" class="btn btn-info " style="float: right;"><i class="fa fa-search"></i>Search</button>
               
                </div>
                </div>
          
           </div>
           </form>
           <div class="row" >
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Procurement Charts</h5>
                        </div>
                        <div class="ibox-content">
                            <div id="ct-chart3" ></div>
                        </div>
                    </div>
                </div>
        </div>
        </div>
        </div>
        </div>
       
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Laporan Pengadaan</h3>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 10%;">#</th>
                                    <th style="width: 30%;">Nama Proyek</th>
                                    <th style="width: 30%;">Diajukan</th>
                                    <th style="width: 30%;">Realisasi</th>
                                 
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr=0; $idx=0; ?>
                                @foreach($procurement as $p)
                                    <?php $ctr++; ?>
                                        <tr>
                                    <td >{{$ctr}}</th>
                                    @if($p->project!=null)
                                    <td >{{$p->project_info->project}}</th>
                                    @else
                                    <td >-</th>

                                    @endif
                                    <td >{{$p->diajukan}}</th>
                                    <td >{{$p->realisasi}}</th></tr>
                                        
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/select2/select2.js")!!}
    {!! Html::script("assets/libs/c3/c3.min.js")!!}
    {!! Html::script("assets/libs/c3/d3.min.js")!!}

    <script>
        $(document).ready(function(){
            $(".specific_dropdown").show().find("input").addClass("validate-hidden");
                    var _url = $("#_url").val();
                    $.ajax
                ({
                    url: _url + '/reports/get-projects',
                    cache: false,
                    success: function ( data ) {
                    $("#share_with_specific").select2({
                            tags: true,
                            tokenSeparators: [',', ' '],
                            multiple: true,
                            data:data
                        });
                    }
                    });
                    c3.generate({
                bindto: '#ct-chart3',
                data:{
                    columns: [
                        ['Diajukan',<?php echo $totaldiajukan; ?>],['Realisasi',<?php echo $totalrealisasi; ?>]
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type: 'bar',
                    
                    groups: [
                        ['data1', 'data2']
                    ]
                }
            });
            $('.data-table').DataTable();
        });
    </script>
@endsection
