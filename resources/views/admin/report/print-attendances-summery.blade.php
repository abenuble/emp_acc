<!DOCTYPE html>

<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - assessment Form</title>

  

<head>
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
        {!! Html::style("assets/libs/chart-horizontal/chartist.min.css") !!}
        {!! Html::style("assets/libs/c3/c3.min.css") !!}
        <style>
        table {
    border-collapse: collapse;
}
        table, th, td {
   border: 1px solid black;
   text-align:center;
   margin-left:2px;
   margin-right:2px;
}
        </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">



        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                <div class="p-30"  >
                
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Laporan Kehadiran Karyawan {{$tahun}}</h3>
                            
                        </div>
                        <div class="panel-body p-none">
                        <div class="row" @if($project=="" || $project==null)style="display:none" @endif>
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Project(s) chart</h5>
                        </div>
                        <div class="ibox-content">
                            <div id="ct-chart3"></div>
                        </div>
                    </div>
                </div>
        </div>
                            <table class="table data-table table-hover table-ultra-responsive" >
                                <thead>
                                <tr>
                                    <th style="width: 10%;">#</th>
                                    <th >{{language_data('Name')}}</th>
                                    <th >Jan</th>
                                    <th >Feb</th>
                                    <th >Mar</th>
                                    <th >Apr</th>
                                    <th >May</th>
                                    <th >Jun</th>
                                    <th >Jul</th>
                                    <th >Aug</th>
                                    <th >Sep</th>
                                    <th >Oct</th>
                                    <th >Nov</th>
                                    <th>Des</th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                <?php $idx=0; $ctr=0;?>
                                @foreach($employees as $d)
                                <?php  $ctr++;?>

                                <tr>
                                    <td data-label="SL">{{$ctr}}</td>
                                    <td data-label="Name"><p>{{$d->fname}} {{$d->lname}}({{$d->employee_code}})</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['jan']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['feb']}}</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['mar']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['apr']}}</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['may']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['jun']}}</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['jul']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['aug']}}</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['sep']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['oct']}}</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['nov']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['des']}}</p></td>

                                
                                </tr>
                                <?php  $idx++;?>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                    </div>

                </div>

            </div>
        </div>
        </div>
    </div>  
</main> 
</body>                
    {!! Html::script("assets/libs/jquery-1.10.2.min.js") !!}
{!! Html::script("assets/libs/jquery.slimscroll.min.js") !!}
{!! Html::script("assets/libs/smoothscroll.min.js") !!}
{!! Html::script("assets/libs/bootstrap/js/bootstrap.min.js") !!}
{!! Html::script("assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js") !!}
{!! Html::script("assets/libs/alertify/js/alertify.js") !!}
{!! Html::script("assets/libs/bootstrap-select/js/bootstrap-select.min.js") !!}
{!! Html::script("assets/js/scripts.js") !!}
    {!! Html::script("assets/libs/c3/c3.min.js")!!}
    {!! Html::script("assets/libs/c3/d3.min.js")!!}


<script>
        $(document).ready(function(){
 

                     c3.generate({
                bindto: '#ct-chart3',
                data:{
                    columns: [
                        <?php
                            $ct=1;
                            if($project!=null && $project!=""){
                        foreach ($projectchart as $p){
                            echo "['".$p['namaproyek']."',".$p['total']."]";
                            if($ct!=sizeof($projectchart)){echo ",";}
                            $ct++;
                        }
                    }?>
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type: 'bar',
                    groups: [
                        ['data1', 'data2']
                    ]
                },                    
                tooltip: {
        show: false
    }
            });
                
   
            $('.data-table').DataTable();
        });
    </script>
</html>

