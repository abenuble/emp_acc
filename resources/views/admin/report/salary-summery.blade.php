@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/chart-horizontal/chartist.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
    <div class="p-30"  >
        
        <div class="panel"  style='padding:10px'>
        <div class="panel-body p-none">
        <form role="form" method="post" action="{{url('reports/salary')}}">
            <h2 class="page-title">{{language_data('Employee Payroll Summery')}}</h2>
            <div class="row">
          
            <div class="col-lg-4">
            <label>Bulan</label>
            <input type="text" class="form-control monthPickerOnly" value="{{get_date_month($month)}}" name="month">

            </div>
                <div class="col-lg-4">
                <div class="form-group">
                        <label><b>Proyek</b></label>
                        <select name="project" class="form-control selectpicker companyy" data-live-search="true" id="project">
                        <option value="0">{{language_data('All contract client')}}</option>
                                        @foreach($projects as $c)
                                        <option value="{{$c->id}}">{{$c->project.'('.$c->project_number.')'}}</option>
                                        @endforeach
                                    </select>
                </div>
                </div>
            <div class="col-lg-4">
                   
                                    <label>Perusahaan</label>
                                    <select name="company" class="form-control selectpicker companyy" data-live-search="true" id="company">
                        <option value="0">All Company</option>
                                        @foreach($companies as $c)
                                            <option value="{{$c->id}}">{{$c->company}}</option>
                                        @endforeach
                                    </select>
                                                     
            </div>
      
           </div>
           <div class="row">
          
          <div class="col-lg-4">
          <label>Tahun</label>
          <input type="text" class="form-control yearPicker" value="{{$year}}" name="year">

          </div>
          </div>
           <div class="row">
           <div class="col-lg-9">
                
                </div>
                <div class="col-lg-2"  style="float: right;">
                <div class="form-group">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <button type="submit" class="btn btn-info " style="float: right;"><i class="fa fa-search"></i>Search</button>
                </div>
                </div>
          
           </div>
           </form>
         
            </div>
        </div>
        </div>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Employee Payroll Summery')}}</h3>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                  
                                    <th >Rekening</th>
                                    <th>Plus</th>
                                   
                                    <th >Nominal</th>
                                    <th >Cd</th>
                                    <th>No</th>
                                    <th >Nama</th>
                                    <th>{{language_data('Information')}}</th>
                                    <th >Rekening Perusahaan</th>
                                
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr=0; $idx=0; ?>
                                @foreach($salaries as $d)
                                <?php $ctr++; ?>
                                <tr>
                                        @if(sizeof($d->bank_info)>0)
                                            @if($d->bank_info->account_number!=null)
                                    <td data-label="SL">{{$d->bank_info->account_number}}</td>
                                            @else
                                            <td data-label="SL">-</td>
                                            @endif

                                        @else
                                        <td data-label="SL">-</td>

                                        @endif
                                    <td data-label="SL">+</td>
                                    <td data-label="SL">Rp.{{number_format($d->total,2,",",".")}}</td>
                                    <td data-label="SL">C</td>
                                    <td data-label="SL">{{$ctr}}</td>
                                    <td data-label="SL">{{$d->employee_info->fname}} {{$d->employee_info->lname}}</td>
                                   
                                    <td data-label="SL">gaji/
                                    @if($d->payment_info->project_info->internal_company_name)
                                    @if($d->payment_info->project_info->internal_company_name->id==1) PT. USAHA PATRA LIMA JAYA 
                                    @elseif($d->payment_info->project_info->internal_company_name->id==2)PT. USAHA YEKAPEPE 
                                    @else company 
                                    @endif
                                    @else
                                    -
                                    @endif
                                    </td>
                                    <td data-label="SL">
                                    @if($d->payment_info->project_info->internal_company_name)
                                            {{$d->payment_info->project_info->internal_company_name->rekening}}
                                    @endif
                                    </td>                                 

                                
                                </tr>
                                <?php $idx++; ?>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/chart-horizontal/chartist.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){
          
            
            $("#company").val("{{$company}}").trigger("change");
            $("#project").val("{{$project}}").trigger("change");
            $('.data-table').DataTable();
            
        });
    </script>
@endsection
