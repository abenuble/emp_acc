@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/select2/select2.css")!!}
    {!! Html::style("assets/libs/c3/c3.min.css") !!}
    {!! Html::style("assets/libs/chart-horizontal/chartist.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
    <div class="p-30" >
        
        <div class="panel"  style='padding:10px'>
        <div class="panel-body p-none">
        <form role="form-some-up" method="post" action="{{url('reports/leaves')}}">
            <h2 class="page-title">Laporan Ketidakhadiran Karyawan</h2><br>
           <div class="row">
                <div class="col-lg-4">
                <div class="form-group">
                        <label>Tahun</label>
                        <input type="text" name="tahun" value="{{$curYear}}" class="form-control yearPicker"/>
                </div>
                </div>
             
            <div class="col-lg-8">
            <div class="form-group">
            <div class="specific_dropdown" >
                                    <label>{{language_data('Client Contract')}}</label>
                                    <input type="text"value="" name="share_with_specific"  id="share_with_specific" style="width:100%" placeholder="choose project"  />
</div>
</div>
</div>
                   
            </div>
     
            <div class="row">
           <div class="col-lg-4">
           </div>
           <div class="col-lg-6" style="float: left;">
           <div class="form-group">
            <div class="specific_dropdown1" >
                                    <label>Jenis KetidakHadiran</label>
                                    <input type="text"value="" name="share_with_specific1"  id="share_with_specific1" style="width:100%" placeholder="Choose Leave Type"  />
</div>
</div>
                </div>
                <div class="col-lg-2"  style="float: right;">
                <div class="form-group">
               <br>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <button type="submit" class="btn btn-info " style="float: right;"><i class="fa fa-search"></i>Search</button>
                     
                </div>
                </div>
          
           </div>
           </form>
           <form style='float:right' method="post" action="{{url('/reports/download-pdf-leaves')}}">
           <input type="hidden" name="_token" value="{{csrf_token()}}">
           <input type='hidden' name='projecte'  id='projecte'/>
           <input type='hidden' name='tahune'  id='tahune'/>
           <input type='hidden' name='jenise' id='jenise'/>
                <button  class="btn btn-success print" style='text-align:right'>Generata PDF</button>
           </form>
           </div>
           </div>
           </div>
           <!-- <div class="row" @if($project=="" || $project==null)style="display:none" @endif>
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Project(s) chart</h5>
                        </div>
                        <div class="ibox-content">
                            <div id="ct-chart3"></div>
                        </div>
                    </div>
                </div>
        </div> -->
        <div class="row" @if($leavetype=="" || $leavetype==null)style="display:none" @endif>
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>LeaveType(s) chart</h5>
                        </div>
                        <div class="ibox-content">
                            <div id="ct-chart4"></div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Laporan KetidakHadiran Karyawan</h3>
                            
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 10%;">#</th>
                                    <th style="width: 20%;">{{language_data('Name')}}</th>
                                    <th style="width: 20%;">Jan</th>
                                    <th style="width: 20%;">Feb</th>
                                    <th style="width: 30%;">Mar</th>
                                    <th style="width: 20%;">Apr</th>
                                    <th style="width: 20%;">May</th>
                                    <th style="width: 30%;">Jun</th>
                                    <th style="width: 20%;">Jul</th>
                                    <th style="width: 20%;">Aug</th>
                                    <th style="width: 30%;">Sep</th>
                                    <th style="width: 20%;">Oct</th>
                                    <th style="width: 20%;">Nov</th>
                                    <th style="width: 30%;">Des</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $ct=0;
                                foreach($leaves as $d){
                                    if($d['jan']==null){$leaves[$ct]['jan']=0;}
                                    if($d['feb']==null){$leaves[$ct]['feb']=0;}
                                    if($d['mar']==null){$leaves[$ct]['mar']=0;}
                                    if($d['apr']==null){$leaves[$ct]['apr']=0;}
                                    if($d['may']==null){$leaves[$ct]['may']=0;}
                                    if($d['jun']==null){$leaves[$ct]['jun']=0;}
                                    if($d['jul']==null){$leaves[$ct]['jul']=0;}
                                    if($d['aug']==null){$leaves[$ct]['aug']=0;}
                                    if($d['sep']==null){$leaves[$ct]['sep']=0;}
                                    if($d['oct']==null){$leaves[$ct]['oct']=0;}
                                    if($d['nov']==null){$leaves[$ct]['nov']=0;}
                                    if($d['des']==null){$leaves[$ct]['des']=0;}
                                    $ct++;

                                }
                               ?>
                                <?php $idx=0; $ctr=0;?>
                                @foreach($employees as $d)
                                <?php  $ctr++;?>

                                <tr>
                                    <td data-label="SL">{{$ctr}}</td>
                                    <td data-label="Name"><p>{{$d->fname}} {{$d->lname}}({{$d->employee_code}})</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['jan']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['feb']}}</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['mar']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['apr']}}</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['may']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['jun']}}</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['jul']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['aug']}}</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['sep']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['oct']}}</p></td>
                                    <td data-label="jan"><p>{{$leaves[$idx]['nov']}}</p></td>
                                    <td data-label="feb"><p>{{$leaves[$idx]['des']}}</p></td>

                                
                                </tr>
                                <?php  $idx++;?>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/select2/select2.js")!!}
    {!! Html::script("assets/libs/c3/c3.min.js")!!}
    {!! Html::script("assets/libs/c3/d3.min.js")!!}
    {!! Html::script("assets/libs/chart-horizontal/chartist.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){
            $('.print').click(function(){
                       var project= $('#share_with_specific').val();
                       var jenis= $('#share_with_specific1').val();
                       var tahun= $('.yearPicker').val();
                    
                       $('#tahune').val(tahun);
                       if(project!=null &&project!=""){ 
                        $('#projecte').val(project);
                      }else{ 
                          alert('Field project harus diisi');
                        return false;
                     }
                     if(jenis!=null &&jenis!=""){ 
                        $('#jenise').val(jenis);
                      }else{ 
                        alert('Field jenis harus diisi');
                        return false;
                     }
         
                    });
            $(".specific_dropdown").show().find("input").addClass("validate-hidden");
                    var _url = $("#_url").val();
                    $.ajax
                ({
                    url: _url + '/reports/get-projects',
                    cache: false,
                    success: function ( data ) {
                    $("#share_with_specific").select2({
                            tags: true,
                            tokenSeparators: [',', ' '],
                            multiple: true,
                            data:data
                        });
                    }
                    });

                       $(".specific_dropdown1").show().find("input").addClass("validate-hidden");
                    var _url = $("#_url").val();
                    $.ajax
                ({
                    url: _url + '/reports/get-leave-type',
                    cache: false,
                    success: function ( data ) {
                    $("#share_with_specific1").select2({
                            tags: true,
                            tokenSeparators: [',', ' '],
                            multiple: true,
                            data:data
                        });
                    }
                    });  
                    c3.generate({
                bindto: '#ct-chart3',
                data:{
                    columns: [
                        <?php
                            $ct=1;
                            if($project!=null && $project!=""){
                        foreach ($projectchart as $p){
                            echo "['".$p['namaproyek']."',".$p['total']."]";
                            if($ct!=sizeof($projectchart)){echo ",";}
                            $ct++;
                        }
                    }?>
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type: 'bar',
                    groups: [
                        ['data1', 'data2']
                    ]
                },                    
                tooltip: {
        show: false
    }
            });
            c3.generate({
                bindto: '#ct-chart4',
                data:{
                    columns: [
                        <?php
                            $ct=1;
                            if($leavetype!=null && $leavetype!=""){
                        foreach ($leavechart as $p){
                            echo "['".$p['namaleave']."',".$p['total']."]";
                            if($ct!=sizeof($leavechart)){echo ",";}
                            $ct++;
                        }
                    }?>
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'
                    },
                    type: 'bar',
                    groups: [
                        ['data1', 'data2']
                    ]
                },                    
                tooltip: {
        show: false
    }
            });

            $("#share_with_specific").val("{{$project}}");
            $("#share_with_specific1").val("{{$leavetype}}");
            $('.data-table').DataTable();
        });
    </script>
@endsection
