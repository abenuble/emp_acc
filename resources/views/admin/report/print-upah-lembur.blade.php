<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - {{language_data('Attendance')}}</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        .help-split {
            display: inline-block;
            width: 30%;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
<div class="row">
       <div class="col-xs-3">
       <img src="<?php echo asset('assets/company_pic/usaha-patra-lima-jaya-pt.jpg');?>" alt="Profile Page" width="200px" height="150px">

       </div>
       <div class="col-xs-9 " >
       <br>
       <br>
       <h3 >LAPORAN LEMBUR BULAN {{ strtoupper($bulan)}} {{$year}}</h3>
       </div>
       </div>
       <br><br>
<div class="row">
      <div class="col-xs-3">
      <b>Lokasi :</b>
      </div>
      <div class="col-xs-3">
      {{$dataemployee->company_name->company}}<br>
      {{$dataemployee->company_name->address}}
      </div>
      <div class="col-xs-3">
     <b> No.Pek :</b>
      </div>
      <div class="col-xs-3">
      {{$dataemployee->company_name->rekening}}
      </div>
      </div>
      
   
      <div class="row">
      <div class="col-xs-3">
     <b>Nama :</b>
      </div>
      <div class="col-xs-3">
      {{$dataemployee->fname}}&nbsp;
      {{$dataemployee->lname}}
      </div>
      <div class="col-xs-3">
      
      </div>
      <div class="col-xs-3">
      </div>
      </div>
      <div class="row">
      <div class="col-xs-3">
  <b> Gaji Pokok :</b>
      </div>
      <div class="col-xs-3">
     Rp. {{number_format($dataemployee->salary,2,",",".")}}
      </div>
      <div class="col-xs-3">
     <b> Pekerja : </b>
      </div>
      <div class="col-xs-3">
      {{$dataemployee->department_name->department}}
      </div>
      </div>
      <div class="row">
      <div class="col-xs-3">

      </div>
      <div class="col-xs-3">
      </div>
      <div class="col-xs-3">
    <b> Bagian : </b>
      </div>
      <div class="col-xs-3">
      {{$dataemployee->designation_name->designation}}
      </div>
      </div>
        <br>
           </div>
       </div>
       <div class="p-30 p-t-none p-b-none">
     
           <div class="row">

               <div class="col-lg-12">
                   <div class="panel">
                       <div class="panel-heading">
                           <h3 class="panel-title">Laporan Gaji Lembur</h3>
                       </div>
                       <div class="panel-body p-none">
                           <table class="table data-table table-hover table-ultra-responsive">
                             
                             
                              
                               <tbody>
                               <tr >
                                 
                                 <th colspan='3'></th>
                                 
                                 <th colspan='2' style='text-align:center'> Jam Lembur</th>
                                
                                 <th></th>
                                 <th  ></th>
                                 <th></th>
                                 <th></th>
                       
                                 <th  colspan='2' style='text-align:center'>Jumlah Uang Makan/Rp.</th>
                                 <th ></th>
                                 <th ></th>
                             
                             </tr>
                               <tr>
                                 
                                 <th>No</th>
                                 <th>Hari</th>
                                
                                 <th >Tanggal</th>
                                 <th >Dari</th>
                                 <th>Sampai</th>
                                 <th >Jumlah Jam Lembur</th>
                                 <th>Jumlah Konversi Faktor</th>
                                 <th >Tarif/Jam Rp</th>
                                 <th >Jumlah</th>
                                 <th >05:00 - 07:00 </th>
                                 <th >11:30 - 21:00</th>
                                 <th >Transport Lembur</th>
                                 <th >Total Diterima</th>
                             
                             </tr>
                               <?php $ctr=0; $idx=0; ?>
                               @foreach($tableemployee as $t)
                               <?php $ctr++; ?>
                               <tr>
                               <td data-label="SL">{{$ctr}}</td>     
                                   <td data-label="SL">{{$t['hari']}}</td>
                                        
                                   <td data-label="SL">{{$t['tanggal']}}</td>     
                                   <td data-label="SL">{{$t['dari']}}</td>     
                                   <td data-label="SL">{{$t['sampai']}}</td>                                   
                                   <td data-label="SL">{{$t['total_jam']}} </td>     
                                   <td data-label="SL">{{$t['konversi']}} </td>   
                                   <td data-label="SL">Rp. {{number_format($t['perjam'],2,",",".")}}</td>     
                                   <td data-label="SL">Rp. {{number_format($t['tarif'],2,",",".")}}</td>     
                                   <td data-label="SL">Rp. {{number_format($t['makan_pagi'],2,",",".")}}</td>     
                                   <td data-label="SL">Rp. {{number_format($t['makan_siang'],2,",",".")}}</td>     
                                   <td data-label="SL"> - </td>     
                                   <td data-label="SL">Rp. {{number_format($t['makan_pagi']+$t['makan_siang']+$t['tarif'],2,",",".")}}</td>     


                               
                               </tr>
                               <?php $idx++; ?>
                               @endforeach
                               <?php if(sizeof($tableemployee)>0){?>
                               <tr>
                               <td colspan='5' style='text-align:center'><b>Total</b> </td>     
                               <td data-label="SL">{{ array_sum(array_column($tableemployee,'total_jam'))}}</td>   
                                   <td data-label="SL">{{ array_sum(array_column($tableemployee,'konversi'))}}</td>  
                                   <td data-label="SL">-</td>   
   
                                   <td data-label="SL">Rp.{{number_format( array_sum(array_column($tableemployee,'tarif')),2,",",".")}} </td>     
                                   <td data-label="SL">Rp.{{ number_format(array_sum(array_column($tableemployee,'makan_pagi')),2,",",".")}}</td>     
                                   <td data-label="SL">Rp.{{number_format( array_sum(array_column($tableemployee,'makan_siang')),2,",",".")}} </td>     
                                   <td data-label="SL"> - </td>     
                                   <td data-label="SL">Rp.{{number_format( array_sum(array_column($tableemployee,'makan_siang'))+
                                    array_sum(array_column($tableemployee,'makan_pagi'))+ array_sum(array_column($tableemployee,'tarif'))
                                   ,2,",",".")}} </td>     


                               
                               </tr>
                                   <?php } ?>
                               </tbody>
                           </table>
      
                       </div>
                   </div>

                   </main>
</body>
</html>