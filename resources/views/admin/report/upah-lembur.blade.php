@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/chart-horizontal/chartist.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
    <div class="p-30"  >
        
        <div class="panel"  style='padding:10px'>
        <div class="panel-body p-none">
        <form role="form" method="post" action="{{url('reports/upah-lembur')}}">
            <h2 class="page-title">Laporan Upah Lembur</h2>
            <div class="row">
          
          
                <div class="col-lg-4">
                <div class="form-group">
                        <label><b>{{language_data('Employee')}}</b></label>
                        <select name="employee" @if($role_id!='1') readonly @endif class="form-control selectpicker employeee" data-live-search="true" id="employee">
                        <option value="0"></option>
                                        @foreach($employee as $c)
                                            <option value="{{$c->id}}">{{$c->fname}} {{$c->lname}}({{$c->employee_code}})</option>
                                        @endforeach
                                    </select>
                </div>
                </div>
                <div class="col-lg-4">
            <label>Bulan</label>
            <input type="text" class="form-control monthPickerOnly" value="{{get_date_month($month)}}" name="month">

            </div>
            <div class="col-lg-4">
          <label>Tahun</label>
          <input type="text" class="form-control yearPicker" value="{{$year}}" name="year">

          </div>
      
           </div>
         
           <div class="row">
           <div class="col-lg-9">
                
                </div>
                <div class="col-lg-2"  style="float: right;">
                <div class="form-group">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <button type="submit" class="btn btn-info " style="float: right;"><i class="fa fa-search"></i>Search</button>
                </div>
                </div>
          
           </div>
           </form>
         
            </div>
        </div>
        </div>
        </div>
        <div class="p-30  p-b-none">
       
       <div class='form-group' style='background-color:white;padding-left:6%;padding-right:6%'>
       <div class="row">
       <div class="col-md-3">
       <img src="<?php echo asset('assets/company_pic/usaha-patra-lima-jaya-pt.jpg');?>" alt="Profile Page" width="200px" height="150px">

       </div>
       <div class="col-md-9 " >
       <br>
       <br>
       <h3 >LAPORAN LEMBUR BULAN {{ strtoupper($month)}} {{$year}}</h3>
       </div>
       </div>
       <?php if(sizeof($dataemployee)>0){ ?>
       <br><br>

      <div class="row">
      <div class="col-md-3">
      <b>Lokasi :</b>
      </div>
      <div class="col-md-3">
      {{$dataemployee->company_name->company}}<br>
      {{$dataemployee->company_name->address}}
      </div>
      <div class="col-md-3">
     <b> No.Pek :</b>
      </div>
      <div class="col-md-3">
      {{$dataemployee->company_name->rekening}}
      </div>
      </div>
      
   
      <div class="row">
      <div class="col-md-3">
     <b>Nama :</b>
      </div>
      <div class="col-md-3">
      {{$dataemployee->fname}}&nbsp;
      {{$dataemployee->lname}}
      </div>
      <div class="col-md-3">
      
      </div>
      <div class="col-md-3">
      </div>
      </div>
      <div class="row">
      <div class="col-md-3">
  <b> Gaji Pokok :</b>
      </div>
      <div class="col-md-3">
     Rp. {{number_format($dataemployee->salary,2,",",".")}}
      </div>
      <div class="col-md-3">
     <b> Pekerja : </b>
      </div>
      <div class="col-md-3">
      {{$dataemployee->department_name->department}}
      </div>
      </div>
      <div class="row">
      <div class="col-md-3">

      </div>
      <div class="col-md-3">
      </div>
      <div class="col-md-3">
    <b> Bagian : </b>
      </div>
      <div class="col-md-3">
      {{$dataemployee->designation_name->designation}}
      </div>
      </div>
        <br>
           </div>
       </div>
       <div class="p-30 p-t-none p-b-none">
       
           @include('notification.notify')
           <div class="row">

               <div class="col-lg-12">
                   <div class="panel">
                       <div class="panel-heading">
                           <h3 class="panel-title">Laporan Gaji Lembur</h3>
                       </div>
                       <div class="panel-body p-none" style="overflow-x:auto;">
                           <table class="table data-table table-hover table-ultra-responsive">
                             
                             
                              
                               <tbody>
                               <tr >
                                 
                                 <th colspan='3'></th>
                                 
                                 <th colspan='2' style='text-align:center'> Jam Lembur</th>
                                
                                 <th></th>
                                 <th  ></th>
                                 <th></th>
                                 <th></th>
                                 <th  colspan='2' style='text-align:center'>Jumlah Uang Makan/Rp.</th>
                                 <th ></th>
                                 <th ></th>
                             
                             </tr>
                               <tr>
                                 
                                 <td>No</td>
                                 <td>Hari</td>
                                
                                 <td style="width: 100px">Tanggal</td>
                                 <td >Dari</td>
                                 <td >Sampai</td>
                                 <td >Jumlah Jam Lembur</td>
                                 <td >Jumlah Konversi Faktor</td>
                                 <td >Tarif/Jam Rp</td>
                                 <td >Jumlah</td>
                                 <td >05:00 - 07:00 </td>
                                 <td >11:30 - 21:00</td>
                                 <td >Transport Lembur</td>
                                 <td >Total Diterima</td>
                             
                             </tr>
                               <?php $ctr=0; $idx=0; ?>
                               @foreach($tableemployee as $t)
                               <?php $ctr++; ?>
                               <tr>
                               <td data-label="No">{{$ctr}}</td>     
                                   <td data-label="Hari">{{$t['hari']}}</td>
                                        
                                   <td data-label="Tanggal">{{date("d-m-Y",strtotime($t['tanggal']))}}</td>     
                                   <td data-label="Dari">{{$t['dari']}}</td>     
                                   <td data-label="Sampai">{{$t['sampai']}}</td>                                   
                                   <td data-label="Jumlah Jam Lembur">{{$t['total_jam']}} </td>     
                                   <td data-label="Jumlah Konversi Faktor">{{$t['konversi']}} </td>    
                                   <td data-label="Tarif/Jam Rp">Rp. {{number_format($t['perjam'],2,",",".")}}</td>   
                                   <td data-label="Jumlah">Rp. {{number_format($t['tarif'],2,",",".")}}</td>     
                                   <td data-label="05:00 - 07:00">Rp. {{number_format($t['makan_pagi'],2,",",".")}}</td>     
                                   <td data-label="11:30 - 21:00">Rp. {{number_format($t['makan_siang'],2,",",".")}}</td>     
                                   <td data-label="Transport Lembur"> - </td>     
                                   <td data-label="Total Diterima">Rp. {{number_format($t['makan_pagi']+$t['makan_siang']+$t['tarif'],2,",",".")}}</td>     


                               
                               </tr>
                               <?php $idx++; ?>
                               @endforeach
                               <?php if(sizeof($tableemployee)>0){?>
                               <tr>
                               <td colspan='5' style='text-align:center'><b>Total</b> </td>     
                               <td data-label="Jumlah Jam Lembur">{{ array_sum(array_column($tableemployee,'total_jam'))}}</td>   
                                   <td data-label="Jumlah Konversi Faktor">{{ array_sum(array_column($tableemployee,'konversi'))}}</td>    
                                   <td data-label="Tarif/Jam Rp">-</td>   
                                   <td data-label="Jumlah">Rp.{{number_format( array_sum(array_column($tableemployee,'tarif')),2,",",".")}} </td>     
                                   <td data-label="05:00 - 07:00">Rp.{{ number_format(array_sum(array_column($tableemployee,'makan_pagi')),2,",",".")}}</td>     
                                   <td data-label="11:30 - 21:00">Rp.{{number_format( array_sum(array_column($tableemployee,'makan_siang')),2,",",".")}} </td>     
                                   <td data-label="Transport Lembur"> - </td>     
                                   <td data-label="Total Diterima">Rp.{{number_format( array_sum(array_column($tableemployee,'makan_siang'))+
                                    array_sum(array_column($tableemployee,'makan_pagi'))+ array_sum(array_column($tableemployee,'tarif'))
                                   ,2,",",".")}} </td>     


                               
                               </tr>
                                   <?php } ?>
                               <tr>
                               <td colspan='11' style='text-align:center'> </td>     
                                 
                                   <td data-label="SL">
                                   <a href="{{url('reports/print-upah-lembur/'.$dataemployee->id.'/'.$month.'/'.$year)}}" class="btn btn-info btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate PDF')}}</a><br>  

                                   </td>     


                               
                               </tr>
                               </tbody>
                           </table>
                           <div class="row">
                                        </div>
                       </div>
                       
                   </div>
                
                   <?php  }else{?>
                   <br><br>
                    <div class="row">
                    <div class="col-md-3" >
    
      </div>
      <div class="col-md-6" style='text-align:center' >
    <h3 style='color:red'>*NO DATA</h3>
      </div>
     
      </div>
                   <?php }?>
               </div>

           </div>

       </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/chart-horizontal/chartist.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){
     
            $(".monthPickerOnly").on('dp.change',function(){
                var bulan = $(this).val();
                
                var tahun =  $(".yearPicker").val();
                var _url = $("#_url").val();
                var dataString = 'bulan=' + bulan+'&tahun='+tahun;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/reports/get-employee-upah-lembur',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#employee").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
                
            });
    
            $(".yearPicker").on('dp.change',function(){
                var tahun = $(this).val();
                var bulan =  $(".monthPickerOnly").val();
                var _url = $("#_url").val();
                var dataString = 'bulan=' + bulan+'&tahun='+tahun;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/reports/get-employee-upah-lembur',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#employee").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
             });
            $("#employee").val("{{$employeee}}").trigger("change");
            <?php  if($role_id!='1'){ ?>
               $("#employee").val($employeee_id).trigger("change");
         <?php  }?>
            $('.data-table').DataTable();
            
        });
    </script>
@endsection
