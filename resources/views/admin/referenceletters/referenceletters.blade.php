@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Surat Refrensi</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Surat Refrensi</h3>
                           
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 10%;">No</th>
                                    <th style="width: 10%;">{{language_data('Code')}}#</th>
                                    <th style="width: 20%;">{{language_data('Name')}}</th>
                                    <th style="width: 20%;">{{language_data('Username')}}</th>
                                    <th style="width: 20%;">{{language_data('Employee Type')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 20%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($employees as $d)
                                <?php $ctr++; ?>
                                <tr>
                                    <td data-label="No" >{{$ctr}}</td>
                                    <td data-label="SL">{{$d->employee_code}}</td>
                                    <td data-label="Name"><p>
                                        @if($employee_permission->R==1) <a href="{{url('employees/view/'.$d->id)}}"> {{$d->fname}} {{$d->lname}}</a> @else {{$d->fname}} {{$d->lname}} @endif
                                    </p></td>
                                    <td data-label="Username"><p>{{$d->user_name}}</p></td>
                                    <td data-label="Username"><p>{{$d->employee_type}}</p></td>
                                    @if($d->status=='active')
                                    <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Active')}}</p></td>
                                    @else
                                    <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Inactive')}}</p></td>
                                    @endif
                                    <td data-label="Actions">
                                        <a class="btn btn-info btn-xs" href="{{url('reference-letter/view/'.$d->id)}}" ><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                    </td>
                                </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('employee/importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>

                                *.xls (Excel 2003), Download template <a href="{{ URL::to('employee/downloadExcel/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal id card -->
            <div class="modal fade" id="id" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Generate ID Card')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ url('employees/download-id-card') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Client Contract')}}</label>
                                    <select name="project" class="form-control selectpicker w100p validate-hidden" data-live-search="true">
                                            <option value='0'>{{language_data('Select Client Contract')}}</option>
                                        @foreach($projects as $d)
                                            <option value='{{$d->id}}' >{{$d->project}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Generate')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Download -->
            <div class="modal fade" id="download" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Download')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{url('employees/download-employee-data')}}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="row">
                                    <div class="form-group">
                                        <label>{{language_data('Based On')}} :</label><br>
                                        <label class="radio-inline">
                                            <input type="radio" name="based" value="project" class="toggle_specific">{{language_data('Client Contract')}}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="based" value="company" class="toggle_specific">{{language_data('Company')}}
                                        </label>
                                        <div class="specific_dropdown" style="display: none;">
                                            <label>{{language_data('Company')}}</label>
                                            <select name="company" class="form-control selectpicker w100p validate-hidden" data-live-search="true">
                                                    <option value='0'>{{language_data('Select Company')}}</option>
                                                @foreach($company as $d)
                                                    <option value='{{$d->id}}' >{{$d->company}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="specific_dropdown2" style="display: none;">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <select name="project" class="form-control selectpicker w100p validate-hidden" data-live-search="true">
                                                    <option value='0'>{{language_data('Select Client Contract')}}</option>
                                                @foreach($projects as $d)
                                                    <option value='{{$d->id}}' >{{$d->project}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="employee_code">NIK</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="fname">Nama Karyawan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="gender">Jenis Kelamin</label>
                                        </div>         
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="id">No. KTP</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="birth_place">Tempat Lahir</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="dob">Tanggal Lahir</label>
                                        </div>         
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="skck">No. SKCK</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="per_address">Alamat Asal</label>
                                        </div>                    
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="per_city">Kota Asal</label>
                                        </div>                    
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="per_district">Kecamatan Asal</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="pre_address">Alamat Domisili</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="pre_city">Kota Domisili</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="pre_district">Kecamatan Domisili</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="phone">Nomor Telpon</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="email">Email</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="religion">Agama</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="last_education">Pendidikan Terakhir</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="university">Lembaga Institusi Pendidikan Terakhir</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="first_year">Tahun Tempuh Pendidikan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="marital_status">Status Perkawinan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="child">Jumlah Tanggungan Anak</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="spouse">Nama Suami/Istri</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="designation">Jabatan/Posisi</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="doj">Tanggal Masuk</label>
                                        </div>  
                                        {{--  <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="remaining working period">Sisa Masa Kerja</label>
                                        </div>  --}}
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="location">Lokasi</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" value="employee_status">Status Karyawan</label>
                                        </div>   
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="checkalll" >Check All</label>
                                        </div>   
                                    </div>
                                </div>
                                
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Download')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){
            $('#checkalll').change(function() {
                if(this.checked) {
                    $('input:checkbox[name="variable[]"]').prop('checked',true);
                }
                else{
                    $('input:checkbox[name="variable[]"]').prop('checked',false);

                }
                    
            });
            $('.data-table').DataTable();

            $(".toggle_specific").click(function () {
                toggle_specific_dropdown();
            });
            toggle_specific_dropdown();
        
            function toggle_specific_dropdown() {
                var $element = $(".toggle_specific:checked");
                if ($element.val() === "company") {
                    $(".specific_dropdown").show().find("select").addClass("validate-hidden");
                    $(".specific_dropdown2").hide().find("select").removeClass("validate-hidden");
                } else if($element.val() === "project") {
                    $(".specific_dropdown").hide().find("select").removeClass("validate-hidden");
                    $(".specific_dropdown2").show().find("select").addClass("validate-hidden");
                } 
            }

        });
    </script>
@endsection
