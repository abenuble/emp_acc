@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title"> Detail Surat Refrensi</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body p-t-20">
                            <div class="clearfix">
                                <div class="pull-left m-r-30">
                                    <div class="thumbnail m-b-none">

                                        @if($employee->avatar!='')
                                            <img src="<?php echo asset('assets/employee_pic/'.$employee->avatar); ?>" alt="Profile Page" width="200px" height="200px">
                                        @else
                                            <img src="<?php echo asset('assets/employee_pic/user.png');?>" alt="Profile Page" width="200px" height="200px">
                                        @endif
                                    </div>
                                </div>
                                <div class="pull-left">
                                    <h3 class="bold font-color-1">{{$employee->fname}} {{$employee->lname}}</h3>
                                    <ul class="info-list">
                                        @if($employee->email!='')
                                        <li><span class="info-list-title">{{language_data('Email')}}</span><span class="info-list-des">{{$employee->email}}</span></li>
                                        @endif

                                        @if($employee->phone!='')
                                            <li><span class="info-list-title">{{language_data('Phone')}}</span><span class="info-list-des">{{$employee->phone}}</span></li>
                                        @endif

                                        @if($employee->user_name!='')
                                            <li><span class="info-list-title">{{language_data('Username')}}</span><span class="info-list-des">{{$employee->user_name}}</span></li>
                                        @endif

                                        @if($employee->pre_address!='')
                                        <li><span class="info-list-title">{{language_data('Address')}}</span><span class="info-list-des">{{$employee->pre_address}}</span></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="p-30 p-t-none p-b-none">
            <div class="row">
                <div class="col-lg-12">
                 

                    <!-- Tab panes -->
                    <div class="tab-content panel p-20">

                   <div  id="employment_history">
                            <div class="row">
                            <label>{{language_data('Employment History')}}</label>
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">No</th>
                                        <th style="width: 20%;">{{language_data('Client Contract')}}</th>
                                        <th style="width: 20%;">{{language_data('Company')}}</th>
                                        <th style="width: 20%;">{{language_data('Division')}}</th>
                                        <th style="width: 10%;">{{language_data('Location')}}</th>
                                        <th style="width: 10%;">{{language_data('Start Date')}}</th>
                                        <th style="width: 10%;">{{language_data('End Date')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($employment as $e)
                                    <tr>
                                        <td data-label="no">{{$e->project_info->project_number}}</td>
                                        <td data-label="project">{{$e->project_info->project}}</td>
                                        <td data-label="company">{{$e->employee_info->company_name->company}}</td>
                                        <td data-label="department">{{$e->designation_info->designation}}</td>
                                        <td data-label="location">{{$e->location}}</td>
                                        <td data-label="start_date">{{get_date_format($e->start_date)}}</td>
                                        <td data-label="end_date">{{get_date_format($e->end_date)}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>

                            <div class="row">
                            <label>{{language_data('Employee Contract Data')}}</label>
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">No</th>
                                        <th style="width: 40%;">{{language_data('Employee Contract')}}</th>
                                        <th style="width: 20%;">{{language_data('Effective Date')}}</th>
                                        <th style="width: 20%;">{{language_data('End Date')}}</th>
                                        <th style="width: 10%;" class="text-right">{{language_data('Actions')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $ctr=1; ?>
                                @foreach($contract as $c)
                                    <tr>
                                        <td data-label="no">{{$ctr++}}</td>
                                        <td data-label="contract">{{$c->letter_number}}</td>
                                        <td data-label="effective_date">{{get_date_format($c->contract_info->effective_date)}}</td>
                                        <td data-label="end_date">{{get_date_format($c->contract_info->end_date)}}</td>
                                        <td data-label="Actions" class="text-right">
                                            <a class="btn btn-complete btn-xs" href="{{url('contracts/view/'.$c->contract_id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>



                      

                       
                       

                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            $("td.number1").each(function(i,v) {
                $(v).text(i + 1);
            });
            $("td.number2").each(function(i,v) {
                $(v).text(i + 1);
            });
            $("td.number3").each(function(i,v) {
                $(v).text(i + 1);
            });
            $("td.number4").each(function(i,v) {
                $(v).text(i + 1);
            });
            $("td.number5").each(function(i,v) {
                $(v).text(i + 1);
            });

            $('#datetimepicker1').datetimepicker({
                // locale: 'id',
                viewMode: 'years',
                format: 'YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                // locale: 'id',
                viewMode: 'years',
                format: 'YYYY'
            });

            /*For DataTable*/
            $('.data-table').DataTable();


            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });


            /*For Delete Bank Account*/
            $(".deleteBankAccount").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/employee/delete-bank-account/" + id;
                    }
                });
            });

            /*For Delete Employee Doc*/
            $(".deleteEmployeeDoc").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/employee/delete-employee-doc/" + id;
                    }
                });
            });

            /*For Math remaining work period*/
            var yearBirth = {{get_date_year($employee->dob)}};
            var maxYear = 55;
            var yearNow = {{get_date_year($date=date("Y"))}};

            var remainingWork = yearBirth + maxYear - yearNow;
            document.getElementById("remainingWork").innerHTML = '<input type="text" class="form-control" readonly value="'+remainingWork+' tahun">' ;

        });

        function addFields()
        {
            // Number of inputs to create
            var number = document.getElementById("new_number").value;
            // Container <div> where dynamic content will be placed
            var container = document.getElementById("depend");
            // Clear previous contents of the container
            while (container.hasChildNodes()) {
                container.removeChild(container.lastChild);
            }
            container.appendChild(document.createTextNode("Name of New Children"));
            for (i=0;i<number;i++){
                // Create an <input> element, set its type and name attributes
                var input = document.createElement("input");
                input.type = "text";
                input.className = "form-control";
                input.name = "name_child[]";
                container.appendChild(input);
                // Append a line break 
                container.appendChild(document.createElement("br"));
            }
            if (number==3){
                var add = document.createElement("BUTTON");
                add.setAttribute("type", "button");
                add.setAttribute("class", "btn btn-success btn-sm pull-right");
                add.setAttribute("onClick", "addInput('depend')");
                var text = document.createTextNode("Add");
                add.appendChild(text);
                container.appendChild(add);
            } else{

            }
        }
        
        var numberChild = 3;
        function addInput(divName)
        {
            var newdiv = document.createElement('div');
            newdiv.innerHTML = " <br><input type='text' class='form-control' name='name_child[]'>";
            document.getElementById(divName).appendChild(newdiv);
            numberChild++;
            
        }
    </script>

@endsection
