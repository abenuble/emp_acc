@extends('master')

@section('content')
    <style>
        .indent {
            text-indent: 50px;
        }
    </style>
    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{$emp_roles->role_name}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <?php 
                use App\Menu;
                use App\EmployeeRolesPermission;
                ?>
                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('employees/update-employee-set-roles')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> {{language_data('Set Roles')}}</h3>
                                </div>

                                <table class="table data-table table-hover table-ultra-responsive">
                                    <thead>
                                        <tr>
                                            <th> Menu <input type="checkbox" name="menu_all" id="menu_all" onclick="select_all_menu(this)"> <span class="help">check/uncheck all</span> </th>
                                            <th width="5%"> C </th>
                                            <th width="5%"> R </th>
                                            <th width="5%"> U </th>
                                            <th width="5%"> D </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($menu as $ctr => $m)
                                            <?php 
                                                $permission = EmployeeRolesPermission::where('perm_id','=',$m->id)->where('role_id','=',$emp_roles->id)->first();
                                            ?>
                                            <tr>
                                                <td> <input type="hidden" name="menu[]" value="{{$m->id}}"> <input type="hidden" name="permission[]" value="{{$permission->id}}"> <input type="checkbox" class="checks" name="{{$permission->id}}" id="{{$permission->id}}" @if($permission->C==1 || $permission->R==1 || $permission->U==1 || $permission->D==1) checked @endif onclick="select_all(this)">  <strong>{{language_data($m->name)}}</strong> </td>
                                                <td align="center"> <input type="checkbox" class="checks" name="create_menu_{{$permission->id}}" id="create_menu_{{$permission->id}}" @if($permission->C==1) checked @endif value="1"> </td>
                                                <td align="center"> <input type="checkbox" class="checks" name="read_menu_{{$permission->id}}" id="read_menu_{{$permission->id}}" @if($permission->R==1) checked @endif value="1"> </td>
                                                <td align="center"> <input type="checkbox" class="checks" name="update_menu_{{$permission->id}}" id="update_menu_{{$permission->id}}" @if($permission->U==1) checked @endif value="1"> </td>
                                                <td align="center"> <input type="checkbox" class="checks" name="delete_menu_{{$permission->id}}" id="delete_menu_{{$permission->id}}" @if($permission->D==1) checked @endif value="1"> </td>
                                            </tr>
                                            <?php 
                                                $has_sub = Menu::where('menu_parent','=',$m->id)->first();
                                                $sub_menus = Menu::where('menu_parent','=',$m->id)->where('status','=','y')->orderBy('indexs','ASC')->get();
                                            ?>
                                            @if($has_sub)
                                                @foreach($sub_menus as $ctr2 => $sm)
                                                    <?php 
                                                        $permission_sub_menu = EmployeeRolesPermission::where('perm_id','=',$sm->id)->where('role_id','=',$emp_roles->id)->first();
                                                    ?>
                                                    <tr>
                                                        <td> <input type="hidden" name="submenu[]" value="{{$sm->id}}"> <input type="hidden" name="permission_sub_menu[]" value="{{$permission_sub_menu->id}}"> <input type="checkbox" class="checks" name="{{$permission_sub_menu->id}}" id="{{$permission_sub_menu->id}}" @if($permission_sub_menu->C==1 || $permission_sub_menu->R==1 || $permission_sub_menu->U==1 || $permission_sub_menu->D==1) checked @endif onclick="select_all_sub(this)"> <p class="indent">{{language_data($sm->name)}}</p> </td>
                                                        <td align="center"> <input type="checkbox" class="checks" name="create_submenu_{{$permission_sub_menu->id}}" id="create_submenu_{{$permission_sub_menu->id}}" @if($permission_sub_menu->C==1) checked @endif value="1"> </td>
                                                        <td align="center"> <input type="checkbox" class="checks" name="read_submenu_{{$permission_sub_menu->id}}" id="read_submenu_{{$permission_sub_menu->id}}" @if($permission_sub_menu->R==1) checked @endif value="1"> </td>
                                                        <td align="center"> <input type="checkbox" class="checks" name="update_submenu_{{$permission_sub_menu->id}}" id="update_submenu_{{$permission_sub_menu->id}}" @if($permission_sub_menu->U==1) checked @endif value="1"> </td>
                                                        <td align="center"> <input type="checkbox" class="checks" name="delete_submenu_{{$permission_sub_menu->id}}" id="delete_submenu_{{$permission_sub_menu->id}}" @if($permission_sub_menu->D==1) checked @endif value="1"> </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                                <input type="hidden" value="{{$emp_roles->id}}" name="role_id">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    <script>
        $(document).ready(function(){
            if ($('.checks').is(':checked')) {
                $("#menu_all").prop("checked", true);
            };
        });
        function select_all_menu(element)
        {
            if(element.checked)
            {
                $(".checks").prop("checked", true);
            }
            else
            {
                $(".checks").prop("checked", false);
            }
        }
        function select_all(element)
        {
            var id = $(element).attr("id");
            if(element.checked)
            {
                $("#create_menu_"+id).prop("checked", true);
                $("#read_menu_"+id).prop("checked", true);
                $("#update_menu_"+id).prop("checked", true);
                $("#delete_menu_"+id).prop("checked", true);
            }
            else
            {
                $("#create_menu_"+id).prop("checked", false);
                $("#read_menu_"+id).prop("checked", false);
                $("#update_menu_"+id).prop("checked", false);
                $("#delete_menu_"+id).prop("checked", false);
            }
        }
        function select_all_sub(element)
        {
            var id = $(element).attr("id");
            if(element.checked)
            {
                $("#create_submenu_"+id).prop("checked", true);
                $("#read_submenu_"+id).prop("checked", true);
                $("#update_submenu_"+id).prop("checked", true);
                $("#delete_submenu_"+id).prop("checked", true);
            }
            else
            {
                $("#create_submenu_"+id).prop("checked", false);
                $("#read_submenu_"+id).prop("checked", false);
                $("#update_submenu_"+id).prop("checked", false);
                $("#delete_submenu_"+id).prop("checked", false);
            }
        }
    </script>
@endsection
