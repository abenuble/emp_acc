@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    <style>
    td {
        vertical-align: top;
        padding-left: 5px;
        padding-right: 5px;
    }
    </style>
@endsection



@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Profile')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body p-t-20">
                            <div class="clearfix">
                                <div class="pull-left m-r-30">
                                    <div class="thumbnail m-b-none">

                                        @if($employee->avatar!='')
                                            <img src="<?php echo asset('assets/employee_pic/'.$employee->avatar); ?>" alt="Profile Page" width="200px" height="200px">
                                        @else
                                            <img src="<?php echo asset('assets/employee_pic/user.png');?>" alt="Profile Page" width="200px" height="200px">
                                        @endif
                                    </div>
                                </div>
                                <div class="pull-left">
                                    <h3 class="bold font-color-1">{{$employee->fname}} {{$employee->lname}}</h3>
                                    <ul class="info-list">
                                        @if($employee->email!='')
                                        <li><span class="info-list-title">{{language_data('Email')}}</span><span class="info-list-des">{{$employee->email}}</span></li>
                                        @endif

                                        @if($employee->phone!='')
                                            <li><span class="info-list-title">{{language_data('Phone')}}</span><span class="info-list-des">{{$employee->phone}}</span></li>
                                        @endif

                                        @if($employee->user_name!='')
                                            <li><span class="info-list-title">Username</span><span class="info-list-des">{{$employee->user_name}}</span></li>
                                        @endif

                                        @if($employee->pre_address!='')
                                        <li><span class="info-list-title">{{language_data('Address')}}</span><span class="info-list-des">{{$employee->pre_address}}</span></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="p-30 p-t-none p-b-none">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#personal_details" aria-controls="home" role="tab" data-toggle="tab">{{language_data('Personal Details')}}</a></li>
                        <li role="presentation"><a href="#dependents" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Dependents')}}</a></li>
                        <li role="presentation"><a href="#work" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Work')}}</a></li>
                        <li role="presentation"><a href="#finance_and_insurance" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Finance and Insurance')}}</a></li>
                        <li role="presentation"><a href="#sick" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Medical History')}}</a></li>

                       <li role="presentation"><a href="#employment_history" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Employment History')}}</a></li>
                        <li role="presentation"><a href="#salary_history" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Salary History')}}</a></li>
                        <li role="presentation"><a href="#warning_history" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Warning History')}}</a></li>
                        {{--  <li role="presentation"><a href="#medical_history" aria-controls="profile" role="tab" data-toggle="tab">{{language_data('Medical History')}}</a></li>  --}}
                        <li role="presentation"><a href="#cuti" aria-controls="settings" role="tab" data-toggle="tab">{{language_data('Leave History')}}</a></li>
                        <li role="presentation"><a href="#documents" aria-controls="settings" role="tab" data-toggle="tab">{{language_data('Documents')}}</a></li>
                        <li role="presentation"><a href="#change-picture" aria-controls="settings" role="tab" data-toggle="tab">{{language_data('Change Picture')}}</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content panel p-20">

                    {{--Personal Details--}}
                        <div role="tabpanel" class="tab-pane active" id="personal_details">
                            <form class="form-some-up form-block" role="form" action="{{url('employees/post-employee-personal-info')}}" method="post">
                                <table width="100%">
                                    <tr>
                                        <td width="33%">
                                            <div class="form-group">
                                                <label>{{language_data('Full Name')}}</label>
                                                <input type="text" class="form-control name"  name="fname" value="{{$employee->fname}}">
                                            </div>
                                            {{-- <div class="form-group">
                                                <label>{{language_data('Last Name')}}</label>
                                                <input type="text" class="form-control name"  name="lname" value="{{$employee->lname}}">
                                            </div> --}}
                                            <div class="form-group">
                                                <label>{{language_data('Employee Code')}}</label>
                                                <span class="help">e.g. "546814" ({{language_data('Unique For every User')}})</span>
                                                <input type="text" class="form-control"  name="employee_code" value="{{$employee->employee_code}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Gender')}}</label>
                                                <select class="selectpicker form-control"  name="gender">
                                                    <option value="male" @if($employee->gender=='male') selected @endif>{{language_data('male')}}</option>
                                                    <option value="female" @if($employee->gender=='female') selected @endif>{{language_data('female')}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>No. KTP</label>
                                                <input type="text" class="form-control"  name="no_ktp" value="{{$employee->no_ktp}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Birth Place')}}</label>
                                                <span class="help">e.g. "Surabaya"</span>
                                                <input type="text" class="form-control"  name="birth_place" value="{{$employee->birth_place}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Date Of Birth')}}</label>
                                                <input type="text" class="form-control datePicker" name="dob" value="{{get_date_format($employee->dob)}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data("Mother's Name")}}</label>
                                                <input type="text" class="form-control name"  name="mother_name" value="{{$employee->mother_name}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('SKCK Number')}}</label>
                                                <input type="text" class="form-control"  name="skck" value="{{$employee->skck}}">
                                            </div>
                                        </td>
                                        <td width="33%">
                                            <div class="form-group">
                                                <label>{{language_data('Permanent Address')}}</label>
                                                <textarea class="form-control" rows="6" placeholder="Alamat"  name="per_address">{{$employee->per_address}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Permanent Village')}}</label>
                                                <input type="text" class="form-control"  name="per_village" value="{{$employee->per_village}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Permanent District')}}</label>
                                                <input type="text" class="form-control"  name="per_district" value="{{$employee->per_district}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Permanent City')}}</label>
                                                <input type="text" class="form-control"  name="per_city" value="{{$employee->per_city}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Present Address')}}</label>
                                                <textarea class="form-control" rows="6" placeholder="Alamat"  name="pre_address">{{$employee->pre_address}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Present Village')}}</label>
                                                <input type="text" class="form-control"  name="pre_village" value="{{$employee->pre_village}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Present District')}}</label>
                                                <input type="text" class="form-control"  name="pre_district" value="{{$employee->pre_district}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Present City')}}</label>
                                                <input type="text" class="form-control"  name="pre_city" value="{{$employee->pre_city}}">
                                            </div>
                                        </td>
                                        <td width="33%">
                                            <div class="form-group">
                                                <label>{{language_data('Phone Number')}}</label>
                                                <input type="text" class="form-control"  name="phone" value="{{$employee->phone}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Email')}}</label>
                                                <input type="email" class="form-control"  name="email" value="{{$employee->email}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Religion')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true" name="religion">
                                                    <option value="islam" @if($employee->religion=='islam') selected @endif>Islam</option>
                                                    <option value="hindu" @if($employee->religion=='hindu') selected @endif>Hindu</option>
                                                    <option value="budha" @if($employee->religion=='budha') selected @endif>Budha</option>
                                                    <option value="kristen" @if($employee->religion=='kristen') selected @endif>Kristen</option>
                                                    <option value="katholik" @if($employee->religion=='katholik') selected @endif>Katholik</option>
                                                    <option value="kong hu cu" @if($employee->religion=='kong hu cu') selected @endif>Kong Hu Cu</option>
                                                    <option value="other" @if($employee->religion=='other') selected @endif>Other</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Last Education')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true" name="last_education">
                                                    @foreach($last_education as $l)
                                                        <option value="{{$l->id}}" @if($employee->last_education==$l->id) selected @endif>{{$l->education}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('University')}}</label>
                                                <input type="text" class="form-control"  name="university" value="{{$employee->university}}">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Status')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true"  name="status">
                                                    <option value="active" @if($employee->status=='active') selected @endif>{{language_data('Active')}}</option>
                                                    <option value="inactive" @if($employee->status=='inactive') selected @endif>{{language_data('Inactive')}}</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                               
                                @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$employee->id}}">
                                <button type="submit" class="btn btn-primary btn-sm">{{language_data('Update')}}</button>
                                @endif

                            </form>        
                        </div>

                        {{--Dependents--}}
                        <div role="tabpanel" class="tab-pane" id="dependents">

                            <form class="form-some-up form-block" role="form" action="{{url('employees/add-employee-dependents')}}" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{language_data('Marital Status')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true"  name="marital_status">
                                                <option value="single"  @if($employee->marital_status == 'single') selected @endif>{{language_data('Single')}}</option>
                                                <option value="married" @if($employee->marital_status == 'married') selected @endif>{{language_data('Married')}}</option>
                                                <option value="divorced"@if($employee->marital_status == 'divorced') selected @endif>{{language_data('Divorced')}}</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Name of Husband/Wife')}}</label>
                                            <input type="text" class="form-control"  value="{{$employee->spouse}}" name="spouse">
                                        </div>

                                        <div class="form-group alldesg">
                                            <label>{{language_data('Number of Child Dependents')}}</label>
                                            <select class="selectpicker form-control" disabled data-live-search="true" id="number">
                                                <option value="0" @if(count($dependents) == '0') selected @endif >0</option>
                                                <option value="1" @if(count($dependents) == '1') selected @endif >1</option>
                                                <option value="2" @if(count($dependents) == '2') selected @endif >2</option>
                                                <option value="3" @if(count($dependents) >= '3') selected @endif >>3</option>
                                            </select>
                                        </div>
                                            
                                    </div>
                                    <div class='col-md-6'>
                                        <table class="table table-hover table-ultra-responsive" id="tabledependents">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10%;">No</th>
                                                    <th>{{language_data('Name')}}</th>
                                                    <th><button class="btn btn-success btn-xs pull-right" type="button" id="add" value="{{language_data('Add')}}" onclick="Javascript:addRow()">+</button></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $ctr=0; ?>
                                            @foreach($dependents as $e)
                                            <?php $ctr++; ?>
                                                <tr>
                                                    <td data-label="no" >{{$ctr}}</td>
                                                    <td data-label="name" >{{$e->child_name}}</td>
                                                    <td><a href="#" data-toggle="tooltip" data-placement="right" title="{{language_data('Delete')}}" class="text-success cdelete"  id="{{$e->id}}"><i class="fa fa-trash"></i></a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                                @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$employee->id}}">
                                <button type="submit" class="btn btn-primary btn-sm">{{language_data('Update')}}</button>
                                @endif
                            </form>
                        </div>

                        {{--Work--}}
                        <div role="tabpanel" class="tab-pane" id="work">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{language_data('Employee Type')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$employee->company_name->category}}" name="employee_type">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Company')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$employee->company_name->company}}" name="company">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Designation')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$employee->designation_name->designation}}" name="designation">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Date Of Join')}}</label>
                                        <input type="text" class="form-control datePicker" readonly value="{{get_date_format($employee->doj)}}" name="doj">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Remaining Working Period')}}</label>
                                        <p id="remainingWork"></p>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{language_data('Client Contract')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$employee->project_name->project}}" name="project">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Employee Status')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$employee->employee_status}}" name="employee_status">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Department')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$employee->department_name->department}}" name="department">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Date Of Leave')}}</label>
                                        <input type="text" class="form-control datePicker" readonly value="{{get_date_format($employee->dol)}}" name="dol">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Location')}}</label>
                                        <input type="text" class="form-control" readonly value="{{$employee->location}}" name="location">
                                    </div>

                                </div>
                            </div>
                     
                        </div>

                        {{--Finance and Insurance--}}
                        <div role="tabpanel" class="tab-pane" id="finance_and_insurance">
                            <form class="form-some-up form-block" role="form" action="{{url('employee/add-bank-account')}}" method="post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{language_data('Bank')}}</label>
                                            <input type="text" class="form-control"  name="bank" value="{{$bank_accounts->bank_name}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{language_data('Account Number')}}</label>
                                            <input type="text" class="form-control"  name="account_number" value="{{$bank_accounts->account_number}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{language_data('Account Name')}}</label>
                                            <input type="text" class="form-control"  name="account_name" value="{{$bank_accounts->account_name}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('Tax Status')}}</label>
                                            <input type="text" class="form-control"  name="tax_status" value="{{$bank_accounts->tax_status}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('NPWP Status')}}</label>
                                            <input type="text" class="form-control"  name="npwp_status" value="{{$bank_accounts->npwp_status}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('NPWP Number')}}</label>
                                            <input type="text" class="form-control"  name="npwp_number" value="{{$bank_accounts->npwp_number}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('Income Tax Discount')}}</label>
                                            <input type="text" class="form-control"  name="pph_cut" value="{{$bank_accounts->pph_cut}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('Employment BPJS Number')}}</label>
                                            <input type="text" class="form-control"  name="employment_bpjs_number" value="{{$bank_accounts->employee_bpjs}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('Employment BPJS Discount')}}</label>
                                            <input type="text" class="form-control"  name="employment_bpjs_cut" value="{{$bank_accounts->employee_bpjs_cut}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('Health BPJS Number')}}</label>
                                            <input type="text" class="form-control"  name="health_bpjs_number" value="{{$bank_accounts->health_bpjs}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('Health BPJS Discount')}}</label>
                                            <input type="text" class="form-control"  name="health_bpjs_cut" value="{{$bank_accounts->health_bpjs_cut}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('Other Insurance Name')}}</label>
                                            <input type="text" class="form-control"  name="other_insurance_name" value="{{$bank_accounts->other_insurance_name}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('Other Insurance Number')}}</label>
                                            <input type="text" class="form-control"  name="other_insurance_number" value="{{$bank_accounts->other_insurance_number}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('Other Insurance Discount')}}</label>
                                            <input type="text" class="form-control"  name="other_insurance_cut" value="{{$bank_accounts->other_insurance_cut}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{language_data('Effective Date')}}</label>
                                            <input type="text" class="form-control datePicker"  name="effective_date" value="{{get_date_format($bank_accounts->effective_date)}}">
                                        </div>
                                    </div>
                                </div>

                                @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$employee->id}}">
                                <button type="submit" class="btn btn-primary btn-sm">{{language_data('Update')}}</button>
                                @endif
                            </form>
                        </div>

                       
                        {{--Employment History--}}
                        <div role="tabpanel" class="tab-pane" id="employment_history">
                            <div class="row">
                            <label>{{language_data('Employment History')}}</label>
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">No</th>
                                        <th style="width: 20%;">{{language_data('Client Contract')}}</th>
                                        <th style="width: 20%;">{{language_data('Company')}}</th>
                                        <th style="width: 20%;">{{language_data('Division')}}</th>
                                        <th style="width: 10%;">{{language_data('Location')}}</th>
                                        <th style="width: 10%;">{{language_data('Start Date')}}</th>
                                        <th style="width: 10%;">{{language_data('End Date')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $ctr=0; ?>
                                @foreach($employment as $e)
                                <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="no" >{{$ctr}}</td>
                                        <td data-label="number" >{{$e->project_info->project}}({{$e->project_info->project_number}})</td>

                                        <td data-label="company">{{$e->employee_info->company_name->company}}</td>
                                        <td data-label="department">{{$e->department_info->department}}/{{$e->designation_info->designation}}</td>
                                        <td data-label="location">{{$e->location}}</td>
                                        <td data-label="start_date">{{get_date_format($e->start_date)}}</td>
                                        <td data-label="end_date">{{get_date_format($e->end_date)}} </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>

                            <div class="row">
                            <label>{{language_data('Employee Contract Data')}}</label>
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 25%;">{{language_data('Client Contract')}}</th>
                                        <th style="width: 20%;">{{language_data('Employee Contract')}}</th>
                                        <th style="width: 20%;">{{language_data('Effective Date')}}</th>
                                        <th style="width: 20%;">{{language_data('End Date')}}</th>
                                        <th style="width: 10%;" class="text-right">{{language_data('Actions')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($contract as $c)
                                <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="no" >{{$ctr}}</td>
                                        <td data-label="projetc" >{{$c->project_info->project_number}}</td>
                                        <td data-label="contract">{{$c->letter_number}}</td>
                                        @if($c->contract_info)
                                            <td data-label="effective_date">{{get_date_format($c->contract_info->effective_date)}}</td>
                                            <td data-label="end_date">{{get_date_format($c->contract_info->end_date)}}</td>
                                        @endif
                                        <td data-label="Actions" class="text-right">
                                            <a class="btn btn-complete btn-xs" href="{{url('contracts/view/'.$c->contract_id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                        {{--Salary History--}}
                        <div role="tabpanel" class="tab-pane" id="sick">
                            <div class="row">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">No</th>
                                        <th style="width: 20%;">{{language_data('Name of Illness')}}</th>
                                        <th style="width: 50%;">{{language_data('From')}}</th>
                                        <th style="width: 20%;" >{{language_data('To')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($sick as $s)
                                <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="no" >{{$ctr}}</td>
                                        <td data-label="sick_name">{{$s->sick_name}}</td>
                                        <td data-label="leave_from">{{$s->leave_from}}</td>
                                        <td data-label="leave_to">{{$s->leave_to}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                        {{--Salary History--}}
                        <div role="tabpanel" class="tab-pane" id="salary_history">
                            <div class="row">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">No</th>
                                        <th style="width: 20%;">{{language_data('Salary Type')}}</th>
                                        <th style="width: 50%;">{{language_data('Total Salary Accepted')}}</th>
                                        <th style="width: 20%;" class="text-right">{{language_data('Actions')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($salary as $s)
                                <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="no" >{{$ctr}}</td>
                                        <td data-label="Date">{{$s->payroll_info->payroll_name}}</td>
                                        <td data-label="total_salary_accepted">{{app_config('CurrencyCode')}} {{number_format($s->salary,0)}}</td>
                                        <td data-label="Actions" class="text-right">
                                            <a class="btn btn-success btn-xs"  href="{{url('payroll/view-payroll-types/'.$s->payroll)}}">{{language_data('Details')}}</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>

                        {{--Warning History--}}
                        <div role="tabpanel" class="tab-pane" id="warning_history">
                            <div class="row">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">No</th>
                                        <th style="width: 15%;">{{language_data('Warning Type')}}</th>
                                        <th style="width: 15%;">{{language_data('Reason')}}</th>
                                        <th style="width: 20%;">{{language_data('Effective Date')}}</th>
                                        <th style="width: 20%;">{{language_data('End Date')}}</th>
                                        <th style="width: 20%;" class="text-right">{{language_data('Actions')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($warning as $w)
                                    @if($w->status=='accepted')
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="no" >{{$ctr}}</td>
                                        <td data-label="warning_type">{{$w->type}}</td>
                                        <td data-label="reason_type">{{$w->violate}}</td>
                                        <td data-label="effective_date">{{get_date_format($w->effective_date)}}</td>
                                        <td data-label="end_date">{{get_date_format($w->end_date)}}</td>
                                        <td data-label="Actions" class="text-right">
                                            <a class="btn btn-complete btn-xs" href="{{url('warnings/view/'.$w->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>

                        {{--Medical History--}}
                        <div role="tabpanel" class="tab-pane" id="medical_history">
                            <div class="row">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">No</th>
                                        <th style="width: 15%;">{{language_data('Warning Type')}}</th>
                                        <th style="width: 15%;">{{language_data('Reason')}}</th>
                                        <th style="width: 20%;">{{language_data('Effective Date')}}</th>
                                        <th style="width: 20%;">{{language_data('End Date')}}</th>
                                        <th style="width: 20%;" class="text-right">{{language_data('Actions')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($warning as $w)
                                    @if($w->status=='accepted')
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="no" >{{$ctr}}</td>
                                        <td data-label="warning_type">{{$w->type}}</td>
                                        <td data-label="reason_type">{{$w->violate}}</td>
                                        <td data-label="effective_date">{{get_date_format($w->effective_date)}}</td>
                                        <td data-label="end_date">{{get_date_format($w->end_date)}}</td>
                                        <td data-label="Actions" class="text-right">
                                            <a class="btn btn-complete btn-xs" href="{{url('warnings/view/'.$w->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>

                        {{--Documents--}}
                        <div role="tabpanel" class="tab-pane" id="documents">
                            <div class="row">
                                <div class="row">

                                    @if($permcheck->U==1)
                                    <div class="col-lg-3">
                                        <div class="panel">
                                            <div class="panel-body">
                                                <form class="" role="form" method="post" action="{{url('employee/add-document')}}" enctype="multipart/form-data">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title"> {{language_data('Add Document')}}</h3>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>{{language_data('Document Name')}}</label>
                                                        <input type="text" class="form-control" required name="document_name">
                                                    </div>

                                                    <div class="form-group">

                                                        <label>{{language_data('Select Document')}}</label>
                                                        <div class="input-group input-group-file">
                                                                <span class="input-group-btn">
                                                                    <span class="btn btn-primary btn-file">
                                                                        {{language_data('Browse')}} <input type="file" class="form-control" name="file">
                                                                    </span>
                                                                </span>
                                                            <input type="text" class="form-control" readonly="">
                                                        </div>
                                                    </div>

                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" value="{{$employee->id}}" name="cmd">
                                                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add')}} </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    <div class="col-lg-9">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">{{language_data('All Documents')}}</h3>
                                            </div>
                                            <div class="panel-body p-none">
                                                <table class="table data-table table-hover table-ultra-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 5%;">No</th>
                                                        <th style="width: 60%;">{{language_data('Document Name')}}</th>
                                                        <th style="width: 35%;" class="text-right">{{language_data('Actions')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $ctr = 0; ?>
                                                    @foreach($employee_doc as $pd)
                                                        <?php $ctr++; ?>
                                                        <tr>
                                                            <td data-label="no" >{{$ctr}}</td>
                                                            <td data-label="Document Name">{{$pd->file_title}}</td>
                                                            <td class="text-right">
                                                                <a href="{{url('employee/download-employee-document/'.$pd->id)}}" class="btn btn-success btn-xs"><i class="fa fa-download"></i> {{language_data('Download')}}</a>
                                                                <a href="#" class="btn btn-danger btn-xs deleteEmployeeDoc" id="{{$pd->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>

                        {{--Documents--}}
                        <div role="tabpanel" class="tab-pane" id="cuti">
                            <div class="row">
                                <div class="row">

                                    @if($permcheck->U==1)
                                    <div class="col-lg-5">
                                        <form class="" role="form" method="post" action="{{url('employee/add-cuti')}}" enctype="multipart/form-data">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"> {{language_data('Add Employee Leave Information')}}</h3>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label>{{language_data('From')}}</label>
                                                    <input type="text" class="form-control" required name="date_from" id="date_from">
                                                </div>
                                            </div>
                
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label>{{language_data('To')}}</label>
                                                    <input type="text" class="form-control" required name="date_to" id="date_to">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>{{language_data('Reason')}}</label>
                                                <input type="text" class="form-control" required name="reason">
                                            </div>

                                            <div class="form-group">
                                                <label>{{language_data('Select Document')}}</label>
                                                <div class="input-group input-group-file">
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-primary btn-file">
                                                                {{language_data('Browse')}} <input type="file" class="form-control" name="file">
                                                            </span>
                                                        </span>
                                                    <input type="text" class="form-control" readonly="">
                                                </div>
                                            </div>

                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" value="{{$employee->id}}" name="cmd">
                                            <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add')}} </button>
                                        </form>
                                    </div>
                                    @endif

                                    <div class="col-lg-7">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">{{language_data('All Documents')}}</h3>
                                            </div>
                                            <div class="panel-body p-none">
                                                <table class="table data-table table-hover table-ultra-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 5%;">No</th>
                                                        <th style="width: 20%;">{{language_data('From')}}</th>
                                                        <th style="width: 20%;">{{language_data('To')}}</th>
                                                        <th style="width: 30%;">{{language_data('Reason')}}</th>
                                                        <th style="width: 30%;">{{language_data('Document')}}</th>
                                                        <th style="width: 5%;" class="text-right">{{language_data('Actions')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $ctr = 0; ?>
                                                    @foreach($employee_leave as $el)
                                                        <?php $ctr++; ?>
                                                        <tr>
                                                            <td data-label="no">{{$ctr}}</td>
                                                            <td data-label="Tgl Mulai">{{$el->date_from}}</td>
                                                            <td data-label="Tgl Sampai">{{$el->date_to}}</td>
                                                            <td data-label="Alasan">{{$el->reason}}</td>
                                                            <td data-label="Document">{{$el->file}}</td>
                                                            <td class="text-right">
                                                                <a href="{{url('employee/download-employee-cuti-document/'.$el->id)}}" class="btn btn-success btn-xs"><i class="fa fa-download"></i> {{language_data('Download')}}</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="change-picture">
                            <form role="form" action="{{url('employees/update-employee-avatar')}}" method="post" enctype="multipart/form-data">

                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group input-group input-group-file">
                                            <span class="input-group-btn">
                                                <span class="btn btn-primary btn-file">
                                                    {{language_data('Browse')}} <input type="file" class="form-control" name="image">
                                                </span>
                                            </span>
                                        <input type="text" class="form-control" readonly="">
                                        
                                    </div>

                                    @if($role_id == 1)
                                    <div class="form-group">
                                        <label>{{language_data('User Role')}}</label>
                                        <select class="selectpicker form-control"  name="role_update">
                                            @foreach($role as $r)
                                                <option value="{{$r->id}}" @if($employee->role_id==$r->id) selected @endif>{{$r->role_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="cmd" value="{{$employee->id}}">
                                        <input type="submit" value="{{language_data('Update')}}" class="btn btn-primary">

                                    </div>

                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {

            
            $('#date_from,#date_to').datetimepicker({
                locale: 'id',
                useCurrent: false,
                format: 'DD MMMM YYYY',
                minDate: moment().subtract(1, 'days')
            });
            $('#date_from').datetimepicker().on('dp.change', function (e) {
                var incrementDay = moment(new Date(e.date));
            
                $('#date_to').data('DateTimePicker').minDate(incrementDay);
                $(this).data("DateTimePicker").hide();
            });

            $('#date_to').datetimepicker().on('dp.change', function (e) {
                var decrementDay = moment(new Date(e.date));
            
                $('#date_from').data('DateTimePicker').maxDate(decrementDay);
                    $(this).data("DateTimePicker").hide();
            });

            $('#datetimepicker1').datetimepicker({
                viewMode: 'years',
                format: 'YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                viewMode: 'years',
                format: 'YYYY'
            });

            /*For DataTable*/
            $('.data-table').DataTable();


            /*For Delete Dependents*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/employee/delete-dependent/" + id;
                    }
                });
            });
            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });


            /*For Delete Bank Account*/
            $(".deleteBankAccount").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/employee/delete-bank-account/" + id;
                    }
                });
            });

            /*For Delete Employee Doc*/
            $(".deleteEmployeeDoc").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/employee/delete-employee-doc/" + id;
                    }
                });
            });

            /*For Math remaining work period*/
            var yearBirth = {{get_date_year($employee->dob)}};
            var maxYear = 55;
            var yearNow = {{get_date_year($date=date("Y"))}};

            var remainingWork = yearBirth + maxYear - yearNow;
            document.getElementById("remainingWork").innerHTML = '<input type="text" class="form-control" readonly value="'+remainingWork+' tahun">' ;

        });

        function addFields()
        {
            // Number of inputs to create
            var number = document.getElementById("new_number").value;
            // Container <div> where dynamic content will be placed
            var container = document.getElementById("depend");
            // Clear previous contents of the container
            while (container.hasChildNodes()) {
                container.removeChild(container.lastChild);
            }
            container.appendChild(document.createTextNode("Name of New Children"));
            for (i=0;i<number;i++){
                // Create an <input> element, set its type and name attributes
                var input = document.createElement("input");
                input.type = "text";
                input.className = "form-control";
                input.name = "name_child[]";
                container.appendChild(input);
                // Append a line break 
                container.appendChild(document.createElement("br"));
            }
            if (number==3){
                var add = document.createElement("BUTTON");
                add.setAttribute("type", "button");
                add.setAttribute("class", "btn btn-success btn-sm pull-right");
                add.setAttribute("onClick", "addInput('depend')");
                var text = document.createTextNode("Add");
                add.appendChild(text);
                container.appendChild(add);
            } else{

            }
        }
        
        var numberChild = 3;
        function addInput(divName)
        {
            var newdiv = document.createElement('div');
            newdiv.innerHTML = " <br><input type='text' class='form-control' name='name_child[]'>";
            document.getElementById(divName).appendChild(newdiv);
            numberChild++;
            
        }

        function addRow() {

            var table = document.getElementById("tabledependents");

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            row.insertCell(0).innerHTML= rowCount;
            row.insertCell(1).innerHTML= '<input type="text" class="form-control" value="" name="name_child[]">';
            row.insertCell(2).innerHTML= '<a href="#" data-toggle="tooltip" data-placement="right" title="{{language_data("Delete")}}" class="text-success" onClick="Javacsript:deleteRow(this)" ><i class="fa fa-trash"></i></a>';

        }

        function deleteRow(obj) {
            
            var index = obj.parentNode.parentNode.rowIndex;
            var table = document.getElementById("tabledependents");
            table.deleteRow(index);
        
        }
    </script>

@endsection
