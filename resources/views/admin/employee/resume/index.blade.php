
<main id="wrapper" class="wrapper" style="background-color: white;">
    <div class="container container-printable" style="background-color: white;">
        <div class="p-30 p-t-none p-b-none" style="background-color: white;">
        <div class="col-lg-12">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-body">
                <form class="" role="form" action="{{url('assessments/edit-form')}}" method="post">
                <div class="row" style="padding-left:20px">
            <table width="100%">
                <tbody>
                <tr>
                    <td style="border: 0;  text-align: center" width="100%">
                        <span style="font-size: 14px;"><strong>
                        <br>
                        <h2 style='font-family: Calibri;font-weight:500'>CURRICULUM VITAE</h2><br>
                   
                    </td>
                    </tr>
                    <tr>
                    <td style="background-color:#888;text-align: center;padding-top:-5px" width="100%">
                        <h3 style="font-size: 14px;"><strong>
                        DATA PRIBADI
                        </h3>
                    </td>
                    </tr>
                    <br><br>
                    <tr>
                    <td style="border: 0;  text-align: right" width="62%">
                      <div class="row pull-left">
                        <div class="col-xs-4">
                        <img src="<?php echo asset('assets/employee_pic/user.png');?>" alt="Profile Page" width="118px" height="157px">
                        </div>
                        <div class="col-xs-8 ">
                        <div class="form-group pull-left" style='text-align:justify'>
                            <span  >Nama Lengkap : {{$employee->fname}} {{$employee->lname}}</span><br>
                            <span  >Jenis Kelamin : {{$employee->gender}} </span><br>
                            <span  >Tempat / Tanggal Lahir : {{$employee->birth_place}} , {{get_date_format($employee->dob)}}</span><br>
                            <span  >No KTP : {{$employee->no_ktp}} </span><br>
                           <span  >Agama : {{$employee->religion}} </span><br>
                           <span  >Alamat : {{$employee->pre_address}} </span><br>
                           <span  >Kota Domisili : {{$employee->per_city}} </span><br>
                           <span  >Kecamatan Domisili : {{$employee->per_disctirct}} </span><br>
                           <span  >Kota Asal : {{$employee->pre_city}} </span><br>
                           <span  >Kecamatan Asal : {{$employee->pre_disctrict}} </span><br>
                           <span  >Email : {{$employee->email}} </span><br>
                           <span  >Telpon : {{$employee->phone}} </span><br>
                           <span  >No SKSC : {{$employee->sksc}} </span><br>
                           <span  >Pendidikan Terakhir :  
                           
                           @if($employee->last_education=='3') SMA/SMK @endif
                               @if($employee->last_education=='5')D3 @endif
                               @if($employee->last_education=='7') S1 @endif
                               @if($employee->last_education=='8') S2 @endif</span><br>
          <span  >Universitas : {{$employee->university}} </span><br>
          <span  >Tahun Pertama : {{$employee->first_year}} </span><br>
          <span  >Tahun Terakhir : {{$employee->last_year}} </span><br>
          </div>
                        </div>
                      </div>

                    
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#888; text-align: center;padding-top:-5px" width="100%">
                        <h3 style="font-size: 14px;"><strong>
                      TANGGUNGAN
                   </h3>
                    </td>
                    </tr>
                <tr>
                <td>
                <div class="row pull-left">
                <br>
                <div class="form-group" style='padding-left:10px'>
                <span  >Status Pernikah : {{$employee->marital_status}} </span><br>
                <span  >Nama Suami/Istri : {{$employee->spouse}} </span><br>
                <span  >Jumlah Tanggungan Anak: {{sizeof($dependents)}} </span><br>
                <span  >Anak : </span><br>
                @foreach($dependents as $depend)
                <span  >{{$depend->child_name}} </span><br>
                                       
                @endforeach
                </div>
                </div>
                  </td>
                </tr>
                <tr>
                    <td style="background-color:#888; text-align: center;padding-top:-5px" width="100%">
                        <h3 style="font-size: 14px;"><strong>
                      KERJA
                   </h3>
                    </td>
                    </tr>
                 <tr>
                <td>
                <div class="row pull-left">
                <br>
                <div class="form-group" style='padding-left:10px'>
                <span  >Tipe Karyawan : {{$employee->company_name->category}}</span><br>
                <span  >Proyek : {{$employee->project_name->project}}</span><br>
                <span  >Perusahaan : {{$employee->company_name->company}} </span><br>
                <span  >Status Karyawan : {{$employee->status}}</span><br>
                <span  >Posisi : {{$employee->designation_name->designation}}</span><br>
                <span  >Divisi : {{$employee->department_name->department}}</span><br>
                <span  >Tanggal Bergabung : {{get_date_format($employee->doj)}}</span><br>
                <span  >Lokasi : {{$employee->location}}</span><br>
               </div>
                </div>
                  </td>
                </tr>
                <tr>
                    <td style="background-color:#888;  text-align: center;padding-top:-5px" width="100%">
                        <h3 style="font-size: 14px;"><strong>
                        KEUANGAN DAN ASURANSI
                   </h3>
                    </td>
                    </tr>
                <tr>
                <td>
                <div class="row pull-left">
                <br>
                <div class="form-group" style='padding-left:10px'>

                    @if($bank_accounts)
                <span  >Bank : {{$bank_accounts->bank_name}}</span><br>
                <span  >Nomor Akun : {{$bank_accounts->}}</span><br>
                <span  >Nama Akun :{{$bank_accounts->account_name}} </span><br>
                <span  >Status Pajak : {{$bank_accounts->tax_status}}</span><br>
                <span  >Status NPWP : {{$bank_accounts->npwp_status}}</span><br>
                <span  >Nomor PWMP : {{$bank_accounts->npwp_number}}</span><br>
                <span  >Nomor BPJS Ketenagakerjaan : {{$bank_accounts->employment_bpjs}}</span><br>
                <span  >Nomor BPJS Kesehatan : {{$bank_accounts->health_bpjs}}</span><br>
                <span  >Nama Asuransi Lain : "{{$bank_accounts->other_insurance_name}}</span><br>
                <span  >Nomor Asuransi Lain :{{$bank_accounts->other_insurance_number}}</span><br>
                <span  >Tanggal Berlaku : {{get_date_format($bank_accounts->effective_date)}}</span><br>
                <span  >Fungsi pengguna pusat biaya : {{$bank_accounts->user_cost_center_function}}</span><br>
                <span  >Nama fungsi pengguna : {{$bank_accounts->user_function_name}}</span><br>
                <span  >Fungsi KBO pengguna : {{$bank_accounts->user_kbo_function}}</span><br>
                <span  >Nama KBO : {{$bank_accounts->kbo_name}}</span><br>
                    @else
                    <span  >-</span><br>
                    @endif
                    </div>
                    <table style="background-color:#545454" width="100%" cellspacing="0" cellpadding="20" border="0">
                        <tbody>
                            <tr>
                                <td valign="top" align="center">
                                <table style="text-align:left" width="670" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        
                                        <tr>
                                            <td style="background-color:#f26726">
                                            <table style="background-color:#f26726" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="width:10px;line-height:0pt" valign="bottom"><img alt="" style="border-width:0px;border-style:solid" src="l.png" width="10" height="6" class="CToWUd"></td>
                                                        <td style="text-align:left;color:#ffffff;line-height:24px;font-family:Arial;font-size:20px;font-weight:bold;background-color:#f26726">Informasi Proyek </td>
                                                        <td style="width:10px;line-height:0pt" valign="bottom"><img alt="" style="border-width:0px;border-style:solid" src="r.png" width="10" height="6" class="CToWUd"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="width:10px;text-align:left;line-height:0pt;font-size:0pt"> </td>
                                                        <td style="padding-top:10px;background-color:#ffffff">
                                                        <table style="font-family:Arial,sans-serif" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width:20px"> </td>
                                                                    <td style="padding:5px 0px;width:150px">Nama Proyek</td>
                                                                    <td style="padding:5px 0px;font-weight:bold">Mayfair</td>
                                                                    <td style="width:20px"> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> </td>
                                                                    <td style="padding:5px 0px">Pengembang</td>
                                                                    <td style="padding:5px 0px;font-weight:bold">Wiraland</td>
                                                                    <td> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> </td>
                                                                    <td style="padding:5px 0px">Jenis Property</td>
                                                                    <td style="padding:5px 0px;font-weight:bold">Residential Landed</td>
                                                                    <td> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> </td>
                                                                    <td style="padding:5px 0px">Status</td>
                                                                    <td style="padding:5px 0px;font-weight:bold">Hak Guna Bangunan</td>
                                                                    <td> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> </td>
                                                                    <td style="padding:5px 0px">Lokasi</td>
                                                                    <td style="padding:5px 0px;font-weight:bold">Medan</td>
                                                                    <td> </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </td>
                                                        <td style="width:10px"> </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="width:10px"> </td>
                                                        <td style="background-color:#ffffff">&nbsp;</td>
                                                        <td style="width:10px"> </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td style="padding:0px 10px;text-align:left;line-height:0pt;font-size:0pt">
                                            <img alt="" src="https://ci4.googleusercontent.com/proxy/wj7EvoWIUq69fIQKkNnIMoezWqBT6nMYIJEVttZum3wbR_vkYgXCpdr0_wJPveTUWeiPA-JUVY0WM-1QqD1odg9l-ry7fIE-o_ZLt2wxC_L4=s0-d-e1-ft#http://mobile.epropertytrack.com/files/img/shadow_title.jpg" class="CToWUd">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                  </td>
                </tr>
             
                </tbody>
            </table>
                   
                           
                            </div>

    <br>
    <div class="row" style="background-color:#888;  text-align: center;padding-top:-5px" width="100%">
                    <div class='col-xs-12'>
                        <h3 style="font-size: 14px;"><strong>
                        KEUANGAN DAN ASURANSI
                   </h3>
                    </div>
                    </div>
                <div class="row">
                
                            <label>{{language_data('Employment History')}}</label>
                            <div class="col-lg-12">
                                    <div class="panel">
                                      
                                        <div class="panel-body p-none">
                                            <table class="table">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width: 20%;">No</th>
                                        <th style="width: 10%;">{{language_data('Project')}}</th>
                                        <th style="width: 10%;" >{{language_data('Company')}}</th>
                                        <th style="width: 10%;">{{language_data('Division')}}</th>
                                        <th style="width: 10%;">{{language_data('Location')}}</th>
                                        <th style="width: 10%;">{{language_data('Start Date')}}</th>
                                        <th style="width: 10%;">{{language_data('End Date')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($employment as $e)
                                    <tr>
                                   
                                        <td   >{{$e->project_info->project_number}}</td>
                                        <td >{{$e->project_info->project}}</td>
                                        <td >{{$e->employee_info->company_name->company}}</td>

                                        <td >@if($e->department_info) {{$e->department_info->department}}/{{$e->designation_info->designation}} @endif</td>
                                        <td >{{$e->location}}</td>
                                        <td >{{get_date_format($e->project_info->end_date)}}</td>
                                      
                                        <td>@if($e->fiind){{get_date_format($e->fiind->end_date)}}  @else -@endif</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                            </div>
                             </div>
                            </div>
                          
<br>

                             <div class="row">
                            <label>Data Kontrak</label>
                            <div class="col-lg-12">
                                    <div class="panel">
                                      
                                        <div class="panel-body p-none">
                                            <table class="table">
                           
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 20%;">{{language_data('Contract')}}</th>
                                        <th style="width: 20%;">{{language_data('Effective Date')}}</th>
                                        <th style="width: 20%;">{{language_data('End Date')}}</th>
                                      
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($contract as $c)
                                    <tr>
                                        <td >{{$e->project_info->project_number}}</td>
                                        <td >{{$c->letter_number}}</td>
                                            @if($c->contract_info)
                                        <td >{{get_date_format($c->contract_info->effective_date)}}</td>
                                        <td >{{get_date_format($c->contract_info->end_date)}}</td>
                                            @endif

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                           </div>
                           </div>
                            </div>
                        </form>
                        </div>
            </div></div>
                 </div>
            </div>
        
        </div>
    </div>  
</main>

