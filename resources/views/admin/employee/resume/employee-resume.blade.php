<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - CURRICULUM VITAE</title>

<head>
    {!! Html::style("https://fonts.googleapis.com/css?family=Roboto:400,300,500,700") !!}
    {!! Html::style("http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css") !!}
    {!! Html::style("application/resources/views/admin/employee/resume/css/font-awesome.min.css") !!}
    {!! Html::style("application/resources/views/admin/employee/resume/css/bootstrap.min.css") !!}
    {!! Html::style("application/resources/views/admin/employee/resume/css/style.css") !!}
    <style> 
      td {
        font-size: 13px; font-family: "Times New Roman", Times, sans-serif;
      }
      th {
        font-size: 13px; font-family: "Times New Roman", Times, sans-serif;
      }
      h1 {
        font-weight: bold; font-family: "Times New Roman", Times, sans-serif; text-align: center;
      }
      center{
        text-align: center;
      }
      .section-title{
        text-align: center; background-color: lightgrey;
      }
      .img-responsive{
        height: 240px;
        width: 180px;
      }
    </style>
</head>
<body>
<div>
<div class="container">
<center><h1>CURRICULUM VITAE</h1></center>
<div class="expertise-wrapper col-md-12 section-wrapper gray-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title" >
                    <center><h1>DATA PRIBADI</h1></center>
                </div>
            </div>
        </div>
        <table>
            <tr>
                <td rowspan="13" width="33%">
                    <div class="profile-img">
                        @if($employee->avatar!='')
                            <img src="<?php echo asset('assets/employee_pic/'.$employee->avatar); ?>" class="img-responsive" alt=""/>
                        @else
                            <img src="<?php echo asset('assets/employee_pic/user.png');?>" class="img-responsive" alt=""/>
                        @endif
                    </div>
                </td>
                <td width="1%"></td>
                <td width="29%"><strong>Nama Lengkap</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="33%">{{$employee->fname}} {{$employee->lname}}</td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"><strong>NIK KTP</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="33%">{{$employee->no_ktp}}</td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"><strong>Alamat KTP</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="33%">{{$employee->per_address}}, {{$employee->per_village}}, {{$employee->per_district}}, {{$employee->per_city}}</td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"><strong>Tempat/Tanggal Lahir</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="33%">{{$employee->birth_place}} , {{get_date_format($employee->dob)}}</td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"><strong>Agama</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="33%">{{$employee->religion}}</td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"><strong>Status Perkawinan</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="33%">{{$employee->marital_status}}</td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"><strong>Telepon</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="33%">{{$employee->phone}}</td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"><strong>Email</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="33%">{{$employee->email}}</td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"></td>
                <td width="3%"></td>
                <td width="33%"></td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"></td>
                <td width="3%"></td>
                <td width="33%"></td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"></td>
                <td width="3%"></td>
                <td width="33%"></td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"></td>
                <td width="3%"></td>
                <td width="33%"></td>
            </tr>
            <tr>
                <td width="1%"></td>
                <td width="29%"></td>
                <td width="3%"></td>
                <td width="33%"></td>
            </tr>
        </table>


    </div>
</div>
<!-- .expertise-wrapper col-md-12 -->


<div class="expertise-wrapper col-md-12 section-wrapper gray-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title" >
                    <center><h1>TANGGUNGAN</h1></center>
                </div>
            </div>
        </div>
        <table>
        <tr>
            <td width="37%"><strong>Status Pernikahan</strong></td>
            <td width="3%"><center>:</center></td>
            <td width="60%">{{$employee->marital_status}}</td>
        </tr>
        <tr>
            <td width="37%"><strong>Nama Suami/Istri</strong></td>
            <td width="3%"><center>:</center></td>
            <td width="60%">{{$employee->spouse}}</td>
        </tr>
        <tr>
            <td width="37%"><strong>Jumlah Tanggungan Anak</strong></td>
            <td width="3%"><center>:</center></td>
            <td width="60%">{{sizeof($dependents)}}</td>
        </tr>
        <tr>
            <td width="37%"><strong>Anak</strong></td>
            <td width="3%"><center>:</center></td>
            <td width="60%">
                @foreach($dependents as $depend)
                <span> - {{$depend->child_name}} </span><br>
                @endforeach
            </td>
        </tr>
    </table>


    </div>
</div>
<!-- .expertise-wrapper col-md-12 -->

<div class="section-wrapper skills-wrapper gray-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title" >
                    <center><h1>KERJA</h1></center>
                </div>
            </div>
        </div>
        <table>
            <tr>
                <td width="37%"><strong>Tipe Karyawan</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$employee->company_name->category}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Proyek</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$employee->project_name->project}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Perusahaan</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$employee->company_name->company}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Status Karyawan</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$employee->status}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Posisi</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$employee->designation_name->designation}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Divisi</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$employee->department_name->department}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Tanggal Bergabung</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{get_date_format($employee->doj)}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Lokasi</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$employee->location}}</td>
            </tr>
        </table>
    </div>
    <!-- .container-fluid -->
</div>
<!-- .skills-wrapper -->
<div class="section-wrapper skills-wrapper gray-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title" >
                    <center><h1>KEUANGAN DAN ASURANSI</h1></center>
                </div>
            </div>

        </div>
        

        <table>
            <tr>
                <td width="37%"><strong>Bank</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->bank_name}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Nomor Akun</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->account_number}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Nama Akun</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->account_name}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Status Pajak</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->tax_status}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Status NPWP</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->npwp_status}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Nomor NPWP</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->npwp_number}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Nomor BPJS Ketenagakerjaan</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->employment_bpjs}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Nomor BPJS Kesehatan</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->health_bpjs}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Nama Asuransi Lain</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->other_insurance_name}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Nomor Asuransi Lain</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->other_insurance_number}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Tanggal Berlaku</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{get_date_format($bank_accounts->effective_date)}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Fungsi pengguna pusat biaya</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->user_cost_center_function}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Nama fungsi pengguna</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->user_function_name}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Fungsi KBO pengguna</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->user_kbo_function}}</td>
            </tr>
            <tr>
                <td width="37%"><strong>Nama KBO</strong></td>
                <td width="3%"><center>:</center></td>
                <td width="60%">{{$bank_accounts->kbo_name}}</td>
            </tr>
        </table>
    </div>
    <!-- .container-fluid -->
</div>
<!-- .skills-wrapper -->

<div class="expertise-wrapper col-md-12 section-wrapper gray-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title" >
                    <center><h1>RIWAYAT PEKERJAAN</h1></center>
                </div>
            </div>
        </div>
        <table>
            <thead>
                <tr>
                    <th style="width: 5%;">No</th>
                    <th style="width: 25%;">{{language_data('Project')}}</th>
                    <th style="width: 20%;" >{{language_data('Company')}}</th>
                    <th style="width: 20%;">{{language_data('Division')}}</th>
                    <th style="width: 10%;">{{language_data('Location')}}</th>
                    <th style="width: 10%;">Durasi</th>
                </tr>
            </thead>
            <tbody>
            <?php $ctr = 1;?>
            @foreach($employment as $e)
                <tr>
                    <td >{{$ctr++}}</td>
                    <td >{{$e->project_info->project}}</td>
                    <td >{{$e->employee_info->company_name->company}}</td>
                    <td >@if($e->department_info) {{$e->department_info->department}}/{{$e->designation_info->designation}} @endif</td>
                    <td >{{$e->location}}</td>
                    <td >{{date('d/m/Y',strtotime($e->project_info->end_date))}} - @if($e->fiind){{date('d/m/Y',strtotime($e->fiind->end_date))}}  @else -@endif</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- .expertise-wrapper col-md-12 -->

<div class="expertise-wrapper col-md-12 section-wrapper gray-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title" >
                    <center><h1>DATA KONTRAK</h1></center>
                </div>
            </div>
        </div>
        <table>
            <thead>
                <tr>
                    <th style="width: 5%;">No</th>
                    <th style="width: 20%;">{{language_data('Contract')}}</th>
                    <th style="width: 20%;">{{language_data('Effective Date')}}</th>
                    <th style="width: 20%;">{{language_data('End Date')}}</th>
                </tr>
            </thead>
            <tbody>
            <?php $ctr = 1;?>
            @foreach($contract as $c)
                <tr>
                    <td >{{$ctr++}}</td>
                    <td >{{$c->letter_number}}</td>
                    @if($c->contract_info)
                    <td >{{get_date_format($c->contract_info->effective_date)}}</td>
                    <td >{{get_date_format($c->contract_info->end_date)}}</td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- .expertise-wrapper col-md-12 -->
</div>
</div>

</body>
</html>