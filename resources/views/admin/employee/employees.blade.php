@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Employees')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Employees')}}</h3>
                            @if($permcheck->C==1)
                            <a href="{{url('employees/add')}}"><button class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add New Employee')}}</button></a>
                            <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload">{{language_data('Upload')}}</button></a>             
                            @endif
                            <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#download">{{language_data('Download Data')}}</button></a>       
                            <a><button class="btn btn-warning btn-sm pull-right" data-toggle="modal" data-target="#id">{{language_data('Generate ID card')}}</button></a>       <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 10%;">No</th>
                                    <th style="width: 10%;">NIK</th>
                                    <th style="width: 10%;">Tanggal Masuk</th>
                                    <th style="width: 10%;">No. Rekening</th>
                                    <th style="width: 15%;">{{language_data('Name')}}</th>
                                    <th style="width: 15%;">{{language_data('Designation')}}</th>
                                    <th style="width: 15%;">{{language_data('Company')}}</th>
                                    <th style="width: 10%;">{{language_data('Location')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 5%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($employees as $d)
                                <?php $ctr++; ?>
                                <tr>
                                    <td data-label="No">{{$ctr}}</td>
                                    <td data-label="NIK">{{$d->employee_code}}</td>
                                    <td data-label="Tanggal Masuk">{{get_date_format($d->doj)}}</td>
                                    <td data-label="No. Rekening">{{$d->bank_info->account_number}}</td>
                                    <td data-label="Name"><p>
                                        @if($employee_permission->R==1) <a href="{{url('employees/view/'.$d->id)}}"> {{$d->fname}} {{$d->lname}}</a></a> @else {{$d->fname}} {{$d->lname}} @endif
                                    </p></td>                    
                                    <td data-label="Designation"><p> @if($d->designation_name) {{$d->designation_name->designation}} @else -@endif</p></td>
                                    <td data-label="Company"><p>  @if($d->company_name) {{$d->company_name->company}} @else -@endif </p></td>      
                                    <td data-label="Location"><p> {{$d->location}} </p></td>                    
                                    @if($d->status=='active')
                                    <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Active')}}@if($d->dol<date('Y-m-d')) : {{language_data('End Date Passed')}} @endif</p></td>
                                    @else
                                    <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Inactive')}}</p></td>
                                    @endif
                                    <td data-label="Actions">
                                    <a class="btn btn-info btn-xs" href="{{url('jobs/download-resume/1/'.$d->id)}}"> Generate CV </a>
                                    <a class="btn btn-success btn-xs" href="{{url('employees/view/'.$d->id)}}" ><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>
                                    </td>
                                </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('employee/importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>

                                *.xls (Excel 2003), Download template <a href="{{ URL::to('employee/downloadExcel/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal id card -->
            <div class="modal fade" id="id" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Generate ID Card')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ url('employees/download-id-card') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Client Contract')}}</label>
                                    <select name="project" class="form-control selectpicker w100p validate-hidden" data-live-search="true">
                                            <option value='0'>{{language_data('Select Client Contract')}}</option>
                                        @foreach($projects as $d)
                                            <option value='{{$d->id}}' >{{$d->project}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Generate')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Download -->
            <div class="modal fade" id="download" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Download')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" id="downloadEmployee" action="{{url('employees/download-employee-data')}}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="row">
                                    <div class="form-group">
                                        <label>{{language_data('Based On')}} :</label><br>
                                        <label class="radio-inline">
                                            <input type="radio" name="based" value="project" class="toggle_specific">{{language_data('Client Contract')}}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="based" value="company" class="toggle_specific">{{language_data('Company')}}
                                        </label>
                                        <div class="specific_dropdown sembunyi">
                                            <label>{{language_data('Company')}}</label>
                                            <select name="company" class="form-control selectpicker w100p validate-hidden" data-live-search="true">
                                                    <option value='0'>{{language_data('Select Company')}}</option>
                                                @foreach($company as $d)
                                                    <option value='{{$d->id}}' >{{$d->company}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="specific_dropdown2 sembunyi">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <select name="project" class="form-control selectpicker w100p validate-hidden" data-live-search="true">
                                                    <option value='0'>{{language_data('Select Client Contract')}}</option>
                                                @foreach($projects as $d)
                                                    <option value='{{$d->id}}' >{{$d->project}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="employee_code">NIK</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="doj">Tanggal Masuk</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="dol">Tanggal Berakhir</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="fname">Nama Karyawan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="gender">Jenis Kelamin</label>
                                        </div>         
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="company">Perusahaan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="designation">Jabatan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="location">Lokasi</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="no_ktp">No. KTP</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="dob">Tanggal Lahir</label>
                                        </div>       
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="birth_place">Tempat Lahir</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="per_address">Alamat (KTP)</label>
                                        </div> 
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="per_village">Kelurahan (KTP)</label>
                                        </div>            
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="per_district">Kecamatan (KTP)</label>
                                        </div>                    
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="per_city">Kota (KTP)</label>
                                        </div>                
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="pre_address">Alamat Domisili</label>
                                        </div>                
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="last_education">Pendidikan Terakhir</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="religion">Agama</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="mother_name">Nama Ibu Kandung</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="marital_status">Status Perkawinan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.tax_status">Status Pajak</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="phone">Nomor Telpon</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="email">Email</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.account_number">No Rekening</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.npwp_number">No NPWP</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.employee_bpjs">BPJS Karyawan</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.health_bpjs">BPJS Kesehatan</label>
                                        </div>  
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.other_insurance_number">No Asuransi Lain</label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="variable[]" class="checks" value="payment_type">Upah Karyawan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.employee_bpjs_cut">Potongan BPJS Ketenagakerjaan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.health_bpjs_cut">Potongan BPJS Ketenagakerjaan</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.other_insurance_cut">Potongan Asuransi Lain</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="banks[]" class="checks" value="banks.pph_cut">Potongan PPH 21</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="checkalll" >Check All</label>
                                        </div>   
                                      
                                    </div>
                                </div>
                                
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="button" onclick="downloadEmployee()" class="btn btn-primary">{{language_data('Download')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){
            $('#checkalll').change(function() {
                if(this.checked) {
                    $('input:checkbox[name="variable[]"]').prop('checked',true);
                $('input:checkbox[name="banks[]"]').prop('checked',true);
                }
                else{
                    $('input:checkbox[name="variable[]"]').prop('checked',false);
                    $('input:checkbox[name="banks[]"]').prop('checked',false);

                }
                    
            });
            $('.data-table').DataTable();

            $(".toggle_specific").click(function () {
                // alert("masuk");
                toggle_specific_dropdown();
            });
            toggle_specific_dropdown();
        
            function toggle_specific_dropdown() {
                var element = $(".toggle_specific:checked");
                console.log(element.val());
                if (element.val() == "company") {
                    $(".specific_dropdown").removeClass("sembunyi").addClass( "block" ).find("select").addClass("validate-hidden");
                    $(".specific_dropdown2").removeClass("block").addClass( "sembunyi" ).find("select").removeClass("validate-hidden");
                } else if(element.val() == "project") {
                    $(".specific_dropdown").removeClass("block").addClass( "sembunyi" ).find("select").removeClass("validate-hidden");
                    $(".specific_dropdown2").removeClass("sembunyi").addClass( "block" ).find("select").addClass("validate-hidden");
                } 
            }

        });
        function downloadEmployee(){
            console.log($('.specific_dropdown').is(':visible'));
            console.log($('.specific_dropdown2').is(':visible'));
            if($('.checks').is(':checked')){
                $("#downloadEmployee").submit();
            } else {
                alert("Silahkan pilih data yang akan di unduh");
                return false;
            }
        } 
    </script>
@endsection
