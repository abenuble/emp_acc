<!DOCTYPE html>
<html>
<head>
<style>
.container {
    padding: 2px 16px;
}
</style>
</head>
<div class="container">

    <table >
        <tr>
            <th > Employee Code </th>
            <th > Name </th>
            <th > Picture </th>
        </tr>
        @foreach($key as $e)
            <tr>
                <td > {{$e['employee_code']}} </td>
                <td > {{$e['fname']}} {{$e['lname']}} </td>
                <td >
                    @if($e['avatar']!='')
                        <img src="<?php echo public_path('assets/employee_pic/'.$e['avatar']); ?>" alt="Profile Page" width="100%">
                    @else
                        <img src="<?php echo public_path('assets/employee_pic/user.png');?>" alt="Profile Page" width="100%">
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
</div>
</html> 
