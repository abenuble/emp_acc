@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    <style>
    td {
        vertical-align: top;
        padding-left: 5px;
        padding-right: 5px;
    }
    </style>
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Add Employee')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Add Employee')}}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="" name="form" role="form" action="{{url('employees/add-employee-post')}}" method="post" enctype="multipart/form-data" onsubmit="return phonenumber(document.form.phone);">                          
                                
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('Personal Details')}}</h3>
                                </div>
                                {{--Personal Details--}}
                                <table width="100%">
                                    <tr>
                                        <td width="33%">
                                            <div class="form-group">
                                                <label>{{language_data('Full Name')}}</label>
                                                <input type="text" class="form-control name"  name="fname" value="">
                                            </div>
                                            {{-- <div class="form-group">
                                                <label>{{language_data('Last Name')}}</label>
                                                <input type="text" class="form-control name"  name="lname" value="">
                                            </div> --}}
                                            <div class="form-group">
                                                <label>{{language_data('Employee Code')}}</label>
                                                <span class="help">e.g. "546814" ({{language_data('Unique For every User')}})</span>
                                                <input type="text" class="form-control"  name="employee_code" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Gender')}}</label>
                                                <select class="selectpicker form-control"  name="gender">
                                                    <option value="male" >{{language_data('male')}}</option>
                                                    <option value="female" >{{language_data('female')}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>No. KTP</label>
                                                <input type="text" class="form-control"  name="no_ktp" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Birth Place')}}</label>
                                                <span class="help">e.g. "Surabaya"</span>
                                                <input type="text" class="form-control"  name="birth_place" value="Surabaya">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Date Of Birth')}}</label>
                                                <input type="text" class="form-control datePicker" name="dob" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data("Mother's Name")}}</label>
                                                <input type="text" class="form-control name"  name="mother_name" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('SKCK Number')}}</label>
                                                <input type="text" class="form-control"  name="skck" value="">
                                            </div>
                                        </td>
                                        <td width="33%">
                                            <div class="form-group">
                                                <label>{{language_data('Permanent Address')}}</label>
                                                <textarea class="form-control" rows="6" placeholder="Alamat"  name="per_address"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"> RT </span>
                                                    <input type="text" class="form-control" name="rt" required value="">
                                                    <span class="input-group-addon"> RW </span>
                                                    <input type="text" class="form-control" name="rw" required value=""> 
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Permanent Village')}}</label>
                                                <input type="text" class="form-control"  name="per_village" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Permanent District')}}</label>
                                                <input type="text" class="form-control"  name="per_district" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Permanent City')}}</label>
                                                <input type="text" class="form-control"  name="per_city" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Present Address')}}</label>
                                                <textarea class="form-control" rows="6" placeholder="Alamat"  name="pre_address"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Present Village')}}</label>
                                                <input type="text" class="form-control"  name="pre_village" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Present District')}}</label>
                                                <input type="text" class="form-control"  name="pre_district" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Present City')}}</label>
                                                <input type="text" class="form-control"  name="pre_city" value="">
                                            </div>
                                        </td>
                                        <td width="33%">
                                            <div class="form-group">
                                                <label>{{language_data('Phone Number')}}</label>
                                                <input type="text" class="form-control"  name="phone" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Email')}}</label>
                                                <input type="email" class="form-control"  name="email" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Religion')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true" name="religion">
                                                    <option value="islam">Islam</option>
                                                    <option value="hindu">Hindu</option>
                                                    <option value="budha">Budha</option>
                                                    <option value="kristen">Kristen</option>
                                                    <option value="katholik">Katholik</option>
                                                    <option value="kong hu cu">Kong Hu Cu</option>
                                                    <option value="other">Other</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Last Education')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true" name="last_education">
                                                    @foreach($last_education as $l)
                                                        <option value="{{$l->id}}">{{$l->education}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('University')}}</label>
                                                <input type="text" class="form-control"  name="university" value="">
                                            </div>
                                            <div class="form-group hidden">
                                                <label>{{language_data('Status')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true"  name="status">
                                                    <option value="active" >{{language_data('Active')}}</option>
                                                    <option value="inactive" >{{language_data('Inactive')}}</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('Dependents')}}</h3>
                                </div>
                                {{--Dependents--}}
                                <table width="100%">
                                    <tr>
                                        <td width="50%">
                                            <div class="form-group">
                                                <label>{{language_data('Marital Status')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true"  required=""  name="marital_status">
                                                    <option value="single">{{language_data('Single')}}</option>
                                                    <option value="married">{{language_data('Married')}}</option>
                                                    <option value="divorced">{{language_data('Divorced')}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group alldesg">
                                                <label>{{language_data('Number of Child Dependents')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true" onload="addFields()" onchange="addFields()"  name="child" id="number">
                                                    <option value="0">0</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">>3</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td width="50%">
                                            <div class="form-group">
                                                <label>{{language_data('Name of Husband/Wife')}}</label>
                                                <input type="text" class="form-control"  value="" name="spouse">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Name of Children')}}</label>
                                            </div>
                                            <div class="form-group" id="depend">
                                            </div>   
                                        </td>
                                    </tr>
                                </table>

                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('Work')}}</h3>
                                </div>
                                {{--Work--}}
                                <table width="100%">
                                    <tr>
                                        <td width="50%">
                                            <div class="form-group">
                                                <label>{{language_data('Employee Type')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true"  required=""  @if( $comp_id == '' ) disabled @endif  name="employee_type" id="employee_type">
                                                    <option value="0">{{language_data('Select Type')}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="el3">{{language_data('Company')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true"  required="" name="company" id="company_id">
                                                    <option value="0">{{language_data('Select Company')}}</option>
                                                    @foreach($company as $p)
                                                        <option value="{{$p->id}}"> {{$p->company}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Designation')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true" required=""  @if( $dep_id == '' ) disabled @endif  name="designation" id="designation">
                                                    <option value="0">{{language_data('Select Designation')}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Date Of Join')}}</label>
                                                <input type="text" class="form-control datePicker" require="" required=""  name="doj">
                                            </div>
                                            <div class="form-group" id="career">
                                            </div>
                                        </td>
                                        <td width="50%">
                                            <div class="form-group">
                                                <label>{{language_data('Client Contract')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true" required=""  @if( $comp_id == '' ) disabled @endif name="project" id="project_id">
                                                    <option value="0">{{language_data('Select Client Contract')}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Employee Status')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true"  required=""  name="employee_status">
                                                    {{-- <option value="permanent">{{language_data('Permanent Employee')}}</option> --}}
                                                    <option value="contract">{{language_data('Contract Employee')}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Division')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true" required=""  @if( $proj_id == '' ) disabled @endif  name="department" id="department_id">
                                                    <option value="0">{{language_data('Select Division')}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Location')}}</label>
                                                <input type="text" class="form-control" require="" required=""  name="location">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('Finance and Insurance')}}</h3>
                                </div>
                                {{--Finance and Insurance--}}
                                <table width="100%">
                                    <tr>
                                        <td width="33%">
                                            <div class="form-group">
                                                <label>{{language_data('Bank')}}</label>
                                                <input type="text" class="form-control"  required=""  name="bank" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('Tax Status')}}</label>
                                                <input type="text" class="form-control"  required=""  name="tax_status" value="">
                                            </div>
                                        </td>
                                        <td width="33%">
                                            <div class="form-group">
                                                <label>{{language_data('Account Number')}}</label>
                                                <input type="number" class="form-control"  required=""  name="account_number" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>{{language_data('NPWP Status')}}</label>
                                                <input type="text" class="form-control"  required=""  name="npwp_status" value="">
                                            </div>
                                        </td>
                                        <td width="33%">
                                            <div class="form-group">
                                                <label>{{language_data('Account Name')}}</label>
                                                <input type="text" class="form-control"  required=""  name="account_name" value="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%" colspan="2">
                                            <div class="form-group">
                                                <label>{{language_data('NPWP Number')}}</label>
                                                <input type="text" class="form-control"  required=""  name="npwp_number" value="">
                                            </div>
                                        </td>
                                        <td width="50%">
                                            <div class="form-group">
                                                <label>{{language_data('Income Tax Discount')}}</label>
                                                <input type="text" class="form-control"  required=""  name="pph_cut" value="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%" colspan="2">
                                            <div class="form-group">
                                                <label>{{language_data('Employment BPJS Number')}}</label>
                                                <input type="text" class="form-control"  required=""  name="employment_bpjs_number" value="">
                                            </div>
                                        </td>
                                        <td width="50%">
                                            <div class="form-group">
                                                <label>{{language_data('Employment BPJS Discount')}}</label>
                                                <input type="text" class="form-control"  required=""  name="employment_bpjs_cut" value="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%" colspan="2">
                                            <div class="form-group">
                                                <label>{{language_data('Health BPJS Number')}}</label>
                                                <input type="text" class="form-control"  required=""  name="health_bpjs_number" value="">
                                            </div>
                                        </td>
                                        <td width="50%">
                                            <div class="form-group">
                                                <label>{{language_data('Health BPJS Discount')}}</label>
                                                <input type="text" class="form-control"  required=""  name="health_bpjs_cut" value="">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="33%">
                                            <div class="form-group">
                                                <label>{{language_data('Other Insurance Name')}}</label>
                                                <input type="text" class="form-control"  name="other_insurance_name" value="">
                                            </div>
                                        </td>
                                        <td width="33%">
                                            <div class="form-group">
                                                <label>{{language_data('Other Insurance Number')}}</label>
                                                <input type="text" class="form-control"  name="other_insurance_number" value="">
                                            </div>
                                        </td>
                                        <td width="33%">
                                            <div class="form-group col-sm-6">
                                                <label>{{language_data('Other Insurance Discount')}}</label>
                                                <input type="text" class="form-control"  name="other_insurance_cut" value="">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label>{{language_data('Effective Date')}}</label>
                                                <input type="text" class="form-control datePicker"  name="effective_date">
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                                <div class="panel-heading">
                                    <h3 class="panel-title">{{language_data('Documents')}}</h3>
                                </div>
                                {{--Documents--}}
                                <div class="row">
                                    <div class="col-md-6">
                                        
                                        <div class="form-group">
                                            <label>{{language_data('Photo')}}</label>
                                            <div class="input-group">
                                                <input id="photo" name="photo" class="form-control" type="file">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Diploma')}}</label>
                                            <div class="input-group">
                                                <input id="diploma" name="diploma" class="form-control" type="file">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>{{language_data('Curricullum Vitae')}}</label>
                                            <div class="input-group">
                                                <input id="cv" name="cv" class="form-control" type="file">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Identity Card')}}</label>
                                            <div class="input-group">
                                                <input id="idcard" name="idcard" class="form-control" type="file">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="schedule" value="{{app_config('DefaultSchedule')}}">
                                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Save')}}</button><br><br>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

<script type='text/javascript'>

        $(document).ready(function () {
            $('#datetimepicker1').datetimepicker({
                viewMode: 'years',
                format: 'YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                viewMode: 'years',
                format: 'YYYY'
            });
        
            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For Project Loading*/
            $("#company_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'comp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee/get-project',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#project_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For type Loading*/
            $("#company_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'comp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee/get-employee-type',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#employee_type").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For career Loading*/
            $("#designation").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'des_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee/get-career',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#career").html( data);
                    }
                });
            });

            /*For department Loading*/
            $("#project_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee/get-department',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#department_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

    });

    function phonenumber(inputtxt)
        {
            var phoneno = /^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;
            var phoneno2 = /^\(?([0-9]{4})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;  
            var phoneno3 = /^\(?([0-9]{4})\)?[-]?([0-9]{4})[-]?([0-9]{4})$/;  
            var phoneno4 = /^\(?([0-9]{4})\)?[-]?([0-9]{4})[-]?([0-9]{3})$/;
            if(inputtxt.value.match(phoneno)){
                return true;
            } else if(inputtxt.value.match(phoneno2)){
                return true;
            } else if(inputtxt.value.match(phoneno3)){
                return true;
            } else if(inputtxt.value.match(phoneno4)){
                return true;
            } else{
                alert("Not a valid Phone Number");
                return false;
            }
        }

    function addFields()
    {
        // Number of inputs to create
        var number = document.getElementById("number").value;
        // Container <div> where dynamic content will be placed
        var container = document.getElementById("depend");
        // Clear previous contents of the container
        while (container.hasChildNodes()) {
            container.removeChild(container.lastChild);
        }
        for (i=0;i<number;i++){
            // Append a node with a random text
            container.appendChild(document.createTextNode("Child " + (i+1) + " "));
            // Create an <input> element, set its type and name attributes
            var input = document.createElement("input");
            input.type = "text";
            input.className = "form-control";
            input.value = "anak";
            input.name = "name_child[]";
            container.appendChild(input);
            // Append a line break 
            container.appendChild(document.createElement("br"));
        }
        if (number==3){
            var add = document.createElement("BUTTON");
            add.setAttribute("type", "button");
            add.setAttribute("class", "btn btn-success btn-sm pull-right");
            add.setAttribute("onClick", "addInput('depend')");
            var text = document.createTextNode("Add");
            add.appendChild(text);
            container.appendChild(add);
        } else{

        }
    }
    
    var numberChild = 3;
    var counter = 1;
    var limit = 5;
    function addInput(divName)
    {
        if (counter == limit)  {
            alert("You have reached the limit of adding " + counter + " inputs");
        }
        else {
            var newdiv = document.createElement('div');
            newdiv.innerHTML = "{{language_data('Child')}} " + (numberChild + 1) + " <br><input type='text' class='form-control' value='' name='name_child[]'><br>";
            document.getElementById(divName).appendChild(newdiv);
            numberChild++;
        }
    }
</script>
    
@endsection
