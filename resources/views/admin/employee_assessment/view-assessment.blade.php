@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Lihat Penilaian</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('employee-assessments/update-post')}}" method="post">
                            <input type="hidden" name="emp_asses_id" value="{{$assessments->id}}"/>
                                <div class="panel-heading">
                                    <h3 class="panel-title"> Lihat Penilaian</h3>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                            <input type="text"  class="form-control datePicker" required value="{{get_date_format($assessments->date)}}" readonly name="date">
                                        </div>
                                    </div>                                     
                                </div> 
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Proyek</label>
                                            <input type="text"  class="form-control" required value="{{$assessments->project_info->project}}" readonly name="project">

                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Perusahaan</label>
                                            <input type="text"  class="form-control" required value="{{$assessments->company_info->company}}" readonly name="company">

                                           
                                        </div>
                                    </div>                                        
                                </div> 
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NIK</label>
                                            <input type="text"  class="form-control" required value="{{$assessments->employee_info->employee_code}}" readonly name="employee">
                                            <input type="hidden"  class="form-control" required value="{{$assessments->employee_info->id}}" readonly name="emp_id">

                                         
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Nama Karyawan</label>
                                            <input type="text"  class="form-control" readonly id="employee_name" value="{{$assessments->employee_info->fname}} {{$assessments->employee_info->fname}}" name="employee_name">
                                        </div>
                                    </div>                                        
                                </div> 
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Posisi/Jabatan</label>
                                            <input type="text"  class="form-control" id="designation" readonly value="{{$assessments->employee_info->designation_name->designation}}" name="designation">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Lokasi</label>
                                            <input type="text" id="location" readonly class="form-control" value="{{$assessments->employee_info->location}}" name="location">
                                        </div>
                                    </div>                                        
                                </div> 
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Periode Awal</label>
                                            <input type="text"  class="form-control monthPicker" readonly required value="{{get_date_month($assessments->period_from)}}" name="period_from">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Periode Akhir</label>
                                            <input type="text"  class="form-control monthPicker" readonly required value="{{get_date_month($assessments->period_to)}}" name="period_to">
                                        </div>
                                    </div>                                        
                                </div> 
                                <hr>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Aspek Penilaian</h3>
                                            </div>
                                            <div class="panel-body p-none">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10%;">Dimensi</th>
                                                        <th style="width: 40%;">Aspek</th>
                                                        <th style="width: 5%;">Bobot</th>
                                                        <th style="width: 5%;">Nilai</th>
                                                        <th style="width: 5%;">Nilai Terbobot</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($assessment_aspect as $aspect)
                                                        <tr>
                                                            <td data-label="Dimension">{{$aspect->aspect_info->dimension_info->dimension}}</td>
                                                            <td data-label="Aspect">
                                                                <div class="form-group">
                                                                    <input type="text" readonly class="form-control" value="{{$aspect->aspect_info->aspect}}" name="aspect[]">  
                                                                    <input type="hidden" readonly class="form-control" value="{{$aspect->id}}" name="employeeaspect[]">  

                                                                </div>
                                                            </td>
                                                                    
                                                            <td data-label="Weight">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" readonly value="{{$aspect->weight}} %" name="weight[]">
                                                                </div>
                                                            </td>

                                                            <td data-label="Grade">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control qty" name="grade[]" required value="{{$aspect->grade}}" data-cubics="{{$aspect->weight}}" >
                                                                </div>
                                                            </td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control cubics" readonly value="{{$aspect->weighted_grade}}" name="weighted_grade[]">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    @endforeach
                                                        <tr>
                                                            <td data-label="Dimension"></td>
                                                            <td data-label="Aspect"><center><label>SUB TOTAL</label></center></td>
                                                            <td data-label="Weight"></td>
                                                            <td data-label="Grade"></td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control totalcubics" readonly>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td data-label="Dimension"><center><label>PELANGGARAN</label></center></td>
                                                            <td data-label="Aspect">
                                                            <select class="selectpicker form-control" data-live-search="true"  name="violation" id="violation_id">
                                                            <option value="0">Pilih Pelanggaran</option>
                                                                    @foreach($violation as $p)
                                                                        @if($assessment_violation)
                                                                        <option value="{{$p->id}}" @if($assessment_violation->violation==$p->violation) selected @endif> {{$p->violation}}</option>
                                                                        @else
                                                                        <option value="{{$p->id}}"> {{$p->violation}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
            
                                                            </td>
                                                            <td data-label="Weight"></td>
                                                            <td data-label="Grade"></td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group" >
                                                                @if($assessment_violation)
                                                                    <input type="text" class="form-control violation" readonly name="violation_grade" value="{{$assessment_violation->weighted_grade}}" id="violation">
                                                                    @else
                                                                    <input type="text" class="form-control violation" readonly name="violation_grade"  id="violation">

                                                                    @endif   
                                                                                                                                    </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td data-label="Dimension"></td>
                                                            <td data-label="Aspect"><center><label>TOTAL</label></center></td>
                                                            <td data-label="Weight"></td>
                                                            <td data-label="Grade"></td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control total" readonly value="{{$assessments->grade}}" name="total">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                        <label>Informasi Nilai</label>
                                            <input type="text" id="information" class="form-control" name="information" readonly value="{{$assessments->information}}" >
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                        <label>Catatan Tambahan</label>
                                            <textarea type="text" rows="5" class="form-control" name="additional_note"  >{{$assessments->additional_note}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                    @if($assessments->status=='pending')
                                        @if($permcheck->U==1)
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" class="btn btn-success btn-xs pull-right"><i class="fa fa-save"></i> {{language_data('Save')}} </button>
                                        @endif
                                    @else
                                    <a href="{{url('employee-assessments/downloadPdf/'.$assessments->id)}}" class="btn btn-info btn-xs pull-right"><i class="fa fa-file-pdf-o"></i> {{language_data('Generate PDF')}}</a><br>    

                                    @endif
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>

                
        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            total();
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For total Loading*/
            $("#violation_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'vio_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-violation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#violation").val( data);
                        total2();
                    }
                });
            });

            /*For Project Loading*/
            $("#company_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'comp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-project',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#project_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For employee Loading*/
            $("#project_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-employee-code',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#emp_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For employee name Loading*/
            $("#emp_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-presence-grade',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#presence_grade").val( data);
                    }
                });
            });

            /*For employee name Loading*/
            $("#emp_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-employee-name',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#employee_name").val( data);
                    }
                });
            });

            /*For designation Loading*/
            $("#emp_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").val( data);
                    }
                });
            });

            /*For location Loading*/
            $("#emp_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-location',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#location").val( data);
                    }
                });
            });


            function total() {
                var total1 = 0;
                $('input.cubics').each(function () {
                    var n = parseFloat($(this).val());
                    total1 += isNaN(n) ? 0 : n;
                });
                $('.totalcubics').val(total1.toFixed(2));
            }
            function total2() {
                var total2 = 0;
                $('input.cubics').each(function () {
                    var n = parseFloat($(this).val());
                    var violation = parseFloat($('input.violation').val()) || 0;
                    total2 += isNaN(n) ? 0 : n;
                    total3 = total2 - violation;
                    if(total3<0){
                        total3 = 0;
                    }
                });
                $('.total').val(total3.toFixed(2));

                var nilai = $('.total').val();
                console.log(nilai);
                if (nilai <= 20){
                    $('#information').val('Kurang Sekali');
                } else if (21 <= nilai && nilai <= 40) {
                    $('#information').val('Kurang');
                } else if (41 <= nilai && nilai <= 60) {
                    $('#information').val('Cukup');
                } else if (61 <= nilai && nilai <= 80) {
                    $('#information').val('Baik');
                } else if (81 <= nilai && nilai <= 100) {
                    $('#information').val('Baik Sekali');
                }
            }

            $('input.qty').keyup(function () {
                var val = parseFloat($(this).val());
                val = (val ? val /100 * $(this).data('cubics') : '');
                $(this).closest('td').next().find('input.cubics').val(val.toFixed(2));
                total();
                total2();
            });

        });


    </script>
@endsection
