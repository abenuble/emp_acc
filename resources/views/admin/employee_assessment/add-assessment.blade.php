@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Add Assessment</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('employee-assessments/add-post')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> Add Assessment</h3>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker" value="" name="date">
                                        </div>
                                    </div>                                     
                                </div> 
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Period From</label>
                                            <input type="text" class="form-control monthPicker" value="" name="period_from" id="period_from">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Period To</label>
                                            <input type="text" class="form-control monthPicker" value="" name="period_to" id="period_to">
                                        </div>
                                    </div>                                        
                                </div> 
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $comp_id == '' ) disabled @endif name="project" id="project_id">
                                                <option value="0">{{language_data('Select Client Contract')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>{{language_data('Company')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="company" id="company_id">
                                                <option value="0">{{language_data('Select Company')}}</option>
                                                @foreach($company as $p)
                                                    <option value="{{$p->id}}"> {{$p->company}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>                                        
                                </div> 
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{{language_data('Employee Code')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $proj_id == '' ) disabled @endif name="emp_id" id="emp_id">
                                                <option value="0">{{language_data('Select Employee Code')}}</option>
                                            </select>
                                        </div>
                                    </div>                              
                                </div> 
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>{{language_data('Designation')}}</label>
                                            <input type="text" class="form-control" readonly value="" name="designation" id="designation">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>{{language_data('Location')}}</label>
                                            <input type="text" class="form-control" readonly value="" name="location" id="location">
                                        </div>
                                    </div>                                        
                                </div> 
                                <hr>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Assessment Aspects</h3>
                                            </div>
                                            <div class="panel-body p-none">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10%;">Dimension</th>
                                                        <th style="width: 40%;">Aspect</th>
                                                        <th style="width: 5%;">Weight</th>
                                                        <th style="width: 5%;">Grade</th>
                                                        <th style="width: 5%;">Weighted Grade</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($assessment_aspect as $aspect)
                                                        <tr>
                                                            <td data-label="Dimension">{{$aspect->dimension_info->dimension}}</td>
                                                            <td data-label="Aspect">
                                                                <div class="form-group">
                                                                    <input type="text" readonly class="form-control" value="{{$aspect->aspect}}">  
                                                                    <input type="hidden" readonly class="form-control" value="{{$aspect->id}}" name="aspect[]">  
                                                                </div>
                                                            </td>
                                                                    
                                                            <td data-label="Weight">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" readonly value="{{$aspect->weight}} %" name="weight[]">
                                                                </div>
                                                            </td>

                                                            <td data-label="Grade">
                                                                <div class="form-group">
                                                                @if($aspect->dimension_info->dimension=='Presensi')
                                                                    <input type="text" class="form-control qty" name="grade[]" value="" id="presence_grade" data-cubics="{{$aspect->weight}}" >
                                                                @else
                                                                    <input type="text" class="form-control qty" name="grade[]" value="" data-cubics="{{$aspect->weight}}" >
                                                                @endif
                                                                </div>
                                                            </td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control cubics" readonly name="weighted_grade[]">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    @endforeach
                                                        <tr>
                                                            <td data-label="Dimension"></td>
                                                            <td data-label="Aspect"><center><label>SUB TOTAL</label></center></td>
                                                            <td data-label="Weight"></td>
                                                            <td data-label="Grade"></td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control totalcubics" readonly>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td data-label="Dimension"><center><label>VIOLATION</label></center></td>
                                                            <td data-label="Aspect">
                                                                <select class="selectpicker form-control" data-live-search="true" name="violation" id="violation_id">
                                                                    <option value="0">Select violation</option>
                                                                    @foreach($violation as $p)
                                                                        <option value="{{$p->id}}"> {{$p->violation}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td data-label="Weight"></td>
                                                            <td data-label="Grade"></td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group" >
                                                                    <input type="text" class="form-control violation" readonly name="violation_grade" value="0" id="violation">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td data-label="Dimension"></td>
                                                            <td data-label="Aspect"><center><label>TOTAL</label></center></td>
                                                            <td data-label="Weight"></td>
                                                            <td data-label="Grade"></td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control total" readonly name="total">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                        <label>Grade Information</label>
                                            <input type="text" class="form-control" id="information" readonly name="information">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                        <label>Additional Note</label>
                                            <textarea type="text" rows="5" class="form-control" name="additional_note" require=""></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-success btn-xs pull-right"><i class="fa fa-save"></i> {{language_data('Save')}} </button>
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>

                
        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For total Loading*/
            $("#violation_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'vio_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-violation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#violation").val( data);
                        total2();
                    }
                });
            });

            /*For Project Loading*/
            $("#company_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'comp_id=' + id;
                var data = '';
                var data2 = '';
                if(id!='0'){
                    $.ajax
                    ({
                        type: "POST",
                        url: _url + '/employee-assessments/get-project',
                        data: dataString,
                        cache: false,
                        success: function ( data ) {
                            $("#project_id").html( data).removeAttr('disabled').selectpicker('refresh');
                            $("#employee_name").val( data2);
                            $("#emp_id").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#designation").val( data2);
                            $("#location").val( data2);
                        }
                    });
                } else {
                    $("#project_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#employee_name").val( data);
                    $("#emp_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#designation").val( data);
                    $("#location").val( data);
                }
            });

            /*For employee Loading*/
            $("#project_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + id;
                var data = '';
                var data2 = '';
                if(id!='0'){
                    $.ajax
                    ({
                        type: "POST",
                        url: _url + '/employee-assessments/get-employee-code',
                        data: dataString,
                        cache: false,
                        success: function ( data ) {
                            $("#employee_name").val( data2);
                            $("#emp_id").html( data).removeAttr('disabled').selectpicker('refresh');
                            $("#designation").val( data2);
                            $("#location").val( data2);
                        }
                    });
                } else {
                    $("#employee_name").val( data);
                    $("#emp_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#designation").val( data);
                    $("#location").val( data);
                }
            });

            /*For employee name Loading*/
            $("#emp_id").change(function () {
                var id = $(this).val();
                var month_from = $('#period_from').val();
                var month_to = $('#period_to').val();
                var _url = $("#_url").val();
                var dataobject = {emp_id:id, month_from:month_from, month_to:month_to};
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-presence-grade',
                    data: dataobject,
                    cache: false,
                    success: function ( data ) {
                        $("#presence_grade").val( data);
                        var val = parseFloat(data);
                        val = (val ? val /100 * data : '');
                        $("#presence_grade").closest('td').next().find('input.cubics').val(val.toFixed(2));
                        total();
                        total2();
                    }
                });
            });

            /*For employee name Loading*/
            $("#emp_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-employee-name',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#employee_name").val( data);
                    }
                });
            });

            /*For designation Loading*/
            $("#emp_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").val( data);
                    }
                });
            });

            /*For location Loading*/
            $("#emp_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee-assessments/get-location',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#location").val( data);
                    }
                });
            });


            function total() {
                var total1 = 0;
                $('input.cubics').each(function () {
                    var n = parseFloat($(this).val());
                    total1 += isNaN(n) ? 0 : n;
                });
                $('.totalcubics').val(total1.toFixed(2));
            }
            function total2() {
                var total2 = 0;
                $('input.cubics').each(function () {
                    var n = parseFloat($(this).val());
                    var violation = parseFloat($('input.violation').val()) || 0;
                    total2 += isNaN(n) ? 0 : n;
                    total3 = total2 - violation;
                    if(total3<0){
                        total3 = 0;
                    }
                });
                $('.total').val(total3.toFixed(2));

                var nilai = $('.total').val();
                console.log(nilai);
                if (nilai <= 20){
                    $('#information').val('Kurang Sekali');
                } else if (21 <= nilai && nilai <= 40) {
                    $('#information').val('Kurang');
                } else if (41 <= nilai && nilai <= 60) {
                    $('#information').val('Cukup');
                } else if (61 <= nilai && nilai <= 80) {
                    $('#information').val('Baik');
                } else if (81 <= nilai && nilai <= 100) {
                    $('#information').val('Baik Sekali');
                }
            }

            $('input.qty').keyup(function () {
                var val = parseFloat($(this).val());
                val = (val ? val /100 * $(this).data('cubics') : '');
                $(this).closest('td').next().find('input.cubics').val(val);
                total();
                total2();
            });

        });


    </script>
@endsection
