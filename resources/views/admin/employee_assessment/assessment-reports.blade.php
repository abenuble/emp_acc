@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Assessment Forms')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Assessment Forms')}}</h3>
                            @if($permcheck->C==1)
                            <a class="btn btn-success btn-sm pull-right" href="{{url('employee-assessments/add')}}"><i class="fa fa-plus"></i> {{language_data('Add New Assessment')}}</a>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 15%;">{{language_data('Client Contract')}}</th>
                                    <th style="width: 15%;">{{language_data('Company')}}</th>
                                    <th style="width: 15%;">{{language_data('Employee')}}</th>
                                    <th style="width: 10%;">Grade</th>
                                    <th style="width: 10%;">Average</th>
                                    <th style="width: 10%;">Difference</th>
                                    
                                    <th style="width: 15%;">{{language_data('Information')}}</th>
                                    <th style="width: 5%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reports as $f)
                                    <tr>
                                        <td data-label="No">{{$f->id}}</td>
                                        <td data-label="Project"><p>{{$f->project_info->project}}</p></td>
                                        <td data-label="Company"><p>{{$f->company_info->company}}</p></td>
                                        <td data-label="Employee"><p>{{$f->employee_info->fname}} {{$f->employee_info->lname}}</p></td>
                                        <td data-label="Grade"><p>{{$f->grade}}</p></td>
                                        <td data-label="Average"><p>{{$f->average}}</p></td>
                                        <td data-label="Difference"><p>{{$f->difference}}%</p></td>
                                    
                                        <td data-label="Information"><p>{{$f->information}}</p></td>
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-success btn-xs" href="{{url('employee-assessments/assessments/'.$f->emp_id)}}"><i class="fa fa-edit"></i> {{language_data('View')}}</a>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });

    </script>
@endsection
