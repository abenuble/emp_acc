@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Form Penilaian</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Proyek</label>
                                        <input type="text" readonly class="form-control" value="{{$reports->project_info->project}}">
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label>Perusahaan</label>
                                        <input type="text" readonly class="form-control" value="{{$reports->company_info->company}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>NIK</label>
                                        <input type="text" readonly class="form-control" value="{{$reports->employee_info->employee_code}}">
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label>Nama Karyawan</label>
                                        <input type="text" readonly class="form-control" value="{{$reports->employee_info->fname}} {{$reports->employee_info->lname}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Form Penilaian</h3>
                            @if($permcheck->C==1)
                            <a class="btn btn-success btn-sm pull-right" href="{{url('employee-assessments/add')}}"><i class="fa fa-plus"></i> Tambah Penilaian</a>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 15%;">Tanggal</th>
                                    <th style="width: 15%;">Periode Awal</th>
                                    <th style="width: 15%;">Periode Akhir</th>
                                    <th style="width: 10%;">Nilai</th>
                                    <th style="width: 15%;">Informasi</th>
                                    <th style="width: 15%;">Status</th>
                                        <th style="width: 20%;">Catatan Tambahan</th>
                                    <th style="width: 5%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($assessments as $f)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="no" >{{$ctr}}</td>
                                        <td data-label="Date"><span style="display:none;">{{$f->date}}</span><p>{{get_date_format($f->date)}}</p></td>
                                        <td data-label="Period From"><p>{{get_date_month($f->period_from)}}</p></td>
                                        <td data-label="Period To"><p>{{get_date_month($f->period_to)}}</p></td>
                                        <td data-label="Grade"><p>{{$f->grade}}</p></td>
                                        <td data-label="Information"><p>{{$f->information}}</p></td>

                                           @if($f->status=='approved')
                                    <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Approved')}}</p></td>
                                    @elseif($f->status=='pending')
                                    <td data-label="Status"><p class="btn btn-warning btn-xs">{{language_data('Pending')}}</p></td>
                                    @else
                                    <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Rejected')}}</p></td>
                                    @endif
                                      
                                        <td data-label="Additional Note"><p>{{$f->additional_note}}</p></td>
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-success btn-xs" href="{{url('employee-assessments/assessment-aspects/'.$f->id)}}"><i class="fa fa-edit"></i> {{language_data('View')}}</a>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

        });

    </script>
@endsection
