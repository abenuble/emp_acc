<!DOCTYPE html>

<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - assessment Form</title>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        .help-split {
            display: inline-block;
            width: 30%;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-30 p-t-none p-b-none">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-body">
                        <form class="" role="form" action="{{url('assessments/edit-form')}}" method="post">
                            <div class="panel-heading">
                                <h1  style="text-align:center"> Employee Assesment</h1>
                            </div>

                             <div class="row">

<div class="col-lg-12">
    <div class="panel">
        <div class="panel-body">
            <form class="" role="form" action="{{url('employee-assessments/update-post')}}" method="post">
                <div class="panel-heading">
                </div>

                <div class="row">
                    <div class="col-xs-5">
                        <div class="form-group">
                            <strong>Date : </strong> {{get_date_format($assessments->date)}}
                        </div>
                    </div> 
                    <div class="col-xs-5">
                        <div class="form-group">
                            <strong>Project : </strong>{{$assessments->project_info->project}}

                        </div>
                    </div>                                    
                </div> 
                <div class="row">
                   
                    <div class="col-xs-5">
                        <div class="form-group">
                            <strong>Company : </strong>{{$assessments->company_info->company}}

                           
                        </div>
                    </div>  
                    <div class="col-xs-5">
                        <div class="form-group">
                            <strong>Employee Code  : </strong>{{$assessments->employee_info->employee_code}}

                         
                        </div>
                    </div>                                      
                </div> 
                <div class="row">
                  
                    <div class="col-xs-5">
                        <div class="form-group">
                            <strong>Employee Name : </strong>{{$assessments->employee_info->fname}} {{$assessments->employee_info->fname}}
                        </div>
                    </div>   
                    <div class="col-xs-5">
                        <div class="form-group">
                            <strong>Designation : </strong>{{$assessments->employee_info->designation_name->designation}}
                        </div>
                    </div>                                     
                </div> 
                <div class="row">
                 
                    <div class="col-xs-5">
                        <div class="form-group">
                            <strong>Location : </strong>{{$assessments->employee_info->location}}
                        </div>
                    </div>  
                                                      
                </div> 
                <div class="row">
                <div class="col-xs-5">
                        <div class="form-group">
                            <strong>Period From : </strong>{{$assessments->period_from}}
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <div class="form-group">
                            <strong>Period To : </strong>{{$assessments->period_to}}
                        </div>
                    </div>                                        
                </div> 
                             
                            <div class="row">

                                    <div class="col-lg-12">
                                        <div class="panel">
                                        
                                            <div class="panel-body p-none">
                                                <table class="table">
                                                    
                                                    <tr>
                                                        <td ><strong>Dimension</strong></th>
                                                        <td ><strong>Aspect</strong></th>
                                                        <td ><strong>Weight</strong></th>
                                                        <td ><strong>Grade</strong></th>
                                                        <td ><strong>Weighted Grade</strong></th>
                                                    </tr>
                                                   
                                                    <tbody>
                                                    <?php $tot=0; ?>
                                                    @foreach($assessment_aspect as $aspect)
                                                        
                                                        <tr>
                                                            <td data-label="Dimension">{{$aspect->aspect_info->dimension_info->dimension}}</td>
                                                            <td data-label="Aspect">
                                                                <div class="form-group">
                                                                {{$aspect->aspect_info->aspect}}

                                                                </div>
                                                            </td>
                                                                    
                                                            <td data-label="Weight">
                                                                <div class="form-group">
                                                                    {{$aspect->weight}} %            
                                                                                                                        </div>
                                                            </td>

                                                            <td data-label="Grade">
                                                                <div class="form-group">
                                                                {{$aspect->grade}}
                                                                </div>
                                                            </td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group">
                                                                {{$aspect->weighted_grade}}
                                                                </div>
                                                            </td>
                                                        </tr>
                                                            <?php $tot+=$aspect->weighted_grade; ?>
                                                    @endforeach
                                                        <tr>
                                                            <td data-label="Dimension"></td>
                                                            <td data-label="Aspect"><center><label>SUB TOTAL</label></center></td>
                                                            <td data-label="Weight"></td>
                                                            <td data-label="Grade"></td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group">
                                                                {{$tot}}
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td data-label="Dimension"><center><label>VIOLATION</label></center></td>
                                                         
                                                            <td data-label="Aspect">
                                                           @if($assessment_violation) {{$assessment_violation->violation}} @endif
                                                            
            
                                                            </td>
                                                            <td data-label="Weight"></td>
                                                            <td data-label="Grade"></td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group" >
                                                                @if($assessment_violation){{$assessment_violation->weighted_grade}} @endif
                                                                                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td data-label="Dimension"></td>
                                                            <td data-label="Aspect"><center><label>TOTAL</label></center></td>
                                                            <td data-label="Weight"></td>
                                                            <td data-label="Grade"></td>
                                                            <td data-label="Weighted Grade">
                                                                <div class="form-group">
                                                                {{$assessments->grade}}
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                    
                                    </div>

                            </div>
                            
                            <div class="row">
                <div class="col-xs-4">
                        <div class="form-group">
                            <strong>Informasi Nilai : </strong>{{$assessments->information}}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <strong>Catatan Tambahan : </strong>{{$assessments->additional_note}}
                        </div>
                    </div>                                        
                </div> 
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>


</body>
</html>