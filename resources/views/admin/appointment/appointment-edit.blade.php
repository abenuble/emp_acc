@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Appointment')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> View Appointment
                                @if($appointments->status=='draft')
                                    <a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a>
                                @elseif($appointments->status=='accepted')
                                    <a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a>
                                @else
                                    <a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a>
                                @endif
                                </h3>
                            </div>
                            
                            <form class="form-some-up form-block" role="form" action="{{url('appointments/update')}}" method="post">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Appointment Letter Number')}}</label>
                                            <input type="text" class="form-control" readonly required="" value="{{$appointments->letter_number}}" name="letter_number">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker" readonly value="{{get_date_format($appointments->date)}}" required="" name="date">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Letter Draft')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$appointments->draft_letter_info->tplname}}" required="" name="draft_letter">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$appointments->project_info->project_number}}" required="" name="project_number" id="project_id">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Company')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$appointments->company_info->company}}" required="" name="company_name">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-none" >
                                            <label for="e20">{{language_data('Employee Code')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$appointments->employee_code}}" required="" name="employee_code">
                                        </div>
                                    
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Employee Name')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$appointments->employee_info->fname}} {{$appointments->employee_info->lname}}" required="" name="employee_name">
                                        </div>                                    
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-none" >
                                            <label for="e20">{{language_data('Department')}}/{{language_data('Designation')}}<label>
                                            <input type="text" class="form-control" readonly value="{{$appointments->designation_old_info->department_name->department}} ({{$appointments->designation_old_info->designation}})" required="" name="designation_old">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Location')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$appointments->location_old}}" required="" name="location_old">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Employee Status')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$appointments->employee_status_old}}" required="" name="employee_status_old">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row"><br>
                                <h3 class="panel-title">{{language_data('Appointment')}}</h3>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Appointment Date')}}</label>
                                            <input type="text" class="form-control datePicker" required="" value="{{get_date_format($appointments->appointment_date)}}" name="appointment_date">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Department')}}/{{language_data('Designation')}}<label>
                                            <select name="designation_new" class="form-control selectpicker designation_new"  data-live-search="true" id="designation_new">
                                                <option value="0">{{language_data('Select Department')}}/{{language_data('Designation')}}<option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Location')}}</label>
                                            <select name="location_new" class="form-control selectpicker location_new" data-live-search="true" id="location_new">
                                                <option value="0">{{language_data('Select Location')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Employee Status')}}</label>
                                            <select name="employee_status_new" class="form-control selectpicker employee_status_new">
                                                <option value="0">{{language_data('Select Employee Status')}}</option>
                                                <option value="contract" @if($appointments->employee_status_new=='contract') selected @endif >{{language_data('Contract')}}</option>
                                                <option value="permanent"  @if($appointments->employee_status_new=='permanent') selected @endif >{{language_data('Permanent')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>          
                                <br>

                                
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$appointments->id}}">
                                <button type="submit" class="btn btn-sm btn-primary pull-right">{{language_data('Update')}}</button>
                            </form>
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
           
         

            $('.data-table').DataTable();

            /*For Location new Loading*/
            $("#project_id").ready(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + {{$appointments->project_number}};
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/appointments/get-location-new',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#location_new").html( data).selectpicker('refresh');
                        $('#location_new').val("<?php echo$appointments->location_new; ?>").trigger("change");
           
                    }
                });
            });

            /*For designation_new Loading*/
            $("#employee_id").ready(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + {{$appointments->employee_name}};
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/appointments/get-designation-new',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation_new").html( data).selectpicker('refresh');
                        $('#designation_new').val("<?php echo$appointments->designation_new; ?>").trigger("change");

                    }
                });
            });


        });

    </script>
@endsection
