@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Bank')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-10">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('banks/update')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> {{language_data('View Bank')}}</h3>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="company">
                                        @foreach($company as $c)
                                            <option value="{{$c->id}}" @if($banks->company==$c->id) selected @endif >{{$c->company}}</option>
                                         @endforeach
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Bank')}}</label>
                                    <input type="text" class="form-control" value="{{$banks->bank}}" required="" name="bank">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Branch')}}</label>
                                    <input type="text" class="form-control" value="{{$banks->branch}}" required="" name="branch">
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Account Number')}}</label>
                                    <input type="text" class="form-control" value="{{$banks->account_number}}" required="" name="account_number">
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Name')}}</label>
                                    <input type="text" class="form-control" value="{{$banks->name}}" required="" name="name">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Approximate Account Name')}}</label>
                                    <select class="selectpicker form-control" @if($comp_id=='') @endif data-live-search="true" id="coa_id" name="coa_id">
                                        @foreach($coa as $co)
                                            <option value="{{$co->id}}" @if($banks->coa_id==$co->id) selected @endif>{{$co->coa}} ({{$co->coa_code}})</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Status')}} : </label>
                                    <select class="selectpicker form-control" data-live-search="true" name="status">
                                        <option value="active" @if($banks->status=='active') selected @endif >{{language_data('Active')}}</option>
                                        <option value="inactive" @if($banks->status=='inactive') selected @endif >{{language_data('Inactive')}}</option>
                                    </select>
                                </div>

                                @if($permcheck->U==1)
                                <div class="row">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" value="{{$banks->id}}" name="cmd">
                                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update')}} </button>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();


        });

    </script>
@endsection
