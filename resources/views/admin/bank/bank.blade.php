@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Bank')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Bank')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#bank"><i class="fa fa-plus"></i> {{language_data('Add New Bank')}}</button>
                            <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload">{{language_data('Upload')}}</button></a><br>   
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 25%;">{{language_data('Company')}}</th>
                                    <th style="width: 20%;">{{language_data('Bank')}}</th>
                                    <th style="width: 10%;">{{language_data('Branch')}}</th>
                                    <th style="width: 15%;">{{language_data('Account Number')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 15%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($bank as $b)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Company"><p>{{$b->company_info->company}}</p></td>
                                        <td data-label="Bank"><p>{{$b->bank}}</p></td>
                                        <td data-label="Branch"><p>{{$b->branch}}</p></td>
                                        <td data-label="Number"><p>{{$b->account_number}}</p></td>
                                        @if($b->status=='active')
                                            <td data-label="status"><p class="btn btn-complete btn-xs">{{language_data('Active')}}</p></td>
                                        @else
                                            <td data-label="status"><p class="btn btn-danger btn-xs">{{language_data('Inactive')}}</p></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                            <a class="btn btn-complete btn-xs" href="banks/view/{{$b->id}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal add-->
            <div class="modal fade" id="bank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Bank')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('banks/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="company" id="company_id">
                                            <option value="0">{{language_data('Select Company')}}</option>
                                        @foreach($company as $c)
                                            <option value="{{$c->id}}">{{$c->company}}</option>
                                         @endforeach
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Bank')}}</label>
                                    <input type="text" class="form-control" required="" name="bank">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Branch')}}</label>
                                    <input type="text" class="form-control" required="" name="branch">
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Account Number')}}</label>
                                    <input type="text" class="form-control" required="" name="account_number">
                                </div>
                                
                                <div class="form-group">
                                    <label>{{language_data('Name')}}</label>
                                    <input type="text" class="form-control" required="" name="name">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Approximate Account Name')}}</label>
                                    <select class="selectpicker form-control" @if($comp_id=='') disabled @endif data-live-search="true" id="coa_id" name="coa_id">
                                        <option value="0">{{language_data('Select COA Code')}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('banks/importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file" accept="application/vnd.ms-excel"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
 
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('banks/downloadExcel/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/banks/delete/" + id;
                    }
                });
            });

            /*For Coa Loading*/
            $("#company_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'comp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/banks/get-coa',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#coa_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });


        });
    </script>
@endsection
