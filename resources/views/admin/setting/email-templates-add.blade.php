@extends('master')


{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Add Email Template')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-7">
                    <div class="panel">
                        <div class="panel-body">
                            <form method="POST" action="{{ url('settings/email-templates-add-post') }}">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> {{language_data('Add Email Template')}}</h3>
                                </div>


                                <div class="form-group">
                                    <label for="tplname">{{language_data('Template Name')}}</label>
                                    <input type="text" class="form-control" id="name" name="name" value="">
                                </div>

                                <div class="form-group">
                                    <label for="subject">{{language_data('Subject')}}</label>
                                    <input type="text" class="form-control" id="subject" name="subject" value="">
                                </div>
                                
                                <div class="form-group">
                                    <label for="table_type">{{language_data('Letter Type')}}</label>
                                    <select name="table_type" id="table_types" class="selectpicker form-control">
                                        <option value="" >{{language_data('Select Letter Type')}}</option>
                                        <option value="sys_warning" >{{language_data('Warning Letter')}}</option>
                                        {{-- <option value="sys_appointments" >{{language_data('Appointment Letter')}}</option> --}}
                                        <option value="sys_resign" >{{language_data('Resign Letter')}}</option>
                                        {{-- <option value="sys_phk" >{{language_data('Termination Letter')}}</option> --}}
                                        <option value="sys_end_contracts" >{{language_data('End Contract Letter')}}</option>
                                        <option value="sys_contracts" >{{language_data('Contract Letter')}}</option>
                                        <option value="sys_mutations" >Surat Perubahan Status</option>
                                        <option value="sys_surat_pengantar" >Surat Pengantar</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="Status">{{language_data('Status')}}</label>
                                    <select name="status" class="selectpicker form-control">
                                        <option value="1" >{{language_data('Active')}}</option>
                                        <option value="0" >{{language_data('Inactive')}}</option>
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="message">{{language_data('Message')}}</label>
                                    <textarea id="textarea-wysihtml5" class="textarea-wysihtml5 form-control" name="message"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="available_variables">{{language_data('Available Variables')}} :</label>
                                    <div id="variables"></div>
                                    
                                </div>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" name="update" class="btn btn-success"><i class="fa fa-edit"></i> {{language_data('Update')}}</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    <script>
    $(document).ready(function () {
        /*For variable Loading*/
        $("#table_types").change(function () {
            var id = $(this).val();
            var _url = $("#_url").val();
            var dataString = 'table_type=' + id;
            $.ajax
            ({
                type: "POST",
                url: _url + '/settings/get-variable',
                data: dataString,
                cache: false,
                success: function ( data ) {
                    $("#variables").html( data);
                }
            });
        });

        
    });
    </script>
@endsection
