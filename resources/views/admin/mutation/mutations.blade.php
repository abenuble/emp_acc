@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Mutation')}}</h2> 
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Mutation')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#mutation"><i class="fa fa-plus"></i> {{language_data('Add New Mutation')}}</button>
                            @endif
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 10%;">{{language_data('Client Contract Number')}}</th>
                                    <th style="width: 10%;">{{language_data('Company')}}</th>
                                    <th style="width: 15%;">{{language_data('Employee')}}</th>
                                    <th style="width: 5%;">{{language_data('Area Code')}}</th>
                                    <th style="width: 20%;">{{language_data('Department')}}/{{language_data('Designation')}}</th>
                                    <th style="width: 20%;">{{language_data('Employee')}} {{language_data('Status')}}</th>
                                    <th style="width: 5%;">{{language_data('Status')}}</th>
                                    <th style="width: 10%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($mutations as $a)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No">{{$ctr}}</td>
                                        <td data-label="Project Number"><p>{{$a->project_info->project_number}}</p></td>
                                        <td data-label="Company"><p>{{$a->company_info->company}}</p></td>
                                        <td data-label="Employee"><p>{{$a->employee_info->fname}} {{$a->employee_info->lname}}</p></td>
                                        <td data-label="Area Code"><p>{{$a->company_info->company_code}} > {{$a->company_info_new->company_code}}</p></td>
                                        <td data-label="Department/Designation"><p>{{$a->designation_old_info->designation}} > {{$a->designation_new_info->designation}}</p></td>
                                        <td data-label="Employee Status"><p>{{$a->employee_status_old}} > {{$a->employee_status_new}}</p></td>
                                        @if($a->status=='draft')
                                            <td data-label="status"><a class="btn btn-warning btn-xs">{{language_data('Drafted')}}</a></td>
                                        @elseif($a->status=='accepted')
                                            <td data-label="status"><a class="btn btn-success btn-xs">{{language_data('Accepted')}}</a></td>
                                        @else
                                            <td data-label="status"><a class="btn btn-danger btn-xs">{{language_data('Rejected')}}</a></td>
                                        @endif
                                        <td data-label="Actions" class="">
                                        @if($a->status=='draft')
                                            @if($permcheck->U==1)
                                                <a class="btn btn-success btn-xs" href="{{url('mutations/edit/'.$a->id)}}"><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>
                                            @endif

                                            @if($permcheck->D==1)
                                                <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$a->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                            @endif
                                        @else
                                            <a class="btn btn-complete btn-xs" href="{{url('mutations/view/'.$a->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                     
                                        @endif
                                        </td>

                                        
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade" id="mutation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Mutation')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('mutations/add')}}" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Mutation Letter Number')}}</label>
                                            <input type="text" class="form-control" required readonly name="letter_number" value="{{$number}}/Mts-SSS/{{get_roman_letters($month)}}/{{$year}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}}</label>
                                            <input type="text" class="form-control datePicker" required="" name="date">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Letter Draft')}}</label>
                                            <select name="draft_letter" class="form-control selectpicker">
                                                <option value="0">{{language_data('Select Draft Letter')}}</option>
                                            @foreach($email_template as $em)
                                                <option value="{{$em->id}}">{{$em->tplname}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Mutation Type')}}</label>
                                            <select name="mutation_type" class="form-control selectpicker">
                                                <option value="mutasi">Mutasi</option>
                                                <option value="demosi">Demosi</option>
                                                <option value="promosi">Promosi</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Company')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="company_name" id="company_id">
                                                <option value="0">{{language_data('Select Company')}}</option>
                                                @foreach($company as $p)
                                                    <option value="{{$p->id}}"> {{$p->company}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Client Contract')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $comp_id == '' ) disabled @endif name="project_number" id="project_id">
                                                <option value="0">{{language_data('Select Client Contract')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Area Code')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $comp_id == '' ) disabled @endif name="company_code_old" id="company_code_old">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-none" >
                                            <label for="e20">{{language_data('Department')}}/{{language_data('Designation')}}</label>
                                            <select name="designation_old" class="form-control selectpicker" data-live-search="true" @if( $emp_id == '' ) disabled @endif id="designation_old">
                                                <option value="0">{{language_data('Select Department')}}/{{language_data('Designation')}}<option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none" >
                                            <label for="e20">{{language_data('Employee Code')}}</label>
                                            <select name="employee_code" class="form-control selectpicker" data-live-search="true" @if( $emp_id == '' ) disabled @endif id="employee_code">
                                                <option value="0">{{language_data('Select Employee Code')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Employee Name')}}</label>
                                            <select name="employee_name" class="form-control selectpicker" data-live-search="true" @if( $comp_id == '' ) disabled @endif id="employee_id">
                                                <option value="0">{{language_data('Select Employee')}}</option>
                                            </select>
                                        </div>
                                    
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-none alldesg" >
                                            <label for="e20">{{language_data('Payment Type')}}</label>
                                            <select name="payment_type_old" class="form-control selectpicker" data-live-search="true" @if( $emp_id == '' ) disabled @endif id="payment_type_old">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Location')}}</label>
                                            <select name="location_old" class="form-control selectpicker" data-live-search="true" @if( $emp_id == '' ) disabled @endif id="location_old">
                                                <option value="0">{{language_data('Select Location')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Employee Status')}}</label>
                                            <select name="employee_status_old" class="form-control selectpicker" data-live-search="true" @if( $emp_id == '' ) disabled @endif id="employee_status_old">
                                                <option value="0">{{language_data('Select Employee Status')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="form-group">
                                        <label for="message">{{language_data('Copies')}}</label>
                                        <input type="text" class="form-control"    name="tembusan"/>
                                    </div>
                                <div class="row"><br>
                                <h3 class="panel-title">{{language_data('Mutation')}}</h3>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Mutation Date')}}</label>
                                            <input type="text" class="form-control datePicker" required="" name="mutation_date">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group m-none" >
                                            <label for="e20">{{language_data('Payment Type')}}</label>
                                            <select name="payment_type_new" class="form-control selectpicker" data-live-search="true" @if( $emp_id == '' ) disabled @endif id="payment_type_new">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Area Code')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" @if( $comp_id == '' ) disabled @endif name="company_code_new" id="company_code_new">
                                            </select>
                                        </div>
                                    </div>
                                </div>      
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Department')}}/{{language_data('Designation')}}</label>
                                            <select name="designation_new" class="form-control selectpicker" data-live-search="true" @if( $emp_id == '' ) disabled @endif id="designation_new">
                                                <option value="0">{{language_data('Select Department')}}/{{language_data('Designation')}}<option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Location')}}</label>
                                            <select name="location_new" class="form-control selectpicker" data-live-search="true" @if( $proj_id == '' ) disabled @endif id="location_new">
                                                <option value="0">{{language_data('Select Location')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-none">
                                            <label for="e20">{{language_data('Employee Status')}}</label>
                                            <select name="employee_status_new" class="form-control selectpicker">
                                                <option value="PKWT">PKWT</option>
                                                <option value="PKWTT">PKWTT</option>
                                                <option value="TETAP">TETAP</option>
                                                <option value="HARIAN">HARIAN</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>                                

                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
           

            /*For Project Loading*/
            $("#company_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'comp_id=' + id;
                var data = '';
                var data2 = '';
                if(id!='0'){
                    $.ajax
                    ({
                        type: "POST",
                        url: _url + '/mutations/get-project',
                        data: dataString,
                        cache: false,
                        success: function ( data ) {
                            $("#project_id").html( data).removeAttr('disabled').selectpicker('refresh');
                            $("#employee_id").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#employee_code").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#location_old").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#location_new").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#designation_old").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#designation_new").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#employee_status_old").html( data2).removeAttr('disabled').selectpicker('refresh');
                            var id = $("#project_id").val();
                            var _url = $("#_url").val();
                            var dataString2 = 'proj_id=' + id;
                            var data1 = '';
                            var data12 = '';
                            if(id!='0'){
                                $.ajax
                                ({
                                    type: "POST",
                                    url: _url + '/mutations/get-employee',
                                    data: dataString2,
                                    cache: false,
                                    success: function ( data1 ) {
                                        $("#employee_id").html( data1).removeAttr('disabled').selectpicker('refresh');
                                        $("#employee_code").html( data12).removeAttr('disabled').selectpicker('refresh');
                                        $("#location_old").html( data12).removeAttr('disabled').selectpicker('refresh');
                                        $("#designation_old").html( data12).removeAttr('disabled').selectpicker('refresh');
                                        $("#designation_new").html( data12).removeAttr('disabled').selectpicker('refresh');
                                        $("#employee_status_old").html( data12).removeAttr('disabled').selectpicker('refresh');
                                    }
                                });
                            } else {
                                $("#employee_id").html( data1).removeAttr('disabled').selectpicker('refresh');
                                $("#employee_code").html( data1).removeAttr('disabled').selectpicker('refresh');
                                $("#location_old").html( data1).removeAttr('disabled').selectpicker('refresh');
                                $("#designation_old").html( data1).removeAttr('disabled').selectpicker('refresh');
                                $("#designation_new").html( data1).removeAttr('disabled').selectpicker('refresh');
                                $("#employee_status_old").html( data1).removeAttr('disabled').selectpicker('refresh');
                            }
                            $.ajax
                            ({
                                type: "POST",
                                url: _url + '/mutations/get-location-new',
                                data: dataString2,
                                cache: false,
                                success: function ( data ) {
                                    $("#location_new").html( data).removeAttr('disabled').selectpicker('refresh');
                                }
                            });
                        }
                    });
                    $.ajax
                    ({
                        type: "POST",
                        url: _url + '/mutations/get-company-code',
                        data: dataString,
                        cache: false,
                        success: function ( data ) {
                            $("#company_code_old").html( data).removeAttr('disabled').selectpicker('refresh');
                        }
                    });
                } else {
                    $("#company_code_old").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#company_code_new").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#project_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#employee_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#employee_code").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#location_old").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#location_new").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#designation_old").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#designation_new").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#employee_status_old").html( data).removeAttr('disabled').selectpicker('refresh');
                }
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/mutations/get-company-code-new',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#company_code_new").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            /*For Employee Loading*/
            $("#project_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'proj_id=' + id;
                var data = '';
                var data2 = '';
                if(id!='0'){
                    $.ajax
                    ({
                        type: "POST",
                        url: _url + '/mutations/get-employee',
                        data: dataString,
                        cache: false,
                        success: function ( data ) {
                            $("#employee_id").html( data).removeAttr('disabled').selectpicker('refresh');
                            $("#employee_code").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#location_old").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#designation_old").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#designation_new").html( data2).removeAttr('disabled').selectpicker('refresh');
                            $("#employee_status_old").html( data2).removeAttr('disabled').selectpicker('refresh');
                        }
                    });
                } else {
                    $("#employee_id").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#employee_code").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#location_old").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#designation_old").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#designation_new").html( data).removeAttr('disabled').selectpicker('refresh');
                    $("#employee_status_old").html( data).removeAttr('disabled').selectpicker('refresh');
                }
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/mutations/get-location-new',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#location_new").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });

            // /*For Location new Loading*/
            // $("#project_id").change(function () {
            //     var id = $(this).val();
            //     var _url = $("#_url").val();
            //     var dataString = 'proj_id=' + id;
            //     $.ajax
            //     ({
            //         type: "POST",
            //         url: _url + '/mutations/get-location-new',
            //         data: dataString,
            //         cache: false,
            //         success: function ( data ) {
            //             $("#location_new").html( data).removeAttr('disabled').selectpicker('refresh');
            //         }
            //     });
            // });

            /*For designation_old Loading*/
            $("#employee_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'emp_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/mutations/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation_old").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/mutations/get-designation-new',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation_new").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/mutations/get-location',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#location_old").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/mutations/get-payment-type',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#payment_type_old").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/mutations/get-payment-type-new',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#payment_type_new").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/mutations/get-employee-code',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#employee_code").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/mutations/get-employee-status',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#employee_status_old").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });
// {{-- 
//             /*For designation_new Loading*/
//             $("#employee_id").change(function () {
//                 var id = $(this).val();
//                 var _url = $("#_url").val();
//                 var dataString = 'emp_id=' + id;
//                 $.ajax
//                 ({
//                     type: "POST",
//                     url: _url + '/mutations/get-designation-new',
//                     data: dataString,
//                     cache: false,
//                     success: function ( data ) {
//                         $("#designation_new").html( data).removeAttr('disabled').selectpicker('refresh');
//                     }
//                 });
//             });

//             /*For location_old Loading*/
//             $("#employee_id").change(function () {
//                 var id = $(this).val();
//                 var _url = $("#_url").val();
//                 var dataString = 'emp_id=' + id;
//                 $.ajax
//                 ({
//                     type: "POST",
//                     url: _url + '/mutations/get-location',
//                     data: dataString,
//                     cache: false,
//                     success: function ( data ) {
//                         $("#location_old").html( data).removeAttr('disabled').selectpicker('refresh');
//                     }
//                 });
//             });

//             /*For employee code Loading*/
//             $("#employee_id").change(function () {
//                 var id = $(this).val();
//                 var _url = $("#_url").val();
//                 var dataString = 'emp_id=' + id;
//                 $.ajax
//                 ({
//                     type: "POST",
//                     url: _url + '/mutations/get-employee-code',
//                     data: dataString,
//                     cache: false,
//                     success: function ( data ) {
//                         $("#employee_code").html( data).removeAttr('disabled').selectpicker('refresh');
//                     }
//                 });
//             });

//             /*For employee status Loading*/
//             $("#employee_id").change(function () {
//                 var id = $(this).val();
//                 var _url = $("#_url").val();
//                 var dataString = 'emp_id=' + id;
//                 $.ajax
//                 ({
//                     type: "POST",
//                     url: _url + '/mutations/get-employee-status',
//                     data: dataString,
//                     cache: false,
//                     success: function ( data ) {
//                         $("#employee_status_old").html( data).removeAttr('disabled').selectpicker('refresh');
//                     }
//                 });
//             }); --}}
            
            
            
        });
        $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/mutations/delete/" + id;
                    }
                });
            });
    </script>
@endsection
