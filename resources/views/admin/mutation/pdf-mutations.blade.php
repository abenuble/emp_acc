<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>{{app_config('AppName')}} - MUTATION</title>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<head>
    <style>
        @page {
            size: 21cm 29.7cm;
            margin-left: 1cm;
            margin-right: 1cm;
            font: 12pt "Tahoma";
        }
        h1 {
            font: 16pt "Tahoma";
        }
        td {
            font: 12pt "Tahoma";
            padding: 3px;
        }
        hr {
            border: none;
            height: 1px;
            /* Set the hr color */
            color: #333; /* old IE */
            background-color: #333; /* Modern Browsers */
        }
        .td {
            text-decoration: underline;
        }
    </style>
</head>
<body class="printable-page">

<main id="wrapper" class="wrapper">
    <div class="container container-printable">
        <div class="p-25 p-t-none p-b-none">
            {!!$message!!}
        </div>
    </div>
</main>

</body>
</html>