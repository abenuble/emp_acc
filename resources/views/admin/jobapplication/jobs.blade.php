@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Job Applications')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Job Applications')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal"
                                    data-target="#add-new-job"><i class="fa fa-plus"></i> {{language_data('Add New Job')}}
                            </button>
                            @endif

                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 55%;">{{language_data('Job Application')}}</th>
                                    <th style="width: 5%;">{{language_data('Status')}}</th>
                                    <th style="width: 35%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($jobs as $d)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="no" >{{$ctr}}</td>
                                        <td data-label="Job Application">
                                            {{$d->position_name->designation}} ({{$d->job_location}})<br>
                                            {{$d->company_name->company}}<br>
                                            @if($d->project!=1)
                                            {{$d->project_name->project}}<br>
                                            @endif
                                            Close Date: {{get_date_format($d->close_date)}}<br>
                                            <a href="{{url('jobs/view-applicant/'.$d->id)}}"><i class="fa fa-list"></i> {{language_data('View')}}</a>
                                            @if($permcheck->U==1)
                                            <a href="{{url('jobs/edit/'.$d->id)}}"><i class="fa fa-edit"></i> {{language_data('Edit')}}</a>
                                            @endif
                                            @if($permcheck->D==1)
                                            <a href="#" class="cdelete" id="{{$d->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                            @endif
                                        </td>
                                        @if($d->status=='drafted')
                                            <td data-label="Status"><p class="btn btn-warning btn-xs">{{language_data('Drafted')}}</p></td>
                                        @elseif($d->status=='opening')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Open')}}</p></td>
                                        @elseif($d->status=='closed')
                                            <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Closed')}}</p></td>
                                        @endif
                                        <td data-label="Actions">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h3> {{$d->belum}} </h3><br>
                                                    <span> {{language_data('Unread')}} </span>
                                                </div>
                                                {{-- <div class="col-md-4">
                                                    <h3> {{$d->psiko}} </h3><br>
                                                    <span> {{language_data('Psychotest')}} </span>
                                                </div> --}}
                                                <div class="col-md-4">
                                                    <h3> {{$d->wawan}} </h3><br>
                                                    <span> {{language_data('Interview')}} </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h3> {{$d->diter}} </h3><br>
                                                    <span> {{language_data('Accepted')}} </span>
                                                </div>
                                                <div class="col-md-4">
                                                    <h3> {{$d->waiting}} </h3><br>
                                                    <span> Waiting List </span>
                                                </div>
                                                <div class="col-md-4">
                                                    <h3> {{$d->tolak}} </h3><br>
                                                    <span> Rejected </span>
                                                </div> 
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade" id="add-new-job" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Job')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" id="form" action="{{url('jobs/post-new-job')}}">

                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Job Number')}}</label>
                                    <input type="text" class="form-control" required="" name="job_number" value='{{$number}}/lowongan/{{get_roman_letters($month)}}/{{$year}}'>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Client Contract Name')}}</label>
                                    <select name="project" id="e20" class="form-control selectpicker"
                                            data-live-search="true">
                                        @foreach($projects as $pro)
                                            @if($pro->id==1)
                                            <option value="{{$pro->id}}">{{language_data('Select Client Contract')}}</option>
                                            @else
                                            <option value="{{$pro->id}}">{{$pro->project}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Position')}}</label>
                                    <select name="position" id="e20" class="form-control selectpicker"
                                            data-live-search="true">
                                        @foreach($designation as $des)
                                            <option value="{{$des->id}}">{{$des->designation}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Quota')}}</label>
                                    <span class="help">e.g. "2"</span>
                                    <input type="number" min="0" class="form-control" required="" name="quota">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Job Location')}}</label>
                                    <input type="text" class="form-control" required="" name="job_location">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Job Type')}}</label>
                                    <select name="job_type" class="form-control selectpicker">
                                        <option value="Contractual">{{language_data('Contractual')}}</option>
                                        <option value="Part Time">{{language_data('Part Time')}}</option>
                                        <option value="Full Time">{{language_data('Full Time')}}</option>
                                    </select>

                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Experience')}}</label>
                                            <input type="number" min="0" class="form-control" name="experience">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Last Education')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="last_education">
                                                @foreach($last_education as $l)
                                                    <option value="{{$l->id}}">{{$l->education}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>        
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Apply Date')}}</label>
                                            <input type="text" class="form-control" name="apply_date" required="" id="start_date">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Close Date')}}</label>
                                            <input type="text" class="form-control" name="close_date" id="end_date">
                                        </div>
                                    </div>    
                                </div>    
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Salary Range')}}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Salary From')}}</label>
                                            <input type="text" id="salary_from" class="form-control" name="salary_from" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Salary To')}}</label>
                                            <input type="text" id="salary_to"  class="form-control" name="salary_to" value="">
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="form-group m-none">
                                    <label for="e20">{{language_data('Status')}}</label>
                                    <select name="status" class="form-control selectpicker">
                                        <option value="opening">{{language_data('Open')}}</option>
                                        <option value="closed">{{language_data('Closed')}}</option>
                                        <option value="drafted">{{language_data('Drafted')}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Short Description')}}</label>
                                    <textarea class="form-control" rows="5" name="short_description"></textarea>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

             <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('companies/importExcel') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file" accept="application/vnd.ms-excel"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
 
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('jobs/downloadExcelPelamar/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

    <script>
        $(document).ready(function () {
            

            $('#start_date,#end_date').datetimepicker({
                locale: 'id',
                useCurrent: false,
                format: 'DD MMMM YYYY',
                minDate: moment()
            });
            $('#start_date').datetimepicker().on('dp.change', function (e) {
                var incrementDay = moment(new Date(e.date));
                incrementDay.add(1, 'days');
                $('#end_date').data('DateTimePicker').minDate(incrementDay);
                $(this).data("DateTimePicker").hide();
            });

            $('#end_date').datetimepicker().on('dp.change', function (e) {
                var decrementDay = moment(new Date(e.date));
                decrementDay.subtract(1, 'days');
                $('#start_date').data('DateTimePicker').maxDate(decrementDay);
                    $(this).data("DateTimePicker").hide();
            });
            // Set up the number formatting.
            $('#salary_from').number( true, 0, '.', ',' );
            // Set up the number formatting.
            $('#salary_to').number( true, 0, '.', ',' );
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Job Info*/
           


        });
        $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/jobs/delete-job/" + id;
                    }
                });
            });
    </script>
@endsection
