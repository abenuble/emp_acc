@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Edit Job')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-lg-7">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="{{url('jobs/job-edit-post')}}" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> {{language_data('Edit Job')}}</h3>
                                </div>


                                <div class="form-group">
                                    <label>{{language_data('Job Number')}}</label>
                                    <input type="text" class="form-control" readonly name="job_number" value="{{$job->no_position}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Client Contract Name')}}</label>
                                    <input type="text" class="form-control" readonly name="project" value="{{$job->project_name->project}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Position')}}</label>
                                    <input type="text" class="form-control" readonly name="position" value="{{$job->position_name->designation}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Quota')}}</label>
                                    <input type="number" class="form-control" name="quota" value="{{$job->quota}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Job Location')}}</label>
                                    <input type="text" class="form-control" readonly name="job_location" value="{{$job->job_location}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Job Type')}}</label>
                                    <select name="job_type" class="form-control selectpicker">
                                        <option value="Contractual">{{language_data('Contractual')}}</option>
                                        <option value="Part Time">{{language_data('Part Time')}}</option>
                                        <option value="Full Time">{{language_data('Full Time')}}</option>
                                    </select>

                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Experience')}}</label>
                                            <input type="number" min="0" class="form-control" name="experience" value="{{$job->experience}}">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Last Education')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="last_education">
                                                @foreach($last_education as $l)
                                                    <option value="{{$l->id}}" @if($job->last_education==$l->id) selected @endif>{{$l->education}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>        
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Apply Date')}}</label>
                                            <input type="text" class="form-control" name="apply_date" required="" id="start_date" value="{{date('m/d/Y g:i A', strtotime($job->post_date))}}">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Close Date')}}</label>
                                            <input type="text" class="form-control" name="close_date" id="end_date" value="{{date('m/d/Y g:i A', strtotime($job->close_date))}}">
                                        </div>
                                    </div>    
                                </div>               
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Salary Range')}}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Salary From')}}</label>
                                            <input type="text" class="form-control" name="salary_from" id="salary_from" value="{{$job->salary_from}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Salary To')}}</label>
                                            <input type="text" class="form-control" name="salary_to" id="salary_to" value="{{$job->salary_to}}">
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="form-group m-none">
                                    <label for="e20">{{language_data('Status')}}</label>
                                    <select name="status" class="form-control selectpicker">
                                        <option value="opening">{{language_data('Open')}}</option>
                                        <option value="closed">{{language_data('Closed')}}</option>
                                        <option value="drafted">{{language_data('Drafted')}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Short Description')}}</label>
                                    <textarea class="form-control" rows="5" name="short_description">{{$job->short_description}}</textarea>
                                </div>




                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" value="{{$job->id}}" name="cmd">
                                <button type="submit" class="btn btn-danger btn-sm pull-left" href="/jobs">{{language_data('Cancel')}}</button>
                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-edit"></i> {{language_data('Update')}} </button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}
    <script>
        $(document).ready(function () {

            $('#start_date').datetimepicker().on('dp.change', function (e) {
                var incrementDay = moment(new Date(e.date));
                incrementDay.add(1, 'days');
                $('#end_date').data('DateTimePicker').minDate(incrementDay);
                $(this).data("DateTimePicker").hide();
            });

            $('#end_date').datetimepicker().on('dp.change', function (e) {
                var decrementDay = moment(new Date(e.date));
                decrementDay.subtract(1, 'days');
                $('#start_date').data('DateTimePicker').maxDate(decrementDay);
                    $(this).data("DateTimePicker").hide();
            });
            // Set up the number formatting.
            $('#salary_from').number( true, 0, '.', ',' );
            // Set up the number formatting.
            $('#salary_to').number( true, 0, '.', ',' );
        });   
    </script>
@endsection
