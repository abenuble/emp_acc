@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('View Applicant')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
                <div class="col-lg-10">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-heading">
                                <h3 class="panel-title"> {{language_data('View Applicant')}}</h3>
                            </div>
                            <form class="form-some-up form-block" role="form" action="{{url('jobs/set-applicant-status')}}" method="post" enctype="multipart/form-data">
                                @if($applicants->status=='Unread')
                                <div class="form-group">
                                    <label>{{language_data('Name')}}</label>
                                    <input type="text" class="form-control" required="" value="{{$applicants->name}}" name="name">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Email')}}</label>
                                    <input type="text" class="form-control" required="" value="{{$applicants->email}}" name="email">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Phone Number')}}</label>
                                    <input type="text" class="form-control" required="" value="{{$applicants->phone}}" name="phone">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Address')}}</label>
                                    <textarea class="form-control" rows="5" name="address">{{$applicants->address}}</textarea>
                                </div>
                                @else
                                <div class="form-group">
                                    <label>{{language_data('Name')}}</label>
                                    <input type="text" class="form-control" required="" readonly value="{{$applicants->name}}" name="name">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Email')}}</label>
                                    <input type="text" class="form-control" required="" readonly value="{{$applicants->email}}" name="email">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Phone Number')}}</label>
                                    <input type="text" class="form-control" required="" readonly value="{{$applicants->phone}}" name="phone">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Address')}}</label>
                                    <textarea class="form-control" rows="5" readonly name="address">{{$applicants->address}}</textarea>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Experience')}}</label>
                                            <input type="number" class="form-control" readonly value="{{$applicants->experience}}" name="experience">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Last Education')}}</label>
                                            <input type="text" class="form-control" readonly value="{{$applicants->last_education_info->education}}" name="last_education">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Photo')}}</label>
                                            @if($applicants->photo=='' || $applicants->photo==null)
                                            <div class="input-group">
                                                <input id="file" name="photo" class="form-control" type="file" accept="image/*">
                                            </div>
                                            @else
                                            <br>
                                            <span><a href="{{url('jobs/download-photo/'.$applicants->id)}}"> {{$applicants->photo}} </a></span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Curricullum Vitae')}}</label>
                                            @if($applicants->resume=='' || $applicants->resume==null)
                                            <div class="input-group">
                                                <input id="file" name="resume" class="form-control" type="file" accept="image/*">
                                            </div>
                                            @else
                                            <br>
                                            <span><a href="{{url('jobs/download-resume/2/'.$applicants->id)}}"> {{$applicants->resume}} </a></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if($jobview->quota>$job_applicants_confirm)
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>{{language_data('Status')}}</label>
                                                <select class="selectpicker form-control" data-live-search="true" name="status">
                                                    <option value="Unread" @if($applicants->status=='Unread') selected @endif >{{language_data('Unread')}}</option>
                                                    <option value="Interview" @if($applicants->status=='Interview') selected @endif >{{language_data('Interview')}}</option>
                                                    <option value="Waiting List" @if($applicants->status=='Waiting List') selected @endif >{{language_data('Waiting List')}}</option>
                                                    <option value="Confirm" @if($applicants->status=='Confirm') selected @endif >{{language_data('Confirm')}}</option>
                                                    <option value="Rejected" @if($applicants->status=='Rejected') selected @endif >{{language_data('Rejected')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                
                                {{-- <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Psychotest Status')}}</label>
                                            <select name="status_psychotest" disabled class="form-control selectpicker">
                                                <option value="scheduled"@if($applicants->sub_status_psychotest=='scheduling') selected @endif>Scheduling</option>
                                                <option value="scheduled"@if($applicants->sub_status_psychotest=='scheduled') selected @endif>Scheduled</option>
                                                <option value="pass"@if($applicants->sub_status_psychotest=='pass') selected @endif>Pass</option>
                                                <option value="not pass"@if($applicants->sub_status_psychotest=='not pass') selected @endif>Not Pass</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>{{language_data('Schedule')}}</label>
                                            <input type="text" class="form-control datePicker" readonly value="{{get_date_format($applicants->psychotest_schedule)}}" name="psychotest_schedule" required="">
                                        </div>
                                    </div>

                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <label>{{language_data('IQ Grade')}}</label>
                                            <input type="text" class="form-control" required readonly value="{{$applicants->psychotest}}" name="psychotest_grade">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('File Psychotest')}}</label>
                                            @if($applicants->psychotest_file=='')
                                            <div class="input-group">
                                                <input id="file" name="psychotest_file" class="form-control" type="file">
                                            </div>
                                            @else
                                            <br>
                                            <span><a href="{{url('jobs/download-psychotest/'.$applicants->id)}}">{{$applicants->psychotest_file}}</a></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Information')}}</label>
                                            <textarea class="form-control" rows="5" readonly name="information_psychotest">{{$applicants->psychotest_information}}</textarea>
                                        </div>
                                    </div>
                                </div>   --}}

                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Interview Status')}}</label>
                                            <select name="status_interview" disabled class="form-control selectpicker">
                                                <option value="scheduling"@if($applicants->sub_status_interview=='scheduling') selected @endif>{{language_data('Scheduling')}}</option>
                                                <option value="scheduled"@if($applicants->sub_status_interview=='scheduled') selected @endif>{{language_data('Scheduled')}}</option>
                                                <option value="pass"@if($applicants->sub_status_interview=='pass') selected @endif>{{language_data('Pass')}}</option>
                                                <option value="not pass"@if($applicants->sub_status_interview=='not pass') selected @endif>{{language_data('Not Pass')}}</option>
                                                <option value="accepted"@if($applicants->sub_status_interview=='accepted') selected @endif>{{language_data('Accepted')}}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Schedule')}}</label>
                                            <input type="text" class="form-control datePicker" readonly name="interview_schedule" value="{{get_date_format($applicants->interview_schedule)}}" required="">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('File Interview')}}</label>
                                            @if($applicants->interview_file=='')
                                            <div class="input-group">
                                                <input id="file" name="interview_file" class="form-control" type="file" accept="image/*">
                                            </div>
                                            @else
                                            <br>
                                            <span><a href="{{url('jobs/download-interview/'.$applicants->id)}}">{{$applicants->interview_file}}</a></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Information')}}</label>
                                            <textarea class="form-control" rows="5" readonly name="information_interview">{{$applicants->interview_information}}</textarea>
                                        </div>
                                    </div>
                                </div>  

                                {{-- <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Medical Tests Status')}}</label>
                                            <select name="status_healthtest" disabled class="form-control selectpicker">
                                                <option value="scheduling"@if($applicants->sub_status_healthtest=='scheduling') selected @endif>{{language_data('Scheduling')}}</option>
                                                <option value="scheduled"@if($applicants->sub_status_healthtest=='scheduled') selected @endif>{{language_data('Scheduled')}}</option>
                                                <option value="pass"@if($applicants->sub_status_healthtest=='pass') selected @endif>{{language_data('Pass')}}</option>
                                                <option value="not pass"@if($applicants->sub_status_healthtest=='not pass') selected @endif>{{language_data('Not Pass')}}</option>
                                                <option value="accepted"@if($applicants->sub_status_healthtest=='accepted') selected @endif>{{language_data('Accepted')}}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>{{language_data('Schedule')}}</label>
                                            <input type="text" class="form-control datePicker" readonly name="healthtest_schedule" value="{{get_date_format($applicants->healthtest_schedule)}}" required="">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>File {{language_data('Medical Tests')}}</label>
                                            @if($applicants->healthtest_file=='')
                                            <div class="input-group">
                                                <input id="file" name="healthtest_file" class="form-control" type="file" accept="image/*">
                                            </div>
                                            @else
                                            <br>
                                            <span><a href="{{url('jobs/download-interview/'.$applicants->id)}}">{{$applicants->healthtest_file}}</a></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Information')}}</label>
                                            <textarea class="form-control" rows="5" required readonly name="information_healthtest">{{$applicants->healthtest_information}}</textarea>
                                        </div>
                                    </div>  
                                </div>   --}}

                                @if($permcheck->U==1)
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$applicants->id}}">
                                <input type="hidden" name="job_id" value="{{$applicants->job_id}}">
                                <a href="{{url('jobs/view-applicant/'.$applicants->job_id)}}" type="button" class="btn btn-default pull-right" >{{language_data('Cancel')}}</a>
                                <button type="submit" class="btn btn-primary pull-right">{{language_data('Update')}}</button>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>

        </div>
    </section>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();


        });

    </script>
@endsection
