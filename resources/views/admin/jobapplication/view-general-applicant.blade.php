@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Job Applicants')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">

                        <div class="panel-body">
                        <form class="form-some-up" role="form" action="{{url('jobs/update-general-applicant')}}" method="post" enctype="multipart/form-data">
                            <br>
                        {{--Job Detail--}}
                            <div class="col-lg-12">
                                <div class="form-group a">
                                    <label>{{language_data('Name')}}</label>
                                    <input type="text" class="form-control name" name="name" value="{{$generalapplicantview->name}}">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Email')}}</label>
                                    <span class="help">e.g. "coderpixel@gmail.com" ({{language_data('Unique For every User')}})</span>
                                    <input type="text" class="form-control" name="email" value="{{$generalapplicantview->email}}">
                                </div>

                                <div class="form-group a">
                                    <label>No. KTP</label>
                                    <input type="text" class="form-control" name="ktp" value="{{$generalapplicantview->ktp}}">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent Address')}}</label>
                                    <textarea class="form-control" rows="5" placeholder="Alamat" name="address">{{$generalapplicantview->address}}</textarea>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent Village')}}</label>
                                    <input type="text" class="form-control" name="village" value="{{$generalapplicantview->village}}">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent District')}}</label>
                                    <input type="text" class="form-control" name="district" value="{{$generalapplicantview->district}}">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent City')}}</label>
                                    <input type="text" class="form-control" name="city" value="{{$generalapplicantview->city}}">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present Address')}}</label>
                                    <textarea placeholder="Alamat" class="form-control" rows="5" name="address_domisili">{{$generalapplicantview->address}}</textarea>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present Village')}}</label>
                                    <input type="text" class="form-control" name="village_domisili" value="{{$generalapplicantview->village_domisili}}">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present District')}}</label>
                                    <input type="text" class="form-control" name="district_domisili" value="{{$generalapplicantview->district_domisili}}">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present City')}}</label>
                                    <input type="text" class="form-control" name="city_domisili" value="{{$generalapplicantview->city_domisili}}">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Phone')}}</label>
                                    <span class="help">e.g. "xxx-xxx-xxxx, xxxx-xxx-xxxx, xxxx-xxxx-xxxx, xxxx-xxxx-xxx"</span>
                                    <input type="text" class="form-control" name="phone" value="{{$generalapplicantview->phone}}">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Phone')}}</label>
                                    <input type="text" class="form-control" name="birth_place" value="{{$generalapplicantview->birth_place}}">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Date Of Birth')}}</label>
                                    <input type="text" class="form-control datePicker" value="{{get_date_format($generalapplicantview->birth_date)}}"  name="birth_date">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Gender')}}</label>
                                    <select name="gender" class="form-control selectpicker">
                                        <option value="male" @if($generalapplicantview->gender=='male') selected @endif>Laki-laki</option>
                                        <option value="female" @if($generalapplicantview->gender=='female') selected @endif>Perempuan</option>
                                    </select>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Marital Status')}}</label>
                                    <select name="marital_status" class="form-control selectpicker">
                                        <option value="Lajang" @if($generalapplicantview->marital_status=='Lajang') selected @endif>Lajang</option>
                                        <option value="Menikah (K1)" @if($generalapplicantview->marital_status=='Menikah (K1)') selected @endif>Menikah (K1)</option>
                                        <option value="Menikah (K2)" @if($generalapplicantview->marital_status=='Menikah (K2)') selected @endif>Menikah (K2)</option>
                                        <option value="Menikah (K3)" @if($generalapplicantview->marital_status=='Menikah (K3)') selected @endif>Menikah (K3)</option>
                                        <option value="Menikah (K4)" @if($generalapplicantview->marital_status=='Menikah (K4)') selected @endif>Menikah (K4)</option>
                                        <option value="Duda" @if($generalapplicantview->marital_status=='Duda') selected @endif>Duda</option>
                                        <option value="Janda" @if($generalapplicantview->marital_status=='Janda') selected @endif>Janda</option>
                                    </select>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Religion')}}</label>
                                    <select name="religion" class="form-control selectpicker">
                                        <option value="Islam" @if($generalapplicantview->religion=='Islam') selected @endif>Islam</option>
                                        <option value="Kristen" @if($generalapplicantview->religion=='Kristen') selected @endif>Kristen</option>
                                        <option value="Hindu" @if($generalapplicantview->religion=='Hindu') selected @endif>Hindu</option>
                                        <option value="Budha" @if($generalapplicantview->religion=='Budha') selected @endif>Budha</option>
                                    </select>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data("Biological Mother's Name")}}</label>
                                    <input type="text" class="form-control" value="{{$generalapplicantview->ibu_kandung}}" name="ibu_kandung">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Job title proposed')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="designation">
                                        <option value='0'>Semua Jabatan</option>
                                        @foreach($designation as $a)
                                            <option value="{{$a->id}}" @if($generalapplicantview->designation==$a->id) selected @endif>{{$a->designation}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-lg-6 a">
                                    <div class="form-group">
                                        <label>{{language_data('Experience')}}</label>
                                        <input type="number" min="0" max="15" class="form-control" value="{{$generalapplicantview->experience}}" name="experience">
                                    </div>
                                </div>

                                <div class="col-lg-6 a">
                                    <div class="form-group">
                                        <label>{{language_data('Last Education')}}</label>
                                        <select class="selectpicker form-control" data-live-search="true" name="last_education">
                                            @foreach($last_education as $l)
                                                <option value="{{$l->id}}" @if($generalapplicantview->education==$l->id) selected @endif>{{$l->education}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                    
                                <div class="form-group">
                                    <label>{{language_data('Curricullum Vitae')}}</label><br>
                                    @if($generalapplicantview->resume==null)
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" disabled class="form-control" name="resume" id="file">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly="" name="file_name" value="{{$generalapplicantview->resume}}">
                                    </div>
                                    @else
                                    <a type="button" href="{{url('assets/applicant_doc/'.$generalapplicantview->resume)}}" class="btn btn-default" readonly="" name="file_name">{{$generalapplicantview->resume}}</a>
                                    @endif
                                </div>
                                    
                                <div class="form-group">
                                    <label>{{language_data('Photo')}}</label><br>
                                    @if($generalapplicantview->photo==null)                                    
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" disabled class="form-control" name="photo" id="photo">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly="" name="photo_name" value="{{$generalapplicantview->photo}}">
                                    </div>
                                    @else
                                    <a type="button" href="{{url('assets/applicant_doc/'.$generalapplicantview->photo)}}" class="btn btn-default" readonly="" name="photo_name">{{$generalapplicantview->photo}}</a>
                                    @endif
                                </div>
                            </div>

                            
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="cmd" value="{{$generalapplicantview->id}}">
                            <a href="{{url('jobs/general-applicants')}}" type="button" class="btn btn-default pull-right">{{language_data('Back')}}</a>
                            <button type="submit" class="btn btn-primary pull-right">{{language_data('Save')}}</button>
                        </form>
                        </div>
                    </div>
                        
                </div> 
            </div>

            
            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {{--{!! Html::script("assets/libs/jquery-1.10.2.min.js") !!}--}}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Application Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/jobs/delete-application/" + id;
                    }
                });
            });


        });

    </script>
    
@endsection
