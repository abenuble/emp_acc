@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('General Applicants')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('General Applicants')}}</h3>
                            @if($permcheck->C==1)
                            <button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload">{{language_data('Upload')}}</button>
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal"
                                    data-target="#add-new-applicants"><i class="fa fa-plus"></i> {{language_data('Add New Applicants')}}
                            </button>
                            <br>
                            @endif
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 25%;">{{language_data('Name')}}</th>
                                    <th style="width: 15%;">{{language_data('Email')}}</th>
                                    <th style="width: 10%;">{{language_data('Phone')}}</th>
                                    <th style="width: 10%;">{{language_data('Applying Date')}}</th>
                                    <th style="width: 10%;">{{language_data('Apply For')}}</th>
                                    @if($permcheck->D==1)
                                    <th style="width: 25%;">{{language_data('Actions')}}</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($general_applicants as $ctr => $d)
                                    <?php $ctr++; ?>
                                    <tr>
                                        <td data-label="No" >{{$ctr}}</td>
                                        <td data-label="Name"><a href="{{url('jobs/view-general-applicant/'.$d->id)}}">{{$d->name}}</a></td>
                                        <td data-label="Email"><a href="mailto:{{$d->email}}">{{$d->email}}</a></td>
                                        <td data-label="Phone"><a href="tel:{{$d->phone}}">{{$d->phone}}</a></td>
                                        <td data-label="Applying Date">{{get_date_format($d->created_at)}}</td>
                                        <td data-label="Apply For">@if($d->designation==''||$d->designation==0) Semua Jabatan @else {{$d->designation_info->designation}} @endif</td>
                                        @if($permcheck->D==1)
                                        <td data-label="Actions">
                                            <div class="btn-group btn-mini-group dropdown-default">
                                                <a class="btn btn-success btn-sm dropdown-toggle btn-animated from-top fa fa-caret-down" data-toggle="dropdown" href="#" aria-expanded="false"><span><i class="fa fa-bars"></i></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="{{url('jobs/view-general-applicant/'.$d->id)}}" data-toggle="tooltip" data-placement="right" title="{{language_data('Edit')}}" class="text-success"><i class="fa fa-edit"></i></a></li>
                                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="{{language_data('Delete')}}" id="{{$d->id}}" class="text-success cdelete"><i class="fa fa-trash"></i></a></li>
                                                </ul>
                                            </div>
                                        </td>
                                        @endif
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Modal -->
            <div class="modal fade" id="add-new-applicants" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog" role="document"  >
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Applicants')}}</h4>
                        </div>
                        <form class="form-some-up" name="form" role="form" method="post" action="{{url('jobs/add-general-applicant')}}" enctype="multipart/form-data" onsubmit="return phonenumber(document.form.phone);">
                            <div class="modal-body">

                                <div class="form-group a">
                                    <label>{{language_data('Name')}}</label>
                                    <input type="text" class="form-control name" name="name" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Email')}}</label>
                                    <span class="help">e.g. "coderpixel@gmail.com" ({{language_data('Unique For every User')}})</span>
                                    <input type="text" class="form-control" name="email" value="">
                                </div>

                                <div class="form-group a">
                                    <label>No. KTP</label>
                                    <input type="text" class="form-control"  name="ktp" id="ktp" onchange="checkktp()" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent Address')}}</label>
                                    <textarea placeholder="Alamat" class="form-control" rows="5" name="address" value=""></textarea>
                                </div>
                                <div class="form-group a">
                                    <div class="input-group">
                                        <span class="input-group-addon"> RT </span>
                                        <input type="text" class="form-control" name="rt" required value="">
                                        <span class="input-group-addon"> RW </span>
                                        <input type="text" class="form-control" name="rw" required value=""> 
                                    </div>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent Village')}}</label>
                                    <input type="text" class="form-control"  name="village" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent District')}}</label>
                                    <input type="text" class="form-control"  name="district" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent City')}}</label>
                                    <input type="text" class="form-control"  name="city" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present Address')}}</label>
                                    <textarea placeholder="Alamat" class="form-control" rows="5" name="address_domisili" value=""></textarea>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present Village')}}</label>
                                    <input type="text" class="form-control"  name="village_domisili" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present District')}}</label>
                                    <input type="text" class="form-control"  name="district_domisili" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present City')}}</label>
                                    <input type="text" class="form-control"  name="city_domisili" value="">
                                </div>
                                
                                <div class="form-group a">
                                    <label>{{language_data('Phone')}}</label>
                                    <span class="help">e.g. "xxx-xxx-xxxx, xxxx-xxx-xxxx, xxxx-xxxx-xxxx, xxxx-xxxx-xxx"</span>
                                    <input type="text" class="form-control" name="phone" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Place of Birth')}}</label>
                                    <input type="text" class="form-control"  name="birth_place" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Date Of Birth')}}</label>
                                    <input type="text" class="form-control datePicker" required=""  name="birth_date">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Gender')}}</label>
                                    <select name="gender" class="form-control selectpicker">
                                        <option value="male">{{language_data('male')}}</option>
                                        <option value="female">{{language_data('female')}}</option>
                                    </select>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Marital Status')}}</label>
                                    <select name="marital_status" class="form-control selectpicker">
                                        <option value="Lajang">{{language_data('Single')}}</option>
                                        <option value="Menikah (K1)">{{language_data('Married (K1)')}}</option>
                                        <option value="Menikah (K2)">{{language_data('Married (K2)')}}</option>
                                        <option value="Menikah (K3)">{{language_data('Married (K3)')}}</option>
                                        <option value="Menikah (K4)">{{language_data('Married (K4)')}}</option>
                                        <option value="Duda">{{language_data('Widower')}}</option>
                                        <option value="Janda">{{language_data('Widow')}}</option>
                                    </select>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Religion')}}</label>
                                    <select name="religion" class="form-control selectpicker">
                                        <option value="Islam">{{language_data('Islam')}}</option>
                                        <option value="Kristen">{{language_data('Christian')}}</option>
                                        <option value="Hindu">{{language_data('Hindu')}}</option>
                                        <option value="Budha">{{language_data('Buddha')}}</option>
                                    </select>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data("Biological Mother's Name")}}</label>
                                    <input type="text" class="form-control" required=""  name="ibu_kandung">
                                </div>

                                <div class="form-group a alldesg">
                                    <label>{{language_data('Job title proposed')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="designation">
                                        <option value='0'>{{language_data('All Designation')}}</option>
                                        @foreach($designation as $a)
                                            <option value="{{$a->id}}">{{$a->designation}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-lg-6 a">
                                    <div class="form-group">
                                        <label>{{language_data('Experience')}}</label>
                                        <input type="number" min="0" max="15" class="form-control" name="experience">
                                    </div>
                                </div>

                                <div class="col-lg-6 a">
                                    <div class="form-group">
                                        <label>{{language_data('Last Education')}}</label>
                                        <select class="selectpicker form-control" data-live-search="true" name="last_education">
                                            @foreach($last_education as $l)
                                                <option value="{{$l->id}}">{{$l->education}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                
                                <div class="form-group a">
                                    <label>{{language_data('Curricullum Vitae')}}</label>
                                    <div class="input-group">
                                        <input id="resume" name="resume" class="form-control" type="file" accept="application/pdf">
                                    </div>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Photo')}}</label>
                                    <div class="input-group">
                                        <input id="photo" name="photo" class="form-control" type="file"  accept="image/*">
                                    </div>
                                </div>


                            </div>


                                
                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <!-- Modal Upload -->
            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('jobs/importExcelGeneral') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file" accept="application/vnd.ms-excel"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
                                
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('jobs/downloadExcelGeneral/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/jquery-number-master/jquery.number.js")!!}

    <script>
        $(document).ready(function () {
            /*For DataTable*/
            $('.data-table').DataTable();
            $('#exp').number( true,0 );

            /*For Delete Application Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/jobs/delete-gen-application/" + id;
                    }
                });
            });

            $('#resume').on('change', function(evt) {
                console.log(this.files);
                console.log(this.files[0].type);
                console.log(this.files[0].size);
                if(this.files[0].type!='application/pdf'){
                    bootbox.alert("File must be a PDF file");
                    var data = "";
                    $('#resume').val(data);
                }
                if(this.files[0].size>500000){
                    bootbox.alert("File must not more than 500 kb");
                    var data = "";
                    $('#resume').val(data);
                }
            });
            $('#photo').on('change', function(evt) {
                console.log(this.files[0].type);
                console.log(this.files[0].size);
                if(this.files[0].size>500000){
                    bootbox.alert("File must not more than 500 kb");
                    var data = "";
                    $('#photo').val(data);
                }
            });

        });
        function checkktp(){
            var id = $('#ktp').val();
            var dataString = 'ktp=' + id;
            $.ajax
            ({
                type: "GET",
                url: '/sss/apply-job/checkktp',
                data: dataString,
                cache: false,
                success: function ( data ) {
                    if(data=='blocked'){
                        bootbox.alert("KTP Telah Terblokir");
                        $('#ktp').val('');
                    } else if(data=='blocked'){
                        bootbox.alert("KTP Telah Terdaftar");
                        $('#ktp').val('');
                    }
                }
            });
        }

        function phonenumber(inputtxt)
        {
            var phoneno = /^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;
            var phoneno2 = /^\(?([0-9]{4})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;  
            var phoneno3 = /^\(?([0-9]{4})\)?[-]?([0-9]{4})[-]?([0-9]{4})$/;  
            var phoneno4 = /^\(?([0-9]{4})\)?[-]?([0-9]{4})[-]?([0-9]{3})$/;
            if(inputtxt.value.match(phoneno)){
                return true;
            } else if(inputtxt.value.match(phoneno2)){
                return true;
            } else if(inputtxt.value.match(phoneno3)){
                return true;
            } else if(inputtxt.value.match(phoneno4)){
                return true;
            } else{
                alert("Not a valid Phone Number");
                return false;
            }
        }
    </script>
@endsection
