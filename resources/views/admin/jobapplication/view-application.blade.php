@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Job Applicants')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

            <div class="col-lg-12">
                <div class="panel-body">
                    <div class="row text-center">

                        <div class="col-sm-3 m-b-15">
                            <div class="z-shad-1">
                                <div class="bg-success text-white p-15 clearfix">
                                    <span class="pull-left font-45 m-l-10"><i class="fa fa-book"></i></span>

                                    <div class="pull-right text-right m-t-15">
                                        <font size="4"><span class=" m-b-5">{{language_data('Applicants')}}</span></font><br>
                                        <font size="4"><span class=" m-b-5">{{$job_applicants}}</span></font>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col-sm-3 m-b-15">
                            <div class="z-shad-1">
                                <div class="bg-complete text-white p-15 clearfix">
                                    <span class="pull-left font-45 m-l-10"><i class="fa fa-user"></i></span>

                                    <div class="pull-right text-right m-t-15">
                                        <font size="4"><span class="m-b-5"> {{language_data('Waiting List')}}</span></font><br>
                                        <font size="4"><span class="m-b-5">{{$job_applicants_waiting_list}}</span></font>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col-sm-3 m-b-15">
                            <div class="z-shad-1">
                                <div class="bg-primary text-white p-15 clearfix">
                                    <span class="pull-left font-45 m-l-10"><i class="fa fa-calendar"></i></span>

                                    <div class="pull-right text-right m-t-15">
                                        <font size="4"><span class="m-b-5"> {{language_data('Close Date')}}</span></font><br>
                                        <font size="4"><span class="m-b-5">{{get_date_format($jobview->close_date)}}</span></font>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col-sm-3 m-b-15">
                            <div class="z-shad-1">
                                <div class="bg-complete-darker text-white p-15 clearfix">
                                    <span class="pull-left font-45 m-l-10"><i class="fa fa-envelope"></i></span>

                                    <div class="pull-right text-right m-t-15">
                                        <font size="4"><span class="m-b-5"> {{language_data('Applicants')}} {{language_data('Accepted')}}</span></font><br>
                                        <font size="4"><span class="m-b-5">{{$job_applicants_confirm}}</span></font>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">

                        {{--Job Detail--}}
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Job Detail')}}</h3>
                        </div>
                        <div class="col-lg-12">

                                <div class="form-group">
                                    <label>{{language_data('Job Number')}}</label>
                                    <input type="text" class="form-control" readonly name="job_number" value="{{$jobview->no_position}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Client Contract Name')}}</label>
                                    <input type="text" class="form-control" readonly name="project" value="@if($jobview->project!=1) {{$jobview->project_name->project}} @else - @endif">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Position')}}</label>
                                    <input type="text" class="form-control" readonly name="position" value="{{$jobview->position_name->designation}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Quota')}}</label>
                                    <input type="number" class="form-control" readonly name="no_position" value="{{$jobview->quota}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Job Location')}}</label>
                                    <input type="text" class="form-control" readonly name="job_location" value="{{$jobview->job_location}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Job Type')}}</label>
                                    <input type="text" class="form-control" readonly name="job_type" value="{{$jobview->job_type}}">

                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('Experience')}}</label>
                                        <input type="number" class="form-control" readonly name="experience" value="{{$jobview->experience}}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Post Date')}}</label>
                                        <input type="text" class="form-control datePicker" readonly name="post_date" value="{{get_date_format($jobview->post_date)}}">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{language_data('Last Education')}}</label>
                                        @if($jobview->last_education!='' && $jobview->last_education!='0')
                                            <input type="text" class="form-control" readonly name="last_education" value="{{$jobview->last_education_info->education}}">
                                        @else
                                            <input type="text" class="form-control" readonly name="last_education" value="{{$jobview->education}}">
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label>{{language_data('Close Date')}}</label>
                                        <input type="text" class="form-control datePicker" readonly name="close_date" value="{{get_date_format($jobview->close_date)}}">
                                    </div>
                                </div>                        
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{language_data('Salary Range')}}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Salary From')}}</label>
                                            <input type="text" class="form-control" readonly name="salary_range" value="{{app_config('Currency')}} {{number_format($jobview->salary_from,0)}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{language_data('Salary To')}}</label>
                                            <input type="text" class="form-control" readonly name="salary_range" value="{{app_config('Currency')}} {{number_format($jobview->salary_to,0)}}">
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="form-group m-none">
                                    <label for="e20">{{language_data('Status')}}</label>
                                    <input type="text" class="form-control" readonly name="status" value="{{$jobview->status}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Short Description')}}</label>
                                    <textarea class="form-control" rows="5" readonly name="short_description">{{$jobview->short_description}}</textarea>
                                </div>


                            </div>

                        {{--Applicants List--}}
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Job Applicants')}}</h3>
                            @if($jobview->status=='opening'  && $jobview->quota > $job_applicants_confirm)
                            @if($permcheck->C==1)
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#add-new-applicants"><i class="fa fa-plus"></i> {{language_data('Add New Applicants')}}</button>
                            <a><button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#upload">{{language_data('Upload')}}</button></a><br>                
                            @endif

                            <br>
                            @endif
                        </div>
                        <div class="panel-body p-none">
                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 5%;"></th>
                                    <th style="width: 20%;">{{language_data('Name')}}</th>
                                    <th style="width: 20%;">{{language_data('Email')}}</th>
                                    <th style="width: 15%;">{{language_data('Phone')}}</th>
                                    <th style="width: 10%;">{{language_data('Status')}}</th>
                                    <th style="width: 25%;">{{language_data('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $ctr = 0; ?>
                                @foreach($applicants as $d)
                                <?php $ctr++; ?>
                                <tr>
                                    <td data-label="no" >{{$ctr}}</td>
                                    @if($d->education >= $jobview->last_education && $d->experience >= $jobview->experience)
                                        <td><i class="fa fa-check" style="color:green;"></i></td>
                                    @else
                                        <td><i class="fa fa-exclamation-circle" style="color:orange;"></i></td>
                                    @endif
                                    <td data-label="Name"><a href="{{url('jobs/view-job-applicant/'.$d->id)}}">{{$d->name}}</a></td>
                                    <td data-label="Username"><a href="mailto:{{$d->email}}">{{$d->email}}</a></td>
                                    <td data-label="Phone"><a href="tel:{{$d->phone}}">{{$d->phone}}</a></td>
                                    @if($jobview->quota>$job_applicants_confirm)
                                        @if($d->status=='Unread')
                                            <td data-label="Status"><p class="btn btn-warning btn-xs">{{language_data('Unread')}}</p></td>
                                            <td data-label="Actions">
                                                <a class="btn btn-complete btn-xs" href="{{url('jobs/download-resume/2/'.$d->id)}}"><i class="fa fa-download"></i> {{language_data('Resume')}}</a>
                                                <a class="btn btn-success btn-xs" href="{{url('jobs/view-interview-applicant/'.$d->id)}}"><i class="fa fa-edit"></i> {{language_data('Status')}}</a>
                                                <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$d->id}}"><i class="fa fa-trash"></i> {{language_data('Delete')}}</a>
                                            </td>
                                        @elseif($d->status=='Interview')
                                            <td data-label="Status"><p class="btn btn-complete btn-xs">{{language_data('Interview')}} : {{language_data($d->sub_status_psychotest)}}</p></td>
                                            <td data-label="Actions">
                                                <a class="btn btn-complete btn-xs" href="{{url('jobs/view-interview-applicant/'.$d->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            </td>
                                        @elseif($d->status=='Waiting List')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data($d->status)}}</p></td>
                                            <td data-label="Actions">
                                                <a class="btn btn-complete btn-xs" href="{{url('jobs/view-interview-applicant/'.$d->id)}}"><i class="fa fa-eye"></i> {{language_data('View')}}</a>
                                            </td>
                                        @elseif($d->status=='Confirm')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Confirm')}}</p></td>
                                            <td data-label="Actions"></td>
                                        @else
                                            <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Rejected')}}</p></td>
                                            <td data-label="Actions"></td>
                                        @endif
                                    @else
                                        @if($d->status=='Unread')
                                            <td data-label="Status"><p class="btn btn-warning btn-xs">{{language_data('Unread')}}</p></td>
                                            <td data-label="Actions">
                                            </td>
                                        @elseif($d->status=='Interview')
                                            <td data-label="Status"><p class="btn btn-complete btn-xs">{{language_data('Interview')}} : {{language_data($d->sub_status_psychotest)}}</p></td>
                                            <td data-label="Actions">
                                            </td>
                                        @elseif($d->status=='Waiting List')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data($d->status)}}</p></td>
                                            <td data-label="Actions">
                                            </td>
                                        @elseif($d->status=='Confirm')
                                            <td data-label="Status"><p class="btn btn-success btn-xs">{{language_data('Confirm')}}</p></td>
                                            <td data-label="Actions"></td>
                                        @else
                                            <td data-label="Status"><p class="btn btn-danger btn-xs">{{language_data('Rejected')}}</p></td>
                                            <td data-label="Actions"></td>
                                        @endif
                                    @endif

                                    
                                </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Modal add -->
            <div class="modal fade" id="add-new-applicants" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog" role="document"  >
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Add New Applicants')}}</h4>
                        </div>
                        <div class="modal-body">

                            <label class="radio-inline">
                                <input type="radio" class="toggle_specific" name="newapp" value="new" id="newapp1">Input Pelamar
                            </label>
                            <label class="radio-inline">
                                <input type="radio" class="toggle_specific" name="newapp" value="old" id="newapp2">Pilih Pelamar
                            </label>
                        </div>
                        <form class="form-some-up a" name="form" role="form" method="post" action="{{url('jobs/add-applicant')}}" enctype="multipart/form-data" onsubmit="return !!(phonenumber(document.form.phone) or phonenumber(document.form.phone_old));">
                            <div class="modal-body a" style="display: none;">
                                <div class="form-group a">
                                    <label>{{language_data('Name')}}</label>
                                    <input type="text" class="form-control name" name="name" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Email')}}</label>
                                    <span class="help">e.g. "coderpixel@gmail.com" ({{language_data('Unique For every User')}})</span>
                                    <input type="text" class="form-control" name="email" value="">
                                </div>

                                <div class="form-group a">
                                    <label>No. KTP</label>
                                    <input type="text" class="form-control"  name="ktp" id="ktp" onchange="checkktp()" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent Address')}}</label>
                                    <textarea class="form-control" rows="5" placeholder="Alamat" name="address" value=""></textarea>
                                </div>

                                <div class="form-group a">
                                    <div class="input-group">
                                        <span class="input-group-addon"> RT </span>
                                        <input type="text" class="form-control" name="rt" required value="">
                                        <span class="input-group-addon"> RW </span>
                                        <input type="text" class="form-control" name="rw" required value=""> 
                                    </div>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent Village')}}</label>
                                    <input type="text" class="form-control"  name="village" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent District')}}</label>
                                    <input type="text" class="form-control"  name="district" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Permanent City')}}</label>
                                    <input type="text" class="form-control"  name="city" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present Address')}}</label>
                                    <textarea placeholder="Alamat" class="form-control" rows="5" name="address_domisili" value=""></textarea>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present Village')}}</label>
                                    <input type="text" class="form-control"  name="village_domisili" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present District')}}</label>
                                    <input type="text" class="form-control"  name="district_domisili" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Present City')}}</label>
                                    <input type="text" class="form-control"  name="city_domisili" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Phone')}}</label>
                                    <span class="help">e.g. "xxx-xxx-xxxx, xxxx-xxx-xxxx, xxxx-xxxx-xxxx, xxxx-xxxx-xxx"</span>
                                    <input type="text" class="form-control" name="phone" value="">
                                </div>
        
                                <div class="form-group a">
                                    <label>{{language_data('Place of Birth')}}</label>
                                    <input type="text" class="form-control"  name="birth_place" value="">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Date Of Birth')}}</label>
                                    <input type="text" class="form-control datePicker" required=""  name="birth_date">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Gender')}}</label>
                                    <select name="gender" class="form-control selectpicker">
                                        <option value="male">{{language_data('male')}}</option>
                                        <option value="female">{{language_data('female')}}</option>
                                    </select>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Marital Status')}}</label>
                                    <select name="marital_status" class="form-control selectpicker">
                                        <option value="Lajang">{{language_data('Single')}}</option>
                                        <option value="Menikah (K1)">{{language_data('Married (K1)')}}</option>
                                        <option value="Menikah (K2)">{{language_data('Married (K2)')}}</option>
                                        <option value="Menikah (K3)">{{language_data('Married (K3)')}}</option>
                                        <option value="Menikah (K4)">{{language_data('Married (K4)')}}</option>
                                        <option value="Duda">{{language_data('Widower')}}</option>
                                        <option value="Janda">{{language_data('Widow')}}</option>
                                    </select>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Religion')}}</label>
                                    <select name="religion" class="form-control selectpicker">
                                        <option value="Islam">{{language_data('Islam')}}</option>
                                        <option value="Kristen">{{language_data('Christian')}}</option>
                                        <option value="Hindu">{{language_data('Hindu')}}</option>
                                        <option value="Budha">{{language_data('Buddha')}}</option>
                                    </select>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data("Biological Mother's Name")}}</label>
                                    <input type="text" class="form-control" required=""  name="ibu_kandung">
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Job title proposed')}}</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="designation">
                                        <option value="{{$jobview->position_name->id}}">{{$jobview->position_name->designation}}</option>
                                    </select>
                                </div>

                                <div class="col-lg-6 a">
                                    <div class="form-group">
                                        <label>{{language_data('Experience')}}</label>
                                        <input type="number" min="0" max="15" class="form-control" name="experience">
                                    </div>
                                </div>

                                <div class="col-lg-6 a">
                                    <div class="form-group">
                                        <label>{{language_data('Last Education')}}</label>
                                        <select class="selectpicker form-control" data-live-search="true" name="last_education">
                                            @foreach($last_education as $l)
                                                <option value="{{$l->id}}">{{$l->education}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                
                                <div class="form-group a">
                                    <label>{{language_data('Curricullum Vitae')}}</label>
                                    <div class="input-group">
                                        <input id="resume" name="resume" class="form-control" type="file" accept="application/pdf">
                                    </div>
                                </div>

                                <div class="form-group a">
                                    <label>{{language_data('Photo')}}</label>
                                    <div class="input-group">
                                        <input id="photo" name="photo" class="form-control" type="file"  accept="image/*">
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer a" style="display: none;">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$jobview->id}}">
                                <input type="hidden" name="newapp" value="new">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>

                        <form class="form-some-up b" name="form" role="form" method="post" action="{{url('jobs/add-applicant')}}" enctype="multipart/form-data" onsubmit="return !!(phonenumber(document.form.phone) or phonenumber(document.form.phone_old));">
                            
                            <div class="modal-body b" style="display: none;">
                                <div class="form-group b" style="display: none;">
                                    <label>{{language_data('Name')}}</label>
                                    <select name="id_old" class="form-control selectpicker" data-live-search="true" id="id_old">
                                            <option value='0'>{{language_data('Select Applicant')}}</option>
                                        @foreach($general_applicants as $gen)
                                            <option value='{{$gen->id}}'>{{$gen->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group b" style="display: none;" id="name_old">
                                    <input type="hidden" class="form-control name" readonly name="name_old">
                                </div>

                                <div class="form-group b" style="display: none;" id="email_old">
                                    <label>{{language_data('Email')}}</label>
                                    <span class="help">e.g. "coderpixel@gmail.com" ({{language_data('Unique For every User')}})</span>
                                    <input type="text" class="form-control" readonly name="email_old">
                                </div>

                                <div class="form-group b" style="display: none;" id="phone_old">
                                    <label>{{language_data('Phone Number')}}</label>
                                    <span class="help">e.g. "xxx-xxx-xxxx, xxxx-xxx-xxxx, xxxx-xxxx-xxxx, xxxx-xxxx-xxx"</span>
                                    <input type="text" class="form-control" readonly name="phone_old">
                                </div>

                                <div class="form-group b" style="display: none;" id="address_old">
                                    <label>{{language_data('Address')}}</label>
                                    <textarea class="form-control" placeholder="Alamat" rows="5" readonly name="address_old"></textarea>
                                </div>

                                <div class="col-lg-6 b" style="display: none;">
                                    <div class="form-group" id="experience_old">
                                        <label>{{language_data('Experience')}}</label>
                                        <input type="number" class="form-control" value="" readonly name="experience_old">
                                    </div>
                                </div>

                                <div class="col-lg-6 b" style="display: none;">
                                    <div class="form-group" id="last_education_old">
                                        <label>{{language_data('Last Education')}}</label>
                                        <input type="text" class="form-control" value="" readonly name="last_education_old">
                                    </div>
                                </div>

                                
                                <div class="form-group b" style="display: none;">
                                    <label>{{language_data('Curricullum Vitae')}}</label>
                                    <div class="input-group input-group-file" id="file_old">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" disabled class="form-control" name="file_old" id="file_old">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly="" name="file_old">
                                    </div>
                                </div>

                                <div class="form-group b" style="display: none;">
                                    <label>{{language_data('Photo')}}</label>
                                    <div class="input-group input-group-file" id="image_old">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" disabled class="form-control" name="image_old" id="image_old">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly="" name="images_old">
                                    </div>
                                </div>

                                <div class="form-group b" style="display: none;">
                                    <label>{{language_data('Status')}}</label>
                                    <select name="status_old" class="form-control selectpicker">
                                        <option value="Unread">{{language_data('Unread')}}</option>
                                        <option value="Psychotest">{{language_data('Psychotest')}}</option>
                                        <option value="Interview">{{language_data('Interview')}}</option>
                                        <option value="Confirm">{{language_data('Confirm')}}</option>
                                        <option value="Rejected">{{language_data('Rejected')}}</option>
                                    </select>
                                </div>
                            </div>


                                
                            <div class="modal-footer b" style="display: none;">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="cmd" value="{{$jobview->id}}">
                                <input type="hidden" name="newapp" value="old">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Add')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{language_data('Upload File')}}</h4>
                        </div>
                        <form class="form-some-up" role="form" action="{{ URL::to('jobs/importExcelPekerja') }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label>{{language_data('Select file')}}</label>
                                    <div class="input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse')}} <input type="file" class="form-control" name="import_file" accept="application/vnd.ms-excel"/>
                                            </span>
                                        </span>
                                    <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>
 
                                *.xls (Excel 2003), Download template <a href="{{ URL::to('jobs/downloadExcelPelamar/xls') }}">here</a>
                            
                            </div>

                            <div class="modal-footer">
                            <input type="hidden" name="cmd" value="{{$jobview->id}}">
                            
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{language_data('Close')}}</button>
                                <button type="submit" class="btn btn-primary">{{language_data('Upload File')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/wysihtml5x/wysihtml5x-toolbar.min.js")!!}
    {!! Html::script("assets/libs/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/id.js")!!}

    <script>
        $(document).ready(function () {
            $('#resume').on('change', function(evt) {
                console.log(this.files[0].type);
                console.log(this.files[0].size);
                if(this.files[0].type!='application/pdf'){
                    bootbox.alert("File must be a PDF file");
                    var data = "";
                    $('#resume').val(data);
                }
                if(this.files[0].size>500000){
                    bootbox.alert("File must not more than 500 kb");
                    var data = "";
                    $('#resume').val(data);
                }
            });
            $('#photo').on('change', function(evt) {
                console.log(this.files[0].size);
                if(this.files[0].size>500000){
                    bootbox.alert("File must not more than 500 kb");
                    var data = "";
                    $('#photo').val(data);
                }
            });
            /*For DataTable*/
            $('.data-table').DataTable();

            /*For Delete Application Info*/
            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/jobs/delete-application/" + id;
                    }
                });
            });


        });

        function phonenumber(inputtxt)
        {
            var phoneno = /^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;
            var phoneno2 = /^\(?([0-9]{4})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;  
            var phoneno3 = /^\(?([0-9]{4})\)?[-]?([0-9]{4})[-]?([0-9]{4})$/;  
            var phoneno4 = /^\(?([0-9]{4})\)?[-]?([0-9]{4})[-]?([0-9]{3})$/;
            if(inputtxt.value.match(phoneno)){
                return true;
            } else if(inputtxt.value.match(phoneno2)){
                return true;
            } else if(inputtxt.value.match(phoneno3)){
                return true;
            } else if(inputtxt.value.match(phoneno4)){
                return true;
            } else{
                alert("Not a valid Phone Number");
                return false;
            }
        }

        $(".toggle_specific").click(function () {
            toggle_specific_dropdown();
        });
        toggle_specific_dropdown();

        
        function toggle_specific_dropdown() {
            var $element = $(".toggle_specific:checked");
            if ($element.val() === "new") {
                $(".a").show();
                $(".b").hide();
            } else if ($element.val() === "old") {
                $(".b").show();
                $(".a").hide();
            }
        }
        
        $('#id_old').on('change', function(e){
            console.log(e);

            var gen_app_id = e.target.value;
            var _url = $("#_url").val();

            //ajax
            $.get(_url + '/jobs/ajax-generalApp?gen_app_id=' + gen_app_id, function(data){
                //success data
                $('#name_old').empty();
                $('#email_old').empty();
                $('#phone_old').empty();
                $('#address_old').empty();
                $('#experience_old').empty();
                $('#last_education_old').empty();
                $('#file_old').empty();
                $('#image_old').empty();
                $.each(data, function(index, genObj){
                    var nama_pendidikan = '';

                    if(genObj.education == 0){
                        nama_pendidikan = 'Non Pendidikan';
                    }else if(genObj.education == 1){
                        nama_pendidikan = 'SD';
                    }else if(genObj.education == 2){
                        nama_pendidikan = 'SMP';
                    }else if(genObj.education == 3){
                        nama_pendidikan = 'SMA/SMK';
                    }else if(genObj.education == 4){
                        nama_pendidikan = 'D1';
                    }else if(genObj.education == 5){
                        nama_pendidikan = 'D3';
                    }else if(genObj.education == 6){
                        nama_pendidikan = 'D4';
                    }else if(genObj.education == 7){
                        nama_pendidikan = 'S1';
                    }else if(genObj.education == 8){
                        nama_pendidikan = 'S2';
                    }else if(genObj.education == 9){
                        nama_pendidikan = 'S3';
                    }
                    $('#name_old').append('<input type="hidden" value="'+genObj.name+'" readonly name="name_old">'); 
                    $('#email_old').append('<label>Email</label><input type="text" class="form-control" value="'+genObj.email+'" readonly name="email_old">'); 
                    $('#phone_old').append('<label>Phone Number</label><input type="text" class="form-control" value="'+genObj.phone+'" readonly name="phone_old">');
                    $('#address_old').append('<label>{{language_data('Address')}}</label><textarea class="form-control" rows="5" value="'+genObj.address+'" readonly name="address_old">'+genObj.address+'</textarea>');
                    $('#experience_old').append('<label>Experience</label><input type="number" class="form-control" value="'+genObj.experience+'" readonly name="experience_old">');
                    $('#last_education_old').append('<label>Last Education</label><input type="text" class="form-control" value="'+nama_pendidikan+'" readonly><input type="hidden" class="form-control" value="'+genObj.education+'" readonly name="last_education_old">');
                    if(genObj.resume!==null&&genObj.resume!=='null'&&genObj.resume!==''){
                        $('#file_old').append('<span class="input-group-btn"><span class="btn btn-primary btn-file">Browse <input type="file" class="form-control" name="file_old" id="file_old"></span></span><input type="text" class="form-control" readonly="" value="'+genObj.resume+'" name="file_old">');
                    }else{
                        $('#file_old').append('<span class="input-group-btn"><span class="btn btn-primary btn-file">Browse <input type="file" class="form-control" name="file_old" id="file_old"></span></span><input type="text" class="form-control" readonly="" value="" name="file_old">');
                    }
                    if(genObj.photo!==null&&genObj.photo!=='null'&&genObj.photo!==''){
                        $('#image_old').append('<span class="input-group-btn"><span class="btn btn-primary btn-file">Browse <input type="file" class="form-control" name="image_old" id="image_old"></span></span><input type="text" class="form-control" readonly="" value="'+genObj.photo+'" name="images_old">');
                    } else {
                        $('#image_old').append('<span class="input-group-btn"><span class="btn btn-primary btn-file">Browse <input type="file" class="form-control" name="image_old" id="image_old"></span></span><input type="text" class="form-control" readonly="" value="" name="images_old">');
                    }
                });
            });
        });
        function checkktp(){
            var id = $('#ktp').val();
            var dataString = 'ktp=' + id;
            $.ajax
            ({
                type: "GET",
                url: '/sss/apply-job/checkktp',
                data: dataString,
                cache: false,
                success: function ( data ) {
                    if(data=='blocked'){
                        bootbox.alert("KTP Telah Terblokir");
                        $('#ktp').val('');
                    } else if(data=='blocked'){
                        bootbox.alert("KTP Telah Terdaftar");
                        $('#ktp').val('');
                    }
                }
            });
        }
    </script>
    
@endsection
