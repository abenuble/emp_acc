var $ = jQuery;

$(document).ready(function(){
	if ( $('.textarea-wysihtml5').length > 0 ) {
		var _url = $("#_url").val();
		// alert(_url);
		// $('.textarea-wysihtml5').wysihtml5({
		// 	toolbar: {
		// 		'fa': true
		// 	}
		// });
		
		// CKEDITOR.replace( 'textarea-wysihtml5' , {
		// 	filebrowserBrowseUrl: _url +'application/vendor/unisharp/laravel-ckeditor/plugins/ckfinder/ckfinder.html',
		// 	filebrowserUploadUrl: _url +'application/vendor/unisharp/laravel-ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
		// });
        // $('.textarea-wysihtml5').ckeditor({
		// 	filebrowserBrowseUrl: _url +'application/vendor/unisharp/laravel-ckeditor/plugins/ckfinder/ckfinder.html',
		// 	filebrowserUploadUrl: _url +'application/vendor/unisharp/laravel-ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
        // });
        CKEDITOR.editorConfig = function( config ) {
            config.toolbarGroups = [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                { name: 'forms', groups: [ 'forms' ] },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                { name: 'links', groups: [ 'links' ] },
                { name: 'insert', groups: [ 'insert' ] },
                '/',
                { name: 'styles', groups: [ 'styles' ] },
                { name: 'colors', groups: [ 'colors' ] },
                { name: 'tools', groups: [ 'tools' ] },
                { name: 'others', groups: [ 'others' ] },
                { name: 'about', groups: [ 'about' ] }
            ];
        };
        $('.textarea-wysihtml5').ckeditor({
			filebrowserBrowseUrl: _url +'/application/vendor/unisharp/laravel-ckeditor/plugins/ckfinder/ckfinder.html',
			filebrowserUploadUrl: _url +'/application/vendor/unisharp/laravel-ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
		});
	}
	$( "form" ).submit(function( event ) {
        var fields = $(this).find("div.block").find(".selectpicker").serializeArray();
        console.log(fields);
        if(fields.length!=0){
            $.each(fields, function(i, field) {
                if (!field.value || field.value==0){
                    event.preventDefault();
                    alert(field.name + " is required");
                    return false;
                }
            }); 
        } else {
			var fields = $(this).find("div.alldesg").find(".selectpicker").serializeArray();
			console.log(fields);
			if(fields.length!=0){
				$.each(fields, function(i, field) {
					if (!field.value){
						event.preventDefault();
						alert(field.name + " is required");
						return false;
					}
				}); 
			} else {
				var fields = $(this).find("div.sembunyi").find(".selectpicker").serializeArray();
				console.log(fields);
				if(fields.length!=0){
					
				} else {
					var fields = $(this).find("div:visible").find(".selectpicker").serializeArray();
					console.log(fields);
					if(fields.length!=0){
						$.each(fields, function(i, field) {
							if (!field.value || field.value==0){
								event.preventDefault();
								alert(field.name + " is required");
								return false;
							}
						}); 
					}
				}
			}
		}
		return true;
	});

    // var screenWidth=window.screen.width;
    // if ( screenWidth >= 1920){
    //     $('body').addClass("no-nav-animation left-bar-open");
    //     var handler = setTimeout(function(){
    //         $('body').removeClass("no-nav-animation");
    //         clearTimeout(handler);
    //     }, 200);
    // }

});


$('#left-nav .nav-bottom-sec').slimScroll({
	height: '100%',
	size: '15px',
	color: '#999'
});


$('#bar-setting').click(function(e){
	e.preventDefault();

	if ( $(window).width() > 767 ) {
		$('body.has-left-bar').toggleClass('left-bar-open');
	} else {
		$('.left-nav-bar .nav-bottom-sec').slideToggle( 500, function(){
			$('body.has-left-bar').toggleClass('left-bar-open');
		});
	}

});
$('.name').change(function(){
    var str = $(this).val();
    str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
    $(this).val(str);
});



$('#left-navigation').find('li.has-sub>a').on('click', function(e){
	e.preventDefault();
	var $thisParent = $(this).parent();

	if ( $thisParent.hasClass('sub-open') ) {

		// Hide the Submenu
		$thisParent.removeClass('sub-open').children('ul.sub').slideUp(150);

	} else {

		// Show the Submenu
		$thisParent.addClass('sub-open').children('ul.sub').slideDown(150);

		// Hide Others Submenu
		$thisParent.siblings('.sub-open').removeClass('sub-open').children('ul.sub').slideUp(150);

	}
});


// alertify customize

alertify.warning = alertify.extend("warning");
alertify.info = alertify.extend("info");

// Tooltip init

$(function () {
	$('[data-toggle="tooltip"]').tooltip()
});

// Form
(function(){

	$('.form-group.form-group-default .form-control').on('focus', function(e){
		$(this).closest('.form-group').addClass('focused');
	}).on('blur', function(e){
		var $closest = $(this).closest('.form-group');
		if ($(this).val().length > 0) {
			$closest.addClass('filled');
		} else {
			$closest.removeClass('filled');
		}
		$closest.removeClass('focused');
	});

	$('.form-group.form-group-default select.form-control').on('change', function(){
		$(this).closest('.form-group').addClass('filled');
	});

})();



