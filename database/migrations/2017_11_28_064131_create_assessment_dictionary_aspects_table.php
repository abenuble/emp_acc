<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentDictionaryAspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_assessment_dictionary_aspects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assessment');
            $table->text('aspect');
            $table->text('KS')->nullable();
            $table->text('K')->nullable();
            $table->text('C')->nullable();
            $table->text('B')->nullable();
            $table->text('BS')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_assessment_dictionary_aspects');
    }
}
