<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEndContractRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_end_contract_recipients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('letter_number',50);
            $table->string('project_number',50);
            $table->string('end_contract_id',50);
            $table->integer('recipients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_end_contract_recipients');
    }
}
