<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAssessmentReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_employee_assessment_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->integer('project');
            $table->integer('company');
            $table->float('grade',5,2);
            $table->float('average',5,2);
            $table->float('difference',5,2);
            $table->text('information');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_employee_assessment_reports');
    }
}
