<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_project_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_project');
            $table->string('item',150);
            $table->integer('amount');
            $table->integer('amount_bought')->default('0');
            $table->integer('leftovers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_project_items');
    }
}
