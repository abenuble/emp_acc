<?php

use Illuminate\Database\Schema\Blueprint;
use App\Component;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_payment_components', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->integer('payment');
            $table->text('month');
            $table->integer('total')->default(0);
            $table->timestamps();
            $columns = Component::pluck('component')->toArray();
            foreach($columns as $col){
                $table->integer(preg_replace('/ /', '_', $col))->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_payment_components');
    }
}
