<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAssessmentViolationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_employee_assessment_violations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->integer('emp_assessment_id');
            $table->text('violation');
            $table->float('weighted_grade',5,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_employee_assessment_violations');
    }
}
