<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_procurements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('procurement_number',50);
            $table->integer('company');
            $table->integer('project')->nullable();
            $table->enum('type', ['internal', 'project']);
            $table->enum('category', ['subsist','non-subsist']);
            $table->date('date');
            $table->text('supplier');
            $table->enum('status',['approved','draft','rejected'])->default('draft');
            $table->string('reception_number',50)->nullable();
            $table->date('reception_date')->nullable();
            $table->text('reception_file')->nullable();
            $table->string('realization_number',50)->nullable();
            $table->date('realization_date')->nullable();
            $table->enum('realization_status',['approved','draft','rejected'])->nullable();
            $table->integer('realization_total')->nullable();
            $table->integer('realization_differences')->nullable();
            $table->string('payout_number',50)->nullable();
            $table->date('payout_date',50)->nullable();
            $table->text('payout_file')->nullable();
            $table->enum('payout_status',['approved','draft','rejected'])->nullable();
            $table->enum('process_status',['reception','realization','payout'])->nullable();
            $table->integer('total');
            $table->integer('deficiency');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_procurements');
    }
}
