<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_out_letters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('letter_number',50);
            $table->text('subject');
            $table->text('message');
            $table->date('date');
            $table->enum('status',['approved','draft','rejected'])->default('draft');
            $table->text('category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_out_letters');
    }
}
