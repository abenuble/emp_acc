<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysEndContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_end_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('draft_letter',50);
            $table->date('date');
            $table->date('effective_date');
            $table->string('project_number',50);
            $table->text('company_name');
            $table->text('share_with');
            $table->enum('status', ['draft', 'accepted', 'rejected'])->default('draft');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_end_contracts');
    }
}
