<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_accounting_banks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('proof_number',50);
            $table->date('date');
            $table->integer('attachment');
            $table->enum('proof_type',['bank_in','bank_out']);
            $table->integer('bank_coa');
            $table->integer('total');
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_accounting_banks');
    }
}
