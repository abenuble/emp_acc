<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingCashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_accounting_cashes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('proof_number',50);
            $table->date('date');
            $table->integer('attachment');
            $table->enum('proof_type',['cash_in','cash_out']);
            $table->integer('cash_coa');
            $table->integer('total');
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_accounting_cashes');
    }
}
