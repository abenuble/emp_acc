<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysWarningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_warning', function (Blueprint $table) {
            $table->increments('id');
            $table->string('draft_letter',50);
            $table->string('letter_number',50)->unique();
            $table->date('date');
            $table->text('type');
            $table->string('project_number',50);
            $table->text('company_name');
            $table->string('employee_code',50);
            $table->text('employee_name');
            $table->string('official_report_number',50);
            $table->date('official_report_date');
            $table->text('violate');
            $table->date('effective_date');
            $table->date('end_date');
            $table->text('warning_information');
            $table->enum('status', ['draft', 'accepted', 'rejected'])->default('draft');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_warning');
    }
}
