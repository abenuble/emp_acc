<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysMutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_mutations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('draft_letter',50);
            $table->string('letter_number',50)->unique();
            $table->date('date');
            $table->date('mutation_date');
            $table->string('project_number',50);
            $table->text('company_name');
            $table->string('employee_code',50);
            $table->text('employee_name');
            $table->integer('designation_old');
            $table->text('location_old');
            $table->text('employee_status_old');
            $table->integer('designation_new');
            $table->text('location_new')->nullable();
            $table->text('employee_status_new');
            $table->enum('status', ['draft', 'accepted', 'rejected'])->default('draft');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_mutations');
    }
}
