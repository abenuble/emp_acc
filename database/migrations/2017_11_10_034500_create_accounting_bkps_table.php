<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingBkpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_accounting_bkps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bkp_code',50);
            $table->integer('project');
            $table->date('date');
            $table->integer('attachment');
            $table->enum('payment_to',['client_company','others','giro_check']);
            $table->integer('paid_to')->nullable();
            $table->text('about');
            $table->integer('total_payment');
            $table->integer('voucher_debt');
            $table->integer('discount');
            $table->integer('paid_amount');
            $table->enum('expenditure_type',['bank','cash','check']);
            $table->integer('bank_id')->nullable();
            $table->integer('nominal_bank')->nullable();
            $table->integer('cash_id')->nullable();
            $table->integer('nominal_cash')->nullable();
            $table->integer('check_id')->nullable();
            $table->integer('nominal_check')->nullable();
            $table->enum('status',['approved','draft','rejected'])->default('draft');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_accounting_bkps');
    }
}
