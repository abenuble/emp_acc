<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_vehicle_taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('vehicle');
            $table->string('license',50)->unique();
            $table->date('tax_due_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_vehicle_taxes');
    }
}
