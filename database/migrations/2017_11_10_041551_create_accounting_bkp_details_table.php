<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingBkpDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_accounting_bkp_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bkp');
            $table->integer('coa');
            $table->integer('procurement')->nullable();
            $table->integer('deficiency')->default(0);
            $table->integer('payment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_accounting_bkp_details');
    }
}
