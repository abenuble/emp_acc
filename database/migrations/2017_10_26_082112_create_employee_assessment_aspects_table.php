<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAssessmentAspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_employee_assessment_aspects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->integer('emp_assessment_id');
            $table->text('aspect');
            $table->integer('weight');
            $table->integer('grade');
            $table->float('weighted_grade',5,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_employee_assessment_aspects');
    }
}
