<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_project_vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_project');
            $table->integer('emp_id');
            $table->text('brand');
            $table->text('type');
            $table->text('tmt');
            $table->string('license_plate',10);
            $table->date('vehicle_registration_date');
            $table->date('tax_due_date');
            $table->text('insurance_vendor');
            $table->text('vehicle_vendor');
            $table->integer('car_rental_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_project_vehicles');
    }
}
