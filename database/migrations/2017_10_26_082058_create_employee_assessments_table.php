<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_employee_assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->text('period_from');
            $table->text('period_to');
            $table->integer('emp_id');
            $table->integer('assessment_form');
            $table->integer('project');
            $table->integer('company');
            $table->float('grade',5,2);
            $table->text('information');
            $table->text('additional_note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_employee_assessments');
    }
}
