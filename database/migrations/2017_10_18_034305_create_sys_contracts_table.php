<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('draft_letter',50);
            $table->date('date');
            $table->string('project_number',50);
            $table->text('contract_types');
            $table->text('project_name');
            $table->text('company_name');
            $table->integer('payroll_types');
            $table->date('effective_date');
            $table->date('end_date');
            $table->text('share_with');
            $table->enum('status', ['draft', 'accepted', 'rejected'])->default('draft');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_contracts');
    }
}
