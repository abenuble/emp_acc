<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcurementGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_procurement_goods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('procurement');
            $table->text('goods');
            $table->integer('quantity');
            $table->text('unit');
            $table->integer('unit_price');
            $table->integer('total_price');
            $table->integer('reception_quantity');
            $table->integer('realization_quantity');
            $table->integer('realization_unit_price');
            $table->integer('realization_total_price');
            $table->integer('realization_difference');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_procurement_goods');
    }
}
