<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingCashDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_accounting_cash_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('proof_id');
            $table->integer('coa');
            $table->integer('amount');
            $table->integer('in');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sys_accounting_cash_details');
    }
}
