<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssessmentAspect extends Model
{
    protected $table='sys_assessment_aspect';

    /* leave dimension_info Start Here */
    public function dimension_info()
    {
        return $this->hasOne('App\AssessmentDimension','id','dimension');
    }
}
