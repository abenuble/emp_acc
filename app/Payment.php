<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'sys_payments';

    /* payroll_info function */
    public function payroll_info()
    {
        return $this->hasOne('App\PayrollTypes','id','payroll_type');
    }
    /* project_info function */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','project');
    }
    /* company_info function */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','company');
    }
    /* employee_info function */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','emp_id');
    }
}
