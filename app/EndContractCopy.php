<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EndContractCopy extends Model
{
    protected $table = 'sys_end_contract_copies';

    /* contract_info Start Here */
    public function end_contract_info()
    {
        return $this->hasOne('App\EndContract','id','end_contract_id');
    }
}
