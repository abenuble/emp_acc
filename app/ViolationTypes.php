<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViolationTypes extends Model
{
    protected $table = 'sys_violation_types';
}
