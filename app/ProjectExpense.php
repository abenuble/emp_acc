<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectExpense extends Model
{
    protected $table='sys_project_expense';

    /* project  Function Start Here */
    public function project_name()
    {
        return $this->hasOne('App\Project','id','id_project');
    }

    public function company_name()
    {
        return $this->hasOne('App\Company','id','id_company');
    }
}
