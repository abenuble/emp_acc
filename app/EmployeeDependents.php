<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeDependents extends Model
{
    protected $table='sys_employee_dependents';
    protected $fillable=['emp_id','child_name'];

    /* employee  Function Start Here */
    public function employee_name()
    {
        return $this->hasOne('App\Employee','employee_code','emp_id');
    }
}
