<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    protected $table = 'sys_components';
    public function coa_info()
    {
        return $this->hasOne('App\Coa','id','coa');
    }

}
