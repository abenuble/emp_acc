<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingBkmDetail extends Model
{
    protected $table = 'sys_accounting_bkm_details';
    /* bkp_info Start Here */
    public function bkm_info()
    {
        return $this->hasOne('App\AccountingBkm','id','bkp');
    }
    /* coa_info Start Here */
    public function coa_info()
    {
        return $this->hasOne('App\Coa','id','coa');
    }
    /* expense_info Start Here */
    public function expense_info()
    {
        return $this->hasOne('App\Expense','id','expense');
    }
    public function procurement_info()
    {
        return $this->hasOne('App\Procurement','id','expense');
    }
}
