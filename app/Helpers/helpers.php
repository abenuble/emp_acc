<?php
use App\AppConfig;
use App\Notification;
use App\Calendar;
use App\Employee;
use App\AccountSetting;
use App\Menu;
use App\EmployeeRoles;
use App\EmployeeRolesPermission;
use App\Leave;
use App\Task;
use App\SupportTickets;
use App\Language;
use Illuminate\Support\Facades\Schema;
use Stichoza\GoogleTranslate\TranslateClient;
use \App\LanguageData;
use \Statickidz\GoogleTranslate;

use App\AccountingBKM;
use App\AccountingBKP;
use App\Appointment;
use App\Project;
use App\Contract;
use App\Endcontract;
use App\Mutation;
use App\OvertimeWarrant;
use App\Payment;
use App\Phk;
use App\Procurement;
use App\Expense;
use App\Resign;
use App\SuratPengantar;
use App\Warning;
use App\Company;

function app_config($value = '')
{
    $conf = AppConfig::where('setting','=',$value)->first();
    return $conf->value;
}

function language_data($value=''){
    $lan_info_en = LanguageData::where('lan_id','=','1')->where('lan_data',$value)->first();
    $lan_info_id = LanguageData::where('lan_id','=','2')->where('lan_data',$value)->first();

    if($lan_info_en){

    } else {
        $lan_info_en = new languageData();
        $lan_info_en->lan_id = 1;
        $lan_info_en->lan_data = $value;
        $lan_info_en->lan_value = $value;
        $lan_info_en->save();
    }

    if($lan_info_id){

    } else {
        $lan_info_id = new languageData();
        $lan_info_id->lan_id = 2;
        $lan_info_id->lan_data = $value;
        $lan_info_id->lan_value = GoogleTranslate::translate('en', 'id', $value);
        $lan_info_id->save();
    }

    $language=app_config('Language');
    $lan_info= LanguageData::where('lan_id',$language)->where('lan_data',$value)->first();
    if($lan_info){
        return $lan_info->lan_value;
    } else {
        return $value;
    }
}

function menu_access($id=''){

    $menu = Menu::Where('status','=','y')->orderBy('indexs','ASC')->get();
    $role = EmployeeRoles::where('status','Active')->get();
    foreach($menu as $m){
        foreach($role as $r){
            $permcheck = EmployeeRolesPermission::where('role_id', $r->id)->where('perm_id', $m->id)->first();
            if(empty($permcheck)){
                $permission = new EmployeeRolesPermission();
                $permission->role_id = $r->id;
                $permission->perm_id = $m->id;
                $permission->C = 1;
                $permission->R = 1;
                $permission->U = 1;
                $permission->D = 1;
                $permission->save();
            } 
        }
    }

    $role_id=\Illuminate\Support\Facades\Auth::user()->role_id;

    $menus_permission=\App\EmployeeRolesPermission::where('role_id',$role_id)->where('perm_id',$id)->first();

    if ($menus_permission->R==1){
        return true;
    }else{
        return false;
    }

}	

// CARI SETTING AKUN

function findSettingAccount($id = NULL){
    $data = array();
    if ($id) {
        $AccountSetting  = AccountSetting::find($id);
        if ($AccountSetting) {
            $data = array(
                'menu'       	    => $AccountSetting->menu,
                'activities' 	    => $AccountSetting->activities,
                'm_coa_id_debit' 	=> $AccountSetting->debit,
                'm_coa_id_kredit' 	=> $AccountSetting->credit,
            );
        }
    }
    return $data;
}

// Format Transaction Code

function format_kode_transaksi($type, $query, $bln = NULL, $thn = NULL){
    if ($bln) {
        $bln = $bln;
    } else {
        $bln = date('m');
    }
    $thn = date('y');
    if ($query<>false) {
        foreach ($query as $row) {
            $urut = intval($row->id) + 1;
            $seq = sprintf("%05d",$urut);
        }
    } else {
        $seq = sprintf("%05d",1);
    }
    $kode_baru = $type.$thn.$bln.$seq;
    return $kode_baru;
}

function weeks_in_month($year, $month, $start_day_of_week)
{
  // Total number of days in the given month.
  $num_of_days = date("t", mktime(0,0,0,$month,1,$year));

  // Count the number of times it hits $start_day_of_week.
  $num_of_weeks = 0;
  for($i=1; $i<=$num_of_days; $i++)
  {
    $day_of_week = date('w', mktime(0,0,0,$month,$i,$year));
    if($day_of_week==$start_day_of_week)
      $num_of_weeks++;
  }

  return $num_of_weeks;
}

function countries($default = '')
{
    $countries = '
                                    <option value="Afganistan">Afghanistan</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                    <option value="Andorra">Andorra</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bonaire">Bonaire</option>
                                    <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                    <option value="Brunei">Brunei</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">Burkina Faso</option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Canary Islands">Canary Islands</option>
                                    <option value="Cape Verde">Cape Verde</option>
                                    <option value="Cayman Islands">Cayman Islands</option>
                                    <option value="Central African Republic">Central African Republic</option>
                                    <option value="Chad">Chad</option>
                                    <option value="Channel Islands">Channel Islands</option>
                                    <option value="Chile">Chile</option>
                                    <option value="China">China</option>
                                    <option value="Christmas Island">Christmas Island</option>
                                    <option value="Cocos Island">Cocos Island</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Comoros">Comoros</option>
                                    <option value="Congo">Congo</option>
                                    <option value="Cook Islands">Cook Islands</option>
                                    <option value="Costa Rica">Costa Rica</option>
                                    <option value="Cote DIvoire">Cote DIvoire</option>
                                    <option value="Croatia">Croatia</option>
                                    <option value="Cuba">Cuba</option>
                                    <option value="Curaco">Curacao</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">Czech Republic</option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Djibouti">Djibouti</option>
                                    <option value="Dominica">Dominica</option>
                                    <option value="Dominican Republic">Dominican Republic</option>
                                    <option value="East Timor">East Timor</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">El Salvador</option>
                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                    <option value="Eritrea">Eritrea</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Ethiopia">Ethiopia</option>
                                    <option value="Falkland Islands">Falkland Islands</option>
                                    <option value="Faroe Islands">Faroe Islands</option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="French Guiana">French Guiana</option>
                                    <option value="French Polynesia">French Polynesia</option>
                                    <option value="French Southern Ter">French Southern Ter</option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Gibraltar">Gibraltar</option>
                                    <option value="Great Britain">Great Britain</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Greenland">Greenland</option>
                                    <option value="Grenada">Grenada</option>
                                    <option value="Guadeloupe">Guadeloupe</option>
                                    <option value="Guam">Guam</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guinea">Guinea</option>
                                    <option value="Guyana">Guyana</option>
                                    <option value="Haiti">Haiti</option>
                                    <option value="Hawaii">Hawaii</option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="Iceland">Iceland</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Iran">Iran</option>
                                    <option value="Iraq">Iraq</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Isle of Man">Isle of Man</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakhstan">Kazakhstan</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kiribati">Kiribati</option>
                                    <option value="Korea North">Korea North</option>
                                    <option value="Korea Sout">Korea South</option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                    <option value="Laos">Laos</option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Lesotho">Lesotho</option>
                                    <option value="Liberia">Liberia</option>
                                    <option value="Libya">Libya</option>
                                    <option value="Liechtenstein">Liechtenstein</option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">Luxembourg</option>
                                    <option value="Macau">Macau</option>
                                    <option value="Macedonia">Macedonia</option>
                                    <option value="Madagascar">Madagascar</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Malawi">Malawi</option>
                                    <option value="Maldives">Maldives</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Marshall Islands">Marshall Islands</option>
                                    <option value="Martinique">Martinique</option>
                                    <option value="Mauritania">Mauritania</option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mayotte">Mayotte</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Midway Islands">Midway Islands</option>
                                    <option value="Moldova">Moldova</option>
                                    <option value="Monaco">Monaco</option>
                                    <option value="Mongolia">Mongolia</option>
                                    <option value="Montserrat">Montserrat</option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Mozambique">Mozambique</option>
                                    <option value="Myanmar">Myanmar</option>
                                    <option value="Nambia">Nambia</option>
                                    <option value="Nauru">Nauru</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherland Antilles">Netherland Antilles</option>
                                    <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                    <option value="Nevis">Nevis</option>
                                    <option value="New Caledonia">New Caledonia</option>
                                    <option value="New Zealand">New Zealand</option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Niue">Niue</option>
                                    <option value="Norfolk Island">Norfolk Island</option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Palau Island">Palau Island</option>
                                    <option value="Palestine">Palestine</option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Phillipines">Philippines</option>
                                    <option value="Pitcairn Island">Pitcairn Island</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Puerto Rico">Puerto Rico</option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Republic of Montenegro">Republic of Montenegro</option>
                                    <option value="Republic of Serbia">Republic of Serbia</option>
                                    <option value="Reunion">Reunion</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russia">Russia</option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="St Barthelemy">St Barthelemy</option>
                                    <option value="St Eustatius">St Eustatius</option>
                                    <option value="St Helena">St Helena</option>
                                    <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                    <option value="St Lucia">St Lucia</option>
                                    <option value="St Maarten">St Maarten</option>
                                    <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                                    <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                                    <option value="Saipan">Saipan</option>
                                    <option value="Samoa">Samoa</option>
                                    <option value="Samoa American">Samoa American</option>
                                    <option value="San Marino">San Marino</option>
                                    <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Serbia">Serbia</option>
                                    <option value="Seychelles">Seychelles</option>
                                    <option value="Sierra Leone">Sierra Leone</option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Slovakia">Slovakia</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="Solomon Islands">Solomon Islands</option>
                                    <option value="Somalia">Somalia</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="Spain">Spain</option>
                                    <option value="Sri Lanka">Sri Lanka</option>
                                    <option value="Sudan">Sudan</option>
                                    <option value="Suriname">Suriname</option>
                                    <option value="Swaziland">Swaziland</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">Switzerland</option>
                                    <option value="Syria">Syria</option>
                                    <option value="Tahiti">Tahiti</option>
                                    <option value="Taiwan">Taiwan</option>
                                    <option value="Tajikistan">Tajikistan</option>
                                    <option value="Tanzania">Tanzania</option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Togo">Togo</option>
                                    <option value="Tokelau">Tokelau</option>
                                    <option value="Tonga">Tonga</option>
                                    <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">Turkmenistan</option>
                                    <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                                    <option value="Tuvalu">Tuvalu</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Erimates">United Arab Emirates</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="United States of America">United States of America</option>
                                    <option value="Uraguay">Uruguay</option>
                                    <option value="Uzbekistan">Uzbekistan</option>
                                    <option value="Vanuatu">Vanuatu</option>
                                    <option value="Vatican City State">Vatican City State</option>
                                    <option value="Venezuela">Venezuela</option>
                                    <option value="Vietnam">Vietnam</option>
                                    <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                    <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                    <option value="Wake Island">Wake Island</option>
                                    <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                                    <option value="Yemen">Yemen</option>
                                    <option value="Zaire">Zaire</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                    ';
    $default = '"' . $default . '"';



    $countries = str_replace($default, $default . '  selected="selected"', $countries);

    return $countries;
}


function view_id(){
    $url      = Request::url();
    $url_path = parse_url($url, PHP_URL_PATH);
    $basename = pathinfo($url_path, PATHINFO_BASENAME);

    return $basename;
}


function _info($id){
    $user=Employee::find($id);
    return $user;
}


function _render($template,$data)
{

    foreach($data as $key => $value)
    {
        $template = str_replace('{{'.$key.'}}', $value, $template);
    }

    return $template;
}

function _raid($l){
    return substr(str_shuffle(str_repeat('0123456789',$l)),0,$l);
}

function timezoneList()
{
    $timezoneIdentifiers = DateTimeZone::listIdentifiers();
    $utcTime = new DateTime('now', new DateTimeZone('UTC'));

    $tempTimezones = array();
    foreach ($timezoneIdentifiers as $timezoneIdentifier) {
        $currentTimezone = new DateTimeZone($timezoneIdentifier);

        $tempTimezones[] = array(
            'offset' => (int)$currentTimezone->getOffset($utcTime),
            'identifier' => $timezoneIdentifier
        );
    }
    usort($tempTimezones, function($a, $b) {
        return ($a['offset'] == $b['offset'])
            ? strcmp($a['identifier'], $b['identifier'])
            : $a['offset'] - $b['offset'];
    });

    $timezoneList = array();
    foreach ($tempTimezones as $tz) {
        $sign = ($tz['offset'] > 0) ? '+' : '-';
        $offset = gmdate('H:i', abs($tz['offset']));
        $timezoneList[$tz['identifier']] = '(UTC ' . $sign . $offset . ') ' .
            $tz['identifier'];
    }

    return $timezoneList;
}

function yearDropdown($startYear, $endYear, $id="year",$selected=""){
    //start the select tag
    echo "<select class='form-control selectpicker' data-live-search='true'  name=".$id.">";

    //echo each year as an option
    for ($i=$endYear;$i>=$startYear;$i--){
        if($i==$selected){
            $selected_year='selected';
        }else{
            $selected_year='';
        }
        echo "<option value=".$i." ".$selected_year." >".$i."</option>";
    }

    //close the select tag
    echo "</select>";
}


function latest_five_leave_application(){
    return Leave::where('status','pending')->orderBy('created_at','desc')->take(5)->get();
}

function latest_five_tasks(){
    return Task::where('status','pending')->orderBy('created_at','desc')->take(5)->get();
}

function latest_five_tickets(){
    return SupportTickets::where('status','Pending')->orderBy('created_at','desc')->take(5)->get();
}

function latest_five_events(){
    $events = Calendar::where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->orderBy('created_at','desc')->get();
    // var_dump($events); exit;
    return $events;
}
function notif_company($tag,$tag_id){
    $comp = Company::find(\Auth::user()->company);
    $notif_company = $comp->company;
    if($tag=='bkm'){
        $bkm = AccountingBKM::find($tag_id);
        $notif_company = $bkm->project_info->company_name->company;
    }  if($tag=='bkp'){
        $bkp = AccountingBKP::find($tag_id);
        $notif_company = $bkp->project_info->company_name->company;
    }  if($tag=='appointments'){
        $appointment = Appointment::find($tag_id);
        $notif_company = $appointment->project_info->company_name->company;
    }  if($tag=='projects'){
        $projects = Project::find($tag_id);
        $notif_company = $projects->company_name->company;
    }  if($tag=='employees'){
        $employees = Employee::find($tag_id);
        $notif_company = $employees->company_name->company;
    }  if($tag=='contracts'){
        $contracts = Contract::find($tag_id);
        $notif_company = $contracts->company_info->company;
        // echo $notif_company; exit;
    }  if($tag=='endcontracts'){
        $endcontracts = Endcontract::find($tag_id);
        $notif_company = $endcontracts->company_info->company;
    }  if($tag=='mutations'){
        $mutations = Mutation::find($tag_id);
        $notif_company = $mutations->company_info->company;
    }  if($tag=='overtime/warrants'){
        $overtime = OvertimeWarrant::find($tag_id);
        $notif_company = $overtime->employee_info->company_name->company;
    }  if($tag=='payments'){
        $payments = Payment::find($tag_id);
        $notif_company = $payments->company_info->company;
    }  if($tag=='terminations'){
        $terminations = Phk::find($tag_id);
        $notif_company = $terminations->company_info->company;
    }  if($tag=='procurements'){
        $procurements = Procurement::find($tag_id);
        $notif_company = $procurements->company_info->company;
    }  if($tag=='expense'){
        $expense = Expense::find($tag_id);
        $notif_company = $expense->company_info->company;
    }  if($tag=='resigns'){
        $resigns = Resign::find($tag_id);
        $notif_company = $resigns->company_info->company;
    }  if($tag=='surat_pengantar'){
        $surat_pengantar = SuratPengantar::find($tag_id);
        $notif_company = $surat_pengantar->company_info->company;
    }  if($tag=='warnings'){
        $warnings = Warning::find($tag_id);
        $notif_company = $warnings->company_info->company;
    } 
    return $notif_company;
}

function notifications(){
    $role_id= \Auth::user()->role_id;
    $whereString ="(route in (select sys_menu.url from sys_menu JOIN sys_employee_roles_permission ON sys_menu.id = sys_employee_roles_permission.perm_id WHERE sys_employee_roles_permission.role_id= ".$role_id." AND sys_employee_roles_permission.R =1) OR route='public/calendar')";
    $notifs = Notification::whereRaw($whereString)->where('show_date',date('Y-m-d'))->orderBy('id','desc')->get();
    // var_dump($notifs);exit;
    return $notifs;
}


function latest_five_employee_leave_application($employee_id=''){
    return Leave::where('status','pending')->where('emp_id',$employee_id)->orderBy('created_at','desc')->take(5)->get();
}

function latest_five_employee_tickets($employee_id=''){
    return SupportTickets::where('status','Pending')->where('emp_id',$employee_id)->orderBy('created_at','desc')->take(5)->get();
}


function get_date_format($date){

    if ($date==''){
        return false;
    }

    $tanggal = date(app_config('DateFormat'),strtotime($date));

	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode(' ', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
    // return $pecahkan[0] . ' ' . $bulan[ (int)date('m',strtotime($pecahkan[1])) ] . ' ' . $pecahkan[2];
    return $tanggal;
}
function get_date_format_indonesia($date){

    if ($date==''){
        return false;
    }

    $tanggal = date('d F Y',strtotime($date));

	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode(' ', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
    return $pecahkan[0] . ' ' . $bulan[ (int)date('m',strtotime($pecahkan[1])) ] . ' ' . $pecahkan[2];
    // return $tanggal;
}

function get_date_day($date){

    if ($date==''){
        return false;
    }

    $day = date('l',strtotime($date));

	$hari = array (
		'Sunday' =>   'Minggu',
		'Monday' =>   'Senin',
		'Tuesday' =>   'Selasa',
		'Wednesday' =>   'Rabu',
		'Thursday' =>   'Kamis',
		'Friday' =>   'Jumat',
		'Saturday' =>   'Sabtu',
	);
	$day = $hari[$day];
	
    return $day;
}


function get_date_format_angka($date){

    if ($date==''){
        return false;
    }

	// $pecahkan = explode('-', $date);
	
	// variabel pecahkan 0 = tahun
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tanggal
 
    // return $pecahkan[2] . '/' . $pecahkan[1] . '/' . $pecahkan[0];
    
    $tanggal = date(app_config('DateFormat'),strtotime($date));
    return $tanggal;
}


function get_date_format_inggris($date){
    if ($date==''){
        return false;
    }

    // echo $date;
    $tmpdue = explode(' ',$date);
    if(sizeof($tmpdue)==3){
        if($tmpdue[1] == 'Januari'){
            $tmpdue[1] = 'January';
        } if($tmpdue[1] == 'Februari'){
            $tmpdue[1] = 'February';
        } if($tmpdue[1] == 'Maret'){
            $tmpdue[1] = 'March';
        } if($tmpdue[1] == 'April'){
            $tmpdue[1] = 'April';
        } if($tmpdue[1] == 'Mei'){
            $tmpdue[1] = 'May';
        } if($tmpdue[1] == 'Juni'){
            $tmpdue[1] = 'June';
        } if($tmpdue[1] == 'Juli'){
            $tmpdue[1] = 'July';
        } if($tmpdue[1] == 'Agustus'){
            $tmpdue[1] = 'August';
        } if($tmpdue[1] == 'September'){
            $tmpdue[1] = 'September';
        } if($tmpdue[1] == 'Oktober'){
            $tmpdue[1] = 'October';
        } if($tmpdue[1] == 'November'){
            $tmpdue[1] = 'November';
        } if($tmpdue[1] == 'Desember'){
            $tmpdue[1] = 'December';
        }
        $date = $tmpdue[0].' '.$tmpdue[1].' '.$tmpdue[2];
        $date = date('Y-m-d',strtotime($date));
    } else {
        $tmpdue = explode('/',$date);
        if(sizeof($tmpdue)==3){
            $date = $tmpdue[2].'-'.$tmpdue[1].'-'.$tmpdue[0];
        } else {
            $date = date_format(date_create($date),"Y-m-d");
        }
    }
    // echo $date; exit;
	
	// variabel tmpdue 0 = tanggal
	// variabel tmpdue 1 = bulan
    // variabel tmpdue 2 = tahun
    
 
    return $date;
}


function get_date_format_inggris_bulan($date){

    if ($date==''){
        return false;
    }

    $tmpdue = explode(' ',$date);
    if(sizeof($tmpdue)==2){
        if($tmpdue[0] == 'Januari'){
            $tmpdue[0] = 'January';
        } if($tmpdue[0] == 'Februari'){
            $tmpdue[0] = 'February';
        } if($tmpdue[0] == 'Maret'){
            $tmpdue[0] = 'March';
        } if($tmpdue[0] == 'April'){
            $tmpdue[0] = 'April';
        } if($tmpdue[0] == 'Mei'){
            $tmpdue[0] = 'May';
        } if($tmpdue[0] == 'Juni'){
            $tmpdue[0] = 'June';
        } if($tmpdue[0] == 'Juli'){
            $tmpdue[0] = 'July';
        } if($tmpdue[0] == 'Agustus'){
            $tmpdue[0] = 'August';
        } if($tmpdue[0] == 'September'){
            $tmpdue[0] = 'September';
        } if($tmpdue[0] == 'Oktober'){
            $tmpdue[0] = 'October';
        } if($tmpdue[0] == 'November'){
            $tmpdue[0] = 'November';
        } if($tmpdue[0] == 'Desember'){
            $tmpdue[0] = 'December';
        }
        $date = $tmpdue[0].' '.$tmpdue[1];
    }
	
	// variabel tmpdue 1 = bulan
    // variabel tmpdue 2 = tahun
    
    $date = date('Y-m',strtotime($date));
 
    return $date;
}

function get_roman_letters($integer){
    
    // Convert the integer into an integer (just to make sure)
    $integer = intval($integer);
    $result = '';
    
    // Create a lookup array that contains all of the Roman numerals.
    $lookup = array('M' => 1000,
    'CM' => 900,
    'D' => 500,
    'CD' => 400,
    'C' => 100,
    'XC' => 90,
    'L' => 50,
    'XL' => 40,
    'X' => 10,
    'IX' => 9,
    'V' => 5,
    'IV' => 4,
    'I' => 1);
    
    foreach($lookup as $roman => $value){
    // Determine the number of matches
    $matches = intval($integer/$value);
    
    // Add the same number of characters to the string
    $result .= str_repeat($roman,$matches);
    
    // Set the integer to be the remainder of the integer and the value
    $integer = $integer % $value;
    }
    
    // The Roman numeral should be built, return it
    return $result;
    
}

function get_time_format($date){

    if ($date==''){
        return false;
    }

    return date('g:i A',strtotime($date));
}

function get_date_year($date){
    
    if ($date==''){
        return false;
    }

    return date('Y',strtotime($date));
}

function get_date_month($date){
    
    if ($date==''){
        return false;
    }

    $tanggal = date(app_config('DateFormat'),strtotime($date));

	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode(' ', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
    return $bulan[ (int)date('m',strtotime($pecahkan[1])) ] . ' ' . $pecahkan[2];
}

function getTableColumns($table){
    return Schema::getColumnListing($table);
}

function get_language(){
    return Language::where('status','Active')->get();
}

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}



/*For Permission*/

function permission ($role_id,$perm_id) {

    $permcheck = \App\EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', $perm_id)->first();

    if ($permcheck==NULL){
        return false;
    }else{
        if (!$permcheck->perm_id>0){
            return false;
        }
        return true;
    }

}