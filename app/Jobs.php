<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $table = 'sys_jobs';
    protected $fillable = ['position','project','company','no_position','job_type','employee_type','job_type','experience','age','job_location','salary_range','short_description','post_date','apply_date','close_date','status','description','quota'];

    /* position  Function Start Here */
    public function position_name()
    {
        return $this->hasOne('App\Designation','id','position');
    }

    /* project  Function Start Here */
    public function project_name()
    {
        return $this->hasOne('App\Project','id','project');
    }

    /* company  Function Start Here */
    public function company_name()
    {
        return $this->hasOne('App\Company','id','company');
    }

    /* last education  Function Start Here */
    public function last_education_info()
    {
        return $this->hasOne('App\LastEducation','id','last_education');
    }


}
