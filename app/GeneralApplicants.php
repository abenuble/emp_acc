<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralApplicants extends Model
{
    protected $table = 'sys_general_applicant';

    
    /* last education  Function Start Here */
    public function last_education_info()
    {
        return $this->hasOne('App\LastEducation','id','education');
    }
    public function designation_info()
    {
        return $this->hasOne('App\Designation','id','designation');
    }

}
