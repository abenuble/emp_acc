<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProfileStructure extends Model
{
    protected $table = 'sys_company_profile_organizational_structure';
}
