<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalDocument extends Model
{
    protected $table='sys_legal_documents';
}
