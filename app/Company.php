<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'sys_companies';
    protected $fillable = ['id_company', 'company','address', 'phone_number'];
}
