<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coa extends Model
{
    protected $table = 'sys_coa';

    /* company_info  Function Start Here */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','company');
    }
    
    /* header_info  Function Start Here */
    public function header_info()
    {
        return $this->hasOne('App\Coa','id','header');
    }
    
    /* sub_header_info  Function Start Here */
    public function sub_header_info()
    {
        return $this->hasOne('App\Coa','id','sub_header');
    }
}
