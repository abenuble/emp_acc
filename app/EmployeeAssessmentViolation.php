<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAssessmentViolation extends Model
{
    protected $table='sys_employee_assessment_violations';

}
