<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeBankAccount extends Model
{
    protected $table='sys_employee_bank_accounts';
    protected $fillable=['emp_id','bank_name','branch_name','account_name','account_number','tax_status','ifsc_code','pan_no','npwp_status','npwp_number','employee_bpjs','health_bpjs','maps_number','maps_tmt','user_cost_center_function','user_function_name','user_kbo_function','kbo_name'];

    /* employee  Function Start Here */
    public function employee_name()
    {
        return $this->hasOne('App\Employee','employee_code','emp_id');
    }
}
