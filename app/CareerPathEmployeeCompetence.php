<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerPathEmployeeCompetence extends Model
{
    protected $table = 'sys_career_path_employee_competence';

    /* career_path_info Function Start Here */
    public function career_path_info()
    {
        return $this->hasOne('App\CareerPathEmployee','id','career_path');
    }

}
