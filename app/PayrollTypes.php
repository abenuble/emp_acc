<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollTypes extends Model
{
    protected $table = 'sys_payroll_types';
    
    /* payroll function */
    public function payroll_component()
    {
        return $this->hasMany('App\PayrollComponent','id_payroll_type','id');
    }
    
    /* payroll function */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','company');
    }
    
    /* payroll function */
    public function designation_info()
    {
        return $this->hasOne('App\Designation','id','designation');
    }
}
