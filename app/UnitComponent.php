<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitComponent extends Model
{
    protected $table = 'sys_unit_components';
}
