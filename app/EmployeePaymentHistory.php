<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeePaymentHistory extends Model
{
    protected $table = 'sys_employee_payment_histories';
    /* employee_info function */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','emp_id');
    }
    /* employee_info function */
    public function payroll_info()
    {
        return $this->hasOne('App\PayrollTypes','id','payroll');
    }
}
