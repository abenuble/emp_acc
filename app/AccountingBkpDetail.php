<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingBkpDetail extends Model
{
    protected $table = 'sys_accounting_bkp_details';
    /* bkp_info Start Here */
    public function bkp_info()
    {
        return $this->hasOne('App\AccountingBkp','id','bkp');
    }
    /* coa_info Start Here */
    public function coa_info()
    {
        return $this->hasOne('App\Coa','id','coa');
    }
    /* procurement_info Start Here */
    public function procurement_info()
    {
        return $this->hasOne('App\Procurement','id','procurement');
    }
}
