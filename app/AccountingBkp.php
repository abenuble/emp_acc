<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingBkp extends Model
{
    protected $table = 'sys_accounting_bkps';
    /* project_info Start Here */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','project');
    }
    /* company_info Start Here */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','paid_to');
    }
    /* bank_info Start Here */
    public function bank_info()
    {
        return $this->hasOne('App\Bank','id','expenditure_id');
    }
    /* cash_info Start Here */
    public function cash_info()
    {
        return $this->hasOne('App\Cash','id','expenditure_id');
    }
    /* check_info Start Here */
    public function check_info()
    {
        return $this->hasOne('App\Check','id','expenditure_id');
    }
}
