<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveOutsourceReport extends Model
{
    protected $table='sys_leave_outsource_report';

    /* employee_id  Function Start Here */
    public function employee_id()
    {
        return $this->hasOne('App\Employee','id','emp_id');
    }


}
