<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cash extends Model
{
    protected $table = 'sys_cash';

    /* company_info  Function Start Here */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','company');
    }
    
    /* coa_info  Function Start Here */
    public function coa_info()
    {
        return $this->hasOne('App\Coa','id','coa_id');
    }
    
}
