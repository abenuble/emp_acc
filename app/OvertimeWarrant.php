<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OvertimeWarrant extends Model
{
    protected $table='sys_overtime_warrant';

    /* employee_id  Function Start Here */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','emp_id');
    }


}