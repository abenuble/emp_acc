<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mutation extends Model
{
    protected $table = 'sys_mutations';

    /* project_info Start Here */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','project_number');
    }

    /* company_info Start Here */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','company_name');
    }

    /* company_info Start Here */
    public function company_info_new()
    {
        return $this->hasOne('App\Company','id','company_code_new');
    }

    /* employee_info Start Here */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','employee_name');
    }
    
    /* designation_info Start Here */
    public function designation_old_info()
    {
        return $this->hasOne('App\Designation','id','designation_old');
    }

    /* designation_info Start Here */
    public function designation_new_info()
    {
        return $this->hasOne('App\Designation','id','designation_new');
    }
    
    /* designation_info Start Here */
    public function payment_old_info()
    {
        return $this->hasOne('App\PayrollTypes','id','payment_type_old');
    }

    /* designation_info Start Here */
    public function payment_new_info()
    {
        return $this->hasOne('App\PayrollTypes','id','payment_type_new');
    }

    /* draft_letter_info Start Here */
    public function draft_letter_info()
    {
        return $this->hasOne('App\EmailTemplate','id','draft_letter');
    }
}
