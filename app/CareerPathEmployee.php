<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerPathEmployee extends Model
{
    protected $table = 'sys_career_path_employee';

    /* department_info Function Start Here */
    public function department_info()
    {
        return $this->hasOne('App\Department','id','department');
    }

    /* designation_info  Function Start Here */
    public function designation_info()
    {
        return $this->hasOne('App\Designation','id','designation');
    }

    /* education_info  Function Start Here */
    public function education_info()
    {
        return $this->hasOne('App\LastEducation','id','education');
    }

    /* career_info  Function Start Here */
    public function career_info()
    {
        return $this->hasOne('App\Designation','career','designation');
    }

}
