<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerPathEmployeeAssessment extends Model
{
    protected $table = 'sys_career_path_employee_assessment';

    /* competence_info  Function Start Here */
    public function competence_info()
    {
        return $this->hasOne('App\CareerPathEmployeeCompetence','id','competence');
    }
    
    /* employee_info  Function Start Here */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','emp_id');
    }

}
