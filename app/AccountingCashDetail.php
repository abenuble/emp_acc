<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingCashDetail extends Model
{
    protected $table = 'sys_accounting_cash_details';
    /* cash_info Start Here */
    public function cash_info()
    {
        return $this->hasOne('App\AccountingCash','id','proof_id');
    }
}
