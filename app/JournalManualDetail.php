<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JournalManualDetail extends Model
{
    protected $table = 'sys_journal_manual_details';
    /* journal_info Start Here */
    public function journal_info()
    {
        return $this->hasOne('App\JournalManual','id','journal_manual');
    }
}
