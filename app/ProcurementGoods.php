<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcurementGoods extends Model
{
    protected $table = 'sys_procurement_goods';

    /* procurement  Function Start Here */
    public function procurement_info()
    {
        return $this->hasOne('App\Procurement','id','procurement');
    }
    public function item_info()
    {
        return $this->hasOne('App\ProjectItem','id','goods');
    }
}
