<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phk extends Model
{
    protected $table = 'sys_phk';
    
    /* project_info Start Here */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','project_number');
    }

    /* company_info Start Here */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','company_name');
    }

    /* employee_info Start Here */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','employee_name');
    }

    /* draft_letter_info Start Here */
    public function draft_letter_info()
    {
        return $this->hasOne('App\EmailTemplate','id','draft_letter');
    }
}
