<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeCutiFiles extends Model
{
    protected $table='sys_employee_cuti_files';

    /* employee  Function Start Here */
    public function employee_name()
    {
        return $this->hasOne('App\Employee','employee_code','emp_id');
    }
}
