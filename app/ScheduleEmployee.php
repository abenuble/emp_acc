<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleEmployee extends Model
{
    protected $table = 'sys_schedule_employee';

    /* employee_info  Function Start Here */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','emp_id');
    }

}
