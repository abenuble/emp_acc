<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectItem extends Model
{
    protected $table = 'sys_project_items';

    /* project Function Start Here */
    public function project_name()
    {
        return $this->hasOne('App\Project','id','id_project');
    }

}
