<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JournalManual extends Model
{
    protected $table = 'sys_journal_manuals';
}
