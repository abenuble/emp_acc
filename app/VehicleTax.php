<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleTax extends Model
{
    protected $table = 'sys_vehicle_taxes';
}
