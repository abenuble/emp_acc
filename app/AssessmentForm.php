<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssessmentForm extends Model
{
    protected $table='sys_assessment';
}
