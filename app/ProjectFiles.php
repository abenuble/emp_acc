<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectFiles extends Model
{
    protected $table='sys_project_files';

    /* project  Function Start Here */
    public function project_name()
    {
        return $this->hasOne('App\Project','id','id_project');
    }
}
