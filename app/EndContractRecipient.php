<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EndContractRecipient extends Model
{
    protected $table = 'sys_end_contract_recipients';

        /* contract_info Start Here */
        public function end_contract_info()
        {
            return $this->hasOne('App\EndContract','id','end_contract_id');
        }
    
        /* project_info Start Here */
        public function project_info()
        {
            return $this->hasOne('App\Project','id','project_number');
        }
    
        /* company_info Start Here */
        public function company_info()
        {
            return $this->hasOne('App\Company','id','company_name');
        }
    
        /* draft_letter_info Start Here */
        public function draft_letter_info()
        {
            return $this->hasOne('App\EmailTemplate','id','draft_letter');
        }

        /* employee_info Start Here */
        public function employee_info()
        {
            return $this->hasOne('App\Employee','id','recipients');
        }
}
