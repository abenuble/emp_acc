<?php
/**
 * User: shamim
 * Date: 10/17/16
 * Time: 11:19 AM
 */

namespace App\Classes;

use App\EmployeeRolesPermission;
use App\Menu;


Class permission {

    public static function permitted ($id,$crud) {

        $menus = Menu::value('id')->toArray();
        $permid = array_search($id, $menus);

        $role = \Auth::user()->role_id;

        $permcheck = EmployeeRolesPermission::where('role_id', $role)->where('perm_id', $permid)->first();
        if($crud == 'create'){
            if ($permcheck->C==1){
                return 'access granted';
            } else{
                return 'access denied';
            }
        } else if($crud == 'read'){
            if ($permcheck->R==1){
                return 'access granted';
            } else{
                return 'access denied';
            }
        } else if($crud == 'update'){
            if ($permcheck->U==1){
                return 'access granted';
            } else{
                return 'access denied';
            }
        } else if($crud == 'delete'){
            if ($permcheck->D==1){
                return 'access granted';
            } else{
                return 'access denied';
            }
        }

    }

}