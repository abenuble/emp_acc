<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuratPengantarRecipient extends Model
{
    protected $table = 'sys_surat_pengantar_recipients';

    /* surat_pengantar_info Start Here */
    public function surat_pengantar_info()
    {
        return $this->hasOne('App\SuratPengantar','id','surat_pengantar');
    }

    /* employee_info Start Here */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','recipients');
    }
}
