<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAssessmentAspect extends Model
{
    protected $table='sys_employee_assessment_aspects';

    /* aspect_info Start Here */
    public function aspect_info()
    {
        return $this->hasOne('App\AssessmentAspect','id','aspect');
    }
}
