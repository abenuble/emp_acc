<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingCheck extends Model
{
    protected $table = 'sys_accounting_checks';
}
