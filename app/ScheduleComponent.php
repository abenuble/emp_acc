<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleComponent extends Model
{
    protected $table = 'sys_schedule_component';

    /* schedule_info  Function Start Here */
    public function schedule_info()
    {
        return $this->hasOne('App\Schedule','id','schedule');
    }

}
