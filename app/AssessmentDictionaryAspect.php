<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssessmentDictionaryAspect extends Model
{
    protected $table = 'sys_assessment_dictionary_aspects';

    /* assessment_info Start Here */
    public function assessment_info()
    {
        return $this->hasOne('App\AssessmentDictionary','id','assessment');
    }
    
}
