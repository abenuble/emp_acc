<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingCheckDetail extends Model
{
    protected $table = 'sys_accounting_check_details';
    /* check_info Start Here */
    public function check_info()
    {
        return $this->hasOne('App\AccountingCheck','id','proof_id');
    }
}
