<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingCash extends Model
{
    protected $table = 'sys_accounting_cashes';
}
