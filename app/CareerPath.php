<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerPath extends Model
{
    protected $table = 'sys_career_path';

    /* division_info Function Start Here */
    public function division_info()
    {
        return $this->hasOne('App\Division','id','position');
    }

    /* education_info  Function Start Here */
    public function education_info()
    {
        return $this->hasOne('App\LastEducation','id','education');
    }

}
