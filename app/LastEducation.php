<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LastEducation extends Model
{
    protected $table = 'sys_last_education';
}
