<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssessmentDictionary extends Model
{
    protected $table = 'sys_assessment_dictionary';
}
