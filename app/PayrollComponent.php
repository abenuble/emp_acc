<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollComponent extends Model
{
    protected $table = 'sys_payroll_components';

    /* Component function */
    public function component_name()
    {
        return $this->hasOne('App\Component','id','id_component');
    }

    /* Component function */
    public function unit_name()
    {
        return $this->hasOne('App\UnitComponent','id','id_unit');
    }

    /* payroll function */
    public function payroll_name()
    {
        return $this->hasOne('App\PayrollTypes','id','id_payroll_type');
    }
}
