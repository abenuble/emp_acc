<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAssessment extends Model
{
    protected $table='sys_employee_assessments';
    
    /* employee_info Start Here */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','emp_id');
    }
    
    /* project_info Start Here */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','project');
    }
    
    /* company_info Start Here */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','company');
    }
}
