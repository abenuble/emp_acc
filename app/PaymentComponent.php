<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentComponent extends Model
{
    protected $table = 'sys_payment_components';
    /* employee_info function */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','emp_id');
    }
    public function payment_info(){
        return $this->hasOne('App\Payment','id','payment');
    }
    public function bank_info()
    {
        return $this->hasOne('App\EmployeeBankAccount','emp_id','emp_id');
    }
}
