<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuratPengantar  extends Model
{
    protected $table = 'sys_surat_pengantar';

    /* project_info Start Here */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','project');
    }

    /* company_info Start Here */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','company');
    }

    /* employee_info Start Here */
    public function designation_info()
    {
        return $this->hasOne('App\Designation','id','designation');
    }

    /* draft_letter_info Start Here */
    public function draft_letter_info()
    {
        return $this->hasOne('App\EmailTemplate','id','draft_letter');
    }

    /* payroll_info Start Here */
    public function job_info()
    {
        return $this->hasOne('App\Jobs','id','job');
    }
}
