<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'sys_project';
    protected $fillable = ['id','company','project','start_date','end_date','value','status','file_1','file_2','file_3'];

    /* company  Function Start Here */
    public function company_name()
    {
        return $this->hasOne('App\Company','id','company');
    }

    /* company  Function Start Here */
    public function internal_company_name()
    {
        return $this->hasOne('App\Company','id','internal_company');
    }

}
