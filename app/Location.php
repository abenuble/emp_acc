<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'sys_location';
    protected $fillable = ['id','location','address'];

}
