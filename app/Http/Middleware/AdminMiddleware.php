<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Log; 
use Activity; 
use App\Employee;  
use App\Project;
use App\Menu;
use App\EmployeeRoles;
use App\EmployeeRolesPermission;
class AdminMiddleware
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if($this->auth->check()){
            if($this->auth->user()->role_id=='0'){
                $this->auth->logout();
                return redirect('/')->with([
                    'message'=>'Invalid Access',
                    'message_important'=>true
                ]);
            }

        }
        $menu = Menu::Where('status','=','y')->orderBy('indexs','ASC')->get();
        $role = EmployeeRoles::where('status','Active')->get();
        foreach($menu as $m){
            foreach($role as $r){
                $permcheck = EmployeeRolesPermission::where('role_id', $r->id)->where('perm_id', $m->id)->first();
                if(empty($permcheck)){
                    $permission = new EmployeeRolesPermission();
                    $permission->role_id = $r->id;
                    $permission->perm_id = $m->id;
                    $permission->C = 1;
                    $permission->R = 1;
                    $permission->U = 1;
                    $permission->D = 1;
                    $permission->save();
                } 
            }
        }

        return $next($request);
    }
}
