<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\permission;
use App\EndContract;
use App\EndContractRecipient;
use App\EndContractConcern;
use App\EndContractCopy;
use App\Company;
use App\Notification;
use App\Project;
use App\EmailTemplate;
use App\Employee;
use App\EmploymentHistory;
use App\ContractRecipient;
use App\EmployeeRolesPermission;
use Illuminate\Support\Facades\Input;
use DB;
use WkPdf;
class EndContractController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* endcontract  Function Start Here */
    public function endcontracts()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 52)->first();

        $comp_id = '';
        $proj_id = '';


        $endcontracts = EndContract::orderBy('id','asc')->get();
        $company = Company::all();
        $project = Project::all();
        $email_template = EmailTemplate::where('table_type','=','sys_end_contracts')->get();
        

        return view('admin.endcontract.endcontracts', compact('pay_id','payroll_types','comp_id','proj_id','endcontracts','employee','company','project','email_template','permcheck'));

    }
    /* getEmployee Function Start Here */
    public function getEmployee(Request $request)
    {
        $project = $request->proj_id;

        $employee = Employee::where('role_id','!=','1')->where('project','=',$project)->where('status','=','active')->get();

        $members_dropdown = array();

        foreach($employee as $emp){
            $members_dropdown[] = array("id" => $emp->id, "text" => $emp->fname . " " . $emp->lname);
        }
        return $members_dropdown;
    }


    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            // echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->where('status','=','opening')->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project_number . '</option>';
            }
        }
    }

    /* deleteEndContract  Function Start Here */
    public function deleteEndContract($id)
    {

        $endcontracts = EndContract::find($id);
        if ($endcontracts) {
            $endcontracts->delete();

            return redirect('endcontracts')->with([
                'message' => 'End Contract Deleted Successfully'
            ]);
        } else {
            return redirect('endcontracts')->with([
                'message' => 'End Contract Not Found',
                'message_important' => true
            ]);
        }

    }

    /* addEndContract  Function Start Here */
    public function addEndContract(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'draft_letter' => 'required',
            'date' => 'required',
            'project_number' => 'required',
            'company_name' => 'required',
            'effective_date' => 'required',
            'concerns' => 'required',
            'copies' => 'required',
            'share_with' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('endcontracts')->withErrors($v->errors());
        }

        $draft_letter = Input::get('draft_letter');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $effective_date = Input::get('effective_date');
        $effective_date = get_date_format_inggris($effective_date);
        $project_number = Input::get('project_number');
        $company_name = Input::get('company_name');
        $share_with = Input::get('share_with');
        $share_with_specific = Input::get('share_with_specific');
        $concerns = Input::get('concerns');
        $copies = Input::get('copies');
        //Penomoran id
        $last_number_id = EndContract::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }


        $endcontract = new EndContract();
        $endcontract->id = $number_id;
        $endcontract->draft_letter = $draft_letter;
        $endcontract->date = $date;
        $endcontract->project_number = $project_number;
        $endcontract->company_name = $company_name;
        $endcontract->effective_date = $effective_date;
        $endcontract->share_with = $share_with;
        $endcontract->status = 'draft';
        $endcontract->save();

        $end_contract_id = $endcontract->id;
        
        $notification = new Notification();
        $notification->id_tag = $end_contract_id;
        $notification->tag = 'endcontracts';
        $notification->title = 'Persetujuan Berakhir Kontrak';
        $notification->description = 'Persetujuan Berakhir Kontrak Pegawai Proyek '.$project_number;
        $notification->show_date = date('Y-m-d');
        $notification->start_date = $effective_date;
        $notification->end_date = $effective_date;
        $notification->route = 'approvals/endcontracts';
        $notification->save();

        $comp = Company::find(\Auth::user()->company);
        $perusahaan = '';
        if($share_with == 'all'){

            $recipients = Employee::where('project','=',$project_number)->where('role_id','!=',1)->get(['id','employee_code'])->toArray();

            $num_elements = 0;
            
            $sql_data = array();
    
            while($num_elements<count($recipients)) {
                //Penomoran surat
                $date = date('d');
                $month = date('m');
                $year = date('Y');
                $last_letter_number = DB::table('sys_end_contract_recipients')->orderBy('id','asc')->value('letter_number');
                if($last_letter_number){
                    $last_number = explode('/', $last_letter_number);
                }
                    else{
                        $last_number = 0;
                    }
              
                // if($date=='01'){
                //     $number = 01;
                // } else{
                //     $number = 1 + $last_number[0];
                // }
                $perusahaan = 'SSS';
                // $employee_code = implode(".", $recipients[$num_elements]['employee_code']);
                $sql_data[] = array(
                    'letter_number' => substr($recipients[$num_elements]['employee_code'],-4)."/REF-".$perusahaan."/".get_roman_letters($month)."/".$year,
                    'project_number' => $project_number,
                    'end_contract_id' => $end_contract_id,
                    'recipients' => $recipients[$num_elements]['id'],
                );
                $num_elements++;
            }
    
            $end_contract_recipient = EndContractRecipient::insert($sql_data);

            $num_elements_concern = 0;
            
            $sql_data_concern = array();
    
            while($num_elements_concern<count($concerns)) {
                $sql_data_concern[] = array(
                    'end_contract_id' => $end_contract_id,
                    'concerns' => $concerns[$num_elements_concern],
                );
                $num_elements_concern++;
            }
    
            $end_contract_concern = EndContractConcern::insert($sql_data_concern);

            $num_elements_copy = 0;
            
            $sql_data_copy = array();
    
            while($num_elements_copy<count($copies)) {
                $sql_data_copy[] = array(
                    'end_contract_id' => $end_contract_id,
                    'copies' => $copies[$num_elements_copy],
                );
                $num_elements_copy++;
            }
    
            $end_contract_copy = EndContractCopy::insert($sql_data_copy);

        } elseif($share_with == 'specific'){
            $recipients = explode(",", $share_with_specific);

            $num_elements = 0;
            
            $sql_data = array();
    
            //Penomoran surat
            $date = date('d');
            $month = date('m');
            $year = date('Y');
            $last_letter_number = DB::table('sys_end_contract_recipients')->orderBy('id','asc')->value('letter_number');
            $last_number = explode('/', $last_letter_number);
            // if($date=='01'){
            //     $number = 01;
            // } else{
            //     $number = 1 + $last_number[0];
            // }
            while($num_elements<count($recipients)) {
                $emp_code = Employee::find($recipients[$num_elements]);
                $sql_data[] = array(
                    'letter_number' => substr($emp_code->employee_code,-4)."/REF-".$perusahaan."/".get_roman_letters($month)."/".$year,
                    'project_number' => $project_number,
                    'end_contract_id' => $end_contract_id,
                    'recipients' => $emp_code->id,
                );
                $num_elements++;
            }
    
            $end_contract_recipient = EndContractRecipient::insert($sql_data);

            $num_elements_concern = 0;
            
            $sql_data_concern = array();
    
            while($num_elements_concern<count($concerns)) {
                $sql_data_concern[] = array(
                    'end_contract_id' => $end_contract_id,
                    'concerns' => $concerns[$num_elements_concern],
                );
                $num_elements_concern++;
            }
    
            $end_contract_concern = EndContractConcern::insert($sql_data_concern);

            $num_elements_copy = 0;
            
            $sql_data_copy = array();
    
            while($num_elements_copy<count($copies)) {
                $sql_data_copy[] = array(
                    'end_contract_id' => $end_contract_id,
                    'copies' => $copies[$num_elements_copy],
                );
                $num_elements_copy++;
            }
    
            $end_contract_copy = EndContractCopy::insert($sql_data_copy);
        }


        if ($endcontract!='') {
            return redirect('endcontracts')->with([
                'message' => 'End Contract Added Successfully'
            ]);

        } else {
            return redirect('endcontracts')->with([
                'message' => 'End Contract Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateEndContract  Function Start Here */
    public function updateEndContract(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'effective_date' => 'required',
            'draft_letter' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('endcontracts')->withErrors($v->errors());
        }
        $endcontract = EndContract::find($cmd);
        $effective_date = Input::get('effective_date');
        $effective_date = get_date_format_inggris($effective_date);
        $draft_letter = Input::get('draft_letter');

        if ($endcontract) {
            $endcontract->effective_date = $effective_date;
            $endcontract->draft_letter = $draft_letter;
            $endcontract->status = 'draft';
            $endcontract->save();
            
            $notification = Notification::where('tag','=','endcontracts')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->start_date = $effective_date;
                $notification->end_date = $effective_date;
                $notification->save();
            }

            return redirect('endcontracts')->with([
                'message' => 'EndContract Updated Successfully'
            ]);

        } else {
            return redirect('endcontracts')->with([
                'message' => 'EndContract Not Found',
                'message_important' => true
            ]);
        }
    }

    /* setEndContractStatus  Function Start Here */
    public function setEndContractStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('endcontracts')->withErrors($v->fails());
        }

        $endcontracts  = EndContract::find($cmd);
        if($endcontracts){
            $endcontracts->status=$request->status;
            $endcontracts->save();

            return redirect('endcontracts')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('endcontracts')->with([
                'message' => 'EndContract not found',
                'message_important'=>true
            ]);
        }

    }

    /* viewEndContract  Function Start Here */
    public function viewEndContract($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 52)->first();
        $endcontracts = EndContract::find($id);
        $recipients = EndContractRecipient::where('end_contract_id','=',$id)->get();
        $concerns = EndContractConcern::where('end_contract_id','=',$id)->get();
        $copies = EndContractCopy::where('end_contract_id','=',$id)->get();
        return view('admin.endcontract.endcontract-view', compact('concerns','copies','recipients','endcontracts','permcheck'));
    }

    /* editEndContract  Function Start Here */
    public function editEndContract($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id',52)->first();
        $endcontracts = EndContract::find($id);
        $recipients = EndContractRecipient::where('end_contract_id','=',$id)->get();
        $email_template = EmailTemplate::where('table_type','=','sys_end_contracts')->get();
        $concerns = EndContractConcern::where('end_contract_id','=',$id)->get();
        $copies = EndContractCopy::where('end_contract_id','=',$id)->get();
        return view('admin.endcontract.endcontract-edit', compact('concerns','copies','email_template','recipients','endcontracts','permcheck'));
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {

        $recipients = EndContractRecipient::find($id);
        $address = $recipients->employee_info->per_address;
        $tgl_msk =$recipients->employee_info->doj;
        $company =$recipients->employee_info->project_name->internal_company_name->company;
        $employee_id = $recipients->recipients;
        $employment     = EmploymentHistory::where('emp_id','=',$employee_id)->get();
        $contract       = ContractRecipient::where('recipients','=',$employee_id)->get();
        $draft_letter = EmailTemplate::find($recipients->end_contract_info->draft_letter);

        $copy = EndContractCopy::where('end_contract_id','=',$recipients->end_contract_id)->get();

        foreach($copy as $c){
            $copies[] = $c->copies;
        }
        $concern = EndContractConcern::where('end_contract_id','=',$recipients->end_contract_id)->get();

        foreach($concern as $g){
            $concerns[] = $g->concerns;
        }

        $letter_number = $recipients->letter_number;
        $date = get_date_format_indonesia($recipients->end_contract_info->date);
        $project_number = $recipients->project_info->project_number;
        $company_name = $recipients->end_contract_info->company_info->company;
        $dol = get_date_format_indonesia($recipients->employee_info->dol);
        $doj = get_date_format_indonesia($recipients->employee_info->doj);
        $designation = $recipients->employee_info->designation_name->designation;
        $employee_name = $recipients->employee_info->fname;
        $effective_date = get_date_format_indonesia($recipients->end_contract_info->effective_date);
        $date_of_birth = $recipients->employee_info->birth_place . ", " .  $recipients->employee_info->dob;

        $type = 'Berakhirnya Kontrak';
        $data = array(
            'letter_number' => $letter_number,
            'date' => $date,
            'designation' => $designation,
            'type' => $type,
            'dol' => $dol,
            'doj' => $doj,
            'project_number' => $project_number,
            'company_name' => $company_name,
            'effective_date' => $effective_date,
            'employee_name' => $employee_name,
            'address' => $address,
            'date_of_birth' => $date_of_birth,
            'concerns' => implode(", ", $concerns),
            'copies' => implode(", ", $copies)
        );

        $template = $draft_letter->message;
        
        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.endcontract.pdf-endcontract', compact('message','letter_number','employment','contract','effective_date','tgl_msk','address','employee_name','company'));

        return $pdf->stream($letter_number.'.pdf');
    }

    /* downloadAllPdf  Function Start Here */
    public function downloadAllPdf($id)
    {
        $endcontracts = EndContract::find($id);
        $recipients = EndContractRecipient::where('end_contract_id','=', $id)->get();
        $draft_letter = EmailTemplate::find($endcontracts->draft_letter);
        $template = $draft_letter->message;

        $copy = EndContractCopy::where('end_contract_id','=',$id)->get();

        foreach($copy as $c){
            $copies[] = $c->copies;
        }
        $concern = EndContractConcern::where('end_contract_id','=',$id)->get();

        foreach($concern as $g){
            $concerns[] = $g->concerns;
        }

        $num_elements = 0;
        
        foreach($recipients as $rec){
            $letter_number[] = $rec->letter_number;
            $date[] = get_date_format_indonesia($rec->end_contract_info->date);
            $project_number[] = $rec->project_info->project_number;
            $company_name[] = $rec->end_contract_info->company_info->company;
            $employee_name[] = $rec->employee_info->fname . " " . $rec->employee_info->lname;
            $effective_date[] = get_date_format_indonesia($rec->end_contract_info->effective_date);
            $address[] = $rec->employee_info->per_address;
            $designation[] = $rec->employee_info->designation_name->designation;
            $location[] = $rec->employee_info->location;
            $date_of_birth[] = $rec->employee_info->birth_place . ", " .  $rec->employee_info->dob;
            $dol[] = get_date_format_indonesia($rec->employee_info->dol);
            $doj[] = get_date_format_indonesia($rec->employee_info->doj);
            $type[] = 'Berakhirnya Kontrak';
            $designation[] = $rec->employee_info->designation_name->designation;
        }

        while($num_elements<count($recipients)) {
            $data[$num_elements] = array(
                'letter_number' => $letter_number[$num_elements],
                'date' => $date[$num_elements],
                'designation' => $designation[$num_elements],
                'type' => $type[$num_elements],
                'dol' => $dol[$num_elements],
                'doj' => $doj[$num_elements],
                'project_number' => $project_number[$num_elements],
                'company_name' => $company_name[$num_elements],
                'effective_date' => $effective_date[$num_elements],
                'employee_name' => $employee_name[$num_elements],
                'address' => $address[$num_elements],
                'location' => $location[$num_elements],
                'designation' => $designation[$num_elements],
                'date_of_birth' => $date_of_birth[$num_elements],
                'concerns' => implode(", ", $concerns),
                'copies' => implode(", ", $copies)
            );
            $message[$num_elements] = array(
                'message' => _render($template, $data[$num_elements]));
            $num_elements++;
        }        

        $pdf=WkPdf::loadView('admin.endcontract.pdf-all-endcontract', compact('message','letter_number'));

        return $pdf->stream('endcontracts.pdf');
    }
}
