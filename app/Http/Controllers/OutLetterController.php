<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeRolesPermission;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\OutLetter;
use App\Company;
use DB;
use WkPdf;

class OutLetterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* outLetters  Function Start Here */
    public function outLetters()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 73)->first();
        $outletters = OutLetter::where('category','=','out')->orderBy('id','asc')->get();
        return view('admin.outletter.out-letters', compact('outletters','permcheck'));
    }

    /* manageTemplate  Function Start Here */
    public function manageTemplate($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 73)->first();
        $outletters = OutLetter::find($id);

        if($outletters){
            return view('admin.outletter.out-manage', compact('outletters','permcheck'));
        }else{
            return redirect('outgoing-letter/out-letters')->with([
                'message' => 'Letter Not Found',
                'message_important'=>true
            ]);
        }

    }

    /* updateTemplate  Function Start Here */
    public function updateTemplate(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'subject' => 'required', 'message' => 'required', 'date' => 'required'
        ]);

        $cmd = Input::get('cmd');

        if ($v->fails()) {
            return redirect('outgoing-letter/out-letters/manage/' . $cmd)->withErrors($v->errors());
        }

        $subject = Input::get('subject');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $message = Input::get('message');
        $tembusan=Input::get('tembusan');
        OutLetter::where('id', '=', $cmd)->update([
            'subject' => $subject,
            'tembusan'=>$tembusan,
            'message' => $message,
            'date' => $date,
        ]);

        return redirect('outgoing-letter/out-letters/manage/' . $cmd)->with([
            'message' => 'Letter Update Successfully'
        ]);

    }

    /* addTemplate  Function Start Here */
    public function addTemplate()
    {
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        // $last_letter_number = DB::table('sys_out_letters')->orderBy('id','asc')->value('letter_number');
        // if($last_letter_number){
        //     $last_number = explode('/', $last_letter_number);}
        //     else{ $last_number=0;}
        // if($date=='01'){
        //     $number = 01;
        // } else{
        //     $number = 1 + $last_number[0];
        // }
        $last_letter_number = DB::table('sys_out_letters')->where('letter_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('letter_number');
        if($last_letter_number==''){
            $number = 1;
        } else{
            $last_number = explode('/', $last_letter_number);
            $number = 1 + $last_number[0];
        }
        $company = Company::find(\Auth::user()->company);


        return view('admin.outletter.out-letter-add', compact('month','year','number','company'));
    }

    /* addTemplatePost  Function Start Here */
    public function addTemplatePost(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'subject' => 'required', 'message' => 'required', 'date' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('outgoing-letter/out-letters')->withErrors($v->errors());
        }

        $letter_number = Input::get('letter_number');
        $subject = Input::get('subject');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $message = Input::get('message');
        $tembusan=Input::get('tembusan');

        $outletters = new OutLetter();
        $outletters->letter_number = $letter_number;
        $outletters->subject = $subject;
        $outletters->tembusan = $tembusan;
        $outletters->message = $message;
        $outletters->date = $date;
        $outletters->category = 'out';
        $outletters->save();

        return redirect('outgoing-letter/out-letters')->with([
            'message' => 'Letter Added Successfully'
        ]);

    }
    /* getVariable  Function Start Here */
    public function getVariable(Request $request)
    {

        $variables = array("letter_number", "date", "subject");
        foreach ($variables as $var) {
            echo '{{' . $var . '}}, ' ;
        }
        
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {

        $outletters = OutLetter::find($id);

        $letter_number = $outletters->letter_number;
        $date = get_date_format($outletters->date);
        $subject = $outletters->subject;
        
        $data = array(
            'letter_number' => $letter_number,
            'date' => $date,
            'subject' => $subject,
        );

        $template = $outletters->message;
        
        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.outletter.pdf-out-letter', compact('message','letter_number'));

        return $pdf->stream($letter_number.'.pdf');
    }
}
