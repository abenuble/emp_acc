<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Company;
use App\Department;
use App\Designation;
use App\Employee;
use App\Leave;
use App\Project;
use App\ProjectNeeds;
use App\LeaveOutsourceReport;
use App\LeaveInternalReport;
use App\ScheduleEmployee;
use Illuminate\Http\Request;
use WkPdf;
use DB;
use Excel;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

date_default_timezone_set(app_config('Timezone'));

class AttendanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* report  Function Start Here */
    public function report()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 39)->first();
        $employee_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
        $date_from = '';
        $date_to = '';
        $emp_id = '';
        $dep_id = '';
        $des_id = '';
        
        $attendance_internal_report = LeaveInternalReport::orderBy('id','asc')->get();

        $attendance = Attendance::orderBy('id','asc')->get();
        $employee = Employee::where('status', 'active')->where('role_id','!=','1')->get();
        $department = Department::all();
        return view('admin.attendance.attendance', compact('attendance_internal_report','attendance', 'employee', 'department', 'date_from','date_to', 'emp_id', 'dep_id', 'des_id','permcheck','employee_permission'));
    }

    /* getAllPdfReport  Function Start Here */
    public function getAllPdfReport()
    {
        $attendance = Attendance::where('emp_type','=','Internal')->get();

        $pdf=WkPdf::loadView('admin.attendance.all-pdf-attendance-report', compact('attendance'));

        return $pdf->stream('attendance_internal.pdf');
    }

    /* getAllPdfReportOutsource  Function Start Here */
    public function getAllPdfReportOutsource()
    {
        $attendance = Attendance::where('emp_type','=','Outsourcing')->get();

        $pdf=WkPdf::loadView('admin.attendance.all-pdf-attendance-outsource-report', compact('attendance'));

        return $pdf->stream('attendance_outsourcing.pdf');
    }

    /* postSetOvertime  Function Start Here */
    public function postSetOvertime(Request $request)
    {
        $attend_id=$request->attend_id;
        $overTimeValue=$request->overTimeValue;

        if($attend_id!='' AND $overTimeValue!=''){
            Attendance::where('id', $attend_id)->update(['overtime' => $overTimeValue]);
            return 'success';
        }else{
            return 'failed';
        }
    }


    /* update  Function Start Here */
    public function update()
    {
        $employee = Employee::where('status', 'active')->where('role_id','!=','1')->get();
        return view('admin.attendance.update-attendance', compact('employee'));

    }

    /* postUpdateAttendance  Function Start Here */
    public function postUpdateAttendance(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'employee' => 'required', 'date' => 'required', 'clock_in' => 'required', 'clock_out' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('attendance/update')->withErrors($v->errors());
        }

        $employee = Input::get('employee');
        $date = Input::get('date');
        $date=get_date_format_inggris($date);
        $clock_in = Input::get('clock_in');
        $clock_out = Input::get('clock_out');
        $entry_time=strtotime($clock_in);
        $out_time=strtotime($clock_out);

        $get_attendance_data=$out_time-$entry_time;

        $check_clock=($get_attendance_data > 0 ) ? 1 : ( ( $get_attendance_data < 0 ) ? -1 : 0);

        if ($check_clock==-1){
            return redirect('attendance/update')->with([
                'message'=>language_data('Insert your time perfectly'),
                'message_important'=>true
            ]);
        }
        $schedule_employee = ScheduleEmployee::where('emp_id','=',$employee)->where('date','=',$date)->first();

        if(!empty($schedule_employee)){
            $office_in_time = $schedule_employee->starts_at;
            $office_out_time = $schedule_employee->ends_at;
        } else {
            $office_in_time = app_config('OfficeInTime');
            $office_out_time = app_config('OfficeOutTime');   
        }

        $late_1 = strtotime($office_in_time);
        $late_2 = strtotime($clock_in);
        $late = ($late_2 - $late_1);

        if ($late < 0) {
            $late = 0;
        }
        $late = $late / 60;


        $early_leave_1 = strtotime($clock_out);

        $early_leave_2 = strtotime($office_out_time);
        $early_leave = ($early_leave_2 - $early_leave_1);

        if ($early_leave < 0) {
            $early_leave = 0;
        }
        $early_leave = $early_leave / 60;

        $office_hour = (strtotime($clock_out) - strtotime($clock_in)) / 60;
        $total = $office_hour - $early_leave;


        $emp_info = Employee::find($employee);
        $designation = $emp_info->designation;
        $emp_type = $emp_info->employee_type;
        $department = $emp_info->department;
        $project = $emp_info->project;
        $location = $emp_info->location;


        $attendance = Attendance::where('emp_id', $employee)->where('date', $date)->first();

        if ($attendance) {
            $attendance->clock_in = $clock_in;
            $attendance->clock_out = $clock_out;
            $attendance->late = $late;
            $attendance->early_leaving = $early_leave;
            $attendance->overtime= '0';
            $attendance->total= $total;
            $attendance->status = 'Present';
            $attendance->save();
            if($late>15){
                $attendance->presence_status = 'late';
                $attendance->save();
            } else{
                $attendance->presence_status = 'ontime';
                $attendance->save();
            }

        } else {
            $adance = new Attendance();
            $adance->emp_id = $employee;
            $adance->emp_type = $emp_type;
            $adance->designation = $designation;
            $adance->department = $department;
            $adance->date = $date;
            $adance->month = date('Y-m',strtotime($date));
            $adance->clock_in = $clock_in;
            $adance->clock_out = $clock_out;
            $adance->late = $late;
            $adance->early_leaving = $early_leave;
            $adance->overtime= '0';
            $adance->total = $total;
            $adance->status = 'Present';
            $adance->save();
            if($late>15){
                $adance->presence_status = 'late';
                $adance->save();
            } else{
                $adance->presence_status = 'ontime';
                $adance->save();
            }

        }

        $internalReport = LeaveInternalReport::where('emp_id', $employee)->first();
        $at_total = Attendance::where('emp_id','=', $employee)->where('status','=','Present')->count();
        $lv_total = Attendance::where('emp_id','=', $employee)->where('status','=','Absent')->count();
        $ov_total = Attendance::where('emp_id','=', $employee)->sum('overtime');

        if($internalReport){
            $internalReport->total_attendance = $at_total;
            $internalReport->total_leave = $lv_total;
            $internalReport->total_overtime = $ov_total;
            $internalReport->save();
        } else {
            $entryreport = new LeaveInternalReport();
            $entryreport->emp_id = $employee;
            $entryreport->total_attendance = $at_total;
            $entryreport->total_leave = $lv_total;
            $entryreport->total_overtime = $ov_total;
            $entryreport->designation = $designation;
            $entryreport->department = $department;
            $entryreport->project = $project;
            $entryreport->location = $location;
            $entryreport->save();
        }

        return redirect('attendance/report')->with([
            'message' => language_data('Attendance Updated Successfully')
        ]);

    }


    /* getDesignation  Function Start Here */
    public function getDesignation(Request $request)
    {

        $dep_id = $request->dep_id;
        if ($dep_id) {
            echo '<option value="0">Select Designation</option>';
            $designation = Designation::where('did', $dep_id)->get();
            foreach ($designation as $d) {
                echo '<option value="' . $d->id . '">' . $d->designation . '</option>';
            }
        }
    }

    /* postCustomSearch  Function Start Here */
    public function postCustomSearch(Request $request)
    {

        $emp_id = Input::get('employee');
        $dep_id = Input::get('department');
        $des_id = Input::get('designation');

        $designation = Designation::where('did',$dep_id)->get();

        if ($emp_id!=0) {
            $attendance_query = LeaveInternalReport::where('emp_id', $emp_id);
        }

        if ($dep_id!=0 && $emp_id!=0) {
            $attendance_query->Where('department', $dep_id);
        } else {
            $attendance_query = LeaveInternalReport::where('department', $dep_id);
        }

        if ($des_id!=0) {
            $attendance_query->Where('designation', $des_id);
        }

        $attendance_internal_report = $attendance_query->get();


        $employee = Employee::where('status', 'active')->get();
        $department = Department::all();
        return view('admin.attendance.attendance', compact('attendance_internal_report', 'employee', 'department', 'date_to','date_from', 'emp_id', 'dep_id', 'des_id','designation'));

    }
    /* getPdfReport  Function Start Here */
    public function getPdfReport($date,$emp_id=0,$dep_id=0,$des_id=0)
    {

        $date=explode('_',$date);

        if (is_array($date)){
            $date_from=$date['0'];
            $date_to=$date['1'];
        }else{
            return redirect('attendance/report')->with([
                'message' => 'Invalid Date',
                'message_important' => true
            ]);
        }

        $date_from=get_date_format_inggris($date_from);
        $date_to=get_date_format_inggris($date_to);
        $attendance_query = Attendance::whereBetween('date', [$date_from,$date_to]);

        if ($emp_id) {
            $attendance_query->Where('emp_id', $emp_id);
        }

        if ($dep_id) {
            $attendance_query->Where('department', $dep_id);
        }

        if ($des_id) {
            $attendance_query->Where('designation', $des_id);
        }

        $attendance = $attendance_query->get();

        $pdf=\WkPdf::loadView('admin.attendance.all-pdf-attendance-report', compact('attendance'));

        return $pdf->stream('attendance.pdf');

    }

    /* editAttendance  Function Start Here */
    public function editAttendance($id)
    {

        $attendance = Attendance::find($id);
        if ($attendance) {
            return view('admin.attendance.edit-attendance', compact('attendance'));
        } else {
            return redirect('attendance/report')->with([
                'message' => language_data('Attendance Info Not Found'),
                'message_important' => true
            ]);
        }
    }

    /* postEditAttendance  Function Start Here */
    public function postEditAttendance(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'clock_in' => 'required', 'clock_out' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('attendance/edit/' . $cmd)->withErrors($v->errors());
        }

        $clock_in = Input::get('clock_in');
        $clock_out = Input::get('clock_out');

        $entry_time=strtotime($clock_in);
        $out_time=strtotime($clock_out);

        $get_attendance_data=$out_time-$entry_time;

        $check_clock=($get_attendance_data > 0 ) ? 1 : ( ( $get_attendance_data < 0 ) ? -1 : 0);

        if ($check_clock==-1){
            return redirect('attendance/edit/' . $cmd)->with([
                'message'=>language_data('Insert your time perfectly'),
                'message_important'=>true
            ]);
        }
        $attendance = Attendance::find($cmd);

        $schedule_employee = ScheduleEmployee::where('emp_id','=',$attendance->emp_id)->where('date','=',$attendance->date)->first();

        if(!empty($schedule_employee)){
            $office_in_time = $schedule_employee->starts_at;
            $office_out_time = $schedule_employee->ends_at;
        } else {
            $office_in_time = app_config('OfficeInTime');
            $office_out_time = app_config('OfficeOutTime');   
        }

        $check_out_entry=strtotime($office_in_time);
        $check_out_data=$out_time-$check_out_entry;


        $check_clock=($check_out_data > 0 ) ? 1 : ( ( $check_out_data < 0 ) ? -1 : 0);

        if ($check_clock==-1){
            return redirect('attendance/edit/' . $cmd)->with([
                'message'=>language_data('Office time: In Time').$office_in_time.' '.language_data('and Out Time').' '.$office_out_time,
                'message_important'=>true
            ]);
        }


        $late_1 = strtotime($office_in_time);
        $late_2 = strtotime($clock_in);
        $late = ($late_2 - $late_1);

        if ($late < 0) {
            $late = 0;
        }
        $late = $late / 60;


        $early_leave_1 = strtotime($clock_out);
        $early_leave_2 = strtotime($office_out_time);
        $early_leave = ($early_leave_2 - $early_leave_1);

        if ($early_leave < 0) {
            $early_leave = 0;
        }
        $early_leave = $early_leave / 60;

        $office_hour = (strtotime($clock_out) - strtotime($clock_in)) / 60;
        $total = $office_hour - $early_leave;

        if ($attendance) {
            $attendance->clock_in = $clock_in;
            $attendance->clock_out = $clock_out;
            $attendance->late = $late;
            $attendance->early_leaving = $early_leave;
            $attendance->total = $total;
            $attendance->status = 'Present';
            $attendance->save();

            if($late>15){
                $attendance->presence_status = 'late';
                $attendance->save();
            } else{
                $attendance->presence_status = 'ontime';
                $attendance->save();
            }

            return redirect('attendance/report')->with([
                'message' => language_data('Attendance Update Successfully')
            ]);

        } else {
            return redirect('attendance/report')->with([
                'message' => language_data('Attendance Info Not Found'),
                'message_important' => true
            ]);
        }


    }

     /* overtimeAttendance  Function Start Here */
     public function overtimeAttendance($id)
     {
         $attendance = Attendance::find($id);
         if ($attendance) {
             return view('admin.attendance.overtime-attendance', compact('attendance'));
         } else {
             return redirect('attendance/report')->with([
                 'message' => language_data('Attendance Info Not Found'),
                 'message_important' => true
             ]);
         }
     }
 
     /* postOvertimeAttendance  Function Start Here */
     public function postOvertimeAttendance(Request $request)
     {
         $cmd = Input::get('cmd');
         $v = \Validator::make($request->all(), [
             'overtime' => 'required'
         ]);
 
         if ($v->fails()) {
             return redirect('attendance/overtime/' . $cmd)->withErrors($v->errors());
         }
 
         $overtime = Input::get('overtime');
         $overtime_information = Input::get('overtime_information'); 
         $overtime_status = Input::get('overtime_status'); 
 
         $attendance = Attendance::find($cmd);
 
         if ($attendance) {
             $attendance->overtime = $overtime;
             $attendance->overtime_information = $overtime_information;
             $attendance->overtime_status = $overtime_status;
             $attendance->save();

             $internalReport = LeaveInternalReport::where('emp_id', $emp_id)->first();
             $ov_total = Attendance::where('emp_id','=', $emp_id)->sum('overtime');
     
             if($internalReport){
                 $internalReport->total_overtime = $ov_total;
                 $internalReport->save();
             } 
 
             return redirect('attendance/report')->with([
                 'message' => language_data('Attendance Update Successfully')
             ]);
 
         } else {
             return redirect('attendance/report')->with([
                 'message' => language_data('Attendance Info Not Found'),
                 'message_important' => true
             ]);
         }
 
 
     }

     /* overtimeOutsourceAttendance  Function Start Here */
     public function overtimeOutsourceAttendance($id)
     {
         $attendance = Attendance::find($id);
         if ($attendance) {
             return view('admin.attendance.outsource-overtime-attendance', compact('attendance'));
         } else {
             return redirect('attendance/outsource-report')->with([
                 'message' => language_data('Attendance Info Not Found'),
                 'message_important' => true
             ]);
         }
     }
 
     /* postOvertimeOutsourceAttendance  Function Start Here */
     public function postOvertimeOutsourceAttendance(Request $request)
     {
         $cmd = Input::get('cmd');
         $v = \Validator::make($request->all(), [
             'overtime' => 'required'
         ]);
 
         if ($v->fails()) {
             return redirect('attendance/overtime-outsource/' . $cmd)->withErrors($v->errors());
         }

         $date = Input::get('date');
         $date = get_date_format_inggris($date);

         $emp_id = Input::get('emp_id');
 
         $overtime = Input::get('overtime');
         $overtime_information = Input::get('overtime_information'); 
         $overtime_status = 'Accepted'; 
 
         $attendance = Attendance::find($cmd);
 
         if ($attendance) {
             $attendance->overtime = $overtime;
             $attendance->overtime_information = $overtime_information;
             $attendance->overtime_status = $overtime_status;
             $attendance->save();

             $outsourceReport = LeaveOutsourceReport::where('emp_id', $emp_id)->first();
             $ov_total = Attendance::where('emp_id','=', $emp_id)->sum('overtime');
     
             if($outsourceReport){
                 $outsourceReport->total_overtime = $ov_total;
                 $outsourceReport->save();
             } 
 
             return redirect('attendance/outsource-report')->with([
                 'message' => language_data('Attendance Update Successfully')
             ]);
 
         } else {
             return redirect('attendance/outsource-report')->with([
                 'message' => language_data('Attendance Info Not Found'),
                 'message_important' => true
             ]);
         }
 
 
     }

    /* deleteAttendance  Function Start Here */
    public function deleteAttendance($id)
    {
        $attendance=Attendance::find($id);
        if($attendance){
            $attendance->delete();
            return redirect('attendance/report')->with([
                'message' => language_data('Attendance Deleted Successfully')
            ]);
        }else{
            return redirect('attendance/report')->with([
                'message' => language_data('Attendance Info Not Found'),
                'message_important' => true
            ]);
        }
    }

    /* reportOutsource  Function Start Here */
    public function reportOutsource()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 40)->first();
        $employee_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
        $month = '';
        $date_to = '';
        $emp_id = '';
        $comp_id = '';
        $proj_id = '';
        $dep_id = '';
        $des_id = '';

        $attendance_outsource_report = LeaveOutsourceReport::orderBy('id','asc')->get();

        $department = Department::all();
        $designation = Designation::all();
        $project = Project::all();
        $company = Company::all();
        return view('admin.attendance.outsource-attendance', compact('company','attendance_outsource_report','designation','project', 'employee', 'department', 'month','date_to', 'emp_id', 'dep_id', 'des_id', 'comp_id', 'proj_id','permcheck','employee_permission'));
    }

    /* postOutsourceCustomSearch  Function Start Here */
    public function postOutsourceCustomSearch(Request $request)
    {
        $v = \Validator::make($request->all(), [
        ]);

        if ($v->fails()) {
            return redirect('attendance/outsource-report')->withErrors($v->errors());
        }

        $proj_id = Input::get('project');
        $comp_id = Input::get('company');
        $dep_id = Input::get('department');
        $des_id = Input::get('designation');
        $location = Input::get('location');

        $designation = Designation::where('did',$dep_id)->get();
        $projects = Project::find($proj_id);

        $attendance_query = LeaveOutsourceReport::where('project', $proj_id);

        if ($dep_id) {
            $attendance_query->Where('department', $dep_id);
        }

        if ($des_id) {
            $attendance_query->Where('designation', $des_id);
        }

        $attendance_outsource_report = $attendance_query->get();


        $employee = Employee::where('status', 'active')->get();
        $department = Department::all();
        $company = Company::all();
        $project = Project::all();
        $projectneeds = ProjectNeeds::all();
        return view('admin.attendance.outsource-attendance', compact('company','location','projectneeds','comp_id','proj_id','attendance_outsource_report', 'employee', 'department', 'proj_id', 'dep_id', 'des_id','designation','projects','project','month'));

    }

    /* getCompany  Function Start Here */
    public function getCompany(Request $request)
    {

        $proj_id = $request->proj_id;
        if ($proj_id) {
            $project = Project::where('id', $proj_id)->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->company . '">' . $d->company_name->company . '</option>';
            }
        }
    }

    /* getLocation  Function Start Here */
    public function getLocation(Request $request)
    {

        $des_id = $request->des_id;
        if ($des_id) {
            echo '<option value="0">Select Location</option>';
            $location = ProjectNeeds::where('id_designation', $des_id)->get();
            foreach ($location as $d) {
                echo '<option value="' . $d->location . '">' . $d->location . '</option>';
            }
        }
    }

    /* updateOutsource  Function Start Here */
    public function updateOutsource()
    {
        $employee = Employee::where('status', 'active')->where('role_id','!=','1')->get();
        return view('admin.attendance.outsource-update-attendance', compact('employee'));

    }

    public function updateOutsourceDetail()
    {

        $id=Input::get('idatt');
        $idem=Input::get('idem');
        $presence=Input::get('presence');

        $informasi=Input::get('informasi');
        $banks = Attendance::find($id);

        if($presence=='on'){
            $presence="Present";
        }
        else{
            $presence="Absent";
        }
        $banks->leave_id="";
        $banks->status = $presence;

        $banks->leave_information = $informasi;

        $banks->save();
       // $employee = Employee::where('status', 'active')->where('role_id','!=','1')->get();
        //return view('admin.attendance.outsource-update-attendance', compact('employee'));
        return redirect('attendance/view-outsource/'.$idem)->with([
            'message' => language_data('Attendance Updated Successfully')
        ]);
    }
    /* postUpdateOutsourceAttendance  Function Start Here */
    public function postUpdateOutsourceAttendance(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'employee' => 'required', 'date' => 'required', 'clock_in' => 'required', 'clock_out' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('attendance/outsource-update')->withErrors($v->errors());
        }

        $employee = Input::get('employee');
        $date = Input::get('date');
        $date=get_date_format_inggris($date);
        $clock_in = Input::get('clock_in');
        $clock_out = Input::get('clock_out');
        $entry_time=strtotime($clock_in);
        $out_time=strtotime($clock_out);

        $get_attendance_data=$out_time-$entry_time;

        $check_clock=($get_attendance_data > 0 ) ? 1 : ( ( $get_attendance_data < 0 ) ? -1 : 0);

        if ($check_clock==-1){
            return redirect('attendance/outsource-update')->with([
                'message'=>language_data('Insert your time perfectly'),
                'message_important'=>true
            ]);
        }
        $schedule_employee = ScheduleEmployee::where('emp_id','=',$employee)->where('date','=',$date)->first();

        if(!empty($schedule_employee)){
            $office_in_time = $schedule_employee->starts_at;
            $office_out_time = $schedule_employee->ends_at;
        } else {
            $office_in_time = app_config('OfficeInTime');
            $office_out_time = app_config('OfficeOutTime');   
        }

        $late_1 = strtotime($office_in_time);
        $late_2 = strtotime($clock_in);
        $late = ($late_2 - $late_1);

        if ($late < 0) {
            $late = 0;
        }
        $late = $late / 60;


        $early_leave_1 = strtotime($clock_out);

        $early_leave_2 = strtotime($office_out_time);
        $early_leave = ($early_leave_2 - $early_leave_1);

        if ($early_leave < 0) {
            $early_leave = 0;
        }
        $early_leave = $early_leave / 60;

        $office_hour = (strtotime($clock_out) - strtotime($clock_in)) / 60;
        $total = $office_hour - $early_leave;


        $emp_info = Employee::find($employee);
        $designation = $emp_info->designation;
        $emp_type = $emp_info->employee_type;
        $department = $emp_info->department;
        $project = $emp_info->project;
        $location = $emp_info->location;


        $attendance = Attendance::where('emp_id', $employee)->where('date', $date)->first();

        if ($attendance) {
            $attendance->clock_in = $clock_in;
            $attendance->clock_out = $clock_out;
            $attendance->late = $late;
            $attendance->early_leaving = $early_leave;
            $attendance->overtime= '0';
            $attendance->total= $total;
            $attendance->status = 'Present';
            $attendance->save();
            if($late>15){
                $attendance->presence_status = 'late';
                $attendance->save();
            } else{
                $attendance->presence_status = 'ontime';
                $attendance->save();
            }

            

        } else {
            $adance = new Attendance();
            $adance->emp_id = $employee;
            $adance->emp_type = $emp_type;
            $adance->designation = $designation;
            $adance->department = $department;
            $adance->month = date('Y-m',strtotime($date));
            $adance->date = $date;
            $adance->clock_in = $clock_in;
            $adance->clock_out = $clock_out;
            $adance->late = $late;
            $adance->early_leaving = $early_leave;
            $adance->overtime= '0';
            $adance->total = $total;
            $adance->status = 'Present';
            $adance->save();
            if($late>15){
                $adance->presence_status = 'late';
                $adance->save();
            } else{
                $adance->presence_status = 'ontime';
                $adance->save();
            }

            

        }

        $outsourceReport = LeaveOutsourceReport::where('emp_id', $employee)->first();
        $at_total = Attendance::where('emp_id','=', $employee)->where('status','=','Present')->count();
        $lv_total = Attendance::where('emp_id','=', $employee)->where('status','=','Absent')->count();
        $ov_total = Attendance::where('emp_id','=', $employee)->sum('overtime');

        if($outsourceReport){
            $outsourceReport->total_attendance = $at_total;
            $outsourceReport->total_leave = $lv_total;
            $outsourceReport->total_overtime = $ov_total;
            $outsourceReport->save();
        } else {
            $entryreport = new LeaveOutsourceReport();
            $entryreport->emp_id = $employee;
            $entryreport->total_attendance = $at_total;
            $entryreport->total_leave = $lv_total;
            $entryreport->total_overtime = $ov_total;
            $entryreport->designation = $designation;
            $entryreport->department = $department;
            $entryreport->project = $project;
            $entryreport->location = $location;
            $entryreport->save();
        }

        return redirect('attendance/outsource-report')->with([
            'message' => language_data('Attendance Updated Successfully')
        ]);

    }

    /* viewInternal  Function Start Here */
    public function viewInternal($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 39)->first();
        $employee_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
        $month = '';
        $date_from = '';
        $date_to = '';
        $emp_id = '';
        $dep_id = '';
        $des_id = '';


        $attendance = Attendance::where('emp_id','=',$id)->get();
        $employee = Employee::find($id);
        $department = Department::all();
        if ($attendance) {
            return view('admin.attendance.view-attendance', compact('department','emp_id','dep_id','des_id','employee','date_from','date_to','month','overtime_total','leave_total','attendance_total','attendance','attendance_id','permcheck','employee_permission'));
        } else {
            return redirect('attendance/outsource-report')->with([
                'message' => language_data('Attendance Info Not Found'),
                'message_important' => true
            ]);
        }
    }

    /* postViewInternalCustomSearch  Function Start Here */
    public function postViewInternalCustomSearch(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'month' => 'required'
        ]);

        $cmd = Input::get('cmd');

        if ($v->fails()) {
            return redirect('attendance/report')->withErrors($v->errors());
        }

        $month = Input::get('month');
        $month = get_date_format_inggris_bulan($month);

        $emp_id = Input::get('employee');
        $dep_id = Input::get('department');
        $des_id = Input::get('designation');

        $designation = Designation::where('did',$dep_id)->get();


        $attendance_query = Attendance::where('emp_id', '=', $cmd);

        if ($month) {
            $attendance_query->Where('month', $month);
        }

        $attendance = $attendance_query->get();


        $employee = Employee::find($cmd);
        $department = Department::all();
        return view('admin.attendance.view-attendance', compact('month','attendance', 'employee', 'department', 'date_to','date_from', 'emp_id', 'dep_id', 'des_id','designation'));

    }

    /* editAttendanceLeave  Function Start Here */
    public function editAttendanceLeave(Request $request)
    {
        $emp_id=Input::get('emp_id');
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'information'=>'required'
        ]);

        if($v->fails()){
            return redirect('schedules/view-employee/'.$emp_id)->withErrors($v->fails());
        }

        $attendance_employee  = Attendance::find($cmd);
        if($attendance_employee){
            $attendance_employee->leave_information = $request->information;
            $attendance_employee->save();
            

            return redirect('attendance/view-outsource/'.$emp_id)->with([
                'message' => 'Attendance updated successfully',
            ]);

        }else{
            return redirect('attendance/view-outsource/'.$emp_id)->with([
                'message' => 'Attendance not found',
                'message_important'=>true
            ]);
        }

    }

    /* viewOutsource  Function Start Here */
    public function viewOutsource($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 40)->first();
        $month = '';
        $attendance_id = Attendance::where('emp_id','=',$id)->first();
        $attendance = Attendance::where('emp_id','=',$id)->orderBy('date','ASC')->get();
        $attendance_total = Attendance::where('emp_id','=',$id)->where('status','=','Present')->count();
        $leave_total = Attendance::where('emp_id','=',$id)->where('status','=','Absent')->count();
        $overtime_total = Attendance::where('emp_id','=',$id)->sum('overtime');
        $days_before = strtotime(date('Y-m-d'));
        if ($attendance) {
            return view('admin.attendance.view-outsource', compact('days_before','month','overtime_total','leave_total','attendance_total','attendance','attendance_id','permcheck'));
        } else {
            return redirect('attendance/outsource-report')->with([
                'message' => language_data('Attendance Info Not Found'),
                'message_important' => true
            ]);
        }
    }

    /* postViewOutsourceCustomSearch  Function Start Here */
    public function postViewOutsourceCustomSearch(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
        ]);

        if ($v->fails()) {
            return redirect('attendance/view-outsource/'.$cmd)->withErrors($v->errors());
        }

        $month = Input::get('month');
        $month = get_date_format_inggris_bulan($month);

        $attendance_outsource_view = Attendance::where('emp_id','=',$cmd);
        $attendance_total_view = Attendance::where('emp_id','=',$cmd);
        $leave_total_view = Attendance::where('emp_id','=',$cmd);
        $overtime_total_view = Attendance::where('emp_id','=',$cmd);

        if ($month) {
            $attendance_outsource_view->Where('month', '=', $month);
            $attendance_total_view->Where('month', '=', $month);
            $leave_total_view->Where('month', '=', $month);
            $overtime_total_view->Where('month', '=', $month);
        }

        $attendance = $attendance_outsource_view->get();
        $attendance_total = $attendance_total_view->where('status','=','Present')->count();
        $leave_total = $leave_total_view->where('status','=','Absent')->count();
        $overtime_total = $overtime_total_view->sum('overtime');

        $attendance_id = Attendance::where('emp_id','=',$cmd)->first();

        return view('admin.attendance.view-outsource', compact('month','attendance','attendance_total','leave_total','overtime_total','attendance_id'));

    }

    /* download excel function start here */
    public function downloadExcelOutsource()
    {
        return response()->download(public_path('assets/template_xls/attendances_outsource.xls'));
    }

    /* import excel function start here */
    public function importExcelOutsource()
    {
        // // Get current data date from table
        // $date = Attendance::lists('date')->toArray();
        // // Get current data emp_id from table
        // $emp_id = Attendance::lists('emp_id')->toArray();

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
			})->get();
            if ($fileType != 'xls'){
                return redirect('attendance/outsource-report')->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else{
                if(!empty($data) && $data->count()){
                    $insert = array();
                    foreach ($data as $key => $value) {
                        $employee = Employee::where('employee_code','=', $value->nik)->first();
    
                        if($employee==''){
                            return redirect('attendance/report')->with([
                                'message' => language_data('Employee Not Found'),
                                'message_important' => true
                            ]);
    
                        }
                        $tanggal = get_date_format_inggris($value->tanggal);
    
                        // Skip department previously added using in_array
                        $attendance = Attendance::where('emp_id','=',$employee->id)->where('date','=',$tanggal)->first();
                        if ($attendance){
                            continue;
                        }
    
                        $schedule_employee = ScheduleEmployee::where('emp_id','=',$employee->id)->where('date','=',$tanggal)->first();
                        
                        if(!empty($schedule_employee)){
                            $office_in_time = $schedule_employee->starts_at;
                            $office_out_time = $schedule_employee->ends_at;
                        } else {
                            $office_in_time = app_config('OfficeInTime');
                            $office_out_time = app_config('OfficeOutTime');   
                        }
                
                        $late_1 = strtotime($office_in_time);
                        $late_2 = strtotime($value->jam_masuk);
                        $late = ($late_2 - $late_1);
                
                        if ($late < 0) {
                            $late = 0;
                        }
                        $late = $late / 60;
                
                
                        $early_leave_1 = strtotime($value->jam_pulang);
                
                        $early_leave_2 = strtotime($office_out_time);
                        $early_leave = ($early_leave_2 - $early_leave_1);
                
                        if ($early_leave < 0) {
                            $early_leave = 0;
                        }
                        $early_leave = $early_leave / 60;
                
                        $office_hour = (strtotime($value->jam_pulang) - strtotime($value->jam_masuk)) / 60;
                        $total = $office_hour - $early_leave;
                        
                        if($late>15){
                            $presence_status = 'late';
                        } else{
                            $presence_status = 'ontime';
                        }

                        $insert[] = [
                            'emp_id' => $employee->id, 
                            'emp_type' => $employee->employee_type, 
                            'designation' => $employee->designation, 
                            'department' => $employee->department, 
                            'date' => $tanggal, 
                            'month' => date('Y-m', strtotime($tanggal)), 
                            'clock_in' => $value->jam_masuk, 
                            'clock_out' => $value->jam_pulang, 
                            'late' => $late, 
                            'early_leaving' => $early_leave, 
                            'status' => 'Present', 
                            'presence_status' => $presence_status, 
                            'total' => $total
                        ];
                        $info[] = [
                            'project' => $employee->project,
                            'location' => $employee->location
                        ];
        
        
                    }
                    if(!empty($insert)){
                        Attendance::insert($insert);
                        
                        $emp_id = array();
                        $designation = array();
                        $department = array();
                        $project = array();
                        $location = array();
                        foreach($insert as $i){
                            $emp_id[] = $i['emp_id'];
                            $designation[] = $i['designation'];
                            $department[] = $i['department'];
                        }
                        foreach($info as $d){
                            $project[] = $d['project'];
                            $location[] = $d['location'];
                        }
                        for($a=0;$a<count($emp_id);$a++){
                            $outsourceReport = LeaveOutsourceReport::where('emp_id', $emp_id[$a])->first();
                            $at_total = Attendance::where('emp_id','=', $emp_id[$a])->where('status','=','Present')->count();
                            $lv_total = Attendance::where('emp_id','=', $emp_id[$a])->where('status','=','Absent')->count();
                            $ov_total = Attendance::where('emp_id','=', $emp_id[$a])->sum('overtime');
                    
                            if($outsourceReport){
                                $outsourceReport->total_attendance = $at_total;
                                $outsourceReport->total_leave = $lv_total;
                                $outsourceReport->total_overtime = $ov_total;
                                $outsourceReport->save();
                            } else {
                                $entryreport = new LeaveOutsourceReport();
                                $entryreport->emp_id = $emp_id[$a];
                                $entryreport->total_attendance = $at_total;
                                $entryreport->total_leave = $lv_total;
                                $entryreport->total_overtime = $ov_total;
                                $entryreport->designation = $designation[$a];
                                $entryreport->department = $department[$a];
                                $entryreport->project = $project[$a];
                                $entryreport->location = $location[$a];
                                $entryreport->save();
                            }
                        }
    
            
                        return redirect('attendance/outsource-report')->with([
                            'message' => language_data('Attendance Update Successfully')
                        ]);
                
                        
                    } else {
                        return redirect('attendance/outsource-report')->with([
                            'message' => language_data('Attendance Info Not Found'),
                            'message_important' => true
                        ]);
                    }
                }
            }
			
		}
		return back();
    }

    /* download excel function start here */
    public function downloadExcel()
    {
        return response()->download(public_path('assets/template_xls/attendances_internal.xls'));
    }

    /* import excel function start here */
    public function importExcel()
    {
        // // Get current data date from table
        // $date = Attendance::lists('date')->toArray();
        // // Get current data emp_id from table
        // $emp_id = Attendance::lists('emp_id')->toArray();

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
			})->get();
            if ($fileType != 'xls'){
                return redirect('attendance/report')->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else{
                // echo "<pre>"; print_r($data);
                if(!empty($data) && $data->count()){
                    $insert = array();
                    $info = array();
                    foreach ($data as $key => $value) {
                        $employee = Employee::where('employee_code','=', $value->nik)->first();
                        // echo "employee";print_r($employee);
    
                        if($employee==''){
                            return redirect('attendance/report')->with([
                                'message' => language_data('Employee Not Found'),
                                'message_important' => true
                            ]);
    
                        }
                        $tanggal = get_date_format_inggris($value->tanggal);
    
                        // Skip department previously added using in_array
                        $attendance = Attendance::where('emp_id','=',$employee->id)->where('date','=',$tanggal)->first();
                        if ($attendance){
                            continue;
                        }
    
                        $schedule_employee = ScheduleEmployee::where('emp_id','=',$employee->id)->where('date','=',$tanggal)->first();
                        
                        if(!empty($schedule_employee)){
                            $office_in_time = $schedule_employee->starts_at;
                            $office_out_time = $schedule_employee->ends_at;
                        } else {
                            $office_in_time = app_config('OfficeInTime');
                            $office_out_time = app_config('OfficeOutTime');   
                        }
                
                        $late_1 = strtotime($office_in_time);
                        $late_2 = strtotime($value->jam_masuk);
                        $late = ($late_2 - $late_1);
                
                        if ($late < 0) {
                            $late = 0;
                        }
                        $late = $late / 60;
                
                
                        $early_leave_1 = strtotime($value->jam_pulang);
                
                        $early_leave_2 = strtotime($office_out_time);
                        $early_leave = ($early_leave_2 - $early_leave_1);
                
                        if ($early_leave < 0) {
                            $early_leave = 0;
                        }
                        $early_leave = $early_leave / 60;
                
                        $office_hour = (strtotime($value->jam_pulang) - strtotime($value->jam_masuk)) / 60;
                        $total = $office_hour - $early_leave;
                        
                        if($late>15){
                            $presence_status = 'late';
                        } else{
                            $presence_status = 'ontime';
                        }

                        // echo $value->jam_pulang; print_r($insert);
                        $insert[] = array(
                            'emp_id' => $employee->id, 
                            'emp_type' => $employee->employee_type, 
                            'designation' => $employee->designation, 
                            'department' => $employee->department, 
                            'date' => $tanggal, 
                            'month' => date('Y-m', strtotime($tanggal)), 
                            'clock_in' => $value->jam_masuk, 
                            'clock_out' => $value->jam_pulang, 
                            'late' => $late, 
                            'early_leaving' => $early_leave, 
                            'status' => 'Present', 
                            'presence_status' => $presence_status, 
                            'total' => $total
                        );
                        // echo "array insert ";print_r($insert);
                        $info[] = [
                            'project' => $employee->project,
                            'location' => $employee->location
                        ];
        
                    }
                    // echo "array test "; print_r($insert);exit;
                    if(!empty($insert)){
                        Attendance::insert($insert);
                        
                        $emp_id = array();
                        $designation = array();
                        $department = array();
                        $project = array();
                        $location = array();
                        foreach($insert as $i){
                            $emp_id[] = $i['emp_id'];
                            $designation[] = $i['designation'];
                            $department[] = $i['department'];
                        }
                        foreach($info as $d){
                            $project[] = $d['project'];
                            $location[] = $d['location'];
                        }
                        for($a=0;$a<count($emp_id);$a++){
                            $internalReport = LeaveInternalReport::where('emp_id', $emp_id[$a])->first();
                            $at_total = Attendance::where('emp_id','=', $emp_id[$a])->where('status','=','Present')->count();
                            $lv_total = Attendance::where('emp_id','=', $emp_id[$a])->where('status','=','Absent')->count();
                            $ov_total = Attendance::where('emp_id','=', $emp_id[$a])->sum('overtime');
                    
                            if($internalReport){
                                $internalReport->total_attendance = $at_total;
                                $internalReport->total_leave = $lv_total;
                                $internalReport->total_overtime = $ov_total;
                                $internalReport->save();
                            } else {
                                $entryreport = new LeaveInternalReport();
                                $entryreport->emp_id = $emp_id[$a];
                                $entryreport->total_attendance = $at_total;
                                $entryreport->total_leave = $lv_total;
                                $entryreport->total_overtime = $ov_total;
                                $entryreport->designation = $designation[$a];
                                $entryreport->department = $department[$a];
                                $entryreport->project = $project[$a];
                                $entryreport->location = $location[$a];
                                $entryreport->save();
                            }
                            
                        }
    
            
                        return redirect('attendance/report')->with([
                            'message' => language_data('Attendance Update Successfully')
                        ]);
                
                        
                    } else {
                        return redirect('attendance/report')->with([
                            'message' => language_data('Attendance Info Not Found'),
                            'message_important' => true
                        ]);
                    }
                }
            }
			
		}
		return back();
    }

    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project . '</option>';
            }
        }
    }


}
