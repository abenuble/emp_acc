<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Cashflow;
use App\Cash;
use App\Coa;
use App\Company;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;

date_default_timezone_set(app_config('Timezone'));

class CashController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* Cash  Function Start Here */
    public function Cash()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 35)->first();
        $comp_id = '';
        $cash = Cash::orderBy('id','asc')->get();
        $coa = Coa::where('status','=','active')->get();
        $cashflow = Cashflow::where('status','=','active')->get();
        $company = Company::where('category','=','Internal')->where('status','=','active')->get();
        return view('admin.cash.cash', compact('cash','coa','company','cashflow','comp_id','permcheck'));

    }

    /* setCashStatus  Function Start Here */
    public function setCashStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('cash')->withErrors($v->fails());
        }

        $cash  = Cash::find($cmd);
        if($cash){
            $cash->status=$request->status;
            $cash->save();

            return redirect('cash')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('cash')->with([
                'message' => 'Cash not found',
                'message_important'=>true
            ]);
        }

    }

    /* addCash  Function Start Here */
    public function addCash(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'company' => 'required',
            'cash' => 'required',
            'coa_id' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('cash')->withErrors($v->errors());
        }

        $company = Input::get('company');
        $cash = Input::get('cash');
        $coa_id = Input::get('coa_id');

        $cashs = new Cash();
        $cashs->company = $company;
        $cashs->cash = $cash;
        $cashs->coa_id = $coa_id;
        $cashs->save();

        if ($cashs!='') {
            return redirect('cash')->with([
                'message' => 'Cash Added Successfully'
            ]);

        } else {
            return redirect('cash')->with([
                'message' => 'Cash Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateCash  Function Start Here */
    public function updateCash(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'company' => 'required',
            'cash' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('cash')->withErrors($v->errors());
        }
        $cashs = Cash::find($cmd);
        $company = Input::get('company');
        $cash = Input::get('cash');
        $status = Input::get('status');

        if ($cashs) {
            $cashs->company = $company;
            $cashs->cash = $cash;
            $cashs->status = $status;
            $cashs->save();
            
            return redirect('cash')->with([
                'message' => 'Cash Updated Successfully'
            ]);

        } else {
            return redirect('cash')->with([
                'message' => 'Cash Not Found',
                'message_important' => true
            ]);
        }
    }


    /* deleteCash  Function Start Here */
    public function deleteCash($id)
    {

        $cashs = Cash::find($id);
        if ($cashs) {

            $cashs->delete();

            return redirect('cash')->with([
                'message' => 'Cash Deleted Successfully'
            ]);
        } else {
            return redirect('cash')->with([
                'message' => 'Cash Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewCash  Function Start Here */
    public function viewCash($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 35)->first();
        $comp_id = '';
        $cashs = Cash::find($id);
        $coa = Coa::where('status','=','active')->where('company','=',$cashs->company)->where('sub_header','=','3')->get();
        $company = Company::where('category','=','Internal')->get();
        return view('admin.cash.view-cash', compact('comp_id','cashs','company','coa','permcheck'));
    }

    /* getCoa  Function Start Here */
    public function getCoa(Request $request)
    {
        $comp_id = $request->comp_id;
        if ($comp_id) {
            echo '<option value="0">Select COA</option>';
            $coa = Coa::where('company', $comp_id)->where('sub_header','=','3')->get();
            foreach ($coa as $c) {
                echo '<option value="' . $c->id . '">' . $c->coa . '('.$c->coa_code.')</option>';
            }
        }
    }

    /* download excel function start here */
    public function downloadExcel()
    {
        return response()->download(public_path('assets/template_xls/cash.xls'));
    }

    /* import excel function start here */
    public function importExcel()
    {
        // Get current data from company table
        $cash = Cash::lists('cash')->toArray();

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
			})->get();
            if ($fileType != 'xls'){
                return redirect('banks')->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else{
                if(!empty($data) && $data->count()){
                    $insert = array();
                    foreach ($data as $key => $value) {
                        // Skip cash previously added using in_array
                        if (in_array($value->kas, $cash)){
                            continue;
                        } else {
                            //Penomoran id
                            $last_number = Cash::max('id');
                            if($last_number==''){
                                $number = 1;
                            } else {
                                $number = 1 + $last_number;
                            }
                            $kode_coa = explode(" ",$value->bagan_akun);
                            $company = Company::where('company','=',$value->perusahaan)->first();
                            $coa = Coa::where('coa_code','=',$kode_coa)->first();

                            if($company==''){
                                return redirect('cash')->with([
                                    'message' => 'Perusahaan Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            } 
                            if($coa==''){
                                return redirect('cash')->with([
                                    'message' => 'COA Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            } 
                            if($value->kas==''){
                                return redirect('cash')->with([
                                    'message' => 'Kas Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            } 
                            $cashs = new Cash();
                            $cashs->id = $number;
                            $cashs->company = $company->id;
                            $cashs->coa_id = $coa->id;
                            $cashs->cash = $value->kas;
                            $cashs->save();
                        }

                    }
                    if(!empty($cashs)){
                        return redirect('cash')->with([
                            'message' => 'Cash Added Successfully'
                        ]);
                    }
                } else{
                    return redirect('cash')->with([
                        'message' => 'Cash Not Found',
                        'message_important' => true
                    ]);
                } 
            }
			
        }
		return back();
    }


}
