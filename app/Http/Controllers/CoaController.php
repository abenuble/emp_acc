<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Cashflow;
use App\Cashflow2;
use App\Coa;
use App\Coa2;
use App\Company;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;

date_default_timezone_set(app_config('Timezone'));

class CoaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* Coa  Function Start Here */
    public function Coa()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 36)->first();
        $coa = Coa::all();
        $cashflow = Cashflow::where('status','=','active')->get();
        $company = Company::where('category','=','Internal')->where('status','=','active')->get();
        $header = Coa::where('coa_type','=','header')->where('status','=','active')->get();
        $sub_header = Coa::where('coa_type','=','sub header')->where('status','=','active')->get();
        return view('admin.coa.coa', compact('coa','company','header','sub_header','cashflow','permcheck'));

    }

    /* setCoaStatus  Function Start Here */
    public function setCoaStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('coa')->withErrors($v->fails());
        }

        $coa  = Coa::find($cmd);
        if($coa){
            $coa->status=$request->status;
            $coa->save();

            return redirect('coa')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('coa')->with([
                'message' => 'COA not found',
                'message_important'=>true
            ]);
        }

    }

    /* addCoa  Function Start Here */
    public function addCoa(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'company' => 'required',
            'code' => 'required',
            'type' => 'required',
            'coa' => 'required',
            'cashflow' => 'required',
            'debit_credit' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('coa')->withErrors($v->errors());
        }

        $company = Input::get('company');
        $coa = Input::get('coa');
        $header = Input::get('header');
        $sub_header = Input::get('sub_header');
        $code = Input::get('code');
        $type = Input::get('type');
        $cashflow = Input::get('cashflow');
        $debit_credit = Input::get('debit_credit');
        $information = Input::get('information');

        //Penomoran id
        $last_number = Coa::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }

        $coas = new Coa();
        $coas->id = $number;
        $coas->company = $company;
        $coas->coa = $coa;
        $coas->header = $header;
        $coas->sub_header = $sub_header;
        $coas->coa_code = $code;
        $coas->coa_type = $type;
        $coas->cashflow = $cashflow;
        $coas->debit_credit = $debit_credit;
        $coas->information = $information;
        $coas->save();
        $positif = '';
        if($debit_credit == 'debit'){
            $positif = '1';
        } else if($debit_credit == 'credit'){
            $positif = '-1';
        }

        $data_coa[] = array(
            'id'				=> $number,
            'nama'				=> $coa,
            'kode'  			=> $code,
            'header_coa'	    => $header,
            'subheader_coa'	    => $sub_header,
            'positif'	        => $positif,
            'id_cashflow'		=> $cashflow,
            'tipe_akun'		    => $account_type,
        );
        $insert_coa  = Coa2::insert($data_coa);

        if ($coas!='') {
            return redirect('coa')->with([
                'message' => 'COA Added Successfully'
            ]);

        } else {
            return redirect('coas')->with([
                'message' => 'COA Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateCoa  Function Start Here */
    public function updateCoa(Request $request)
    {

        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'company' => 'required',
            'code' => 'required',
            'type' => 'required',
            'coa' => 'required',
            'cashflow' => 'required',
            'debit_credit' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('coa')->withErrors($v->errors());
        }
        $coas = Coa::find($cmd);
        $coas2 = Coa2::find($cmd);
        $company = Input::get('company');
        $coa = Input::get('coa');
        $header = Input::get('header');
        $sub_header = Input::get('sub_header');
        $code = Input::get('code');
        $type = Input::get('type');
        $cashflow = Input::get('cashflow');
        $account_type = Input::get('account_type');
        $debit_credit = Input::get('debit_credit');
        $information = Input::get('information');
        $status = Input::get('status');

        if ($coas) {
            $coas->company = $company;
            $coas->coa = $coa;
            $coas->header = $header;
            $coas->sub_header = $sub_header;
            $coas->coa_code = $code;
            $coas->coa_type = $type;
            $coas->cashflow = $cashflow;
            $coas->account_type = $account_type;
            $coas->debit_credit = $debit_credit;
            $coas->information = $information;
            $coas->status = $status;
            $coas->save();
            
            $positif = '';
            if($debit_credit == 'debit'){
                $positif = '1';
            } else if($debit_credit == 'credit'){
                $positif = '-1';
            }

            $data_coa = array(
                'nama'				=> $coa,
                'kode'  			=> $code,
                'header_coa'	    => $header,
                'subheader_coa'	    => $sub_header,
                'positif'	        => $positif,
                'id_cashflow'		=> $cashflow,
                'tipe_akun'		    => $account_type,
            );
            DB::table('coa')
            ->where('id', $cmd)
            ->update($data_coa);
            
            return redirect('coa')->with([
                'message' => 'COA Updated Successfully'
            ]);

        } else {
            return redirect('coa')->with([
                'message' => 'COA Not Found',
                'message_important' => true
            ]);
        }
    }


    /* deleteCoa  Function Start Here */
    public function deleteCoa($id)
    {
        $coas = Coa::find($id);
        if ($coas) {

            $coas->delete();

            return redirect('coa')->with([
                'message' => 'COA Deleted Successfully'
            ]);
        } else {
            return redirect('coa')->with([
                'message' => 'COA Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewCoa  Function Start Here */
    public function viewCoa($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 36)->first();
        $coas = Coa::find($id);
        $company = Company::where('category','=','Internal')->get();
        $cashflow = Cashflow::where('status','=','active')->get();
        $header = Coa::where('coa_type','=','header')->where('status','=','active')->get();
        $sub_header = Coa::where('coa_type','=','sub header')->where('status','=','active')->get();
        return view('admin.coa.view-coa', compact('coas','company','cashflow','header','sub_header','permcheck'));
    }

    /* download excel function start here */
    public function downloadExcel()
    {
        return response()->download(public_path('assets/template_xls/coa.xls'));
    }

    /* import excel function start here */
    public function importExcel()
    {
        // Get current data from company table
        $coa_code = Coa::lists('coa_code')->toArray();

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
			})->get();
            if ($fileType != 'xls'){
                return redirect('cashflow')->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else{
                if(!empty($data) && $data->count()){
                    $insert = array();
                    foreach ($data as $key => $value) {
                        // Skip coa_code previously added using in_array
                        if (in_array($value->kode_bagan_akun, $coa_code)){
                            continue;
                        } else{
                            //Penomoran id
                            $last_number = Coa::max('id');
                            if($last_number==''){
                                $number = 1;
                            } else {
                                $number = 1 + $last_number;
                            }
                            
                            $company = Company::where('company','=',$value->perusahaan)->first();
                            $cashflow = Cashflow::where('cashflow','=',$value->arus_kas)->first();
                            $header = Coa::where('id','=',$value->header)->first();
                            $sub_header = Coa::where('id','=',$value->sub_header)->first();

                            $tipe_akun = 0;
                            if(strtolower($value->tipe_akun) == 'aktiva lancar'){
                                $tipe_akun = 1;
                            } else if(strtolower($value->tipe_akun) == 'aktiva tetap'){
                                $tipe_akun = 2;
                            } else if(strtolower($value->tipe_akun) == 'hutang'){
                                $tipe_akun = 3;
                            } else if(strtolower($value->tipe_akun) == 'modal'){
                                $tipe_akun = 4;
                            } else if(strtolower($value->tipe_akun) == 'pendapatan'){
                                $tipe_akun = 5;
                            } else if(strtolower($value->tipe_akun) == 'beban'){
                                $tipe_akun = 6;
                            } else if(strtolower($value->tipe_akun) == 'pendapatan lain-lain'){
                                $tipe_akun = 7;
                            } else if(strtolower($value->tipe_akun) == 'beban lain-lain'){
                                $tipe_akun = 8;
                            }

                            $positif = '';
                            if(strtolower($value->debit_kredit) == 'debit' || strtolower($value->debit_kredit) == 'debet'){
                                $positif = '1';
                                $debet_kredit = 'debit';
                            } else if(strtolower($value->debit_kredit) == 'kredit' || strtolower($value->debit_kredit) == 'credit'){
                                $positif = '-1';
                                $debet_kredit = 'credit';
                            }

                            if($company==''){
                                return redirect('coa')->with([
                                    'message' => 'Perusahaan Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            }
                            if($cashflow==''){
                                return redirect('coa')->with([
                                    'message' => 'Arus Kas Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            } 
                            if($value->bagan_akun=='' || $value->kode_bagan_akun=='' || $value->tipe_bagan_akun=='' || $value->debit_kredit=='' || $value->informasi==''){
                                return redirect('coa')->with([
                                    'message' => 'COA Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            } 
                            if($header==''){
                                $coas = new Coa();
                                $coas->id = $number;
                                $coas->company = $company->id;
                                $coas->coa = $value->bagan_akun;
                                $coas->coa_code = $value->kode_bagan_akun;
                                $coas->coa_type = $value->tipe_bagan_akun;
                                $coas->cashflow = $cashflow->id;
                                $coas->account_type = $tipe_akun;
                                $coas->debit_credit = $debet_kredit;
                                $coas->information = $value->informasi;
                                $coas->save();

                                $data_coa[] = array(
                                    'id'				=> $number,
                                    'nama'				=> $value->bagan_akun,
                                    'kode'  			=> $value->kode_bagan_akun,
                                    'header_coa'	    => '',
                                    'subheader_coa'	    => '',
                                    'positif'	        => $positif,
                                    'id_cashflow'		=> $cashflow->id,
                                    'tipe_akun'		    => $tipe_akun,
                                );
    
                            } else if($sub_header==''){
                                $coas = new Coa();
                                $coas->id = $number;
                                $coas->company = $company->id;
                                $coas->coa = $value->bagan_akun;
                                $coas->header = $header->id;
                                $coas->coa_code = $value->kode_bagan_akun;
                                $coas->coa_type = $value->tipe_bagan_akun;
                                $coas->cashflow = $cashflow->id;
                                $coas->account_type = $tipe_akun;
                                $coas->debit_credit = $debet_kredit;
                                $coas->information = $value->informasi;
                                $coas->save();

                                $data_coa[] = array(
                                    'id'				=> $number,
                                    'nama'				=> $value->bagan_akun,
                                    'kode'  			=> $value->tipe_bagan_akun,
                                    'header_coa'	    => $header,
                                    'subheader_coa'	    => '',
                                    'positif'	        => $positif,
                                    'id_cashflow'		=> $cashflow->id,
                                    'tipe_akun'		    => $tipe_akun,
                                );
    
                            } else {
                                $coas = new Coa();
                                $coas->id = $number;
                                $coas->company = $company->id;
                                $coas->coa = $value->bagan_akun;
                                $coas->header = $header->id;
                                $coas->sub_header = $sub_header->id;
                                $coas->coa_code = $value->kode_bagan_akun;
                                $coas->coa_type = $value->tipe_bagan_akun;
                                $coas->cashflow = $cashflow->id;
                                $coas->account_type = $tipe_akun;
                                $coas->debit_credit = $debet_kredit;
                                $coas->information = $value->informasi;
                                $coas->save();

                                $data_coa[] = array(
                                    'id'				=> $number,
                                    'nama'				=> $value->bagan_akun,
                                    'kode'  			=> $value->kode_bagan_akun,
                                    'header_coa'	    => $header,
                                    'subheader_coa'	    => $sub_header,
                                    'positif'	        => $positif,
                                    'id_cashflow'		=> $cashflow->id,
                                    'tipe_akun'		    => $tipe_akun,
                                );
    
                            }

                        }
                    }
                    $insert_coa  = Coa2::insert($data_coa);
                    if(!empty($coas)){
                        return redirect('coa')->with([
                            'message' => 'COA Added Successfully'
                        ]);
                    }
                } else{
                    return redirect('coa')->with([
                        'message' => 'COA Not Found',
                        'message_important' => true
                    ]);
                } 
            }
        }
		return back();
    }


}
