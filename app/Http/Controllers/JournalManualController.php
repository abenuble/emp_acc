<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\JournalManual;
use App\JournalManualDetail;
use App\Coa;
use App\Ledger;
use App\Expense;
use App\EmployeeRolesPermission;
use App\Coa2;
use App\Ledger2;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;

class JournalManualController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* contract  Function Start Here */
    public function JournalManual()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 79)->first();
        $journal_manual = JournalManual::orderBy('id','asc')->get();
        return view('admin.journal_manual.journal-manual', compact('journal_manual','permcheck'));

    }

    /* getCoa Function Start Here */
    public function getCoa()
    {
        echo '<option value="0">Select COA</option>';
        $coa = Coa::where('coa_type','=','approximate name')->get();
        foreach ($coa as $d) {
            echo '<option value="' . $d->id . '">(' . $d->coa_code . ') ' . $d->coa . '</option>';
        }
    }
    public function getExpenseDetail(Request $request)
    {
       $id = $request->id;
       $hsl=Expense::find($id);
       if($hsl){
           echo $hsl->total;
       }
       else{echo "0";}
    }
    /* addJournalManual  Function Start Here */
    public function addJournalManual()
    {
        $date = date("Y-m-d");
        $coa = Coa::where('coa_type','=','approximate name')->get();
        $expense = Expense::where('status','=','Approved')->get();

        return view('admin.journal_manual.add-journal-manual', compact('coa','date','expense'));

    }

    /* addPostJournalManual  Function Start Here */
    public function addPostJournalManual(Request $request)
    {

        $tagihan = Input::get('tagihan');
        $tagihan_id = Input::get('id_tagihan');
        $tagihan_nominal = (int)str_replace(',', '', Input::get('nominal_tagihan'));
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $date2 = date('Y-m-01',strtotime($date));
        $number = $this->get_kode_transaksi();
        $company = \Auth::user()->company;

        $coa_id = Input::get('m_coa_id');
        $nominal_debit = Input::get('jurnal_manualdet_nominal_debet');
        $nominal_kredit = Input::get('jurnal_manualdet_nominal_kredit');
        
        $jurnal_manual_debit_kredit = Input::get('jurnal_manual_debit_kredit');

        $total_debit = (int)str_replace(',', '', Input::get('jurnal_manual_total_debet'));
        $total_kredit = (int)str_replace(',', '', Input::get('jurnal_manual_total_kredit'));
        
        $information = Input::get('jurnal_manual_keterangan');

        //Penomoran id
        $last_number_id = JournalManual::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $journal_manual = new JournalManual();
        $journal_manual->number = $number;
        $journal_manual->date = $date;
        $journal_manual->company = $company;
        $journal_manual->tagihan = $tagihan;
        $journal_manual->tagihan_id = $tagihan_id;
        $journal_manual->tagihan_nominal = $tagihan_nominal;
        $journal_manual->total_debit = $total_debit;
        $journal_manual->total_kredit = $total_kredit;
        $journal_manual->information = $information;
        $journal_manual->save();

        $jurnal_manual_id = $journal_manual->id;

        $data_buku_besar = array();
        $data_buku_besar2 = array();

        for($a = 0; $a < sizeof($coa_id); $a++){
            $transaction_number = $this->get_kode_transaksi2();
            $data_detail[] = array(
                'journal_manual'		=> $jurnal_manual_id,
                'coa_id'        		=> $coa_id[$a],
                'debit_credit'        	=> $jurnal_manual_debit_kredit[$a],
                'nominal_debit'			=> (int)str_replace(',', '', $nominal_debit[$a]),
                'nominal_kredit'		=> (int)str_replace(',', '', $nominal_kredit[$a]),
            );

            $debit = 367;
            $kredit = 367;
            $nominal = 0;
            if($jurnal_manual_debit_kredit[$a]=='d'){
                $debit = $coa_id[$a];
                $nominal = (int)str_replace(',', '', $nominal_debit[$a]);
            } else {
                $kredit = $coa_id[$a];
                $nominal = (int)str_replace(',', '', $nominal_kredit[$a]);
            }
            $data_buku_besar[] = array(
                'company'               => $company,
                'period'                => $date2,
                'date'                  => $date,
                'transaction_number'    => $transaction_number,
                'information'           => $information,
                'id_coa_debit'          => $debit,
                'id_coa_kredit'         => $kredit,
                'nominal'               => $nominal,
                'customer_id'           => 0,
                'supplier_id'           => 0,
                'invoice_number'        => $number,
                'id_cashflow_debit'     => 0,
                'id_cashflow_credit'    => 0,
                'id_file'               => 0,
                'baris_file'            => 0,
                'hitung'                => 0,
            );

            $data_buku_besar2[] = array(
                'branch_id'             => $company,
                'periode'               => $date2,
                'tanggal'               => $date,
                'no_transaksi'          => $transaction_number,
                'keterangan'            => $information,
                'id_coa_debit'          => $debit,
                'id_coa_kredit'         => $kredit,
                'nominal'               => $nominal,
                'customer_id'           => 0,
                'supplier_id'           => 0,
                'no_invoice'            => $number,
                'id_cashflow_debit'     => 0,
                'id_cashflow_kredit'    => 0,
                'id_file'               => 0,
                'baris_file'            => 0,
                'hitung'                => 0,
            );   
            $coa1 = Coa2::find($debit);
            $coa2 = Coa2::find($kredit);
            
            if($coa1){
                if($coa1->tipe_akun>=5){
                    $data_buku_besar3[] = array(
                        'branch_id'				=> $company,
                        'periode'				=> $date2,
                        'tanggal'  				=> $date,
                        'no_transaksi'      	=> $transaction_number,
                        'keterangan'			=> 'Laba Tahun Berjalan',
                        'id_coa_debit'			=> 100,
                        'id_coa_kredit'			=> 0,
                        'nominal'				=> $nominal,
                        'customer_id'			=> 0,
                        'supplier_id'			=> 0,
                        'no_invoice'    		=> 0,
                        'id_cashflow_debit'		=> 0,
                        'id_cashflow_kredit'	=> 0,
                        'id_file'				=> 0,
                        'baris_file'			=> 0,
                        'hitung'				=> 0,
                    );
                }
            }
            
            if($coa2){
                if($coa2->tipe_akun>=5){
                    $data_buku_besar3[] = array(
                        'branch_id'				=> $company,
                        'periode'				=> $date2,
                        'tanggal'  				=> $date,
                        'no_transaksi'      	=> $transaction_number,
                        'keterangan'			=> 'Laba Tahun Berjalan',
                        'id_coa_debit'			=> 0,
                        'id_coa_kredit'			=> 100,
                        'nominal'				=> $nominal,
                        'customer_id'			=> 0,
                        'supplier_id'			=> 0,
                        'no_invoice'    		=> 0,
                        'id_cashflow_debit'		=> 0,
                        'id_cashflow_kredit'	=> 0,
                        'id_file'				=> 0,
                        'baris_file'			=> 0,
                        'hitung'				=> 0,
                    );
                }
            }
        }
        
        $journal_manual_detail  = JournalManualDetail::insert($data_detail);
        $insert_ledger          = Ledger::insert($data_buku_besar);
        $insert_ledger2         = Ledger2::insert($data_buku_besar2);
        if(isset($data_buku_besar3)){
            $insert_ledger3         = Ledger2::insert($data_buku_besar3);
        }
    

        if ($journal_manual!='') {
            return redirect('accountings/journal-manual')->with([
                'message' => 'Journal Manual Added Successfully'
            ]);

        } else {
            return redirect('accountings/journal-manual')->with([
                'message' => 'Journal Manual Already Exist',
                'message_important' => true
            ]);
        }
    }

    /* viewJournalManual  Function Start Here */
    public function viewJournalManual($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 79)->first();
        $coa = Coa::where('coa_type','=','approximate name')->get();
        $journal_manual = JournalManual::find($id);
        $detail = JournalManualDetail::where('journal_manual','=',$id)->get();
        $expense = Expense::where('status','=','Approved')->get();
        return view('admin.journal_manual.view-journal-manual', compact('journal_manual','detail','coa','expense','permcheck'));
    }

    /* generate tansaction code */
	public function get_kode_transaksi(){
		$bln = date('m');
		$thn = date('y');
        $query = DB::table('sys_journal_manuals')
                    ->selectRaw('MID(number,7,5) as id')
                    ->whereRaw('MID(number,1,6) = "JM'.$thn.''.$bln.'"')
                    ->orderBy('number', 'asc')
                    ->limit(1)
                    ->get();
		$kode_baru = format_kode_transaksi('JM',$query);
		return $kode_baru;
	}

    /* generate tansaction code */
	public function get_kode_transaksi2(){
		$bln = date('m');
		$thn = date('y');
        $query = DB::table('sys_ledger')
                    ->selectRaw('MID(transaction_number,7,5) as id')
                    ->whereRaw('MID(transaction_number,1,6) = "LG'.$thn.''.$bln.'"')
                    ->orderBy('transaction_number', 'asc')
                    ->limit(1)
                    ->get();
		$kode_baru = format_kode_transaksi('LG',$query);
		return $kode_baru;
	}


}
