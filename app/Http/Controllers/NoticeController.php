<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Notice;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
date_default_timezone_set(app_config('Timezone'));
class NoticeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* noticeBoard  Function Start Here */
    public function noticeBoard()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 15)->first();
        $notice=Notice::orderBy('id','asc')->get();
        return view('admin.notice-board',compact('notice','permcheck'));
    }

    /* postNewNotice  Function Start Here */
    public function postNewNotice(Request $request)
    {

        $v=\Validator::make($request->all(),[
            'notice_title'=>'required','status'=>'required'
        ]);

        if($v->fails()){
            return redirect('notice-board')->withErrors($v->errors());
        }

        $notice=new Notice();
        $notice->title=$request->notice_title;
        $notice->status=$request->status;
        $notice->description=$request->description;
        $notice->save();

        return redirect('notice-board')->with([
            'message'=> language_data('Notice Added Successfully')
        ]);

    }

    /* deleteNotice  Function Start Here */
    public function deleteNotice($id)
    {


        $notice=Notice::find($id);
        if($notice){
            $notice->delete();
            return redirect('notice-board')->with([
                'message'=> language_data('Notice Deleted Successfully')
            ]);
        }else{
            return redirect('notice-board')->with([
                'message'=> language_data('Notice not found'),
                'message_important'=>true
            ]);
        }
    }

    /* editNotice  Function Start Here */
    public function editNotice($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 15)->first();
        $notice=Notice::find($id);
        if($notice){
            return view('admin.edit-notice',compact('notice','permcheck'));
        }else{
            return redirect('notice-board')->with([
                'message'=> language_data('Notice not found'),
                'message_important'=>true
            ]);
        }
    }

    /* postEditNotice  Function Start Here */
    public function postEditNotice(Request $request)
    {
        $cmd=Input::get('cmd');

        $v=\Validator::make($request->all(),[
            'notice_title'=>'required','status'=>'required'
        ]);

        if($v->fails()){
            return redirect('notice-board/edit/'.$cmd)->withErrors($v->errors());
        }

        $notice=Notice::find($cmd);
        if($notice){
            $notice->title=$request->notice_title;
            $notice->status=$request->status;
            $notice->description=$request->description;
            $notice->save();

            return redirect('notice-board')->with([
                'message'=> language_data('Notice Updated Successfully')
            ]);
        }else{
            return redirect('notice-board')->with([
                'message'=> language_data('Notice not found'),
                'message_important'=>true
            ]);
        }
    }


}
