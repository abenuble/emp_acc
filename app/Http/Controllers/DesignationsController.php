<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\CareerPathEmployee;
use App\EmployeeRolesPermission;
use App\Department;
use App\Designation;
use App\Employee;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;

date_default_timezone_set(app_config('Timezone'));

class DesignationsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* designations  Function Start Here */
    public function designations()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 23)->first();
        $departments = Department::where('status','=','active')->get();
        $designations = Designation::where('id','!=','0')->orderBy('id','asc')->get();
        return view('admin.designation.designations', compact('departments', 'designations','permcheck'));
    }

    /* editDesignation  Function Start Here */
    public function editDesignation($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 23)->first();
        $departments = Department::where('status','=','active')->get();
        $designations = Designation::find($id);
        $career = Designation::all();
        return view('admin.designation.edit-designation', compact('departments','designations','career','permcheck'));

    }

    /* addDesignation  Function Start Here */
    public function addDesignation(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'designation' => 'required', 'department' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('designations')->withErrors($v->errors());
        }
        $department = Input::get('department');
        $designation = Input::get('designation');
        
        $exist = Designation::where('designation', $designation)->first();
        if ($exist) {
            return redirect('designations')->with([
                'message' => language_data('Designation Already Exist'),
                'message_important' => true
            ]);
        }
        
        //Penomoran id
        $last_number = Designation::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }

        $designations = new Designation();
        $designations->id = $number;
        $designations->did = $department;
        $designations->designation = $designation;
        $designations->career = '0';
        $designations->save();
        
        
        if ($designations!='') {
            $career_id = $designations->id;
            
            $career = Designation::find($career_id);
            $career_paths = CareerPathEmployee::where('designation','=',$career->id)->first();
            
            if($career_paths==''){
                //Penomoran id
                $last_number_id = CareerPathEmployee::max('id');
                if($last_number_id==''){
                    $number_id = 1;
                } else {
                    $number_id = 1 + $last_number_id;
                }
    
                $careers = new CareerPathEmployee();
                $careers->id = $number_id;
                $careers->department = $career->did;
                $careers->designation = $career->id;
                $careers->save();
                
                return redirect('designations')->with([
                    'message' => language_data('Designation Added Successfully')
                ]);
    
            } else {
                return redirect('designations')->with([
                    'message' => language_data('Designation Added Successfully')
                ]);
            }
        } else {
            return redirect('designations')->with([
                'message' => language_data('Designation Already Exist'),
                'message_important' => true
            ]);
        }

    }

    /* updateDesignation  Function Start Here */
    public function updateDesignation(Request $request)
    {
        $cmd = Input::get('cmd');

        $v = \Validator::make($request->all(), [
            'designation' => 'required', 'department' => 'required', 'career_path' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('designations')->withErrors($v->errors());
        }

        $designation = trim(Input::get('designation'));
        $department = Input::get('department');
        $career_path = Input::get('career_path');
        $status = Input::get('status');

        $des = Designation::find($cmd);

        $des->did = $department;
        $des->designation = $designation;
        $des->career = $career_path;
        $des->status = $status;
        $des->save();

        $career = Designation::find($career_path);
        
        $career_paths = CareerPathEmployee::where('designation','=',$career->id)->first();
        if($career_paths==''){
            //Penomoran id
            $last_number_id = CareerPathEmployee::max('id');
            if($last_number_id==''){
                $number_id = 1;
            } else {
                $number_id = 1 + $last_number_id;
            }

            $careers = new CareerPathEmployee();
            $careers->id = $number_id;
            $careers->department = $career->did;
            $careers->designation = $career->id;
            $careers->save();

            return redirect('designations')->with([
                'message' => language_data('Designation Update Successfully')
            ]);
        } else {
            return redirect('designations')->with([
                'message' => language_data('Designation Update Successfully')
            ]);
        }
        

    }

    /* download excel function start here */
    public function downloadExcel()
    {
        return response()->download(public_path('assets/template_xls/designations.xls'));
    }

    /* import excel function start here */
    public function importExcel()
    {
        // Get current data from designation table
        $designation = Designation::lists('designation')->toArray();

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
			})->get();
            if ($fileType != 'xls'){
                return redirect('companies')->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else{
                if(!empty($data) && $data->count()){
                    $insert = array();
                    foreach ($data as $key => $value) {
                        // Skip designation previously added using in_array
                        if (in_array($value->jabatan, $designation)){
                            continue;
                        } else{
                            //Penomoran id
                            $last_number = Designation::max('id');
                            if($last_number==''){
                                $number = 1;
                            } else {
                                $number = 1 + $last_number;
                            }
                            if ($value->jabatan=='') {
                                return redirect('designations')->with([
                                    'message' => language_data('Designation Not Found'),
                                    'message_important' => true
                                ]);
                            } 
                            if ($value->divisi=='') {
                                return redirect('designations')->with([
                                    'message' => 'Divisi Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            } 
                            $department = Department::where('department','=',$value->divisi)->first();

                            if($department==''){
                                //Penomoran id
                                $last_number = Department::max('id');
                                if($last_number==''){
                                    $number = 1;
                                } else {
                                    $number = 1 + $last_number;
                                }
                                $departments = new Department();
                                $departments->id = $number;
                                $departments->department = $value->divisi;
                                $departments->save();

                                $did = $departments->id;
                            } else {
                                $did = $department->id;
                            }

                            $designations = new Designation();
                            $designations->id = $number;
                            $designations->did = $did;
                            $designations->designation = $value->jabatan;
                            $designations->save();
                            
                            $career_id = $designations->id;
                            
                            $career = Designation::find($career_id);
                            $career_paths = CareerPathEmployee::where('designation','=',$career->id)->first();
                            
                            if($career_paths==''){
                                //Penomoran id
                                $last_number_id = CareerPathEmployee::max('id');
                                if($last_number_id==''){
                                    $number_id = 1;
                                } else {
                                    $number_id = 1 + $last_number_id;
                                }
                    
                                $careers = new CareerPathEmployee();
                                $careers->id = $number_id;
                                $careers->department = $career->did;
                                $careers->designation = $career->id;
                                $careers->save();
                                
                            } 
            
                        }
                    }
                    return redirect('designations')->with([
                        'message' => language_data('Designation Added Successfully')
                    ]);
                } else{
                    return redirect('designations')->with([
                        'message' => language_data('Designation Not Found'),
                        'message_important' => true
                    ]);
                } 
            }
		}
		return back();
    }


}
