<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Calendar;
use App\Department;
use App\Employee;
use App\SMSGateway;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Twilio\Rest\Client;

date_default_timezone_set(app_config('Timezone'));

class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* calendar  Function Start Here */
    public function calendar()
    {
        return view('admin.calendar.calendar');
    }

    /* eventCalendar  Function Start Here */
    public function eventCalendar()
    {
        $fdate = Input::get('from');
        $fdate = $fdate / 1000;
        $fdate = date('Y-m-d', $fdate);
        $tdate = Input::get('to');
        $tdate = $tdate / 1000;
        $tdate = date('Y-m-d', $tdate);
        $out = array();

        //find current month
        $d = Calendar::all();
        foreach ($d as $xs) {
            $id = $xs->id;
            $start_date = $xs->start_date;
            $end_date = $xs->end_date;
            $starts_at = $xs->starts_at;
            $ends_at = $xs->ends_at;
            $occasion = $xs->occasion;
            $description = $xs->description;
            $location = $xs->location;
            $event = $xs->event;
            $occasion_name = $occasion . ', ' . $location . ', ' . get_date_format($start_date) .' ' . $starts_at . ' - ' . get_date_format($end_date) . ' ' . $ends_at . ', ' . $description;

            $url = url('calendar/view-calendar/' . $id);
            $out[] = array(
                'id' => $id,
                'title' => $occasion_name,
                'url' => $url,
                'class' => 'event-'.$event,
                'start' => strtotime($start_date." ".$starts_at) . '000',
                'end' => strtotime($end_date.$ends_at) . '000'
            );
        }

        echo json_encode(array('success' => 1, 'result' => $out));
        exit;
    }


    /* addCalendar  Function Start Here */
    public function addCalendar()
    {
        $employee = Employee::where('role_id','!=','1')->get();
        $members_dropdown = [];
        $teams_dropdown = [];
        foreach($employee as $emp){
            $members_dropdown[] = array("type" => "member", "id" => "member:". $emp->id, "text" => $emp->fname . " " . $emp->lname);
        }

        $department = Department::all();

        foreach($department as $dept){
            $teams_dropdown[] = array("type" => "department", "id" => "department:". $dept->id, "text" => $dept->department);
        }

        $members_and_teams_dropdown = array_merge($members_dropdown,$teams_dropdown);

        $selected_color = "";
        $colors = array("#83c340", "#2d9cdb", "#aab7b7", "#f1c40f", "#e74c3c", "#ad159e", "#34495e");
        foreach($colors as $color){
            $active_class = '';
            if ($selected_color === $color) {
                $active_class = "active";
            }
        }
        
        return view('admin.calendar.add-calendar', compact('members_and_teams_dropdown','active_class','colors','selected_color'));
    }

    /* postAddCalendar  Function Start Here */
    public function postAddCalendar(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'start_date' => 'required', 'occasion' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('calendar/add')->withErrors($v->errors());
        }

        $start_date             = Input::get('start_date');
        $start_date             = get_date_format_inggris($start_date);
        $end_date               = Input::get('end_date');
        $end_date               = get_date_format_inggris($end_date);
        $starts_at              = Input::get('start_time');
        $mulai_dari             = date('H:i',strtotime($starts_at));
        $starts_at              = date('H:i:s',strtotime($starts_at));
        $ends_at                = Input::get('end_time');
        $hingga                 = date('H:i',strtotime($ends_at));
        $ends_at                = date('H:i:s',strtotime($ends_at));
        $event                  = Input::get('warna');
        
        // echo $request->sms_notification;exit;

        if($event==='#83c340'){
            $event_code = "success";
        } else if($event==="#2d9cdb"){
            $event_code = "info";
        } else if($event==="#aab7b7"){
            $event_code = "primary";
        } else if($event==="#f1c40f"){
            $event_code = "warning";
        } else if($event==="#e74c3c"){
            $event_code = "important";
        } else if($event==="#ad159e"){
            $event_code = "special";
        } else {
            $event_code = "inverse";
        }
        
        if($request->share_with==='specific'){
            $calendar               = new Calendar();
            $calendar->start_date   = $start_date;
            $calendar->end_date     = $end_date;
            $calendar->starts_at    = $starts_at;
            $calendar->ends_at      = $ends_at;
            $calendar->occasion     = $request->occasion;
            $calendar->description  = $request->description;
            $calendar->location     = $request->location;
            $calendar->shared_with  = $request->share_with_specific;
            $calendar->event        = $event_code;
            $calendar->save();
        } else {
            $calendar               = new Calendar();
            $calendar->start_date   = $start_date;
            $calendar->end_date     = $end_date;
            $calendar->starts_at    = $starts_at;
            $calendar->ends_at      = $ends_at;
            $calendar->occasion     = $request->occasion;
            $calendar->description  = $request->description;
            $calendar->location     = $request->location;
            $calendar->shared_with  = $request->share_with;
            $calendar->event        = $event_code;
            $calendar->save();
        }
        // if($request->sms_notification==1){
        
        //     // $sms_gateway = SMSGateway::where('status', 'Active')->first();
        //     // $gateway_name = $sms_gateway->name;
        //     // if ($gateway_name == 'Twilio') {
        //     //     $sid = $sms_gateway->user_name;
        //     //     $token = $sms_gateway->password;
    
        //     //     $client = new Client($sid, $token);
        //     //     $message = 'Acara : '.$request->occasion.', Deskripsi : '.$request->occasion.', Lokasi : '.$request->location.', Mulai dari : '.$request->start_date.' '.$mulai_dari.', Hingga : '.$request->end_date.' '.$hingga;
        //     //     if($request->share_with_specific!='all'){
        //     //         $share_with = explode(',',$request->share_with_specific);
        //     //         for($a=0;$a<count($share_with);$a++){
        //     //             $employee = Employee::find(str_replace('member:','',$share_with[$a]));
        //     //             if ($employee) {
        //     //                 $response = $client->account->messages->create(
        //     //                     "$employee->phone",
        //     //                     array(
        //     //                         'from' => '+19383333659',
        //     //                         'body' => $message
        //     //                     )
        //     //                 );
        //     //             }
        //     //         }
        //     //     } else{
        //     //         $employee = Employee::where('role_id','!=','1')->get();
        //     //         if ($employee) {
        //     //             foreach($employee as $e){
        //     //                 $response = $client->account->messages->create(
        //     //                     "$e->phone",
        //     //                     array(
        //     //                         'from' => '+19383333659',
        //     //                         'body' => $message
        //     //                     )
        //     //                 );
        //     //             }
        //     //         }
        //     //     }
        //     // }
    
        //         $message = 'Acara : '.$request->occasion.', Deskripsi : '.$request->occasion.', Lokasi : '.$request->location.', Mulai dari : '.$request->start_date.' '.$mulai_dari.', Hingga : '.$request->end_date.' '.$hingga;
        //         if($request->share_with_specific!='all'){
        //             $share_with = explode(',',$request->share_with_specific);
        //             for($a=0;$a<count($share_with);$a++){
        //                 $employee = Employee::find(str_replace('member:','',$share_with[$a]));
        //                 if ($employee) {                            
        //                     $xmlData = '
        //                     <bulk_sending>
        //                         <username>pmudemo_api</username>
        //                         <password>35c57b5b059f9dc15d985ff45a685692</password>
        //                         <priority>normal</priority>
        //                         <sender>ProMDemo</sender>
        //                         <dr_url>Http://210.45.90.102/receiver_status/dr_status.php</dr_url>
        //                         <allowduplicate>1</allowduplicate>
        //                         <data_packet>
        //                             <packet>
        //                                 <msisdn>'.$employee->phone.'</msisdn>
        //                                 <sms>'.$message.'</sms>
        //                                 <is_long_sms>Y</is_long_sms>
        //                             </packet>
        //                         </data_packet>
        //                     </bulk_sending>
        //                     ';
        //                     $post = 'data='. urlencode($xmlData);
        //                     $url = "http://webapps.promediautama.com:29003/sms_applications/smsb/api_mt_send_message.php";
        //                     $ch = curl_init(); 
        //                     curl_setopt($ch, CURLOPT_URL, $url); 
        //                     curl_setopt($ch, CURLOPT_POST ,1); 
        //                     curl_setopt($ch, CURLOPT_POSTFIELDS ,$post);
        //                     curl_setopt($ch, CURLOPT_RETURNTRANSFER ,1); 
        //                     $data = curl_exec($ch); 
        //                     curl_close($ch);
        //                 }
        //             }
        //         } else{
        //             $employee = Employee::where('role_id','!=','1')->get();
        //             if ($employee) {
        //                 foreach($employee as $e){                        
        //                     $xmlData = '
        //                     <bulk_sending>
        //                         <username>pmudemo_api</username>
        //                         <password>35c57b5b059f9dc15d985ff45a685692</password>
        //                         <priority>normal</priority>
        //                         <sender>ProMDemo</sender>
        //                         <dr_url></dr_url>
        //                         <allowduplicate>1</allowduplicate>
        //                         <data_packet>
        //                             <packet>
        //                                 <msisdn>'.$e->phone.'</msisdn>
        //                                 <sms>'.$message.'</sms>
        //                                 <is_long_sms>Y</is_long_sms>
        //                             </packet>
        //                         </data_packet>
        //                     </bulk_sending>
        //                     ';
        //                     $post = 'data='. urlencode($xmlData);
        //                     $url = "http://webapps.promediautama.com:29003/sms_applications/smsb/api_mt_send_message.php";
        //                     $ch = curl_init(); 
        //                     curl_setopt($ch, CURLOPT_URL, $url); 
        //                     curl_setopt($ch, CURLOPT_POST ,1); 
        //                     curl_setopt($ch, CURLOPT_POSTFIELDS ,$post);
        //                     curl_setopt($ch, CURLOPT_RETURNTRANSFER ,1); 
        //                     $data = curl_exec($ch); 
        //                     curl_close($ch);
        //                 }
        //             }
        //         }

        // }
        

        if ($calendar->wasRecentlyCreated) {
            return redirect('calendar')->with([
                'message' => 'Event Updated Successfully'
            ]);

        } else {
            return redirect('calendar')->with([
                'message' => 'Event Already Exist',
                'message_important' => true
            ]);
        }

    }

    /* viewCalendar  Function Start Here */
    public function viewCalendar($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 19)->first();
        $employee = Employee::where('role_id','!=','1')->get();

        foreach($employee as $emp){
            $members_dropdown[] = array("type" => "member", "id" => "member:". $emp->id, "text" => $emp->fname . " " . $emp->lname);
        }

        $department = Department::all();

        foreach($department as $dept){
            $teams_dropdown[] = array("type" => "department", "id" => "department:". $dept->id, "text" => $dept->department);
        }

        $members_and_teams_dropdown = array_merge($members_dropdown,$teams_dropdown);
        $calendar = Calendar::find($id);
        if ($calendar) {
            $selected_color = '';
            $colors = array("#83c340", "#2d9cdb", "#aab7b7", "#f1c40f", "#e74c3c", "#ad159e", "#34495e");
            $active_class = '';
            return view('admin.calendar.view-calendar', compact('calendar','selected_color','colors','active_class','members_and_teams_dropdown','permcheck'));

        } else {
            return redirect('calendar')->with([
                'message' => 'Event Not Found',
                'message_important' => true
            ]);
        }
    }


    /* deleteCalendar  Function Start Here */
    public function deleteCalendar($id)
    {
        $calendar = Calendar::find($id);
        if ($calendar) {
            $calendar->delete();

            return redirect('calendar')->with([
                'message' => 'Event Deleted Successfully'
            ]);

        } else {
            return redirect('calendar')->with([
                'message' => 'Event Not Found',
                'message_important' => true
            ]);
        }
    }

    /* postEditCalendar  Function Start Here */
    public function postEditCalendar(Request $request)
    {

        $cmd = Input::get('cmd');

        $v = \Validator::make($request->all(), [
            'occasion' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('calendar/view-calendar/' . $cmd)->withErrors($v->errors());
        }

        $calendar = Calendar::find($cmd);
        if ($calendar) {
            $start_date             = Input::get('start_date');
            $start_date             = get_date_format_inggris($start_date);
            $end_date               = Input::get('end_date');
            $end_date               = get_date_format_inggris($end_date);
            $starts_at              = Input::get('start_time');
            $starts_at              = date('H:i:s',strtotime($starts_at));
            $ends_at                = Input::get('end_time');
            $ends_at                = date('H:i:s',strtotime($ends_at));
            $event                  = Input::get('warna');
            
            $calendar->start_date   = $start_date;
            $calendar->end_date     = $end_date;
            $calendar->starts_at    = $starts_at;
            $calendar->ends_at      = $ends_at;
            $calendar->occasion     = $request->occasion;
            $calendar->description  = $request->description;
            $calendar->location     = $request->location;
            $calendar->event        = $event;
            $calendar->save();

            return redirect('calendar')->with([
                'message' => 'Event Updated Successfully'
            ]);

        } else {
            return redirect('calendar')->with([
                'message' => 'Event Already Exist',
                'message_important' => true
            ]);
        }
    }
    
    /* sendSMSNotification  Function Start Here */
    public function sendSMSNotification(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'phone' => 'required', 'message' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('calendar')->withErrors($v->errors());
        }

        $phone = Input::get('phone');
        $message = Input::get('message');
        $sender_id=app_config('AppName');

        $sms_gateway = SMSGateway::where('status', 'Active')->first();
        $gateway_name = $sms_gateway->name;

        if ($gateway_name == 'Twilio') {
            $sid = $sms_gateway->user_name;
            $token = $sms_gateway->password;

            try {

                $client = new Client($sid, $token);
                $response = $client->account->messages->create(
                    "$phone",
                    array(
                        'from' => '+19383333659',
                        'body' => $message
                    )
                );

                if ($response->sid!='') {
                    return redirect('calendar')->with([
                        'message' => language_data('SMS sent successfully')
                    ]);
                } else {
                    return redirect('calendar')->with([
                        'message' => language_data('Please check your Twilio Credentials'),
                        'message_important'=>true
                    ]);
                }
            } catch (Exception $e) {
                return redirect('calendar')->with([
                    'message' => language_data('Please check your Twilio Credentials'),
                    'message_important'=>true
                ]);
            }


        } elseif ($gateway_name == 'Route SMS') {

            $sms_url=$sms_gateway->api_link;
            $user=$sms_gateway->user_name;
            $password=$sms_gateway->password;

            $sms_sent_to_user = "$sms_url" . "?type=0" . "&username=$user" . "&password=$password" ."&destination=$phone" . "&source=$sender_id" . "&message=$message" . "&dlr=1";
            $get_sms_status=file_get_contents($sms_sent_to_user);
            $get_sms_status = str_replace("1701", language_data('Success'), $get_sms_status);
            $get_sms_status = str_replace("1709", language_data('User Validation Failed'), $get_sms_status);
            $get_sms_status = str_replace("1025", language_data('Insufficient Credit'), $get_sms_status);
            $get_sms_status = str_replace("1710", language_data('Internal Error'), $get_sms_status);
            $get_sms_status = str_replace("1706", language_data('Invalid receiver'), $get_sms_status);
            $get_sms_status = str_replace("1705", language_data('Invalid SMS'), $get_sms_status);
            $get_sms_status = str_replace("1707", language_data('Invalid sender'), $get_sms_status);
            $get_sms_status = str_replace(",", " ", $get_sms_status);
            $pos = strpos($get_sms_status, language_data('Success'));

            if ($pos === false) {
                return redirect('calendar')->with([
                    'message' => language_data('SMS sent successfully')
                ]);
            } else {
                return redirect('calendar')->with([
                    'message' => $get_sms_status,
                    'message_important'=>true
                ]);
            }



        } elseif ($gateway_name == 'Bulk SMS') {

            $sms_url=$sms_gateway->api_link;
            $user=$sms_gateway->user_name;
            $password=$sms_gateway->password;


            $url = "$sms_url" . "/eapi/submission/send_sms/2/2.0?username=$user" . "&password=$password" ."&msisdn=$phone" ."&message=".urlencode($message);


            $ret = file_get_contents($url);

            $send = explode("|",$ret);

            if ($send[0]=='0') {
                $get_sms_status= language_data('In progress');
            }elseif($send[0]=='1'){
                $get_sms_status= language_data('Scheduled');
            }elseif($send[0]=='22'){
                $get_sms_status= language_data('Internal Error');
            }elseif($send[0]=='23'){
                $get_sms_status= language_data('Authentication failure');
            }elseif($send[0]=='24'){
                $get_sms_status= language_data('Data validation failed');
            }elseif($send[0]=='25'){
                $get_sms_status= language_data('Insufficient Credit');
            }elseif($send[0]=='26'){
                $get_sms_status= language_data('Upstream credits not available');
            }elseif($send[0]=='27'){
                $get_sms_status= language_data('You have exceeded your daily quota');
            }elseif($send[0]=='28'){
                $get_sms_status= language_data('Upstream quota exceeded');
            }elseif($send[0]=='40'){
                $get_sms_status= language_data('Temporarily unavailable');
            }elseif($send[0]=='201'){
                $get_sms_status= language_data('Maximum batch size exceeded');
            }elseif($send[0]=='200'){
                $get_sms_status= language_data('Success');
            }
            else{
                $get_sms_status= language_data('Failed');
            }

            if($get_sms_status=='Success'){
                return redirect('calendar')->with([
                    'message' => language_data('SMS sent successfully')
                ]);
            }else{
                return redirect('calendar')->with([
                    'message' => $get_sms_status,
                    'message_important'=>true
                ]);
            }

        } else {
            return redirect('calendar')->with([
                'message' => language_data('Gateway information not found'),
                'message_important' => true
            ]);
        }

    }



}
