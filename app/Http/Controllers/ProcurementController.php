<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\EmployeeRolesPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Classes\permission;
use App\Company;
use App\Notification;
use App\Project;
use App\ProjectItem;
use App\Procurement;
use App\ProcurementGoods;
use App\Employee;
use DB;
use WkPdf;
class ProcurementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* procurements  Function Start Here */
    public function procurements()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 10)->first();
        $procurements = Procurement::orderBy('id','asc')->get();
        $company = Company::find(\Auth::user()->company);
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        $last_letter_number = DB::table('sys_procurements')->orderBy('id','asc')->value('procurement_number');
        if($last_letter_number){
            $last_number = explode('/', $last_letter_number);

        }
        else{
            $last_number = 0;
        }
        if($date=='01'){
            $number = 01;
        } else{
            $number = 1 + $last_number[0];
        }
        $comp = Company::find(\Auth::user()->company);
        return view('admin.procurement.procurements', compact('procurements','company','date','month','year','number','comp','permcheck'));

    }

    public function printRealizationProcurement($id){
        // $pdf = WkPdf::loadView('admin.procurement.pdf-realization');

        // return $pdf->stream('assessment_form.pdf');
        $employee = \Auth::user()->id;
        $employee  = Employee::find($employee);
    
        $procurement=Procurement::find($id);
        $procurement_goods=ProcurementGoods::where('procurement','=',$id)->get();
        $total=0;
        $sisa=0;
        foreach($procurement_goods as $p){
            if($p->realization_total_price){
            $total+=$p->realization_total_price;}
            if($p->realization_difference){
                $sisa+=$p->realization_difference;
            }
        }
         $pdf = WkPdf::loadView('admin.procurement.pdf-realization',compact('procurement','procurement_goods','total','sisa','employee'));

         return $pdf->stream('RealisasiPengadaan.pdf');
       

    }
    public function printPayoutProcurement($id){
        $employee = \Auth::user()->id;
        $employee  = Employee::find($employee);
        $procurement=Procurement::find($id);
        $procurement_goods=ProcurementGoods::where('procurement','=',$id)->get();
        $total=0;
        $sisa=0;
        foreach($procurement_goods as $p){
            if($p->received_total_price){
            $total+=$p->received_total_price;}
            if($p->realization_difference){
                $sisa+=$p->realization_difference;
            }
        }
        $pdf = WkPdf::loadView('admin.procurement.pdf-payout',compact('procurement','procurement_goods','total','sisa','employee'));

        return $pdf->stream('PembayaranPengadaan.pdf');

    }
    public function printProcureProcurement($id){
        $employee = \Auth::user()->id;
        $employee  = Employee::find($employee);
        $procurement=Procurement::find($id);
        $procurement_goods=ProcurementGoods::where('procurement','=',$id)->get();
        $total=0;
   
        foreach($procurement_goods as $p){
            if($p->total_price){
            $total+=$p->total_price;}
          
        }
        $pdf = WkPdf::loadView('admin.procurement.pdf-procure',compact('procurement','procurement_goods','total','employee'));

        return $pdf->stream('Pengadaan.pdf');

    }
    /* addProcurement  Function Start Here */
    public function addProcurement(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'date' => 'required',
            'supplier' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('procurements')->withErrors($v->errors());
        }

        $procurement_number = Input::get('procurement_number');
        $company = Input::get('company');
        $perihal = Input::get('perihal');
        $project = Input::get('project');
        $type = Input::get('type');
        $category = Input::get('category');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $supplier = Input::get('supplier');
        $total = (int)str_replace(',', '', Input::get('total'));

        if($type == "internal"){
            $project = \Auth::user()->project;
        }
        //Penomoran id
        $last_number_id = Procurement::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $procurements = new Procurement();
        $procurements->id = $number_id;
        $procurements->perihal = $perihal;
        $procurements->procurement_number = $procurement_number;
        $procurements->company = $company;
        $procurements->project = $project;
        $procurements->type = $type;
        $procurements->category = $category;
        $procurements->date = $date;
        $procurements->supplier = $supplier;
        $procurements->total = $total;
        $procurements->deficiency = $total;
        $procurements->save();

        $procurement_id = $procurements->id;
        
        $notification = new Notification();
        $notification->id_tag = $procurement_id;
        $notification->tag = 'procurements';
        $notification->title = 'Persetujuan Pengadaan';
        $notification->description = 'Persetujuan Pengadaan '.$procurement_number;
        $notification->show_date = date('Y-m-d');
        $notification->start_date = $date;
        $notification->end_date = $date;
        $notification->route = 'approvals/procurements';
        $notification->save();

        $item = Input::get('item');
        $qty = Input::get('qty');
        $unit = Input::get('unit');
        $unit_price = Input::get('unit_price');
        $total_price = Input::get('total_price');

        for($a=0;$a<count($item);$a++){
            //Penomoran id
            $last_number_id = ProcurementGoods::max('id');
            if($last_number_id==''){
                $number_id = 1;
            } else {
                $number_id = 1 + $last_number_id;
            }
            $procurement_goods = new ProcurementGoods();
            $procurement_goods->id = $number_id;
            $procurement_goods->procurement = $procurement_id;
            $procurement_goods->goods = $item[$a];
            $procurement_goods->quantity = $qty[$a];
            $procurement_goods->unit = $unit[$a];
            $procurement_goods->unit_price = (int)str_replace(',', '', $unit_price[$a]);
            $procurement_goods->total_price = (int)str_replace(',', '', $total_price[$a]);
            $procurement_goods->save();
            
        }

        if ($procurements!='') {
            return redirect('procurements')->with([
                'message' => 'Procurement Added Successfully'
            ]);

        } else {
            return redirect('procurements')->with([
                'message' => 'Procurement Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateProcurement  Function Start Here */
    public function updateProcurement(Request $request)
    {
        $cmd = Input::get('cmd');

        $procurements = Procurement::find($cmd);
        $procurement_goods = ProcurementGoods::where('procurement','=', $cmd)->get();

        $reception_number = Input::get('reception_number');
        $reception_date = Input::get('reception_date');
        $reception_date = get_date_format_inggris($reception_date);

        $reception_file = Input::file('reception_file');
        if($reception_file!=''){    
            $reception_path = Input::file('reception_file')->getRealPath();
            $reception_target_file = $reception_path . basename($_FILES["reception_file"]["name"]);
            $reception_fileType = pathinfo($reception_target_file,PATHINFO_EXTENSION);
        }

        $reception_quantity = Input::get('reception_quantity');

        $realization_number = Input::get('realization_number');
        $realization_date = Input::get('realization_date');
        $realization_date = get_date_format_inggris($realization_date);
        $realization_total = (int)str_replace(',', '', Input::get('realization_total'));
        $realization_differences = (int)str_replace(',', '', Input::get('realization_differences'));
        $realization_status = 'draft';

        $payout_number = Input::get('payout_number');
        $payout_date = Input::get('payout_date');
        $payout_date = get_date_format_inggris($payout_date);

        $payout_file = Input::file('payout_file');
        if($payout_file!=''){    
            $payout_path = Input::file('payout_file')->getRealPath();
            $payout_target_file = $payout_path . basename($_FILES["payout_file"]["name"]);
            $payout_fileType = pathinfo($payout_target_file,PATHINFO_EXTENSION);
        }

        $payout_status = 'draft';       

        if ($procurements->category=='petty-cash') {
            $realization_quantity = Input::get('realization_quantity');
            $realization_unit_price = Input::get('realization_unit_price');
            $realization_total_price = Input::get('realization_total_price');
            $realization_difference = Input::get('realization_difference');

            $procurements->total = $realization_total;
            $procurements->realization_number = $realization_number;
            $procurements->realization_date = $realization_date;
            $procurements->realization_status = $realization_status;
            $procurements->realization_total = $realization_total;
            $procurements->realization_differences = $realization_differences;
            $procurements->deficiency = $realization_differences;
            $procurements->process_status = 'realization';
            $procurements->save();

            ProcurementGoods::where('procurement','=', $cmd)->delete();

            $item = Input::get('item');
            $qty = Input::get('qty');
            $unit = 'pcs';

            for($a=0;$a<count($item);$a++){
                //Penomoran id
                $last_number_id = ProcurementGoods::max('id');
                if($last_number_id==''){
                    $number_id = 1;
                } else {
                    $number_id = 1 + $last_number_id;
                }
                $procurement_goods_petty_cash = new ProcurementGoods();
                $procurement_goods_petty_cash->id = $number_id;
                $procurement_goods_petty_cash->procurement = $cmd;
                $procurement_goods_petty_cash->goods = $item[$a];
                $procurement_goods_petty_cash->quantity = $qty[$a];
                $procurement_goods_petty_cash->unit = $unit;
                $procurement_goods_petty_cash->unit_price = (int)str_replace(',', '', $realization_unit_price[$a]);
                $procurement_goods_petty_cash->total_price = (int)str_replace(',', '', $realization_total_price[$a]);
                $procurement_goods_petty_cash->realization_quantity = $realization_quantity[$a];
                $procurement_goods_petty_cash->realization_unit_price = (int)str_replace(',', '', $realization_unit_price[$a]);
                $procurement_goods_petty_cash->realization_total_price = (int)str_replace(',', '', $realization_total_price[$a]);
                $procurement_goods_petty_cash->save();
                
            }

            return redirect('procurements')->with([
                'message' => 'Procurement Updated Successfully'
            ]);

        } else if ($procurements->category=='subsist' && ($reception_fileType == 'jpg' || $reception_fileType == 'png' || $reception_fileType == 'jpeg' || $reception_fileType == 'pdf')) {
            $destinationPath = public_path() . '/assets/procurement_files/';
            $file_name = $reception_file->getClientOriginalName();
            Input::file('reception_file')->move($destinationPath, $file_name);

            $realization_quantity = Input::get('realization_quantity');
            $realization_unit_price = Input::get('realization_unit_price');
            $realization_total_price = Input::get('realization_total_price');
            $realization_difference = Input::get('realization_difference');

            $procurements->reception_number = $reception_number;
            $procurements->reception_date = $reception_date;
            $procurements->reception_file = $file_name;
            $procurements->realization_number = $realization_number;
            $procurements->realization_date = $realization_date;
            $procurements->realization_status = $realization_status;
            $procurements->realization_total = $realization_total;
            $procurements->realization_differences = $realization_differences;
            $procurements->deficiency = $realization_differences;
            $procurements->process_status = 'realization';
            $procurements->save();

            $a = 0;
            foreach($procurement_goods as $goods){
                $goods->reception_quantity = $reception_quantity[$a];
                $goods->realization_quantity = $realization_quantity[$a];
                $goods->realization_unit_price = (int)str_replace(',', '', $realization_unit_price[$a]);
                $goods->realization_total_price = (int)str_replace(',', '', $realization_total_price[$a]);
                $goods->realization_difference = (int)str_replace(',', '', $realization_difference[$a]);
                $goods->save();
                $a++;
            }

            return redirect('procurements')->with([
                'message' => 'Procurement Updated Successfully'
            ]);

        } else if ($procurements->category=='non-subsist' && ($reception_fileType == 'jpg' || $reception_fileType == 'png' || $reception_fileType == 'jpeg' || $reception_fileType == 'pdf') && ($payout_fileType == 'jpg' || $payout_fileType == 'png' || $payout_fileType == 'jpeg' || $payout_fileType == 'pdf')) {
            $destinationPath = public_path() . '/assets/procurement_files/';
            $file_name = $reception_file->getClientOriginalName();
            Input::file('reception_file')->move($destinationPath, $file_name);

            $file_name_payout = $payout_file->getClientOriginalName();
            Input::file('payout_file')->move($destinationPath, $file_name_payout);

            $procurements->reception_number = $reception_number;
            $procurements->reception_date = $reception_date;
            $procurements->reception_file = $file_name;
            $procurements->payout_number = $payout_number;
            $procurements->payout_date = $payout_date;
            $procurements->payout_status = $payout_status;
            $procurements->payout_file = $file_name_payout;
            $procurements->process_status = 'payout';
            $procurements->save();

            $a = 0;
            foreach($procurement_goods as $goods){
                $goods->reception_quantity = $reception_quantity[$a];
                $goods->save();
                $a++;
            }

            return redirect('procurements')->with([
                'message' => 'Procurement Updated Successfully'
            ]);

        } else {
            return redirect('procurements')->with([
                'message' => 'Upload an Image or PDF',
                'message_important' => true
            ]);
        }
    }


    /* deleteProcurement  Function Start Here */
    public function deleteProcurement($id)
    {

        $exist_check = Employee::where('procurements', $id)->where('role_id','!=','1')->first();

        if ($exist_check) {
            return redirect('procurements')->with([
                'message' => 'Employee added on this procurements. to remove: unassigned employee',
                'message_important' => true
            ]);
        }

        $procurements = Procurement::find($id);
        if ($procurements) {

            Designation::where('did', $id)->delete();
            $procurements->delete();

            return redirect('procurements')->with([
                'message' => 'Procurement Deleted Successfully'
            ]);
        } else {
            return redirect('procurements')->with([
                'message' => 'Procurement Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewProcurement  Function Start Here */
    public function viewProcurement($id)
    {

        $procurements = Procurement::find($id);
        $project = Project::where('status','=','opening')->get();
        $procurement_goods = ProcurementGoods::where('procurement','=',$id)->get();
        return view('admin.procurement.view-procurement', compact('procurements','project','procurement_goods'));
    }

    /* realizationProcurement  Function Start Here */
    public function realizationProcurement($id)
    {

        $procurements = Procurement::find($id);
        $project = Project::where('status','=','opening')->get();
        $procurement_goods = ProcurementGoods::where('procurement','=',$id)->get();
        return view('admin.procurement.realize-procurement', compact('procurements','project','procurement_goods'));
    }

    /* payoutProcurement  Function Start Here */
    public function payoutProcurement($id)
    {
        $procurements = Procurement::find($id);
        $project = Project::where('status','=','opening')->get();
        $procurement_goods = ProcurementGoods::where('procurement','=',$id)->get();
        return view('admin.procurement.payout-procurement', compact('procurements','project','procurement_goods'));
    }

    /* wizardProcurement  Function Start Here */
    public function wizardProcurement($id)
    {
        $procurements = Procurement::find($id);
        $project = Project::where('status','=','opening')->get();
        $procurement_goods = ProcurementGoods::where('procurement','=',$id)->get();
        
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        $last_letter_number = DB::table('sys_procurements')->orderBy('id','asc')->value('reception_number');
        if( $last_letter_number ){$last_number = explode('/', $last_letter_number);}
            else{$last_number=0;}
  
        if($date=='01'){
            $number = 01;
        } else{
            $number = 1 + $last_number[0];
        }
        $last_letter_number_realization = DB::table('sys_procurements')->whereNotNull('realization_number')->orderBy('id','asc')->value('realization_number');
       if( $last_letter_number_realization){
        $last_number_realization = explode('/', $last_letter_number_realization);

       }
        else{
            $last_number_realization=0;
        }
        if($date=='01'){
            $number_realization = 01;
        } else{
            $number_realization = 1 + $last_number_realization[0];
        }
        $last_letter_number_payout = DB::table('sys_procurements')->orderBy('id','asc')->value('payout_number');
       
        if( $last_letter_number_payout ){$last_number_payout = explode('/', $last_letter_number);}
            else{$last_number_payout=0;}
        
        if($date=='01'){
            $number_payout = 01;
        } else{
            $number_payout = 1 + $last_number_payout[0];
        }
        return view('admin.procurement.wizard-procurement', compact('procurements','project','procurement_goods','number','number_realization','number_payout','year','month'));
    }

    /* wizard2Procurement  Function Start Here */
    public function wizard2Procurement($id)
    {
        $procurements = Procurement::find($id);
        $project = Project::where('status','=','opening')->get();
        $procurement_goods = ProcurementGoods::where('procurement','=',$id)->get();
        
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        $last_letter_number = DB::table('sys_procurements')->orderBy('id','asc')->value('reception_number');
        $last_number = explode('/', $last_letter_number);
        if($date=='01'){
            $number = 01;
        } else{
            $number = 1 + $last_number[0];
        }
        $last_letter_number_realization = DB::table('sys_procurements')->orderBy('id','asc')->value('realization_number');
        $last_number_realization = explode('/', $last_letter_number_realization);
        if($date=='01'){
            $number_realization = 01;
        } else{
            $number_realization = 1 + $last_number_realization[0];
        }
        $last_letter_number_payout = DB::table('sys_procurements')->orderBy('id','asc')->value('payout_number');
        $last_number_payout = explode('/', $last_letter_number_payout);
        if($date=='01'){
            $number_payout = 01;
        } else{
            $number_payout = 1 + $last_number_payout[0];
        }
        return view('admin.procurement.wizard2-procurement', compact('procurements','project','procurement_goods','number','number_realization','number_payout','year','month'));
    }

    /* downloadPayout  Function Start Here */
    public function downloadPayout($id)
    {

        $file = Procurement::find($id)->payout_file;
        return response()->download(public_path('/assets/procurement_files/' . $file));
    }

    
    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $type = $request->type;
        if ($type=='project') {
            $project = Project::where('status','=','opening')->get();
            echo '<option value="0">Select Project</option>';
            foreach($project as $c){
                $project_item = ProjectItem::where('id_project','=',$c->id)->get()->toArray();
                if(sizeof($project_item)!=0){
                    echo '<option value="' . $c->id . '">'. $c->project . '</option>';
                }
            }
        } else {
            echo '';
        }
    }

    /* getProjectItem Function Start Here */
    public function getProjectItem(Request $request)
    {

        $id = $request->id;
        if ($id) {
            $project_item = ProjectItem::where('id_project','=',$id)->get()->toArray();
            // echo "<pre>"; print_r($project_item); exit;
            if($project_item){
                return $project_item;
            } 
        }
    }

    /* getProjectItemDetail Function Start Here */
    public function getProjectItemDetail(Request $request)
    {

        $id = $request->id;
        if ($id) {
            $project_item_detail = ProjectItem::find($id)->toArray();
            // echo "<pre>"; print_r($project_item_detail); exit;
            if($project_item_detail){
                return $project_item_detail;
            } 
        }
    }


}
