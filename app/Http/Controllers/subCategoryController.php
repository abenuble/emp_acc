<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Subcategory;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;

date_default_timezone_set(app_config('Timezone'));

class SubcategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* subCategories  Function Start Here */
    public function subCategories()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 21)->first();
        $subcategories = Subcategory::orderBy('id','asc')->get();
        return view('admin.subcategory.subCategories', compact('subcategories','permcheck'));

    }

    /* editSubcategory  Function Start Here */
    public function editSubcategory($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 21)->first();
        $subcategories = Subcategory::find($id);
        return view('admin.subcategory.edit-subcategory', compact('subcategories','permcheck'));

    }

    /* addSubcategory  Function Start Here */
    public function addSubcategory(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'subcategory' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('subcategories')->withErrors($v->errors());
        }
        $subcategory = Input::get('subcategory');
        $status = Input::get('status');

        $exist = Subcategory::where('subcategory', $subcategory)->first();
        if ($exist) {
            return redirect('subcategories')->with([
                'message' => 'Sub Category Already Exist',
                'message_important' => true
            ]);
        }

        //Penomoran id
        $last_number = Subcategory::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }
        $subCategories = new Subcategory();
        $subCategories->id = $number;
        $subCategories->subcategory = $subcategory;
        $subCategories->status = $status;
        $subCategories->save();

        if ($subCategories!='') {
            return redirect('subcategories')->with([
                'message' => 'Sub Category Added Successfully'
            ]);

        } else {
            return redirect('subcategories')->with([
                'message' => 'Sub Category Already Exist',
                'message_important' => true
            ]);
        }


    }


    /* updateSubcategory  Function Start Here */
    public function updateSubcategory(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'subcategories' => 'required',
            'status'     => 'required'
        ]);

        if ($v->fails()) {
            return redirect('subcategories')->withErrors($v->errors());
        }
        $subcategory = Subcategory::find($cmd);
        $subcategory_name = Input::get('subcategories');
        $status = Input::get('status');

        if ($subcategory) {
            $subcategory->subcategory = $subcategory_name;
            $subcategory->status = $status;
            $subcategory->save();

            return redirect('subcategories')->with([
                'message' => 'Sub Category Added Successfully'
            ]);

        } else {
            return redirect('subcategories')->with([
                'message' => 'Sub Category Not Found',
                'message_important' => true
            ]);
        }
    }

    /* download excel function start here */
    public function downloadExcel()
    {
        return response()->download(public_path('assets/template_xls/sub_category.xls'));

    }

    /* import excel function start here */
    public function importExcel()
    {
        // Get current data from subcategory table
        $subcategory = Subcategory::lists('subcategory')->toArray();
        
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file   = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
            })->get();
            if ($fileType != 'xls'){
                return redirect('companies')->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else {
                if(!empty($data) && $data->count()){
                    $insert = array();
                    foreach ($data as $key => $value) {
                        // Skip subcategory previously added using in_array
                        if (in_array($value->sub_kategori, $subcategory)){
                            continue;
                        } else{
                            //Penomoran id
                            $last_number = Subcategory::max('id');
                            if($last_number==''){
                                $number = 1;
                            } else {
                                $number = 1 + $last_number;
                            }
                            if ($value->sub_kategori=='') {
                                return redirect('subcategories')->with([
                                    'message' => 'Sub Category Not Found',
                                    'message_important' => true
                                ]);
                            } 
                            $subCategories = new Subcategory();
                            $subCategories->id = $number;
                            $subCategories->subcategory = $value->sub_kategori;
                            $subCategories->status='active';
                            $subCategories->save();
                        }
                    }
                    return redirect('subcategories')->with([
                        'message' => 'Sub Category Added Successfully'
                    ]);
                } else{
                    return redirect('subcategories')->with([
                        'message' => 'Sub Category Not Found',
                        'message_important' => true
                    ]);
                } 
            }
			
		}
		return back();
    }

}
