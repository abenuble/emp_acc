<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\Employee;
use App\Company;
use App\Component;
use App\Notification;
use App\Project;
use App\Payment;
use App\PaymentComponent;
use App\PayrollTypes;
use App\PayrollComponent;
use App\EmployeeRolesPermission;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;
date_default_timezone_set(app_config('Timezone'));

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* payments  Function Start Here */
    public function payments()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 53)->first();
        $comp_id = '';
        $proj_id = '';

        $payments = Payment::orderBy('id','asc')->get();
        $payroll = PayrollTypes::all();
        $company = Company::all();
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        $last_letter_number = DB::table('sys_payments')->orderBy('id','asc')->value('letter_number');
        if(  $last_letter_number){
            $last_number = explode('/', $last_letter_number);
        }
            else{
                $last_number=0;
            }
       
        if($date=='01'){
            $number = 01;
        } else{
            $number = 1 + $last_number[0];
        }
        $comp = Company::find(\Auth::user()->company);
        return view('admin.payment.payments', compact('payments','company','comp_id','proj_id','payroll','date','year','month','number','comp','permcheck'));

    }
    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project . '</option>';
            }
        }
    }

    /* getProjectNumber Function Start Here */
    public function getProjectNumber(Request $request)
    {

        $proj_id = $request->proj_id;
        if ($proj_id) {
            $project = Project::find($proj_id);
            echo '<label>Project Number</label>';
            echo '<input type="text" readonly class="form-control" required="" value="' . $project->project_number . '"  name="project_number">';
        }
    }

    /* deletePayment  Function Start Here */
    public function deletePayment($id)
    {

        $payments = Payment::find($id);
        if ($payments) {
            $payments->delete();
            
            $payment_component = PaymentComponent::where('payment','=',$id)->delete();

            return redirect('payments')->with([
                'message' => 'Payment Deleted Successfully'
            ]);
        } else {
            return redirect('payments')->with([
                'message' => 'Payment Not Found',
                'message_important' => true
            ]);
        }

    }


    /* addPayment  Function Start Here */
    public function addPayment(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'month' => 'required',
            'project_number' => 'required',
            'project' => 'required',
            'company' => 'required',
            'payroll_type' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('payments')->withErrors($v->errors());
        }
        $letter_number = Input::get('letter_number');
        $tembusan = Input::get('tembusan');
        $month = Input::get('month');
        $month = get_date_format_inggris_bulan($month);
        $project = Input::get('project');
        $company = Input::get('company');
        $payroll_type = Input::get('payroll_type');

        $exist = Payment::where('project', $project)->where('month','=',$month)->where('company','=',$company)->where('payroll_type','=',$payroll_type)->first();
        if ($exist) {
            return redirect('payments')->with([
                'message' => 'Payment Already Exist',
                'message_important' => true
            ]);
        }
        $payroll_components = PayrollComponent::where('id_payroll_type','=',$payroll_type)->pluck('id_component')->toArray();
        $columns = Component::all()->toArray();
        //Penomoran id
        $last_number = Payment::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }
        $payments = new Payment();
        $payments->id = $number;
        $payments->tembusan = $tembusan;
        $payments->letter_number = $letter_number;
        $payments->month = $month;
        $payments->project = $project;
        $payments->company = $company;
        $payments->payroll_type = $payroll_type;
        $payments->save();
        
        $notification = new Notification();
        $notification->id_tag = $payments->id;
        $notification->tag = 'payments';
        $notification->title = 'Persetujuan Pembayaran Gaji';
        $notification->description = 'Persetujuan Pembayaran Gaji '.$letter_number;
        $notification->show_date = date('Y-m-d');
        $notification->start_date = date('Y-m-t', strtotime($month));
        $notification->end_date = date('Y-m-t', strtotime($month));
        $notification->route = 'approvals/payments';
        $notification->save();

        $employee = Employee::where('project','=',$project)->where('role_id','!=',1)->where('payment_type','=',$payroll_type)->get();

        $data2 = array();
        $ctr = 0;
        foreach($employee as $e){
            if($e->status=='active'){
                $data2[$ctr]['emp_id'] = $e->id;
                $data2[$ctr]['payment'] = $payments->id;
                $data2[$ctr]['month'] = $month;
                // $data2[$ctr]['Salary'] = $e->salary;
                // $data2[$ctr]['Attendance_Allowance'] = $e->attendance_allowance;
                $data2[$ctr]['total'] = $e->salary + $e->attendance_allowance;
                foreach($columns as $q){
                    if(in_array($q['id'],$payroll_components)){
                        if(str_replace(' ', '_', $q['component']) == 'Salary'){
                            $data2[$ctr][str_replace(' ', '_', $q['component'])] = $e->salary;
                        } else if(str_replace(' ', '_', $q['component']) == 'Attendance_Allowance') {
                            $data2[$ctr][str_replace(' ', '_', $q['component'])] = $e->attendance_allowance;
                        } else {
                            $data2[$ctr][str_replace(' ', '_', $q['component'])] = 0;
                        }
                    }
                }
                $ctr++;
            } else {
                $data2[$ctr]['emp_id'] = $e->id;
                $data2[$ctr]['payment'] = $payments->id;
                $data2[$ctr]['month'] = $month;
                // $data2[$ctr]['Salary'] = 0;
                // $data2[$ctr]['Attendance_Allowance'] = 0;
                $data2[$ctr]['total'] = 0;
                foreach($columns as $q){
                    if(in_array($q['id'],$payroll_components)){
                        if(str_replace(' ', '_', $q['component']) == 'Salary'){
                            $data2[$ctr][str_replace(' ', '_', $q['component'])] = 0;
                        } else if(str_replace(' ', '_', $q['component']) == 'Attendance_Allowance') {
                            $data2[$ctr][str_replace(' ', '_', $q['component'])] = 0;
                        } else {
                            $data2[$ctr][str_replace(' ', '_', $q['component'])] = 0;
                        }
                    }
                }
                $ctr++;
            }
        }

        if ($payments!='') {
            $payment_component = PaymentComponent::insert($data2);
            return redirect('payments')->with([
                'message' => 'Payment Added Successfully'
            ]);

        } else {
            return redirect('payments')->with([
                'message' => 'Payment Already Exist',
                'message_important' => true
            ]);
        }

        


    }
    public function updatePayment(Request $request)
    {
        $cmd = Input::get('cmd');
        $month = Input::get('month');
        $month = get_date_format_inggris_bulan($month);
        $tembusan = Input::get('tembusan');

        $v = \Validator::make($request->all(), [
            'month' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('payments')->withErrors($v->errors());
        }
        $payment = Payment::find($cmd);
    

        if ($payment) {
            $payment->month = $month;
            $payment->tembusan = $tembusan;

            $payment->status = 'draft';
            $payment->save();
            
            $notification = Notification::where('tag','=','payments')->where('id_tag','=',$cmd)->first();
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->start_date = date('Y-m-t', strtotime($month));
                $notification->end_date = date('Y-m-t', strtotime($month));
                $notification->save();
            }

            return redirect('payments')->with([
                'message' => 'Resign Updated Successfully'
            ]);

        } else {
            return redirect('payments')->with([
                'message' => 'Resign Not Found',
                'message_important' => true
            ]);
        }
    }
    public function editPayment($id)
    {
        
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 53)->first();
        $payments = Payment::find($id);
        $payroll_components = PayrollComponent::where('id_payroll_type','=',$payments->payroll_type)->get();
        $payment_components = PaymentComponent::where('payment','=', $id)->get()->toArray();
        $employee = PaymentComponent::where('payment','=', $id)->get();
        $employee_name = array();
        foreach($employee as $e){
            $employee_name[] = $e->employee_info->fname.' '.$e->employee_info->lname;
        }
        $payment_columns = Component::where('status','=', 'active')->get();
        return view('admin.payment.edit-payment', compact('payments','payment_components','payment_columns','payroll_components','employee_name','permcheck'));
    }
    /* viewPayment  Function Start Here */
    public function viewPayment($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 53)->first();
        
        $payments = Payment::find($id);
        $payroll_components = PayrollComponent::where('id_payroll_type','=',$payments->payroll_type)->get();
        $payment_components = PaymentComponent::where('payment','=', $id)->get()->toArray();
        $employee = PaymentComponent::where('payment','=', $id)->get();
        $employee_name = array();
        foreach($employee as $e){
            $employee_name[] = $e->employee_info->fname.' '.$e->employee_info->lname;
        }
        $payment_columns = Component::where('status','=', 'active')->get();
        return view('admin.payment.view-payment', compact('payments','payment_components','payment_columns','payroll_components','employee_name','permcheck'));
    }

    /* download excel function start here */
    public function downloadExcel(Request $request)
    {
        $payment = Input::get('payment');
        $payments = Payment::find($payment);
        $payroll_components = PayrollComponent::where('id_payroll_type','=',$payments->payroll_type)->get();
        $payment_component = PaymentComponent::where('payment','=',$payment)->get()->toArray();
        $employee = PaymentComponent::where('payment','=', $payment)->get();
        $employee_code = array();
        $employee_name = array();
        foreach($employee as $e){
            $employee_name[] = $e->employee_info->fname.' '.$e->employee_info->lname;
            $employee_code[] = $e->employee_info->employee_code;
        }

        $data = array();
        $a = 1;
        $ctr = 0;
        foreach($payment_component as $p => $value){

            $data[$ctr]['No']                   = $a;
            $data[$ctr]['NIK']                  = $employee_code[$ctr];
            $data[$ctr]['Nama Pegawai']         = $employee_name[$ctr];
            foreach($payroll_components as $comp){
                $data[$ctr][$comp->component_name->component] = $value[str_replace(' ','_',$comp->component_name->component)];
            }
            $data[$ctr]['total'] = $value['total'];
            $a++;
            $ctr++;
        }
        if(!empty($data)){
            return Excel::create('payments', function($excel) use ($data) {
                $excel->sheet('mySheet', function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download('xls');
         }else {
            return redirect('payments/view/'.$payment)->with([
                'message' => 'Payment Not Found',
                'message_important' => true
            ]);
        }
        
    }

    public function DownloadComponent(Request $request){
        $namapayroll=Input::get('namapayroll');
      
        $data = Input::get('variable');
        $payment = Input::get('payment');
        $data = str_replace(" ","_",$data,$i);
        $payment_components = PaymentComponent::where('payment','=', $payment)->get();

        $dat = array();
        $sum=array();
        foreach($data as $d){
           
            $sum[0][$d]=0;
          }
        $a = 0;
        $ctr=1;
        foreach($payment_components as $p ){
            $emp= Employee::find($p->emp_id);

          
            
            $dat[$a] = [
                'No' => $ctr,
                'Employee Name' => $emp->fname." ".$emp->lname, 
                'Employee Code' => $emp->employee_code,
                'Payroll ' => $namapayroll,
             
            ];
              foreach($data as $d){
                  if($p->$d){
                         $dat[$a][$d]=$p->$d ; 
                         $sum[0][$d]+=$p->$d ;
                }
                  else{
                         $dat[$a][$d]=0; 
                }
              
              }
            if($ctr==sizeof($payment_components)){
                $dat[$ctr] = [
                    'No' => "",
                    'Employee Name' => "", 
                    'Employee Code' => "",
                    'Payroll ' => "total : ",
                 
                ];
                foreach($data as $d){
                 
                           $dat[$ctr][$d]=$sum[0][$d] ; 
                }
            }
            $a++;
            $ctr++;
        }
       
        $type = 'xls';
		return Excel::create('Payment_Component', function($excel) use ($dat) {
			$excel->sheet('mySheet', function($sheet) use ($dat)
	        {
				$sheet->fromArray($dat);
	        });
        })->download($type);   
    
    }
      /* download excel function start here */
      public function downloadExcelAll(Request $request)
      {
          $payroll = Input::get('payroll_type');
          if($payroll=="0"){
            $payment = Payment::get();
          }
          else{
          $payment = Payment::where('payroll_type','=',$payroll)->get();}
  
          $data = array();
          $a = 1;
          foreach($payment as $p => $value){
              $idpayment=$value->id;
              $paycomponents= PaymentComponent::where('payment','=',$idpayment)->get();
              $total=0;
              if(sizeof($paycomponents)>0){
                 foreach($paycomponents as $q => $valuee){
                        $total+= $valuee->total;
                  }
              }
              
              $data[] = [
                  'No' => $a,
                  'Project Number' => Project::find($value->project)->project_number, 
                  'Month' => $value->month,
                  'Company' => $value->company_info->company,
                  'Payroll' => $value->payroll_info->payroll_name,
                  'total'   =>$total
               
              ];
              $a++;
          }
          if(!empty($data)){
              return Excel::create('payments', function($excel) use ($data) {
                  $excel->sheet('mySheet', function($sheet) use ($data)
                  {
                      $sheet->fromArray($data);
                  });
              })->download('xls');
           }else {
              return redirect('payments')->with([
                  'message' => 'Payment Not Found',
                  'message_important' => true
              ]);
          }
          
      }
    /* import excel function start here */
    public function importExcel()
    {

        if(Input::hasFile('import_file')){
            $payment = Input::get('payment');
            $month = Input::get('month');
            $month = get_date_format_inggris_bulan($month);
            $path = Input::file('import_file')->getRealPath();
            $target_file   = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
			})->get()->toArray();
            if ($fileType != 'xls'){
                return redirect('payments/view/'.$payment)->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else {
                if(!empty($data) && sizeof($data)){
                    $insert = array();
                    foreach ($data as $key => $value) {
                        
                        $employee = Employee::where('employee_code','=',$value['nik'])->first();

                        $payment_component = PaymentComponent::where('emp_id','=',$employee->id)->where('payment','=',$payment)->first();
                        $payments = Payment::find($payment);
                        $payroll_components = PayrollComponent::where('id_payroll_type','=',$payments->payroll_type)->get();
                        

                        if($payment_component!=''){

                            foreach($payroll_components as $comp){
                                $insert[str_replace(' ','_',$comp->component_name->component)] = $value[strtolower(str_replace(' ','_',$comp->component_name->component))];
                            }
                            $insert['total'] = $value['total'];

                            $payment_component_update = PaymentComponent::where('emp_id','=',$employee->id)
                                            ->where('payment','=',$payment)
                                            ->update($insert);
                        } else {
                            
                            $data['emp_id'] = $employee->id;
                            foreach($payroll_components as $comp){
                                $data[str_replace(' ','_',$comp->component_name->component)] = $value[strtolower(str_replace(' ','_',$comp->component_name->component))];
                            }
                            $data['total'] = $value['total'];
                            $payment_component_insert = PaymentComponent::insert($data);
                        }
    
                    }
                    if($payment_component_update){
                        return redirect('payments/view/'.$payment)->with([
                            'message' => 'Payment Added Successfully'
                        ]);
                    } else if($payment_component_insert){
                        return redirect('payments/view/'.$payment)->with([
                            'message' => 'Payment Added Successfully'
                        ]);
                    } else {
                        return redirect('payments/view/'.$payment)->with([
                            'message' => 'Payment Not Found',
                            'message_important' => true
                        ]);
                    } 
                }

            }
		}
		return back();
    }
    
    /* printPayslip  Function Start Here */
    public function printPayslip($id)
    {
        $payslip = PaymentComponent::find($id)->toarray();
        $tmppayslip= PaymentComponent::find($id);
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        if($payslip){
         
            $payslip['company_id']=$tmppayslip->employee_info->project_name->internal_company;
            $payslip['company_address']=$tmppayslip->employee_info->project_name->internal_company_name->address;
            $payslip['company_phone']=$tmppayslip->employee_info->project_name->internal_company_name->phone_number;
            $payslip['designation']=$tmppayslip->employee_info->designation_name->designation;
            $payslip['employee_code']=$tmppayslip->employee_info->employee_code;
            $payslip['lname']=$tmppayslip->employee_info->lname;
            $payslip['fname']=$tmppayslip->employee_info->fname;
            $payslip['pre_address']=$tmppayslip->employee_info->pre_address;
            $payslip['pre_city']=$tmppayslip->employee_info->pre_city;
            $payment_component_tunjangan=array();
            $payment_component_iuran=array();
            $payment=Payment::find($payslip['payment']);
            if($payment){
                $payment_component_iuran=PayrollComponent::where('id_payroll_type','=',$payment->payroll_type)->whereIn('id_component', function($query)
                {
                $query->select('id')
                      ->from('sys_components')
                      ->where('type','iuran');
                })->get();
              
                $payment_component_tunjangan=PayrollComponent::where('id_payroll_type','=',$payment->payroll_type)->whereIn('id_component', function($query)
                {
                $query->select('id')
                      ->from('sys_components')
                      ->where('type','tunjangan');
                })->get();
               
            }
            
            $pdf=WkPdf::loadView('admin.payment.print-payslip',compact('payslip','date','month','year','payment_component_iuran','payment_component_tunjangan'));

            return $pdf->stream('Payslip.pdf');
        }else{
            return redirect('payment/view/'.$payslip['payment'])->with([
                'message' => language_data('Payment Details Not found'),
                'message_important' => true
            ]);
        }

    }
    
    
       /* printAllPayslip  Function Start Here */
       public function printAllPayslip($id)
       {
           $date = date('d');
           $month = date('m');
           $year = date('Y');
           $payslips = PaymentComponent::where('payment','=',$id)->get();
           $ctr=0;
           $payslip=array();
           $payment_component_tunjangan=array();
           $payment_component_iuran=array();
           if($payslips){
               foreach($payslips as $p){
   
                   $payslip[$ctr] = PaymentComponent::find($p->id)->toarray();
                   $tmppayslip= PaymentComponent::find($p->id);
                   if($payslip[$ctr]){
                    
                    $payslip[$ctr]['company_id']=$tmppayslip->employee_info->project_name->internal_company;
                    $payslip[$ctr]['company_address']=$tmppayslip->employee_info->project_name->internal_company_name->address;
                    $payslip[$ctr]['company_phone']=$tmppayslip->employee_info->project_name->internal_company_name->phone_number;
                    $payslip[$ctr]['designation']=$tmppayslip->employee_info->designation_name->designation;
                    $payslip[$ctr]['employee_code']=$tmppayslip->employee_info->employee_code;
                    $payslip[$ctr]['lname']=$tmppayslip->employee_info->lname;
                    $payslip[$ctr]['fname']=$tmppayslip->employee_info->fname;
                    $payslip[$ctr]['pre_address']=$tmppayslip->employee_info->pre_address;
                    $payslip[$ctr]['pre_city']=$tmppayslip->employee_info->pre_city;
                       $payment_component_tunjangan[$ctr]=array();
                       $payment_component_iuran[$ctr]=array();
                       $payment=Payment::find($payslip[$ctr]['payment']);
                       if($payment){
                           $payment_component_iuran[$ctr]=PayrollComponent::where('id_payroll_type','=',$payment->payroll_type)->whereIn('id_component', function($query)
                           {
                           $query->select('id')
                                 ->from('sys_components')
                                 ->where('type','iuran');
                           })->get();
                         
                           $payment_component_tunjangan[$ctr]=PayrollComponent::where('id_payroll_type','=',$payment->payroll_type)->whereIn('id_component', function($query)
                           {
                           $query->select('id')
                                 ->from('sys_components')
                                 ->where('type','tunjangan');
                           })->get();
                          
                       }
               }
               $ctr++;

           }
           $pdf=WkPdf::loadView('admin.payment.print-all-payslip',compact('payslip','date','month','year','payment_component_iuran','payment_component_tunjangan'));

           return $pdf->stream('Payslips.pdf');
               
           }else{
               return redirect('payment/view/'.$payslips->payment)->with([
                   'message' => language_data('Payment Details Not found'),
                   'message_important' => true
               ]);
           }
   
       }


}
