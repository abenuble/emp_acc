<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\Designation;
use App\Http\Requests;
use App\CareerPathEmployee;
use App\CareerPathEmployeeAssessment;
use App\CareerPathEmployeeCompetence;
use App\LastEducation;
use App\GeneralApplicants;
use App\Employee;
use App\EmploymentHistory;
use App\EmployeeDependents;
use App\EmployeeBankAccount;
use App\ContractRecipient;
use App\EmployeeFiles;
use App\EmployeeRoles;
use App\EmployeeRolesPermission;
use App\JobApplicants;
use App\Jobs;
use App\Department;
use App\Company;
use App\TaxRules;
use App\Warning;
use App\Leave;
use App\Project;
use App\ProjectNeeds;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\EmployeePaymentHistory;
use Excel;
use DB;
use File;
use WkPdf;
use MPDF;
date_default_timezone_set(app_config('Timezone'));
class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* jobs  Function Start Here */
    public function jobs()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 37)->first();
        $jobs = Jobs::orderBy('id','asc')->get();
        $expjob=Jobs::where('close_date','<', DB::raw('curdate()'))->where('status','=','opening')->update(array('status'=>"closed"));
       
        $ctr=0;
        foreach($jobs as $j){
            $jobs[$ctr]->belum=JobApplicants::where('job_id','=',$j->id)->where('status','Unread')->count();
            $jobs[$ctr]->psiko=JobApplicants::where('job_id','=',$j->id)->where('status','Psychotest')->count();
            $jobs[$ctr]->wawan=JobApplicants::where('job_id','=',$j->id)->where('status','Interview')->count();
            $jobs[$ctr]->diter=JobApplicants::where('job_id','=',$j->id)->where('status','Confirm')->count();
            $jobs[$ctr]->waiting=JobApplicants::where('job_id','=',$j->id)->where('status','Waiting List')->count();
            $jobs[$ctr]->tolak=JobApplicants::where('job_id','=',$j->id)->where('status','Rejected')->count();
            $ctr++;

        }
        $projects = Project::all();
        $designation = Designation::all();
        $last_education = LastEducation::all();
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        // $last_letter_number = DB::table('sys_jobs')->orderBy('id','asc')->value('no_position');
        // $last_number = explode('/', $last_letter_number);
        // if($date=='01'){
        //     $number = 01;
        // } else{
        //     $number = 1 + $last_number[0];
        // }
        $last_letter_number = DB::table('sys_jobs')->where('no_position','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('no_position');
        if($last_letter_number==''){
            $number = 1;
        } else{
            $last_number = explode('/', $last_letter_number);
            $number = 1 + $last_number[0];
        }

        return view('admin.jobapplication.jobs', compact('year','month','number','jobs', 'designation', 'projects','last_education','permcheck'));
    }

    /* postNewJob  Function Start Here */
    public function postNewJob(Request $request)
    {
        // $v = \Validator::make($request->all(), [
        //     'position' => 'required', 'no_position' => 'required', 'post_date' => 'required', 'status' => 'required'
        // ]);

        // if ($v->fails()) {
        //     return redirect('jobs')->withErrors($v->fails());
        // }

        $project = Input::get('project');

        $projects = Project::find($project);

        $company = $projects->company;
        $position = Input::get('position');
        $job_number = Input::get('job_number');
        $quota = Input::get('quota');
        $post_date=date('Y-m-d');
        $apply_date = Input::get('apply_date');
        $apply_date=get_date_format_inggris($apply_date);
        $close_date = Input::get('close_date');
        $close_date=get_date_format_inggris($close_date);
        $status = Input::get('status');
        $job_type=Input::get('job_type');
        $experience=Input::get('experience');
        $last_education=Input::get('last_education');
        $job_location=Input::get('job_location');
        $salary_from        = (int)str_replace(',', '', Input::get('salary_from'));
        $salary_to          = (int)str_replace(',', '', Input::get('salary_to'));
        $short_description=Input::get('short_description');

        if ($position != '') {
            $exist = Jobs::where('position', '=', $position)->where('project', '=', $project)->first();

            if ($exist) {
                return redirect('jobs')->with([
                    'message' => language_data('This Job Post Already Exist'),
                    'message_important' => true
                ]);
            }
        }

        //Penomoran id
        $last_number_id = Jobs::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $jobs = new Jobs();
        $jobs->id = $number_id;
        $jobs->project = $project;
        $jobs->company = $company;
        $jobs->position = $position;
        $jobs->no_position = $job_number;
        $jobs->job_type = $job_type;
        $jobs->experience = $experience;
        $jobs->last_education = $last_education;
        $jobs->job_location = $job_location;
        $jobs->salary_from = $salary_from;
        $jobs->salary_to = $salary_to;
        $jobs->post_date = $post_date;
        $jobs->apply_date = $apply_date;
        $jobs->close_date = $close_date;
        $jobs->status = $status;
        $jobs->short_description = $short_description;
        $jobs->quota = $quota;
        $jobs->save();

        $id_job = $jobs->id;

        //Penomoran id
        $last_number_id_need = ProjectNeeds::max('id_needs');
        if($last_number_id_need==''){
            $number_id_need = 1;
        } else {
            $number_id_need = 1 + $last_number_id_need;
        }

        $projectneeds = new ProjectNeeds();
        $projectneeds->id_needs = $number_id_need;
        $projectneeds->id = $project;
        $projectneeds->id_designation = $position;
        $projectneeds->id_payment = 0;
        $projectneeds->id_job = $id_job;
        $projectneeds->location = $job_location;
        $projectneeds->open_date = $apply_date;
        $projectneeds->close_date = $close_date;
        $projectneeds->total = $quota;
        $projectneeds->save();


        return redirect('jobs')->with([
            'message' => language_data('Job Added Successfully')
        ]);

    }


    /* editJob  Function Start Here */
    public function editJob($id)
    {
        $job = Jobs::find($id);
        if ($job) {
            $applicants = JobApplicants::where('job_id', '=', $id)->get();
            $designation = Designation::all();
            $last_education = LastEducation::all();
            // echo "<pre>";
            // print_r($last_education); exit;
            return view('admin.jobapplication.edit-job', compact('job','applicants','designation','last_education'));
        } else {
            return redirect('jobs')->with([
                'message' => language_data('Job not found'),
                'message_important' => true
            ]);
        }
    }


    /* postEditJob  Function Start Here */
    public function postEditJob(Request $request)
    {

        $cmd = Input::get('cmd');
        $apply_date         = Input::get('apply_date');
        $apply_date         = get_date_format_inggris($apply_date);
        $close_date         = Input::get('close_date');
        $close_date         = get_date_format_inggris($close_date);
        $quota              = Input::get('quota');
        $status             = Input::get('status');
        $last_education     = Input::get('last_education');
        $job_type           = Input::get('job_type');
        $experience         = Input::get('experience');
        $salary_from        = (int)str_replace(',', '', Input::get('salary_from'));
        $salary_to          = (int)str_replace(',', '', Input::get('salary_to'));
        $short_description  = Input::get('short_description');

        $jobs               = Jobs::find($cmd);
        $exist_pos          = $jobs->position;
        
        $jobs->job_type = $job_type;
        $jobs->experience = $experience;
        $jobs->salary_from = $salary_from;
        $jobs->salary_to = $salary_to;
        $jobs->short_description = $short_description;
        $jobs->last_education = $last_education;
        $jobs->apply_date = $apply_date;
        $jobs->close_date = $close_date;
        $jobs->quota = $quota;
        $jobs->status = $status;
        $jobs->save();

        return redirect('jobs')->with([
            'message' => language_data('Job Update Successfully')
        ]);

    }

    /* deleteJob Function Start Here */
    public function deleteJob($id)
    {

        $job = Jobs::find($id);
        if ($job) {
            JobApplicants::where('job_id','=',$id)->delete();
            $job->delete();
            return redirect('jobs')->with([
                'message' => language_data('Job Deleted Successfully')
            ]);
        } else {
            return redirect('jobs')->with([
                'message' => language_data('Job not found'),
                'message_important' => true
            ]);
        }
    }

    /* viewJobApplicant  Function Start Here */
    public function viewJobApplicant($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 37)->first();
        $applicants = JobApplicants::find($id);
        $jobview = Jobs::find($applicants->job_id);
        if ($applicants) {
            
            $job_applicants_confirm = JobApplicants::where([
                                            ['job_id', '=', $jobview->id],
                                            ['status', '=', 'Confirm'],
                                        ])->count();
            return view('admin.jobapplication.view-job-applicant', compact('applicants','jobview','job_applicants_confirm','permcheck'));
        }
        
        
    }

    /* viewGeneralApplicant  Function Start Here */
    public function viewGeneralApplicant($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 38)->first();

        $generalapplicantview = GeneralApplicants::find($id);
        $designation = Designation::all();
        $last_education = LastEducation::all();

        if ($generalapplicantview) {
            return view('admin.jobapplication.view-general-applicant', compact('generalapplicantview','permcheck','designation','last_education'));
        }
        
        
    }
    /*function updateGeneralApplicant start here */
    public function updateGeneralApplicant(Request $request)
    {
                
        $cmd=Input::get('cmd');

        $name            = Input::get('name');
        $email           = Input::get('email');
        $phone           = Input::get('phone');
        $address         = Input::get('address');
        $district        = Input::get('district');
        $city            = Input::get('city');
        $ktp             = Input::get('ktp');
        $birth_place     = Input::get('birth_place');
        $birth_date      = Input::get('birth_date');
        $birth_date      = get_date_format_inggris($birth_date);
        $gender          = Input::get('gender');
        $marital_status  = Input::get('marital_status');
        $religion        = Input::get('religion');
        $ibu_kandung     = Input::get('ibu_kandung');
        $designation     = Input::get('designation');
        $experience      = Input::get('experience');
        $last_education  = Input::get('last_education');

        $resume          = Input::file('resume');
        $photo           = Input::file('photo');
        $destinationPath_file = public_path() . '/assets/applicant_doc/';
        $file_name       = '';
        $photo_name       = '';
        if($resume){
            $target_file    = $destinationPath_file . basename($_FILES["resume"]["name"]);
            $imageFileType  = pathinfo($target_file,PATHINFO_EXTENSION);   
            if ($imageFileType == 'jpg' OR $imageFileType == 'png' OR $imageFileType == 'jpeg' OR $imageFileType == 'pdf') {
                $destinationPath = public_path() . '/assets/applicant_doc/';
                $file_name       = $resume->getClientOriginalName();
                Input::file('file')->move($destinationPath, $file_name);

            } else {
                return redirect('jobs/view-applicant/' . $cmd)->with([
                    'message' => 'Upload an Image or PDF',
                    'message_important' => true
                ]);
            }
        }
        if($photo){
            $target_photo   = $destinationPath_file . basename($_FILES["photo"]["name"]);
            $imageFileType_photo  = pathinfo($target_photo,PATHINFO_EXTENSION);       
            if ($imageFileType_photo == 'jpg' OR $imageFileType_photo == 'png' OR $imageFileType_photo == 'jpeg' OR $imageFileType_photo == 'pdf') {
                $destinationPath = public_path() . '/assets/applicant_doc/';
                $photo_name       = $photo->getClientOriginalName();
                Input::file('photo')->move($destinationPath, $photo_name);

            } else {
                return redirect('jobs/view-applicant/' . $cmd)->with([
                    'message' => 'Upload an Image or PDF',
                    'message_important' => true
                ]);
            }
        }

        $general_applicant  = GeneralApplicants::find($cmd);

        $general_applicant->name            = $name;
        $general_applicant->email           = $email;
        $general_applicant->phone           = $phone;
        $general_applicant->address         = $address;
        $general_applicant->district        = $district;
        $general_applicant->city            = $city;
        $general_applicant->ktp             = $ktp;
        $general_applicant->birth_place     = $birth_place;
        $general_applicant->birth_date      = $birth_date;
        $general_applicant->gender          = $gender;
        $general_applicant->marital_status  = $marital_status;
        $general_applicant->religion        = $religion;
        $general_applicant->ibu_kandung     = $ibu_kandung;
        $general_applicant->experience      = $experience;
        $general_applicant->designation     = $designation;
        $general_applicant->education       = $last_education;
        $general_applicant->save();
        return redirect('jobs/general-applicants')->with([
            'message' => 'Applicant Updated Successfully'
        ]);
            
    }

    /* viewApplicant  Function Start Here */
    public function viewApplicant($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 37)->first();
        $jobview = Jobs::find($id);

        if ($jobview) {
            $designation = Designation::all();
            $last_education = LastEducation::all();
            $general_applicants = GeneralApplicants::where('designation','=',$jobview->position)->orWhere('designation','=',0)->get();
            $job_applicants         = JobApplicants::where('job_id', '=', $jobview->id)->count();
            $job_applicants_confirm =   JobApplicants::where([
                                            ['job_id', '=', $jobview->id],
                                            ['status', '=', 'Confirm'],
                                        ])->count();
            if($job_applicants_confirm==$jobview->quota){
                $job_applicants_not_confirm=JobApplicants::where([
                ['job_id', '=', $jobview->id],
                ['status', '!=', 'Confirm'],
                ])->get();
                foreach($job_applicants_not_confirm as $j){
                    $updateapplicant= JobApplicants::find($j->id);
                    $updateapplicant->status='Rejected';
                    $updateapplicant->save();
                    $last_number = GeneralApplicants::max('id');
                    if($last_number==''){
                        $number = 1;
                    } else {
                        $number = 1 + $last_number;
                    }
                    $dataz = array(
                        'id'            =>$number,
                        'name'			=> $j->name,
                        'email'			=> $j->email,
                        'phone'  		=> $j->phone,
                        'address'	    => $j->address,
                        'education'	    => $j->education,
                        'experience'	=> $j->experience,
                        'resume'	    => $j->resume,
                        'photo'     	=> $j->photo
                    );
                    $insert_general         = GeneralApplicants::insert($dataz);
                }
                $jobview->status = 'closed';
                $jobview->save();
            } else if($jobview->close_date<=date('Y-m-d')){
                $jobview->status = 'closed';
                $jobview->save();
            }
            $applicants = JobApplicants::where('job_id', '=', $id)->orderBy('id','asc')->get();

            $job_applicants_waiting_list =  JobApplicants::where([
                                                ['job_id', '=', $jobview->id],
                                                ['status', '=', 'Waiting List'],
                                            ])->count();
            $designation = Designation::all();
            return view('admin.jobapplication.view-application', compact('job_applicants_waiting_list','applicants','designation','jobview','general_applicants','job_applicants','job_applicants_confirm','id','last_education','permcheck','designation'));
        }
        
        
    }

    /* viewPsychotestApplicant  Function Start Here */
    public function viewPsychotestApplicant($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 37)->first();
        $applicants = JobApplicants::find($id);

        if ($applicants) {
            return view('admin.jobapplication.view-psychotest-applicant', compact('applicants','permcheck'));
        }
        
        
    }

    /* viewInterviewApplicant  Function Start Here */
    public function viewInterviewApplicant($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 37)->first();
        $applicants = JobApplicants::find($id);

        if ($applicants) {
            return view('admin.jobapplication.view-interview-applicant', compact('applicants','permcheck'));
        }
        
        
    }

    /* viewHealthtestApplicant  Function Start Here */
    public function viewHealthtestApplicant($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 37)->first();
        $applicants = JobApplicants::find($id);

        if ($applicants) {
            return view('admin.jobapplication.view-medical-applicant', compact('applicants','permcheck'));
        }
        
        
    }

    /* viewWaitingApplicant  Function Start Here */
    public function viewWaitingApplicant($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 37)->first();

        $applicants = JobApplicants::find($id);

        if ($applicants) {
            return view('admin.jobapplication.view-waiting-applicant', compact('applicants','permcheck'));
        }
        
        
    }

    

    /* generalApplicantRetrieve  Function Start Here */
    public function generalApplicantRetrieve()
    {
      
        $gen_id = Input::get('gen_app_id');
        
        $gen_data = GeneralApplicants::where('id', '=', $gen_id)->get();
        
        return \Response::json($gen_data);
        
    }

    /* downloadResume  Function Start Here */
    public function downloadResume($type,$id)
    {
        // echo $type;
        // echo "<br>";
        // echo $id; exit;
        if($type==1){
            return $this->downloadResume1($id);
        } else if ($type==2) {
            return $this->downloadResume2($id);
        }

    }

    /* downloadResume  Function Start Here */
    public function downloadResume1($id)
    {

        $employee       = Employee::find($id);
        if($employee){
            $designation    = Designation::all();
            $department     = Department::all();
            $company        = Company::all();
            $tax            = TaxRules::where('status','active')->get();
            $role           = EmployeeRoles::where('status','Active')->get();
            $sick           =Leave::where('emp_id','=',$id)->where('sick_name','!=',null)->where('status','approved')->get();
            $dependents     = EmployeeDependents::where('emp_id', '=', $id)->get();
            $bank_accounts  = EmployeeBankAccount::where('emp_id', '=', $id)->first();
            $employee_doc   = EmployeeFiles::where('emp_id', '=', $id)->get();
            $warning        = Warning::where('employee_name', '=', $id)->where('status', '=', 'accepted')->get();
            $employment     = EmploymentHistory::where('emp_id','=',$id)->get();
            $contract       = ContractRecipient::where('recipients','=',$id)->get();
            $salary         = EmployeePaymentHistory::where('emp_id','=',$id)->get();
            $cv             = JobApplicants::where('email','=',$employee->email)->first();
    
            if(sizeof($bank_accounts)<1){
                $bank_accounts = array();
                $bank_accounts=(object)$bank_accounts;
                $bank_accounts->bank_name="";
                $bank_accounts->account_number="";
                $bank_accounts->account_name="";
                $bank_accounts->tax_status="";
                $bank_accounts->npwp_status="";
                $bank_accounts->npwp_number="";
                $bank_accounts->employment_bpjs="";
                $bank_accounts->health_bpjs="";
                $bank_accounts->other_insurance_name="";
                $bank_accounts->other_insurance_number="";
                $bank_accounts->effective_date="";
                $bank_accounts->other_insurance_name2="";
                $bank_accounts->other_insurance_number2="";
                $bank_accounts->effective_date2="";
                $bank_accounts->other_insurance_name3="";
                $bank_accounts->other_insurance_number3="";
                $bank_accounts->effective_date3="";
                $bank_accounts->user_cost_center_function="";
                $bank_accounts->user_function_name="";
                $bank_accounts->user_kbo_function="";
                $bank_accounts->kbo_name="";
            }
            $pdf=WkPdf::loadView('admin.employee.resume.employee-resume', compact('contract','employment','warning','employee','designation','department','company','dependents','bank_accounts','employee_doc','tax','role','salary','sick','cv'));
            
            // $pdf = MPDF::loadView('admin.employee.resume.employee-resume', compact('contract','employment','warning','employee','designation','department','company','dependents','bank_accounts','employee_doc','tax','role','salary','sick','cv'));
            return $pdf->stream($employee->employee_code."(CV)".'.pdf'); 
        }
    }
    /* downloadResume  Function Start Here */
    public function downloadResume2($id)
    {
        $file = JobApplicants::find($id)->resume;
        if($file!=''){
            return response()->download(public_path('/assets/applicant_doc/' . $file));
        }
    }
    /* downloadPhoto  Function Start Here */
    public function downloadPhoto($id)
    {
        $file = JobApplicants::find($id)->photo;
        return response()->download(public_path('/assets/applicant_doc/' . $file));
    }

    /* downloadPsychotest  Function Start Here */
    public function downloadPsychotest($id)
    {

        $file = JobApplicants::find($id)->psychotest_file;
        return response()->download(public_path('/assets/psychotest_files/' . $file));
    }

    /* downloadInterview  Function Start Here */
    public function downloadInterview($id)
    {

        $file = JobApplicants::find($id)->interview_file;
        return response()->download(public_path('/assets/interview_files/' . $file));
    }

    /* downloadHealthtest  Function Start Here */
    public function downloadHealthtest($id)
    {
        $file = JobApplicants::find($id)->healthtest_file;
        return response()->download(public_path('/assets/healthtest_files/' . $file));
    }

    /* deleteApplication  Function Start Here */
    public function deleteApplication($id)
    {
        $appStage=app_config('AppStage');
        $delete_app = JobApplicants::find($id);
        $delete_project_needs = ProjectNeeds::where('id_job',$id)->first();
        if ($delete_app) {
            $job_id = $delete_app->job_id;
            $delete_app->delete();
            
            $delete_project_needs->delete();

            return redirect('jobs/view-applicant/' . $job_id)->with([
                'message' => language_data('Applicant Deleted Successfully')
            ]);
        } else {
            return redirect('jobs')->with([
                'message' => language_data('Applicant not found'),
                'message_important' => true
            ]);
        }
    }

    /* setApplicantStatus  Function Start Here */
    public function setApplicantStatus(Request $request)
    {

        $job_id=Input::get('job_id');
        $cmd=Input::get('cmd');

        $job_applicant  = JobApplicants::find($cmd);
        $job            = Jobs::find($job_applicant->job_id);
        $project_need   = ProjectNeeds::where('id','=',$job->project)->where('id_job','=',$job->id)->first();
        $designation    = Designation::find($job->position);
        $last_id        = DB::table('sys_employee')->orderBy('id','asc')->value('id');
        if($job_applicant){
            $job_applicant->name=$request->name;
            $job_applicant->email=$request->email;
            $job_applicant->phone=$request->phone;
            $job_applicant->address=$request->address;
            $job_applicant->status=$request->status;
            $job_applicant->save();
            
            // $psychotest_total = JobApplicants::where('job_id','=',$job_id)->where('status','=','Psychotest')->count();
            $unread_total     = JobApplicants::where('job_id','=',$job_id)->where('status','=','Unread')->count();
            $interview_total  = JobApplicants::where('job_id','=',$job_id)->where('status','=','Interview')->count();
            $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();
    
            // $job->psychotest = $psychotest_total;
            $job->interview  = $interview_total;
            $job->unread     = $unread_total;
            $job->accepted   = $accepted_total;
            $job->save();

            if($request->status == "Rejected"){
                $gen_app                  = new GeneralApplicants();
                $gen_app->name            = $job_applicant->name;
                $gen_app->email           = $job_applicant->email;
                $gen_app->phone           = $job_applicant->phone;
                $gen_app->address_domisili         = $job_applicant->address_domisili;
                $gen_app->village_domisili         = $job_applicant->village_domisili;
                $gen_app->district_domisili        = $job_applicant->district_domisili;
                $gen_app->city_domisili            = $job_applicant->city_domisili;
                $gen_app->address         = $job_applicant->address;
                $gen_app->village         = $job_applicant->village;
                $gen_app->district        = $job_applicant->district;
                $gen_app->city            = $job_applicant->city;
                $gen_app->experience      = $job_applicant->experience;
                $gen_app->education       = $job_applicant->education;
                $gen_app->resume          = $job_applicant->resume;
                $gen_app->photo           = $job_applicant->photo;
                $gen_app->save();
    
                return redirect('jobs/view-applicant/' . $job_id);
            } 
            elseif ($request->status == "Confirm"){
                $name = trim($job_applicant->name);
                $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
                $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
                $month=date('m');
                $thn=date('y');
                $time = $month.$thn;
                $company_area = $job->company_name->company_code;
                $emmployee_code=Employee::select(DB::raw("SUBSTRING(employee_code,1,3) as 'AREA'"),DB::raw("SUBSTRING(employee_code,4,7) as 'TIME'"),DB::raw("SUBSTRING(employee_code,8,11) as 'kode'"))
                // ->having('AREA','=',$company_area)
                // ->having('TIME','=',$time)
                ->orderBy('kode', 'asc')
                ->first();
            
                if( $emmployee_code){
                    $kode= $emmployee_code->kode+1;
                    
                }else{
                    $kode="1";
                }
                
   
                $kodeemployee = str_pad($company_area,3,'0',STR_PAD_LEFT).$time.''.str_pad($kode,4,'0',STR_PAD_LEFT);
                $employee_code= $kodeemployee;
                //Penomoran id
                $last_number_id = Employee::max('id');
                if($last_number_id==''){
                    $number_id = 1;
                } else {
                    $number_id = 1 + $last_number_id;
                }
                $exist = Employee::where('no_ktp','=',$job_applicant->ktp)->first();
                if($exist){
                    $exist->user_name       = $employee_code;
                    $exist->employee_code   = $employee_code;
                    $exist->no_ktp          = $job_applicant->ktp;
                    $exist->project         = $job->project;
                    $exist->company         = $job->company;
                    $exist->employee_type   = $job->company_name->category;
                    $exist->location        = $job->job_location;
                    $exist->designation     = $job->position;
                    $exist->department      = $designation->did;
                    $exist->email           = $job_applicant->email;
                    $exist->phone           = $job_applicant->phone;
                    $exist->pre_address     = $job_applicant->address_domisili;
                    $exist->pre_village     = $job_applicant->village_domisili;
                    $exist->pre_district    = $job_applicant->district_domisili;
                    $exist->pre_city        = $job_applicant->city_domisili;
                    $exist->per_address     = $job_applicant->address;
                    $exist->per_village     = $job_applicant->village;
                    $exist->per_district    = $job_applicant->district;
                    $exist->per_city        = $job_applicant->city;
                    $exist->doj             = date('Y-m-d');
                    $exist->dol             = get_date_format_inggris($job->project_name->end_date);
                    $exist->employee_status = 'HARIAN';
                    $exist->status          = 'active';
                    $exist->save();
    
                    $emp_id = $exist->id;

                } else {
                    $employee                  = new Employee();
                    $employee->id              = $number_id;
                    $employee->fname           = $job_applicant->name;
                    $employee->user_name       = $employee_code;
                    $employee->password        = bcrypt($job_applicant->birth_date);
                    $employee->employee_code   = $employee_code;
                    $employee->no_ktp          = $job_applicant->ktp;
                    $employee->project         = $job->project;
                    $employee->company         = $job->company;
                    $employee->employee_type   = $job->company_name->category;
                    $employee->location        = $job->job_location;
                    $employee->designation     = $job->position;
                    $employee->department      = $designation->did;
                    $employee->email           = $job_applicant->email;
                    $employee->phone           = $job_applicant->phone;
                    $employee->pre_address     = $job_applicant->address_domisili;
                    $employee->pre_village     = $job_applicant->village_domisili;
                    $employee->pre_district    = $job_applicant->district_domisili;
                    $employee->pre_city        = $job_applicant->city_domisili;
                    $employee->per_address     = $job_applicant->address;
                    $employee->per_village     = $job_applicant->village;
                    $employee->per_district    = $job_applicant->district;
                    $employee->per_city        = $job_applicant->city;
                    $employee->last_education  = $job_applicant->education;
                    $employee->payment_type    = $project_need->id_payment;
                    $employee->avatar          = $job_applicant->photo;
                    $employee->doj             = date('Y-m-d');
                    $employee->dol             = get_date_format_inggris($job->project_name->end_date);
                    $employee->employee_status = 'HARIAN';
                    $employee->status          = 'active';
                    $employee->save();
    
                    $emp_id = $employee->id;
                }
                if($job_applicant->photo!=null&&$job_applicant->photo!=''){
                    $old_path = public_path() . '/assets/applicant_doc/' . $job_applicant->photo;
                    $destinationPath = public_path() . '/assets/employee_pic/' . $job_applicant->photo;
                    $move = File::move($old_path,$destinationPath);
                }

                $employment = new EmploymentHistory();
                $employment->emp_id = $emp_id;
                $employment->project = $job->project;
                $employment->designation = $job->position;
                $employment->department = $designation->did;
                $employment->location = $job->job_location;
                $employment->save();

                $employee_bank = new EmployeeBankAccount();
                $employee_bank->emp_id = $emp_id;
                $employee_bank->save();

                //Table CareerPathEmpolyeeAssessment Save
                $num_element_competences = 0;

                $career_path = CareerPathEmployee::where('designation','=',$designation->career)->get();
                $career_path_competence=array();
                foreach($career_path as $cp){
                    $career_path_competence = CareerPathEmployeeCompetence::where('career_path','=',$cp->id)->get()->toArray();
                }

                $sql_data_competence = array();
                if($career_path_competence){
                    while($num_element_competences<count($career_path_competence)){
                        $sql_data_competence[] = array(
                            'id'            => "",
                            'emp_id'        => $emp_id,
                            'competence'    => $career_path_competence[$num_element_competences]['id']
                        );
                        $num_element_competences++;
                    }
                }
                $career_assessment = CareerPathEmployeeAssessment::insert($sql_data_competence);
                
                $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();                
                
                $job->accepted   = $accepted_total;
                $job->save();
    
                return redirect('jobs/view-applicant/' . $job_id);
                
                if($job_applicant->resume!=null&&$job_applicant->resume!=''){
                    $destinationPath_new = public_path() . '/assets/employee_doc/' . $job_applicant->resume;
                    $destinationPath_old = public_path() . '/assets/applicant_doc/' . $job_applicant->resume;
                    $move = File::move($destinationPath_old, $destinationPath_new);
                
                    $employee_doc=new EmployeeFiles();
                    $employee_doc->emp_id=$emp_id;
                    $employee_doc->file_title=$job_applicant->resume;
                    $employee_doc->file=$job_applicant->resume;
                    $employee_doc->save();
                }
            }
            else{
                $resume            = Input::file('resume');
                $photo           = Input::file('photo');  

                $destinationPath_file = public_path() . '/assets/applicant_doc/';
                if($resume!=null && $photo!=null){
                $target_file    = $destinationPath_file . basename($_FILES["resume"]["name"]);
                $imageFileType  = pathinfo($target_file,PATHINFO_EXTENSION);   

                $target_photo   = $destinationPath_file . basename($_FILES["photo"]["name"]);
                $imageFileType_photo  = pathinfo($target_photo,PATHINFO_EXTENSION); 
                
                if (($imageFileType == 'jpg' OR $imageFileType == 'png' OR $imageFileType == 'jpeg' OR $imageFileType == 'pdf') && ($imageFileType_photo == 'jpg' OR $imageFileType_photo == 'png' OR $imageFileType_photo == 'jpeg' OR $imageFileType_photo == 'pdf')) {
                    $destinationPath = public_path() . '/assets/applicant_doc/';
                    $file_name       = $resume->getClientOriginalName();
                    Input::file('resume')->move($destinationPath, $file_name);
                    $photo_name      = $photo->getClientOriginalName();
                    Input::file('photo')->move($destinationPath, $photo_name);
                    $job_applicant->resume          = $file_name;
                    $job_applicant->photo           = $photo_name;
                    $job_applicant->save();
                }}
    
                return redirect('jobs/view-applicant/' . $job_id);
            }


            return redirect('jobs/view-applicant/'.$job_id)->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('jobs/view-applicant/'.$job_id)->with([
                'message'=> language_data('Applicant not found'),
                'message_important'=>true
            ]);
        }

    }

    /* setApplicantPsychotestStatus  Function Start Here */
    public function setApplicantPsychotestStatus(Request $request)
    {

        $job_id=Input::get('job_id');
        $cmd=Input::get('cmd');

        $v=\Validator::make($request->all(),[
        ]);

        if($v->fails()){
            return redirect('jobs/view-applicant/'.$job_id)->withErrors($v->fails());
        }

        $job_applicant  = JobApplicants::find($cmd);
        $job            = Jobs::find($job_applicant->job_id);
        $designation    = Designation::find($job->position);
        if($job_applicant){
            $job_applicant->sub_status_psychotest   = $request->status_psychotest;
            $job_applicant->psychotest              = $request->psychotest_grade;
            $job_applicant->psychotest_schedule     = get_date_format_inggris($request->psychotest_schedule);
            $job_applicant->psychotest_information  = $request->information_psychotest;

            if($job_applicant->psychotest_file==''){
                $file                                   = Input::file('psychotest_file');
                if($file){
                    $destinationPath                        = public_path() . '/assets/psychotest_files/';
                    $file_name                              = $file->getClientOriginalName();
                    Input::file('psychotest_file')->move($destinationPath, $file_name);
                    $job_applicant->psychotest_file         = $file_name;
                    $job_applicant->save();
                }
            }

            if($job_applicant->photo==''){
                $file                                   = Input::file('photo');
                if($file){
                    $destinationPath                        = public_path() . '/assets/applicant_doc/';
                    $file_name                              = $file->getClientOriginalName();
                    Input::file('photo')->move($destinationPath, $file_name);
                    $job_applicant->photo                   = $file_name;
                    $job_applicant->save();
                }
            }

            if($job_applicant->resume==''){
                $file                                   = Input::file('resume');
                if($file){
                    $destinationPath                        = public_path() . '/assets/applicant_doc/';
                    $file_name                              = $file->getClientOriginalName();
                    Input::file('resume')->move($destinationPath, $file_name);
                    $job_applicant->resume                   = $file_name;
                    $job_applicant->save();
                }
            }
            if($request->status != "Rejected"){
                if($request->status_psychotest == "scheduling")
                { 
                    $job_applicant->sub_status_psychotest = $request->status_psychotest;
                    $job_applicant->status              = $request->status;
                    $job_applicant->save();
    
                    $psychotest_total = JobApplicants::where('job_id','=',$job_id)->where('status','=','Psychotest')->count();
                    $unread_total     = JobApplicants::where('job_id','=',$job_id)->where('status','=','Unread')->count();
                    $interview_total  = JobApplicants::where('job_id','=',$job_id)->where('status','=','Interview')->count();
                    $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();
            
                    $job->psychotest = $psychotest_total;
                    $job->interview  = $interview_total;
                    $job->unread     = $unread_total;
                    $job->accepted   = $accepted_total;
                    $job->save();
                }
                else if($request->status_psychotest == "scheduled")
                { 
                    $job_applicant->sub_status_psychotest = $request->status_psychotest;
                    $job_applicant->status              = $request->status;
                    $job_applicant->save();
    
                    $psychotest_total = JobApplicants::where('job_id','=',$job_id)->where('status','=','Psychotest')->count();
                    $unread_total     = JobApplicants::where('job_id','=',$job_id)->where('status','=','Unread')->count();
                    $interview_total  = JobApplicants::where('job_id','=',$job_id)->where('status','=','Interview')->count();
                    $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();
            
                    $job->psychotest = $psychotest_total;
                    $job->interview  = $interview_total;
                    $job->unread     = $unread_total;
                    $job->accepted   = $accepted_total;
                    $job->save();
                }
                else if($request->status_psychotest == "pass")
                { 
                    $job_applicant->sub_status_psychotest = $request->status_psychotest;
                    $job_applicant->status              = 'Waiting List';
                    $job_applicant->save();
    
                    $psychotest_total = JobApplicants::where('job_id','=',$job_id)->where('status','=','Psychotest')->count();
                    $unread_total     = JobApplicants::where('job_id','=',$job_id)->where('status','=','Unread')->count();
                    $interview_total  = JobApplicants::where('job_id','=',$job_id)->where('status','=','Interview')->count();
                    $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();
            
                    $job->psychotest = $psychotest_total;
                    $job->interview  = $interview_total;
                    $job->unread     = $unread_total;
                    $job->accepted   = $accepted_total;
                    $job->save();
                }
                else if($request->status_psychotest == "not pass")
                { 
                    $job_applicant->sub_status_psychotest = $request->status_psychotest;
                    $job_applicant->status    = $request->status = "Rejected";
                    $job_applicant->save();
    
                    $psychotest_total = JobApplicants::where('job_id','=',$job_id)->where('status','=','Psychotest')->count();
                    $unread_total     = JobApplicants::where('job_id','=',$job_id)->where('status','=','Unread')->count();
                    $interview_total  = JobApplicants::where('job_id','=',$job_id)->where('status','=','Interview')->count();
                    $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();
            
                    $job->psychotest = $psychotest_total;
                    $job->interview  = $interview_total;
                    $job->unread     = $unread_total;
                    $job->accepted   = $accepted_total;
                    $job->save();
    
                    $gen_app                  = new GeneralApplicants();
                    $gen_app->name            = $job_applicant->name;
                    $gen_app->email           = $job_applicant->email;
                    $gen_app->phone           = $job_applicant->phone;
                    $gen_app->address_domisili         = $job_applicant->address_domisili;
                    $gen_app->village_domisili         = $job_applicant->village_domisili;
                    $gen_app->district_domisili        = $job_applicant->district_domisili;
                    $gen_app->city_domisili            = $job_applicant->city_domisili;
                    $gen_app->address         = $job_applicant->address;
                    $gen_app->village         = $job_applicant->village;
                    $gen_app->district        = $job_applicant->district;
                    $gen_app->city            = $job_applicant->city;
                    $gen_app->experience      = $job_applicant->experience;
                    $gen_app->education       = $job_applicant->education;
                    $gen_app->resume          = $job_applicant->resume;
                    $gen_app->photo           = $job_applicant->photo;
                    $gen_app->save();
                }
                else if($request->status_psychotest == "confirm")
                {  
                    $job_applicant->status              = "Confirm";
                    $job_applicant->save();
                    $project_need   = ProjectNeeds::where('id','=',$job->project)->where('id_job','=',$job->id)->first();
    
                    $name = trim($job_applicant->name);
                    $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
                    $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
    
                    //Penomoran id
                    $last_number_id = Employee::max('id');
                    if($last_number_id==''){
                        $number_id = 1;
                    } else {
                        $number_id = 1 + $last_number_id;
                    }
                    $month=date('m');
                    $thn=date('y');
                    $time = $month.$thn;
                    $company_area = $job->company_name->company_code;
                    $emmployee_code=Employee::select(DB::raw("SUBSTRING(employee_code,1,3) as 'AREA'"),DB::raw("SUBSTRING(employee_code,4,7) as 'TIME'"),DB::raw("SUBSTRING(employee_code,8,11) as 'kode'"))
                    // ->having('AREA','=',$company_area)
                    // ->having('TIME','=',$time)
                    ->orderBy('kode', 'asc')
                    ->first();
                
                    if( $emmployee_code){
                        $kode= $emmployee_code->kode+1;
                        
                    }else{
                        $kode="1";
                    }
                    
       
                    $kodeemployee = str_pad($company_area,3,'0',STR_PAD_LEFT).$time.''.str_pad($kode,4,'0',STR_PAD_LEFT);
                    $employee_code= $kodeemployee;
                        
                    $exist = Employee::where('no_ktp','=',$job_applicant->ktp)->first();
                    if($exist){
                        $exist->user_name       = $employee_code;
                        $exist->employee_code   = $employee_code;
                        $exist->no_ktp          = $job_applicant->ktp;
                        $exist->project         = $job->project;
                        $exist->company         = $job->company;
                        $exist->employee_type   = $job->company_name->category;
                        $exist->location        = $job->job_location;
                        $exist->designation     = $job->position;
                        $exist->department      = $designation->did;
                        $exist->email           = $job_applicant->email;
                        $exist->phone           = $job_applicant->phone;
                        $exist->pre_address     = $job_applicant->address_domisili;
                        $exist->pre_village     = $job_applicant->village_domisili;
                        $exist->pre_district    = $job_applicant->district_domisili;
                        $exist->pre_city        = $job_applicant->city_domisili;
                        $exist->per_address     = $job_applicant->address;
                        $exist->per_village     = $job_applicant->village;
                        $exist->per_district    = $job_applicant->district;
                        $exist->per_city        = $job_applicant->city;
                        $exist->doj             = date('Y-m-d');
                        $exist->dol             = get_date_format_inggris($job->project_name->end_date);
                        $exist->employee_status = 'HARIAN';
                        $exist->status          = 'active';
                        $exist->save();
        
                        $emp_id = $exist->id;

                    } else {
                        $employee                  = new Employee();
                        $employee->id              = $number_id;
                        $employee->fname           = $job_applicant->name;
                        $employee->user_name       = $employee_code;
                        $employee->password        = bcrypt($job_applicant->birth_date);
                        $employee->employee_code   = $employee_code;
                        $employee->no_ktp          = $job_applicant->ktp;
                        $employee->project         = $job->project;
                        $employee->company         = $job->company;
                        $employee->employee_type   = $job->company_name->category;
                        $employee->location        = $job->job_location;
                        $employee->designation     = $job->position;
                        $employee->department      = $designation->did;
                        $employee->email           = $job_applicant->email;
                        $employee->phone           = $job_applicant->phone;
                        $employee->pre_address     = $job_applicant->address_domisili;
                        $employee->pre_village     = $job_applicant->village_domisili;
                        $employee->pre_district    = $job_applicant->district_domisili;
                        $employee->pre_city        = $job_applicant->city_domisili;
                        $employee->per_address     = $job_applicant->address;
                        $employee->per_village     = $job_applicant->village;
                        $employee->per_district    = $job_applicant->district;
                        $employee->per_city        = $job_applicant->city;
                        $employee->last_education  = $job_applicant->education;
                        $employee->payment_type    = $project_need->id_payment;
                        $employee->avatar          = $job_applicant->photo;
                        $employee->doj             = date('Y-m-d');
                        $employee->dol             = get_date_format_inggris($job->project_name->end_date);
                        $employee->employee_status = 'HARIAN';
                        $employee->status          = 'active';
                        $employee->save();
        
                        $emp_id = $employee->id;
                    }
                    if($job_applicant->photo!=null&&$job_applicant->photo!=''){
                        $old_path = public_path() . '/assets/applicant_doc/' . $job_applicant->photo;
                        $destinationPath = public_path() . '/assets/employee_pic/' . $job_applicant->photo;
                        $move = File::move($old_path,$destinationPath);
                    }
    
                    $employment = new EmploymentHistory();
                    $employment->emp_id = $emp_id;
                    $employment->project = $job->project;
                    $employment->designation = $job->position;
                    $employment->department = $designation->did;
                    $employment->location = $job->job_location;
                    $employment->save();
    
                    $employee_bank = new EmployeeBankAccount();
                    $employee_bank->emp_id = $emp_id;
                    $employee_bank->save();
    
                    
                    //Table CareerPathEmpolyeeAssessment Save
                    $num_element_competences = 0;
    
                    $career_path = CareerPathEmployee::where('designation','=',$designation->career)->get();
                    $career_path_competence=[];
                    foreach($career_path as $cp){
                        $career_path_competence = CareerPathEmployeeCompetence::where('career_path','=',$cp->id)->get()->toArray();
                    }
    
                    $sql_data_competence = array();
    
                    while($num_element_competences<count($career_path_competence)){
                        $sql_data_competence[] = array(
                            'id'            => "",
                            'emp_id'        => $emp_id,
                            'competence'    => $career_path_competence[$num_element_competences]['id']
                        );
                        $num_element_competences++;
                    }
    
                    $career_assessment = CareerPathEmployeeAssessment::insert($sql_data_competence);
    
                    $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();                
                    
                    $job->accepted   = $accepted_total;
                    $job->save();
                    
                    if($job_applicant->resume!=null&&$job_applicant->resume!=''){
                        $destinationPath_new = public_path() . '/assets/employee_doc/' . $job_applicant->resume;
                        $destinationPath_old = public_path() . '/assets/applicant_doc/' . $job_applicant->resume;
                        $move = File::move($destinationPath_old, $destinationPath_new);
                    
                        $employee_doc=new EmployeeFiles();
                        $employee_doc->emp_id=$emp_id;
                        $employee_doc->file_title=$job_applicant->resume;
                        $employee_doc->file=$job_applicant->resume;
                        $employee_doc->save();
                    }    
                    $psychotest_total = JobApplicants::where('job_id','=',$job_id)->where('status','=','Psychotest')->count();
                    $unread_total     = JobApplicants::where('job_id','=',$job_id)->where('status','=','Unread')->count();
                    $interview_total  = JobApplicants::where('job_id','=',$job_id)->where('status','=','Interview')->count();
                    $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();
            
                    $job->psychotest = $psychotest_total;
                    $job->interview  = $interview_total;
                    $job->unread     = $unread_total;
                    $job->accepted   = $accepted_total;
                    $job->save();
    
                }
            } 
            else {
                $job_applicant->sub_status_psychotest = $request->status_psychotest = "not pass";
                $job_applicant->status                = $request->status = "Rejected";
                $job_applicant->save();
    
                $psychotest_total = JobApplicants::where('job_id','=',$job_id)->where('status','=','Psychotest')->count();
                $unread_total     = JobApplicants::where('job_id','=',$job_id)->where('status','=','Unread')->count();
                $interview_total  = JobApplicants::where('job_id','=',$job_id)->where('status','=','Interview')->count();
                $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();
        
                $job->psychotest = $psychotest_total;
                $job->interview  = $interview_total;
                $job->unread     = $unread_total;
                $job->accepted   = $accepted_total;
                $job->save();

                $gen_app                  = new GeneralApplicants();
                $gen_app->name            = $job_applicant->name;
                $gen_app->email           = $job_applicant->email;
                $gen_app->phone           = $job_applicant->phone;
                $gen_app->address_domisili         = $job_applicant->address_domisili;
                $gen_app->village_domisili         = $job_applicant->village_domisili;
                $gen_app->district_domisili        = $job_applicant->district_domisili;
                $gen_app->city_domisili            = $job_applicant->city_domisili;
                $gen_app->address         = $job_applicant->address;
                $gen_app->village         = $job_applicant->village;
                $gen_app->district        = $job_applicant->district;
                $gen_app->city            = $job_applicant->city;
                $gen_app->experience      = $job_applicant->experience;
                $gen_app->education       = $job_applicant->education;
                $gen_app->resume          = $job_applicant->resume;
                $gen_app->photo           = $job_applicant->photo;
                $gen_app->save();
            }

            $job_applicant->save();

            return redirect('jobs/view-applicant/'.$job_id)->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('jobs/view-applicant/'.$job_id)->with([
                'message'=> language_data('Applicant not found'),
                'message_important'=>true
            ]);
        }

    }

    /* setApplicantInterviewStatus  Function Start Here */
    public function setApplicantInterviewStatus(Request $request)
    {

        $job_id=Input::get('job_id');
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
        ]);

        if($v->fails()){
            return redirect('jobs/view-applicant/'.$job_id)->withErrors($v->fails());
        }

        $job_applicant  = JobApplicants::find($cmd);
        $job            = Jobs::find($job_applicant->job_id);
        $designation    = Designation::find($job->position);
        $last_id = DB::table('sys_employee')->orderBy('id','asc')->value('id');
        $project_need   = ProjectNeeds::where('id','=',$job->project)->where('id_job','=',$job->id)->first();
        if($job_applicant){
            $job_applicant->sub_status_interview    = $request->status_interview;
            $job_applicant->interview_schedule      = get_date_format_inggris($request->interview_schedule);
            $job_applicant->interview_information   = $request->information_interview;

            if($job_applicant->interview_file==''){
                if(Input::file('interview_file')){
                    $file                                   = Input::file('interview_file');
                    $destinationPath                        = public_path() . '/assets/interview_files/';
                    $file_name                              = $file->getClientOriginalName();
                    Input::file('interview_file')->move($destinationPath, $file_name);
                    $job_applicant->interview_file          = $file_name;
                }
            }

            if($request->status == "Confirm")
            { 
                $job_applicant->status              = $request->status;
                $job_applicant->save();

                $name = trim($job_applicant->name);
                $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
                $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );

                //Penomoran id
                $last_number_id = Employee::max('id');
                if($last_number_id==''){
                    $number_id = 1;
                } else {
                    $number_id = 1 + $last_number_id;
                }
                $month=date('m');
                $thn=date('y');
                $time = $month.$thn;
                $company_area = $job->company_name->company_code;
                $emmployee_code=Employee::select(DB::raw("SUBSTRING(employee_code,1,3) as 'AREA'"),DB::raw("SUBSTRING(employee_code,4,7) as 'TIME'"),DB::raw("SUBSTRING(employee_code,8,11) as 'kode'"))
                // ->having('AREA','=',$company_area)
                // ->having('TIME','=',$time)
                ->orderBy('kode', 'asc')
                ->first();
            
                if( $emmployee_code){
                    $kode= $emmployee_code->kode+1;
                    
                }else{
                    $kode="1";
                }
                
   
                $kodeemployee = str_pad($company_area,3,'0',STR_PAD_LEFT).$time.''.str_pad($kode,4,'0',STR_PAD_LEFT);
                $employee_code= $kodeemployee;
                    
                $exist = Employee::where('no_ktp','=',$job_applicant->ktp)->first();
                if($exist){
                    $exist->user_name       = $employee_code;
                    $exist->employee_code   = $employee_code;
                    $exist->no_ktp          = $job_applicant->ktp;
                    $exist->project         = $job->project;
                    $exist->company         = $job->company;
                    $exist->employee_type   = $job->company_name->category;
                    $exist->location        = $job->job_location;
                    $exist->designation     = $job->position;
                    $exist->department      = $designation->did;
                    $exist->email           = $job_applicant->email;
                    $exist->phone           = $job_applicant->phone;
                    $exist->pre_address     = $job_applicant->address_domisili;
                    $exist->pre_village     = $job_applicant->village_domisili;
                    $exist->pre_district    = $job_applicant->district_domisili;
                    $exist->pre_city        = $job_applicant->city_domisili;
                    $exist->per_address     = $job_applicant->address;
                    $exist->per_village     = $job_applicant->village;
                    $exist->per_district    = $job_applicant->district;
                    $exist->per_city        = $job_applicant->city;
                    $exist->doj             = date('Y-m-d');
                    $exist->dol             = get_date_format_inggris($job->project_name->end_date);
                    $exist->employee_status = 'HARIAN';
                    $exist->status          = 'active';
                    $exist->save();
    
                    $emp_id = $exist->id;

                } else {
                    $employee                  = new Employee();
                    $employee->id              = $number_id;
                    $employee->fname           = $job_applicant->name;
                    $employee->user_name       = $employee_code;
                    $employee->password        = bcrypt($job_applicant->birth_date);
                    $employee->employee_code   = $employee_code;
                    $employee->no_ktp          = $job_applicant->ktp;
                    $employee->project         = $job->project;
                    $employee->company         = $job->company;
                    $employee->employee_type   = $job->company_name->category;
                    $employee->location        = $job->job_location;
                    $employee->designation     = $job->position;
                    $employee->department      = $designation->did;
                    $employee->email           = $job_applicant->email;
                    $employee->phone           = $job_applicant->phone;
                    $employee->pre_address     = $job_applicant->address_domisili;
                    $employee->pre_village     = $job_applicant->village_domisili;
                    $employee->pre_district    = $job_applicant->district_domisili;
                    $employee->pre_city        = $job_applicant->city_domisili;
                    $employee->per_address     = $job_applicant->address;
                    $employee->per_village     = $job_applicant->village;
                    $employee->per_district    = $job_applicant->district;
                    $employee->per_city        = $job_applicant->city;
                    $employee->last_education  = $job_applicant->education;
                    $employee->payment_type    = $project_need->id_payment;
                    $employee->avatar          = $job_applicant->photo;
                    $employee->doj             = date('Y-m-d');
                    $employee->dol             = get_date_format_inggris($job->project_name->end_date);
                    $employee->employee_status = 'HARIAN';
                    $employee->status          = 'active';
                    $employee->save();
    
                    $emp_id = $employee->id;
                }

                if($job_applicant->photo!=null&&$job_applicant->photo!=''){
                    $old_path = public_path() . '/assets/applicant_doc/' . $job_applicant->photo;
                    $destinationPath = public_path() . '/assets/employee_pic/' . $job_applicant->photo;
                    $move = File::move($old_path,$destinationPath);
                }

                $employment = new EmploymentHistory();
                $employment->emp_id = $emp_id;
                $employment->project = $job->project;
                $employment->designation = $job->position;
                $employment->department = $designation->did;
                $employment->location = $job->job_location;
                $employment->save();

                $employee_bank = new EmployeeBankAccount();
                $employee_bank->emp_id = $emp_id;
                $employee_bank->save();

                
                //Table CareerPathEmpolyeeAssessment Save
                $num_element_competences = 0;

                $career_path = CareerPathEmployee::where('designation','=',$designation->career)->get();
                $career_path_competence=[];
                foreach($career_path as $cp){
                    $career_path_competence = CareerPathEmployeeCompetence::where('career_path','=',$cp->id)->get()->toArray();
                }

                $sql_data_competence = array();

                while($num_element_competences<count($career_path_competence)){
                    $sql_data_competence[] = array(
                        'id'            => "",
                        'emp_id'        => $emp_id,
                        'competence'    => $career_path_competence[$num_element_competences]['id']
                    );
                    $num_element_competences++;
                }

                $career_assessment = CareerPathEmployeeAssessment::insert($sql_data_competence);

                $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();                
                
                $job->accepted   = $accepted_total;
                $job->save();
                
                if($job_applicant->resume!=null&&$job_applicant->resume!=''){
                    $destinationPath_new = public_path() . '/assets/employee_doc/' . $job_applicant->resume;
                    $destinationPath_old = public_path() . '/assets/applicant_doc/' . $job_applicant->resume;
                    $move = File::move($destinationPath_old, $destinationPath_new);
                
                    $employee_doc=new EmployeeFiles();
                    $employee_doc->emp_id=$emp_id;
                    $employee_doc->file_title=$job_applicant->resume;
                    $employee_doc->file=$job_applicant->resume;
                    $employee_doc->save();
                }
            } else if($request->status_interview == "pass") {
                $job_applicant->status              = $request->status = "Waiting List";
            } else if($request->status_interview == "not pass") { 
                $job_applicant->status              = $request->status = "Rejected";

                $gen_app                  = new GeneralApplicants();
                $gen_app->name            = $job_applicant->name;
                $gen_app->email           = $job_applicant->email;
                $gen_app->phone           = $job_applicant->phone;
                $gen_app->address_domisili         = $job_applicant->address_domisili;
                $gen_app->village_domisili         = $job_applicant->village_domisili;
                $gen_app->district_domisili        = $job_applicant->district_domisili;
                $gen_app->city_domisili            = $job_applicant->city_domisili;
                $gen_app->address         = $job_applicant->address;
                $gen_app->village         = $job_applicant->village;
                $gen_app->district        = $job_applicant->district;
                $gen_app->city            = $job_applicant->city;
                $gen_app->experience      = $job_applicant->experience;
                $gen_app->education       = $job_applicant->education;
                $gen_app->resume          = $job_applicant->resume;
                $gen_app->photo           = $job_applicant->photo;
                $gen_app->save();
            } else {
                $job_applicant->status              = $request->status;
            }
            $job_applicant->save();

            return redirect('jobs/view-applicant/'.$job_id)->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('jobs/view-applicant/'.$job_id)->with([
                'message'=> language_data('Applicant not found'),
                'message_important'=>true
            ]);
        }

    }

    /* setApplicantHealthtestStatus  Function Start Here */
    public function setApplicantHealthtestStatus(Request $request)
    {

        $job_id=Input::get('job_id');
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
        ]);

        if($v->fails()){
            return redirect('jobs/view-applicant/'.$job_id)->withErrors($v->fails());
        }

        $job_applicant  = JobApplicants::find($cmd);
        $job            = Jobs::find($job_applicant->job_id);
        $designation    = Designation::find($job->position);
        $last_id = DB::table('sys_employee')->orderBy('id','asc')->value('id');
        $project_need   = ProjectNeeds::where('id','=',$job->project)->where('id_job','=',$job->id)->first();
        if($job_applicant){
            $job_applicant->sub_status_healthtest    = $request->status_healthtest;
            $job_applicant->healthtest_schedule      = get_date_format_inggris($request->healthtest_schedule);
            $job_applicant->healthtest_information   = $request->information_healthtest;

            if($job_applicant->healthtest_file==''){
                $file                                   = Input::file('healthtest_file');
                $destinationPath                        = public_path() . '/assets/healthtest_files/';
                $file_name                              = $file->getClientOriginalName();
                Input::file('healthtest_file')->move($destinationPath, $file_name);
                $job_applicant->healthtest_file          = $file_name;
            }

            if($request->status_healthtest == "accepted")
            { 
                $job_applicant->status              = $request->status = "Confirm";
                $job_applicant->save();
                
                $name = trim($job_applicant->name);
                $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
                $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );

                //Penomoran id
                $last_number_id = Employee::max('id');
                if($last_number_id==''){
                    $number_id = 1;
                } else {
                    $number_id = 1 + $last_number_id;
                }
                $month=date('m');
                $thn=date('y');
                $time = $month.$thn;
                $company_area = $job->company_name->company_code;
                $emmployee_code=Employee::select(DB::raw("SUBSTRING(employee_code,1,3) as 'AREA'"),DB::raw("SUBSTRING(employee_code,4,7) as 'TIME'"),DB::raw("SUBSTRING(employee_code,8,11) as 'kode'"))
                // ->having('AREA','=',$company_area)
                // ->having('TIME','=',$time)
                ->orderBy('kode', 'asc')
                ->first();
            
                if( $emmployee_code){
                    $kode= $emmployee_code->kode+1;
                    
                }else{
                    $kode="1";
                }
                
   
                $kodeemployee = str_pad($company_area,3,'0',STR_PAD_LEFT).$time.''.str_pad($kode,4,'0',STR_PAD_LEFT);
                $employee_code= $kodeemployee;
                
   
                $exist = Employee::where('no_ktp','=',$job_applicant->ktp)->first();
                if($exist){
                    $exist->user_name       = $employee_code;
                    $exist->employee_code   = $employee_code;
                    $exist->no_ktp          = $job_applicant->ktp;
                    $exist->project         = $job->project;
                    $exist->company         = $job->company;
                    $exist->employee_type   = $job->company_name->category;
                    $exist->location        = $job->job_location;
                    $exist->designation     = $job->position;
                    $exist->department      = $designation->did;
                    $exist->email           = $job_applicant->email;
                    $exist->phone           = $job_applicant->phone;
                    $exist->pre_address     = $job_applicant->address_domisili;
                    $exist->pre_village     = $job_applicant->village_domisili;
                    $exist->pre_district    = $job_applicant->district_domisili;
                    $exist->pre_city        = $job_applicant->city_domisili;
                    $exist->per_address     = $job_applicant->address;
                    $exist->per_village     = $job_applicant->village;
                    $exist->per_district    = $job_applicant->district;
                    $exist->per_city        = $job_applicant->city;
                    $exist->doj             = date('Y-m-d');
                    $exist->dol             = get_date_format_inggris($job->project_name->end_date);
                    $exist->employee_status = 'HARIAN';
                    $exist->status          = 'active';
                    $exist->save();
    
                    $emp_id = $exist->id;

                } else {
                    $employee                  = new Employee();
                    $employee->id              = $number_id;
                    $employee->fname           = $job_applicant->name;
                    $employee->user_name       = $employee_code;
                    $employee->password        = bcrypt($job_applicant->birth_date);
                    $employee->employee_code   = $employee_code;
                    $employee->no_ktp          = $job_applicant->ktp;
                    $employee->project         = $job->project;
                    $employee->company         = $job->company;
                    $employee->employee_type   = $job->company_name->category;
                    $employee->location        = $job->job_location;
                    $employee->designation     = $job->position;
                    $employee->department      = $designation->did;
                    $employee->email           = $job_applicant->email;
                    $employee->phone           = $job_applicant->phone;
                    $employee->pre_address     = $job_applicant->address_domisili;
                    $employee->pre_village     = $job_applicant->village_domisili;
                    $employee->pre_district    = $job_applicant->district_domisili;
                    $employee->pre_city        = $job_applicant->city_domisili;
                    $employee->per_address     = $job_applicant->address;
                    $employee->per_village     = $job_applicant->village;
                    $employee->per_district    = $job_applicant->district;
                    $employee->per_city        = $job_applicant->city;
                    $employee->last_education  = $job_applicant->education;
                    $employee->payment_type    = $project_need->id_payment;
                    $employee->avatar          = $job_applicant->photo;
                    $employee->doj             = date('Y-m-d');
                    $employee->dol             = get_date_format_inggris($job->project_name->end_date);
                    $employee->employee_status = 'HARIAN';
                    $employee->status          = 'active';
                    $employee->save();
    
                    $emp_id = $employee->id;
                }
                if($job_applicant->photo!=null&&$job_applicant->photo!=''){
                    $old_path = public_path() . '/assets/applicant_doc/' . $job_applicant->photo;
                    $destinationPath = public_path() . '/assets/employee_pic/' . $job_applicant->photo;
                    $move = File::move($old_path,$destinationPath);
                }

                $employment = new EmploymentHistory();
                $employment->emp_id = $emp_id;
                $employment->project = $job->project;
                $employment->designation = $job->position;
                $employment->department = $designation->did;
                $employment->location = $job->job_location;
                $employment->save();

                $employee_bank = new EmployeeBankAccount();
                $employee_bank->emp_id = $emp_id;
                $employee_bank->save();

                
                //Table CareerPathEmpolyeeAssessment Save
                $num_element_competences = 0;

                $career_path = CareerPathEmployee::where('designation','=',$designation->career)->get();

                foreach($career_path as $cp){
                    $career_path_competence = CareerPathEmployeeCompetence::where('career_path','=',$cp->id)->get()->toArray();
                }

                $sql_data_competence = array();

                while($num_element_competences<count($career_path_competence)){
                    $sql_data_competence[] = array(
                        'id'            => "",
                        'emp_id'        => $emp_id,
                        'competence'    => $career_path_competence[$num_element_competences]['id']
                    );
                    $num_element_competences++;
                }

                $career_assessment = CareerPathEmployeeAssessment::insert($sql_data_competence);

                $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();                
                
                $job->accepted   = $accepted_total;
                $job->save();
                
                
                if($job_applicant->resume!=null&&$job_applicant->resume!=''){
                    $destinationPath_new = public_path() . '/assets/employee_doc/' . $job_applicant->resume;
                    $destinationPath_old = public_path() . '/assets/applicant_doc/' . $job_applicant->resume;
                    $move = File::move($destinationPath_old, $destinationPath_new);
                
                    $employee_doc=new EmployeeFiles();
                    $employee_doc->emp_id=$emp_id;
                    $employee_doc->file_title=$job_applicant->resume;
                    $employee_doc->file=$job_applicant->resume;
                    $employee_doc->save();
                }
            }
            elseif($request->status_healthtest == "pass")
            { 
                $job_applicant->status              = $request->status = "Waiting List";
                $job_applicant->save();
            }
            elseif($request->status_healthtest == "not pass")
            { 
                $job_applicant->status              = $request->status = "Rejected";
                $job_applicant->save();

                $gen_app                  = new GeneralApplicants();
                $gen_app->name            = $job_applicant->name;
                $gen_app->email           = $job_applicant->email;
                $gen_app->phone           = $job_applicant->phone;
                $gen_app->address_domisili         = $job_applicant->address_domisili;
                $gen_app->village_domisili         = $job_applicant->village_domisili;
                $gen_app->district_domisili        = $job_applicant->district_domisili;
                $gen_app->city_domisili            = $job_applicant->city_domisili;
                $gen_app->address         = $job_applicant->address;
                $gen_app->village         = $job_applicant->village;
                $gen_app->district        = $job_applicant->district;
                $gen_app->city            = $job_applicant->city;
                $gen_app->experience      = $job_applicant->experience;
                $gen_app->education       = $job_applicant->education;
                $gen_app->resume          = $job_applicant->resume;
                $gen_app->photo           = $job_applicant->photo;
                $gen_app->save();
            }

            $job_applicant->save();

            return redirect('jobs/view-applicant/'.$job_id)->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('jobs/view-applicant/'.$job_id)->with([
                'message'=> language_data('Applicant not found'),
                'message_important'=>true
            ]);
        }

    }

    /* setApplicantWaitingListStatus  Function Start Here */
    public function setApplicantWaitingListStatus(Request $request)
    {
        $job_id=Input::get('job_id');
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
        ]);

        if($v->fails()){
            return redirect('jobs/view-applicant/'.$job_id)->withErrors($v->fails());
        }

        $job_applicant  = JobApplicants::find($cmd);
        $job            = Jobs::find($job_applicant->job_id);
        $designation    = Designation::find($job->position);
        $last_id = DB::table('sys_employee')->orderBy('id','asc')->value('id');
        $project_need   = ProjectNeeds::where('id','=',$job->project)->where('id_job','=',$job->id)->first();
        if($job_applicant){
            $job_applicant->status = $request->status;

            if($request->status == "Confirm")
            { 
                $name = trim($job_applicant->name);
                $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
                $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );

                //Penomoran id
                $last_number_id = Employee::max('id');
                if($last_number_id==''){
                    $number_id = 1;
                } else {
                    $number_id = 1 + $last_number_id;
                }
                $month=date('m');
                $thn=date('y');
                $time = $month.$thn;
                $company_area = $job->company_name->company_code;
                $emmployee_code=Employee::select(DB::raw("SUBSTRING(employee_code,1,3) as 'AREA'"),DB::raw("SUBSTRING(employee_code,4,7) as 'TIME'"),DB::raw("SUBSTRING(employee_code,8,11) as 'kode'"))
                // ->having('AREA','=',$company_area)
                // ->having('TIME','=',$time)
                ->orderBy('kode', 'asc')
                ->first();
            
                if( $emmployee_code){
                    $kode= $emmployee_code->kode+1;
                    
                }else{
                    $kode="1";
                }
                
   
                $kodeemployee = str_pad($company_area,3,'0',STR_PAD_LEFT).$time.''.str_pad($kode,4,'0',STR_PAD_LEFT);
                $employee_code= $kodeemployee;
                
       
                $exist = Employee::where('no_ktp','=',$job_applicant->ktp)->first();
                if($exist){
                    $exist->user_name       = $employee_code;
                    $exist->employee_code   = $employee_code;
                    $exist->no_ktp          = $job_applicant->ktp;
                    $exist->project         = $job->project;
                    $exist->company         = $job->company;
                    $exist->employee_type   = $job->company_name->category;
                    $exist->location        = $job->job_location;
                    $exist->designation     = $job->position;
                    $exist->department      = $designation->did;
                    $exist->email           = $job_applicant->email;
                    $exist->phone           = $job_applicant->phone;
                    $exist->pre_address     = $job_applicant->address_domisili;
                    $exist->pre_village     = $job_applicant->village_domisili;
                    $exist->pre_district    = $job_applicant->district_domisili;
                    $exist->pre_city        = $job_applicant->city_domisili;
                    $exist->per_address     = $job_applicant->address;
                    $exist->per_village     = $job_applicant->village;
                    $exist->per_district    = $job_applicant->district;
                    $exist->per_city        = $job_applicant->city;
                    $exist->doj             = date('Y-m-d');
                    $exist->dol             = get_date_format_inggris($job->project_name->end_date);
                    $exist->employee_status = 'HARIAN';
                    $exist->status          = 'active';
                    $exist->save();
    
                    $emp_id = $exist->id;

                } else {
                    $employee                  = new Employee();
                    $employee->id              = $number_id;
                    $employee->fname           = $job_applicant->name;
                    $employee->user_name       = $employee_code;
                    $employee->password        = bcrypt($job_applicant->birth_date);
                    $employee->employee_code   = $employee_code;
                    $employee->no_ktp          = $job_applicant->ktp;
                    $employee->project         = $job->project;
                    $employee->company         = $job->company;
                    $employee->employee_type   = $job->company_name->category;
                    $employee->location        = $job->job_location;
                    $employee->designation     = $job->position;
                    $employee->department      = $designation->did;
                    $employee->email           = $job_applicant->email;
                    $employee->phone           = $job_applicant->phone;
                    $employee->pre_address     = $job_applicant->address_domisili;
                    $employee->pre_village     = $job_applicant->village_domisili;
                    $employee->pre_district    = $job_applicant->district_domisili;
                    $employee->pre_city        = $job_applicant->city_domisili;
                    $employee->per_address     = $job_applicant->address;
                    $employee->per_village     = $job_applicant->village;
                    $employee->per_district    = $job_applicant->district;
                    $employee->per_city        = $job_applicant->city;
                    $employee->last_education  = $job_applicant->education;
                    $employee->payment_type    = $project_need->id_payment;
                    $employee->avatar          = $job_applicant->photo;
                    $employee->doj             = date('Y-m-d');
                    $employee->dol             = get_date_format_inggris($job->project_name->end_date);
                    $employee->employee_status = 'HARIAN';
                    $employee->status          = 'active';
                    $employee->save();
    
                    $emp_id = $employee->id;
                }
                if($job_applicant->photo!=null&&$job_applicant->photo!=''){
                    $old_path = public_path() . '/assets/applicant_doc/' . $job_applicant->photo;
                    $destinationPath = public_path() . '/assets/employee_pic/' . $job_applicant->photo;
                    $move = File::move($old_path,$destinationPath);
                }

                $employment = new EmploymentHistory();
                $employment->emp_id = $emp_id;
                $employment->project = $job->project;
                $employment->designation = $job->position;
                $employment->department = $designation->did;
                $employment->location = $job->job_location;
                $employment->save();

                $employee_bank = new EmployeeBankAccount();
                $employee_bank->emp_id = $emp_id;
                $employee_bank->save();

                
                //Table CareerPathEmpolyeeAssessment Save
                $num_element_competences = 0;

                $career_path = CareerPathEmployee::where('designation','=',$designation->career)->get();

                foreach($career_path as $cp){
                    $career_path_competence = CareerPathEmployeeCompetence::where('career_path','=',$cp->id)->get()->toArray();
                }

                $sql_data_competence = array();

                while($num_element_competences<count($career_path_competence)){
                    $sql_data_competence[] = array(
                        'id'            => "",
                        'emp_id'        => $emp_id,
                        'competence'    => $career_path_competence[$num_element_competences]['id']
                    );
                    $num_element_competences++;
                }

                $career_assessment = CareerPathEmployeeAssessment::insert($sql_data_competence);

                $accepted_total   = JobApplicants::where('job_id','=',$job_id)->where('status','=','Confirm')->count();                
                
                $job->accepted   = $accepted_total;
                $job->save();

                if($job_applicant->resume!=null&&$job_applicant->resume!=''){
                    $destinationPath_new = public_path() . '/assets/employee_doc/' . $job_applicant->resume;
                    $destinationPath_old = public_path() . '/assets/applicant_doc/' . $job_applicant->resume;
                    $move = File::move($destinationPath_old, $destinationPath_new);
                
                    $employee_doc=new EmployeeFiles();
                    $employee_doc->emp_id=$emp_id;
                    $employee_doc->file_title=$job_applicant->resume;
                    $employee_doc->file=$job_applicant->resume;
                    $employee_doc->save();
                }
            }
            elseif($request->status == "Rejected"){
                $gen_app                  = new GeneralApplicants();
                $gen_app->name            = $job_applicant->name;
                $gen_app->email           = $job_applicant->email;
                $gen_app->phone           = $job_applicant->phone;
                $gen_app->address_domisili         = $job_applicant->address_domisili;
                $gen_app->village_domisili         = $job_applicant->village_domisili;
                $gen_app->district_domisili        = $job_applicant->district_domisili;
                $gen_app->city_domisili            = $job_applicant->city_domisili;
                $gen_app->address         = $job_applicant->address;
                $gen_app->village         = $job_applicant->village;
                $gen_app->district        = $job_applicant->district;
                $gen_app->city            = $job_applicant->city;
                $gen_app->experience      = $job_applicant->experience;
                $gen_app->education       = $job_applicant->education;
                $gen_app->resume          = $job_applicant->resume;
                $gen_app->photo           = $job_applicant->photo;
                $gen_app->save();

            } 
            
            $job_applicant->save();

            return redirect('jobs/view-applicant/'.$job_id)->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('jobs/view-applicant/'.$job_id)->with([
                'message'=> language_data('Applicant not found'),
                'message_important'=>true
            ]);
        }

    }

    /*function addApplicant start here */
    public function addApplicant(Request $request)
    {
        // echo "masuk";exit;

        $cmd        = Input::get('cmd');
        $id_old     = Input::get('id_old');
        $v = \Validator::make($request->all(), [
            'file'=>'max:500','photo'=>'max:500'
        ]);

        if ($v->fails()) {
            return redirect('jobs/view-applicant/'.$cmd)->withErrors($v->errors());
        }

        $email = Input::get('email');
        if ($email != '') {
            $exist = JobApplicants::where('email', '=', $email)->first();
            if ($exist) {
                return redirect('jobs/view-applicant/'.$cmd)->with([
                    'message' => language_data('Email Already Exist'),
                    'message_important' => true
                ]);
            }
        }
        
        $job            = Jobs::find($cmd);

        if($job) {
            $radio           = Input::get('newapp');   
            if($radio == "new"){
                $name = Input::get('name');
                $email = Input::get('email');
                $phone = Input::get('phone');
                $address_domisili = Input::get('address_domisili');
                $village_domisili = Input::get('village_domisili');
                $district_domisili = Input::get('district_domisili');
                $city_domisili = Input::get('city_domisili');
                $address = Input::get('address');
                $rt = Input::get('rt');
                $rw = Input::get('rw');
                $village = Input::get('village');
                $district = Input::get('district');
                $city = Input::get('city');
                $ktp = Input::get('ktp');
                $birth_place = Input::get('birth_place');
                $birth_date = Input::get('birth_date');
                $birth_date = get_date_format_inggris($birth_date);
                $gender = Input::get('gender');
                $marital_status = Input::get('marital_status');
                $religion = Input::get('religion');
                $ibu_kandung = Input::get('ibu_kandung');
                $experience = Input::get('experience');
                $designation = Input::get('designation');
                $last_education = Input::get('last_education');
                $file = Input::file('file');
                $photo = Input::file('photo');
                
                if ($ktp != '') {
                    $exist = JobApplicants::where('ktp', '=', $ktp)->first();
                    if ($exist) {
                        return redirect('jobs/view-applicant/'.$cmd)->with([
                            'message' => 'KTP Already Exist',
                            'message_important' => true
                        ]);
                    }
                }

                $destinationPath_file = public_path() . '/assets/applicant_doc/';
                $file_name       = '';
                $photo_name       = '';
                if($file){
                    $target_file    = $destinationPath_file . basename($_FILES["file"]["name"]);
                    $imageFileType  = pathinfo($target_file,PATHINFO_EXTENSION);   
                    if ($imageFileType == 'jpg' OR $imageFileType == 'png' OR $imageFileType == 'jpeg' OR $imageFileType == 'pdf') {
                        $destinationPath = public_path() . '/assets/applicant_doc/';
                        $file_name       = $file->getClientOriginalName();
                        Input::file('file')->move($destinationPath, $file_name);
        
                    } else {
                        return redirect('jobs/view-applicant/' . $cmd)->with([
                            'message' => 'Upload an Image or PDF',
                            'message_important' => true
                        ]);
                    }
                }
                if($photo){
                    $target_photo   = $destinationPath_file . basename($_FILES["photo"]["name"]);
                    $imageFileType_photo  = pathinfo($target_photo,PATHINFO_EXTENSION);       
                    if ($imageFileType_photo == 'jpg' OR $imageFileType_photo == 'png' OR $imageFileType_photo == 'jpeg' OR $imageFileType_photo == 'pdf') {
                        $destinationPath = public_path() . '/assets/applicant_doc/';
                        $photo_name       = $photo->getClientOriginalName();
                        Input::file('photo')->move($destinationPath, $photo_name);
        
                    } else {
                        return redirect('jobs/view-applicant/' . $cmd)->with([
                            'message' => 'Upload an Image or PDF',
                            'message_important' => true
                        ]);
                    }
                }
                
                $job_applicant                  = new JobApplicants();
                $job_applicant->job_id          = $cmd;
                $job_applicant->project         = $job->project;
                $job_applicant->name            = $name;
                $job_applicant->email           = $email;
                $job_applicant->phone           = $phone;
                $job_applicant->address_domisili         = $address_domisili;
                $job_applicant->village_domisili         = $village_domisili;
                $job_applicant->district_domisili        = $district_domisili;
                $job_applicant->city_domisili            = $city_domisili;
                $job_applicant->address         = $address.', RT : '.$rt.' / RW : '.$rw;
                $job_applicant->village         = $village;
                $job_applicant->district        = $district;
                $job_applicant->city            = $city;
                $job_applicant->ktp             = $ktp;
                $job_applicant->birth_place     = $birth_place;
                $job_applicant->birth_date      = $birth_date;
                $job_applicant->gender          = $gender;
                $job_applicant->marital_status  = $marital_status;
                $job_applicant->religion        = $religion;
                $job_applicant->ibu_kandung     = $ibu_kandung;
                $job_applicant->experience      = $experience;
                $job_applicant->designation     = $designation;
                $job_applicant->education       = $last_education;
                $job_applicant->status          = 'Unread';
                $job_applicant->resume          = $file_name;
                $job_applicant->photo           = $photo_name;
                $job_applicant->save();

                $psychotest_total = JobApplicants::where('job_id','=',$cmd)->where('status','=','Psychotest')->count();
                $unread_total     = JobApplicants::where('job_id','=',$cmd)->where('status','=','Unread')->count();
                $interview_total  = JobApplicants::where('job_id','=',$cmd)->where('status','=','Interview')->count();
        
                $job->psychotest = $psychotest_total;
                $job->interview  = $interview_total;
                $job->unread     = $unread_total;
                $job->save();
    
                return redirect('jobs/view-applicant/'.$cmd)->with([
                    'message' => 'Applicant Added Successfully'
                ]);
            } elseif($radio == "old") {
                $general_app         = GeneralApplicants::find($id_old);
                $name_old            = Input::get('name_old');
                $email_old           = Input::get('email_old');
                $phone_old           = Input::get('phone_old');
                $address_old         = Input::get('address_old');
                $experience_old      = Input::get('experience_old');
                $last_education_old  = Input::get('last_education_old');
                $file_old            = Input::get('file_old');
                $image_old           = Input::get('images_old');
                $status_old          = Input::get('status_old');

                $job_applicant                  = new JobApplicants();
                $job_applicant->job_id          = $cmd;
                $job_applicant->project         = $job->project;
                $job_applicant->name            = $name_old;
                $job_applicant->email           = $email_old;
                $job_applicant->phone           = $phone_old;
                $job_applicant->address         = $address_old;
                $job_applicant->village         = $general_app->village;
                $job_applicant->district        = $general_app->district;
                $job_applicant->city            = $general_app->city;
                $job_applicant->address_domisili         = $general_app->address_domisili;
                $job_applicant->village_domisili         = $general_app->village_domisili;
                $job_applicant->district_domisili        = $general_app->district_domisili;
                $job_applicant->city_domisili            = $general_app->city_domisili;
                $job_applicant->ktp             = $general_app->ktp;
                $job_applicant->birth_place     = $general_app->birth_place;
                $job_applicant->birth_date      = $general_app->birth_date;
                $job_applicant->gender          = $general_app->gender;
                $job_applicant->marital_status  = $general_app->marital_status;
                $job_applicant->religion        = $general_app->religion;
                $job_applicant->ibu_kandung     = $general_app->ibu_kandung;
                $job_applicant->designation     = $cmd;
                $job_applicant->experience      = $experience_old;
                $job_applicant->education       = $last_education_old;
                $job_applicant->resume          = $file_old;
                $job_applicant->photo           = $image_old;
                $job_applicant->status          = $status_old;
                $job_applicant->save();

                $general_app->delete();

                $psychotest_total = JobApplicants::where('job_id','=',$cmd)->where('status','=','Psychotest')->count();
                $unread_total     = JobApplicants::where('job_id','=',$cmd)->where('status','=','Unread')->count();
                $interview_total  = JobApplicants::where('job_id','=',$cmd)->where('status','=','Interview')->count();
        
                $job->psychotest = $psychotest_total;
                $job->interview  = $interview_total;
                $job->unread     = $unread_total;
                $job->save();
                
                return redirect('jobs/view-applicant/'.$cmd)->with([
                    'message' => 'Applicant Added Successfully'
                ]);
            }

            
    
        }


    }

    /* generalApplicants  Function Start Here */
    public function generalApplicants()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 38)->first();
        $general_applicants = GeneralApplicants::orderBy('id','asc')->get();
        $last_education = LastEducation::all();
        $designation = Designation::all();
        return view('admin.jobapplication.general-applicants', compact('general_applicants','last_education','designation','permcheck'));
    }

    /*function addGeneralApplicant start here */
    public function addGeneralApplicant(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'resume'=>'max:500','photo'=>'max:500'
        ]);

        if ($v->fails()) {
            return redirect('jobs/general-applicants')->withErrors($v->errors());
        }

        $email = Input::get('email');
        if ($email != '') {
            $exist = GeneralApplicants::where('email', '=', $email)->first();
            if ($exist) {
                return redirect('jobs/general-applicants')->with([
                    'message' => language_data('Email Already Exist'),
                    'message_important' => true
                ]);
            }
        }
                
        $name            = Input::get('name');
        $phone           = Input::get('phone');
        $address_domisili = Input::get('address_domisili');
        $village_domisili = Input::get('village_domisili');
        $district_domisili = Input::get('district_domisili');
        $city_domisili   = Input::get('city_domisili');
        $address         = Input::get('address');
        $rt              = Input::get('rt');
        $rw              = Input::get('rw');
        $village         = Input::get('village');
        $district        = Input::get('district');
        $city            = Input::get('city');
        $ktp             = Input::get('ktp');
        $birth_place     = Input::get('birth_place');
        $birth_date      = Input::get('birth_date');
        $birth_date      = get_date_format_inggris($birth_date);
        $gender          = Input::get('gender');
        $marital_status  = Input::get('marital_status');
        $religion        = Input::get('religion');
        $ibu_kandung     = Input::get('ibu_kandung');
        $designation     = Input::get('designation');
        $experience      = Input::get('experience');
        $last_education  = Input::get('last_education');
        $resume          = Input::file('resume');
        $photo           = Input::file('photo');
        $destinationPath_file = public_path() . '/assets/applicant_doc/';
        
        if ($ktp != '') {
            $exist = GeneralApplicants::where('ktp', '=', $ktp)->first();
            if ($exist) {
                return redirect('jobs/general-applicants')->with([
                    'message' => 'KTP Already Exist',
                    'message_important' => true
                ]);
            }
        }

        $destinationPath_file = public_path() . '/assets/applicant_doc/';
        $file_name       = '';
        $photo_name       = '';
        if($resume){
            $target_file    = $destinationPath_file . basename($_FILES["resume"]["name"]);
            $imageFileType  = pathinfo($target_file,PATHINFO_EXTENSION);   
            if ($imageFileType == 'jpg' OR $imageFileType == 'png' OR $imageFileType == 'jpeg' OR $imageFileType == 'pdf') {
                $destinationPath = public_path() . '/assets/applicant_doc/';
                $file_name       = $resume->getClientOriginalName();
                Input::file('resume')->move($destinationPath, $file_name);

            } else {
                return redirect('jobs/general-applicants')->with([
                    'message' => 'Upload an Image or PDF',
                    'message_important' => true
                ]);
            }
        }
        if($photo){
            $target_photo   = $destinationPath_file . basename($_FILES["photo"]["name"]);
            $imageFileType_photo  = pathinfo($target_photo,PATHINFO_EXTENSION);       
            if ($imageFileType_photo == 'jpg' OR $imageFileType_photo == 'png' OR $imageFileType_photo == 'jpeg' OR $imageFileType_photo == 'pdf') {
                $destinationPath = public_path() . '/assets/applicant_doc/';
                $photo_name       = $photo->getClientOriginalName();
                Input::file('photo')->move($destinationPath, $photo_name);

            } else {
                return redirect('jobs/general-applicants')->with([
                    'message' => 'Upload an Image or PDF',
                    'message_important' => true
                ]);
            }
        }

        $general_applicant                  = new GeneralApplicants();
        $general_applicant->name            = $name;
        $general_applicant->email           = $email;
        $general_applicant->phone           = $phone;
        $general_applicant->address_domisili         = $address_domisili;
        $general_applicant->village_domisili         = $village_domisili;
        $general_applicant->district_domisili        = $district_domisili;
        $general_applicant->city_domisili            = $city_domisili;
        $general_applicant->address         = $address.', RT : '.$rt.' / RW : '.$rw;
        $general_applicant->village         = $village;
        $general_applicant->district        = $district;
        $general_applicant->city            = $city;
        $general_applicant->ktp             = $ktp;
        $general_applicant->birth_place     = $birth_place;
        $general_applicant->birth_date      = $birth_date;
        $general_applicant->gender          = $gender;
        $general_applicant->marital_status  = $marital_status;
        $general_applicant->religion        = $religion;
        $general_applicant->ibu_kandung     = $ibu_kandung;
        $general_applicant->experience      = $experience;
        $general_applicant->designation     = $designation;
        $general_applicant->education       = $last_education;
        $general_applicant->resume          = $file_name;
        $general_applicant->photo           = $photo_name;
        $general_applicant->save();
        return redirect('jobs/general-applicants')->with([
            'message' => 'Applicant Added Successfully'
        ]);
    
            
    }

    /* deleteGeneralApplication  Function Start Here */
    public function deleteGeneralApplication($id)
    {
        $delete_app = GeneralApplicants::find($id);
        if ($delete_app) {
            $delete_app->delete();

            return redirect('jobs/general-applicants')->with([
                'message' => language_data('Applicant Deleted Successfully')
            ]);
        } else {
            return redirect('jobs')->with([
                'message' => language_data('Applicant not found'),
                'message_important' => true
            ]);
        }
    }

    public function downloadExcelPelamar()
    {        
        return response()->download(public_path('assets/template_xls/pelamar_pekerja.xls'));
    }
    /* download excel function start here */
    public function downloadExcelGeneral()
    {


        return response()->download(public_path('assets/template_xls/pelamar_umum.xls'));

    }
    
    /* import excel function start here */
    public function importExcelGeneral()
    {
        // Get current data from department table
        $general_applicants = GeneralApplicants::lists('ktp')->toArray();
        $last_education = LastEducation::all();
        
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file   = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
            })->get();
            if ($fileType != 'xls'){
                return redirect('jobs/general-applicants')->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else {
                
                if(!empty($data) && $data->count()){
                    $insert = array();
                    foreach ($data as $key => $value) {
                        // Skip department previously added using in_array
                        if (in_array($value->ktp, $general_applicants)){
                            continue;
                        } else{
                            //Penomoran id
                            $last_number = GeneralApplicants::max('id');
                            if($last_number==''){
                                $number = 1;
                            } else {
                                $number = 1 + $last_number;
                            }
                            if ($value->nama_lengkap=='') {
                                return redirect('jobs/general-applicants')->with([
                                    'message' => 'Applicant Not Found',
                                    'message_important' => true
                                ]);
                            } else {
                                $education = 0;
                                foreach ($last_education as $edu){
                                    if(strtolower($value->pendidikan_terakhir)==strtolower($edu->education)){
                                        $education = $edu->id;
                                    }
                                }
                                if(strtolower($value->jenis_kelamin) == 'laki-laki' || strtolower($value->jenis_kelamin) == 'laki- laki'|| strtolower($value->jenis_kelamin) == 'laki -laki'|| strtolower($value->jenis_kelamin) == 'laki - laki'|| strtolower($value->jenis_kelamin) == 'pria'){
                                    $jenis_kelamin = "male";
                                } else {
                                    $jenis_kelamin = "female";
                                }

                                $pengalaman = explode(" ", $value->pengalaman);

                                $applicants                     = new GeneralApplicants();
                                $applicants->id                 = $number;
                                $applicants->name               = $value->nama_lengkap;
                                $applicants->email              = $value->email;
                                $applicants->phone              = $value->telepon;
                                $applicants->address_domisili            = $value->alamat_domisili;
                                $applicants->village_domisili            = $value->kelurahan_domisili;
                                $applicants->district_domisili           = $value->kecamatan_domisili;
                                $applicants->city_domisili               = $value->kota_domisili;
                                $applicants->address            = $value->alamat_ktp;
                                $applicants->village            = $value->kelurahan_ktp;
                                $applicants->district           = $value->kecamatan_ktp;
                                $applicants->city               = $value->kota_ktp;
                                $applicants->ktp                = $value->ktp;
                                $applicants->birth_place        = $value->tempat_lahir;
                                $applicants->birth_date         = $value->tanggal_lahir;
                                $applicants->gender             = $jenis_kelamin;
                                $applicants->marital_status     = $value->status_kawin;
                                $applicants->religion           = $value->agama;
                                $applicants->ibu_kandung        = $value->ibu_kandung;
                                $applicants->education          = $education;
                                $applicants->experience         = $pengalaman[0];
                                $applicants->save();

                            }
                        }
                    }
                    return redirect('jobs/general-applicants')->with([
                        'message' => 'Applicant Added Successfully'
                    ]);
                } else{
                    return redirect('jobs/general-applicants')->with([
                        'message' => 'Applicant Not Found',
                        'message_important' => true
                    ]);
                } 
                
            }
			
		}
		return back();
    }

    //import excel lowongan pekerja
    public function importExcelPekerja()
    {
        // Get current data from department table
        $cmd        = Input::get('cmd');
        $job            = Jobs::find($cmd);
      

        if($job) {
        $general_applicants = JobApplicants::lists('ktp')->toArray();
        $last_education = LastEducation::all();
        
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file   = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
            })->get();
            if ($fileType != 'xls'){
                return redirect('jobs/view-applicant/'.$cmd)->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else {
                // var_dump($data);
                
                if(!empty($data) && $data->count()){
                    $insert = array();
                    foreach ($data as $key => $value) {
                        // Skip department previously added using in_array
                        if (in_array($value->ktp, $general_applicants)){
                            continue;
                        } else{
                            //Penomoran id
                            $last_number = JobApplicants::max('id');
                            if($last_number==''){
                                $number = 1;
                            } else {
                                $number = 1 + $last_number;
                            }
                            if ($value->nama_lengkap=='') {
                                return redirect('jobs/view-applicant/'.$cmd)->with([
                                    'message' => 'Applicant Not Found',
                                    'message_important' => true
                                ]);
                            } else {
                                $education = 0;
                                foreach ($last_education as $edu){
                                    if(strtolower($value->pendidikan_terakhir)==strtolower($edu->education)){
                                        $education = $edu->id;
                                    }
                                }
                                if(strtolower($value->jenis_kelamin) == 'laki-laki' || strtolower($value->jenis_kelamin) == 'laki- laki'|| strtolower($value->jenis_kelamin) == 'laki -laki'|| strtolower($value->jenis_kelamin) == 'laki - laki'|| strtolower($value->jenis_kelamin) == 'pria'){
                                    $jenis_kelamin = "male";
                                } else {
                                    $jenis_kelamin = "female";
                                }

                                $pengalaman = explode(" ", $value->pengalaman);

                                $applicants                     = new JobApplicants();
                                $applicants ->job_id            = $cmd;
                                $applicants ->project           = $job->project;
                                $applicants->id                 = $number;
                                $applicants->name               = $value->nama_lengkap;
                                $applicants->email              = $value->email;
                                $applicants->phone              = $value->telepon;
                                $applicants->address_domisili            = $value->alamat_domisili;
                                $applicants->village_domisili            = $value->kelurahan_domisili;
                                $applicants->district_domisili           = $value->kecamatan_domisili;
                                $applicants->city_domisili               = $value->kota_domisili;
                                $applicants->address            = $value->alamat_ktp;
                                $applicants->village            = $value->kelurahan_ktp;
                                $applicants->district           = $value->kecamatan_ktp;
                                $applicants->city               = $value->kota_ktp;
                                $applicants->ktp                = $value->ktp;
                                $applicants->birth_place        = $value->tempat_lahir;
                                $applicants->birth_date         = $value->tanggal_lahir;
                                $applicants->gender             = $jenis_kelamin;
                                $applicants->marital_status     = $value->status_kawin;
                                $applicants->religion           = $value->agama;
                                $applicants->ibu_kandung        = $value->ibu_kandung;
                                $applicants->education          = $education;
                                $applicants->experience         = $pengalaman[0];
                                $applicants->save();
                            }
                        }
                    }
                    return redirect('jobs/view-applicant/'.$cmd)->with([
                        'message' => 'Applicant Added Successfully'
                    ]);
                } else{
                    return redirect('jobs/view-applicant/'.$cmd)->with([
                        'message' => 'Applicant Not Found',
                        'message_important' => true
                    ]);
                } 
                
            }
			
        }
    }
    else{
        return redirect('jobs/view-applicant/'.$cmd)->with([
            'message' => 'No Jobs Yet',
            'message_important' => true
        ]);
    }
		return back();
    }

}
