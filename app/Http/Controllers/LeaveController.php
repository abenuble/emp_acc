<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Attendance;
use App\Employee;
use App\Leave;
use App\LeaveType;
use App\LeaveOutsourceReport;
use App\LeaveInternalReport;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DateTime;
use DatePeriod;
use DateInterval;
use DB;
date_default_timezone_set(app_config('Timezone'));
class LeaveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* leave  Function Start Here */
    public function leave()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 41)->first();
        $employee_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
        $leave_id = '';
        $employee_id = '';

        $employee=Employee::where('status','active')->where('role_id','!=','1')->get();
        $leave_type=LeaveType::all();
        $leave=Leave::orderBy('id','asc')->get();
        return view('admin.leave.leave',compact('leave_id','leave','leave_type','employee','employee_id','permcheck','employee_permission'));
    }

    /* viewLeave  Function Start Here */
    public function viewLeave($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 41)->first();

        $leave=Leave::find($id);

        return view('admin.leave.view-leave-application',compact('leave','permcheck'));

    }



    //======================================================================
    // postNewLeave Function Start Here
    //======================================================================
    public function postNewLeave(Request $request){
        $v=\Validator::make($request->all(),[
            'employee'=>'required','leave_type'=>'required','leave_from'=>'required','leave_type'=>'required','leave_to' => 'required', 'file'=>'required'
        ]);


        if ($v->fails()) {
            return redirect('leave')->withErrors($v->errors());
        }
        $employee_info          = Employee::find($request->employee);

        $remaining_leave        = $employee_info->remaining_leave;
        
        $file                   = Input::file('file');
        $destinationPath_file   = public_path() . '/assets/leave_file/';
        $target_file            = $destinationPath_file . basename($_FILES["file"]["name"]);
        $imageFileType          = pathinfo($target_file,PATHINFO_EXTENSION);
        $tembusan = Input::get('tembusan');

        $leave_from = get_date_format_inggris($request->leave_from);
        $leave_to = get_date_format_inggris($request->leave_to);

        if($request->leave_type==='3'){
            $leave_1 = strtotime($request->leave_from);
            $leave_2 = strtotime($request->leave_to);
            $leave_total = (($leave_2 - $leave_1)/86400)+1;    
        } else {
            $leave_total = 0;
        }

        if ($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'jpeg' || $imageFileType == 'pdf') {
            $destinationPath        = public_path() . '/assets/leave_file/';
            $file_name              = $file->getClientOriginalName();
            Input::file('file')->move($destinationPath, $file_name);

            $leave = new Leave();
            $leave->emp_id = $request->employee;
            $leave->tembusan = $request->tembusan;
            $leave->leave_from = $leave_from;
            $leave->leave_to = $leave_to;
            $leave->ltype_id = $request->leave_type;
            $leave->sick_name = $request->sick_name;
            $leave->applied_on = date('Y-m-d');
            $leave->leave_reason = $request->leave_reason;
            $leave->leave_file = $file_name;
            $leave->status = 'pending';
            $leave->save();

            return redirect('leave')->with([
                'message' => language_data('Leave added Successfully')
            ]);
        } else {
            return redirect('leave')->with([
                'message' => 'Upload an Image or PDF',
                'message_important' => true
            ]);
        }

        

        

    }


    /* postJobStatus  Function Start Here */
    public function postJobStatus(Request $request)
    {
        $cmd=Input::get('cmd');

        $leave=Leave::find($cmd);
        
        $file                   = Input::file('file');
        $destinationPath_file   = public_path() . '/assets/leave_file/';
        $target_file            = $destinationPath_file . basename($_FILES["file"]["name"]);
        $imageFileType          = pathinfo($target_file,PATHINFO_EXTENSION);

        if($leave){
            
            if ($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'jpeg' || $imageFileType == 'pdf') {
                $destinationPath        = public_path() . '/assets/leave_file/';
                $file_name              = $file->getClientOriginalName();
                Input::file('file')->move($destinationPath, $file_name);

                $leave->leave_file = $file_name;
                $leave->save();

                
                return redirect('leave')->with([
                    'message' => language_data('Leave added Successfully')
                ]);
            } else {
                return redirect('leave')->with([
                    'message' => 'Upload an Image or PDF',
                    'message_important' => true
                ]);
            }
        } else {
            return redirect('leave')->with([
                'message'=> language_data('Leave Application not found'),
                'message_important'=>true
            ]);
        }
    }

    /* deleteLeaveApplication  Function Start Here */
    public function deleteLeaveApplication($id)
    {

        $leave=Leave::find($id);
        if($leave){
            $leave->delete();
            return redirect('leave')->with([
                'message'=> language_data('Leave Application Deleted Successfully')
            ]);
        }else{
            return redirect('leave')->with([
                'message'=>'Leave Application not found',
                'message_important'=>true
            ]);
        }
    }

    /* getLeaveQuota  Function Start Here */
    public function getLeaveQuota(Request $request)
    {

        $leave_id = $request->leave_id;
        if ($leave_id) {
            $leave_type = LeaveType::where('id', $leave_id)->get();
            foreach ($leave_type as $d) {
                echo '<option value="' . $d->id . '">' . $d->leave_quota . '</option>';
            }
        }
    }

    /* getRemainingLeave  Function Start Here */
    public function getRemainingLeave(Request $request)
    {

        $employee_id = $request->employee_id;
        if ($employee_id) {
            $employee_leave = Employee::where('id', '=', $employee_id)->get();
            foreach ($employee_leave as $d) {
                echo '<option>' . $d->remaining_leave . '</option>';
            }
        }
    }

    /* downloadFile  Function Start Here */
    public function downloadFile($id)
    {
        $file = Leave::find($id)->leave_file;
        return response()->download(public_path('/assets/leave_file/' . $file));
    }


}
