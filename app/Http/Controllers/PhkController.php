<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\permission;
use App\Phk;
use App\PhkGround;
use App\Company;
use App\Notification;
use App\Copy;
use App\Project;
use App\ProjectNeeds;
use App\EmailTemplate;
use App\Employee;
use App\EmploymentHistory;
use App\ContractRecipient;
use App\EmployeeRolesPermission;
use Illuminate\Support\Facades\Input;
use DB;
use WkPdf;

class PhkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* contract  Function Start Here */
    public function terminations()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 51)->first();
        $comp_id = '';
        $proj_id = '';
        $emp_id = '';
        $des_id = '';

        $terminations = Phk::orderBy('id','asc')->get();
        $company = Company::all();
        $employee = Employee::where('role_id','!=','1')->get();
        $project = Project::all();
        $email_template = EmailTemplate::where('table_type','=','sys_phk')->get();
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        // $last_letter_number = DB::table('sys_phk')->orderBy('id','asc')->value('letter_number');
        // if( $last_letter_number){
        //     $last_number = explode('/', $last_letter_number);

        // }
        // else{
        //     $last_number=0;
        // }
        // if($date=='01'){
        //     $number = 01;
        // } else{
        //     $number = 1 + $last_number[0];
        // }
        $last_letter_number = DB::table('sys_phk')->where('letter_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('letter_number');
        if($last_letter_number==''){
            $number = 1;
        } else{
            $last_number = explode('/', $last_letter_number);
            $number = 1 + $last_number[0];
        }
        $comp = Company::find(\Auth::user()->company);
        return view('admin.termination.terminations', compact('des_id','comp_id','proj_id','emp_id','month','year','number','terminations','employee','company','comp','project','email_template','permcheck'));

    }

    /* getEmployeeCode  Function Start Here */
    public function getEmployeeCode(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee_code = Employee::where('id', $emp_id)->where('status','=','active')->get();
            foreach ($employee_code as $e) {
                echo '<option value="' . $e->employee_code . '">' . $e->employee_code . '</option>';
            }
        }
    }

    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project_number . '</option>';
            }
        }
    }

    /* getEmployee Function Start Here */
    public function getEmployee(Request $request)
    {

        $proj_id = $request->proj_id;
        if ($proj_id) {
            echo '<option value="0">Select Employee</option>';
            $employee = Employee::where('project', $proj_id)->where('role_id','!=',1)->where('status','active')->get();
            foreach ($employee as $e) {
                echo '<option value="' . $e->id . '">' . $e->fname . ' ' . $e->lname . '</option>';
            }
        }
    }

    /* deleteTermination  Function Start Here */
    public function deleteTermination($id)
    {

        $terminations = Phk::find($id);
        if ($terminations) {
            $terminations->delete();

            return redirect('terminations')->with([
                'message' => 'Termination Deleted Successfully'
            ]);
        } else {
            return redirect('terminations')->with([
                'message' => 'Termination Not Found',
                'message_important' => true
            ]);
        }

    }



    /* addTermination  Function Start Here */
    public function addTermination(Request $request)
    {


        $v = \Validator::make($request->all(), [
            'draft_letter' => 'required',
            
            'date' => 'required',
            'effective_date' => 'required',
            'project_number' => 'required',
            'company_name' => 'required',
            'employee_code' => 'required',
            'employee_name' => 'required',
            'insurance_provider' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('terminations')->withErrors($v->errors());
        }

        $month = date('m');
        $year = date('Y');
        $draft_letter = Input::get('draft_letter');
        $company="SSS";
        $employee_code = Input::get('employee_code');
        // $employee_code = implode(".", $employee_code);
        $employee_name = Input::get('employee_name');
        $letter_number = substr($employee_code,-4)."/RDG-".$company."/".get_roman_letters($month)."/".$year;
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $effective_date = Input::get('effective_date');
        $effective_date = get_date_format_inggris($effective_date);
        $project_number = Input::get('project_number');
        $company_name = Input::get('company_name');
        $insurance_provider = Input::get('insurance_provider');
        $copies = Input::get('copies');
        $grounds = Input::get('grounds');
        //Penomoran id
        $last_number_id = Phk::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $termination = new Phk();
        $termination->id = $number_id;
        $termination->draft_letter = $draft_letter;
        $termination->letter_number = $letter_number;
        $termination->date = $date;
        $termination->effective_date = $effective_date;
        $termination->project_number = $project_number;
        $termination->company_name = $company_name;
        $termination->employee_code = $employee_code;
        $termination->employee_name = $employee_name;
        $termination->insurance_provider = $insurance_provider;
        $termination->status = 'draft';
        $termination->save();
        
        $notification = new Notification();
        $notification->id_tag = $termination->id;
        $notification->tag = 'terminations';
        $notification->title = 'Persetujuan PHK';
        $notification->description = 'Persetujuan PHK Pegawai '.$employee_code;
        $notification->show_date = date('Y-m-d');
        $notification->start_date = $effective_date;
        $notification->end_date = $effective_date;
        $notification->route = 'approvals/terminations';
        $notification->save();

        $num_elements = 0;
        
        $sql_data = array();

        while($num_elements<count($copies)) {
            $sql_data[] = array(
                'letter_number' => $letter_number,
                'copies' => $copies[$num_elements],
            );
            $num_elements++;
        }

        $copy = Copy::insert($sql_data);

        $num_elements_grounds = 0;
        
        $sql_data = array();

        while($num_elements_grounds<count($grounds)) {
            $sql_data[] = array(
                'letter_number' => $letter_number,
                'grounds' => $grounds[$num_elements_grounds],
            );
            $num_elements_grounds++;
        }

        $PhkGround = PhkGround::insert($sql_data);


        if ($termination!='') {
            return redirect('terminations')->with([
                'message' => 'Termination Added Successfully'
            ]);

        } else {
            return redirect('terminations')->with([
                'message' => 'Termination Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateTermination  Function Start Here */
    public function updateTermination(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'effective_date' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('terminations')->withErrors($v->errors());
        }
        $termination = Phk::find($cmd);
        $insurance_provider = Input::get('insurance_provider');
        $effective_date = Input::get('effective_date');
        $effective_date = get_date_format_inggris($effective_date);
        $date = Input::get('date');
        $date = get_date_format_inggris($date);

        if ($termination) {
            $termination->effective_date = $effective_date;
            $termination->insurance_provider = $insurance_provider;
            $termination->date = $date;
            $termination->status = 'draft';
            $termination->save();
            
            $notification = Notification::where('tag','=','terminations')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->start_date = $effective_date;
                $notification->end_date = $effective_date;
                $notification->save();
            }

            return redirect('terminations')->with([
                'message' => 'Termination Updated Successfully'
            ]);

        } else {
            return redirect('terminations')->with([
                'message' => 'Termination Not Found',
                'message_important' => true
            ]);
        }
    }

    /* setTerminationStatus  Function Start Here */
    public function setTerminationStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('terminations')->withErrors($v->fails());
        }

        $terminations  = Phk::find($cmd);
        if($terminations){
            $terminations->status=$request->status;
            $terminations->save();

            return redirect('terminations')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('terminations')->with([
                'message' => 'Termination not found',
                'message_important'=>true
            ]);
        }

    }

    /* viewTermination  Function Start Here */
    public function viewTermination($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 51)->first();
        $terminations = Phk::find($id);
        $copies = Copy::where('letter_number','=',$terminations->letter_number)->get();
        $grounds = PhkGround::where('letter_number','=',$terminations->letter_number)->get();
        return view('admin.termination.termination-view', compact('grounds','copies','terminations','permcheck'));
    }
    
    /* editTermination  Function Start Here */
    public function editTermination($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 51)->first();
        $emp_id = '';
        $proj_id = '';
        $terminations = Phk::find($id);
        $copies = Copy::where('letter_number','=',$terminations->letter_number)->get();
        $grounds = PhkGround::where('letter_number','=',$terminations->letter_number)->get();
        return view('admin.termination.termination-edit', compact('grounds','copies','emp_id','proj_id','terminations','permcheck'));
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {

        $terminations = Phk::find($id);

        $tgl_msk =$terminations->employee_info->doj;
        $address = $terminations->employee_info->pre_address;
        $company =$terminations->employee_info->project_name->internal_company_name->company;
        $draft_letter = EmailTemplate::find($terminations->draft_letter);
        $employee_id = $terminations->employee_name;
        $employment     = EmploymentHistory::where('emp_id','=',$employee_id)->get();
        $contract       = ContractRecipient::where('recipients','=',$employee_id)->get();
        $letter_number = $terminations->letter_number;
        $date = get_date_format($terminations->date);
        $resign_date = get_date_format($terminations->resign_date);
        $project_number = $terminations->project_info->project_number;
        $company_name = $terminations->company_info->company;
        $company_location = $terminations->company_info->address;
        $employee_code = $terminations->employee_code;
        $employee_name = $terminations->employee_info->fname . " " . $terminations->employee_info->lname;
        $employee_address = $terminations->employee_info->pre_address;
        $employee_city = $terminations->employee_info->pre_city;
        $insurance_provider = $terminations->insurance_provider;
        $copy = Copy::where('letter_number','=',$letter_number)->get();

        foreach($copy as $c){
            $copies[] = $c->copies;
        }
        $ground = PhkGround::where('letter_number','=',$letter_number)->get();

        foreach($ground as $g){
            $grounds[] = $g->grounds;
        }
        
        $data = array(
            'letter_number' => $letter_number,
            'date' => $date,
            'resign_date' => $resign_date,
            'project_number' => $project_number,
            'company_name' => $company_name,
            'employee_code' => $employee_code,
            'employee_name' => $employee_name,
            'insurance_provider' => $insurance_provider,
            'employee_address' => $employee_address,
            'employee_city' => $employee_city,
            'company_location' => $company_location,
            'copies' => implode(", ", $copies),
            'grounds' => implode(", ", $grounds)
        );

        $template = $draft_letter->message;
        
        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.termination.pdf-termination', compact('message','letter_number','employment','contract','resign_date','tgl_msk','address','employee_name','company'));

        return $pdf->stream($letter_number.'.pdf');
    }
    
}
