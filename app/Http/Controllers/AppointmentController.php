<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\permission;
use App\Appointment;
use App\Company;
use App\Notification;
use App\Project;
use App\ProjectNeeds;
use App\EmailTemplate;
use App\Employee;
use App\EmployeeRolesPermission;
use Illuminate\Support\Facades\Input;
use DB;
use WkPdf;

class AppointmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* contract  Function Start Here */
    public function appointments()
    {
        // $date = get_date_format(date('d/m/Y'));
        // echo $date; exit;
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 47)->first();
        $comp_id = '';
        $proj_id = '';
        $emp_id = '';
        $des_id = '';


        $appointments = Appointment::orderBy('id','asc')->get();
        $company = Company::all();
        $employee = Employee::where('role_id','!=','1')->get();
        $project = Project::all();
        $email_template = EmailTemplate::where('table_type','=','sys_appointments')->get();
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        $last_letter_number = DB::table('sys_appointments')->orderBy('id','asc')->value('letter_number');
        if($last_letter_number){
            $last_number = explode('/', $last_letter_number);
        }
        else{
            $last_number=0;
        }
        if($date=='01'){
            $number = 01;
        } else{
            $number = 1 + $last_number[0];
        }
        $comp = Company::find(\Auth::user()->company);
        return view('admin.appointment.appointments', compact('des_id','comp_id','proj_id','emp_id','month','year','number','appointments','employee','company','comp','project','email_template','permcheck'));

    }

    /* getDesignation  Function Start Here */
    public function getDesignation(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $designation = Employee::where('id', $emp_id)->get();
            foreach ($designation as $d) {
                echo '<option value="' . $d->designation . '">' . $d->designation_name->department_name->department . ' (' . $d->designation_name->designation . ')</option>';
            }
        }
    }

    /* getDesignationNew  Function Start Here */
    public function getDesignationNew(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $designation = Employee::where('id', $emp_id)->first();
            echo '<option value="' . $designation->designation_name->id . '">' . $designation->designation_name->department_name->department . ' (' . $designation->designation_name->designation . ')</option>';
            echo '<option value="' . $designation->designation_name->career_info->id . '">' . $designation->designation_name->career_info->department_name->department . ' (' . $designation->designation_name->career_info->designation . ')</option>';
        }
    }

    /* getLocation  Function Start Here */
    public function getLocation(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $location = Employee::where('id', $emp_id)->get();
            foreach ($location as $l) {
                echo '<option value="' . $l->location . '">' . $l->location . '</option>';
            }
        }
    }

    /* getLocation  Function Start Here */
    public function getLocationNew(Request $request)
    {
        $proj_id = $request->proj_id;
        if ($proj_id) {
            $location = ProjectNeeds::where('id', $proj_id)->get();
            foreach ($location as $l) {
                echo '<option value="' . $l->location . '">' . $l->location . '</option>';
            }
        }
    }

    /* getEmployeeCode  Function Start Here */
    public function getEmployeeCode(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee_code = Employee::where('id', $emp_id)->where('status','=','active')->get();
            foreach ($employee_code as $e) {
                echo '<option value="' . $e->employee_code . '">' . $e->employee_code . '</option>';
            }
        }
    }
    
    /* getEmployeeStatus  Function Start Here */
    public function getEmployeeStatus(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee = Employee::where('id', $emp_id)->where('status','=','active')->get();
            foreach ($employee as $e) {
                echo '<option value="' . $e->employee_status . '">' . $e->employee_status . '</option>';
            }
        }
    }

    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project_number . '</option>';
            }
        }
    }

    /* getEmployee Function Start Here */
    public function getEmployee(Request $request)
    {

        $proj_id = $request->proj_id;
        if ($proj_id) {
            echo '<option value="0">Select Employee</option>';
            $employee = Employee::where('project', $proj_id)->where('role_id','!=',1)->where('status','=','active')->get();
            foreach ($employee as $e) {
                echo '<option value="' . $e->id . '">' . $e->fname . ' ' . $e->lname . '</option>';
            }
        }
    }

    /* deleteAppointment  Function Start Here */
    public function deleteAppointment($id)
    {

        $appointments = Appointment::find($id);
        if ($appointments) {
            $appointments->delete();

            return redirect('appointments')->with([
                'message' => 'Appointment Deleted Successfully'
            ]);
        } else {
            return redirect('appointments')->with([
                'message' => 'Appointment Not Found',
                'message_important' => true
            ]);
        }

    }



    /* addAppointment  Function Start Here */
    public function addAppointment(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'draft_letter' => 'required',
            
            'date' => 'required',
            'appointment_date' => 'required',
            'project_number' => 'required',
            'company_name' => 'required',
            'employee_code' => 'required',
            'employee_name' => 'required',
            'designation_old' => 'required',
            'location_old' => 'required',
            'employee_status_old' => 'required',
            'designation_new' => 'required',
            'employee_status_new' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('appointments')->withErrors($v->errors());
        }
        $tembusan = Input::get('tembusan');
        $draft_letter = Input::get('draft_letter');
        $letter_number = Input::get('letter_number');
        $date = Input::get('date');
        $date= get_date_format_inggris($date);
        $appointment_date = Input::get('appointment_date');
        $appointment_date= get_date_format_inggris($appointment_date);
        $project_number = Input::get('project_number');
        $company_name = Input::get('company_name');
        $employee_code = Input::get('employee_code');
        $employee_name = Input::get('employee_name');
        $designation_old = Input::get('designation_old');
        $location_old = Input::get('location_old');
        $employee_status_old = Input::get('employee_status_old');
        $designation_new = Input::get('designation_new');
        $location_new = Input::get('location_new');
        $employee_status_new = Input::get('employee_status_new');

        
        //Penomoran id
        $last_number_id = Appointment::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $appointment = new Appointment();
        $appointment->id = $number_id;
        $appointment->tembusan = $tembusan;
        $appointment->draft_letter = $draft_letter;
        $appointment->letter_number = $letter_number;
        $appointment->date = $date;
        $appointment->appointment_date = $appointment_date;
        $appointment->project_number = $project_number;
        $appointment->company_name = $company_name;
        $appointment->employee_code = $employee_code;
        $appointment->employee_name = $employee_name;
        $appointment->designation_old = $designation_old;
        $appointment->location_old = $location_old;
        $appointment->employee_status_old = $employee_status_old;
        $appointment->designation_new = $designation_new;
        $appointment->location_new = $location_new;
        $appointment->employee_status_new = $employee_status_new;
        $appointment->status = 'draft';
        $appointment->save();
        
        $notification = new Notification();
        $notification->id_tag = $appointment->id;
        $notification->tag = 'appointments';
        $notification->title = 'Persetujuan Pengangkatan';
        $notification->description = 'Persetujuan Pengangkatan Pegawai '.$employee_code;
        $notification->show_date = date('Y-m-d');
        $notification->end_date = $appointment_date;
        $notification->route = 'approvals/appointments';
        $notification->save();

        if ($appointment!='') {
            return redirect('appointments')->with([
                'message' => 'Appointment Added Successfully'
            ]);

        } else {
            return redirect('appointments')->with([
                'message' => 'Appointment Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateAppointment  Function Start Here */
    public function updateAppointment(Request $request)
    {

        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'appointment_date' => 'required',
            'designation_new' => 'required',
            'employee_status_new' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('appointments')->withErrors($v->errors());
        }
        $appointment = Appointment::find($cmd);
        $appointment_date = Input::get('appointment_date');
        $appointment_date = get_date_format_inggris($appointment_date);
        $designation_new = Input::get('designation_new');
        $location_new = Input::get('location_new');
        $employee_status_new = Input::get('employee_status_new');

        if ($appointment) {
            $appointment->appointment_date = $appointment_date;
            $appointment->designation_new = $designation_new;
            $appointment->location_new = $location_new;
            $appointment->employee_status_new = $employee_status_new;
            $appointment->status = 'draft';
            $appointment->save();
            
            $notification = Notification::where('tag','=','appointments')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->end_date = $appointment_date;
                $notification->save();
            }

            return redirect('appointments')->with([
                'message' => 'Appointment Updated Successfully'
            ]);

        } else {
            return redirect('appointments')->with([
                'message' => 'Appointment Not Found',
                'message_important' => true
            ]);
        }
    }

    /* setAppointmentStatus  Function Start Here */
    public function setAppointmentStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('appointments')->withErrors($v->fails());
        }

        $appointments  = Appointment::find($cmd);
        if($appointments){

            $appointments->status=$request->status;
            $appointments->save();
            return redirect('appointments')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('appointments')->with([
                'message' => 'Appointment not found',
                'message_important'=>true
            ]);
        }

    }

    /* viewAppointment  Function Start Here */
    public function viewAppointment($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 47)->first();
        $appointments = Appointment::find($id);
        return view('admin.appointment.appointment-view', compact('appointments','permcheck'));
    }

    /* editAppointment  Function Start Here */
    public function editAppointment($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 47)->first();
        $emp_id = '';
        $proj_id = '';
        $appointments = Appointment::find($id);
        return view('admin.appointment.appointment-edit', compact('emp_id','proj_id','appointments','permcheck'));
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {

        $appointments = Appointment::find($id);
        $draft_letter = EmailTemplate::find($appointments->draft_letter);

        $letter_number = $appointments->letter_number;
        $date = get_date_format($appointments->date);
        $appointment_date = get_date_format($appointments->appointment_date);
        $project_number = $appointments->project_info->project_number;
        $company_name = $appointments->company_info->company;
        $employee_code = $appointments->employee_code;
        $employee_name = $appointments->employee_info->fname . " " . $appointments->employee_info->lname;
        $designation_old = $appointments->designation_old_info->designation_old;
        $location_old = $appointments->location_old;
        $employee_status_old = $appointments->employee_status_old;
        $designation_new = $appointments->designation_new_info->designation_new;
        $location_new = $appointments->location_new;
        $employee_status_new = $appointments->employee_status_new;

        
        $data = array(
            'letter_number' => $letter_number,
            'date' => $date,
            'appointment_date' => $appointment_date,
            'project_number' => $project_number,
            'company_name' => $company_name,
            'employee_code' => $employee_code,
            'employee_name' => $employee_name,
            'designation_old' => $designation_old,
            'location_old' => $location_old,
            'employee_status_old' => $employee_status_old,
            'designation_new' => $designation_new,
            'location_new' => $location_new,
            'employee_status_new' => $employee_status_new
        );

        $template = $draft_letter->message;

        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.appointment.pdf-appointments', compact('message','letter_number'));

        return $pdf->stream($letter_number.'.pdf');
    }
}
