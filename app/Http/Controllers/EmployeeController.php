<?php

namespace App\Http\Controllers;

use App\AppConfig;
use App\Attendance;
use App\Classes\permission;
use App\CareerPathEmployee;
use App\CareerPathEmployeeAssessment;
use App\CareerPathEmployeeCompetence;
use App\Company;
use App\Component;
use App\ContractRecipient;
use App\Calendar;
use App\Department;
use App\Designation;
use App\Jobs;
use App\Project;
use App\ProjectNeeds;
use App\EmailTemplate;
use App\Employee;
use App\EmployeeDependents;
use App\EmployeeBankAccount;
use App\EmployeeFiles;
use App\EmployeeCutiFiles;
use App\EmployeeRoles;
use App\EmployeeRolesPermission;
use App\EmploymentHistory;
use App\EmployeePaymentHistory;
use App\Menu;
use App\PayrollComponent;
use App\Notification;
use App\Schedule;
use App\ScheduleComponent;
use App\ScheduleEmployee;
use App\JobApplicants;
use App\Warning;
use App\Leave;
use App\LastEducation;
use App\Http\Requests;
use App\TaxRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;
use WkPdf;
use DateTime;
date_default_timezone_set(app_config('Timezone'));
class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    /* allEmployees  Function Start Here */
    public function allEmployees()
    {
        // $check_status=Employee::where('status','active')->where('role_id','!=','1')->get();
        // if ($check_status){
        //     foreach ($check_status as $cs){
        //         $leave_date=$cs->dol;
        //         if ($leave_date){
        //             if($leave_date!='0000-00-00'){
        //                 if (strtotime($leave_date) < strtotime('now')){
        //                     $cs->status='inactive';
        //                     $cs->save();
        //                 }
        //             }
        //         }

        //     }
        // }
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
        $employee_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
        $company = Company::all();
        $designation = Designation::all();
        $projects = Project::all();
        $employees = Employee::where('role_id','!=','1')->orderBy('id','asc')->get();
        return view('admin.employee.employees', compact('employees','projects','company','designation','permcheck','employee_permission'));
    }

    /* addEmployee  Function Start Here */
    public function addEmployee()
    {
        $comp_id = '';
        $dep_id = '';
        $proj_id = '';

        $last_education = LastEducation::all();
        $department = Department::all();
        $tax = TaxRules::where('status','active')->get();
        $role = EmployeeRoles::where('status','Active')->get();
        $dependents = EmployeeDependents::all();
        $company = Company::where('status','active')->get();
        $designation = Designation::all();
        $projects = Project::all();
        $career_path = CareerPathEmployee::all();
        $career_path_competence = CareerPathEmployeeCompetence::all();
        return view('admin.employee.add-employee', compact('career_path','career_path_competence','proj_id','dep_id','comp_id','department','tax','role','dependents','projects','company','designation','last_education'));
    }

    /* addEmployeePost  Function Start Here */
    public function addEmployeePost(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'fname' => 'required', 'phone' => 'required',  'designation' => 'required','company' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('employees/add')->withErrors($v->errors());
        }
        $company            = Input::get('company');
        $company            = Company::find($company);
        
        $month=date('m');
        $thn=date('y');
        $time = $month.$thn;
        $company_area = $company->company_code;
        $emmployee_code=Employee::select(DB::raw("SUBSTRING(employee_code,1,3) as 'AREA'"),DB::raw("SUBSTRING(employee_code,4,7) as 'TIME'"),DB::raw("SUBSTRING(employee_code,8,11) as 'kode'"))
        // ->having('AREA','=',$company_area)
        // ->having('TIME','=',$time)
        ->orderBy('kode', 'asc')
        ->first();
    
        if( $emmployee_code){
            $kode= $emmployee_code->kode+1;
            
        }else{
            $kode="1";
        }
        

        $kodeemployee = str_pad($company_area,3,'0',STR_PAD_LEFT).$time.''.str_pad($kode,4,'0',STR_PAD_LEFT);
        $employee_code= $kodeemployee;
        // echo $employee_code;
        /*
        if ($employee_code != '') {
            $exist = Employee::where('employee_code', '=', $employee_code)->first();
            if ($exist) {
                return redirect('employees/add')->with([
                    'message' => language_data('Employee Code Already Exist'),
                    'message_important' => true
                ]);
            }
        }*/

        // $username = Input::get('username');
        // if ($username != '') {
        //     $exist = Employee::where('user_name', '=', $username)->first();
        //     if ($exist) {
        //         return redirect('employees/add')->with([
        //             'message' => language_data('Username Already Exist'),
        //             'message_important' => true
        //         ]);
        //     }
        // }
        $email = Input::get('email');
        if ($email != '') {
            $exist = Employee::where('email', '=', $email)->first();
            if ($exist) {
                return redirect('employees/add')->with([
                    'message' => language_data('Email Already Exist'),
                    'message_important' => true
                ]);
            }
        }

        // $passowrd = Input::get('password');
        // $rpassowrd = Input::get('rpassword');

        // if ($passowrd != '') {
        //     if ($passowrd != $rpassowrd) {
        //         return redirect('employees/add')->with([
        //             'message' => language_data('Both Password Does not Match'),
        //             'message_important' => true
        //         ]);
        //     }
        // }
        //Penomoran id
        $last_number = Employee::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }

        //Personal Details
        $birth_place        = Input::get('birth_place');
        $dob                = Input::get('dob');

        $skck               = Input::get('skck');
        $no_ktp             = Input::get('no_ktp');
        $pre_address        = Input::get('pre_address');
        $pre_village        = Input::get('pre_village');
        $pre_district       = Input::get('pre_district');
        $pre_city           = Input::get('pre_city');
        $per_address        = Input::get('per_address');
        $rt                 = Input::get('rt');
        $rw                 = Input::get('rw');
        $per_village        = Input::get('per_village');
        $per_district       = Input::get('per_district');
        $per_city           = Input::get('per_city');
        $phone              = Input::get('phone');
        $religion           = Input::get('religion');
        $last_education     = Input::get('last_education');
        $university         = Input::get('university');
        $mother_name        = Input::get('mother_name');
        // $first_year         = Input::get('first_year');
        // $last_year          = Input::get('last_year');
        $status             = Input::get('status');

        //Dependents
        $marital_status     = Input::get('marital_status');
        $spouse             = Input::get('spouse');
        $child              = Input::get('child');
        $child_name         = Input::get('name_child');

        //Work
        $employee_type      = Input::get('employee_type');
        $project            = Input::get('project');
        $company            = Input::get('company');
        $employee_status    = Input::get('employee_status');
        $designation        = Input::get('designation');
        $department         = Input::get('department');
        $schedule           = Input::get('schedule');
        $date_of_join       = Input::get('doj');
        $date_of_join       = get_date_format_inggris($date_of_join);
        $location           = Input::get('location');

        //Bank & Insurance
        $bank_name          = Input::get('bank');
        $account_number     = Input::get('account_number');
        $account_name       = Input::get('account_name');
        $tax_status         = Input::get('tax_status');
        $npwp_status        = Input::get('npwp_status');
        $npwp_number        = Input::get('npwp_number');
        $pph_cut            = Input::get('pph_cut');
        $employee_bpjs      = Input::get('employment_bpjs_number');
        $employee_bpjs_cut  = Input::get('employment_bpjs_cut');
        $health_bpjs        = Input::get('health_bpjs_number');
        $health_bpjs_cut    = Input::get('health_bpjs_cut');
        $other_insurance_name        = Input::get('other_insurance_name');
        $other_insurance_number      = Input::get('other_insurance_number');
        $other_insurance_cut         = Input::get('other_insurance_cut');
        $effective_date     = Input::get('effective_date');
        $effective_date     = get_date_format_inggris($effective_date);
        // $cost_center        = Input::get('cost_center_user_function');
        // $user_function      = Input::get('user_function_name');
        // $kbo_user           = Input::get('kbo_user_function');
        // $kbo_name           = Input::get('kbo_name');
        // $faskes           = Input::get('faskes');


        //Documents
        $photo              = Input::file('photo');
        $diploma            = Input::file('diploma');
        $cv                 = Input::file('cv');
        $idcard             = Input::file('idcard');

        $destinationPath_file       = public_path() . '/assets/employee_doc/';

        $target_photo               = $destinationPath_file . basename($_FILES['photo']["name"]);
        $imageFileType_photo        = pathinfo($target_photo,PATHINFO_EXTENSION);  

        $target_diploma             = $destinationPath_file . basename($_FILES["diploma"]["name"]);
        $imageFileType_diploma      = pathinfo($target_diploma,PATHINFO_EXTENSION);   

        $target_cv                  = $destinationPath_file . basename($_FILES["cv"]["name"]);
        $imageFileType_cv           = pathinfo($target_cv,PATHINFO_EXTENSION);   

        $target_idcard              = $destinationPath_file . basename($_FILES["idcard"]["name"]);
        $imageFileType_idcard        = pathinfo($target_idcard,PATHINFO_EXTENSION);   


        //Table Employee Save
        $employee                   = new Employee();
        $employee->id               = $number;
        $employee->fname            = $request->fname;
        // $employee->lname            = $request->lname;
        $employee->user_name        = $employee_code;
        $employee->password         = bcrypt($dob);
        $employee->employee_code    = $employee_code;
        $employee->no_ktp           = $no_ktp;
        $employee->birth_place      = $birth_place;
        $dob                = get_date_format_inggris($dob);
        $aniv=new dateTime($dob);
        $aniv->modify('-1 year');
        $employee->dob              = $dob;
        $employee->skck             = $skck;        
        $employee->pre_address      = $pre_address;
        $employee->pre_village      = $pre_village;
        $employee->pre_district     = $pre_district;
        $employee->pre_city         = $pre_city;
        $employee->per_address      = $per_address.', RT : '.$rt.' / RW : '.$rw;
        $employee->per_village      = $per_village;
        $employee->per_district     = $per_district;
        $employee->per_city         = $per_city;
        $employee->phone            = $phone;
        $employee->email            = $email;
        $employee->religion         = $religion;
        $employee->gender           = $request->gender;
        $employee->last_education   = $last_education;
        $employee->university       = $university;
        $employee->mother_name      = $mother_name;
        // $employee->first_year       = $first_year;
        // $employee->last_year        = $last_year;
        $employee->status           = $status;

        $employee->marital_status   = $marital_status;
        $employee->spouse           = $spouse;
        $employee->child            = $child;

        $employee->employee_type    = $employee_type;
        $employee->project          = $project;
        $employee->company          = $company;
        $employee->employee_status  = $employee_status;
        $employee->designation      = $designation;
        $employee->department       = $department;
        $employee->schedule         = $schedule;
        $employee->doj              = $date_of_join;
        $employee->location         = $location;

        $employee->save();

        $emp_id = $employee->id;
        // for($i=0;$i<99 ; $i++){
        //     $aniv->modify('+1 year');
        //    $last_number_notification = Notification::max('id');
        //     if($last_number_notification==''){
        //         $number_notification = 1;
        //     } else {
        //         $number_notification = 1 + $last_number_notification;
        //     }
        //     $last_number_calendar = Calendar::max('id');
        //     if($last_number_calendar==''){
        //         $number_calendar = 1;
        //     } else {
        //         $number_calendar = 1 + $last_number_calendar;
        //     }
        //     $calendar = new Calendar();
        //     $calendar->start_date =$aniv->format('Y-m-d');
        //     $calendar->end_date =$aniv->format('Y-m-d');
        //     $calendar->starts_at = '00:00:00';
        //     $calendar->ends_at =  '12:00:00';
        //     $calendar->occasion = 'Employee Birthday';
        //     $calendar->description = 'Hari Ulang Tahun '.$request->fname.' '.$request->lname;
        //     $calendar->location = '-';
        //     $calendar->shared_with =  'all';
        //     $calendar->event =  'special';
        //     $calendar->tag = 'employee';
        //     $calendar->id_tag = $emp_id;
           
            // $calendar->save();
            // $tmpaniv= clone $aniv;
            // $tmpaniv->modify('- 7 day');
            // $notification = new Notification();
            // $notification->id = $number_notification;
            // $notification->id_tag = $calendar->id;
            // $notification->tag = 'calendar';
            // $notification->title = 'Hari Ulang Tahun '.$request->fname.' '.$request->lname;
            // $notification->description = 'Ulang Tahun Karyawan';
            // $notification->route = 'public/calendar';
            // $notification->show_date =  $tmpaniv->format('Y-m-d');
            // $notification->start_date =  $tmpaniv->format('Y-m-d');
            // $notification->end_date = $aniv->format('Y-m-d');
            // $notification->save();
            
      
           
        // }
        //Table Employee Files Save
        if (($imageFileType_photo == 'jpg' OR $imageFileType_photo == 'png' OR $imageFileType_photo == 'jpeg' OR $imageFileType_photo == 'pdf') && ($imageFileType_diploma == 'jpg' OR $imageFileType_diploma == 'png' OR $imageFileType_diploma == 'jpeg' OR $imageFileType_diploma == 'pdf') && ($imageFileType_cv == 'jpg' OR $imageFileType_cv == 'png' OR $imageFileType_cv == 'jpeg' OR $imageFileType_cv == 'pdf') && ($imageFileType_idcard == 'jpg' OR $imageFileType_idcard == 'png' OR $imageFileType_idcard == 'jpeg' OR $imageFileType_idcard == 'pdf')) {
            $destinationPath = public_path() . '/assets/employee_doc/';
            
            $photo_name      = $photo->getClientOriginalName();
            Input::file('photo')->move($destinationPath, $photo_name);

            $diploma_name      = $diploma->getClientOriginalName();
            Input::file('diploma')->move($destinationPath, $diploma_name);
            
            $cv_name      = $cv->getClientOriginalName();
            Input::file('cv')->move($destinationPath, $cv_name);
            
            $idcard_name      = $idcard->getClientOriginalName();
            Input::file('idcard')->move($destinationPath, $idcard_name);

            $employee_photo             = new EmployeeFiles();
            $employee_photo->emp_id     = $emp_id;
            $employee_photo->file_title = $photo_name;
            $employee_photo->file       = $photo_name;
            $employee_photo->save();

            $employee_diploma             = new EmployeeFiles();
            $employee_diploma->emp_id     = $emp_id;
            $employee_diploma->file_title = $diploma_name;
            $employee_diploma->file       = $diploma_name;
            $employee_diploma->save();
            
            $employee_cv             = new EmployeeFiles();
            $employee_cv->emp_id     = $emp_id;
            $employee_cv->file_title = $cv_name;
            $employee_cv->file       = $cv_name;
            $employee_cv->save();

            $employee_idcard             = new EmployeeFiles();
            $employee_idcard->emp_id     = $emp_id;
            $employee_idcard->file_title = $idcard_name;
            $employee_idcard->file       = $idcard_name;
            $employee_idcard->save();

        } else {
            return redirect('employees/add')->with([
                'message' => 'Upload an Image or PDF',
                'message_important' => true
            ]);
        }

        //Table CareerPathEmpolyeeAssessment Save
        $num_element_competences = 0;
        
        $career = Input::get('career');
        if($career){
            $career_path = CareerPathEmployee::where('designation','=',$career)->get();
            if($career_path){
                $career_path_competence = '';
                foreach($career_path as $cp){
                    $career_path_competence = CareerPathEmployeeCompetence::where('career_path','=',$cp->id)->get()->toArray();
                }
        
                $sql_data_competence = array();
                if($career_path_competence != '' && $career_path_competence != null){
                    while($num_element_competences<count($career_path_competence)){
                        $sql_data_competence[] = array(
                            'id'            => "",
                            'emp_id'        => $emp_id,
                            'competence'    => $career_path_competence[$num_element_competences]['id'],
                            'status'        => "Not Pass",
                            'proof_file'    => "",
                            'created_at'    => "",
                            'updated_at'    => ""
                        );
                        $num_element_competences++;
                    }
            
                    $career_assessment = CareerPathEmployeeAssessment::insert($sql_data_competence);
                }
            }
        }

        //Table ScheduleEmployee Save
        $num_elements_schedule  = 0;
        $schedule_component = ScheduleComponent::where('schedule','=',$schedule)->first();
        
        $list = array();
        $list_day = array();
        $month = date('m');
        $year = date('Y');
        
        for($d=1; $d<=31; $d++){
            $time=mktime(12, 0, 0, $month, $d, $year);          
            if (date('m', $time)==$month){ 
                $list[]=date('Y-m-d', $time);
                $list_day[]=get_date_day($time);
            }
        }

        $sql_data_schedule      = array();
        
        while($num_elements_schedule<count($list)) {
            $sql_data_schedule[] = array(
                'id'                => "",
                'emp_id'            => $emp_id,
                'date'              => $list[$num_elements_schedule],
                'day'               => $list_day[$num_elements_schedule],
                'starts_at'         => $schedule_component->OfficeInTime,
                'ends_at'           => $schedule_component->OfficeOutTime,
                'month'             => date('Y-m'),
                'updated_at'        => "",
                'created_at'        => ""
            );
            $num_elements_schedule++;
        }

        $schedule_employee     = ScheduleEmployee::insert($sql_data_schedule);

        //Table EmployeeDependents Save
        $num_elements           = 0;
        
        $sql_data               = array();
        if($child_name){
            while($num_elements<count($child_name)) {
                $sql_data[] = array(
                    'id'                => "",
                    'emp_id'            => $emp_id,
                    'child_name'        => $child_name[$num_elements],
                    'updated_at'        => "",
                    'created_at'        => ""
                );
                $num_elements++;
            }
    
            $dependents     = EmployeeDependents::insert($sql_data);
        }
        

        //Table EmployeeBankAccount
        $bank                               = new EmployeeBankAccount();
        $bank->emp_id                       = $emp_id;
        // $bank->faskes                       =$faskes;

        $bank->bank_name                    = $bank_name;
        $bank->account_name                 = $account_name;
        $bank->account_number               = $account_number;
        $bank->tax_status                   = $tax_status;
        $bank->npwp_status                  = $npwp_status;
        $bank->npwp_number                  = $npwp_number;
        $bank->pph_cut                      = $pph_cut;
        $bank->employee_bpjs                = $employee_bpjs;
        $bank->employee_bpjs_cut            = $employee_bpjs_cut;
        $bank->health_bpjs                  = $health_bpjs;
        $bank->health_bpjs_cut              = $health_bpjs_cut;
        $bank->other_insurance_name         = $other_insurance_name;
        $bank->other_insurance_number       = $other_insurance_number;
        $bank->other_insurance_number_cut   = $other_insurance_number_cut;
        $bank->effective_date               = $effective_date;
        // $bank->user_cost_center_function    = $cost_center;
        // $bank->user_function_name           = $user_function;
        // $bank->user_kbo_function            = $kbo_user;
        // $bank->kbo_name                     = $kbo_name;

        $bank->save();

        /*For Email Confirmation*/

        $conf = EmailTemplate::where('tplname', '=', 'Employee SignUp')->first();

        $estatus = $conf->status;

        if ($estatus == '1') {

            $sysEmail = app_config('Email');
            $sysCompany = app_config('AppName');
            $sysUrl = url('/');

            $template = $conf->message;
            $subject = $conf->subject;
            $employee_name=$request->fname;
            $data = array(
                'name' => $employee_name,
                'business_name' => $sysCompany,
                'from' => $sysEmail,
                'username' => $username,
                'email' => $email,
                'password' => $passowrd,
                'sys_url' => $sysUrl,
                'template' => $template
            );

            $message = _render($template, $data);
            $mail_subject = _render($subject, $data);
            $body = $message;

            /*Set Authentication*/

            $default_gt = app_config('Gateway');

            if ($default_gt == 'default') {

                $mail=new \PHPMailer();

                $mail->setFrom($sysEmail, $sysCompany);
                $mail->addAddress($email, $employee_name);     // Add a recipient
                $mail->isHTML(true);                                  // Set email format to HTML

                $mail->Subject = $mail_subject;
                $mail->Body    = $body;

                if(!$mail->send()) {
                    return redirect('employees/all')->with([
                        'message' => language_data('Employee Added Successfully But Email Not Send')
                    ]);
                } else {
                    return redirect('employees/all')->with([
                        'message' => language_data('Employee Added Successfully')
                    ]);
                }

            }
            else {
                $host = app_config('SMTPHostName');
                $smtp_username = app_config('SMTPUserName');
                $stmp_password = app_config('SMTPPassword');
                $port = app_config('SMTPPort');
                $secure = app_config('SMTPSecure');


                $mail=new \PHPMailer();

                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = $host;  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = $smtp_username;                 // SMTP username
                $mail->Password = $stmp_password;                           // SMTP password
                $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = $port;

                $mail->setFrom($sysEmail, $sysCompany);
                $mail->addAddress($email, $employee_name);     // Add a recipient
                $mail->isHTML(true);                                  // Set email format to HTML

                $mail->Subject = $mail_subject;
                $mail->Body    = $body;

                if(!$mail->send()) {
                    return redirect('employees/all')->with([
                        'message' => language_data('Employee Added Successfully But Email Not Send')
                    ]);
                } else {
                    return redirect('employees/all')->with([
                        'message' => language_data('Employee Added Successfully')
                    ]);
                }

            }
        }

        return redirect('employees/all')->with([
            'message' => language_data('Employee Added Successfully')
        ]);
        
    }
    


    /* viewEmployee  Function Start Here */
    public function viewEmployee($id)
    {
        $employee       = Employee::find($id);

        if ($employee) {
            $last_education = LastEducation::all();
            $designation    = Designation::all();
            $department     = Department::all();
            $company        = Company::all();
            $tax            = TaxRules::where('status','active')->get();
            $role           = EmployeeRoles::where('status','Active')->get();
            $sick           =Leave::where('emp_id','=',$id)->where('sick_name','!=',null)->where('status','approved')->get();
            $dependents     = EmployeeDependents::where('emp_id', '=', $id)->get();
            $bank_accounts  = EmployeeBankAccount::where('emp_id', '=', $id)->first();
            $employee_doc   = EmployeeFiles::where('emp_id', '=', $id)->get();
            $employee_leave = EmployeeCutiFiles::where('emp_id', '=', $id)->get();
            $warning        = Warning::where('employee_name', '=', $id)->where('status', '=', 'accepted')->get();
            $employment     = EmploymentHistory::where('emp_id','=',$id)->get();
            $contract       = ContractRecipient::where('recipients','=',$id)->get();
            $salary         = EmployeePaymentHistory::where('emp_id','=',$id)->get();
            $cv             = JobApplicants::where('email','=',$employee->email)->first();

            $role_id = \Auth::user()->role_id;
            $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
            if(sizeof($bank_accounts)<1){
                $bank_accounts = array();
                $bank_accounts=(object)$bank_accounts;
                $bank_accounts->bank_name="";
                $bank_accounts->account_number="";
                $bank_accounts->account_name="";
                $bank_accounts->tax_status="";
                $bank_accounts->npwp_status="";
                $bank_accounts->npwp_number="";
                $bank_accounts->pph_cut="";
                $bank_accounts->employee_bpjs_cut="";
                $bank_accounts->health_bpjs_cut="";
                $bank_accounts->other_insurance_cut="";
                $bank_accounts->employee_bpjs="";
                $bank_accounts->health_bpjs="";
                $bank_accounts->other_insurance_name="";
                $bank_accounts->other_insurance_number="";
                $bank_accounts->effective_date="";
                // $bank_accounts->user_cost_center_function="";
                // $bank_accounts->user_function_name="";
                // $bank_accounts->user_kbo_function="";
                // $bank_accounts->kbo_name="";
            }
            // echo "<pre>";
            // print_r($bank_accounts); exit;
            return view('admin.employee.view-employee', compact('contract','employment','warning','employee','designation','department','company','dependents','bank_accounts','employee_doc','tax','role','salary','sick','cv','last_education','permcheck','employee_leave','role_id'));
        } else {
            return redirect('employees/all')->with([
                'message' => language_data('Employee Not Found'),
                'message_important' => true
            ]);
        }
    }


    /* postEmployeePersonalInfo  Function Start Here */
    public function postEmployeePersonalInfo(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'fname' => 'required', 'employee_code' => 'required', 'email' => 'required', 'gender' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('employees/view/' . $cmd)->withErrors($v->errors());
        }

        $employee = Employee::find($cmd);

        $employee_code = Input::get('employee_code');
        $exist_emp_code = $employee->employee_code;
        if ($employee_code != '' AND $employee_code != $exist_emp_code) {
            $exist = Employee::where('employee_code', '=', $employee_code)->first();
            if ($exist) {
                return redirect('employees/view/' . $cmd)->with([
                    'message' => language_data('Employee Code Already Exist'),
                    'message_important' => true
                ]);
            }
        }

        $email = Input::get('email');
        $exist_email = $employee->email;
        if ($email != '' AND $email != $exist_email) {
            $exist = Employee::where('email', '=', $email)->first();
            if ($exist) {
                return redirect('employees/view/' . $cmd)->with([
                    'message' => language_data('Email Already Exist'),
                    'message_important' => true
                ]);
            }
        }

        $passowrd           = Input::get('password');
        $birth_place        = Input::get('birth_place');
        // $kelompok           = Input::get('kelompok');

        $dob                = Input::get('dob');
        $dob                = get_date_format_inggris($dob);
        $aniv=new dateTime($dob);
        $aniv->modify('-1 year');

        $skck               = Input::get('skck');
        $no_ktp             = Input::get('no_ktp');
        $pre_address        = Input::get('pre_address');
        $pre_village        = Input::get('pre_village');
        $pre_district       = Input::get('pre_district');
        $pre_city           = Input::get('pre_city');
        $per_address        = Input::get('per_address');
        $per_village        = Input::get('per_village');
        $per_district       = Input::get('per_district');
        $per_city           = Input::get('per_city');
        $phone              = Input::get('phone');
        $religion           = Input::get('religion');
        $last_education     = Input::get('last_education');
        $university         = Input::get('university');
        // $first_year         = Input::get('first_year');
        // $last_year          = Input::get('last_year');
        $status             = Input::get('status');

        // $field_1             = Input::get('field_1');
        // $field_2             = Input::get('field_2');
        // $field_3            = Input::get('field_3');
        // $field_4            = Input::get('field_4');
        // $field_5             = Input::get('field_5');

        if($employee->dob !=$dob){
            $sama=false;
        }
        else{
            $sama=true;
        }
        // $employee->field_1            = $field_1;
        // $employee->field_2            = $field_2;
        // $employee->field_3            = $field_3;
        // $employee->field_4            = $field_4;
        // $employee->field_5            = $field_5;

        $employee->fname            = $request->fname;
        // $employee->kelompok         = $kelompok;
        $employee->no_ktp           = $no_ktp;
        // $employee->lname            = $request->lname;
        $employee->employee_code    = $employee_code;
        $employee->birth_place      = $birth_place;
        $employee->dob              = $dob;
        $employee->skck             = $skck;        
        $employee->pre_address      = $pre_address;
        $employee->pre_village      = $pre_village;
        $employee->pre_district     = $pre_district;
        $employee->pre_city         = $pre_city;
        $employee->per_address      = $per_address;
        $employee->per_village      = $per_village;
        $employee->per_district     = $per_district;
        $employee->per_city         = $per_city;
        $employee->phone            = $phone;
        $employee->email            = $email;
        $employee->religion         = $religion;
        $employee->gender           = $request->gender;
        $employee->last_education   = $last_education;
        $employee->university       = $university;
        // $employee->first_year       = $first_year;
        // $employee->last_year        = $last_year;
        $employee->status           = $status;

        $employee->save();
    //     if(!$sama){
    //     //     $ada=Calendar::where('tag','=','employee')->where('id_tag','=',$cmd)->get();
    //     //     if(sizeof($ada)>0){
    //     //         $deletedata= Calendar::where('tag','=','employee')->where('id_tag','=',$cmd)->delete();
    //     // // for($i=0;$i<99 ; $i++){
    //     // //     $aniv->modify('+1 year');
    //     // //    $last_number_notification = Notification::max('id');
    //     // //     if($last_number_notification==''){
    //     // //         $number_notification = 1;
    //     // //     } else {
    //     // //         $number_notification = 1 + $last_number_notification;
    //     // //     }
    //     // //     $last_number_calendar = Calendar::max('id');
    //     // //     if($last_number_calendar==''){
    //     // //         $number_calendar = 1;
    //     // //     } else {
    //     // //         $number_calendar = 1 + $last_number_calendar;
    //     // //     }
    //     // //     $calendar = new Calendar();
    //     // //     $calendar->start_date =$aniv->format('Y-m-d');
    //     // //     $calendar->end_date =$aniv->format('Y-m-d');
    //     // //     $calendar->starts_at = '00:00:00';
    //     // //     $calendar->ends_at =  '12:00:00';
    //     // //     $calendar->occasion = 'Employee Birthday';
    //     // //     $calendar->description = 'Hari Ulang Tahun '.$request->fname;
    //     // //     $calendar->location = '-';
    //     // //     $calendar->shared_with =  'all';
    //     // //     $calendar->event =  'special';
    //     // //     $calendar->tag = 'employee';
    //     // //     $calendar->id_tag = $cmd;
           
    //     // //     $calendar->save();
            
    //     // // }
           
    //     // }
    //     // else{
    //     //     for($i=0;$i<99 ; $i++){
    //     //         $aniv->modify('+1 year');
    //     //        $last_number_notification = Notification::max('id');
    //     //         if($last_number_notification==''){
    //     //             $number_notification = 1;
    //     //         } else {
    //     //             $number_notification = 1 + $last_number_notification;
    //     //         }
    //     //         $last_number_calendar = Calendar::max('id');
    //     //         if($last_number_calendar==''){
    //     //             $number_calendar = 1;
    //     //         } else {
    //     //             $number_calendar = 1 + $last_number_calendar;
    //     //         }
    //     //         $calendar = new Calendar();
    //     //         $calendar->start_date =$aniv->format('Y-m-d');
    //     //         $calendar->end_date =$aniv->format('Y-m-d');
    //     //         $calendar->starts_at = '00:00:00';
    //     //         $calendar->ends_at =  '12:00:00';
    //     //         $calendar->occasion = 'Employee Birthday';
    //     //         $calendar->description = 'Hari Ulang Tahun '.$request->fname;
    //     //         $calendar->location = '-';
    //     //         $calendar->shared_with =  'all';
    //     //         $calendar->event =  'special';
    //     //         $calendar->tag = 'employee';
    //     //         $calendar->id_tag = $cmd;
               
    //     //         $calendar->save();
                
    //     //     }
    //     // }
    // }
        return redirect('employees/all')->with([
            'message' => language_data('Employee Updated Successfully')
        ]);


    }

    /* updateEmployeeAvatar  Function Start Here */
    public function updateEmployeeAvatar(Request $request)
    {
        $cmd = Input::get('cmd');
        // $v = \Validator::make($request->all(), [
        //     'image' => 'required', 'cmd' => 'required'
        // ]);

        // if ($v->fails()) {
        //     return redirect('employees/view/' . $cmd)->withErrors($v->errors());
        // }
        $role_id = \Auth::user()->role_id;
        if($role_id==1){
            $role_update = Input::get('role_update');
        }

        $image = Input::file('image');

        $employee = Employee::find($cmd);

        if ($employee) {
            if ($image != '') {
                $destinationPath = public_path() . '/assets/employee_pic/';
                $image_name = $image->getClientOriginalName();
                Input::file('image')->move($destinationPath, $image_name);
                $employee->avatar = $image_name;
            } 
            if($role_id==1){
                $employee->role_id = $role_update;
            }
            $employee->save();
            return redirect('employees/view/' . $cmd)->with([
                'message' => language_data('Avatar Changed Successfully')
            ]);
        } else {
            return redirect('employees/all')->with([
                'message' => language_data('Employee Not Found'),
                'message_important' => true
            ]);
        }
    }

    /* addBankInfo  Function Start Here */
    public function addBankInfo(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'bank'=>'required','account_name'=>'required','account_number'=>'required'
        ]);

        if($v->fails()){
            return redirect('employees/view/'.$cmd)->withErrors($v->errors());
        }

        $bank_name          = Input::get('bank');
        // $faskes           = Input::get('faskes');
        // $kelompok           = Input::get('kelompok');

        $account_number     = Input::get('account_number');
        $account_name       = Input::get('account_name');
        $tax_status         = Input::get('tax_status');
        $npwp_status        = Input::get('npwp_status');
        $npwp_number        = Input::get('npwp_number');
        $pph_cut            = Input::get('pph_cut');
        $employee_bpjs      = Input::get('employment_bpjs_number');
        $employee_bpjs_cut  = Input::get('employment_bpjs_cut');
        $health_bpjs        = Input::get('health_bpjs_number');
        $health_bpjs_cut    = Input::get('health_bpjs_cut');
        $other_insurance_name        = Input::get('other_insurance_name');
        $other_insurance_number      = Input::get('other_insurance_number');
        $other_insurance_cut         = Input::get('other_insurance_cut');
        $effective_date     = Input::get('effective_date');
        $effective_date     = get_date_format_inggris($effective_date);
        // $other_insurance_name2        = Input::get('other_insurance_name2');
        // $other_insurance_number2      = Input::get('other_insurance_number2');
        // $effective_date2     = Input::get('effective_date2');
        // $effective_date2     = get_date_format_inggris($effective_date2);
        // $other_insurance_name3        = Input::get('other_insurance_name3');
        // $other_insurance_number3      = Input::get('other_insurance_number3');
        // $effective_date3     = Input::get('effective_date3');
        // $effective_date3     = get_date_format_inggris($effective_date3);
        // $cost_center        = Input::get('cost_center_user_function');
        // $user_function      = Input::get('user_function_name');
        // $kbo_user           = Input::get('kbo_user_function');
        // $kbo_name           = Input::get('kbo_name');

        $employee_bank = EmployeeBankAccount::where('emp_id','=', $cmd)->first();

        if($employee_bank){
            $employee_bank->bank_name                    = $bank_name;
            // $employee_bank->faskes                         = $faskes;
            // $employee_bank->kelompok                    = $kelompok;

            $employee_bank->account_name                 = $account_name;
            $employee_bank->account_number               = $account_number;
            $employee_bank->tax_status                   = $tax_status;
            $employee_bank->npwp_status                  = $npwp_status;
            $employee_bank->npwp_number                  = $npwp_number;
            $employee_bank->pph_cut                      = $pph_cut;
            $employee_bank->employee_bpjs                = $employee_bpjs;
            $employee_bank->employee_bpjs_cut            = $employee_bpjs_cut;
            $employee_bank->health_bpjs                  = $health_bpjs;
            $employee_bank->health_bpjs_cut              = $health_bpjs_cut;
            $employee_bank->other_insurance_name         = $other_insurance_name;
            $employee_bank->other_insurance_number       = $other_insurance_number;
            $employee_bank->other_insurance_cut          = $other_insurance_cut;
            $employee_bank->effective_date               = $effective_date;
            // $employee_bank->other_insurance_name2         = $other_insurance_name2;
            // $employee_bank->other_insurance_number2       = $other_insurance_number2;
            // $employee_bank->effective_date2               = $effective_date2;
            // $employee_bank->other_insurance_name3         = $other_insurance_name3;
            // $employee_bank->other_insurance_number3       = $other_insurance_number3;
            // $employee_bank->effective_date3               = $effective_date3;
            // $employee_bank->user_cost_center_function    = $cost_center;
            // $employee_bank->user_function_name           = $user_function;
            // $employee_bank->user_kbo_function            = $kbo_user;
            // $employee_bank->kbo_name                     = $kbo_name;
            $employee_bank->save();

            return redirect('employees/view/'.$cmd)->with([
                'message'=>language_data('Bank Account Added Successfully')
            ]);
        } else{
            return redirect('employees/view/'.$cmd)->with([
                'message'=>language_data('Bank Account Already Exist'),
                'message_important'=>true
            ]);
        }
    }

    /* addDependents  Function Start Here */
    public function addDependents(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            
        ]);

        if($v->fails()){
            return redirect('employees/view/'.$cmd)->withErrors($v->errors());
        }

        $marital_status     = Input::get('marital_status');
        $spouse             = Input::get('spouse');
        // $child              = Input::get('child');
        $child_name         = Input::get('name_child');

        $employee = Employee::find($cmd);

        $employee->marital_status   = $marital_status;
        $employee->spouse           = $spouse;

        //Table EmployeeDependents Save
        if($marital_status=='single'){
            $dependents     = EmployeeDependents::where('emp_id', '=', $cmd)->get();
            if ($dependents) {
                EmployeeDependents::where('emp_id', '=', $cmd)->delete();
            }
        } else {
            $num_elements           = 0;
            
            $sql_data               = array();
            while($num_elements<count($child_name)) {
                $sql_data[] = array(
                    'emp_id'            => $cmd,
                    'child_name'        => $child_name[$num_elements],
                );
                $num_elements++;
            }
            $dependents     = EmployeeDependents::insert($sql_data);
        }
        
        $dependents       = EmployeeDependents::where('emp_id', '=', $cmd)->get();
        $employee->child  = count($dependents);
        $employee->save();

        return redirect('employees/view/'.$cmd)->with([
            'message'=>'Dependents Added Successfully'
        ]);

    }

    /* addDocument  Function Start Here */
    public function addCutiDocument(Request $request)
    {
        $cmd = Input::get('cmd');

        $v = \Validator::make($request->all(), [
            'date_from' => 'required','file' => 'required', 'cmd' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('employees/view/' . $cmd)->withErrors($v->errors());
        }

        $date_from=Input::get('date_from');
        $date_from=get_date_format_inggris($date_from);
        $date_to=Input::get('date_to');
        $date_to=get_date_format_inggris($date_to);
        $reason=Input::get('reason');
        $file = Input::file('file');
        $destinationPath_file = public_path() . '/assets/employee_doc/';
        $target_file = $destinationPath_file . basename($_FILES["file"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        $employee = Employee::find($cmd);

        if ($employee) {
            if ($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'jpeg' || $imageFileType == 'pdf') {
                $destinationPath = public_path() . '/assets/employee_doc/';
                $file_name = $file->getClientOriginalName();
                // echo $file_name;exit;
                Input::file('file')->move($destinationPath, $file_name);

                $employee_doc=new EmployeeCutiFiles();
                $employee_doc->emp_id=$employee->id;
                $employee_doc->date_from=$date_from;
                $employee_doc->date_to=$date_to;
                $employee_doc->reason=$reason;
                $employee_doc->file=$file_name;
                $employee_doc->save();

                return redirect('employees/view/' . $cmd)->with([
                    'message' => language_data('Document Uploaded Successfully')
                ]);

            } else {
                return redirect('employees/view/' . $cmd)->with([
                    'message' => 'Upload an Image or PDF',
                    'message_important' => true
                ]);
            }
        } else {
            return redirect('employees/all')->with([
                'message' => language_data('Employee Not Found'),
                'message_important' => true
            ]);
        }
    }

    /* downloadEmployeeCutiDocument  Function Start Here */
    public function downloadEmployeeCutiDocument($id)
    {
        $file = EmployeeCutiFiles::find($id)->file;
        return response()->download(public_path('assets/employee_doc/' . $file));
    }


    /* addDocument  Function Start Here */
    public function addDocument(Request $request)
    {
        $cmd = Input::get('cmd');

        $v = \Validator::make($request->all(), [
            'document_name' => 'required','file' => 'required', 'cmd' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('employees/view/' . $cmd)->withErrors($v->errors());
        }

        $document_name=Input::get('document_name');
        $file = Input::file('file');
        $destinationPath_file = public_path() . '/assets/employee_doc/';
        $target_file = $destinationPath_file . basename($_FILES["file"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        $exist=EmployeeFiles::where('file_title',$document_name)->where('emp_id',$cmd)->first();

        if($exist){
            return redirect('employees/view/' . $cmd)->with([
                'message' => language_data('This Document Already Exist'),
                'message_important' => true
            ]);
        }

        $employee = Employee::find($cmd);

        if ($employee) {
            if ($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'jpeg' || $imageFileType == 'pdf') {
                $destinationPath = public_path() . '/assets/employee_doc/';
                $file_name = $file->getClientOriginalName();
                // echo $file_name;exit;
                Input::file('file')->move($destinationPath, $file_name);

                $employee_doc=new EmployeeFiles();
                $employee_doc->emp_id=$employee->id;
                $employee_doc->file_title=$document_name;
                $employee_doc->file=$file_name;
                $employee_doc->save();

                return redirect('employees/view/' . $cmd)->with([
                    'message' => language_data('Document Uploaded Successfully')
                ]);

            } else {
                return redirect('employees/view/' . $cmd)->with([
                    'message' => 'Upload an Image or PDF',
                    'message_important' => true
                ]);
            }
        } else {
            return redirect('employees/all')->with([
                'message' => language_data('Employee Not Found'),
                'message_important' => true
            ]);
        }
    }

    /* downloadEmployeeDocument  Function Start Here */
    public function downloadEmployeeDocument($id)
    {
        $file = EmployeeFiles::find($id)->file;
        return response()->download(public_path('assets/employee_doc/' . $file));
    }


    /* deleteEmployeeDoc  Function Start Here */
    public function deleteEmployeeDoc($id)
    {
        $emp_doc=EmployeeFiles::find($id);
        $file=$emp_doc->file;
        $cmd=$emp_doc->emp_id;
        if($emp_doc){
            \File::delete(public_path('assets/employee_doc/' . $file));
            $emp_doc->delete();

            return redirect('employees/view/'.$cmd)->with([
                'message'=>language_data('Document Deleted Successfully')
            ]);

        }else{
            return redirect('employees/view/'.$cmd)->with([
                'message'=>language_data('Document Not Found'),
                'message_important'=>true
            ]);
        }
    }

    /* deleteEmployeeDependent  Function Start Here */
    public function deleteEmployeeDependent($id)
    {
        $emp_dep=EmployeeDependents::find($id);
        $emp_id = $emp_dep->emp_id;
        if($emp_dep){
            $emp_dep->delete();

            return redirect('employees/view/'.$emp_id)->with([
                'message'=>language_data('Dependent Deleted Successfully')
            ]);

        }else{
            return redirect('employees/view/'.$emp_id)->with([
                'message'=>language_data('Dependent Not Found'),
                'message_important'=>true
            ]);
        }
    }


    /*Version 1.5*/

    /* employeeRoles  Function Start Here */
    public function employeeRoles()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 26)->first();
        $emp_roles=EmployeeRoles::all();
        return view('admin.employee.employee-roles',compact('emp_roles','permcheck'));
    }

    /* addEmployeeRoles  Function Start Here */
    public function addEmployeeRoles(Request $request){

        $v=\Validator::make($request->all(),[
            'role_name'=>'required','status'=>'required'
        ]);

        if ($v->fails()){
            return redirect('employees/roles')->withErrors($v->errors());
        }

        $emp_roles=new EmployeeRoles();
        $emp_roles->role_name=$request->role_name;
        $emp_roles->status=$request->status;
        $emp_roles->save();

        $role_id = $emp_roles->id;
        
        $menu = Menu::all();

        foreach($menu as $m){
            $menus_permission = new EmployeeRolesPermission();
            $menus_permission->role_id = $role_id;
            $menus_permission->perm_id = $m->id;
            $menus_permission->C = 1;
            $menus_permission->R = 1;
            $menus_permission->U = 1;
            $menus_permission->D = 1;
            $menus_permission->save();
        }

        return redirect('employees/roles')->with([
            'message'=> language_data('Employee Role added successfully')
        ]);

    }

    /* updateEmployeeRoles  Function Start Here */
    public function updateEmployeeRoles(Request $request){

        $cmd=Input::get('cmd');

        $v=\Validator::make($request->all(),[
            'role_name'=>'required','status'=>'required'
        ]);

        if ($v->fails()){
            return redirect('employees/roles')->withErrors($v->errors());
        }

        $emp_roles=EmployeeRoles::find($cmd);

        if ($emp_roles){
            $emp_roles->role_name=$request->role_name;
            $emp_roles->status=$request->status;
            $emp_roles->save();

            return redirect('employees/roles')->with([
                'message'=> language_data('Employee Role updated successfully')
            ]);
        }else{

            return redirect('employees/roles')->with([
                'message'=> language_data('Employee Role info not found'),
                'message_important'=>true
            ]);
        }

    }

    /* setEmployeeRoles  Function Start Here */
    public function setEmployeeRoles($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 26)->first();
        $emp_roles=EmployeeRoles::find($id);
        $menu = Menu::Where('status','=','y')->Where('menu_type','=',0)->orderBy('indexs','ASC')->get();
        return view('admin.employee.set-employee-roles',compact('emp_roles','menu','permcheck'));
    }

    /* updateEmployeeSetRoles  Function Start Here */
    public function updateEmployeeSetRoles(Request $request)
    {

        $role_id = Input::get('role_id');
        $permission = Input::get('permission');
        $permission_sub_menu = Input::get('permission_sub_menu');
        // echo $role_id; echo "<br>"; echo "<pre>";
        // print_r($permission); echo "<br>";
        // print_r($permission_sub_menu); echo "<br>"; exit;
        foreach($permission as $p){
            $create_menu = Input::get('create_menu_'.$p);
            $read_menu = Input::get('read_menu_'.$p);
            $update_menu = Input::get('update_menu_'.$p);
            $delete_menu = Input::get('delete_menu_'.$p);

            $emp_r_perm = EmployeeRolesPermission::find($p);
            $emp_r_perm->C = $create_menu;
            $emp_r_perm->R = $read_menu;
            $emp_r_perm->U = $update_menu;
            $emp_r_perm->D = $delete_menu;
            $emp_r_perm->save();
        }
        foreach($permission_sub_menu as $ps){
            $create_submenu = Input::get('create_submenu_'.$ps);
            $read_submenu = Input::get('read_submenu_'.$ps);
            $update_submenu = Input::get('update_submenu_'.$ps);
            $delete_submenu = Input::get('delete_submenu_'.$ps);

            $emp_r_perm_sub = EmployeeRolesPermission::find($ps);
            $emp_r_perm_sub->C = $create_submenu;
            $emp_r_perm_sub->R = $read_submenu;
            $emp_r_perm_sub->U = $update_submenu;
            $emp_r_perm_sub->D = $delete_submenu;
            $emp_r_perm_sub->save();
        }

        return redirect('employees/set-roles/'.$role_id)->with([
            'message'=> language_data('Permission Updated')
        ]);


    }

    /* deleteEmployeeRoles  Function Start Here */
    public function deleteEmployeeRoles($id)
    {

        $emp_role=EmployeeRoles::find($id);

        if ($emp_role){

            $emp_check=Employee::where('role_id',$id)->where('role_id','!=','1')->first();

            if ($emp_check){
                return redirect('employees/roles')->with([
                    'message'=> language_data('An Employee contain this role'),
                    'message_important'=>true
                ]);
            }


            EmployeeRolesPermission::where('role_id',$id)->delete();
            $emp_role->delete();

            return redirect('employees/roles')->with([
                'message'=> language_data('Employee role deleted successfully')
            ]);

        }else{
            return redirect('employees/roles')->with([
                'message'=> language_data('Employee Role info not found'),
                'message_important'=>true
            ]);
        }



    }

    /* download excel function start here */
    public function downloadExcel(Request $request)
    {
        /*$data = Employee::get()->toArray();
		return Excel::create('employees', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
        })->download($type);*/
        $file = EmployeeFiles::all();
        return response()->download(public_path('assets/template_xls/employees.xls'));
    }

    /* downloadEmployeeExcel function start here */
   /* downloadEmployeeExcel function start here */
   public function downloadEmployeeExcel(Request $request)
   {
       $data = Input::get('variable');
       $banks= Input::get('banks');
       if(isset($banks)){
        $data= array_merge($data,$banks);
       }
       array_push($data,'sys_employee.id');
       array_push($data,'project');
       $project = Input::get('project');
       $company = Input::get('company');
    //    echo "<pre>";
    //    print_r($data);
    //    exit;

       if($project!=0){
           $query = Employee::select($data)->where('project','=', $project)->where('role_id','!=','1')->leftJoin('sys_employee_bank_accounts as banks', 'sys_employee.id', '=', 'banks.emp_id')->distinct()->groupBy('sys_employee.id')->get()->toArray();
       } else if($company!=0){
           $query = Employee::select($data)->where('company','=', $company)->where('role_id','!=','1')->leftJoin('sys_employee_bank_accounts as banks', 'sys_employee.id', '=', 'banks.emp_id')->distinct()->groupBy('sys_employee.id')->get()->toArray();
       } else{
           $query = Employee::select($data)->where('role_id','!=','1')->leftJoin('sys_employee_bank_accounts as banks', 'sys_employee.id', '=', 'banks.emp_id')->distinct()->groupBy('sys_employee.id')->get()->toArray();
       }
           $ct=0;
       foreach($query as $z){
            foreach($data as $d){
                if (strpos($d, 'banks.') !== false){
                    $split = explode(".", $d);
                    $d =  $split[1];
            
                } else if (strpos($d, 'sys_employee.') !== false){
                    $split = explode(".", $d);
                    $d =  $split[1];
            
                }
                if($z[$d] == null || $z[$d] == '-'){
                    if($d!= 'lname'){
                        $query[$ct][$d]="0";
                    }
                }
            }
            $ct++;
       }
    //    echo "<pre>";
    //    print_r($query);
    //    exit;
       $data2 = array();
       $ctr = 0;
       $no = 1;
       foreach($query as $q){

            $data2[$ctr]['No']                 = $no;
            $no++;
            if(isset($q['employee_code'])){
                $data2[$ctr]['NIK']                 = $q['employee_code'];
            }
            if(isset($q['doj'])){
                if($q['doj']!='-'){
                    $data2[$ctr]['Tanggal Masuk']    = date('d/m/Y', strtotime($q['doj']));
                } else {
                    $data2[$ctr]['Tanggal Masuk']    = $q['doj'];
                }
                
            }
            if(isset($q['dol'])){
                if($q['dol']!='-'){
                    $data2[$ctr]['Tanggal Berakhir']    = date('d/m/Y', strtotime($q['dol']));
                } else {
                    $data2[$ctr]['Tanggal Berakhir']    = $q['dol'];
                }
            }
            if(isset($q['account_number'])){
                $data2[$ctr]['No Rekening']     = $q['account_number'];
                
            }
            if(isset($q['fname'])){
                $data2[$ctr]['Nama Karyawan']       = $q['fname'];
                
            }
            if(isset($q['gender'])){
                if($q['gender'] ==  'male'){
                    $data2[$ctr]['Jenis Kelamin']       = 'Laki-laki';
                } else if($q['gender'] ==  'female'){
                    $data2[$ctr]['Jenis Kelamin']       = 'Perempuan';
                }
            }
            if(isset($q['company'])){
                $le=Company::find($q['company']);
                if($le){
                $data2[$ctr]['Perusahaan'] = $le->company;}
                else{  $data2[$ctr]['Perusahaan'] = "-";}
                
            }
            if(isset($q['designation'])){
                $le=Designation::find($q['designation']);
                if($le){
                $data2[$ctr]['Jabatan'] = $le->designation;}
                else{  $data2[$ctr]['Jabatan'] = "-";}
                
            }
            if(isset($q['location'])){
                $data2[$ctr]['Lokasi']              = $q['location'];
                
            }
            if(isset($q['no_ktp'])){
                $data2[$ctr]['No. KTP']             = $q['no_ktp'];
                
            }
            if(isset($q['birth_place'])){
                $data2[$ctr]['Tempat Lahir']        = $q['birth_place'];
                
            }
            if(isset($q['dob'])){
                $data2[$ctr]['Tanggal Lahir']       = date('d/m/Y', strtotime($q['dob']));
                
            }
            if(isset($q['per_address'])){
                $data2[$ctr]['Alamat (KTP)']         = $q['per_address'];
                
            }
            if(isset($q['per_village'])){
                $data2[$ctr]['Kelurahan (KTP)']      = $q['per_village'];
                
            }
            if(isset($q['per_district'])){
                $data2[$ctr]['Kecamatan (KTP)']      = $q['per_district'];
                
            }
            if(isset($q['per_city'])){
                $data2[$ctr]['Kota (KTP)']           = $q['per_city'];
                
            }
            if(isset($q['pre_address'])){
                $data2[$ctr]['Alamat Domisili']     = $q['pre_address'];
                
            }
            if(isset($q['last_education'])){
                $le=LastEducation::find($q['last_education']);
                if($le){
                $data2[$ctr]['Pendidikan Terakhir'] = $le->education;}
                else{  $data2[$ctr]['Pendidikan Terakhir'] = "-";}
                
            }
            if(isset($q['religion'])){
                $data2[$ctr]['Agama']               = $q['religion'];
                
            }
            if(isset($q['mother_name'])){
                $data2[$ctr]['Nama Ibu Kandung']               = $q['mother_name'];
                
            }
            if(isset($q['marital_status'])){
                $marital_status="";
                if($q['marital_status']=='single'){
                    $marital_status="Belum Menikah";
                }
                if($q['marital_status']=='married'){
                    $marital_status="Menikah";
                }
                if($q['marital_status']=='divorced'){
                    $marital_status="Cerai";
                }
                $data2[$ctr]['Status Perkawinan']   = $marital_status;
                
            }
            if(isset($q['tax_status'])){
                $data2[$ctr]['Status Pajak']     = $q['tax_status'];
                
            }
            if(isset($q['phone'])){
                $data2[$ctr]['Nomor Telepon']       = $q['phone'];
                
            }
            if(isset($q['email'])){
                $data2[$ctr]['Email']               = $q['email'];
            
            }
            if(isset($q['npwp_status'])){
                $data2[$ctr]['Status NPWP']     = $q['npwp_status'];
                
            }
            if(isset($q['npwp_number'])){
                $data2[$ctr]['Nomor NPWP']     = $q['npwp_number'];
                
            }
            if(isset($q['employee_bpjs'])){
                $data2[$ctr]['BPJS Ketenagakerjaan']     = $q['employee_bpjs'];
                
            }
            if(isset($q['health_bpjs'])){
                $data2[$ctr]['BPJS Kesehatan']     = $q['health_bpjs'];
                
            }
            if(isset($q['other_insurance_number'])){
                $data2[$ctr]['Nomor Asuransi Lain']     = $q['other_insurance_number'];
                
            }
            $component = Component::lists('component')->toArray();
            foreach($component as $comp){
                $data2[$ctr][$comp] = 0;
            }
            if($q['payment_type'] != 0){
                $payrollcomponent = PayrollComponent::where('id_payroll_type','=',$q['payment_type'])->get();
                foreach($payrollcomponent as $c){
                    $data2[$ctr][$c->component_name->component] = number_format($c->value,0);
                }
            }   
            if(isset($q['employee_bpjs_cut'])){
                $data2[$ctr]['Potongan BPJS Ketenagakerjaan']     = $q['employee_bpjs_cut'];
                
            }
            if(isset($q['health_bpjs_cut'])){
                $data2[$ctr]['Potongan BPJS Kesehatan']     = $q['health_bpjs_cut'];
                
            }
            if(isset($q['other_insurance_cut'])){
                $data2[$ctr]['Potongan Asuransi Lain']     = $q['other_insurance_cut'];
                
            }
            if(isset($q['pph_cut'])){
                $data2[$ctr]['Potongan PPH 21']     = $q['pph_cut'];
                
            }       
           $ctr++;
       }
      
    //    echo "<pre>";
    //    print_r($data2);
    //    exit;
      
       $type = 'xls';
       return Excel::create('Employee', function($excel) use ($data2) {
           $excel->sheet('mySheet', function($sheet) use ($data2)
           {
               $sheet->fromArray($data2);
           });
       })->download($type);
   }

    /* import excel function start here */
    public function importExcel()
    {
        ini_set('memory_limit', '-1');
        // Get current data from employee_code table
        $employee_code = Employee::lists('no_ktp')->toArray();
        // print_r($employee_code); exit;
        if(Input::hasFile('import_file')){
			$path = Input::file('import_file')->getRealPath();
            // print_r($path); exit;
			$data = Excel::load($path, function($reader) {
            })->get();
            // print_r($data); exit;
			if(!empty($data) && $data->count()){
                $insert = array();
                $insert2 = array();
                $insert3 = array();
                $insert4 = array();
                $designationList = array();
                $sql_data_competence = array();
                $ktp = array();
				foreach ($data as $key => $value) {
                    if($value->no_ktp!=''){
                        if(in_array($value->no_ktp, $ktp)){
                            continue;
                        }
                        $ktp[] = $value->no_ktp;
                        // print_r($value); exit;
                        // Skip employee_code previously added using in_array
                        if (in_array($value->no_ktp, $employee_code)){
                            $designation = Designation::where('designation','=',$value->jabatan)->first();
                            if($designation){
                                $department = Department::where('department','=',$designation->did)->first();
                            }
                            $company = Company::where('company','=',$value->perusahaan)->first();
                            if($company){
                                $project = Project::where('company','=',$company->id)->first();
                            }
                            $last_education = LastEducation::where('education','like','%'.$value->pendidikan.'%')->first();
    
                            if($designation==''){
                                //Penomoran id
                                $last_number = Designation::max('id');
                                if($last_number==''){
                                    $number = 1;
                                } else {
                                    $number = 1 + $last_number;
                                }
                                if($value->jabatan!=''){
                                    $designation = new Designation();
                                    $designation->id = $number;
                                    $designation->designation = $value->jabatan;
                                    $designation->did = 1;
                                    $designation->save();
                                }
                            } 
                            if($company==''){
                                //Penomoran id
                                $last_number = Company::max('id');
                                if($last_number==''){
                                    $number = 1;
                                } else {
                                    $number = 1 + $last_number;
                                }
                                if($value->perusahaan!=''){
                                    $company = new Company();
                                    $company->id = $number;
                                    $company->company = $value->perusahaan;
                                    $company->company_code = str_pad($number,3,'0',STR_PAD_LEFT);
                                    $company->save();
                                }
                            } 
                            if($project==''){
                                //Penomoran id
                                $last_number = Project::max('id');
                                if($last_number==''){
                                    $number = 1;
                                } else {
                                    $number = 1 + $last_number;
                                }
    
                                // $company = Company::find($comp_id);
                    
                                $area = $company->company_code;
                                $month = get_roman_letters(date('m'));
                                $year = date('Y');
                    
                                $letter_number = $area.'/Proyek'.'/'.$month.'/'.$year;
    
                                $project                       = new Project();
                                $project->id                   = $number;
                                $project->project_number       = $letter_number;
                                $project->internal_company     = 1;
                                $project->company              = $company->id;
                                $project->project              = 'Kontrak Klien '.$company->company;
                                $project->facility             = '["kesehatan"]';
                                $project->health_facility_type = '["Asuransi"]';;
                                $project->start_date           = date('Y-m-d');
                                $project->status               = 'drafted';
                                $project->end_date             = date('Y-m-d');
                                $project->value                = 0;
                                $project->save();
                                
                                $project_id = $project->id;
                    
                            } else {
                                if($project){
                                    $project_id = $project->id;
                                } else {
                                    //Penomoran id
                                    $last_number = Project::max('id');
                                    if($last_number==''){
                                        $number = 1;
                                    } else {
                                        $number = 1 + $last_number;
                                    }
        
                                    // $company = Company::find($comp_id);
                        
                                    $area = $company->company_code;
                                    $month = get_roman_letters(date('m'));
                                    $year = date('Y');
                        
                                    $letter_number = $area.'/Proyek'.'/'.$month.'/'.$year;
        
                                    $project                       = new Project();
                                    $project->id                   = $number;
                                    $project->project_number       = $letter_number;
                                    $project->internal_company     = 1;
                                    $project->company              = $company->id;
                                    $project->project              = 'Kontrak Klien '.$company->company;
                                    $project->facility             = '["kesehatan"]';
                                    $project->health_facility_type = '["Asuransi"]';;
                                    $project->start_date           = date('Y-m-d');
                                    $project->status               = 'drafted';
                                    $project->end_date             = date('Y-m-d');
                                    $project->value                = 0;
                                    $project->save();
                        
                                    $project_id = $project->id;
                                }
                            }
                            $gender = "";
                            if(strtolower($value->jenis_kelamin) == 'laki-laki' || strtolower($value->jenis_kelamin) == 'pria' || strtolower($value->jenis_kelamin) == 'laki'){
                                $gender = "male";
                            } else if(strtolower($value->jenis_kelamin) == 'wanita' || strtolower($value->jenis_kelamin) == 'perempuan'){
                                $gender = "female";
                            }
                            $status = "";
                            if(strtolower($value->status_pegawai) == 'aktif'){
                                $status = "active";
                            } else if(strtolower($value->status_pegawai) == 'tidak aktif'){
                                $status = "inactive";
                            }
                            $employee_type = 'Outsourcing';
                            $employee_status = 'PKWT';
    
                            if($company->id==1){
                                $employee_type = 'Internal';
                                $employee_status = 'TETAP';
                            }
                            if($last_education){
                                $pendidikan = $last_education->id;
                            } else {
                                $pendidikan = 1;
                            }
    
    
                            $designations = [];
                            $companys = [];
                            $projects = [];
                            $locations = [];
                            $dob = get_date_format_inggris($value->tgl_lahir);
                            $doj = get_date_format_inggris($value->tgl_masuk);
                            $dol = get_date_format_inggris($value->tgl_berakhir);
    
                            // echo $value->no_ktp; 
                            // echo $value->nik; 
                            // echo $value->tgl_lahir; 
                            // exit;
                            $employee_update                   = Employee::where('no_ktp','=',$value->no_ktp)->first();
                            $employee_update->fname            = $value->nama_lengkap;
                            $employee_update->no_ktp           = $value->no_ktp;
                            $employee_update->employee_code    = $value->nik;
                            $employee_update->password         = bcrypt($value->no_ktp);
                            $employee_update->employee_type    = $employee_type;
                            $employee_update->employee_status  = $employee_status;
                            $employee_update->designation      = $designation->id;
                            $employee_update->department       = $designation->did;
                            $employee_update->project          = $project_id;
                            $employee_update->company          = $company->id;
                            $employee_update->location         = $value->area;
                            $employee_update->birth_place      = $value->tempat_lahir;
                            $employee_update->dob              = $dob;      
                            $employee_update->mother_name      = $value->nama_ibu;
                            $employee_update->doj              = $doj;
                            $employee_update->dol              = $dol;
                            $employee_update->phone            = $value->telp;
                            $employee_update->email            = $value->email;
                            $employee_update->religion         = $value->agama;
                            $employee_update->gender           = $gender;
                            $employee_update->last_education   = $pendidikan;
                            $employee_update->status           = $status;
                            $employee_update->per_address      = $value->alamat_ktp;
                            $employee_update->per_village      = $value->kelurahan_ktp;
                            $employee_update->per_district     = $value->kecamatan_ktp;
                            $employee_update->per_city         = $value->kota_ktp;
                            $employee_update->pre_address      = $value->alamat_domisili;
                            $employee_update->pre_village      = $value->kelurahan_domisili;
                            $employee_update->pre_district     = $value->kecamatan_domisili;
                            $employee_update->pre_city         = $value->kota_domisili;
                            $employee_update->save();
    
                            $employee_bank = EmployeeBankAccount::where('emp_id','=', $employee_update->id)->first();
    
                            if($employee_bank){
                                $employee_bank->account_name                 = $employee_update->fname;
                                $employee_bank->account_number               = $value->no_rekening;
                                $employee_bank->pph_cut                      = $value->potongan_pph;
                                $employee_bank->tax_status                   = $value->status;
                                $employee_bank->npwp_number                  = $value->no_npwp;
                                $employee_bank->employee_bpjs                = $value->no_bpjs_tk;
                                $employee_bank->employee_bpjs_cut            = $value->potongan_bpjs_tk;
                                $employee_bank->health_bpjs                  = $value->no_bpjs_kesehatan;
                                $employee_bank->health_bpjs_cut              = $value->potongan_bpjs_kesehatan;
                                $employee_bank->other_insurance_cut          = $value->potongan_asuransi_lain;
                                $employee_bank->other_insurance_number       = $value->no_asuransi_lain;
                                $employee_bank->save();
                            } else {
                                $employee_bank                               = new EmployeeBankAccount();
                                $employee_bank->account_name                 = $employee_update->fname;
                                $employee_bank->account_number               = $value->no_rekening;
                                $employee_bank->pph_cut                      = $value->potongan_pph;
                                $employee_bank->tax_status                   = $value->status;
                                $employee_bank->npwp_number                  = $value->no_npwp;
                                $employee_bank->employee_bpjs                = $value->no_bpjs_tk;
                                $employee_bank->employee_bpjs_cut            = $value->potongan_bpjs_tk;
                                $employee_bank->health_bpjs                  = $value->no_bpjs_kesehatan;
                                $employee_bank->health_bpjs_cut              = $value->potongan_bpjs_kesehatan;
                                $employee_bank->other_insurance_cut          = $value->potongan_asuransi_lain;
                                $employee_bank->other_insurance_number       = $value->no_asuransi_lain;
                                $employee_bank->save();
    
                            }

                            $employment = EmploymentHistory::where('emp_id','=', $employee_update->id)
                            ->where('project','=', $employee_update->project)
                            ->where('designation','=', $employee_update->designation)
                            ->where('department','=', $employee_update->department)
                            ->where('location','=', $employee_update->location)
                            ->first();
                            
                            if($employment){
                                $employment->project = $employee_update->project;
                                $employment->designation = $employee_update->project;
                                $employment->department = $employee_update->project;
                                $employment->location = $employee_update->project;
                                $employment->start_date = $employee_update->doj;
                                $employment->end_date = $employee_update->dol;
                                $employment->save();
                            } else {
                                $employment = new EmploymentHistory();
                                $employment->emp_id = $employee_update->id;
                                $employment->project = $employee_update->project;
                                $employment->designation = $employee_update->project;
                                $employment->department = $employee_update->project;
                                $employment->location = $employee_update->project;
                                $employment->start_date = $employee_update->doj;
                                $employment->end_date = $employee_update->dol;
                                $employment->save();
                            }

                            $notif = Notification::where('id_tag','=', $employee_update->id)
                            ->where('tag','=', 'employees')
                            ->where('title','=', $employee_update->employee_code)
                            ->where('description','=', 'Berakhir Kontrak dalam 30 Hari')
                            ->first();
                            
                            if($notif){
                                $notification->tag = 'employees';
                                $notification->title = $emp->employee_code;
                                $notification->description = 'Berakhir Kontrak dalam 30 Hari';
                                $notification->show_date = date('Y-m-d',strtotime($employee_update->dol.'-30 day'));
                                $notification->start_date = $employee_update->dol;
                                $notification->end_date = $employee_update->dol;
                                $notification->route = 'employees/all';
                                $notification->save();
                            } else {
                                $notification = new Notification();
                                $notification->id_tag = $employee_update->id;
                                $notification->tag = 'employees';
                                $notification->title = $employee_update->employee_code;
                                $notification->description = 'Berakhir Kontrak dalam 30 Hari';
                                $notification->show_date = date('Y-m-d',strtotime($employee_update->dol.'-30 day'));
                                $notification->start_date = $employee_update->dol;
                                $notification->end_date = $employee_update->dol;
                                $notification->route = 'employees/all';
                                $notification->save();
                            }
                            

                        } else {
                            $designation = Designation::where('designation','=',$value->jabatan)->first();
                            if($designation){
                                $department = Department::where('department','=',$designation->did)->first();
                            }
                            $company = Company::where('company','=',$value->perusahaan)->first();
                            if($company){
                                $project = Project::where('company','=',$company->id)->first();
                            }
                            $last_education = LastEducation::where('education','like','%'.$value->pendidikan.'%')->first();
                            
                            if($designation==''){
                                //Penomoran id
                                $last_number = Designation::max('id');
                                if($last_number==''){
                                    $number = 1;
                                } else {
                                    $number = 1 + $last_number;
                                }
                                if($value->jabatan!=''){
                                    $designation = new Designation();
                                    $designation->id = $number;
                                    $designation->designation = $value->jabatan;
                                    $designation->did = 1;
                                    $designation->save();
                                }
                            } 
                            if($company==''){
                                //Penomoran id
                                $last_number = Company::max('id');
                                if($last_number==''){
                                    $number = 1;
                                } else {
                                    $number = 1 + $last_number;
                                }
                                if($value->perusahaan!=''){
                                    $company = new Company();
                                    $company->id = $number;
                                    $company->company = $value->perusahaan;
                                    $company->company_code = str_pad($number,3,'0',STR_PAD_LEFT);
                                    $company->save();
                                }
                            } 
                            if($project==''){
                                //Penomoran id
                                $last_number = Project::max('id');
                                if($last_number==''){
                                    $number = 1;
                                } else {
                                    $number = 1 + $last_number;
                                }
    
                                // $company = Company::find($comp_id);
                    
                                $area = $company->company_code;
                                $month = get_roman_letters(date('m'));
                                $year = date('Y');
                    
                                $letter_number = $area.'/Proyek'.'/'.$month.'/'.$year;
    
                                $project                       = new Project();
                                $project->id                   = $number;
                                $project->project_number       = $letter_number;
                                $project->internal_company     = 1;
                                $project->company              = $company->id;
                                $project->project              = 'Kontrak Klien '.$company->company;
                                $project->facility             = '["kesehatan"]';
                                $project->health_facility_type = '["Asuransi"]';;
                                $project->start_date           = date('Y-m-d');
                                $project->status               = 'drafted';
                                $project->end_date             = date('Y-m-d');
                                $project->value                = 0;
                                $project->save();
                    
                                $project_id = $project->id;
                            } else {
                                if($project){
                                    $project_id = $project->id;
                                } else {
                                    //Penomoran id
                                    $last_number = Project::max('id');
                                    if($last_number==''){
                                        $number = 1;
                                    } else {
                                        $number = 1 + $last_number;
                                    }
        
                                    // $company = Company::find($comp_id);
                        
                                    $area = $company->company_code;
                                    $month = get_roman_letters(date('m'));
                                    $year = date('Y');
                        
                                    $letter_number = $area.'/Proyek'.'/'.$month.'/'.$year;
        
                                    $project                       = new Project();
                                    $project->id                   = $number;
                                    $project->project_number       = $letter_number;
                                    $project->internal_company     = 1;
                                    $project->company              = $company->id;
                                    $project->project              = 'Kontrak Klien '.$company->company;
                                    $project->facility             = '["kesehatan"]';
                                    $project->health_facility_type = '["Asuransi"]';;
                                    $project->start_date           = date('Y-m-d');
                                    $project->status               = 'drafted';
                                    $project->end_date             = date('Y-m-d');
                                    $project->value                = 0;
                                    $project->save();
    
                                    $project_id = $project->id;
                        
                                }
                            }
                            $gender = "";
                            if(strtolower($value->jenis_kelamin) == 'laki-laki' || strtolower($value->jenis_kelamin) == 'pria' || strtolower($value->jenis_kelamin) == 'laki'){
                                $gender = "male";
                            } else if(strtolower($value->jenis_kelamin) == 'wanita' || strtolower($value->jenis_kelamin) == 'perempuan'){
                                $gender = "female";
                            }
                            $status = "";
                            if(strtolower($value->status_pegawai) == 'aktif'){
                                $status = "active";
                            } else if(strtolower($value->status_pegawai) == 'tidak aktif'){
                                $status = "inactive";
                            }
    
                            $dob = get_date_format_inggris($value->tgl_lahir);
                            $doj = get_date_format_inggris($value->tgl_masuk);
                            $dol = get_date_format_inggris($value->tgl_berakhir);
    
                            $employee_type = 'Outsourcing';
                            $employee_status = 'PKWT';
                            echo $value->nama_lengkap;
                            if($company->id==1){
                                $employee_type = 'Internal';
                                $employee_status = 'TETAP';
                            }
                            if($last_education){
                                $pendidikan = $last_education->id;
                            } else {
                                $pendidikan = 1;
                            }
    
                            // $name = implode(" ",$value->nama_lengkap);
                            $designations[] = $designation->id;
                            $companys[] = $company->id;
                            $projects[] = $project_id;
                            $locations[] = $value->area;
                            if($value->no_ktp!='0' && $value->no_ktp!='-' && $value->no_ktp!='' && $value->no_ktp!=null){
                                $insert[] = [
                                    'fname' => $value->nama_lengkap, 
                                    // 'lname' => $value->nama_lengkap, 
                                    'user_name' => strtolower($value->nik), 
                                    'password' => bcrypt($value->no_ktp), 
                                    'employee_code' => $value->nik, 
                                    'employee_type' => $employee_type, 
                                    'employee_status' => $employee_status, 
                                    'no_ktp' => $value->no_ktp, 
                                    'designation' => $designation->id, 
                                    'department' => $designation->did, 
                                    'project' => $project_id, 
                                    'company' => $company->id, 
                                    'location' => $value->area, 
                                    'email' => $value->email, 
                                    'mother_name' => $value->nama_ibu, 
                                    'dob' => $dob, 
                                    'birth_place' => $value->tempat_lahir, 
                                    'doj' => $doj, 
                                    'dol' => $dol, 
                                    'phone' => $value->telp, 
                                    'religion' => $value->agama, 
                                    'last_education' => $pendidikan, 
                                    'per_address' => $value->alamat_ktp, 
                                    'per_village' => $value->kelurahan_ktp, 
                                    'per_district' => $value->kecamatan_ktp, 
                                    'per_city' => $value->kota_ktp, 
                                    'pre_address' => $value->alamat_domisili, 
                                    'pre_village' => $value->kelurahan_domisili, 
                                    'pre_district' => $value->kecamatan_domisili, 
                                    'pre_city' => $value->kota_domisili, 
                                    'status' => $status, 
                                    'gender' => $gender, 
                                ];
                                $insert2[] = [
                                    'emp_id' => '', 
                                    'account_number' => $value->no_rekening, 
                                    'tax_status' => $value->status, 
                                    'npwp_number' => $value->no_npwp, 
                                    'pph_cut' => $value->potongan_pph, 
                                    'employee_bpjs' => $value->no_bpjs_tk, 
                                    'employee_bpjs_cut' => $value->potongan_bpjs_tk, 
                                    'health_bpjs' => $value->no_bpjs_kesehatan, 
                                    'health_bpjs_cut' => $value->potongan_bpjs_kesehatan, 
                                    'other_insurance_number' => $value->no_asuransi_lain, 
                                    'other_insurance_cut' => $value->potongan_asuransi_lain
                                ];
                                $insert3[] = [
                                    'emp_id' => '', 
                                    'project' => $project_id, 
                                    'designation' => $designation->id, 
                                    'department' => $designation->did, 
                                    'location' => $value->area, 
                                    'start_date' => $doj, 
                                    'end_date' => $dol, 
                                ];
                                $insert4[] = [
                                    'id_tag' => '', 
                                    'tag' => 'employees', 
                                    'title' => $value->nik, 
                                    'description' => 'Berakhir Kontrak dalam 30 Hari', 
                                    'show_date' => date('Y-m-d',strtotime($dol.'-30 day')), 
                                    'start_date' => $dol, 
                                    'end_date' => $dol, 
                                    'route' => 'employees/all', 
                                ];
                            }
                        }
                    }
                }
                
                $month = date('m');
                $year = date('Y');
                $last_letter_number = DB::table('sys_jobs')->where('no_position','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('no_position');
                if($last_letter_number==''){
                    $number = 1;
                } else{
                    $last_number = explode('/', $last_letter_number);
                    $number = 1 + $last_number[0];
                }
        
                $num_elements_job           = 0;
                $sql_data_jobs              = array();
                $id_job                     = [];
                
                while($num_elements_job<count($designations)) {

                    $open_dates              = date('Y-m-d');
                    $close_dates             = date('Y-m-d');
                    $career = CareerPathEmployee::where('designation','=',$designations[$num_elements_job])->first();
                    
                    $jobs = Jobs::where('position','=',$designations[$num_elements_job])
                    ->where('project','=',$projects[$num_elements_job])
                    ->where('company','=',$companys[$num_elements_job])
                    // ->where('job_location','=',$locations[$num_elements_job])
                    ->first();
                    if($jobs){
                        
                    } else {
                        if($career){
                            $experience = $career->working_period;
                            $edu = $career->education;
                        } else {
                            $experience = 1;
                            $edu = 1;
                        }
                        
                        $jobs                   = new Jobs();
                        $jobs->position         = $designations[$num_elements_job];
                        $jobs->project          = $projects[$num_elements_job];
                        $jobs->company          = $companys[$num_elements_job];
                        $jobs->no_position      = $num_elements_job + $number."/Lowongan"."/".get_roman_letters(date("m"))."/".date("Y");
                        $jobs->job_location     = $locations[$num_elements_job];
                        $jobs->experience       = $experience;
                        $jobs->last_education   = $edu;
                        $jobs->post_date        = date("Y-m-d");
                        $jobs->apply_date       = $open_dates;
                        $jobs->close_date       = $close_dates;
                        $jobs->quota            = 0;
                        $jobs->save();

                        $id_job[]   = $jobs->id;

                        $sql_data_jobs[] = array(
                            'position'          => $designations[$num_elements_job],
                            'project'           => $projects[$num_elements_job],
                            'company'           => $companys[$num_elements_job],
                            'no_position'       => $num_elements_job + $number."/Lowongan"."/".get_roman_letters(date("m"))."/".date("Y"),
                            'job_location'      => $locations[$num_elements_job],
                            'experience'        => $experience,
                            'last_education'    => $edu,
                            'post_date'         => date("Y-m-d"),
                            'apply_date'        => $open_dates,
                            'close_date'        => $close_dates,
                            'quota'             => 0,
                        );
                    }
                    $num_elements_job++;
                }

                $num_elements               = 0;
                
                $sql_data                   = array();
        
                while($num_elements<count($id_job)) {
                    
                    $open_dates              = date('Y-m-d');
                    $close_dates             = date('Y-m-d');

                    $projectneeds = ProjectNeeds::where('id','=',$sql_data_jobs[$num_elements]['project'])
                    ->where('id_designation','=',$sql_data_jobs[$num_elements]['position'])
                    ->where('id_job','=',$id_job[$num_elements])
                    ->where('location','=',$sql_data_jobs[$num_elements]['job_location'])
                    ->first();
                    if($projectneeds){

                    } else {
                        $sql_data[] = array(
                            'id'                => $sql_data_jobs[$num_elements]['project'],
                            'id_designation'    => $sql_data_jobs[$num_elements]['position'],
                            'id_job'            => $id_job[$num_elements],
                            'location'          => $sql_data_jobs[$num_elements]['job_location'],
                            'open_date'         => $open_dates,
                            'close_date'        => $close_dates,
                            'total'             => 0,
                        );
                    }
                    $num_elements++;
                }
        
                $projectneeds               = ProjectNeeds::insert($sql_data);
                // echo "<pre>";
                // print_r($insert);exit;

				if(!empty($insert)){
                    $emp_id = array();

                    foreach($insert as $i){
                        $emp_id[] = DB::table('sys_employee')->insertGetId($i);
                    }
                    $a=0;
                    foreach ($emp_id as $id){


                        $insert2[$a]['emp_id'] = $id;
                        $insert3[$a]['emp_id'] = $id;
                        $insert4[$a]['id_tag'] = $id;
                        // DB::table('sys_employee_bank_accounts')->insert($insert2[$a]);
                        $a++;
                    }
                    // Employee::insert($insert);
                    EmployeeBankAccount::insert($insert2);
                    EmploymentHistory::insert($insert3);
                    Notification::insert($insert4);

					return redirect('employees/all')->with([
                        'message' => 'Employee Added Successfully'
                    ]);
				} else {
                    return redirect('employees/all')->with([
                        'message' => 'Employee Updated Successfully'
                    ]);
                }
			}
        }
		return back();
    }

    /* getDepartment  Function Start Here */
    public function getDepartment(Request $request)
    {

        $proj_id = $request->proj_id;
        if ($proj_id) {
            echo '<option value="0">Select Department</option>';
            $department = ProjectNeeds::where('id','=', $proj_id)->get();
            foreach ($department as $d) {
                echo '<option value="' . $d->designation_name->did . '">' . $d->designation_name->department_name->department . '</option>';
            }
        }
    }
    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project . '</option>';
            }
        }
    }

    /* getEmployeeType Function Start Here */
    public function getEmployeeType(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            $company = Company::where('id', $comp_id)->get();
            foreach ($company as $d) {
                echo '<option value="' . $d->category . '">' . $d->category . '</option>';
            }
        }
    }

    /* getDesignation  Function Start Here */
    public function getDesignation(Request $request)
    {
        $dep_id = $request->dep_id;
        if ($dep_id) {
            echo '<option value="0">Select Designation</option>';
            $designation = Designation::where('did', $dep_id)->get();
            foreach ($designation as $d) {
                echo '<option value="' . $d->id . '">' . $d->designation . '</option>';
            }
        }
    }

    /* getCareer  Function Start Here */
    public function getCareer(Request $request)
    {
        $des_id = $request->des_id;
        if ($des_id) {
            $designation = Designation::where('id', $des_id)->get();
            foreach ($designation as $d) {
                echo '<input type="hidden" class="form-control" value="' . $d->career . '" name="career">';
            }
        }
    }

    /* downloadIdCard  Function Start Here */
    public function downloadIdCard(Request $request) {

        Excel::create('Id Card', function($excel) {

            $cmd            = Input::get('project');
    
            $project        = Project::find($cmd);
    
            $employee       = Employee::where('project','=', $cmd)->where('role_id','!=',1)->get()->toArray();
            $test           = array('key' => $employee);

            $excel->sheet('New sheet', function($sheet) use ($employee) {
        
                $sheet->loadView('admin.employee.id-card', array('key' => $employee));
        
            });
        
        })->export('xls');

    }

}
