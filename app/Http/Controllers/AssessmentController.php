<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\AssessmentForm;
use App\AssessmentDimension;
use App\AssessmentAspect;
use App\ViolationTypes;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use WkPdf;

date_default_timezone_set(app_config('Timezone'));

class AssessmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* forms  Function Start Here */
    public function forms()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 30)->first();
        $forms = AssessmentForm::orderBy('id','asc')->get();
        return view('admin.assessment.forms', compact('forms','permcheck'));

    }

    /* editDimension  Function Start Here */
    public function editDimension($id)
    {

        $dimension = AssessmentDimension::find($id);
        $assessment_aspect = AssessmentAspect::where('dimension','=',$id)->get();
        if ($dimension) {
            return view('admin.assessment.edit-dimension', compact('dimension','assessment_aspect'));
        } else {
            return redirect('assessment/forms')->with([
                'message' => 'Dimension Info Not Found',
                'message_important' => true
            ]);
        }
    }

    /* postEditDimension  Function Start Here */
    public function postEditDimension(Request $request)
    {
        $cmd = Input::get('cmd');
        $dimension_id = Input::get('dimension_id');
        $v = \Validator::make($request->all(), [
        ]);
        
        if ($v->fails()) {
            return redirect('assessments/view-form/'.$cmd)->withErrors($v->errors());
        }

        $assessment = AssessmentForm::find($cmd);

        $dimension = Input::get('dimension');
        $dimension_weight = Input::get('dimension_weight');
        $aspect = Input::get('aspect');
        $aspect_weight = Input::get('aspect_weight');

        $assessment_dimension = AssessmentDimension::find($dimension_id);
        if($assessment_dimension){
            $assessment_dimension->assessment = $cmd;
            $assessment_dimension->dimension = $dimension;
            $assessment_dimension->dimension_weight = $dimension_weight;
            $assessment_dimension->save();
    
            $num_elements           = 0;
            
            $sql_data               = array();
    
            while($num_elements<count($aspect)) {
                $sql_data[] = array(
                    'id'                => "",
                    'assessment'        => $cmd,
                    'dimension'         => $dimension_id,
                    'aspect'            => $aspect[$num_elements],
                    'weight'            => $aspect_weight[$num_elements],
                    'updated_at'        => "",
                    'created_at'        => ""
                );
                $num_elements++;
            }
    
            $assessment_aspect          = AssessmentAspect::insert($sql_data);

            return redirect('assessments/view-form/'.$cmd)->with([
                'message' => 'Dimension Updated Successfully'
            ]);
        } else {
            return redirect('assessments/view-form/'.$cmd)->with([
                'message' => 'Dimension Not Found',
                'message_important' => true
            ]);
        }

    }


    /* addForm  Function Start Here */
    public function addForm(Request $request)
    {
        $v = \Validator::make($request->all(), [
        ]);

        if ($v->fails()) {
            return redirect('assessments/forms')->withErrors($v->errors());
        }

        $assessment = Input::get('assessment');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);

        $assessment_form = new AssessmentForm();
        $assessment_form->assessment = $assessment;
        $assessment_form->date = $date;
        $assessment_form->save();

        if ($assessment_form!='') {
            return redirect('assessments/forms')->with([
                'message' => 'Form Added Successfully'
            ]);

        } else {
            return redirect('assessments/forms')->with([
                'message' => 'Form Already Exist',
                'message_important' => true
            ]);
        }

    }

    /* addDimension  Function Start Here */
    public function addDimension(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
        ]);
        
        if ($v->fails()) {
            return redirect('assessments/view-form/'.$cmd)->withErrors($v->errors());
        }

        $assessment = AssessmentForm::find($cmd);

        $dimension = Input::get('dimension');
        $dimension_weight = Input::get('dimension_weight');
        $aspect = Input::get('aspect');
        $aspect_weight = Input::get('aspect_weight');

        $assessment_dimension = new AssessmentDimension();
        $assessment_dimension->assessment = $cmd;
        $assessment_dimension->dimension = $dimension;
        $assessment_dimension->dimension_weight = $dimension_weight;
        $assessment_dimension->save();

        $dimension_id = $assessment_dimension->id;

        $num_elements           = 0;
        
        $sql_data               = array();

        while($num_elements<count($aspect)) {
            $sql_data[] = array(
                'id'                => "",
                'assessment'        => $cmd,
                'dimension'         => $dimension_id,
                'aspect'            => $aspect[$num_elements],
                'weight'            => $aspect_weight[$num_elements],
                'updated_at'        => "",
                'created_at'        => ""
            );
            $num_elements++;
        }

        $assessment_aspect          = AssessmentAspect::insert($sql_data);

        if ($assessment_dimension!='') {
            return redirect('assessments/view-form/'.$cmd)->with([
                'message' => 'Dimension Added Successfully'
            ]);

        } else {
            return redirect('assessments/view-form/'.$cmd)->with([
                'message' => 'Dimension Already Exist',
                'message_important' => true
            ]);
        }

    }


    /* editForm  Function Start Here */
    public function editForm(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
        ]);

        if ($v->fails()) {
            return redirect('assessments/forms')->withErrors($v->errors());
        }
        
        $assessment = Input::get('assessment');
        $status = Input::get('status');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        
        $assessment_form = AssessmentForm::find($cmd);
        if ($assessment_form) {
            $assessment_form->assessment = $assessment;
            $assessment_form->date = $date;
            $assessment_form->status = $status;
            $assessment_form->save();

            return redirect('assessments/forms')->with([
                'message' => 'Form Updated Successfully'
            ]);

        } else {
            return redirect('assessments/forms')->with([
                'message' => 'Form Not Found',
                'message_important' => true
            ]);
        }
    }

    /* setFormStatus  Function Start Here */
    public function setFormStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('assessments/forms')->withErrors($v->fails());
        }

        $forms  = AssessmentForm::find($cmd);
        if($forms){
            $forms->status=$request->status;
            $forms->save();

            return redirect('assessments/forms')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('assessments/forms')->with([
                'message' => 'Form not found',
                'message_important'=>true
            ]);
        }

    }


    /* deleteForm  Function Start Here */
    public function deleteForm($id)
    {
        $assessment = AssessmentForm::find($id);
        if ($assessment) {
            $assessment->delete();

            return redirect('assessments/forms')->with([
                'message' => 'Form Deleted Successfully'
            ]);
        } else {
            return redirect('assessments/forms')->with([
                'message' => 'Form Not Found',
                'message_important' => true
            ]);
        }

    }

    /* deleteAspect Function Start Here */
    public function deleteAspect($id)
    {

        $aspect = AssessmentAspect::find($id);
        if ($aspect) {
            $aspect->delete();

            return redirect('assessments/view-form/'.$aspect->assessment)->with([
                'message' => 'Aspect Deleted Successfully'
            ]);
        } else {
            return redirect('assessments/view-form/'.$aspect->assessment)->with([
                'message' => 'Aspect Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewForm  Function Start Here */
    public function viewForm($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 30)->first();
        $assessment = AssessmentForm::find($id);
        $assessment_dimension = AssessmentDimension::where('assessment','=', $id)->get();
        $assessment_aspect = AssessmentAspect::where('assessment','=',$id)->get();
        $total_weight_dimension = AssessmentDimension::where('assessment','=',$id)->sum('dimension_weight');
        $total_weight_aspect = AssessmentAspect::where('assessment','=',$id)->sum('weight');
        return view('admin.assessment.view-form', compact('assessment','assessment_dimension','assessment_aspect','total_weight_dimension','total_weight_aspect','permcheck'));
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {

        $assessment = AssessmentForm::find($id);
        $assessment_dimension = AssessmentDimension::where('assessment','=', $id)->get();
        $assessment_aspect = AssessmentAspect::where('assessment','=',$id)->get();
        $total_weight_dimension = AssessmentDimension::where('assessment','=',$id)->sum('dimension_weight');
        $total_weight_aspect = AssessmentAspect::where('assessment','=',$id)->sum('weight');

        $pdf = WkPdf::loadView('admin.assessment.pdf-form',compact('assessment','assessment_dimension','assessment_aspect','total_weight_dimension','total_weight_aspect'));

        return $pdf->stream('assessment_form.pdf');


    }


}
