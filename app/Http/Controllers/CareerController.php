<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\Company;
use App\Department;
use App\Designation;
use App\Employee;
use App\EmployeeFiles;
use App\EmployeeRolesPermission;
use App\CareerPathEmployee;
use App\CareerPathEmployeeCompetence;
use App\CareerPathEmployeeAssessment;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;

date_default_timezone_set(app_config('Timezone'));

class CareerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* careersEmployee  Function Start Here */
    public function careersEmployee()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 42)->first();
        $employee_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
        $comp_id = '';
        $dep_id = '';
        $des_id = '';
        $careers = CareerPathEmployee::orderBy('id','asc')->get();
        $department = Department::all();
        $designation = Designation::all();
        $employee = Employee::where('role_id','!=','1')->get();
        $company = Company::all();
        return view('admin.career.careers-management-employee', compact('company','careers','department','designation','des_id','comp_id','dep_id','employee','permcheck','employee_permission'));

    }

    /* postCustomSearch  Function Start Here */
    public function postCustomSearch(Request $request)
    {

        $comp_id = Input::get('company');
        $dep_id = Input::get('department');
        $des_id = Input::get('designation');

        $employee_query = Employee::where('role_id','!=','1');

        if ($comp_id) {
            $employee_query->Where('company', $comp_id);
        }

        if ($dep_id) {
            $employee_query->Where('department', $dep_id);
        }

        if ($des_id) {
            $employee_query->Where('designation', $des_id);
        }

        $employee = $employee_query->get();
        $department = Department::all();
        $designation = Designation::all();
        $company = Company::all();
        return view('admin.career.careers-management-employee', compact('company','careers','department','designation','des_id','comp_id','dep_id','employee'));

    }

    /* careers  Function Start Here */
    public function careers()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 31)->first();
        $des_id = '';
        $careers = CareerPathEmployee::orderBy('id','asc')->get();
        $department = Department::all();
        $designation = Designation::all();
        return view('admin.career.careers', compact('careers','department','designation','des_id','permcheck'));

    }

    /* addCareer Function Start Here */
    public function addCareer(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'department' => 'required',
            'designation' => 'required',
            'working_period' => 'required',
            'education' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('careers')->withErrors($v->errors());
        }

        $department = Input::get('department');
        $designation = Input::get('designation');
        $minimum_working_period = Input::get('working_period');
        $minimum_education = Input::get('education');

        $exist = CareerPathEmployee::where('department', $department)->where('designation', $designation)->first();
        if ($exist) {
            return redirect('careers')->with([
                'message' => 'Career Already Exist',
                'message_important' => true
            ]);
        }

        $competence = Input::get('competence');
        $assessment_type = Input::get('assessment_type');
        //Penomoran id
        $last_number_id = CareerPathEmployee::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $careers = new CareerPathEmployee();
        $careers->id = $number_id;
        $careers->department = $department;
        $careers->designation = $designation;
        $careers->working_period = $minimum_working_period;
        $careers->education = $minimum_education;
        $careers->save();

        $id_career = $careers->id;
        
        $num_elements           = 0;
        
        $sql_data               = array();
        
        while($num_elements<count($competence)) {
            $sql_data[] = array(
                'id'                => "",
                'career_path'       => $id_career,
                'competence'        => $competence[$num_elements],
                'assessment_type'   => $assessment_type[$num_elements],
                'created_at'        => "",
                'updated_at'        => ""
            );
            $num_elements++;
        }
        
        $career_path_competence = CareerPathEmployeeCompetence::insert($sql_data);

        if ($careers!='') {
            return redirect('careers')->with([
                'message' => 'Career Added Successfully'
            ]);

        } else {
            return redirect('careers')->with([
                'message' => 'Career Already Exist',
                'message_important' => true
            ]);
        }

    }

    /* viewCareer  Function Start Here */
    public function viewCareer($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 31)->first();
        $career = CareerPathEmployee::find($id);
        $competence = CareerPathEmployeeCompetence::where('career_path', '=', $id)->get();
        return view('admin.career.view-career', compact('career','competence','permcheck'));
    }

    /* viewCareerEmployee  Function Start Here */
    public function viewCareerEmployee($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 42)->first();
        $employee = Employee::find($id);
        $competence = CareerPathEmployeeAssessment::where('emp_id', '=', $id)->get();
        return view('admin.career.view-career-employee', compact('employee','competence','permcheck'));
    }


    /* updateCareer  Function Start Here */
    public function updateCareer(Request $request)
    {

        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
        ]);

        if ($v->fails()) {
            return redirect('careers/view/'.$cmd)->withErrors($v->errors());
        }
        $career = CareerPathEmployee::find($cmd);
        $working_period = Input::get('working_period');
        $education = Input::get('education');
        $status = Input::get('status');

        $competence = Input::get('competence');
        $assessment_type = Input::get('assessment_type');

        if ($career) {
            $career->working_period = $working_period;
            $career->education = $education;
            $career->status = $status;
            $career->save();

            
            $employee = Employee::where('designation','=',$cmd)->get()->toArray();
            
            // echo "<pre>"; print_r($competence);  print_r($assessment_type); exit;
            for($a=0;$a<count($competence);$a++) {

                $career_path_competence = new CareerPathEmployeeCompetence();
                $career_path_competence->career_path = $cmd;
                $career_path_competence->competence = $competence[$a];
                $career_path_competence->assessment_type = $assessment_type[$a];
                $career_path_competence->save();

                $competence_id = $career_path_competence->id;
                
                if($employee){
                    for($b=0;$b<count($employee);$b++) {
                        $career_assessment = new CareerPathEmployeeAssessment();
                        $career_assessment->emp_id = $employee[$b]['id'];
                        $career_assessment->competence = $competence_id;
                        $career_assessment->save();
                    }
                }
            }

            return redirect('careers/view/'.$cmd)->with([
                'message' => 'Career Updated Successfully'
            ]);

        } else {
            return redirect('careers/view/'.$cmd)->with([
                'message' => 'Career Not Found',
                'message_important' => true
            ]);
        }
    }


    /* deleteCareer  Function Start Here */
    public function deleteCareer($id)
    {

        $career = CareerPathEmployee::find($id);
        if ($career) {
            $career->delete();

            $employee = Employee::where('designation','=',$id)->get();
            if($employee){
                foreach($employee as $emp){
                    $CareerPathEmployeeAssessment = CareerPathEmployeeAssessment::where('emp_id','=',$emp->id)->get()->delete();
                }
            }

            return redirect('careers/view/'.$career->career_path)->with([
                'message' => 'Career Deleted Successfully'
            ]);
        } else {
            return redirect('careers/view/'.$career->career_path)->with([
                'message' => 'Career Not Found',
                'message_important' => true
            ]);
        }

    }


    /* deleteCareerCompetence  Function Start Here */
    public function deleteCareerCompetence($id)
    {

        $career = CareerPathEmployeeCompetence::find($id);
        if ($career) {
            $career->delete();

            $CareerPathEmployeeAssessment = CareerPathEmployeeAssessment::where('competence','=',$id)->get()->delete();

            return redirect('careers')->with([
                'message' => 'Competence Deleted Successfully'
            ]);
        } else {
            return redirect('careers')->with([
                'message' => 'Competence Not Found',
                'message_important' => true
            ]);
        }

    }

    /* setCareerStatus  Function Start Here */
    public function setCareerStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('careers')->withErrors($v->fails());
        }

        $careers  = CareerPathEmployee::find($cmd);
        if($careers){
            $careers->status=$request->status;
            $careers->save();

            return redirect('careers')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('careers')->with([
                'message' => 'Career not found',
                'message_important'=>true
            ]);
        }

    }

    /* setCareerAssessmentStatus  Function Start Here */
    public function setCareerAssessmentStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $emp_id=Input::get('emp_id');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('careers/view-employee/'.$emp_id)->withErrors($v->fails());
        }

        $careers_assessment  = CareerPathEmployeeAssessment::find($cmd);
        if($careers_assessment){
            $careers_assessment->status=$request->status;
            $careers_assessment->save();

            $career_qualify = CareerPathEmployeeAssessment::where('emp_id','=', $emp_id)->where('status','=','Pass')->count();
            $career_assessments = CareerPathEmployeeAssessment::where('emp_id','=', $emp_id)->count();

            $progress = ($career_qualify/$career_assessments)*100;

            $employee = Employee::find($emp_id);
            $employee->career_path_progress = $progress;
            $employee->save();


            return redirect('careers/view-employee/'.$emp_id)->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('careers/view-employee/'.$emp_id)->with([
                'message' => 'Career not found',
                'message_important'=>true
            ]);
        }

    }

    /* download excel function start here */
    public function downloadExcel()
    {
        return response()->download(public_path('assets/template_xls/career_competences.xls'));
    }

    /* import excel function start here */
    public function importExcel()
    {
        // Get current data from career table
        $designations = CareerPathEmployee::lists('designation')->toArray();

        if(Input::hasFile('import_file')){
			$path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {
			})->get();
			if(!empty($data) && $data->count()){
                $insert = array();
				foreach ($data as $key => $value) {
                    // Skip career previously added using in_array
                    if (in_array($value->jabatan, $designations))
                    continue;

                    if($value->departemen=='Direktur'){
                        $department = '12';
                    } elseif($value->departemen=='Operasional'){
                        $department = '9';
                    } elseif($value->departemen=='Keuangan'){
                        $department = '6';
                    } elseif($value->departemen=='IT, Marketing & Umum'){
                        $department = '10';
                    } elseif($value->departemen=='HR, Diklat & legal'){
                        $department = '11';
                    }
                    
                    if($value->jabatan=='Direktur'){
                        $designation = '6';
                    } elseif($value->jabatan=='Manager Keuangan'){
                        $designation = '7';
                    } elseif($value->jabatan=='Asisten Manager Penagihan'){
                        $designation = '16';
                    } elseif($value->jabatan=='Asisten Manager Akuntansi'){
                        $designation = '17';
                    } elseif($value->jabatan=='Asisten Manager Per-Bendaharaan'){
                        $designation = '22';
                    } elseif($value->jabatan=='Pelaksana Akuntansi'){
                        $designation = '34';
                    } elseif($value->jabatan=='Pelaksana Per-Bendaharaan'){
                        $designation = '35';
                    } elseif($value->jabatan=='Pelaksana Penagihan'){
                        $designation = '36';
                    } elseif($value->jabatan=='Manager Operasional'){
                        $designation = '8';
                    } elseif($value->jabatan=='Kepala Cabang/ Asisten Manager'){
                        $designation = '11';
                    } elseif($value->jabatan=='Asisten Manager Transportasi'){
                        $designation = '12';
                    } elseif($value->jabatan=='Asisten Manager Security'){
                        $designation = '15';
                    } elseif($value->jabatan=='Asisten Manager Jasa'){
                        $designation = '20';
                    } elseif($value->jabatan=='Asisten Manager Produk'){
                        $designation = '21';
                    } elseif($value->jabatan=='Supervisor Cabang'){
                        $designation = '25';
                    } elseif($value->jabatan=='Supervisor Transportasi'){
                        $designation = '26';
                    } elseif($value->jabatan=='Supervisor Lapangan Shift'){
                        $designation = '27';
                    } elseif($value->jabatan=='Supervisor Lapangan Non Shift'){
                        $designation = '28';
                    } elseif($value->jabatan=='Koordinator Shift Security'){
                        $designation = '29';
                    } elseif($value->jabatan=='Pelaksana Cabang'){
                        $designation = '30';
                    } elseif($value->jabatan=='Pelaksana Security'){
                        $designation = '31';
                    } elseif($value->jabatan=='Pelaksana Jasa'){
                        $designation = '32';
                    } elseif($value->jabatan=='Pelaksana Produk'){
                        $designation = '33';
                    } elseif($value->jabatan=='Pelaksana Transportasi'){
                        $designation = '43';
                    } elseif($value->jabatan=='Manager IT, Marketing & Umum'){
                        $designation = '9';
                    } elseif($value->jabatan=='Asisten Manager Marketing'){
                        $designation = '13';
                    } elseif($value->jabatan=='Asisten Manager Umum'){
                        $designation = '14';
                    } elseif($value->jabatan=='Asisten Manager IT'){
                        $designation = '23';
                    } elseif($value->jabatan=='Pelaksana Marketing'){
                        $designation = '37';
                    } elseif($value->jabatan=='Pelaksana Umum'){
                        $designation = '38';
                    } elseif($value->jabatan=='Pelaksana IT'){
                        $designation = '39';
                    } elseif($value->jabatan=='Manager HR, Diklat & Legal'){
                        $designation = '10';
                    } elseif($value->jabatan=='Asisten Manager Personalia'){
                        $designation = '18';
                    } elseif($value->jabatan=='Asisten Manager Diklat & K3'){
                        $designation = '19';
                    } elseif($value->jabatan=='Asisten Manager Legal'){
                        $designation = '24';
                    } elseif($value->jabatan=='Pelaksana Personalia'){
                        $designation = '40';
                    } elseif($value->jabatan=='Pelaksana Diklat & K3'){
                        $designation = '41';
                    } elseif($value->jabatan=='Pelaksana Legal'){
                        $designation = '42';
                    } 

                    if($value->pendidikan=='SD'){
                        $education = '1';
                    } elseif($value->pendidikan=='SMP'){
                        $education = '2';
                    } elseif($value->pendidikan=='SMA/SMK'){
                        $education = '3';
                    } elseif($value->pendidikan=='D1'){
                        $education = '4';
                    } elseif($value->pendidikan=='D3'){
                        $education = '5';
                    } elseif($value->pendidikan=='D4'){
                        $education = '6';
                    } elseif($value->pendidikan=='S1'){
                        $education = '7';
                    } elseif($value->pendidikan=='S2'){
                        $education = '8';
                    } elseif($value->pendidikan=='S3'){
                        $education = '9';
                    } 

                    // $department = $value->departemen;
                    // $designation = $value->jabatan;
                    $working_period = $value->masa_kerja;
                    // $education = $value->pendidikan;

					$insert[] = [
                        'department' => $department, 
                        'designation' => $designation, 
                        'working_period' => $working_period, 
                        'education' => $education
                    ];
    
                    // Add new jabatan to array
                    $designations[] = $value->jabatan;
				}
				if(!empty($insert)){
					CareerPathEmployee::insert($insert);
					return redirect('careers')->with([
                        'message' => 'Career Added Successfully'
                    ]);
				}
            }
		}
		return back();
    }

    /* import excel function start here */
    public function importExcelCompetent()
    {

        if(Input::hasFile('import_file')){
			$path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {
			})->get();
			if(!empty($data) && $data->count()){
                $insert = array();
				foreach ($data as $key => $value) {

                    
					$insert[] = [
                        'career_path' => $value->id, 
                        'competence' => $value->kompetensi
                    ];
    
				}
				if(!empty($insert)){
					CareerPathEmployeeCompetence::insert($insert);
					return redirect('careers')->with([
                        'message' => 'Career Added Successfully'
                    ]);
				}
            }
		}
		return back();
    }

    /* getDesignation  Function Start Here */
    public function getDesignation(Request $request)
    {
        $dep_id = $request->dep_id;
        if ($dep_id) {
            $designation = Designation::where('did', $dep_id)->get();
            foreach ($designation as $d) {
                echo '<option value="' . $d->id . '">' . $d->designation . '</option>';
            }
        }
    }

    /* downloadAssessmentDocument  Function Start Here */
    public function downloadAssessmentDocument($id)
    {
        $file = CareerPathEmployeeAssessment::find($id)->proof_file;
        return response()->download(public_path('assets/assessment_files/' . $file));
    }

    /* uploadAssessmentDocument  Function Start Here */
    public function uploadAssessmentDocument(Request $request)
    {
        $cmd=Input::get('cmd');
        $emp_id=Input::get('emp_id');
        $v=\Validator::make($request->all(),[
            'file'=>'required'
        ]);

        if($v->fails()){
            return redirect('careers/view-employee/'.$emp_id)->withErrors($v->fails());
        }

        $file = Input::file('file');
        $destinationPath_file = public_path() . '/assets/assessment_files/';
        $target_file = $destinationPath_file . basename($_FILES["file"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        $careers_assessment  = CareerPathEmployeeAssessment::find($cmd);
        if($careers_assessment){
            if ($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'jpeg' || $imageFileType == 'pdf') {
                $destinationPath = public_path() . '/assets/assessment_files/';
                $file_name = $file->getClientOriginalName();
                Input::file('file')->move($destinationPath, $file_name);

                $careers_assessment->proof_file=$file_name;
                $careers_assessment->status='Pass';
                $careers_assessment->save();
                
                $employee_doc=new EmployeeFiles();
                $employee_doc->emp_id=$emp_id;
                $employee_doc->file_title=$file_name;
                $employee_doc->file=$file_name;
                $employee_doc->save();

                return redirect('careers/view-employee/'.$emp_id)->with([
                    'message' => language_data('Document Uploaded Successfully')
                ]);

            } else {
                return redirect('careers/view-employee/'.$emp_id)->with([
                    'message' => 'Upload an Image or PDF',
                    'message_important' => true
                ]);
            }

        }else{
            return redirect('careers/view-employee/'.$emp_id)->with([
                'message' => 'Career not found',
                'message_important'=>true
            ]);
        }

    }

}
