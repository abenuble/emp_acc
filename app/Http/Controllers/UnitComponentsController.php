<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\UnitComponent;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Schema\Blueprint;
use Schema;
use App\Coa;
use Excel;
date_default_timezone_set(app_config('Timezone'));

class UnitComponentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* unitComponent  Function Start Here */
    public function unitComponent()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 100)->first();
        $unit = UnitComponent::orderBy('id','asc')->get();
        return view('admin.unit-component.unit', compact('unit','permcheck'));

    }

     /* import excel function start here */
     public function importExcel()
     {
         $unitComponent = UnitComponent::lists('unit')->toArray();
 
         if(Input::hasFile('import_file')){
             $path = Input::file('import_file')->getRealPath();
             $target_file = $path . basename($_FILES["import_file"]["name"]);
             $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
             $data = Excel::load($path, function($reader) {
             })->get();
             if ($fileType != 'xls'){
                 return redirect('unit-components')->with([
                     'message' => 'Upload an Excel 97-2003 ',
                     'message_important' => true
                 ]);
             } else{       
                 if(!empty($data) && $data->count()){
                     foreach ($data as $key => $value) {
                         // Skip company previously added using in_array
                         if (in_array($value->unit, $unitComponent)){
                             continue;
                         } else {
                             //Penomoran id
                             $last_number = UnitComponent::max('id');
                             if($last_number==''){
                                 $number = 1;
                             } else {
                                 $number = 1 + $last_number;
                             }
                             if ($value->unit=='' ) {
                              continue;
                             } else {
                                 $unit = new UnitComponent();
                                 $unit->id = $number;
                                 $unit->unit = $value->unit;
                                 $unit->status = $value->status;
                                 $unit->save();    
 
                             }
                         }
                     }
                     return redirect('unit-components')->with([
                         'message' => 'Unit Added Successfully'
                     ]);
                 } else{
                     return redirect('unit-components')->with([
                         'message' => 'Unit Not Found',
                         'message_important' => true
                     ]);
                 }   
             }
         }
         return back();
     }

         /* download excel function start here */
         public function downloadExcel()
         {
             return response()->download(public_path('assets/template_xls/unitComponent.xls'));
         }
   
         
    /* addComponent  Function Start Here */
    public function addUnitComponent(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'unit' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('unit-components')->withErrors($v->errors());
        }


        $unit       = Input::get('unit');
        $status     = Input::get('status');

        $exist = UnitComponent::where('unit', $unit)->first();
        if ($exist) {
            return redirect('unit-components')->with([
                'message' => 'Unit Already Exist',
                'message_important' => true
            ]);
        }
        //Penomoran id
        $last_number = UnitComponent::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }
        $units                      = new UnitComponent();
        $units->id                  = $number;
        $units->unit                = $unit;
        $units->status              = $status;
        $units->save();

        if ($units!='') {
            return redirect('unit-components')->with([
                'message' => 'Unit Added Successfully'
            ]);

        } else {
            return redirect('unit-components')->with([
                'message' => 'Unit Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateComponent  Function Start Here */
    public function updateUnitComponent(Request $request)
    {

        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'unit' => 'required',
            'status'     => 'required'
        ]);

        if ($v->fails()) {
            return redirect('unit-components')->withErrors($v->errors());
        }
        $unit = UnitComponent::find($cmd);
        $unit_name = Input::get('unit');
        $status = Input::get('status');
        if ($unit_name != $unit->unit) {
            $exist = UnitComponent::where('unit', $unit_name)->first();
            if ($exist) {
                return redirect('unit-components')->with([
                    'message' => 'Unit Already Exist',
                    'message_important' => true
                ]);
            }
        }

        if ($unit) {
            $unit->unit = $unit_name;
            $unit->status = $status;
            $unit->save();

            return redirect('unit-components')->with([
                'message' => 'Unit Updated Successfully'
            ]);

        } else {
            return redirect('unit-components')->with([
                'message' => 'Unit Not Found',
                'message_important' => true
            ]);
        }
    }

    /* viewComponent  Function Start Here */
    public function viewUnitComponent($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 100)->first();
        $unit = UnitComponent::find($id);
        return view('admin.unit-component.view-unit-component', compact('unit','coa','permcheck'));
    }

    /* setComponentStatus  Function Start Here */
    public function setUnitComponentStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('unit-components')->withErrors($v->fails());
        }
        $unit  = UnitComponent::find($cmd);
        if($unit){
            $unit->status=$request->status;
            $unit->save();

            return redirect('unit-components')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('unit-components')->with([
                'message' => 'Unit not found',
                'message_important'=>true
            ]);
        }

    }


}
