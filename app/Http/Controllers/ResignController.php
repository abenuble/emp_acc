<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\permission;
use App\Resign;
use App\Company;
use App\Copy;
use App\Project;
use App\Notification;
use App\ReferenceLetter;
use App\ProjectNeeds;
use App\EmailTemplate;
use App\Employee;
use App\EmploymentHistory;
use App\ContractRecipient;
use App\EmployeeRolesPermission;
use Illuminate\Support\Facades\Input;
use DB;
use WkPdf;

class ResignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* contract  Function Start Here */
    public function resigns()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 50)->first();
        $comp_id = '';
        $proj_id = '';
        $emp_id = '';
        $des_id = '';

        $resigns = Resign::orderBy('id','asc')->get();
        $company = Company::all();
        $employee = Employee::where('role_id','!=','1')->get();
        $project = Project::all();
        $email_template = EmailTemplate::where('table_type','=','sys_resign')->get();
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        // $last_letter_number = DB::table('sys_resign')->orderBy('id','asc')->value('letter_number');
        // if(  $last_letter_number ){
        //     $last_number = explode('/', $last_letter_number);
        // }
        // else{
        //     $last_number=0;
        // }
        
        // if($date=='01'){
        //     $number = 01;
        // } else{
        //     $number = 1 + $last_number[0];
        // }
        $last_letter_number = DB::table('sys_resign')->where('letter_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('letter_number');
        if($last_letter_number==''){
            $number = 1;
        } else{
            $last_number = explode('/', $last_letter_number);
            $number = 1 + $last_number[0];
        }
        $comp = Company::find(\Auth::user()->company);
        return view('admin.resign.resigns', compact('des_id','comp_id','proj_id','emp_id','month','year','number','resigns','employee','company','comp','project','email_template','permcheck'));

    }

    /* getEmployeeCode  Function Start Here */
    public function getEmployeeCode(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee_code = Employee::find($emp_id);
            echo '<option value="' . $employee_code->employee_code . '">' . $employee_code->employee_code . '</option>';
        }
    }
    /* getEmployeeDate  Function Start Here */
    public function getEmployeeDate(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee_doj = Employee::find($emp_id);
            return get_date_format($employee_doj->doj);
        }
    }

    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            // echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->where('status','=','opening')->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project_number . '</option>';
            }
        }
    }

    /* getEmployee Function Start Here */
    public function getEmployee(Request $request)
    {

        $proj_id = $request->proj_id;
        if ($proj_id) {
            echo '<option value="0">Select Employee</option>';
            $employee = Employee::where('project', $proj_id)->where('role_id','!=',1)->where('status','active')->get();
            foreach ($employee as $e) {
                echo '<option value="' . $e->id . '">' . $e->fname . ' ' . $e->lname . '</option>';
            }
        }
    }

    /* deleteResign  Function Start Here */
    public function deleteResign($id)
    {

        $resigns = Resign::find($id);
        if ($resigns) {
            $resigns->delete();

            return redirect('resigns')->with([
                'message' => 'Resign Deleted Successfully'
            ]);
        } else {
            return redirect('resigns')->with([
                'message' => 'Resign Not Found',
                'message_important' => true
            ]);
        }

    }



    /* addResign  Function Start Here */
    public function addResign(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'draft_letter' => 'required',
            
            'date' => 'required',
            'resign_date' => 'required',
            'project_number' => 'required',
            'company_name' => 'required',
            'employee_code' => 'required',
            'employee_name' => 'required',
            'insurance_provider' => 'required',
            'file_resign_letter' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('resigns')->withErrors($v->errors());
        }
        $month = date('m');
        $year = date('Y');
        $type=Input::get('resign_type');
        $tembusan=Input::get('tembusan');
        $draft_letter = Input::get('draft_letter');
        $company="SSS";
        $employee_code = Input::get('employee_code');
        // $employee_code = implode(".", $employee_code);
        $employee_name = Input::get('employee_name');
        $letter_number = substr($employee_code,-4)."/".$type."-".$company."/".get_roman_letters($month)."/".$year;
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $resign_date = Input::get('resign_date');
        $resign_date = get_date_format_inggris($resign_date);
        $project_number = Input::get('project_number');
        $company_name = Input::get('company_name');
        $reason = Input::get('reason');
        $insurance_provider = Input::get('insurance_provider');
        $copies = Input::get('copies');
        
        $file                   = Input::file('file_resign_letter');
        $destinationPath_file = public_path() . '/assets/resign_files/';
        $target_file = $destinationPath_file . basename($_FILES["file_resign_letter"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        if ($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'jpeg' || $imageFileType == 'pdf') {
            $destinationPath        = public_path() . '/assets/resign_files/';
            $file_name              = $file->getClientOriginalName();
            Input::file('file_resign_letter')->move($destinationPath, $file_name);

            //Penomoran id
            $last_number_id = Resign::max('id');
            if($last_number_id==''){
                $number_id = 1;
            } else {
                $number_id = 1 + $last_number_id;
            }

            $resign = new Resign();
            $resign->id = $number_id;
            $resign->draft_letter = $draft_letter;
            $resign->tembusan = $tembusan;

            $resign->letter_number = $letter_number;
            $resign->date = $date;
            $resign->resign_type = $type;
            $resign->resign_date = $resign_date;
            $resign->project_number = $project_number;
            $resign->company_name = $company_name;
            $resign->employee_code = $employee_code;
            $resign->employee_name = $employee_name;
            $resign->reason = $reason;
            $resign->insurance_provider = $insurance_provider;
            $resign->file_resign_letter = $file_name;
            $resign->status = 'draft';
            $resign->save();
        
            $notification = new Notification();
            $notification->id_tag = $resign->id;
            $notification->tag = 'resigns';
            $notification->title = 'Persetujuan Resign';
            $notification->description = 'Persetujuan Resign Pegawai '.$employee_code;
            $notification->show_date = date('Y-m-d');
            $notification->start_date = $resign_date;
            $notification->end_date = $resign_date;
            $notification->route = 'approvals/resigns';
            $notification->save();

            $num_elements = 0;
            
            $sql_data = array();

            while($num_elements<count($copies)) {
                $sql_data[] = array(
                    'letter_number' => $letter_number,
                    'copies' => $copies[$num_elements],
                );
                $num_elements++;
            }

            $copy = Copy::insert($sql_data);

        } else {
            return redirect('resigns')->with([
                'message' => 'Upload an Image or PDF',
                'message_important' => true
            ]);
        }

        if ($resign!='') {
            return redirect('resigns')->with([
                'message' => 'Resign Added Successfully'
            ]);

        } else {
            return redirect('resigns')->with([
                'message' => 'Resign Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateResign  Function Start Here */
    public function updateResign(Request $request)
    {

        $cmd = Input::get('cmd');
        $tembusan = Input::get('tembusan');

        $v = \Validator::make($request->all(), [
            'resign_date' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('resigns')->withErrors($v->errors());
        }
        $resign = Resign::find($cmd);
        $reason = Input::get('reason');
        $insurance_provider = Input::get('insurance_provider');
        $resign_date = Input::get('resign_date');
        $resign_date = get_date_format_inggris($resign_date);
        $date = Input::get('date');
        $date = get_date_format_inggris($date);

        if ($resign) {
            $resign->reason = $reason;
            $resign->insurance_provider = $insurance_provider;
            $resign->resign_date = $resign_date;
            $resign->date = $date;
            $resign->tembusan = $tembusan;

            $resign->status = 'draft';
            $resign->save();
            
            $notification = Notification::where('tag','=','resigns')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->start_date = $resign_date;
                $notification->end_date = $resign_date;
                $notification->save();
            }

            return redirect('resigns')->with([
                'message' => 'Resign Updated Successfully'
            ]);

        } else {
            return redirect('resigns')->with([
                'message' => 'Resign Not Found',
                'message_important' => true
            ]);
        }
    }

    /* setResignStatus  Function Start Here */
    public function setResignStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('resigns')->withErrors($v->fails());
        }

        $resigns  = Resign::find($cmd);
        if($resigns){
            $resigns->status=$request->status;
            $resigns->save();

            return redirect('resigns')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('resigns')->with([
                'message' => 'Resign not found',
                'message_important'=>true
            ]);
        }

    }

    /* viewResign  Function Start Here */
    public function viewResign($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 50)->first();
        $resigns = Resign::find($id);
        $copies = Copy::where('letter_number','=',$resigns->letter_number)->get();
        return view('admin.resign.resign-view', compact('copies','resigns','permcheck'));
    }

    /* ViewPost  Function Start Here */
    public function ViewPost(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'file_acceptance_resign' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('resigns')->withErrors($v->errors());
        }
        $resign = Resign::find($cmd);
        $file = Input::file('file_acceptance_resign');
        $destinationPath_file = public_path() . '/assets/resign_files/';
        $target_file = $destinationPath_file . basename($_FILES["file_acceptance_resign"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        if ($resign) {
            if ($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'jpeg' || $imageFileType == 'pdf') {
                $destinationPath        = public_path() . '/assets/resign_files/';
                $file_name              = $file->getClientOriginalName();
                Input::file('file_acceptance_resign')->move($destinationPath, $file_name);

                $resign->file_acceptance_resign = $file_acceptance_resign;
                $resign->save();
            }

            return redirect('resigns')->with([
                'message' => 'Resign Updated Successfully'
            ]);

        } else {
            return redirect('resigns')->with([
                'message' => 'Resign Not Found',
                'message_important' => true
            ]);
        }
    }

    /* editResign  Function Start Here */
    public function editResign($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 50)->first();
        $emp_id = '';
        $proj_id = '';
        $resigns = Resign::find($id);
        $copies = Copy::where('letter_number','=',$resigns->letter_number)->get();
        // echo "masuk";
        // exit;
        return view('admin.resign.resign-edit', compact('copies','emp_id','proj_id','resigns','permcheck'));
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {
        $reference_letter = ReferenceLetter::where('tag','=','resign')->where('id_tag','=',$id)->first();
        $resigns = Resign::find($id);
        $employee_id = $resigns->employee_name;
        $employment     = EmploymentHistory::where('emp_id','=',$employee_id)->get();
        $contract       = ContractRecipient::where('recipients','=',$employee_id)->get();
        $draft_letter = EmailTemplate::find($resigns->draft_letter);
        $tgl_msk =$resigns->employee_info->doj;
        $company =$resigns->employee_info->project_name->internal_company_name->company;
        $letter_number = $resigns->letter_number;
        $date = get_date_format_indonesia($resigns->date);
        $resign_date = get_date_format_indonesia($resigns->resign_date);
        $project_number = $resigns->project_info->project_number;
        $company_name = $resigns->company_info->company;
        $designation = $resigns->employee_info->designation_name->designation;
        $employee_code = $resigns->employee_code;
        $employee_name = $resigns->employee_info->fname;
        $dol = get_date_format_indonesia($resigns->employee_info->dol);
        $doj = get_date_format_indonesia($resigns->employee_info->doj);
        $address = $resigns->employee_info->per_address;
        $employee = Employee::find($employee_id);
        $reason = $resigns->reason;
        if($resigns->resign_type=='RSG'){
            $type = 'Pengunduran Diri';
        } else if($resigns->resign_type=='PHK'){
            $type = 'Pemutusan Hubungan Kerja';
        } else {
            $type = 'Berakhirnya Kontrak';
        }
        
        $insurance_provider = $resigns->insurance_provider;
        $copy = Copy::where('letter_number','=',$letter_number)->get();

        foreach($copy as $c){
            $copies[] = $c->copies;
        }
        
        $data = array(
            'letter_number' => $letter_number,
            'date' => $date,
            'resign_date' => $resign_date,
            'designation' => $designation,
            'type' => $type,
            'reason' => $reason,
            'dol' => $dol,
            'doj' => $doj,
            'project_number' => $project_number,
            'company_name' => $company_name,
            'employee_code' => $employee_code,
            'employee_name' => $employee_name,
            'address' => $address,
            'insurance_provider' => $insurance_provider,
            'copies' => implode(", ", $copies)
        );

        
        $template = $draft_letter->message;
        
        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.resign.pdf-resign', compact('message','reference_letter','letter_number','employment','contract','internal_company'));

        return $pdf->stream('resign.pdf');
    }

   /* previewPdf  Function Start Here */
   public function previewPdf($id)
   {

        $reference_letter = ReferenceLetter::where('tag','=','resign')->where('id_tag','=',$id)->first();
        $resigns = Resign::find($id);
        $employee_id = $resigns->employee_name;
        $employment     = EmploymentHistory::where('emp_id','=',$employee_id)->get();
        $contract       = ContractRecipient::where('recipients','=',$employee_id)->get();
        $draft_letter = EmailTemplate::find($resigns->draft_letter);
        $tgl_msk =$resigns->employee_info->doj;
        $company =$resigns->employee_info->project_name->internal_company_name->company;
        $letter_number = $resigns->letter_number;
        $date = get_date_format_indonesia($resigns->date);
        $resign_date = get_date_format_indonesia($resigns->resign_date);
        $project_number = $resigns->project_info->project_number;
        $company_name = $resigns->company_info->company;
        $designation = $resigns->employee_info->designation_name->designation;
        $employee_code = $resigns->employee_code;
        $employee_name = $resigns->employee_info->fname;
        $reason = $resigns->reason;
        $dol = get_date_format_indonesia($resigns->employee_info->dol);
        $doj = get_date_format_indonesia($resigns->employee_info->doj);
        $address = $resigns->employee_info->per_address;
        $employee = Employee::find($employee_id);

        if($resigns->resign_type=='RSG'){
            $type = 'Pengunduran Diri';
        } else if($resigns->resign_type=='PHK'){
            $type = 'Pemutusan Hubungan Kerja';
        } else {
            $type = 'Berakhirnya Kontrak';
        }
        
        $insurance_provider = $resigns->insurance_provider;
        $copy = Copy::where('letter_number','=',$letter_number)->get();

        foreach($copy as $c){
            $copies[] = $c->copies;
        }
        
        $data = array(
            'letter_number' => $letter_number,
            'date' => $date,
            'resign_date' => $resign_date,
            'designation' => $designation,
            'type' => $type,
            'reason' => $reason,
            'dol' => $dol,
            'doj' => $doj,
            'project_number' => $project_number,
            'company_name' => $company_name,
            'employee_code' => $employee_code,
            'employee_name' => $employee_name,
            'address' => $address,
            'insurance_provider' => $insurance_provider,
            'copies' => implode(", ", $copies)
        );

        $template = $draft_letter->message;
        
        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.resign.pdf-resign', compact('message','letter_number','reference_letter','employment','contract','resign_date','tgl_msk','address','employee_name','company','resigns','employee'));

        return $pdf->stream($letter_number.'.pdf');

       
   }

   

    
    /* downloadFileResign  Function Start Here */
    public function downloadFileResign($id)
    {
        $file = Resign::find($id)->file_resign_letter;
        return response()->download(public_path('/assets/resign_files/' . $file));
    }

    /* downloadFileAcceptance  Function Start Here */
    public function downloadFileAcceptance($id)
    {
        $file = Resign::find($id)->file_acceptance_resign;
        return response()->download(public_path('/assets/resign_files/' . $file));
    }

    
}

