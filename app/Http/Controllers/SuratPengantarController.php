<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\permission;
use App\SuratPengantar;
use App\SuratPengantarRecipient;
use App\Company;
use App\Jobs;
use App\JobApplicants;
use App\Copy;
use App\Notification;
use App\PayrollTypes;
use App\PayrollComponent;
use App\Project;
use App\ProjectNeeds;
use App\EmailTemplate;
use App\Employee;
use App\EmployeeRolesPermission;
use Illuminate\Support\Facades\Input;
use DB;
use WkPdf;

class SuratPengantarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* surat_pengantar  Function Start Here */
    public function surat_pengantar()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 105)->first();
        $job_id = '';
        $jobs = Jobs::where('status','<>','drafted')->orderBy('id','asc')->get();
        $surat_pengantar = SuratPengantar::orderBy('id','asc')->get();
        $email_template = EmailTemplate::where('table_type','=','sys_surat_pengantar')->get();
        $employee = Employee::where('role_id','!=','1')->get();

        return view('admin.surat-pengantar.surat-pengantar', compact('job_id','jobs','surat_pengantar','employee','email_template','permcheck'));

    }

    /* getEmployee Function Start Here */
    public function getEmployee(Request $request)
    {
        $job = $request->job_id;
        $type = $request->type;
        $jobs =  Jobs::find($job);
        if($type=='INT'){
            $employee = JobApplicants::where('job_id','=',$job)->where('status','=','Interview')->get();
        } 
        if($type=='PENP' || $type=='TRAI'){
            $employee = JobApplicants::where('job_id','=',$job)->where('status','=','Confirm')->get();
        } 
        if($type=='PETK') {
            $employee = Employee::where('designation','=',$jobs->position)->where('status','=','inactive')->get();
        }

        $members_dropdown = array();

        foreach($employee as $emp){
            if($type=='PETK') {
                $members_dropdown[] = array("id" => $emp->id, "text" => $emp->fname.' '.$emp->lname);
            } else {
                $members_dropdown[] = array("id" => $emp->id, "text" => $emp->name);
            }
        }
        return $members_dropdown;
    }

    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $job = $request->job_id;
        if ($job) {
            $jobs = Jobs::find($job);
            echo '<option value="' . $jobs->project . '">' . $jobs->project_name->project . '</option>';
        }
    }

    /* getProject Function Start Here */
    public function getCompany(Request $request)
    {

        $job = $request->job_id;
        if ($job) {
            $jobs = Jobs::find($job);
            echo '<option value="' . $jobs->company . '">' . $jobs->company_name->company . '</option>';
        }
    }


    /* getLocation  Function Start Here */
    public function getLocation(Request $request)
    {
        $job = $request->job_id;
        if ($job) {
            $jobs = Jobs::find($job);
            echo '<option value="' . $jobs->job_location . '">' . $jobs->job_location . '</option>';
        }
    }

    /* getLocation  Function Start Here */
    public function getDesignation(Request $request)
    {
        $job = $request->job_id;
        if ($job) {
            $jobs = Jobs::find($job);
            echo '<option value="' . $jobs->position . '">' . $jobs->position_name->designation . '</option>';
        }
    }

    /* deleteSurat Pengantar  Function Start Here */
    public function deleteSuratPengantar($id)
    {

        $surat_pengantar = SuratPengantar::find($id);
        if ($surat_pengantar) {
            $surat_pengantar->delete();

            return redirect('surat-pengantar')->with([
                'message' => 'Surat Pengantar Deleted Successfully'
            ]);
        } else {
            return redirect('surat-pengantar')->with([
                'message' => 'Surat Pengantar Not Found',
                'message_important' => true
            ]);
        }

    }


    /* addSurat Pengantar  Function Start Here */
    public function addSuratPengantar(Request $request)
    {

        $draft_letter = Input::get('draft_letter');
        $job = Input::get('job');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $project = Input::get('project');
        $company = Input::get('company');
        $designation = Input::get('designation');
        $location = Input::get('location');
        $surat_pengantar_types = Input::get('surat_pengantar_types');
        $share_with = Input::get('share_with');
        $share_with_specific = Input::get('share_with_specific');
        //Penomoran id
        $last_number_id = SuratPengantar::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $surat_pengantar = new SuratPengantar();
        $surat_pengantar->id = $number_id;
        $surat_pengantar->draft_letter = $draft_letter;
        $surat_pengantar->date = $date;
        $surat_pengantar->surat_pengantar_types = $surat_pengantar_types;
        $surat_pengantar->job = $job;
        $surat_pengantar->project = $project;
        $surat_pengantar->company = $company;
        $surat_pengantar->designation = $designation;
        $surat_pengantar->location = $location;
        $surat_pengantar->share_with = $share_with;
        $surat_pengantar->status = 'draft';
        $surat_pengantar->save();

        $surat_pengantar_id = $surat_pengantar->id;

        $jobs = Jobs::find($job);
        
        $notification = new Notification();
        $notification->id_tag = $surat_pengantar_id;
        $notification->tag = 'surat_pengantar';
        $notification->title = 'Persetujuan Surat Pengantar';
        $notification->description = 'Persetujuan Surat Pengantar Lowongan Pekerjaan '.$jobs->no_position;
        $notification->show_date = date('Y-m-d');
        $notification->start_date = $date;
        $notification->end_date = $date;
        $notification->route = 'approvals/surat_pengantar';
        $notification->save();

        $perusahaan = '';
        if($share_with == 'all'){
            $jobs = Jobs::find($job);
            if($surat_pengantar_types=='INT'){
                $recipients = JobApplicants::where('job_id','=',$job)->where('status','=','Interview')->get()->toArray();
            } 
            if($surat_pengantar_types=='PENP' || $surat_pengantar_types=='TRAI'){
                $data = [
                    '*',
                ];
                $recipients = JobApplicants::select($data)
                ->leftJoin('sys_employee as employee', 'sys_job_applicants.ktp', '=', 'employee.no_ktp')
                ->where('sys_job_applicants.job_id','=',$job)->where('sys_job_applicants.status','=','Confirm')
                ->get()->toArray();
            } 
            if($surat_pengantar_types=='PETK') {
                $recipients = Employee::where('designation','=',$jobs->position)->where('status','=','inactive')->get()->toArray();
            }
    
            $num_elements = 0;
            
            $sql_data = array();
            //Penomoran surat
            $date = date('d');
            $month = date('m');
            $year = date('Y');
            // $last_letter_number = DB::table('sys_surat_pengantar_recipients')->where('letter_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('letter_number');
            // if($last_letter_number==''){
            //     $number = 1;
            // } else{
            //     $last_number = explode('/', $last_letter_number);
            //     $number = 1 + $last_number[0];
            // }
    
            $perusahaan = 'SSS';
            while($num_elements<count($recipients)) {
                // $employee_code = implode(".", $recipients[$num_elements]['employee_code']);
                $sql_data[] = array(
                    'letter_number' => substr($recipients[$num_elements]['employee_code'],-4)."/".$surat_pengantar_types."-".$perusahaan."/".get_roman_letters($month)."/".$year,
                    'surat_pengantar' => $surat_pengantar_id,
                    'recipients' => $recipients[$num_elements]['id'],
                );
                // $number++;
                $num_elements++;
            }
    
            $surat_pengantar_recipient = SuratPengantarRecipient::insert($sql_data);

        } else if($share_with == 'specific'){
            $recipients = explode(",", $share_with_specific);

            $num_elements = 0;
            
            $sql_data = array();
            //Penomoran surat
            $date = date('d');
            $month = date('m');
            $year = date('Y');
            // $last_letter_number = DB::table('sys_surat_pengantar_recipients')->orderBy('id','asc')->value('letter_number');
            // if($last_letter_number){
            //     $last_number = explode('/', $last_letter_number);
            // }
            // else{
            //     $last_number=0;
            // }
            // $last_letter_number = DB::table('sys_surat_pengantar_recipients')->where('letter_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('letter_number');
            // if($last_letter_number==''){
            //     $number = 1;
            // } else{
            //     $last_number = explode('/', $last_letter_number);
            //     $number = 1 + $last_number[0];
            // }
    
            // if($date=='01'){
            //     $number = 01;
            // } else{
            //     $number = 1 + $last_number[0];
            // }
            $perusahaan='SSS';
            while($num_elements<count($recipients)) {
                $jobs = Jobs::find($job);
                if($surat_pengantar_types=='INT'){
                    $emp_code = JobApplicants::where('job_id','=',$job)->where('id','=',$recipients[$num_elements])->where('status','=','Interview')->first();
                } 
                if($surat_pengantar_types=='PENP' || $surat_pengantar_types=='TRAI'){
                    $data = [
                        '*',
                    ];
                    $emp_code = JobApplicants::select($data)
                    ->leftJoin('sys_employee as employee', 'sys_job_applicants.ktp', '=', 'employee.no_ktp')
                    ->where('sys_job_applicants.job_id','=',$job)
                    ->where('sys_job_applicants.id','=',$recipients[$num_elements])
                    ->where('sys_job_applicants.status','=','Confirm')
                    ->first();
                } 
                if($surat_pengantar_types=='PETK') {
                    $emp_code = Employee::find($recipients[$num_elements]);
                }
                // $employee_code = implode(".", $emp_code->employee_code);
                $sql_data[] = array(
                    'letter_number' => substr($emp_code->employee_code,-4)."/".$surat_pengantar_types."-".$perusahaan."/".get_roman_letters($month)."/".$year,
                    'surat_pengantar' => $surat_pengantar_id,
                    'recipients' => $emp_code->id,
                );
                // $number++;
                $num_elements++;
            }
    
            $surat_pengantar_recipient = SuratPengantarRecipient::insert($sql_data);
        }


        if ($surat_pengantar!='') {
            return redirect('surat-pengantar')->with([
                'message' => 'Surat Pengantar Added Successfully'
            ]);

        } else {
            return redirect('surat-pengantar')->with([
                'message' => 'Surat Pengantar Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateSurat Pengantar  Function Start Here */
    public function updateSuratPengantar(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'draft_letter' => 'required',
        ]);

        if ($v->fails()) {
            return redirect('surat-pengantar')->withErrors($v->errors());
        }
        $surat_pengantar = SuratPengantar::find($cmd);
        $draft_letter = Input::get('draft_letter');

        if ($surat_pengantar) {
            $surat_pengantar->draft_letter = $draft_letter;
            $surat_pengantar->status = 'draft';
            $surat_pengantar->save();
            
            $notification = Notification::where('tag','=','surat_pengantar')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->save();
            }

            return redirect('surat-pengantar')->with([
                'message' => 'Surat Pengantar Updated Successfully'
            ]);

        } else {
            return redirect('surat-pengantar')->with([
                'message' => 'Surat Pengantar Not Found',
                'message_important' => true
            ]);
        }
    }

    /* setSuratPengantarStatus  Function Start Here */
    public function setSuratPengantarStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('surat-pengantar')->withErrors($v->fails());
        }

        $surat_pengantar  = SuratPengantar::find($cmd);
        if($surat_pengantar){
            $surat_pengantar->status=$request->status;
            $surat_pengantar->save();

            return redirect('surat-pengantar')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('surat-pengantar')->with([
                'message' => 'Surat Pengantar not found',
                'message_important'=>true
            ]);
        }

    }

    /* viewSurat Pengantar  Function Start Here */
    public function viewSuratPengantar($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 105)->first();
        $surat_pengantar = SuratPengantar::find($id);
        $recipients = SuratPengantarRecipient::where('surat_pengantar','=',$id)->get();
        $email_template = EmailTemplate::where('table_type','=','sys_surat_pengantar')->get();
        return view('admin.surat-pengantar.surat-pengantar-view', compact('email_template','recipients','surat_pengantar','permcheck'));
    }

    /* editSurat Pengantar  Function Start Here */
    public function editSuratPengantar($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 105)->first();
        $surat_pengantar = SuratPengantar::find($id);
        $recipients = SuratPengantarRecipient::where('surat_pengantar','=',$id)->get();
        $email_template = EmailTemplate::where('table_type','=','sys_surat_pengantar')->get();
        return view('admin.surat-pengantar.surat-pengantar-edit', compact('email_template','recipients','surat_pengantar','permcheck'));
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {
        $recipients = SuratPengantarRecipient::find($id);
        // echo "masuk"; exit;
        // echo $recipients->surat_pengantar_info->desgination_info->designation; exit;
        $draft_letter = EmailTemplate::find($recipients->surat_pengantar_info->draft_letter);

        $letter_number = $recipients->letter_number;
        $date = get_date_format_indonesia($recipients->surat_pengantar_info->date);
        $project_number = $recipients->surat_pengantar_info->project_info->project_number;
        $project_name = $recipients->surat_pengantar_info->project_info->project;
        $company_name = $recipients->surat_pengantar_info->company_info->company;
        $employee_name = $recipients->employee_info->fname;
        $company_address = $recipients->surat_pengantar_info->company_info->address;
        $pic = $recipients->surat_pengantar_info->company_info->pic.'<br>'.$recipients->surat_pengantar_info->company_info->pic_jabatan;
        $address = $recipients->employee_info->per_address;
        if($recipients->employee_info->gender=='male'){
            $gender = "Laki-laki";
        } else {
            $gender = "Wanita";
        }
        $last_education = $recipients->employee_info->last_education_info->education;
        $ktp = $recipients->employee_info->no_ktp;
        $phone = $recipients->employee_info->phone;
        $job = $recipients->surat_pengantar_info->designation_info->designation;
        $date_of_birth = $recipients->employee_info->birth_place . ", " .  $recipients->employee_info->dob;
        $salary = number_format($recipients->employee_info->salary,0);
        $attendance_allowance = $recipients->employee_info->attendance_allowance;
        if($recipients->surat_pengantar_info->surat_pengantar_types=='PENP'){
            $types = 'Penempatan Kerja';
        } else if($recipients->surat_pengantar_info->surat_pengantar_types=='TRAI'){
            $types = 'Training';
        } else if($recipients->surat_pengantar_info->surat_pengantar_types=='PETK'){
            $types = 'Pengganti Tenaga Kerja';
        }

        $data = array(
            'letter_number' => $letter_number,
            'day' => get_date_day($date),
            'date' => $date,
            'project_number' => $project_number,
            'project_name' => $project_name,
            'company_name' => $company_name,
            'employee_name' => $employee_name,
            'company_address' => $company_address,
            'pic' => $pic,
            'types' => $types,
            'address' => $address,
            'gender' => $gender,
            'last_education' => $last_education,
            'ktp' => $ktp,
            'phone' => $phone,
            'job' => $job,
            'date_of_birth' => $date_of_birth,
            'salary' => $salary,
            'attendance_allowance' => $attendance_allowance
        );

        $template = $draft_letter->message;
        
        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.surat-pengantar.pdf-surat-pengantar', compact('message','letter_number'));

        return $pdf->stream($letter_number.'.pdf');
    }

    /* downloadAllPdf  Function Start Here */
    public function downloadAllPdf($id)
    {
        $surat_pengantar = SuratPengantar::find($id);
        $recipients = SuratPengantarRecipient::where('surat_pengantar','=', $id)->get();
        $draft_letter = EmailTemplate::find($surat_pengantar->draft_letter);
        $template = $draft_letter->message;

        $num_elements = 0;
        
        foreach($recipients as $rec){
            $letter_number[] = $rec->letter_number;
            $date[] = get_date_format_indonesia($rec->surat_pengantar_info->date);
            $project_number[] = $rec->project_info->project_number;
            $project_name[] = $rec->project_info->project;
            $company_name[] = $rec->surat_pengantar_info->company_info->company;
            $employee_name[] = $rec->employee_info->fname;
            $address[] = $rec->employee_info->per_address;
            $company_address[] = $rec->surat_pengantar_info->company_info->address;
            $pic[] = $rec->surat_pengantar_info->company_info->pic.'<br>'.$rec->surat_pengantar_info->company_info->pic_jabatan;
            $last_education[] = $rec->employee_info->last_education_info->education;
            if($rec->employee_info->gender=='male'){
                $gender[] = "Laki-laki";
            } else {
                $gender[] = "Wanita";
            }
            $ktp[] = $rec->employee_info->no_ktp;
            $phone[] = $rec->employee_info->phone;
            $job[] = $rec->surat_pengantar_info->designation_info->designation;
            $date_of_birth[] = $rec->employee_info->birth_place . ", " .  $rec->employee_info->dob;
            $salary[] = number_format($rec->employee_info->salary,0);
            $attendance_allowance[] = $rec->employee_info->attendance_allowance;
            if($rec->surat_pengantar_info->surat_pengantar_types=='PENP'){
                $types[] = 'Penempatan Kerja';
            } else if($rec->surat_pengantar_info->surat_pengantar_types=='TRAI'){
                $types[] = 'Training';
            } else if($rec->surat_pengantar_info->surat_pengantar_types=='PETK'){
                $types[] = 'Pengganti Tenaga Kerja';
            }
        }

        while($num_elements<count($recipients)) {
            $data[$num_elements] = array(
                'letter_number' => $letter_number[$num_elements],
                'day' => get_date_day($date[$num_elements]),
                'date' => $date[$num_elements],
                'project_number' => $project_number[$num_elements],
                'project_name' => $project_name[$num_elements],
                'company_name' => $company_name[$num_elements],
                'employee_name' => $employee_name[$num_elements],
                'address' => $address[$num_elements],
                'company_address' => $company_address[$num_elements],
                'pic' => $pic[$num_elements],
                'types' => $types[$num_elements],
                'gender' => $gender[$num_elements],
                'last_education' => $last_education[$num_elements],
                'ktp' => $ktp[$num_elements],
                'phone' => $phone[$num_elements],
                'job' => $job[$num_elements],
                'date_of_birth' => $date_of_birth[$num_elements],
                'salary' => $salary[$num_elements],
                'attendance_allowance' => $attendance_allowance[$num_elements]
            );
            $message[$num_elements] = array(
                'message' => _render($template, $data[$num_elements]));
            $num_elements++;
        }        

        $pdf=WkPdf::loadView('admin.surat-pengantar.pdf-all-surat-pengantar', compact('message','letter_number'));

        return $pdf->stream('surat_pengantar.pdf');
    }
}