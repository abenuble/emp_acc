<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\permission;
use App\Warning;
use App\Company;
use App\Notification;
use App\Copy;
use App\Project;
use App\ProjectNeeds;
use App\EmailTemplate;
use App\Employee;
use App\EmployeeRolesPermission;
use Illuminate\Support\Facades\Input;
use DB;
use WkPdf;

class WarningController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* contract  Function Start Here */
    public function warnings()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 49)->first();
        $comp_id = '';
        $proj_id = '';
        $emp_id = '';
        $des_id = '';


        $warnings = Warning::orderBy('id','asc')->get();
        $company = Company::all();
        $employee = Employee::where('role_id','!=','1')->get();
        $project = Project::all();
        $email_template = EmailTemplate::where('table_type','=','sys_warning')->get();
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        // $last_letter_number = DB::table('sys_warning')->orderBy('id','asc')->value('letter_number');
        // if(  $last_letter_number){
        //     $last_number = explode('/', $last_letter_number);
        // }
        //     else{
        //         $last_number=0;
        //     }
       
        // if($date=='01'){
        //     $number = 01;
        // } else{
        //     $number = 1 + $last_number[0];
        // }
        $last_letter_number = DB::table('sys_warning')->where('letter_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('letter_number');
        if($last_letter_number==''){
            $number = 1;
        } else{
            $last_number = explode('/', $last_letter_number);
            $number = 1 + $last_number[0];
        }

        $comp = Company::find(\Auth::user()->company);
        return view('admin.warning.warnings', compact('des_id','comp_id','proj_id','emp_id','month','year','number','warnings','employee','company','comp','project','email_template','permcheck'));

    }

    /* getEmployeeCode  Function Start Here */
    public function getEmployeeCode(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee_code = Employee::where('id', $emp_id)->where('status','=','active')->get();
            foreach ($employee_code as $e) {
                echo '<option value="' . $e->employee_code . '">' . $e->employee_code . '</option>';
            }
        }
    }

    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            // echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->where('status','=','opening')->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project_number . '</option>';
            }
        }
    }

    /* getEmployee Function Start Here */
    public function getEmployee(Request $request)
    {

        $proj_id = $request->proj_id;
        if ($proj_id) {
            echo '<option value="0">Select Employee</option>';
            $employee = Employee::where('project', $proj_id)->where('status','=','active')->where('role_id','!=',1)->get();
            foreach ($employee as $e) {
                echo '<option value="' . $e->id . '">' . $e->fname . ' ' . $e->lname . '</option>';
            }
        }
    }
    public function getEmployeeWarningType(Request $request)
    {
        $type = array('SP 1', 'SP 2', 'SP 3');
        $proj_id = $request->emp_id;
        if ($proj_id) {
            $date=date('Y-m-d');
            $employee = Warning::where('employee_name', $proj_id)->orderBy('id',"DESC")->first();
          if($employee){
             
                  if($employee->end_date >= $date){
                    if($employee->type=="SP 1"){
                        unset($type[0]);
                    }
                    if($employee->type=="SP 2"){
                        unset($type[1]);
                        unset($type[0]);

                    }
                    if($employee->type=="SP 3"){
                        $type=array();
                    }
                    foreach ($type as $e) {
                        echo '<option value="' . $e . '">' . $e . '</option>';
                    }
                }
                else{
                
                    echo '<option value="SP 1">SP 1</option>';
                    echo '<option value="SP 2">SP 2</option>';
                    echo '<option value="SP 3">SP 3</option>';
    
                }
              
          
          }
            else{
                echo '<option value="SP 1">SP 1</option>';
                echo '<option value="SP 2">SP 2</option>';
                echo '<option value="SP 3">SP 3</option>';

            }
            
        }
    }
    /* deleteWarning  Function Start Here */
    public function deleteWarning($id)
    {

        $warnings = Warning::find($id);
        if ($warnings) {
            $warnings->delete();

            return redirect('warnings')->with([
                'message' => 'Warning Deleted Successfully'
            ]);
        } else {
            return redirect('warnings')->with([
                'message' => 'Warning Not Found',
                'message_important' => true
            ]);
        }

    }


    /* addWarning  Function Start Here */
    public function addWarning(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'draft_letter' => 'required',
            'date' => 'required',
            'type' => 'required',
            'project_number' => 'required',
            'company_name' => 'required',
            'employee_code' => 'required',
            'employee_name' => 'required',
            'official_report_number' => 'required',
            'official_report_date' => 'required',
            'violate' => 'required',
            'effective_date' => 'required',
            'warning_information' => 'required',
            'end_date' => 'required',
            'type' =>'required|not_in:0'
        ]);

        if ($v->fails()) {
            return redirect('warnings')->withErrors($v->errors());
        }
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        // $last_letter_number = DB::table('sys_warning')->orderBy('id','asc')->value('letter_number');
        // if(  $last_letter_number){
        //     $last_number = explode('/', $last_letter_number);
        // }
        //     else{
        //         $last_number=0;
        //     }
       
        // if($date=='01'){
        //     $number = 01;
        // } else{
        //     $number = 1 + $last_number[0];
        // }
        $last_letter_number = DB::table('sys_warning')->where('letter_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('letter_number');
        if($last_letter_number==''){
            $number = 1;
        } else{
            $last_number = explode('/', $last_letter_number);
            $number = 1 + $last_number[0];
        }
            $company="SSS";
            $type = Input::get('type');
            $typeletter='';
            if($type=='SP 1'){
                $typeletter="SPI";
            }
            if($type=='SP 2'){
                $typeletter="SPII";
            }
            if($type=='SP 3'){
                $typeletter="SPIII";
            }

        $company_name = Input::get('company_name');
        $employee_code = Input::get('employee_code');
        // $employee_code = implode(".", $employee_code);
        $employee_name = Input::get('employee_name');
        $draft_letter = Input::get('draft_letter');
        $letter_number = substr($employee_code,-4)."/".str_replace(' ', '', $typeletter)."-".$company."/".get_roman_letters($month)."/".$year;
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $official_report_date = Input::get('official_report_date');
        $official_report_date = get_date_format_inggris($official_report_date);
        $effective_date = Input::get('effective_date');
        $effective_date = get_date_format_inggris($effective_date);
        $end_date = Input::get('end_date');
        $end_date = get_date_format_inggris($end_date);
        $project_number = Input::get('project_number');
        $lokasi = Input::get('lokasi');

        $official_report_number = Input::get('official_report_number');
        $violate = Input::get('violate');
        $warning_information = Input::get('warning_information');
        $copies = Input::get('copies');
        //Penomoran id
        $last_number_id = Warning::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $warning = new Warning();
        $warning->id = $number_id;
        $warning->lokasi = $lokasi;
        $warning->draft_letter = $draft_letter;
        $warning->letter_number = $letter_number;
        $warning->date = $date;
        $warning->type = $type;
        $warning->project_number = $project_number;
        $warning->company_name = $company_name;
        $warning->employee_code = $employee_code;
        $warning->employee_name = $employee_name;
        $warning->official_report_number = $official_report_number;
        $warning->official_report_date = $official_report_date;
        $warning->violate = $violate;
        $warning->effective_date = $effective_date;
        $warning->end_date = $end_date;
        $warning->warning_information = $warning_information;
        $warning->status = 'draft';
        $warning->save();
        
        $notification = new Notification();
        $notification->id_tag = $warning->id;
        $notification->tag = 'warnings';
        $notification->title = 'Persetujuan SP';
        $notification->description = 'Persetujuan SP Pegawai '.$employee_code;
        $notification->show_date = date('Y-m-d');
        $notification->start_date = $effective_date;
        $notification->end_date = $effective_date;
        $notification->route = 'approvals/warnings';
        $notification->save();

        $num_elements = 0;
        
        $sql_data = array();

        while($num_elements<count($copies)) {
            $sql_data[] = array(
                'letter_number' => $letter_number,
                'copies' => $copies[$num_elements],
            );
            $num_elements++;
        }

        $copy = Copy::insert($sql_data);

        if ($warning!='') {
            return redirect('warnings')->with([
                'message' => 'Warning Added Successfully'
            ]);

        } else {
            return redirect('warnings')->with([
                'message' => 'Warning Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateWarning  Function Start Here */
    public function updateWarning(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
           
            'date' => 'required',
           
           
          
            'official_report_number' => 'required',
            'official_report_date' => 'required',
            'violate' => 'required',
            'effective_date' => 'required',
            'warning_information' => 'required',
            'end_date' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('warnings')->withErrors($v->errors());
        }
        $warning = Warning::find($cmd);
        $waring_type = Input::get('type');
        $warning_date = Input::get('date');
        $warning_date = get_date_format_inggris($warning_date);
        $official_report_number = Input::get('official_report_number');
        $official_report_date = Input::get('official_report_date');
        $official_report_date = get_date_format_inggris($official_report_date);
        $violate = Input::get('violate');
        $effective_date = Input::get('effective_date');
        $effective_date = get_date_format_inggris($effective_date);
        $end_date = Input::get('end_date');
        $end_date = get_date_format_inggris($end_date);
      
        $warning_information = Input::get('warning_information');

        if ($warning) {
            $warning->date = $warning_date;
            $warning->type=$warning_type;
            $warning->official_report_number = $official_report_number;
            $warning->official_report_date = $official_report_date;
            $warning->violate = $violate;
            $warning->effective_date = $effective_date;
            $warning->end_date = $end_date;
            $warning->warning_information = $warning_information;
            $warning->save();
            
            $notification = Notification::where('tag','=','warnings')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->end_date = $effective_date;
                $notification->save();
            }

            return redirect('warnings')->with([
                'message' => 'Warning Updated Successfully'
            ]);

        } else {
            return redirect('warnings')->with([
                'message' => 'Warning Not Found',
                'message_important' => true
            ]);
        }
    }

    /* setWarningStatus  Function Start Here */
    public function setWarningStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('warnings')->withErrors($v->fails());
        }

        $warnings  = Warning::find($cmd);
        if($warnings){
            $warnings->status=$request->status;
            $warnings->save();

            return redirect('warnings')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('warnings')->with([
                'message' => 'Warning not found',
                'message_important'=>true
            ]);
        }

    }

    /* viewWarning  Function Start Here */
    public function viewWarning($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 49)->first();
        $warnings = Warning::find($id);
        $copies = Copy::where('letter_number','=',$warnings->letter_number)->get();
        return view('admin.warning.warning-view', compact('copies','warnings','permcheck'));
    }

    /* editWarning  Function Start Here */
    public function editWarning($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 49)->first();
        $emp_id = '';
        $proj_id = '';
        $warnings = Warning::find($id);
          $type = array('SP 1', 'SP 2', 'SP 3');
        $proid = $warnings->project_info->id;
        if ($proid) {
            $date=date('Y-m-d');
            $employee = Warning::where('employee_name', $proj_id)->orderBy('id',"DESC")->first();
          if($employee){
             
                  if($employee->end_date >= $date){
                    if($employee->type=="SP 1"){
                        unset($type[0]);
                    }
                    if($employee->type=="SP 2"){
                        unset($type[1]);
                        unset($type[0]);

                    }
                    if($employee->type=="SP 3"){
                        $type=array();
                    }
                }
              
          
          }          
        }
        $copies = Copy::where('letter_number','=',$warnings->letter_number)->get();
        return view('admin.warning.warning-edit', compact('copies','emp_id','proj_id','warnings','type'));
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {
        $warnings = Warning::find($id);
        $draft_letter = EmailTemplate::find($warnings->draft_letter);

        $letter_number = $warnings->letter_number;
        $date = get_date_format_indonesia($warnings->date);
        $project_number = $warnings->project_info->project_number;
        $company_name = $warnings->company_info->company;
        $employee_code = $warnings->employee_code;
        $location = $warnings->lokasi;
        $employee_name = $warnings->employee_info->fname . " " . $warnings->employee_info->lname;
        $designation_name = $warnings->employee_info->designation_name->designation;
        if($warnings->type == "SP 1"){
            $type = '1 (Satu)';
        } else if($warnings->type == "SP 2"){
            $type = '2 (Dua)';
        } else {
            $type = '3 (Tiga)';
        }
        $official_report_number = $warnings->official_report_number;
        $official_report_date = $warnings->official_report_date;
        $violate = $warnings->violate;
        $effective_date = get_date_format_indonesia($warnings->effective_date);
        $end_date = get_date_format_indonesia($warnings->end_date);
        $warning_information = $warnings->warning_information;
        $copy = Copy::where('letter_number','=',$letter_number)->get();
        $copies = array();
        foreach($copy as $c){
            $copies[] = $c->copies;
        }
        $duration = 0;
 
        $ts1 = strtotime($effective_date);
        $ts2 = strtotime($end_date);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $duration = (($year2 - $year1) * 12) + ($month2 - $month1);
        
        $data = array(
            'letter_number' => $letter_number,
            'date' => $date,
            'type' => $type,
            'project_number' => $project_number,
            'company_name' => $company_name,
            'employee_code' => $employee_code,
            'employee_name' => $employee_name,
            'designation' => $designation_name,
            'duration' => $duration,
            'official_report_number' => $official_report_number,
            'official_report_date' => $official_report_date,
            'violate' => $violate,
            'effective_date' => $effective_date,
            'end_date' => $end_date,
            'warning_information' => $warning_information,
            'copies' => implode(", ", $copies),
            'location'=>$location
        );

        $template = $draft_letter->message;
        
        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.warning.pdf-warning', compact('message','letter_number'));

        return $pdf->stream($letter_number.'.pdf');
    }
}
