<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\AccountingBkp;
use App\AccountingBkpDetail;
use App\Bank;
use App\Cash;
use App\Coa;
use App\Company;
use App\Component;
use App\Notification;
use App\Payment;
use App\PaymentComponent;
use App\Procurement;
use App\Project;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;

class AccountingBkpController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* contract  Function Start Here */
    public function bkp()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 74)->first();
        $bkp = AccountingBkp::orderBy('id','asc')->get();
        return view('admin.bkp.bkp', compact('bkp','permcheck'));

    }


    /* getCompany Function Start Here */
    public function getCompany()
    {
        echo '<option value="0">Select Company</option>';
        $company = Company::where('id','!=','1')->where('id','!=','2')->get();
        foreach ($company as $d) {
            echo '<option value="' . $d->id . '">' . $d->company . '</option>';
        }
    }

    /* getCoa Function Start Here */
    public function getCoa()
    {
        echo '<option value="0">Select COA</option>';
        $coa = Coa::all();
        foreach ($coa as $d) {
            echo '<option value="' . $d->id . '">(' . $d->coa_code . ') ' . $d->coa . '</option>';
        }
    }

    /* getBank Function Start Here */
    public function getBank()
    {
        echo '<option value="0">Select Bank</option>';
        $bank = Bank::all();
        foreach ($bank as $d) {
            echo '<option value="' . $d->id . '">' . $d->bank . ' (' . $d->branch . '-' . $d->company_info->company . '-' . $d->account_number . ')</option>';
        }
    }

    /* getCash Function Start Here */
    public function getCash()
    {
        echo '<option value="0">Select Cash</option>';
        $cash = Cash::all();
        foreach ($cash as $d) {
            echo '<option value="' . $d->id . '">' . $d->cash . ' (' . $d->company_info->company . ')</option>';
        }
    }

    public function getProcurement(Request $request)
    {
        $id = $request->id;
        // $path = base_path();
        // echo $path; exit;
        if($id){
            echo '<option value="0">Select Procurement</option>';
            $procurement = Procurement::where('project','=',$id)->where('status','=','approved')->get();
            foreach ($procurement as $d) {
                echo '<option value="' . $d->id . '">' . $d->procurement_number . '</option>';
            }
            $procurement = Procurement::where('project','=',$id)->where('status','=','approved')->where('realization_differences','<','0')->get();
            foreach ($procurement as $d) {
                echo '<option value="' . $d->id . '">' . $d->realization_number . '</option>';
            }
        }
    }

    /* getDetailProcurement Function Start Here */
    public function getDetailProcurement(Request $request)
    {
        $id = $request->id;
        $procurement = Procurement::find($id);

        return $procurement;
    }

    /* getPayment Function Start Here */
    public function getPayment()
    {
        echo '<option value="0">Select Payment</option>';
        $payment = Component::where('status','=','active')->get();
        foreach ($payment as $d) {
            echo '<option value="' . str_replace(' ', '_', $d->component) . '">' . $d->component . '</option>';
        }
    }

    /* getDetailPayment Function Start Here */
    public function getDetailPayment(Request $request)
    {
        $component = $request->component;
        $id = $request->id;
        $month = $request->month;
        $month= get_date_format_inggris_bulan($month);
        $payment = Payment::where('project','=',$id)->where('month','=',$month)->where('status','=','approved')->first();
        if($payment){
            $payment_component = PaymentComponent::where('payment','=',$payment->id)->where('month','=',$month)->sum($component);
        } else {
            $payment_component = 0;
        }

        return $payment_component;
    }

    /* getCoaComponent Function Start Here */
    public function getCoaComponent(Request $request)
    {
        $component = str_replace('_', ' ', $request->component);
        $coa_component = Component::where('component','=',$component)->first();
        $coa = Coa::all();
        $selected = '';
        foreach ($coa as $d) {
            if($d->id == $coa_component->coa){
                $selected = 'selected';
            } else {
                $selected = '';
            }
            echo '<option value="' . $d->id . '" '.$selected.'>(' . $d->coa_code . ') ' . $d->coa . '</option>';
        }
    }

    /* addBkp  Function Start Here */
    public function addBkp()
    {
        $bkp = AccountingBkp::all();
        $project = Project::all();
        $coa = Coa::all();
        $procurement = Procurement::all();
        $company = Company::where('id','!=','1')->where('id','!=','2')->get();
        return view('admin.bkp.add-bkp', compact('bkp','project','coa','procurement','company'));

    }

    /* addPostBkp  Function Start Here */
    public function addPostBkp(Request $request)
    {
        $bkp_code = $this->get_kode_transaksi();
        $project = Input::get('project');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $attachment = Input::get('attachment');
        $payment_to = Input::get('payment_to');
        $paid_to = Input::get('paid_to');

        $coa = Input::get('m_coa_id');
        $procurement_type = Input::get('bkpdet_type');
        // echo "<pre>"; print_r($coa); print_r($procurement_type); exit;
        $procurement = Input::get('bkpdet_detail');
        $deficiency = Input::get('bkpdet_kekurangan');
        $payment = Input::get('bkpdet_jumlah');

        $note = Input::get('note');
        $nomor_giro = Input::get('nomor_giro');

        $voucher_payment = Input::get('bkp_voucher');
        $discount_payment = Input::get('bkp_discount');
        $paid_amount = Input::get('bkp_jumlah_bayar');

        $expenditure_type = Input::get('bkp_pengeluaran');
        $expenditure_id = Input::get('pengeluaran_id');
        $expenditure_nominal = Input::get('bkp_pengeluaran_nominal');
        $giro = Input::get('bkp_giro_nominal');
        $giro_ref = Input::get('bkp_giro_refrensi_id');
        $tanggal_giro=Input::get('tanggal_giro');
        $tanggal_giro= get_date_format_inggris($tanggal_giro);
        $keterangan_giro=Input::get('keterangan_giro');
        //Penomoran id
        $last_number_id = AccountingBkp::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $bkp = new AccountingBkp();
        $bkp->nomor_giro = $nomor_giro;

        $bkp->id = $number_id;
        $bkp->bkp_code = $bkp_code;
        $bkp->project = $project;
        $bkp->date = $date;
        $bkp->attachment = $attachment;
        $bkp->payment_to = $payment_to;
        $bkp->paid_to = $paid_to;
        $bkp->note = $note;
        $bkp->voucher_payment = str_replace(",","",$voucher_payment);
        $bkp->discount_payment = str_replace(",","",$discount_payment);
        $bkp->paid_amount = str_replace(",","",$paid_amount);
        $bkp->expenditure_type = $expenditure_type;
        $bkp->expenditure_id = $expenditure_id;
        $bkp->expenditure_nominal = str_replace(",","",$expenditure_nominal);
        $bkp->giro = $giro;
        $bkp->giro_ref = $giro_ref; 
        $bkp->tanggal_giro=$tanggal_giro;
        $bkp->keterangan_giro=$keterangan_giro;
        $bkp->proof_status = '1';   
        $bkp->save();

        $bkp_id = $bkp->id;
        
        $notification = new Notification();
        $notification->id_tag = $bkp_id;
        $notification->tag = 'bkp';
        $notification->title = 'Verifikasi Bukti Kas Pengeluaran';
        $notification->description = 'Verifikasi Bukti Kas Pengeluaran '.$bkp_code;
        $notification->show_date = date('Y-m-d');
        $notification->start_date = $date;
        $notification->end_date = $date;
        $notification->route = 'accountings/bkp';
        $notification->save();

        for($a = 0; $a < sizeof($coa);$a++){
            //Penomoran id
            $last_number_id_det = AccountingBkpDetail::max('id');
            if($last_number_id_det==''){
                $number_id_det = 1;
            } else {
                $number_id_det = 1 + $last_number_id_det;
            }
            $bkpdet = new AccountingBkpDetail();
            $bkpdet->id = $number_id_det;
            $bkpdet->bkp = $bkp_id;
            $bkpdet->coa = $coa[$a];
            $bkpdet->procurement_type = $procurement_type[$a];
            $bkpdet->procurement = $procurement[$a];
            $bkpdet->deficiency = str_replace(",","",$deficiency[$a]);
            $bkpdet->payment = str_replace(",","",$payment[$a]);
            $bkpdet->save();
        }

        if ($bkp!='') {
            return redirect('accountings/bkp')->with([
                'message' => language_data('Proof of cash payment Added Successfully')
            ]);

        } else {
            return redirect('accountings/bkp')->with([
                'message' => language_data('Proof of cash payment Already Exist'),
                'message_important' => true
            ]);
        }

        


    }


    /* updateContract  Function Start Here */
    public function updateBkp(Request $request)
    {

        $cmd = Input::get('cmd');
        
        $bkp = AccountingBkp::find($cmd);

        $coa = Input::get('m_coa_id');
        $procurement_type = Input::get('bkpdet_typecheck2');
        $procurement = Input::get('bkpdet_detail');
        $deficiency = Input::get('bkpdet_kekurangan');
        $payment = Input::get('bkpdet_jumlah');
        $nomor_giro = Input::get('nomor_giro');

        $note = Input::get('note');

        $voucher_payment = Input::get('bkp_voucher');
        $discount_payment = Input::get('bkp_discount');
        $paid_amount = Input::get('bkp_jumlah_bayar');

        $expenditure_type = Input::get('bkp_pengeluaran');
        $expenditure_id = Input::get('pengeluaran_id');
        $expenditure_nominal = Input::get('bkp_pengeluaran_nominal');
        $giro = Input::get('bkp_giro_nominal');
        $giro_ref = Input::get('bkp_giro_refrensi_id');
        $tanggal_giro=Input::get('tanggal_giro');
        $tanggal_giro=get_date_format_inggris($tanggal_giro);
        $keterangan_giro=Input::get('keterangan_giro');
        if ($bkp) {
            $bkp->note = $note;
            $bkp->nomor_giro = $nomor_giro;

            $bkp->voucher_payment = str_replace(",","",$voucher_payment);
            $bkp->discount_payment = str_replace(",","",$discount_payment);
            $bkp->paid_amount = str_replace(",","",$paid_amount);
            $bkp->expenditure_type = $expenditure_type;
            $bkp->expenditure_id = $expenditure_id;
            $bkp->expenditure_nominal = str_replace(",","",$expenditure_nominal);
            $bkp->giro = $giro;
            $bkp->giro_ref = $giro_ref;  
            $bkp->tanggal_giro=$tanggal_giro;
            $bkp->keterangan_giro=$keterangan_giro;
            $bkp->status = 'draft';
            $bkp->proof_status = '1';   
            $bkp->save();

            AccountingBkpDetail::where('bkp', $cmd)->delete();
            for($a = 0; $a < sizeof($coa);$a++){
                //Penomoran id
                $last_number_id = AccountingBkpDetail::max('id');
                if($last_number_id==''){
                    $number_id = 1;
                } else {
                    $number_id = 1 + $last_number_id;
                }
                $bkpdet = new AccountingBkpDetail();
                $bkpdet->id = $number_id;
                $bkpdet->bkp = $cmd;
                $bkpdet->coa = $coa[$a];
                $bkpdet->procurement_type = $procurement_type[$a];
                $bkpdet->procurement = $procurement[$a];
                $bkpdet->deficiency = str_replace(",","",$deficiency[$a]);
                $bkpdet->payment = str_replace(",","",$payment[$a]);
                $bkpdet->save();
            }
            return redirect('accountings/bkp')->with([
                'message' => 'BKP Updated Successfully'
            ]);

        } else {
            return redirect('accountings/bkp')->with([
                'message' => 'BKP Not Found',
                'message_important' => true
            ]);
        }
    }


    /* deleteBkp  Function Start Here */
    public function deleteBkp($id)
    {
        $bkp = AccountingBkp::find($id);
        if ($bkp) {
            $bkp->delete();

            return redirect('accountings/bkp')->with([
                'message' => 'BKP Deleted Successfully'
            ]);
        } else {
            return redirect('accountings/bkp')->with([
                'message' => 'BKP Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewBkp  Function Start Here */
    public function viewBkp($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 74)->first();
        $no = 0;
        $project = Project::all();
        $coa = Coa::all();
        $procurement = Procurement::all();
        $company = Company::where('id','!=','1')->where('id','!=','2')->get();
        $bkp = AccountingBkp::find($id);
        $bkpdet = AccountingBkpDetail::where('bkp','=',$id)->get();
        return view('admin.bkp.view-bkp', compact('bkp','bkpdet','project','coa','procurement','company','no','permcheck'));
    }


    /* generate tansaction code */
	public function get_kode_transaksi(){
		$bln = date('m');
		$thn = date('y');
        $query = DB::table('sys_accounting_bkps')
                    ->selectRaw('MID(bkp_code,9,5) as id')
                    ->whereRaw('MID(bkp_code,1,8) = "BKPO'.$thn.''.$bln.'"')
                    ->orderBy('bkp_code', 'asc')
                    ->limit(1)
                    ->get();
		$kode_baru = format_kode_transaksi('BKPO',$query);
		return $kode_baru;
	}


}
