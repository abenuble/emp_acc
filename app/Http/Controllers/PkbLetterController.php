<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeRolesPermission;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\PkbLetter;
use App\Company;
use DB;
use WkPdf;

class PkbLetterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* pkbLetters  Function Start Here */
    public function pkbLetters()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 72)->first();
        $pkbletters = PkbLetter::where('category','=','pkb')->orderBy('id','asc')->get();
        return view('admin.pkbletter.pkb-letters', compact('pkbletters','permcheck'));
    }

    /* manageTemplate  Function Start Here */
    public function manageTemplate($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 72)->first();
        $pkbletters = PkbLetter::find($id);

        if($pkbletters){
            return view('admin.pkbletter.pkb-manage', compact('pkbletters','permcheck'));
        }else{
            return redirect('outgoing-letter/pkb-letters')->with([
                'message' => 'Letter Not Found',
                'message_important'=>true
            ]);
        }

    }

    /* updateTemplate  Function Start Here */
    public function updateTemplate(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'subject' => 'required', 'message' => 'required', 'date' => 'required'
        ]);

        $cmd = Input::get('cmd');

        if ($v->fails()) {
            return redirect('outgoing-letter/pkb-letters/manage/' . $cmd)->withErrors($v->errors());
        }

        $subject = Input::get('subject');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $message = Input::get('message');
        $tembusan=Input::get('tembusan');

        PkbLetter::where('id', '=', $cmd)->update([
            'subject' => $subject,
            'message' => $message,
            'tembusan' =>$tembusan,
            'date' => $date,
            'status' => 'draft',
        ]);

        return redirect('outgoing-letter/pkb-letters/manage/' . $cmd)->with([
            'message' => 'Letter Update Successfully'
        ]);

    }

    /* addTemplate  Function Start Here */
    public function addTemplate()
    {
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        // $last_letter_number = DB::table('sys_out_letters')->orderBy('id','asc')->value('letter_number');
        // if($last_letter_number){
        // $last_number = explode('/', $last_letter_number);}
        // else{ $last_number=0;}
        // if($date=='01'){
        //     $number = 01;
        // } else{
        //     $number = 1 + $last_number[0];
        // }
        $last_letter_number = DB::table('sys_out_letters')->where('letter_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('letter_number');
        if($last_letter_number==''){
            $number = 1;
        } else{
            $last_number = explode('/', $last_letter_number);
            $number = 1 + $last_number[0];
        }
        $company = Company::find(\Auth::user()->company);

        return view('admin.pkbletter.pkb-letter-add', compact('month','year','number','company'));
    }

    /* addTemplatePost  Function Start Here */
    public function addTemplatePost(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'subject' => 'required', 'message' => 'required', 'date' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('outgoing-letter/pkb-letters')->withErrors($v->errors());
        }

        $letter_number = Input::get('letter_number');
        $subject = Input::get('subject');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $message = Input::get('message');
        $tembusan=Input::get('tembusan');


        $pkbletters = new PkbLetter();
        $pkbletters->letter_number = $letter_number;
        $pkbletters->subject = $subject;
        $pkbletters->tembusan=$tembusan;
        $pkbletters->message = $message;
        $pkbletters->date = $date;
        $pkbletters->category = 'pkb';
        $pkbletters->save();

        return redirect('outgoing-letter/pkb-letters')->with([
            'message' => 'Letter Added Successfully'
        ]);

    }
    /* getVariable  Function Start Here */
    public function getVariable(Request $request)
    {

        $variables = array("letter_number", "date", "subject");
        foreach ($variables as $var) {
            echo '{{' . $var . '}}, ' ;
        }
        
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {

        $pkbletters = PkbLetter::find($id);

        $letter_number = $pkbletters->letter_number;
        $date = get_date_format($pkbletters->date);
        $subject = $pkbletters->subject;
        
        $data = array(
            'letter_number' => $letter_number,
            'date' => $date,
            'subject' => $subject,
        );

        $template = $pkbletters->message;
        
        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.pkbletter.pdf-pkb-letter', compact('message','letter_number'))->setPaper('a4', 'portrait');

        return $pdf->stream($letter_number.'.pdf');
    }
}
