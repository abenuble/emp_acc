<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Component;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Schema\Blueprint;
use Schema;
use App\Coa;
use Excel;
date_default_timezone_set(app_config('Timezone'));

class ComponentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* components  Function Start Here */
    public function components()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 28)->first();

        $components = Component::orderBy('id','asc')->get();
        // $coa = Coa::where('coa_type','approximate name')->get();
        return view('admin.component.components', compact('components','permcheck'));

    }

     /* import excel function start here */
     public function importExcel()
     {
         $components = Component::lists('component')->toArray();
 
         if(Input::hasFile('import_file')){
             $path = Input::file('import_file')->getRealPath();
             $target_file = $path . basename($_FILES["import_file"]["name"]);
             $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
             $data = Excel::load($path, function($reader) {
             })->get();
             if ($fileType != 'xls'){
                 return redirect('companies')->with([
                     'message' => 'Upload an Excel 97-2003 ',
                     'message_important' => true
                 ]);
             } else{       
                 if(!empty($data) && $data->count()){
                     foreach ($data as $key => $value) {
                         // Skip company previously added using in_array
                         if (in_array($value->component, $components)){
                             continue;
                         } else {
                             //Penomoran id
                             $last_number = Component::max('id');
                             if($last_number==''){
                                 $number = 1;
                             } else {
                                 $number = 1 + $last_number;
                             }
                             if ($value->component=='' ) {
                              continue;
                             } else {
                                 $component = new Component();
                                 $component->id = $number;
                                 $component->component = $value->component;
                                 $component->status = 'active';
                                //  $component->coa = $value->coa;
                                //  $component->type = $value->type;
                                 $component->save();    
 
                             }
                         }
                     }
                     return redirect('components')->with([
                         'message' => 'Component Added Successfully'
                     ]);
                 } else{
                     return redirect('components')->with([
                         'message' => 'Component Not Found',
                         'message_important' => true
                     ]);
                 }   
             }
         }
         return back();
     }

         /* download excel function start here */
         public function downloadExcel()
         {
             return response()->download(public_path('assets/template_xls/components.xls'));
         }
   
         
    /* addComponent  Function Start Here */
    public function addComponent(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'component'         => 'required'
        ]);

        if ($v->fails()) {
            return redirect('components')->withErrors($v->errors());
        }


        $component_name  = Input::get('component');
        // $coa             = Input::get('coa');
        $status          = Input::get('status');
        // $type=Input::get('type');
        $exist = Component::where('component', $component_name)->first();
        if ($exist) {
            return redirect('components')->with([
                'message' => 'Component Already Exist',
                'message_important' => true
            ]);
        }
        //Penomoran id
        $last_number = Component::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }
        $component                      = new Component();
        $component->id                  = $number;
        $component->component           = $component_name;
        $component->status              = $status;
        // $component->coa              = $coa;
        // $component->type=$type;
        $component->save();

        // Schema::table('sys_payment_components', function (Blueprint $table) {
        //     $payment_components = getTableColumns('sys_payment_components');
        //     $columns = Component::pluck('component')->toArray();
        //     foreach($columns as $col){
        //         if(in_array(preg_replace('/ /', '_', $col), $payment_components)){
        //             continue;
        //         } else{
        //             $table->biginteger(preg_replace('/ /', '_', $col))->default(0);
        //         }
        //     }
        // });


        if ($component!='') {
            return redirect('components')->with([
                'message' => 'Component Added Successfully'
            ]);

        } else {
            return redirect('components')->with([
                'message' => 'Component Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateComponent  Function Start Here */
    public function updateComponent(Request $request)
    {

        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'component' => 'required',
            'status'     => 'required'
        ]);

        if ($v->fails()) {
            return redirect('components')->withErrors($v->errors());
        }
        $component = Component::find($cmd);
        $component_name = Input::get('component');
        $value  = Input::get('value');
        // $coa  = Input::get('coa');
        $status = Input::get('status');
        // $type=Input::get('type');
        if ($component_name != $component->component) {

            $exist = Component::where('component', $component_name)->first();
            if ($exist) {
                return redirect('components')->with([
                    'message' => 'Component Already Exist',
                    'message_important' => true
                ]);
            }
        }

        if ($component) {
            $component->component = $component_name;
            $component->value = $value;
            // $component->coa = $coa;
            $component->status = $status;
            // $component->type=$type;
            $component->save();

            return redirect('components')->with([
                'message' => 'Component Updated Successfully'
            ]);

        } else {
            return redirect('components')->with([
                'message' => 'Component Not Found',
                'message_important' => true
            ]);
        }
    }

    /* viewComponent  Function Start Here */
    public function viewComponent($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 28)->first();
        $component = Component::find($id);
        // $coa = Coa::where('coa_type','approximate name')->get();
        return view('admin.component.view-component', compact('component','permcheck'));
    }

    /* setComponentStatus  Function Start Here */
    public function setComponentStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $type=Input::get('type');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('components')->withErrors($v->fails());
        }
        // $coa=Input::get('coa');
        // $type=Input::get('type');
        $component  = Component::find($cmd);
        if($component){
            $component->status=$request->status;
            // $component->coa=$coa;
            // $component->type=$type;
            $component->save();

            return redirect('components')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('components')->with([
                'message' => 'Component not found',
                'message_important'=>true
            ]);
        }

    }


}
