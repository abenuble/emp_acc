<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\AssessmentDictionary;
use App\AssessmentDictionaryAspect;
use App\CareerPath;
use App\Company;
use App\CompanyProfileStructure;
use App\CompanyProfileJobDesc;
use App\Division;
use App\EmployeeRolesPermission;
use App\Employee;
use App\Expense;
use App\LastEducation;
use App\Position;
use App\Project;
use App\Notification;
use App\Calendar;
use App\ViolationTypes;
use App\VisionMission;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use dateTime;

date_default_timezone_set(app_config('Timezone'));

class CompaniesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function cetakWord(){
        // Creating the new document...
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        
        /* Note: any element you append to a document must reside inside of a Section. */
        
        // Adding an empty Section to the document...
        $section = $phpWord->addSection();
        // Adding Text element to the Section having font styled by default...
        $section->addText(
            '"Learn from yesterday, live for today, hope for tomorrow. '
                . 'The important thing is not to stop questioning." '
                . '(Albert Einstein)'
        );
        
        /*
         * Note: it's possible to customize font style of the Text element you add in three ways:
         * - inline;
         * - using named font style (new font style object will be implicitly created);
         * - using explicitly created font style object.
         */
        
        // Adding Text element with font customized inline...
        $section->addText(
            '"Great achievement is usually born of great sacrifice, '
                . 'and is never the result of selfishness." '
                . '(Napoleon Hill)',
            array('name' => 'Tahoma', 'size' => 10)
        );
        
        $section->addText(
            '"The greatest accomplishment is not in never falling, '
                . 'but in rising again after you fall." '
                . '(Vince Lombardi)'
        );
        
        
        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        return $objWriter->save('helloWorld.docx');
        // echo "masuk";
        // print_r($section);
        // // Saving the document as ODF file...
        // $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'ODText');
        // $objWriter->save('helloWorld.odt');
        
        // // Saving the document as HTML file...
        // $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
        // $objWriter->save('helloWorld.html');
        
        /* Note: we skip RTF, because it's not XML-based and requires a different example. */
        /* Note: we skip PDF, because "HTML-to-PDF" approach is used to create PDF documents. */

    }

    /* companyProfile  Function Start Here */
    public function companyProfile()
    {

        $id_sss = '1';
        $sss = Company::find($id_sss);
        $id = '1';
        $organizational_structure = CompanyProfileStructure::find($id);
        $jobdesc = CompanyProfileJobDesc::all();
        $position = Position::all();
        $division = Division::all();
        $violation = ViolationTypes::all();
        $assessment = AssessmentDictionary::all();
        $career_path = CareerPath::all();
        $education = LastEducation::all();
        $vision_mission = VisionMission::find($id);
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 2)->first();
        return view('admin.company.company-profile', compact('vision_mission','education','career_path','assessment','violation','division','position','jobdesc','organizational_structure','sss','permcheck'));

    }

    /* organizationalStructureUpload  Function Start Here */
    public function organizationalStructureUpload()
    {

        $id = '1';
        $organizational_structure = CompanyProfileStructure::find($id);

        $file = Input::file('file');
        $destinationPath_file = public_path() . '/assets/company_profile_files/';
        $target_file = $destinationPath_file . basename($_FILES["file"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        if ($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'jpeg' || $imageFileType == 'pdf') {
            $destinationPath        = public_path() . '/assets/company_profile_files/';
            $file_name              = $file->getClientOriginalName();
            Input::file('file')->move($destinationPath, $file_name);

            $organizational_structure->organizational_structure = $file_name;
            $organizational_structure->save();

            return redirect('company-profile')->with([
                'message' => 'Company Profile Updated Successfully'
            ]);
        }

    }

    /* jobdescAdd  Function Start Here */
    public function jobdescAdd()
    {
        $division          = Input::get('division');
        $position          = Input::get('position');
        $report_to         = Input::get('report_to');
        $detail            = Input::get('detail');

        //Penomoran id
        $last_number = CompanyProfileJobDesc::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }

        $jobdesc = new CompanyProfileJobDesc();
        $jobdesc->id        = $number;
        $jobdesc->division  = $division;
        $jobdesc->position  = $position;
        $jobdesc->report_to = $report_to;
        $jobdesc->detail    = $detail;
        $jobdesc->save();

        return redirect('company-profile')->with([
            'message' => 'Company Profile Updated Successfully'
        ]);

    }

    /* viewJobdesc  Function Start Here */
    public function viewJobdesc($id)
    {
        $jobdesc = CompanyProfileJobDesc::find($id);
        return view('admin.company.view-job-desc', compact('jobdesc'));
    }

    /* editJobdesc  Function Start Here */
    public function editJobdesc($id)
    {
        $jobdesc = CompanyProfileJobDesc::find($id);
        $position = Position::all();
        $division = Division::all();
        return view('admin.company.edit-job-desc', compact('jobdesc','position','division'));
    }

    /* editJobdescPost  Function Start Here */
    public function editJobdescPost()
    {
        $cmd               = Input::get('cmd');

        $jobdesc           = CompanyProfileJobDesc::find($cmd);
        $division          = Input::get('division');
        $position          = Input::get('position');
        $report_to         = Input::get('report_to');
        $detail            = Input::get('detail');

        if($jobdesc){
            $jobdesc->division  = $division;
            $jobdesc->position  = $position;
            $jobdesc->report_to = $report_to;
            $jobdesc->detail    = $detail;
            $jobdesc->save();
    
            return redirect('company-profile')->with([
                'message' => 'Company Profile Updated Successfully'
            ]);

        }

    }

    /* ViolationAdd  Function Start Here */
    public function ViolationAdd()
    {
        $violation          = Input::get('violation');
        $weight             = Input::get('weight');
        $information        = Input::get('information');
        //Penomoran id
        $last_number = ViolationTypes::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }

        $violation_types = new ViolationTypes();
        $violation_types->id  = $number;
        $violation_types->violation  = $violation;
        $violation_types->weight  = $weight;
        $violation_types->information  = $information;
        $violation_types->save();

        return redirect('company-profile')->with([
            'message' => 'Company Profile Updated Successfully'
        ]);

    }

    /* viewViolation  Function Start Here */
    public function viewViolation($id)
    {
        $violation = ViolationTypes::find($id);
        return view('admin.company.view-violation', compact('violation'));
    }

    /* editViolation  Function Start Here */
    public function editViolation($id)
    {
        $violation = ViolationTypes::find($id);
        return view('admin.company.edit-violation', compact('violation'));
    }

    /* editViolationPost  Function Start Here */
    public function editViolationPost()
    {
        $cmd               = Input::get('cmd');

        $violation_types           = ViolationTypes::find($cmd);
        $violation          = Input::get('violation');
        $weight             = Input::get('weight');
        $information        = Input::get('information');

        if($violation_types){
            $violation_types->violation  = $violation;
            $violation_types->weight  = $weight;
            $violation_types->information  = $information;
            $violation_types->save();
    
            return redirect('company-profile')->with([
                'message' => 'Company Profile Updated Successfully'
            ]);

        }

    }

    /* AssessmentAdd  Function Start Here */
    public function AssessmentAdd()
    {
        $dimensions          = Input::get('dimensions');
        $aspects             = Input::get('aspect');
        $KS                  = Input::get('KS');
        $K                   = Input::get('K');
        $C                   = Input::get('C');
        $B                   = Input::get('B');
        $BS                  = Input::get('BS');
        //Penomoran id
        $last_number = AssessmentDictionary::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }

        $assessment_dictionary = new AssessmentDictionary();
        $assessment_dictionary->id  = $number;
        $assessment_dictionary->dimensions  = $dimensions;
        $assessment_dictionary->save();

        $number_id = $assessment_dictionary->id;

        for($a=0;$a<count($aspects);$a++){
            $last_number = AssessmentDictionaryAspect::max('id');
            if($last_number==''){
                $number = 1;
            } else {
                $number = 1 + $last_number;
            }
            $assessment_dictionary_aspect = new AssessmentDictionaryAspect();
            $assessment_dictionary_aspect->id = $number;
            $assessment_dictionary_aspect->assessment = $number_id;
            $assessment_dictionary_aspect->aspect = $aspects[$a];
            $assessment_dictionary_aspect->KS = $KS[$a];
            $assessment_dictionary_aspect->K = $K[$a];
            $assessment_dictionary_aspect->C = $C[$a];
            $assessment_dictionary_aspect->B = $B[$a];
            $assessment_dictionary_aspect->BS = $BS[$a];
            $assessment_dictionary_aspect->save();

        }
        return redirect('company-profile')->with([
            'message' => 'Company Profile Updated Successfully'
        ]);

    }

    /* viewAssessment  Function Start Here */
    public function viewAssessment($id)
    {
        $assessment = AssessmentDictionary::find($id);
        $aspect = AssessmentDictionaryAspect::where('assessment', '=', $id)->get();
        return view('admin.company.view-assessment', compact('assessment','aspect'));
    }

    /* editAssessment  Function Start Here */
    public function editAssessment($id)
    {
        $assessment = AssessmentDictionary::find($id);
        $aspect = AssessmentDictionaryAspect::where('assessment', '=', $id)->get();
        return view('admin.company.edit-assessment', compact('assessment','aspect'));
    }

    /* editAssessmentPost  Function Start Here */
    public function editAssessmentPost()
    {
        $cmd                = Input::get('cmd');

        $assessment         = AssessmentDictionary::find($cmd);
        $dimension          = Input::get('dimensions');
        $aspects            = Input::get('aspect');
        $KS                 = Input::get('KS');
        $K                  = Input::get('K');
        $C                  = Input::get('C');
        $B                  = Input::get('B');
        $BS                 = Input::get('BS');

        if($assessment){
            $assessment->dimensions  = $dimension;
            $assessment->save();

            $aspect = AssessmentDictionaryAspect::where('assessment', '=', $cmd)->get();
            foreach($aspect as $a){
                $a->delete();
            }
            for($a=0;$a<count($aspects);$a++){
                $last_number = AssessmentDictionaryAspect::max('id');
                if($last_number==''){
                    $number = 1;
                } else {
                    $number = 1 + $last_number;
                }
                $assessment_dictionary_aspect = new AssessmentDictionaryAspect();
                $assessment_dictionary_aspect->id = $number;
                $assessment_dictionary_aspect->assessment = $cmd;
                $assessment_dictionary_aspect->aspect = $aspects[$a];
                $assessment_dictionary_aspect->KS = $KS[$a];
                $assessment_dictionary_aspect->K = $K[$a];
                $assessment_dictionary_aspect->C = $C[$a];
                $assessment_dictionary_aspect->B = $B[$a];
                $assessment_dictionary_aspect->BS = $BS[$a];
                $assessment_dictionary_aspect->save();

            }
    
            return redirect('company-profile')->with([
                'message' => 'Company Profile Updated Successfully'
            ]);

        }

    }

    /* CareerPathAdd  Function Start Here */
    public function CareerPathAdd()
    {
        $level = Input::get('level');
        $position = Input::get('position');
        $education = Input::get('education');
        $major = Input::get('major');
        $working_period = Input::get('working_period');
        $competences = Input::get('competences');
        //Penomoran id
        $last_number = CareerPath::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }

        $career_path = new CareerPath();
        $career_path->id  = $number;
        $career_path->level  = $level;
        $career_path->position  = $position;
        $career_path->education  = $education;
        $career_path->major  = $major;
        $career_path->working_period  = $working_period;
        $career_path->competences  = $competences;
        $career_path->save();

        return redirect('company-profile')->with([
            'message' => 'Company Profile Updated Successfully'
        ]);

    }

    /* viewCareerPath  Function Start Here */
    public function viewCareerPath($id)
    {
        $careerpath = CareerPath::find($id);
        $education = LastEducation::all();
        $division = Division::all();
        return view('admin.company.view-careerpath', compact('careerpath','education','division'));
    }

    /* editCareerPath  Function Start Here */
    public function editCareerPath($id)
    {
        $careerpath = CareerPath::find($id);
        $education = LastEducation::all();
        $division = Division::all();
        return view('admin.company.edit-careerpath', compact('careerpath','education','division'));
    }

    /* editCareerPathPost  Function Start Here */
    public function editCareerPathPost()
    {
        $cmd = Input::get('cmd');

        $careerpath = CareerPath::find($cmd);
        $level = Input::get('level');
        $position = Input::get('position');
        $education = Input::get('education');
        $major = Input::get('major');
        $working_period = Input::get('working_period');
        $competences = Input::get('competences');

        if($careerpath){
            $careerpath->level  = $level;
            $careerpath->position  = $position;
            $careerpath->education  = $education;
            $careerpath->major  = $major;
            $careerpath->working_period  = $working_period;
            $careerpath->competences  = $competences;
            $careerpath->save();

    
            return redirect('company-profile')->with([
                'message' => 'Company Profile Updated Successfully'
            ]);

        }

    }


    /* companies  Function Start Here */
    public function companies()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 24)->first();
        $companies = Company::orderBy('id','asc')->get();
        return view('admin.company.companies', compact('companies','permcheck'));

    }

    /* editCompany  Function Start Here */
    public function editCompany($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 24)->first();
        $company = Company::find($id);
        return view('admin.company.edit-company', compact('company','permcheck'));

    }

    /* addCompany  Function Start Here */
    public function addCompany(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'company' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('companies')->withErrors($v->errors());
        }

        $company_code = Input::get('company_code');
        $company_name = Input::get('company');

        $address = Input::get('address');
        $phone_number = Input::get('phone');
        $pic = Input::get('pic');
        $pic_jabatan = Input::get('pic_jabatan');
        $pic_phone = Input::get('pic_phone');
        $status = Input::get('status');       
        
        $exist = Company::where('company', $company_name)->first();
        if ($exist) {
            return redirect('companies')->with([
                'message' => 'Company Already Exist',
                'message_important' => true
            ]);
        }

        //Penomoran id
        $last_number = Company::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }

        $company = new Company();
        $company->id = $number;
        $company->company_code = $company_code;
        $company->company = $company_name;
        $company->address = $address;
        $company->phone_number = $phone_number;
        $company->pic = $pic;
        $company->pic_jabatan = $pic_jabatan;
        $company->pic_phone = $pic_phone;
        $company->status = $status;
        $company->save();

        if ($company!='') {
            return redirect('companies')->with([
                'message' => 'Company Added Successfully'
            ]);

        } else {
            return redirect('companies')->with([
                'message' => 'Company Already Exist',
                'message_important' => true
            ]);
        }
    }


    /* updateCompany  Function Start Here */
    public function updateCompany(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'company' => 'required',
            'status'     => 'required'
        ]);

        if ($v->fails()) {
            return redirect('companies')->withErrors($v->errors());
        }
        $company = Company::find($cmd);

        $company_code = Input::get('company_code');
        $company_name = Input::get('company');

        $address = Input::get('address');
        $phone_number = Input::get('phone_number');
        $pic = Input::get('pic');
        $pic_jabatan = Input::get('pic_jabatan');
        $pic_phone = Input::get('pic_phone');
        $status = Input::get('status');       
        
        if ($company) {
            $company->company_code = $company_code;
            $company->company = $company_name;
            $company->address = $address;
            $company->phone_number = $phone_number;
            $company->pic = $pic;
            $company->pic_jabatan = $pic_jabatan;
            $company->pic_phone = $pic_phone;
            $company->status = $status;
            $company->save();

            return redirect('companies')->with([
                'message' => 'Company Updated Successfully'
            ]);

        } else {
            return redirect('companies')->with([
                'message' => 'Company Not Found',
                'message_important' => true
            ]);
        }
    }

    /* editMission  Function Start Here */
    public function editMission(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'mission' => 'required',
        ]);

        if ($v->fails()) {
            return redirect('company-profile')->withErrors($v->errors());
        }
        $vision_mission = VisionMission::find($cmd);
        $mission = Input::get('mission');


        if ($vision_mission) {
            $vision_mission->mission = $mission;
            $vision_mission->save();

            return redirect('company-profile')->with([
                'message' => 'Mission Updated Successfully'
            ]);

        } else {
            return redirect('company-profile')->with([
                'message' => 'Mission Not Found',
                'message_important' => true
            ]);
        }
    }

    /* editVision  Function Start Here */
    public function editVision(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'vision' => 'required',
        ]);

        if ($v->fails()) {
            return redirect('company-profile')->withErrors($v->errors());
        }
        $vision_mission = VisionMission::find($cmd);
        $vision = Input::get('vision');


        if ($vision_mission) {
            $vision_mission->vision = $vision;
            $vision_mission->save();

            return redirect('company-profile')->with([
                'message' => 'Vision Updated Successfully'
            ]);

        } else {
            return redirect('company-profile')->with([
                'message' => 'Vision Not Found',
                'message_important' => true
            ]);
        }
    }

    /* editSlogan  Function Start Here */
    public function editSlogan(Request $request)
    {

        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'slogan' => 'required',
        ]);

        if ($v->fails()) {
            return redirect('company-profile')->withErrors($v->errors());
        }
        $vision_mission = VisionMission::find($cmd);
        $slogan = Input::get('slogan');


        if ($vision_mission) {
            $vision_mission->slogan = $slogan;
            $vision_mission->save();

            return redirect('company-profile')->with([
                'message' => 'Slogan Updated Successfully'
            ]);

        } else {
            return redirect('company-profile')->with([
                'message' => 'Slogan Not Found',
                'message_important' => true
            ]);
        }
    }
    
        /* editPatra  Function Start Here */
        public function editSSS(Request $request)
        {
            $cmd = Input::get('cmd');
            $v = \Validator::make($request->all(), [
                'file' => 'required',
                'address' => 'required',
                'phone' => 'required',
            ]);
    
            if ($v->fails()) {
                return redirect('company-profile')->withErrors($v->errors());
            }
            $sss = Company::find($cmd);
            $file = Input::file('file');
            $path = Input::file('file')->getRealPath();
            $target_file = $path . basename($_FILES["file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
            $address = Input::get('address');
            $phone = Input::get('phone');
    
            if ($fileType == 'jpg' || $fileType == 'jpeg' || $fileType == 'png'){
                $destinationPath = public_path() . '/assets/company_pic/';
    
                $file_name = $file->getClientOriginalName();
                Input::file('file')->move($destinationPath, $file_name);
    
                if ($sss) {
                    $sss->address = $address;
                    $sss->phone_number = $phone;
                    $sss->avatar = $file_name;
                    $sss->save();
        
                    return redirect('company-profile')->with([
                        'message' => 'Company Updated Successfully'
                    ]);
        
                } else {
                    return redirect('company-profile')->with([
                        'message' => 'Company Not Found',
                        'message_important' => true
                    ]);
                }
               
            } else{
                return redirect('company-profile')->with([
                    'message' => 'Upload an Image',
                    'message_important' => true
                ]);
    
            }
    
        }

    /* deleteCompany  Function Start Here */
    public function deleteCompany($id)
    {
        $company = Company::find($id);
        if ($company) {
            $company->delete();

            return redirect('companies')->with([
                'message' => 'Company Deleted Successfully'
            ]);
        } else {
            return redirect('companies')->with([
                'message' => 'Company Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewCompany  Function Start Here */
    public function viewCompany($id)
    {
        $company            = Company::find($id);
        $employees          = Employee::where('company', '=', $id)->count();
        $projects           = Project::where('company', '=', $id)->count();
        $project            = Project::where('company', '=', $id)->get();
        $employee           = Employee::where('company', '=', $id)->get();
        $role_id = \Auth::user()->role_id;
        $employee_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
        $project_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 5)->first();
        return view('admin.company.view-company', compact('company','project','projects','employee_permission','project_permission','employees','employee'));
    }

    /* download excel function start here */
    public function downloadExcel()
    {
        return response()->download(public_path('assets/template_xls/companies.xls'));
    }

    /* import excel function start here */
    public function importExcel()
    {
        // Get current data from company table
        $company = Company::lists('company')->toArray();

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
            })->get();
            if ($fileType != 'xls'){
                return redirect('companies')->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else{       
                if(!empty($data) && $data->count()){
                    foreach ($data as $key => $value) {
                        // Skip company previously added using in_array
                        if (in_array($value->perusahaan, $company)){
                            continue;
                        } else {
                            //Penomoran id
                            $last_number = Company::max('id');
                            if($last_number==''){
                                $number = 1;
                            } else {
                                $number = 1 + $last_number;
                            }
                            if ($value->perusahaan=='' || $value->alamat=='' || $value->telepon=='') {
                                return redirect('companies')->with([
                                    'message' => 'Perusahaan Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            } else {

                                
                                // $anniversary = get_date_format_inggris($value->hari_jadi);
                                // $aniv=new dateTime($anniversary);
                                // $aniv->modify('-1 year');

                                $companies = new Company();
                                $companies->id = $number;
                                $companies->company_code = $value->kode_area_perusahaan;
                                $companies->company = $value->perusahaan;
                                $companies->address = $value->alamat;
                                $companies->phone_number = $value->telepon;
                                $companies->pic = $value->pic;
                                $companies->pic_phone = $value->telepon_pic;
                                // $companies->anniversary = $anniversary;
                                // $companies->rekening = $value->no_rekening;
                                $companies->save();  

                                
                                // if($companies->anniversary!=$anniversary){
                                //     $sama=false;
                                // }
                                // else{
                                //     $sama=true;
                                // }

                                // if(!$sama){
                                //     $ada=Calendar::where('tag','=','company')->where('id_tag','=',$companies->id)->get();
                                //     if(sizeof($ada)>0){
                                //         $deletedata= Calendar::where('tag','=','company')->where('id_tag','=',$companies->id)->delete();
                                //         for($i=0;$i<99 ; $i++){
                                //             $aniv->modify('+1 year');
                                //             $last_number_notification = Notification::max('id');
                                //             if($last_number_notification==''){
                                //                 $number_notification = 1;
                                //             } else {
                                //                 $number_notification = 1 + $last_number_notification;
                                //             }
                                //             $last_number_calendar = Calendar::max('id');
                                //             if($last_number_calendar==''){
                                //                 $number_calendar = 1;
                                //             } else {
                                //                 $number_calendar = 1 + $last_number_calendar;
                                //             }
                                //             $calendar = new Calendar();
                                //             $calendar->start_date =$aniv->format('Y-m-d');
                                //             $calendar->end_date =$aniv->format('Y-m-d');
                                //             $calendar->starts_at = '00:00:00';
                                //             $calendar->ends_at =  '12:00:00';
                                //             $calendar->occasion = 'Company Birthday';
                                //             $calendar->description = 'Hari Jadi '.$value->perusahaan;
                                //             $calendar->location =  $value->alamat;
                                //             $calendar->shared_with =  'all';
                                //             $calendar->event =  'special';
                                //             $calendar->tag = 'company';
                                //             $calendar->id_tag = $companies->id;
                                //             $calendar->save(); 
                                //         }  
                                //     } else{
                                //         for($i=0;$i<99 ; $i++){
                                //             $aniv->modify('+1 year');
                                //             $last_number_notification = Notification::max('id');
                                //             if($last_number_notification==''){
                                //                 $number_notification = 1;
                                //             } else {
                                //                 $number_notification = 1 + $last_number_notification;
                                //             }
                                //             $last_number_calendar = Calendar::max('id');
                                //             if($last_number_calendar==''){
                                //                 $number_calendar = 1;
                                //             } else {
                                //                 $number_calendar = 1 + $last_number_calendar;
                                //             }
                                //             $calendar = new Calendar();
                                //             $calendar->start_date =$aniv->format('Y-m-d');
                                //             $calendar->end_date =$aniv->format('Y-m-d');
                                //             $calendar->starts_at = '00:00:00';
                                //             $calendar->ends_at =  '12:00:00';
                                //             $calendar->occasion = 'Company Birthday';
                                //             $calendar->description = 'Hari Jadi '.$value->perusahaan;
                                //             $calendar->location =  $value->alamat;
                                //             $calendar->shared_with =  'all';
                                //             $calendar->event =  'special';
                                //             $calendar->tag = 'company';
                                //             $calendar->id_tag = $companies->id;
                                //             $calendar->save(); 
                                //         }     
                                //     }
                                // }
                            }
                        }
                    }
                    return redirect('companies')->with([
                        'message' => 'Company Added Successfully'
                    ]);
                } else{
                    return redirect('companies')->with([
                        'message' => 'Company Not Found',
                        'message_important' => true
                    ]);
                }   
            }
        }
		return back();
    }


}
