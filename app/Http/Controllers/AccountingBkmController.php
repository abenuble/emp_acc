<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\AccountingBkm;
use App\AccountingBkmDetail;
use App\Bank;
use App\Cash;
use App\Coa;
use App\Company;
use App\Notification;
use App\Procurement;
use App\Expense;
use App\Project;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;

class AccountingBkmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* bkm  Function Start Here */
    public function bkm()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 75)->first();
        $bkm = AccountingBkm::orderBy('id','asc')->get();
        return view('admin.bkm.bkm', compact('bkm','permcheck'));

    }


    /* getCompany Function Start Here */
    public function getCompany()
    {
        echo '<option value="0">Select Company</option>';
        $company = Company::where('id','!=','1')->where('id','!=','2')->get();
        foreach ($company as $d) {
            echo '<option value="' . $d->id . '">' . $d->company . '</option>';
        }
    }

    /* getCoa Function Start Here */
    public function getCoa()
    {
        echo '<option value="0">Select COA</option>';
        $coa = Coa::all();
        foreach ($coa as $d) {
            echo '<option value="' . $d->id . '">(' . $d->coa_code . ') ' . $d->coa . '</option>';
        }
    }

    /* getBank Function Start Here */
    public function getBank()
    {
        echo '<option value="0">Select Bank</option>';
        $bank = Bank::all();
        foreach ($bank as $d) {
            echo '<option value="' . $d->id . '">' . $d->bank . ' (' . $d->branch . '-' . $d->company_info->company . '-' . $d->account_number . ')</option>';
        }
    }

    /* getCash Function Start Here */
    public function getCash()
    {
        echo '<option value="0">Select Cash</option>';
        $cash = Cash::all();
        foreach ($cash as $d) {
            echo '<option value="' . $d->id . '">' . $d->cash . ' (' . $d->company_info->company . ')</option>';
        }
    }

    /* getExpense Function Start Here */
    public function getExpense(Request $request)
    {
        $id = $request->id;
        // $path = base_path();
        // echo $path; exit;
        if($id){
            echo '<option value="0">Select Expense</option>';
            $expense = Expense::where('project','=',$id)->where('status','=','Approved')->get();
            foreach ($expense as $d) {
                echo '<option value="' . $d->id . '">' . $d->expense_number . '</option>';
            }
        }
    }

    public function getProcure(Request $request)
    {
        $id = $request->id;
        // $path = base_path();
        // echo $path; exit;
        if($id){
            echo '<option value="0">Select Procurement</option>';
            $expense = Procurement::where('project','=',$id)->where('status','=','Approved')->where('realization_differences','>','0')->get();
            foreach ($expense as $d) {
                echo '<option value="' . $d->id . '">' . $d->procurement_number . '</option>';
            }
        }
    }
    /* getDetailExpense Function Start Here */
    public function getDetailExpense(Request $request)
    {
        $id = $request->id;
        $expense = Expense::find($id);

        return $expense;
    }

    public function getDetailProcure(Request $request)
    {
        $id = $request->id;
        $expense = Procurement::find($id);

        return $expense;
    }

    /* addBkm  Function Start Here */
    public function addBkm()
    {

        $bkm = AccountingBkm::all();
        $project = Project::all();
        $coa = Coa::all();
        $expense = Expense::all();
        $company = Company::where('id','!=','1')->where('id','!=','2')->get();
        return view('admin.bkm.add-bkm', compact('bkm','project','coa','expense','company'));

    }

    /* addPostBkm  Function Start Here */
    public function addPostBkm(Request $request)
    {
        $bkm_code = $this->get_kode_transaksi();
        $project = Input::get('project');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $attachment = Input::get('attachment');
        $payment_from = Input::get('payment_from');
        $paid_from = Input::get('paid_from');
        $nomor_giro = Input::get('nomor_giro');

        $coa = Input::get('m_coa_id');
        $expense_type = Input::get('bkmdet_typecheck2');
        $expense = Input::get('bkmdet_detail');
        $deficiency = Input::get('bkmdet_kekurangan');
        $payment = Input::get('bkmdet_jumlah');

        $note = Input::get('note');

        $voucher_payment = Input::get('bkm_voucher');
        $discount_payment = Input::get('bkm_discount');
        $paid_amount = Input::get('bkm_jumlah_bayar');

        $income_type = Input::get('bkm_pemasukkan');
        $income_id = Input::get('pemasukkan_id');
        $income_nominal = Input::get('bkm_pemasukkan_nominal');
        $giro = Input::get('bkm_giro_nominal');
        $giro_ref = Input::get('bkm_giro_refrensi_id');
        $tanggal_giro = Input::get('tanggal_giro');
        $tanggal_giro = get_date_format_inggris($tanggal_giro);

        $keterangan_giro=Input::get('keterangan_giro');
        //Penomoran id
        $last_number_id = AccountingBkm::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $bkm = new AccountingBkm();
        $bkm->id = $number_id;
        $bkm->nomor_giro = $nomor_giro;
        $bkm->bkm_code = $bkm_code;
        $bkm->project = $project;
        $bkm->date = $date;
        $bkm->attachment = $attachment;
        $bkm->payment_from = $payment_from;
        $bkm->paid_from = $paid_from;
        $bkm->note = $note;
        $bkm->voucher_payment = str_replace(",","",$voucher_payment);
        $bkm->discount_payment = str_replace(",","",$discount_payment);
        $bkm->paid_amount = str_replace(",","",$paid_amount);
        $bkm->income_type = $income_type;
        $bkm->income_id = $income_id;
        $bkm->income_nominal = str_replace(",","",$income_nominal);
        $bkm->giro = $giro;
        $bkm->giro_ref = $giro_ref; 
        $bkm->tanggal_giro=$tanggal_giro;
        $bkm->keterangan_giro=$keterangan_giro;
        $bkm->proof_status = '1';   
        $bkm->save();

        $bkm_id = $bkm->id;
        
        $notification = new Notification();
        $notification->id_tag = $bkm_id;
        $notification->tag = 'bkm';
        $notification->title = 'Verifikasi Bukti Kas Masuk';
        $notification->description = 'Verifikasi Bukti Kas Masuk '.$bkm_code;
        $notification->show_date = date('Y-m-d');
        $notification->start_date = $date;
        $notification->end_date = $date;
        $notification->route = 'accountings/bkm';
        $notification->save();

        for($a = 0; $a < sizeof($coa);$a++){
            //Penomoran id
            $last_number_id_det = AccountingBkmDetail::max('id');
            if($last_number_id_det==''){
                $number_id_det = 1;
            } else {
                $number_id_det = 1 + $last_number_id_det;
            }
            $bkmdet = new AccountingBkmDetail();
            $bkmdet->id = $number_id_det;
            $bkmdet->bkm = $bkm_id;
            $bkmdet->coa = $coa[$a];
            $bkmdet->expense_type = $expense_type[$a];
            $bkmdet->expense = $expense[$a];
            $bkmdet->deficiency = str_replace(",","",$deficiency[$a]);
            $bkmdet->payment = str_replace(",","",$payment[$a]);
            $bkmdet->save();
        }

        if ($bkm!='') {
            return redirect('accountings/bkm')->with([
                'message' => 'Bukti Kas Masuk Sukses ditambahkan'
            ]);

        } else {
            return redirect('accountings/bkm')->with([
                'message' => 'Bukti Kas Masuk Sukses Gagal ditambahkan',
                'message_important' => true
            ]);
        }

        


    }


    /* updateBkm  Function Start Here */
    public function updateBkm(Request $request)
    {

        $cmd = Input::get('cmd');
        
        $bkm = AccountingBkm::find($cmd);

        $coa = Input::get('m_coa_id');
        $expense_type = Input::get('bkmdet_typecheck2');
        $expense = Input::get('bkmdet_detail');
        $deficiency = Input::get('bkmdet_kekurangan');
        $payment = Input::get('bkmdet_jumlah');

        $note = Input::get('note');
        $nomor_giro = Input::get('nomor_giro');

        $voucher_payment = Input::get('bkm_voucher');
        $discount_payment = Input::get('bkm_discount');
        $paid_amount = Input::get('bkm_jumlah_bayar');

        $income_type = Input::get('bkm_pemasukkan');
        $income_id = Input::get('pemasukkan_id');
        // echo $income_id; exit;
        $income_nominal = Input::get('bkm_pemasukkan_nominal');
        $giro = Input::get('bkm_giro_nominal');
        $giro_ref = Input::get('bkm_giro_refrensi_id');
        $tanggal_giro=Input::get('tanggal_giro');
        $tanggal_giro= get_date_format_inggris($tanggal_giro);
        $keterangan_giro=Input::get('keterangan_giro');
        if ($bkm) {
            $bkm->note = $note;
            $bkm->nomor_giro = $nomor_giro;
            $bkm->voucher_payment = str_replace(",","",$voucher_payment);
            $bkm->discount_payment = str_replace(",","",$discount_payment);
            $bkm->paid_amount = str_replace(",","",$paid_amount);
            $bkm->income_type = $income_type;
            $bkm->income_id = $income_id;
            $bkm->income_nominal = str_replace(",","",$income_nominal);
            $bkm->giro = $giro;
            $bkm->giro_ref = $giro_ref;  
            $bkm->tanggal_giro=$tanggal_giro;
            $bkm->keterangan_giro=$keterangan_giro;
            $bkm->status = 'draft';
            $bkm->proof_status = '1';   
            $bkm->save();
            

            AccountingBkmDetail::where('bkm', $cmd)->delete();
            for($a = 0; $a < sizeof($coa);$a++){
                //Penomoran id
                $last_number_id = AccountingBkmDetail::max('id');
                if($last_number_id==''){
                    $number_id = 1;
                } else {
                    $number_id = 1 + $last_number_id;
                }
                $bkmdet = new AccountingBkmDetail();
                $bkmdet->id = $number_id;
                $bkmdet->bkm = $cmd;
                $bkmdet->coa = $coa[$a];
                $bkmdet->expense_type = $expense_type[$a];
                $bkmdet->expense = $expense[$a];
                $bkmdet->deficiency = str_replace(",","",$deficiency[$a]);
                $bkmdet->payment = str_replace(",","",$payment[$a]);
                $bkmdet->save();
            }
            return redirect('accountings/bkm')->with([
                'message' => 'BKM Updated Successfully'
            ]);

        } else {
            return redirect('accountings/bkm')->with([
                'message' => 'BKM Not Found',
                'message_important' => true
            ]);
        }
    }


    /* deleteBkm  Function Start Here */
    public function deleteBkm($id)
    {

        $bkm = AccountingBkm::find($id);
        if ($bkm) {
            $bkm->delete();

            return redirect('accountings/bkm')->with([
                'message' => 'BKM Deleted Successfully'
            ]);
        } else {
            return redirect('accountings/bkm')->with([
                'message' => 'BKM Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewBkm  Function Start Here */
    public function viewBkm($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 75)->first();
        $no = 0;
        $project = Project::all();
        $coa = Coa::all();
        $expense = Expense::all();
        $company = Company::where('id','!=','1')->where('id','!=','2')->get();
        $bkm = AccountingBkm::find($id);
        $bkmdet = AccountingBkmDetail::where('bkm','=',$id)->get();
        return view('admin.bkm.view-bkm', compact('bkm','bkmdet','project','coa','expense','company','no','permcheck'));
    }


    /* generate tansaction code */
	public function get_kode_transaksi(){
		$bln = date('m');
		$thn = date('y');
        $query = DB::table('sys_accounting_bkms')
                    ->selectRaw('MID(bkm_code,9,5) as id')
                    ->whereRaw('MID(bkm_code,1,8) = "BKMO'.$thn.''.$bln.'"')
                    ->orderBy('bkm_code', 'asc')
                    ->limit(1)
                    ->get();
		$kode_baru = format_kode_transaksi('BKMO',$query);
		return $kode_baru;
	}


}
