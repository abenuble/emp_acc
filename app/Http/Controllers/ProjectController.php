<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\Company;
use App\Designation;
use App\Employee;
use App\CareerPathEmployee;
use App\Component;
use App\JobApplicants;
use App\Jobs;
use App\Expense;
use App\ExpenseDetail;
use App\EmployeeRolesPermission;
use App\Notification;
use App\ProjectNeeds;
use App\Project;
use App\ProjectFiles;
use App\ProjectExpense;
use App\ProjectItem;
use App\ProjectVehicle;
use App\PayrollTypes;
use App\PayrollComponent;
use App\EmployeePaymentHistory;
use App\Subcategory;
use App\Http\Requests;
use App\LastEducation;
use App\EmployeeDependents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Excel;
use WkPdf;
use Session;


date_default_timezone_set(app_config('Timezone'));

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* projects  Function Start Here */
    public function Project()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 5)->first();
        $companyemp = Company::find(\Auth::user()->company);

        $internal_company = Employee::find(\Auth::user()->id);
        $company = Company::where('status','=','active')->get();
        $subcategories = Subcategory::where('status','=','active')->get();
        $designation = Designation::all();
        $projectneeds = ProjectNeeds::all();
        $projects = Project::orderBy('id','asc')->get();
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        // $last_letter_number = DB::table('sys_project')->orderBy('id','asc')->value('project_number');
        // $last_number = explode('/', $last_letter_number);
        // if($date=='01'){
        //     $number = 01;
        // } else{
        //     $number = 1 + $last_number[0];
        // }
        $last_letter_number = DB::table('sys_project')->where('project_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('project_number');
        if($last_letter_number==''){
            $number = 1;
        } else{
            $last_number = explode('/', $last_letter_number);
            $number = 1 + $last_number[0];
        }
        // foreach($projects as $p){
        //     if($p->end_date<=date('Y-m-d')){
        //         if($p->status == 'opening'){
        //             $p->status = 'closed';
        //             $p->save();
        //         }
        //     }
        // }
        return view('admin.project.projects', compact('year','month','number','projects','company','companyemp','designation','location','projectneeds','subcategories','internal_company','permcheck'));

    }
    /* getAreaCode Function Start Here */
    public function getAreaCode(Request $request)
    {
        $comp_id = $request->comp_id;
        if ($comp_id) {
            $company = Company::find($comp_id);

            $area = $company->company_code;
            $month = get_roman_letters(date('m'));
            $year = date('Y');

            $letter_number = $area.'/Proyek'.'/'.$month.'/'.$year;
            return $letter_number;
        }
    }
    public function downloadExcelItem()
    {
        return response()->download(public_path('assets/template_xls/project_item.xls'));

    }
    public function downloadExcelTemp()
    {


        return response()->download(public_path('assets/template_xls/proyek.xls'));

    }
      /* import excel function start here */
      public function importExcel()
      {
          // Get current data from subcategory table
          $company = Company::lists('company','id')->toArray();
   
          if(Input::hasFile('import_file')){
        
              $path = Input::file('import_file')->getRealPath();
              $target_file   = $path . basename($_FILES["import_file"]["name"]);
              $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
              $data = Excel::load($path, function($reader) {
              })->get();
              if ($fileType != 'xls'){
                  return redirect('projects')->with([
                      'message' => 'Upload an Excel 97-2003 ',
                      'message_important' => true
                  ]);
              } else {
                  if(!empty($data) && $data->count()){
                     
                      $insert = array();
                      foreach ($data as $key => $value) {
                          $company_id=0;
                        if (false !== $key = array_search($value->perusahaan, $company)) {
                            $company_id=$key;
                        }

                          if ($company_id!=0){
                             
                            //Penomoran id
                            $last_number = Project::max('id');
                            if($last_number==''){
                                $number = 1;
                            } else {
                                $number = 1 + $last_number;
                            }

                            $start_date = get_date_format_inggris($value->tanggal_mulai);
                            $end_date = get_date_format_inggris($value->tanggal_berakhir);

                            $status = "";
                            if(strtolower($value->status)=='buka'){
                                $status = "opening";
                            } else if(strtolower($value->status)=='diajukan'){
                                $status = "drafted";
                            } else if(strtolower($value->status)=='tutup'){
                                $status = "closed";
                            }
                            
                            $project = new Project();
                            $project->id = $number;
                            $project->company = $company_id;
                            $project->project_number = $value->nomor_kontrak_klien;
                            $project->project = $value->nama_kontrak_klien;
                            $project->facility = $value->fasilitas;
                            $project->health_facility_type = $value->jenis_fasilitas_kesehatan;
                            $project->accident_insurance = $value->jaminan_kecelakaan_kerja;
                            $project->life_insurance = $value->jaminan_kematian;
                            $project->jaminan_hari_tua = $value->jaminan_hari_tua;
                            $project->jaminan_pensiun = $value->jaminan_pensiun;
                            $project->insurance_name = $value->nama_asuransi_kesehatan;
                            $project->start_date = $start_date;
                            $project->end_date = $end_date;
                            // $project->value = $value->nilai;
                            $project->status = $status;
                            $project->save();
                          } else{
                        
                            continue;
                          }
                      }
                    
                      return redirect('projects')->with([
                          'message' => 'Project Added Successfully'
                      ]);
                  } else{
                      return redirect('projects')->with([
                          'message' => 'Project Not Found',
                          'message_important' => true
                      ]);
                  } 
              }
              
          }
          return back();
      }

        /* import excel item function start here */
        public function importExcelItem($a)
        {
            Session::flash('tabs', 'item'); 
            if(Input::hasFile('import_file')){
                
                $path = Input::file('import_file')->getRealPath();
                $target_file   = $path . basename($_FILES["import_file"]["name"]);
                $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
                $data = Excel::load($path, function($reader) {
                })->get();
                if ($fileType != 'xls'){
                    return redirect('projects/view/'.$a)->with([
                        'message' => 'Upload an Excel 97-2003 ',
                        'message_important' => true
                    ]);
                } else {
                    if(!empty($data) && $data->count()){
                        
                        $insert = array();
                        foreach ($data as $key => $value) {
                        
                        
                            // Skip subcategory previously added using in_array
                            if ($value->barang!=''){
                                
                                //Penomoran id
                                $last_number = ProjectItem::max('id');
                                if($last_number==''){
                                    $number = 1;
                                } else {
                                    $number = 1 + $last_number;
                                }
                                
                                    $project = new ProjectItem();
                                    $project->id = $number;
                                    $project->id_project = $a;
                                    $project->item = $value->barang;
                                    $project->amount = $value->jumlah;
                                    $project->amount_bought = $value->jumlah_terbeli;
                                    $project->leftovers = $value->kekurangan;
                                
                                    $project->save();
                                
                                
                            } else{
                        
                            continue;
                            }
                        }
                    
                        return redirect('projects/view/'.$a)->with([
                            'message' => 'Item Added'
                        ]);
                    } else{
                        return redirect('projects/view/'.$a)->with([
                            'message' => 'Item Not Found',
                            'message_important' => true
                        ]);
                    } 
                }
                
            }
            return back();
        }
    public function downloadExcelProject()
    {
        $data = Input::get('variable');
        $company = Input::get('company');

        array_push($data,'accident_insurance');
        array_push($data,'life_insurance');
        array_push($data,'jaminan_pensiun');
        array_push($data,'jaminan_hari_tua');
        array_push($data,'insurance_name');

        $where=[];
        if(sizeof($data)>=1){
            if($company!="0"){
                $where['company']=$company;
            }
            
            $type = 'xls';
            $query = Project::select($data)->where($where)->get()->toArray();
            $tags = array_map(function($tag) {
                $ret = $tag;
                if(array_key_exists('company', $ret)){
                    $findcomp=Company::find($ret['company']);
                    if($findcomp){
                        $comp= $findcomp->company;
                    }
                    else{
                        $comp="-";
                    }

                    $ret['Perusahaan'] = $comp;
                    unset($ret['company']);
                }
                if(array_key_exists('project_number', $ret)){
                
                    $ret['Nomor Kontrak Klien'] =   $ret['project_number'];
                    unset($ret['project_number']);
                }
                if(array_key_exists('project', $ret)){
                
                    $ret['Nama Kontrak Klien'] =   $ret['project'];
                    unset($ret['project']);
                }
                if(array_key_exists('facility', $ret)){
                
                    $ret['Fasilitas'] =   $ret['facility'];
                    unset($ret['facility']);
                }
                if(array_key_exists('health_facility_type', $ret)){
                
                    $ret['Jenis Fasilitas Kesehatan'] =   $ret['health_facility_type'];
                    unset($ret['health_facility_type']);
                }
                if(array_key_exists('insurance_name', $ret)){
                
                    $ret['Nama Asuransi Kesehatan'] =   $ret['insurance_name'];
                    unset($ret['insurance_name']);
                }
                if(array_key_exists('accident_insurance', $ret)){
                
                    $ret['Jaminan Kecelakaan Kerja (JKK)'] =   $ret['accident_insurance'];
                    unset($ret['accident_insurance']);
                }
                if(array_key_exists('life_insurance', $ret)){
                
                    $ret['Jaminan Kematian (JKM)'] =   $ret['life_insurance'];
                    unset($ret['life_insurance']);
                }
                if(array_key_exists('jaminan_hari_tua', $ret)){
                
                    $ret['Jaminan Hari Tua (JHT)'] =   $ret['jaminan_hari_tua'];
                    unset($ret['jaminan_hari_tua']);
                }
                if(array_key_exists('jaminan_pensiun', $ret)){
                
                    $ret['Jaminan Pensiun (JP)'] =   $ret['jaminan_pensiun'];
                    unset($ret['jaminan_pensiun']);
                }
                if(array_key_exists('start_date', $ret)){
                
                    $ret['Tanggal Mulai'] =   date('d/m/Y',strtotime($ret['start_date']));
                    unset($ret['start_date']);
                }
                if(array_key_exists('end_date', $ret)){
                
                    $ret['Tanggal Berakhir'] =   date('d/m/Y',strtotime($ret['end_date']));
                    unset($ret['end_date']);
                }
                // if(array_key_exists('value', $ret)){
                
                //     $ret['Nilai'] =   $ret['value'];
                //     unset($ret['value']);
                // }
                if(array_key_exists('status', $ret)){
                    $status = '';
                    if($ret['status']=='opening'){
                        $status = 'Buka';
                    }
                    if($ret['status']=='drafted'){
                        $status = 'Diajukan';
                    }
                    if($ret['status']=='closed'){
                        $status = 'Tutup';
                    }
                    $ret['Status'] =   $status;
                    unset($ret['status']);
                }
                return $ret;
            }, $query);
            return Excel::create('Project', function($excel) use ($tags) {
                $excel->sheet('mySheet', function($sheet) use ($tags)
                {
                    $sheet->fromArray($tags);
                });
            })->download($type);
        }
        else{return redirect('projects')->with([
            'message' => 'No data selected',
            'message_important' => true
        ]);}
    }
    public function downloadExcelProjectExpense()
    {
        $cmd = Input::get('cmd');
  
      
       $data=Input::get('variable');
       if(sizeof($data)>=1){
       $type = 'xls';
        $query = Expense::select($data)->where('project','=',$cmd)->get()->toArray();
      
		return Excel::create('Project', function($excel) use ($query) {
			$excel->sheet('mySheet', function($sheet) use ($query)
	        {
				$sheet->fromArray($query);
	        });
        })->download($type);}
        else{
            return redirect('projects')->with([
                'message' => 'No data selected',
                'message_important' => true
            ]);
        }
    }
        
    /* editPayment  Function Start Here */
    public function editPayment($id)
    {
        $project_needs = ProjectNeeds::where('id_needs','=',$id)->first();
        $payroll_type = PayrollTypes::all();
        return view('admin.project.edit-payment', compact('project_needs','payroll_type'));

    }
    /* editPayment  Function Start Here */
    public function viewPayment($id)
    {
        $project_needs = ProjectNeeds::where('id_needs','=',$id)->first();
        $payroll_type = PayrollTypes::all();
        return view('admin.project.view-payment', compact('project_needs','payroll_type'));

    }
    
    /* editVehicle  Function Start Here */
    public function editVehicle($id)
    {
        $employee = Employee::find($id);
        return view('admin.project.edit-vehicle', compact('employee'));

    }

    /* addProject  Function Start Here */
    public function addProject(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'projects' => 'required',
        ]);

        if ($v->fails()) {
            return redirect('projects')->withErrors($v->errors());
        }

        
        $contract_file          = Input::file('contract_file');
        if($contract_file!=null){
            $contract_file_path     = Input::file('contract_file')->getRealPath();
            $target_file_contract   = $contract_file_path . basename($_FILES["contract_file"]["name"]);
            $imageFileType_contract = pathinfo($target_file_contract,PATHINFO_EXTENSION);
        }

        // $bid_file               = Input::file('bid_file');
        // if($bid_file!=null){
        //     $bid_file_path          = Input::file('bid_file')->getRealPath();
        //     $target_file_bid        = $bid_file_path . basename($_FILES["bid_file"]["name"]);
        //     $imageFileType_bid      = pathinfo($target_file_bid,PATHINFO_EXTENSION);
        // }

        $agreement_file             = Input::file('agreement_file');
        if($agreement_file!=null){
            $agreement_file_path    = Input::file('agreement_file')->getRealPath();
            $target_file_agreement  = $agreement_file_path . basename($_FILES["agreement_file"]["name"]);
            $imageFileType_agreement= pathinfo($target_file_agreement,PATHINFO_EXTENSION);
        }
        $neworduplicate         =Input::get('neworduplicate');

        $projects_name          = Input::get('projects');
        $internal_company       = Input::get('internal_company');
        $company                = Input::get('company');
        $projects_number        = Input::get('project_number');
        $start_date             = Input::get('start_date');
        $start_date             = get_date_format_inggris($start_date);
        $end_date               = Input::get('end_date');
        $end_date               = get_date_format_inggris($end_date);
        // $value                  = str_replace(',', '', Input::get('value'));

        $facility               = Input::get('facility');
        $health_facility_type               = Input::get('health_facility_type');
        $accident_insurance               = Input::get('accident_insurance');
        $jaminan_hari_tua               = Input::get('jaminan_hari_tua');
        $jaminan_pensiun               = Input::get('jaminan_pensiun');
        $life_insurance               = Input::get('life_insurance');
        $insurance_name               = Input::get('insurance_name');

        $designation            = Input::get('designation');
        $location               = Input::get('location');
        $open_date              = Input::get('open_date');
        $close_date             = Input::get('close_date');
        $total                  = Input::get('total');

        if($contract_file!=null){
            if ($imageFileType_contract == 'jpg' || $imageFileType_contract == 'png' || $imageFileType_contract == 'jpeg' || $imageFileType_contract == 'pdf' || $imageFileType_contract == 'xls' || $imageFileType_contract == 'xlsx') {
            } else {
                return redirect('projects')->with([
                    'message' => 'Upload an Image or PDF or Excel File',
                    'message_important' => true
                ]);
            }
        }

        if($agreement_file!=null){
            if ($imageFileType_agreement == 'jpg' || $imageFileType_agreement == 'png' || $imageFileType_agreement == 'jpeg' || $imageFileType_agreement == 'pdf' || $imageFileType_agreement == 'xls' || $imageFileType_agreement == 'xlsx') {
            } else {
                return redirect('projects')->with([
                    'message' => 'Upload an Image or PDF or Excel File',
                    'message_important' => true
                ]);
            }
        }

            $destinationPath            = public_path() . '/assets/project_doc/';

            if($contract_file!=null){
                $file_name_contract         = $contract_file->getClientOriginalName();
                Input::file('contract_file')->move($destinationPath, $file_name_contract);
            }
            // if($bid_file!=null){
            //     $file_name_bid              = $bid_file->getClientOriginalName();
            //     Input::file('bid_file')->move($destinationPath, $file_name_bid);
            // }
            
            if($agreement_file!=null){
                $file_name_agreement        = $agreement_file->getClientOriginalName();
                Input::file('agreement_file')->move($destinationPath, $file_name_agreement);
            }
            //Penomoran id
            $last_number_id = Project::max('id');
            if($last_number_id==''){
                $number_id = 1;
            } else {
                $number_id = 1 + $last_number_id;
            }

            $projects                   = new Project();
            $projects->id               = $number_id;
            $projects->project_number   = $projects_number;
            $projects->internal_company = $internal_company;
            $projects->company          = $company;
            $projects->project          = $projects_name;
            $projects->facility         = json_encode($facility);
            $projects->health_facility_type         = $health_facility_type;
            $projects->accident_insurance         = $accident_insurance;
            $projects->life_insurance         = $life_insurance;
            $projects->jaminan_pensiun         = $jaminan_pensiun;
            $projects->jaminan_hari_tua         = $jaminan_hari_tua;
            $projects->insurance_name         = $insurance_name;
            $projects->start_date       = $start_date;
            
            $projects->status       = 'drafted';
            $projects->end_date         = $end_date;
            // $projects->value            = $value;
        
            $projects->save();

            $proj_id  = $projects->id;

            if($neworduplicate=='duplicate'){
                $daftarkaryawantmp=Input::get('daftarkaryawan');
                $daftarkaryawandecode = html_entity_decode($daftarkaryawantmp);
                $daftarkaryawan = json_decode($daftarkaryawandecode);
                    foreach($daftarkaryawan as $z){
                        $empp= Employee::find($z->id);
                        $empp->project= $proj_id;
                        $empp->save();
                    }
            }
                else{}

            if($contract_file!=null){
                $contract_document_name = Input::get('contract_document_name');
                $project_doc            = new ProjectFiles();
                $project_doc->id_project= $proj_id;
                $project_doc->file_title= $contract_document_name;
                $project_doc->file      = $file_name_contract;
                $project_doc->save();
            }

            // if($bid_file!=null){
            //     $bid_document_name      = Input::get('bid_document_name');
            //     $project_doc            = new ProjectFiles();
            //     $project_doc->id_project= $proj_id;
            //     $project_doc->file_title= $bid_document_name;
            //     $project_doc->file      = $file_name_bid;
            //     $project_doc->save();
            // }
            if($agreement_file!=null){
                $agreement_document_name= Input::get('agreement_document_name');
                $project_doc            = new ProjectFiles();
                $project_doc->id_project= $proj_id;
                $project_doc->file_title= $agreement_document_name;
                $project_doc->file      = $file_name_agreement;
                $project_doc->save();
            }
            //Penomoran surat
            $date = date('d');
            $month = date('m');
            $year = date('Y');
            // $last_letter_number = DB::table('sys_jobs')->orderBy('id','asc')->value('no_position');
            // $last_number = explode('/', $last_letter_number);
            // if($date=='01'){
            //     $number = 01;
            // } else{
            //     $number = 1 + $last_number[0];
            // }
            $last_letter_number = DB::table('sys_jobs')->where('no_position','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('no_position');
            if($last_letter_number==''){
                $number = 1;
            } else{
                $last_number = explode('/', $last_letter_number);
                $number = 1 + $last_number[0];
            }
    
            $num_elements_job           = 0;
            $sql_data_jobs              = array();
            
            while($num_elements_job<count($designation)) {

                $open_datess              = get_date_format_inggris($open_date[$num_elements_job]);
                $close_datess             = get_date_format_inggris($close_date[$num_elements_job]);
                $career = CareerPathEmployee::where('designation','=',$designation[$num_elements_job])->first();
                $sql_data_jobs[] = array(
                    'position'          => $designation[$num_elements_job],
                    'project'           => $proj_id,
                    'company'           => $request->company,
                    'no_position'       => $num_elements_job + $number."/Lowongan"."/".get_roman_letters(date("m"))."/".date("Y"),
                    'job_location'      => $location[$num_elements_job],
                    'experience'        => $career->working_period,
                    'last_education'    => $career->education,
                    'post_date'         => date("Y-m-d"),
                    'apply_date'        => $open_datess,
                    'close_date'        => $close_datess,
                    'quota'             => $total[$num_elements_job],
                );
                $num_elements_job++;
            }

            $id_job                     = [];
            
            foreach($sql_data_jobs as $data_job){
                $id_job[]   = DB::table('sys_jobs')->insertgetId($data_job);
            }

            $num_elements           = 0;
            
            $sql_data               = array();
    
            while($num_elements<count($designation)) {
                
                $open_dates              = get_date_format_inggris($open_date[$num_elements]);
                $close_dates             = get_date_format_inggris($close_date[$num_elements]);

                $sql_data[] = array(
                    'id'                => $proj_id,
                    'id_designation'    => $designation[$num_elements],
                    'id_job'            => $id_job[$num_elements],
                    'location'          => $location[$num_elements],
                    'open_date'         => $open_dates,
                    'close_date'        => $close_dates,
                    'total'             => $total[$num_elements],
                );
                $num_elements++;
            }
    
            $projectneeds               = ProjectNeeds::insert($sql_data);
        
            $notification = new Notification();
            $notification->id_tag = $projects->id;
            $notification->tag = 'projects';
            $notification->title = $projects_name;
            $notification->description = 'Persetujuan Proyek';
            $notification->show_date = date('Y-m-d');
            $notification->start_date = $start_date;
            $notification->end_date = $start_date;
            $notification->route = 'approvals/projects';
            $notification->save();
            return redirect('projects')->with([
                'message' => 'Project Added Successfully'
            ]);

        
        if ($projects!='') {
            return redirect('projects')->with([
                'message' => 'Project Added Successfully'
            ]);

        } else {
            return redirect('projects')->with([
                'message' => 'Project Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateProject  Function Start Here */
    public function updateProject(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'projects' => 'required',
        ]);

        if ($v->fails()) {
            return redirect('projects')->withErrors($v->errors());
        }
        $projects               = Project::find($cmd);
        $project_number          = Input::get('project_number');
        $projects_name          = Input::get('projects');
        $start_date               = Input::get('start_date');
        $start_date               = get_date_format_inggris($start_date);
        $end_date               = Input::get('end_date');
        $end_date               = get_date_format_inggris($end_date);
        // $value                  = str_replace(',', '', Input::get('value'));

        if ($projects) {
            $projects->project          = $projects_name;
            $projects->project_number   = $project_number;
            $projects->start_date         = $start_date;
            $projects->end_date         = $end_date;
            // $projects->value            = $value;
            $projects->save();

            $notification = Notification::where('tag','=','projects')->where('id_tag','=',$cmd)->first();
            if($notification){
                $notification->title = $projects_name;
                $notification->show_date = get_date_format_inggris($end_date.'-30 day');
                $notification->start_date = $end_date;
                $notification->end_date = $end_date;
                $notification->save();
            }

            return redirect('projects')->with([
                'message' => 'Project Updated Successfully'
            ]);

        } else {
            return redirect('projects')->with([
                'message' => 'Project Not Found',
                'message_important' => true
            ]);
        }
    }

    /* updateProjectDetail  Function Start Here */
    public function updateProjectDetail(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'designation' => 'required',
            'location'    => 'required',
            'total'       => 'required'
        ]);

        if ($v->fails()) {
            return redirect('projects/view/'.$cmd)->withErrors($v->errors());
        }
        $projects               = Project::find($cmd);

        if ($projects) {
            $designation            = Input::get('designation');
            $location               = Input::get('location');
            $open_date              = Input::get('open_date');
            $close_date             = Input::get('close_date');
            $total                  = Input::get('total');
            $company                = Input::get('company_id');

            //Penomoran surat
            $date = date('d');
            $month = date('m');
            $year = date('Y');
            // $last_letter_number = DB::table('sys_jobs')->orderBy('id','asc')->value('no_position');
            // $last_number = explode('/', $last_letter_number);
            // if($date=='01'){
            //     $number = 01;
            // } else{
            //     $number = 1 + $last_number[0];
            // }
            $last_letter_number = DB::table('sys_jobs')->where('no_position','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('no_position');
            if($last_letter_number==''){
                $number = 1;
            } else{
                $last_number = explode('/', $last_letter_number);
                $number = 1 + $last_number[0];
            }

            $num_elements_job           = 0;

            $sql_data_jobs              = array();
            if($projects->status="opening"){
                $status_job = 'opening';
            } else {
                $status_job = 'drafted';
            }
            
            while($num_elements_job<count($designation)) {

                $tmpdue = explode(' ',$open_date[$num_elements_job]);
                $open_dates              = get_date_format_inggris($open_date[$num_elements_job]);
                $close_dates             = get_date_format_inggris($close_date[$num_elements_job]);
                $career = CareerPathEmployee::where('designation','=',$designation[$num_elements_job])->first();
                $sql_data_jobs[] = array(
                    'position'          => $designation[$num_elements_job],
                    'project'           => $cmd,
                    'company'           => $company,
                    'no_position'       => $num_elements_job + $number."/Lowongan"."/".get_roman_letters(date("m"))."/".date("Y"),
                    'job_location'      => $location[$num_elements_job],
                    'experience'        => $career->working_period,
                    'last_education'    => $career->education,
                    'post_date'         => date("Y-m-d"),
                    'apply_date'        => $open_dates,
                    'close_date'        => $close_dates,
                    'status'            => $status_job,
                    'quota'             => $total[$num_elements_job]
                );
                $num_elements_job++;
            }

            $num_elements               = 0;

            $id_job                     = [];

            foreach($sql_data_jobs as $data_job){
                $id_job[]   = DB::table('sys_jobs')->insertgetId($data_job);
            }

            $sql_data                   = array();
            
            while($num_elements<count($designation)) {
                
                $open_datess              = get_date_format_inggris($open_date[$num_elements]);
                $close_datess             = get_date_format_inggris($close_date[$num_elements]);
                $sql_data[] = array(
                    'id'                => $cmd,
                    'id_designation'    => $designation[$num_elements],
                    'id_job'            => $id_job[$num_elements],
                    'location'          => $location[$num_elements],
                    'open_date'         => $open_datess,
                    'close_date'        => $close_datess,
                    'total'             => $total[$num_elements],
                );
                $num_elements++;
            }
            // echo "<pre>"; print_r($sql_data); print_r($sql_data_jobs); exit;
            $projectneedsupdate     = ProjectNeeds::insert($sql_data);

            return redirect('projects/view/'.$cmd)->with([
                'message' => 'Project Updated Successfully'
            ]);

        } else {
            return redirect('projects/view/'.$cmd)->with([
                'message' => 'Project Not Found',
                'message_important' => true
            ]);
        }
    }


    /* deleteProject  Function Start Here */
    public function deleteProject($id)
    {

        $projects = Project::find($id);
        if ($projects) {

            ProjectNeeds::where('id', $id)->delete();
            ProjectItem::where('id_project', $id)->delete();
            ProjectFiles::where('id_project', $id)->delete();
            ProjectVehicle::where('id_project', $id)->delete();
            Jobs::where('project', $id)->delete();
            Employee::where('project', $id)->update(['status' => 'inactive']);
            $projects->delete();

            return redirect('projects')->with([
                'message' => 'Project Deleted Successfully'
            ]);
        } else {
            return redirect('projects')->with([
                'message' => 'Project Not Found',
                'message_important' => true
            ]);
        }

    }
  
    public function getdetailproject(Request $request){
        $id=$request->id;
        if($id){
            $data['project']=Project::where('id','=',$id)->first();
        $data['project_file']=ProjectFiles::where('id_project','=',$id)->get();
        $data['project_need']=ProjectNeeds::where('id','=',$id)->get();
        $data['employee']  = Employee::where('project', '=', $id)->where('role_id','!=',1)->get();

        $data['des']=Designation::all();
        echo json_encode($data);}
    }
    public function getdesignationname(Request $request){
        $id=$request->id;
        if($id){
            $data['des']=Designation::where('id','=',$id)->first();
      
        echo json_encode($data);}
    }
    /* viewProject  Function Start Here */
    public function viewProject($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 5)->first();
        $tabs='';

        if(Session::has('tabs')){
            $tabs= Session::get('tabs');
         
        }
        $projectview    = Project::find($id);
        $job            = Jobs::where('project','=',$id)->get();
        $projects       = Project::all();

        if ($projectview) {

            $employee_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
            $employees              = Employee::where('project', '=', $id)->where('role_id','!=',1)->count();
            $jobs                   = Jobs::where('project', '=', $id)->count();
            $jobs_closed            = Jobs::where([
                                                    ['project', '=', $id],
                                                    ['status', '=', 'closed'],
                                    ])->count();
            $job_applicants         = JobApplicants::where('project', '=', $id)->count();
            $job_applicants_confirm = JobApplicants::where([
                                                    ['project', '=', $id],
                                                    ['status', '=', 'Confirm'],
                                    ])->count();
            $company                = Company::all();
            $designation            = Designation::all();
            $payment                = PayrollTypes::where('status','=','active')->get();
            $employee               = Employee::where('project', '=', $id)->where('role_id','!=',1)->where('status','active')->get();
            $projectneeds           = ProjectNeeds::where('id', '=', $id)->get();
            $projectneedsview       = ProjectNeeds::find($id);
            $project_doc            = ProjectFiles::where('id_project', '=', $id)->get();
            $expenses                = Expense::where('project','=',$id)->where(function($q) {
                $q->where('status','=', 'Approved')
                  ->orWhere('status','=', 'Edited');
            })->sum('total');
            //Penomoran surat
            $date = date('d');
            $month = date('m');
            $year = date('Y');
            // $last_letter_number = DB::table('sys_expense')->orderBy('id','asc')->value('expense_number');
            // $last_number = explode('/', $last_letter_number);
            // if($date=='01'){
            //     $number = 01;
            // } else{
            //     $number = 1 + $last_number[0];
            // }
            $last_letter_number = DB::table('sys_expense')->where('expense_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('expense_number');
            if($last_letter_number==''){
                $number = 1;
            } else{
                $last_number = explode('/', $last_letter_number);
                $number = 1 + $last_number[0];
            }
            $subcategories = Subcategory::where('status','=','active')->get();
            $company = Company::find(\Auth::user()->company);

            // if($projectview->end_date<=date('Y-m-d')){
            //     if($projectview->status=='opening'){
            //         $projectview->status - 'closed';
            //         $projectview->save();
            //     }
            // }

            $expense = Expense::where('project','=', $id)->orderBy('id','asc')->get();
            $item = ProjectItem::where('id_project','=', $id)->orderBy('id','asc')->get();
            return view('admin.project.view-projects', compact('item','expense','projects','number','month','year','job_applicants_confirm','job_applicants','job_view','job','jobs_closed','jobs','projectview','projectneedsview','company','designation','payment','location','employee','projectneeds','project_doc','employees','expenses','subcategories','tabs','employee_permission','permcheck'));
        }
    }

    /* viewProjectExpense  Function Start Here */
    public function viewProjectExpense($id)
    {
        $expense = Expense::find($id);
        if ($expense) {

            $expense_detail = ExpenseDetail::where('expense_id', '=', $id)->get();

            return view('admin.project.view-expense', compact('expense','expense_detail'));
        }
    }

    public function downloadProjectExpense($id)
    {
        $expense = Expense::find($id);
        if ($expense) {
            $kata= self::Terbilang($expense->total) ." Rupiah";

            $expense_detail = ExpenseDetail::where('expense_id', '=', $id)->get();

            $pdf=WkPdf::loadView('admin.project.pdf-expense', compact('expense','expense_detail','kata'));

            return $pdf->stream("ProjectExpenses.pdf");
        
        }
    }

    function Terbilang($x) 
    { 
      $abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"); 
      if ($x < 12) 
        return " " . $abil[$x]; 
      elseif ($x < 20) 
        return $this->Terbilang($x - 10) . "Belas"; 
      elseif ($x < 100) 
        return $this->Terbilang($x / 10) . " Puluh" . $this->Terbilang($x % 10); 
      elseif ($x < 200) 
        return " seratus" . $this->Terbilang($x - 100); 
      elseif ($x < 1000) 
        return $this->Terbilang($x / 100) . " Ratus" . $this->Terbilang($x % 100); 
      elseif ($x < 2000) 
        return " seribu" . $this->Terbilang($x - 1000); 
      elseif ($x < 1000000) 
        return $this->Terbilang($x / 1000) . " Ribu" . $this->Terbilang($x % 1000); 
      elseif ($x < 1000000000) 
        return $this->Terbilang($x / 1000000) . " Juta" . $this->Terbilang($x % 1000000); 
        
        
    } 
        public function downloadProjectExpenseKwitansi($id)
        {
            $expense = Expense::find($id);
            if ($expense) {
                    $kata= self::Terbilang($expense->total) ." Rupiah";
                $expense_detail = ExpenseDetail::where('expense_id', '=', $id)->get();
    
                $pdf=WkPdf::loadView('admin.project.pdf-expense-kwitansi', compact('expense','expense_detail','kata'));
    
                return $pdf->stream("ProjectExpenses.pdf");
            
            }
        }
    /* viewProjectVehicle  Function Start Here */
    public function viewProjectVehicle($id)
    {
        $vehicle = ProjectVehicle::find($id);
        if ($vehicle) {

            return view('admin.project.view-vehicle', compact('vehicle'));
        }
    }
    /* closingProject  Function Start Here */
    public function closingProject(Request $request)
    {
        $cmd = Input::get('cmd');
        $projects = Project::find($cmd);
        if ($projects) {
            $projects->status = 'closed';
            $projects->save();

            return redirect('projects/view/'.$cmd)->with([
                'message' => 'Project Closed Successfully'
            ]);

        }
    }

    /* getComponent Function Start Here */
    public function getComponent(Request $request)
    {

        $pay_id = $request->pay_id;
        if ($pay_id) {
            echo '<label>Components</label>';
            $payroll_type = PayrollTypes::find($pay_id);
            $component = PayrollComponent::where('id_payroll_type','=',$pay_id)->get();
            echo '<table class="table data-table table-hover table-ultra-responsive">';
            echo '<thead>';
                echo '<tr>';
                    echo '<td >'.language_data('Component Name').'</td>';
                    echo '<td >'.language_data('Unit Component').'</td>';
                    echo '<td >'.language_data('Unit Nominal').'</td>';
                    echo '<td >'.language_data('Converter').'</td>';
                    echo '<td >'.language_data('Nominal').'</td>';
                echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            foreach($component as $c){
                echo '<tr>';
                    echo '<td>'. $c->component_name->component . '</td>';
                    echo '<td>'. $c->unit_name->unit . '</td>';
                    echo '<td><span class="pull-right">'. number_format($c->unit_nominal,0) . '</span></td>';
                    echo '<td><span class="pull-right">'. number_format($c->converter,0) . '</span></td>';
                    echo '<td><span class="pull-right">'. number_format($c->value,0) . '</span></td>';
                echo '</tr>';
            }
            echo '</tbody>';
            echo '<tfoot>';
                echo '<tr>';
                    echo '<td colspan="4">Gaji Diterima </td>';
                    echo '<td><span class="pull-right">'. number_format($payroll_type->total,0) . '</span></td>';
                echo '</tr>';
            echo '</tfoot>';
            echo '</table>';
        }
    }

    /* download excel function start here */
    public function downloadExcel(Request $request)
    {
        $data = Input::get('variable');
        $banks= Input::get('banks');
        if(isset($banks)){
         $data= array_merge($data,$banks);
        }
        array_push($data,'sys_employee.id');
     //    array_push($data,'last_year');
        array_push($data,'project');
     //    array_push($data,'lname');
        $project = Input::get('project');
        $company = Input::get('company');
     //    echo "<pre>";
     //    print_r($data);
     //    exit;
 
        if($project!=0){
            $query = Employee::select($data)->where('project','=', $project)->where('role_id','!=','1')->leftJoin('sys_employee_bank_accounts as banks', 'sys_employee.id', '=', 'banks.emp_id')->distinct()->groupBy('sys_employee.id')->get()->toArray();
        } else if($company!=0){
            $query = Employee::select($data)->where('company','=', $company)->where('role_id','!=','1')->leftJoin('sys_employee_bank_accounts as banks', 'sys_employee.id', '=', 'banks.emp_id')->distinct()->groupBy('sys_employee.id')->get()->toArray();
        } else{
            $query = Employee::select($data)->where('role_id','!=','1')->leftJoin('sys_employee_bank_accounts as banks', 'sys_employee.id', '=', 'banks.emp_id')->distinct()->groupBy('sys_employee.id')->get()->toArray();
        }
            $ct=0;
        foreach($query as $z){
             foreach($data as $d){
                 if (strpos($d, 'banks.') !== false){
                     $split = explode(".", $d);
                     $d =  $split[1];
             
                 } else if (strpos($d, 'sys_employee.') !== false){
                     $split = explode(".", $d);
                     $d =  $split[1];
             
                 }
                 if($z[$d] == null || $z[$d] == '-'){
                     if($d!= 'lname'){
                         $query[$ct][$d]="0";
                     }
                 }
             }
             $ct++;
        }
     //    echo "<pre>";
     //    print_r($query);
     //    exit;
        $data2 = array();
        $ctr = 0;
        foreach($query as $q){

            if(isset($q['employee_code'])){
                $data2[$ctr]['NIK']                 = $q['employee_code'];
            }
            if(isset($q['fname'])){
                $data2[$ctr]['Nama Karyawan']       = $q['fname'];
                
            }
            if(isset($q['gender'])){
                if($q['gender'] ==  'male'){
                    $data2[$ctr]['Jenis Kelamin']       = 'Laki-laki';
                } else if($q['gender'] ==  'female'){
                    $data2[$ctr]['Jenis Kelamin']       = 'Perempuan';
                }
            }
            if(isset($q['no_ktp'])){
                $data2[$ctr]['No. KTP']             = $q['no_ktp'];
                
            }
            if(isset($q['birth_place'])){
                $data2[$ctr]['Tempat Lahir']        = $q['birth_place'];
                
            }
            if(isset($q['dob'])){
                $data2[$ctr]['Tanggal Lahir']       = $q['dob'];
                
            }
            if(isset($q['skck'])){
                $data2[$ctr]['No. SKCK']            = $q['skck'];
                
            }
            if(isset($q['per_address'])){
                $data2[$ctr]['Alamat (KTP)']         = $q['per_address'];
                
            }
            if(isset($q['per_village'])){
                $data2[$ctr]['Kelurahan (KTP)']      = $q['per_village'];
                
            }
            if(isset($q['per_district'])){
                $data2[$ctr]['Kecamatan (KTP)']      = $q['per_district'];
                
            }
            if(isset($q['per_city'])){
                $data2[$ctr]['Kota (KTP)']           = $q['per_city'];
                
            }
            if(isset($q['pre_address'])){
                $data2[$ctr]['Alamat Domisili']     = $q['pre_address'];
                
            }
            if(isset($q['pre_village'])){
                $data2[$ctr]['Kelurahan Domisili']      = $q['pre_village'];
                
            }
            if(isset($q['pre_district'])){
                $data2[$ctr]['Kecamatan Domisili']  = $q['pre_district'];
                
            }
            if(isset($q['pre_city'])){
                $data2[$ctr]['Kota Domisili']       = $q['pre_city'];
                
            }
            if(isset($q['phone'])){
                $data2[$ctr]['Nomor Telepon']       = $q['phone'];
                
            }
            if(isset($q['email'])){
                $data2[$ctr]['Email']               = $q['email'];
            
            }
            if(isset($q['religion'])){
                $data2[$ctr]['Agama']               = $q['religion'];
                
            }
            if(isset($q['last_education'])){
                $le=LastEducation::find($q['last_education']);
                if($le){
                $data2[$ctr]['Pendidikan Terakhir'] = $le->education;}
                else{  $data2[$ctr]['Pendidikan Terakhir'] = "-";}
                
            }
            if(isset($q['project'])){
                $le=Project::find($q['project']);
                if($le){
                $data2[$ctr]['Proyek'] = $le->project;}
                else{  $data2[$ctr]['Proyek'] = "-";}
                
            }
            if(isset($q['university'])){
                $data2[$ctr]['Lembaga Institusi']   = $q['university'];
                
            }
            // if(isset($q['first_year'])){
            //     $data2[$ctr]['Tahun Tempuh']        = $q['last_year']-$q['first_year'];
                
            // }
            if(isset($q['marital_status'])){
                $data2[$ctr]['Status Perkawinan']   = $q['marital_status'];
                
            }
            if(isset($q['child'])){
                $data2[$ctr]['Jumlah Tanggungan']   = $q['child'];
                $query_anak = EmployeeDependents::where('emp_id','=', $q['id'])->get()->toArray();
                
                if(isset($query_anak[0]['child_name'])){
                    $data2[$ctr]['Anak 1']   = $query_anak[0]['child_name'];
                
                } else {
                    $data2[$ctr]['Anak 1']   = '';
                }
                if(isset($query_anak[1]['child_name'])){
                    $data2[$ctr]['Anak 2']   = $query_anak[1]['child_name'];
                
                } else {
                    $data2[$ctr]['Anak 2']   = '';
                }
                if(isset($query_anak[2]['child_name'])){
                    $data2[$ctr]['Anak 3']   = $query_anak[2]['child_name'];
                
                } else {
                    $data2[$ctr]['Anak 3']   = '';
                }
            }
            if(isset($q['spouse'])){
                $data2[$ctr]['Nama Suami Atau Istri']    = $q['spouse'];
                
            }
            if(isset($q['designation'])){
                $des=Designation::find($q['designation']);
                if($des){
                $data2[$ctr]['Jabatan/Posisi']      = $des->designation;}
                else{  $data2[$ctr]['Jabatan/Posisi']      = "-"; }
                
            }
            if(isset($q['doj'])){
                $data2[$ctr]['Tanggal Masuk']       = $q['doj'];
                
            }
            if(isset($q['location'])){
                $data2[$ctr]['Lokasi']              = $q['location'];
                
            }
            if(isset($q['employee_status'])){
                $data2[$ctr]['Status Karyawan']     = $q['employee_status'];
                
            }
            if(isset($q['bank_name'])){
                $data2[$ctr]['Nama Bank']     = $q['bank_name'];
                
            }
            if(isset($q['branch_name'])){
                $data2[$ctr]['Nama Cabang']     = $q['branch_name'];
                
            }
            if(isset($q['account_name'])){
                $data2[$ctr]['Nama Akun']     = $q['account_name'];
                
            }
            if(isset($q['account_number'])){
                $data2[$ctr]['Nomor Akun']     = $q['account_number'];
                
            }
            if(isset($q['tax_status'])){
                $data2[$ctr]['Status Pajak']     = $q['tax_status'];
                
            }
            if(isset($q['npwp_status'])){
                $data2[$ctr]['Status NPWP']     = $q['npwp_status'];
                
            }
            if(isset($q['npwp_number'])){
                $data2[$ctr]['Nomor NPWP']     = $q['npwp_number'];
                
            }
            if(isset($q['employee_bpjs'])){
                $data2[$ctr]['BPJS Karyawan']     = $q['employee_bpjs'];
                
            }
            if(isset($q['health_bpjs'])){
                $data2[$ctr]['BPJS Kesehatan']     = $q['health_bpjs'];
                
            }
            if(isset($q['other_insurance_name'])){
                $data2[$ctr]['Nama Asuransi Lain 1']     = $q['other_insurance_name'];
                
            }
            if(isset($q['other_insurance_name2'])){
                $data2[$ctr]['Nama Asuransi Lain 2']     = $q['other_insurance_name2'];
                
            }
            if(isset($q['other_insurance_name3'])){
                $data2[$ctr]['Nama Asuransi Lain 3']     = $q['other_insurance_name3'];
                
            }
            if(isset($q['other_insurance_number'])){
                $data2[$ctr]['Nomor Asuransi Lain 1']     = $q['other_insurance_number'];
                
            }
            if(isset($q['other_insurance_number2'])){
                $data2[$ctr]['Nomor Asuransi Lain 2']     = $q['other_insurance_number2'];
                
            }
            if(isset($q['other_insurance_number3'])){
                $data2[$ctr]['Nomor Asuransi Lain 3']     = $q['other_insurance_number3'];
            }
            if(isset($q['field_1'])){
                $data2[$ctr]['Field 1']     = $q['field_1'];
            }
            if(isset($q['field_2'])){
                $data2[$ctr]['Field 2']     = $q['field_2'];
            }
            if(isset($q['field_3'])){
                $data2[$ctr]['Field 3']     = $q['field_3'];
            }
            if(isset($q['field_4'])){
                $data2[$ctr]['Field 4']     = $q['field_4'];
            }
            if(isset($q['field_5'])){
                $data2[$ctr]['Field 5']     = $q['field_5'];
            }

            $component = Component::lists('component')->toArray();
            foreach($component as $comp){
                $data2[$ctr][$comp] = 0;
            }
            if($q['payment_type'] != 0){
                $payrollcomponent = PayrollComponent::where('id_payroll_type','=',$q['payment_type'])->get();
                foreach($payrollcomponent as $c){
                    $data2[$ctr][$c->component_name->component] = number_format($c->value,0);
                }
            }
           
            $ctr++;
        }
       
     //    echo "<pre>";
     //    print_r($data2);
     //    exit;
       
        $type = 'xls';
        return Excel::create('Employee', function($excel) use ($data2) {
            $excel->sheet('mySheet', function($sheet) use ($data2)
            {
                $sheet->fromArray($data2);
            });
        })->download($type);
    }

    /* addDocument  Function Start Here */
    public function addDocument(Request $request)
    {
        Session::flash('tabs', 'project_contract_file'); 

        $cmd = Input::get('cmd');

        $v = \Validator::make($request->all(), [
            'document_name' => 'required','file' => 'required', 'cmd' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('projects/view/' . $cmd)->withErrors($v->errors());
        }

        $document_name=Input::get('document_name');
        $file = Input::file('file');
        $destinationPath_file = public_path() . '/assets/project_doc/';
        $target_file = $destinationPath_file . basename($_FILES["file"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        $exist=ProjectFiles::where('file_title',$document_name)->where('id_project',$cmd)->first();

        if($exist){
            return redirect('projects/view/' . $cmd)->with([
                'message' => language_data('This Document Already Exist'),
                'message_important' => true
            ]);
        }

        $projects = Project::find($cmd);

        if ($projects) {
            if ($imageFileType == 'jpg' OR $imageFileType == 'png' OR $imageFileType == 'jpeg' OR $imageFileType == 'pdf') {
                $destinationPath = public_path() . '/assets/project_doc/';
                $file_name = $file->getClientOriginalName();
                Input::file('file')->move($destinationPath, $file_name);

                $project_doc=new ProjectFiles();
                $project_doc->id_project=$projects->id;
                $project_doc->file_title=$document_name;
                $project_doc->file=$file_name;
                $project_doc->save();

                return redirect('projects/view/' . $cmd)->with([
                    'message' => language_data('Document Uploaded Successfully')
                ]);

            } else {
                return redirect('projects/view/' . $cmd)->with([
                    'message' => 'Upload an Image or PDF',
                    'message_important' => true
                ]);
            }
        } else {
            return redirect('projects/all')->with([
                'message' => language_data('Project Not Found'),
                'message_important' => true
            ]);
        }
    }

    /* downloadProjectDocument  Function Start Here */
    public function downloadProjectDocument($id)
    {
        $file = ProjectFiles::find($id)->file;
        return response()->download(public_path('assets/project_doc/' . $file));
    }


    /* deleteProjectDoc  Function Start Here */
    public function deleteProjectDoc($id)
    {

        $project_doc=ProjectFiles::find($id);
        $file=$project_doc->file;
        $cmd=$project_doc->id_project;
        if($project_doc){
            \File::delete(public_path('assets/project_doc/' . $file));
            $project_doc->delete();

            return redirect('projects/view/'.$cmd)->with([
                'message'=>language_data('Document Deleted Successfully')
            ]);

        }else{
            return redirect('projects/view/'.$cmd)->with([
                'message'=>language_data('Document Not Found'),
                'message_important'=>true
            ]);
        }
    }

    public function updateExpense(Request $request)
    {

        $cmd = Input::get('cmd');

        $total          = str_replace(",", "", Input::get('alltotal'));
        $management_fee = Input::get('fee');
        $pembulatan     = str_replace(",", "",  Input::get('pembulatan'));
        $ppn_tax        = str_replace(",", "",  Input::get('ppn'));
        $post_date      = Input::get('post_date');
        $post_date      = get_date_format_inggris($post_date);
        $item           = Input::get('item');
        $ppnper           = Input::get('ppnper');
        $keterangan = Input::get('keterangan');
        if($keterangan=='on'){
            $keterangan=1;
        }
        else{  $keterangan=0;}
        $qty            = Input::get('qty');
        $unit_price     = str_replace(",", "", Input::get('unit'));
        $total_price    = str_replace(",", "", Input::get('total'));
        $status="Edited";
            $project_expense=Expense::find($cmd);
            $project_expense->keterangan=$keterangan;

            $project_expense->date=$post_date;
            $project_expense->total=$total;
            $project_expense->status=$status;
            $project_expense->management_fee=$management_fee;
            $project_expense->pembulatan=$pembulatan;
            $project_expense->ppn_tax=$ppn_tax;
            $project_expense->total_edit=1;
            $project_expense->save();
            
            $notification = Notification::where('tag','=','expense')->where('id_tag','=',$cmd)->first();
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->start_date = $post_date;
                $notification->end_date = $post_date;
                $notification->save();
            }

            
            $num_elements               = 0;

            $sql_data                   = array();
            
            $delexpensedetail=  ExpenseDetail::where('expense_id','=', $cmd)->delete();
            while($num_elements<count($item)) {
                $sql_data[] = array(
                    'expense_id'  => $cmd,
                    'item'        => $item[$num_elements],
                    'qty'         => $qty[$num_elements],
                    'ppn'         =>$ppnper[$num_elements],
                    'unit_price'  => $unit_price[$num_elements],
                    'total_price' => $total_price[$num_elements],
                );
                $num_elements++;
            }
            
            $expense_detail     = ExpenseDetail::insert($sql_data);



            return redirect('projects/view-expense/' . $cmd)->with([
                'message' => language_data('Expense Added Successfully')
            ]);

       
    }

    public function checkExist(Request $request){
        $id=$request->project_number;
        if($id){
            $data=Project::where('project_number','=',$id)->get();
            if(sizeof($data)>0){
                echo "0";
            }
            else{
                echo "1";
            }
        }
        else{
            echo "0";
        }
    }

    /* addExpense  Function Start Here belum fix */
    public function addExpense(Request $request)
    {
        Session::flash('tabs', 'expense'); 

        $cmd = Input::get('cmd');

        $v = \Validator::make($request->all(), [
            'expense_number' => 'required','post_date' => 'required', 'cmd' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('projects/view/' . $cmd)->withErrors($v->errors());
        }

        $expense_number = Input::get('expense_number');
        $post_date      = Input::get('post_date');
        $post_date      = get_date_format_inggris($post_date);
        $company        = Input::get('company');
        $projects       = Project::find($cmd);
        $about          = Input::get('about');
        $pembulatan     = str_replace(",", "", Input::get('pembulatan'));
        $total          = str_replace(",", "", Input::get('alltotal'));
        $management_fee = Input::get('fee');
        $ppn_tax        = str_replace(",", "",  Input::get('ppn'));
        $keterangan = Input::get('keterangan');
        if($keterangan=='on'){
            $keterangan=1;
        }
        else{  $keterangan=0;}
        $item           = Input::get('item');
        $ppnper           = Input::get('ppnper');
        $qty            = Input::get('qty');
        $unit_price     = str_replace(",", "", Input::get('unit'));
        $total_price    = str_replace(",", "", Input::get('total'));

        if ($projects) {

            $project_expense=new Expense();
            $project_expense->keterangan=$keterangan;

            $project_expense->expense_number=$expense_number;
            $project_expense->date=$post_date;
            $project_expense->project=$cmd;
            $project_expense->company=$company;
            $project_expense->about=$about;
            $project_expense->total=$total;
            $project_expense->pembulatan=$pembulatan;
            $project_expense->management_fee=$management_fee;
            $project_expense->ppn_tax=$ppn_tax;
            $project_expense->save();

            $expense_id = $project_expense->id;
        
            $notification = new Notification();
            $notification->id_tag = $projects->id;
            $notification->tag = 'expense';
            $notification->title = 'Persetujuan Tagihan '.$expense_number;
            $notification->description = 'Persetujuan Tagihan '.$expense_number;
            $notification->show_date = date('Y-m-d');
            $notification->start_date = $post_date;
            $notification->end_date = $post_date;
            $notification->route = 'approvals/expenses';
            $notification->save();
            
            $num_elements               = 0;

            $sql_data                   = array();
            
            while($num_elements<count($item)) {
                $sql_data[] = array(
                    'expense_id'  => $expense_id,
                    'item'        => $item[$num_elements],
                    'ppn'         => $ppnper[$num_elements],
                    'qty'         => $qty[$num_elements],
                    'unit_price'  => $unit_price[$num_elements],
                    'total_price' => $total_price[$num_elements],
                );
                $num_elements++;
            }
            
            $expense_detail     = ExpenseDetail::insert($sql_data);



            return redirect('projects/view/' . $cmd)->with([
                'message' => language_data('Expense Added Successfully')
            ]);

        } else {
            return redirect('projects/all')->with([
                'message' => language_data('Project Not Found'),
                'message_important' => true
            ]);
        }
    }
    /* addItem  Function Start Here */
    public function addItem(Request $request)
    {
        Session::flash('tabs', 'item'); 

        $cmd = Input::get('cmd');

        $v = \Validator::make($request->all(), [
            'item' => 'required','amount' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('projects/view/' . $cmd)->withErrors($v->errors());
        }

        $projects       = Project::find($cmd);
        
        $item           = Input::get('item');
        $amount         = Input::get('amount');

        if ($projects) {

            $project_item=new ProjectItem();
            $project_item->id_project=$cmd;
            $project_item->item=$item;
            $project_item->amount=$amount;
            $project_item->leftovers=$amount;
            $project_item->save();

            return redirect('projects/view/' . $cmd)->with([
                'message' => 'Item Added Successfully'
            ]);

        } else {
            return redirect('projects/all')->with([
                'message' => language_data('Project Not Found'),
                'message_important' => true
            ]);
        }
    }
    
    /* setVehicle  Function Start Here */
    public function setVehicle(Request $request)
    {
        Session::flash('tabs', 'vehicle'); 

        $cmd        = Input::get('cmd');

        $v = \Validator::make($request->all(), [
            'brand' => 'required',
            'type' => 'required',
            'tmt' => 'required',
            'license_plate' => 'required',
            'vehicle_registration_date' => 'required',
            'tax_due_date' => 'required',
            'insurance_vendor' => 'required',
            'vehicle_vendor' => 'required',
            'car_rental_value' => 'required'
            
        ]);
        $employee = Employee::find($cmd);

        if ($v->fails()) {
            return redirect('projects/view/'.$employee->project)->withErrors($v->errors());
        }

        $brand = Input::get('brand');
        $type = Input::get('type');
        $tmt = Input::get('tmt');
        $tmt = get_date_format_inggris($tmt);
        $license_plate = Input::get('license_plate');
        $vehicle_registration_date = Input::get('vehicle_registration_date');
        $vehicle_registration_date = get_date_format_inggris($vehicle_registration_date);
        $tax_due_date = Input::get('tax_due_date');
        $tax_due_date = get_date_format_inggris($tax_due_date);
        $insurance_vendor = Input::get('insurance_vendor');
        $vehicle_vendor = Input::get('vehicle_vendor');
        $car_rental_value = Input::get('car_rental_value');


        if($employee){
            $project_vehicle = new ProjectVehicle();
            $project_vehicle->id_project = $employee->project;
            $project_vehicle->emp_id = $employee->id;
            $project_vehicle->brand = $brand;
            $project_vehicle->type = $type;
            $project_vehicle->tmt = $tmt;
            $project_vehicle->license_plate = $license_plate;
            $project_vehicle->vehicle_registration_date = $vehicle_registration_date;
            $project_vehicle->tax_due_date = $tax_due_date;
            $project_vehicle->insurance_vendor = $insurance_vendor;
            $project_vehicle->vehicle_vendor = $vehicle_vendor;
            $project_vehicle->car_rental_value = $car_rental_value;
            $project_vehicle->save();

            $vehicle_id = $project_vehicle->id;

            $employee->vehicle = $vehicle_id;
            $employee->save();

            return redirect('projects/view/' . $employee->project)->with([
                'message' => 'Vehicle Added Successfully'
            ]);

        } else {
            return redirect('projects/all')->with([
                'message' => language_data('Project Not Found'),
                'message_important' => true
            ]);
        }

    }



    /* setPaymentType  Function Start Here */
    public function setPaymentType(Request $request)
    {
        Session::flash('tabs', 'payment'); 
        $proj_id        = Input::get('proj_id');
        $des_id         = Input::get('des_id');
        $cmd            = Input::get('cmd');
        $last_id = DB::table('sys_employee')->orderBy('id','asc')->value('id');

        $v = \Validator::make($request->all(), [
            'payment_type' => 'required',
        ]);

        if ($v->fails()) {
            return redirect('projects/view/'.$proj_id)->withErrors($v->errors());
        }

        $paytype = Input::get('payment_type');
        $payroll = PayrollTypes::find($paytype);
        $projectneeds = ProjectNeeds::where('id_needs', '=', $cmd)->update(['id_payment' => $paytype]);
        $employee = Employee::where('project','=',$proj_id)->where('designation','=',$des_id)->get();
        foreach($employee as $emp){
            $emp->payment_type = $paytype;
            $emp->save();
            $history = EmployeePaymentHistory::where('emp_id','=',$emp->id)->where('payroll','=',$payroll->id)->first();
            if($history){

            } else {
                $employee_payment_history = new EmployeePaymentHistory();
                $employee_payment_history->emp_id = $emp->id;
                $employee_payment_history->payroll = $payroll->id;
                $employee_payment_history->salary = $payroll->total;
                $employee_payment_history->save();
            }
        }


        return redirect('projects/view/'.$proj_id)->with([
            'message' => 'Project Updated Successfully'
        ]);

    }


}
