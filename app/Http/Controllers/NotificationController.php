<?php

namespace App\Http\Controllers;
use DB;
use App\Classes\permission;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

date_default_timezone_set(app_config('Timezone'));

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* notification  Function Start Here */
    public function notifications()
    {
        $role_id= \Auth::user()->role_id;
        $whereString ="route in (select sys_menu.url from sys_menu JOIN sys_employee_roles_permission ON sys_menu.id = sys_employee_roles_permission.perm_id WHERE sys_employee_roles_permission.role_id= ".$role_id." AND sys_employee_roles_permission.R =1) OR route='public/calendar'";
        $notifications = Notification::where(DB::raw($whereString))->where('end_date','<=',date('Y-12-31'))->where('start_date','>=',date('Y-01-31'))->orderBy('id','asc')->get();
        return view('admin.notification.notifications', compact('notifications'));
    }
}
