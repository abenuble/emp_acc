<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Schedule;
use App\ScheduleComponent;
use App\ScheduleEmployee;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\permission;
use App\Attendance;
use App\Company;
use App\ReferenceLetter;
use App\AssessmentDimension;
use App\EmployeeAssessmentAspect;
use App\EmployeeAssessmentViolation;
use App\EmployeeRolesPermission;
use App\ViolationTypes;
use App\Appointment;
use App\EmployeePaymentHistory;
use App\EmployeeAssessment;
use App\EmailTemplate;
use App\EmployeeAssessmentReport;
use App\EmployeeResign;
use App\Expense;
use App\ExpenseDetail;
use App\Project;
use App\ProjectNeeds;
use App\ProjectItem;
use App\CareerPathEmployee;
use App\CareerPathEmployeeCompetence;
use App\CareerPathEmployeeAssessment;
use App\Copy;
use App\Contract;
use App\ContractRecipient;
use App\Component;
use App\Jobs;
use App\Leave;
use App\LeaveType;
use App\LeaveOutsourceReport;
use App\LeaveInternalReport;
use App\Ledger;
use App\Mutation;
use App\StatusChange;
use App\SuratPengantar;
use App\SuratPengantarRecipient;
use App\Notification;
use App\Warning;
use App\Resign;
use App\PkbLetter;
use App\Phk;
use App\PhkGround;
use App\Procurement;
use App\ProcurementGoods;
use App\Payment;
use App\PaymentComponent;
use App\PayrollTypes;
use App\PayrollComponent;
use App\Employee;
use App\EmploymentHistory;
use App\EndContract;
use App\EndContractRecipient;
use App\EndContractConcern;
use App\EndContractCopy;
use App\OvertimeWarrant;
use Illuminate\Support\Facades\Input;
use DateTime;
use DatePeriod;
use DateInterval;
use DB;
use WkPdf;

class ApprovalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* pkbletters  Function Start Here */
    public function pkbletters()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 69)->first();
        $pkbletters = PkbLetter::where('category','=','pkb')->orderBy('id','asc')->get();
        return view('admin.approval.pkb-letters', compact('pkbletters','permcheck'));

    }
    public function outletters()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 70)->first();
        $pkbletters = PkbLetter::where('category','=','out')->orderBy('id','asc')->get();
        return view('admin.approval.out-letters', compact('pkbletters','permcheck'));

    }

    /* viewPkbletter  Function Start Here */
    public function viewPkbletter($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 69)->first();
        $pkbletters = PkbLetter::find($id);
        return view('admin.approval.view-pkb-letter', compact('pkbletters','permcheck'));
    }
    public function viewOutletter($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 70)->first();
        $pkbletters = PkbLetter::find($id);
        return view('admin.approval.view-out-letter', compact('pkbletters','permcheck'));
    }
    /* setPkbletterStatus  Function Start Here */
    public function setPkbletterStatus(Request $request)
    {
        $information= Input::get('information');

        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/pkb-letters')->withErrors($v->fails());
        }

        $pkbletters  = PkbLetter::find($cmd);
        if($pkbletters){
            $pkbletters->status = $request->status;
            $pkbletters->information = $information;

            $pkbletters->save();

            return redirect('approvals/pkb-letters')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/pkb-letters')->with([
                'message' => 'Letter not found',
                'message_important'=>true
            ]);
        }

    }

    public function setEmployeeAssesmentStatus(Request $request)
    {
        $cmd=Input::get('cmd');
            
        $pkbletters  = employeeAssessment::find($cmd);
        if($pkbletters){
            $pkbletters->status = $request->status;
            $pkbletters->save();

            return redirect('approvals/employeeassesment')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/employeeassesment')->with([
                'message' => 'Letter not found',
                'message_important'=>true
            ]);
        }

    }

    public function setOutletterStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/out-letters')->withErrors($v->fails());
        }
        $information= Input::get('information');
        $pkbletters  = PkbLetter::find($cmd);
        if($pkbletters){
            $pkbletters->status = $request->status;
            $pkbletters->information = $information;
            $pkbletters->save();

            return redirect('approvals/out-letters')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/out-letters')->with([
                'message' => 'Letter not found',
                'message_important'=>true
            ]);
        }

    }

    /* payouts  Function Start Here */
    public function payouts()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 68)->first();
        $payouts = Procurement::where('category','=','non-subsist')->where('process_status','=','payout')->orderBy('id','asc')->get();
        return view('admin.approval.payouts', compact('payouts','permcheck'));

    }

    /* viewPayout  Function Start Here */
    public function viewPayout($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 68)->first();
        $payouts = Procurement::find($id);
        $procurement_goods = ProcurementGoods::where('procurement','=',$id)->get();
        return view('admin.approval.view-payout', compact('payouts','procurement_goods','permcheck'));
    }

    /* setPayoutStatus  Function Start Here */
    public function setPayoutStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/payouts')->withErrors($v->fails());
        }

        $payouts  = Procurement::find($cmd);
        if($payouts){
            $payouts->payout_status = $request->status;
            $payouts->save();

            return redirect('approvals/payouts')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/payouts')->with([
                'message' => 'Procurement not found',
                'message_important'=>true
            ]);
        }

    }

    /* realizations  Function Start Here */
    public function realizations()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 67)->first();
        $realizations = Procurement::where('category','=','subsist')->orWhere('category','=','petty-cash')->where('process_status','=','realization')->orderBy('id','asc')->get();
        return view('admin.approval.realizations', compact('realizations','permcheck'));

    }

    /* viewRealization  Function Start Here */
    public function viewRealization($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 67)->first();
        $realizations = Procurement::find($id);
        $procurement_goods = ProcurementGoods::where('procurement','=',$id)->get();
        return view('admin.approval.view-realization', compact('realizations','procurement_goods','permcheck'));
    }

    /* setRealizationStatus  Function Start Here */
    public function setRealizationStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/realizations')->withErrors($v->fails());
        }

        $date = date('Y-m-d');
        $date2 = date('Y-m');
        $realizations = Procurement::find($cmd);
        $realization_differences = Input::get('realization_differences');
        if($realizations){
            $realizations->realization_status = $request->status;

            if($realizations->type=='project' && $request->status=='approved' ){
                $total=0;
                $procurement_goods=ProcurementGoods::where('procurement','=',$cmd)->get();
                foreach($procurement_goods as $p){
                 
                        $projectitem=ProjectItem::find($p->goods);
                        if($projectitem){
                            $newamount=$projectitem->amount_bought+$p->realization_quantity;
                            $projectitem->amount_bought= $newamount;
                            $projectitem->leftovers= $projectitem->amount - $newamount;
                            $projectitem->save();

                        }
                        

                    if($p->total_price){
                    $total+=$p->total_price;}
                  
                }
            }
            if($realization_differences != 0){
				$dataAccount 	= findSettingAccount(1);
				$id_coa_debit 	= json_decode($dataAccount['m_coa_id_debit']);
				$id_coa_kredit 	= json_decode($dataAccount['m_coa_id_kredit']);

                $transaction_number = $this->get_kode_transaksi2();
                $data_buku_besar[] = array(
                    'company'				=> $realizations->company,
                    'period'				=> $date2,
                    'date'  				=> $date,
                    'transaction_number'	=> $transaction_number,
                    'information'			=> $dataAccount['activities'],
                    'id_coa_debit'			=> $id_coa_debit[0],
                    'id_coa_kredit'			=> $id_coa_kredit[0],
                    'nominal'				=> (int)abs(str_replace(',', '', $realization_differences)),
                    'customer_id'			=> 0,
                    'supplier_id'			=> 0,
                    'invoice_number'		=> $realizations->realization_number,
                    'id_cashflow_debit'		=> 0,
                    'id_cashflow_credit'	=> 0,
                    'id_file'				=> 1,
                    'baris_file'			=> 1,
                    'hitung'				=> 0,
                );
                $insert_ledger          = Ledger::insert($data_buku_besar);

            }

            $realizations->save();

            return redirect('approvals/realizations')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/realizations')->with([
                'message' => 'Procurement not found',
                'message_important'=>true
            ]);
        }

    }

    /* procurements  Function Start Here */
    public function procurements()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 6)->first();
        $procurements = Procurement::orderBy('id','asc')->get();
        return view('admin.approval.procurements', compact('procurements','permcheck'));

    }

    /* viewProcurement  Function Start Here */
    public function viewProcurement($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 66)->first();
        $procurements = Procurement::find($id);
        $procurement_goods = ProcurementGoods::where('procurement','=',$id)->get();
        return view('admin.approval.view-procurement', compact('procurements','procurement_goods','permcheck'));
    }

    /* setProcurementStatus  Function Start Here */
    public function setProcurementStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $keterangan=Input::get('keterangan');

        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/procurements')->withErrors($v->fails());
        }

        $date = date('Y-m-d');
        $date2 = date('Y-m');
        $procurements  = Procurement::find($cmd);
        if($procurements){
            $procurements->keterangan=$keterangan;

            $procurements->status = $request->status;
            if($procurements->category == 'petty-cash'){
                $procurements->process_status= 'realization';
            } else {
                $procurements->process_status= 'reception';
            }

            if($procurements->category == 'subsist'|| $procurements->category == 'petty-cash'){
				$dataAccount 	= findSettingAccount(1);
				$id_coa_debit 	= json_decode($dataAccount['m_coa_id_debit']);
				$id_coa_kredit 	= json_decode($dataAccount['m_coa_id_kredit']);

                $transaction_number = $this->get_kode_transaksi2();
                $data_buku_besar[] = array(
                    'company'				=> $procurements->company,
                    'period'				=> $date2,
                    'date'  				=> $date,
                    'transaction_number'	=> $transaction_number,
                    'information'			=> $dataAccount['activities'],
                    'id_coa_debit'			=> $id_coa_debit[0],
                    'id_coa_kredit'			=> $id_coa_kredit[0],
                    'nominal'				=> $procurements->total,
                    'customer_id'			=> 0,
                    'supplier_id'			=> 0,
                    'invoice_number'		=> $procurements->procurement_number,
                    'id_cashflow_debit'		=> 0,
                    'id_cashflow_credit'	=> 0,
                    'id_file'				=> 1,
                    'baris_file'			=> 1,
                    'hitung'				=> 0,
                );
                $insert_ledger          = Ledger::insert($data_buku_besar);
            }

            
            $procurements->save();
            
            $notification = Notification::where('tag','=','procurements')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }

            return redirect('approvals/procurements')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/procurements')->with([
                'message' => 'Procurement not found',
                'message_important'=>true
            ]);
        }

    }

    // /* payments  Function Start Here */
    // public function payments()
    // {
    //     $role_id = \Auth::user()->role_id;
    //     $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 63)->first();
    //     $payments = Payment::orderBy('id','asc')->get();
    //     return view('admin.approval.payments', compact('payments','permcheck'));

    // }

    // /* viewPayment  Function Start Here */
    // public function viewPayment($id)
    // {
    //     $role_id = \Auth::user()->role_id;
    //     $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 63)->first();
    //     $payments = Payment::find($id);
    //     $payroll_components = PayrollComponent::where('id_payroll_type','=',$payments->payroll_type)->get();
    //     $payment_components = PaymentComponent::where('payment','=', $id)->get()->toArray();
    //     $employee = PaymentComponent::where('payment','=', $id)->get();
    //     $employee_name = array();
    //     foreach($employee as $e){
    //         $employee_name[] = $e->employee_info->fname.' '.$e->employee_info->lname;
    //     }
    //     // echo"<pre>";print_r($employee_name);exit;
    //     $payment_columns = Component::where('status','=', 'active')->get();
    //     return view('admin.approval.view-payment', compact('payments','payment_components','payment_columns','payroll_components','employee_name','permcheck'));
    // }

    // /* setPaymentStatus  Function Start Here */
    // public function setPaymentStatus(Request $request)
    // {

    //     $cmd=Input::get('cmd');
    //     $keterangan=Input::get('keterangan');
    //     $v=\Validator::make($request->all(),[
    //         'status'=>'required'
    //     ]);

    //     if($v->fails()){
    //         return redirect('approvals/payments')->withErrors($v->fails());
    //     }

    //     $payments  = Payment::find($cmd);
    //     if($payments){
    //         $payments->status=$request->status;
    //         $payments->keterangan=$keterangan;

    //         $payments->save();
            
    //         $notification = Notification::where('tag','=','payments')->where('id_tag','=',$cmd)->first();
        
    //         if($notification){
    //             $notification->end_date = date('Y-m-d');
    //             $notification->save();
    //         }

    //         $employee = PaymentComponent::where('payment','=',$payments->id)->get();

    //         foreach($employee as $e){
    //             $sala=$e->total;
    //             if($sala==null || $sala==""){
    //                 $sala=0;
    //             }
    //             $employee_payment_history = new EmployeePaymentHistory();
    //             $employee_payment_history->emp_id = $e->emp_id;
    //             $employee_payment_history->payslip = $e->id;
    //             $employee_payment_history->date = date('Y-m-d');
    //             $employee_payment_history->salary = $sala;
    //             $employee_payment_history->save();
    //         }


    //         return redirect('approvals/payments')->with([
    //             'message'=> language_data('Status updated successfully'),
    //         ]);

    //     }else{
    //         return redirect('approvals/payments')->with([
    //             'message' => 'Appointment not found',
    //             'message_important'=>true
    //         ]);
    //     }

    // }

    /* appointments  Function Start Here */
    public function appointments()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 56)->first();
        $appointments = Appointment::orderBy('id','asc')->get();
        return view('admin.approval.appointments', compact('appointments','permcheck'));

    }

    /* viewAppointment  Function Start Here */
    public function viewAppointment($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 56)->first();
        $appointments = Appointment::find($id);
        return view('admin.approval.view-appointment', compact('appointments','permcheck'));
    }

    /* setAppointmentStatus  Function Start Here */
    public function setAppointmentStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $keterangan=Input::get('keterangan');

        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/appointments')->withErrors($v->fails());
        }
        $emp_id=Input::get('emp_id');
        $project=Input::get('project');
        $dept=Input::get('dept');
        $desg=Input::get('desg');
        $desg_old=Input::get('desg_old');
        $location=Input::get('location_new');
        $start_date=Input::get('appointment_date');
        $start_date=get_date_format_inggris($start_date);
        $start_date_old=Input::get('start_date_old');
        $start_date_old=get_date_format_inggris($start_date_old);
        $employee_status_new = Input::get('employee_status_new');

        // echo $project;
        // exit;

        $appointments  = Appointment::find($cmd);
        if($appointments){
            $appointments->keterangan=$keterangan;
            $appointments->status=$request->status;
            $appointments->save();
            
            $notification = Notification::where('tag','=','appointments')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }
            if($request->status=='accepted'){
                $employee = Employee::find($emp_id);
                EmploymentHistory::where('emp_id','=',$employee->id)
                ->where('project','=',$employee->project)
                ->where('department','=',$employee->department)
                ->where('designation','=',$employee->designation)
                ->update(['end_date' => $start_date]);

                if($employee_status_new == 'contract'){
                    $end_date = $employee->dol;
                } else if($employee_status_new == 'permanent'){
                    $end_date = '-';
                }

                Employee::where('id','=',$emp_id)->update(['project' => $project, 'department' => $dept, 'designation' => $desg, 'location' => $location, 'doj' => $start_date, 'dol' => $end_date]);
                $history = new EmploymentHistory();
                $history->emp_id = $emp_id;
                $history->project = $project;
                $history->department = $dept;
                $history->designation = $desg;
                $history->location = $location;
                $history->start_date = $start_date;
                $history->end_date = $end_date;
                $history->save();
                
                //Table CareerPathEmpolyeeAssessment Save
                $num_element_competences = 0;
                
                $career = Input::get('career');
                if($career!=0){
        
                    $career_path = CareerPathEmployee::where('designation','=',$career)->get();
            
                    if($career_path){
                        foreach($career_path as $cp){
                            $career_path_competence = CareerPathEmployeeCompetence::where('career_path','=',$cp->id)->get()->toArray();
                        }

                        if($career_path_competence){
                            $sql_data_competence = array();
                    
                            while($num_element_competences<count($career_path_competence)){
                                $sql_data_competence[] = array(
                                    'id'            => "",
                                    'emp_id'        => $emp_id,
                                    'competence'    => $career_path_competence[$num_element_competences]['id'],
                                    'status'        => "Not Pass",
                                    'proof_file'    => "",
                                    'created_at'    => "",
                                    'updated_at'    => ""
                                );
                                $num_element_competences++;
                            }
                    
                            $career_assessment = CareerPathEmployeeAssessment::insert($sql_data_competence);

                        }
                    }
                }
            }

            return redirect('approvals/appointments')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/appointments')->with([
                'message' => 'Appointment not found',
                'message_important'=>true
            ]);
        }

    }
    
    /* expenses  Function Start Here */
    public function expenses()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 65)->first();
        $expenses = Expense::orderBy('id','asc')->get();
        return view('admin.approval.expenses', compact('expenses','permcheck'));

    }

    /* viewExpense  Function Start Here */
    public function viewExpense($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 65)->first();
        $expense = Expense::find($id);
        if ($expense) {

            $expense_detail = ExpenseDetail::where('expense_id', '=', $id)->get();

            return view('admin.approval.view-expense', compact('expense','expense_detail','permcheck'));
        }
    }

    /* setExpenseStatus  Function Start Here */
    public function setExpenseStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/expenses')->withErrors($v->fails());
        }

        $expenses  = Expense::find($cmd);
        if($expenses){
            $expenses->status=$request->status;
            $expenses->save();
            
            $notification = Notification::where('tag','=','expenses')->where('id_tag','=',$cmd)->first();
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }

            return redirect('approvals/expenses')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/expenses')->with([
                'message' => 'Appointment not found',
                'message_important'=>true
            ]);
        }

    }

    /* projects  Function Start Here */
    public function projects()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 54)->first();
        $projects = Project::orderBy('id','asc')->get();
        return view('admin.approval.projects', compact('projects','permcheck'));

    }

    /* viewProject  Function Start Here */
    public function viewProject($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 54)->first();
        $projectview    = Project::find($id);

        if ($projectview) {
            $projectneeds      = ProjectNeeds::where('id','=',$id)->get();
            return view('admin.approval.view-project', compact('projectview','projectneeds','permcheck'));
        }
    }

    /* setProjectStatus  Function Start Here */
    public function setProjectStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/projects')->withErrors($v->fails());
        }

        $projects  = Project::find($cmd);
        if($projects){
            $projects->status=$request->status;
            $projects->save();
            
            $notification = Notification::where('tag','=','projects')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }

            $jobs = Jobs::where('project','=',$cmd)->where('status','=','drafted')->get();

            foreach ($jobs as $j){
                $j->status = 'opening';
                $j->save();
            }
        
            $notification = new Notification();
            $notification->id_tag = $projects->id;
            $notification->tag = 'projects';
            $notification->title = $projects->projects;
            $notification->description = 'Berakhir Proyek';
            $notification->show_date = date('Y-m-d',strtotime($projects->end_date.'-30 day'));
            $notification->start_date = $projects->end_date;
            $notification->end_date = $projects->end_date;
            $notification->route = 'projects';
            $notification->save();
    

            return redirect('approvals/projects')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/projects')->with([
                'message' => 'Project not found',
                'message_important'=>true
            ]);
        }

    }

    /* surat_pengantar  Function Start Here */
    public function surat_pengantar()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 107)->first();
        $job_id = '';
        $jobs = Jobs::where('status','<>','drafted')->orderBy('id','asc')->get();
        $surat_pengantar = SuratPengantar::orderBy('id','asc')->get();
        $email_template = EmailTemplate::where('table_type','=','sys_surat_pengantar')->get();
        $employee = Employee::where('role_id','!=','1')->get();
        return view('admin.approval.surat-pengantar', compact('job_id','jobs','surat_pengantar','employee','email_template','permcheck'));
        

    }

    /* viewSuratPengantar  Function Start Here */
    public function viewSuratPengantar($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 107)->first();
        $surat_pengantar = SuratPengantar::find($id);
        $recipients = SuratPengantarRecipient::where('surat_pengantar','=',$id)->get();
        $email_template = EmailTemplate::where('table_type','=','sys_surat_pengantar')->get();
        return view('admin.approval.view-surat-pengantar', compact('email_template','recipients','surat_pengantar','permcheck'));
    }
    
    /* setSuratPengantarStatus  Function Start Here */
    public function setSuratPengantarStatus(Request $request)
    {
        
        $keterangan=Input::get('keterangan');
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/surat-pengantar')->withErrors($v->fails());
        }

        $surat_pengantar  = SuratPengantar::find($cmd);
        if($surat_pengantar){

            $surat_pengantar->status=$request->status;
            $surat_pengantar->keterangan=$keterangan;
            $surat_pengantar->save();
            
            if($request->status == 'accepted' && $surat_pengantar->surat_pengantar_types=='PETK'){
                $detail = SuratPengantarRecipient::where('surat_pengantar','=',$cmd)->get();
                foreach($detail as $det){                    
                    $employee = Employee::find($det->recipients);
                    EmploymentHistory::where('emp_id','=',$employee->id)
                    ->where('project','=',$employee->project)
                    ->where('department','=',$employee->department)
                    ->where('designation','=',$employee->designation)
                    ->update(['end_date' => $surat_pengantar->date]);

                    $end_date = '-';

                    Employee::where('id','=',$employee->id)
                    ->update([
                        'company' => $surat_pengantar->company, 
                        'project' => $surat_pengantar->project, 
                        'department' => $surat_pengantar->designation_info->department_name->id, 
                        'designation' => $surat_pengantar->designation, 
                        'location' => $surat_pengantar->location,
                        'doj' => $surat_pengantar->date, 
                        'dol' => $end_date
                    ]);
                    $history = new EmploymentHistory();
                    $history->emp_id = $employee->id;
                    $history->project = $surat_pengantar->project;
                    $history->department = $surat_pengantar->designation_info->department_name->id;
                    $history->designation = $surat_pengantar->designation;
                    $history->location = $surat_pengantar->location;
                    $history->start_date = $surat_pengantar->date;
                    $history->end_date = $end_date;
                    $history->save();
                    
                }
            }
            $notification = Notification::where('tag','=','surat_pengantar')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }

            return redirect('approvals/surat-pengantar')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/surat-pengantar')->with([
                'message' => 'Surat Pengantar not found',
                'message_important'=>true
            ]);
        }

    }

    /* contracts  Function Start Here */
    public function contracts()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 55)->first();
        $contracts = Contract::orderBy('id','asc')->get();
        return view('admin.approval.contracts', compact('contracts','permcheck'));

    }

    /* viewContract  Function Start Here */
    public function viewContract($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 55)->first();
        $contracts = Contract::find($id);
        $recipients = ContractRecipient::where('contract_id','=',$id)->get();
        return view('admin.approval.view-contract', compact('recipients','contracts','permcheck'));
    }

    /* setContractStatus  Function Start Here */
    public function setContractStatus(Request $request)
    {
        
        $keterangan=Input::get('keterangan');
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/contracts')->withErrors($v->fails());
        }

        $contracts  = Contract::find($cmd);
        if($contracts){
            $begins = date('Y-m-d',strtotime(get_date_format_inggris($contracts->effective_date). '-1 day'));
            $ends = date('Y-m-d',strtotime(get_date_format_inggris($contracts->end_date). ' +1 day'));
            $begin = new DateTime($begins);
            $end = new DateTime($ends);


            $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
          

            $contractdet=ContractRecipient::where('contract_id','=',$cmd)->get();
            foreach($contractdet as $c){
                if($c->recipients!=null){
                    $emp=Employee::find($c->recipients);
                    Employee::where('id','=',$c->recipients)
                    ->update(['employee_status' => $contracts->contract_types, 'doj' => get_date_format_inggris($contracts->effective_date), 'dol' => get_date_format_inggris($contracts->end_date)]);

                    EmploymentHistory::where('emp_id','=',$emp->id)
                    ->where('project','=',$emp->project)
                    ->where('department','=',$emp->department)
                    ->where('designation','=',$emp->designation)
                    ->update(['start_date' => get_date_format_inggris($contracts->effective_date), 'end_date' => get_date_format_inggris($contracts->end_date)]);

                    $notification = new Notification();
                    $notification->id_tag = $emp->id;
                    $notification->tag = 'employees';
                    $notification->title = $emp->employee_code;
                    $notification->description = 'Berakhir Kontrak dalam 30 Hari';
                    $notification->show_date = date('Y-m-d',strtotime($contracts->end_date.'-30 day'));
                    $notification->start_date = $contracts->end_date;
                    $notification->end_date = $contracts->end_date;
                    $notification->route = 'employees/all';
                    $notification->save();
                }
            }
            $contracts->status=$request->status;
            $contracts->keterangan=$keterangan;
            $contracts->save();
            
            $notification = Notification::where('tag','=','contracts')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }

            return redirect('approvals/contracts')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/contracts')->with([
                'message' => 'Contract not found',
                'message_important'=>true
            ]);
        }

    }

    /* leave  Function Start Here */
    public function leave()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 57)->first();
        $role_id = \Auth::user()->role_id;
        $employee_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
        $leave = Leave::orderBy('id','asc')->get();
        return view('admin.approval.leave', compact('leave','employee_permission','permcheck'));

    }

    /* viewLeave  Function Start Here */
    public function viewLeave($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 57)->first();

        $leave=Leave::find($id);

        return view('admin.approval.view-leave',compact('leave','permcheck'));

    }
    public function employeeAssessments($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 71)->first();
        $reports = EmployeeAssessmentReport::where('emp_id','=',$id)->first();
        $assessments = EmployeeAssessment::where('emp_id','=',$id)->get();
        return view('admin.approval.assessments', compact('assessments','reports','permcheck'));

    }
    public function viewAssessment($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 71)->first();
        $company = Company::orderBy('id','asc')->get();
        $project = Project::orderBy('id','asc')->get();
        $employee = Employee::orderBy('id','asc')->get();
        $assessments = EmployeeAssessment::find($id);
        $assessment_dimension = AssessmentDimension::where('assessment','=', $assessments->assessment_form)->get();
        $assessment_aspect = EmployeeAssessmentAspect::where('emp_assessment_id','=',$assessments->id)->get();
        $assessment_violation = EmployeeAssessmentViolation::where('emp_assessment_id','=',$assessments->id)->first();
        $violation = ViolationTypes::orderBy('id','asc')->get();
        return view('admin.approval.view-assessment', compact('assessments','assessment_dimension','assessment_aspect','assessment_violation','violation','company','project','employee','permcheck'));
    }
    public function reports()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 71)->first();
        $reports = EmployeeAssessmentReport::orderBy('id','asc')->get();
        $assessments = EmployeeAssessment::orderBy('id','asc')->get();
        return view('admin.approval.assessment-reports', compact('reports','assessments','permcheck'));

    }

    /* setLeaveStatus  Function Start Here */
    public function setLeaveStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $idemp=Input::get('id');
        $leave_type=Input::get('leave_type');

        $leave  = Leave::find($cmd);
      //  echo $leave_type;
        //echo $idemp;
        $employee_info = Employee::find($request->id);
        
        $remaining_leave        = $employee_info->remaining_leave;
        $leave_from = date('Y-m-d',strtotime(get_date_format_inggris($request->leave_from)));
        $leave_to = date('Y-m-d',strtotime(get_date_format_inggris($request->leave_to). ' +1 day'));

        if($request->leave_type==='3'){
            $leave_1 = strtotime($request->leave_from);
            $leave_2 = strtotime($request->leave_to);
            $leave_total = (($leave_2 - $leave_1)/86400)+1;    
        } else {
            $leave_total = 0;
        }
        
        if($leave){
            $leave->status=$request->status;
            $leave->remark=$request->remark;
            $leave->save();

            if($request->status==='approved'){
                //mengurangi remaining leave jika leave
                if($leave_type=="Leave"){
                    $leave_1 = strtotime($request->leave_from);
                    $leave_2 = strtotime($request->leave_to);
                    $leave_total = (($leave_2 - $leave_1)/86400)+1; 
                $employee_leave = Employee::where('id','=',$idemp);
                $employee_leave->decrement('remaining_leave',  $leave_total);
            }

                $employee_info->remaining_leave = ($remaining_leave - $leave_total);
                $employee_info->save();    

                $begin = new DateTime($leave_from);
                $end = new DateTime($leave_to);

                $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
                
                foreach($daterange as $date){
                    
                    $attendance = Attendance::where('emp_id', $request->id)->where('date','=',  $date->format("Y-m-d"))->first();

                    if (!is_null($attendance)) {
                        $attendance->clock_in = '';
                        $attendance->clock_out = '';
                        if($leave_type=="Leave"){
                        $attendance->status = 'Absent';
                    }
                        else{
                            $attendance->status = 'Present';
                        }
                        $attendance->leave_id = $cmd;
                        $attendance->save();
            
                    } else {
                        $adance = new Attendance();
                        $adance->emp_id = $request->id;
                        $adance->emp_type = $employee_info->employee_type;
                        $adance->designation = $employee_info->designation;
                        $adance->department = $employee_info->department;
                        $adance->date = $date->format("Y-m-d");
                        $adance->month = $date->format("Y-m");
                        if($leave_type=="Leave"){
                            $adance->status = 'Absent';
                        }
                            else{
                                $adance->status = 'Present';
                            }
                     
                        $adance->leave_id = $cmd;
                        $adance->save();
                    }

                    if($employee_info->employee_type=='Outsourcing'){
                        $outsourceReport = LeaveOutsourceReport::where('emp_id', $request->id)->first();
                        $at_total = Attendance::where('emp_id','=', $request->id)->where('status','=','Present')->count();
                        $lv_total = Attendance::where('emp_id','=', $request->id)->where('status','=','Absent')->count();
                        $ov_total = Attendance::where('emp_id','=', $request->id)->sum('overtime');
                
                        if($outsourceReport){
                            $outsourceReport->total_attendance = $at_total;
                            $outsourceReport->total_leave = $lv_total;
                            $outsourceReport->total_overtime = $ov_total;
                            $outsourceReport->save();
                        } else {
                            $entryreport = new LeaveOutsourceReport();
                            $entryreport->emp_id = $request->id;
                            $entryreport->total_attendance = $at_total;
                            $entryreport->total_leave = $lv_total;
                            $entryreport->total_overtime = $ov_total;
                            $entryreport->designation = $employee_info->designation;
                            $entryreport->department = $employee_info->department;
                            $entryreport->project = $employee_info->project;
                            $entryreport->location = $employee_info->location;
                            $entryreport->save();
                        }
                        
                    } elseif($employee_info->employee_type=='Internal'){
                        $internalReport = LeaveInternalReport::where('emp_id', $request->id)->first();
                        $at_total = Attendance::where('emp_id','=', $request->id)->where('status','=','Present')->count();
                        $lv_total = Attendance::where('emp_id','=', $request->id)->where('status','=','Absent')->count();
                        $ov_total = Attendance::where('emp_id','=', $request->id)->sum('overtime');
                
                        if($internalReport){
                            $internalReport->total_attendance = $at_total;
                            $internalReport->total_leave = $lv_total;
                            $internalReport->total_overtime = $ov_total;
                            $internalReport->save();
                        } else {
                            $entryreport = new LeaveInternalReport();
                            $entryreport->emp_id = $request->id;
                            $entryreport->total_attendance = $at_total;
                            $entryreport->total_leave = $lv_total;
                            $entryreport->total_overtime = $ov_total;
                            $entryreport->designation = $employee_info->designation;
                            $entryreport->department = $employee_info->department;
                            $entryreport->project = $employee_info->project;
                            $entryreport->location = $employee_info->location;
                            $entryreport->save();
                        }   
                    }   
                } 
                               
            }

            return redirect('approvals/leave')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/leave')->with([
                'message' => 'Leave not found',
                'message_important'=>true
            ]);
        }

    }
    
    /* mutations  Function Start Here */
    public function mutations()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 58)->first();
        $mutations = Mutation::orderBy('id','asc')->get();
        return view('admin.approval.mutations', compact('mutations','permcheck'));

    }

    /* viewMutation  Function Start Here */
    public function viewMutation($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 58)->first();
        $mutations = Mutation::find($id);
        return view('admin.approval.view-mutation', compact('mutations','permcheck'));
    }

    /* setMutationStatus  Function Start Here */
    public function setMutationStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $keterangan=Input::get('keterangan');

        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/mutations')->withErrors($v->fails());
        }
        $emp_id=Input::get('emp_id');
        $project=Input::get('project');
        $company=Input::get('project');
        $dept=Input::get('dept');
        $desg=Input::get('desg');
        $desg_old=Input::get('desg_old');
        $location=Input::get('location_new');
        $start_date=Input::get('mutation_date');
        $start_date=get_date_format_inggris($start_date);
        $start_date_old=Input::get('start_date_old');
        $start_date_old=get_date_format_inggris($start_date_old);
        $employee_status_new = Input::get('employee_status_new');

        // echo $project;
        // exit;
        $end_date = '';
        $mutations  = Mutation::find($cmd);
        if($mutations){
            $mutations->keterangan=$keterangan;

            $mutations->status=$request->status;
            $mutations->save();
            
            $notification = Notification::where('tag','=','mutations')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }
            if($request->status=='accepted'){
                $employee = Employee::find($mutations->employee_name);
                EmploymentHistory::where('emp_id','=',$mutations->employee_name)
                ->where('project','=',$employee->project)
                ->where('department','=',$employee->department)
                ->where('designation','=',$employee->designation)
                ->update(['end_date' => $start_date]);

                if($employee_status_new == 'contract'){
                    $end_date = $employee->dol;
                } else if($employee_status_new == 'permanent'){
                    $end_date = '-';
                }

                $employee_code_old = substr($employee->employee_code,3);
                $employee_code_new = str_pad($mutations->company_code_new,3,'0',STR_PAD_LEFT).$employee_code_old;

                Employee::where('id','=',$mutations->employee_name)
                ->update([
                    'company' => $mutations->company_code_new, 
                    'employee_code' => $employee_code_new, 
                    'department' => $dept, 
                    'designation' => $desg, 
                    'location' => $mutations->location_new,
                    'payment_type' => $mutations->payment_type_new, 
                    'doj' => $start_date, 
                    'dol' => $end_date
                ]);
                $history = new EmploymentHistory();
                $history->emp_id = $mutations->employee_name;
                $history->project = $mutations->project_number;
                $history->department = $dept;
                $history->designation = $desg;
                $history->location = $location;
                $history->start_date = $start_date;
                $history->end_date = $end_date;
                $history->save();
                
                //Table CareerPathEmpolyeeAssessment Save
                $num_element_competences = 0;
                
                $career = Input::get('career');
                if($career!=0){
                    $career_path = CareerPathEmployee::where('designation','=',$career)->first();
            
                    if($career_path){
                        $career_path_competence = CareerPathEmployeeCompetence::where('career_path','=',$career_path->id)->get()->toArray();
    
                        if($career_path_competence){
                            $sql_data_competence = array();
                    
                            while($num_element_competences<count($career_path_competence)){
                                $sql_data_competence[] = array(
                                    'id'            => "",
                                    'emp_id'        => $mutations->employee_name,
                                    'competence'    => $career_path_competence[$num_element_competences]['id'],
                                    'status'        => "Not Pass",
                                    'proof_file'    => "",
                                    'created_at'    => "",
                                    'updated_at'    => ""
                                );
                                $num_element_competences++;
                            }
                    
                            $career_assessment = CareerPathEmployeeAssessment::insert($sql_data_competence);
    
                        }
                    }
                }
        
            }

            return redirect('approvals/mutations')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/mutations')->with([
                'message' => 'Mutation not found',
                'message_important'=>true
            ]);
        }

    }

    
    /* status_change  Function Start Here */
    public function status_change()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 103)->first();
        $status_change = StatusChange::orderBy('id','asc')->get();
        return view('admin.approval.status_change', compact('status_change','permcheck'));

    }

    /* viewStatusChange  Function Start Here */
    public function viewStatusChange($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 103)->first();
        $status_change = StatusChange::find($id);
        return view('admin.approval.view-status_change', compact('status_change','permcheck'));
    }

    /* setStatusChangeStatus  Function Start Here */
    public function setStatusChangeStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $keterangan=Input::get('keterangan');

        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/status_change')->withErrors($v->fails());
        }
        $emp_id=Input::get('emp_id');
        $project=Input::get('project');
        $dept=Input::get('dept');
        $desg=Input::get('desg');
        $desg_old=Input::get('desg_old');
        $location=Input::get('location_new');
        $start_date=Input::get('status_change_date');
        $start_date=get_date_format_inggris($start_date);
        $start_date_old=Input::get('start_date_old');
        $start_date_old=get_date_format_inggris($start_date_old);
        $employee_status_new = Input::get('employee_status_new');

        // echo $project;
        // exit;
        $end_date = '';
        $status_change  = StatusChange::find($cmd);
        if($status_change){
            $status_change->keterangan=$keterangan;

            $status_change->status=$request->status;
            $status_change->save();
            
            $notification = Notification::where('tag','=','status_change')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }
            if($request->status=='accepted'){
                $employee = Employee::find($emp_id);
                EmploymentHistory::where('emp_id','=',$employee->id)->where('project','=',$employee->project)->where('department','=',$employee->department)->where('designation','=',$employee->designation)->update(['end_date' => $start_date]);

                if($employee_status_new == 'contract'){
                    $end_date = $employee->dol;
                    if($end_date==''){
                        $end_date = date('Y-m-d');
                    }
                } else if($employee_status_new == 'permanent'){
                    $end_date = '-';
                } else {
                    $end_date = '-';
                }

                Employee::where('id','=',$emp_id)->update(['project' => $project, 'department' => $dept, 'designation' => $desg, 'location' => $location, 'doj' => $start_date, 'dol' => $end_date]);
                $history = new EmploymentHistory();
                $history->emp_id = $emp_id;
                $history->project = $project;
                $history->department = $dept;
                $history->designation = $desg;
                $history->location = $location;
                $history->start_date = $start_date;
                $history->end_date = $end_date;
                $history->save();
                
                //Table CareerPathEmpolyeeAssessment Save
                $num_element_competences = 0;
                
                $career = Input::get('career');
                if($career!=0){
                    $career_path = CareerPathEmployee::where('designation','=',$career)->get();
            
                    if($career_path){
                        foreach($career_path as $cp){
                            $career_path_competence = CareerPathEmployeeCompetence::where('career_path','=',$cp->id)->get()->toArray();
                        }
    
                        if($career_path_competence){
                            $sql_data_competence = array();
                    
                            while($num_element_competences<count($career_path_competence)){
                                $sql_data_competence[] = array(
                                    'id'            => "",
                                    'emp_id'        => $emp_id,
                                    'competence'    => $career_path_competence[$num_element_competences]['id'],
                                    'status'        => "Not Pass",
                                    'proof_file'    => "",
                                    'created_at'    => "",
                                    'updated_at'    => ""
                                );
                                $num_element_competences++;
                            }
                    
                            $career_assessment = CareerPathEmployeeAssessment::insert($sql_data_competence);
    
                        }
                    }
                }
        
            }

            return redirect('approvals/status-change')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/status-change')->with([
                'message' => 'Status Change not found',
                'message_important'=>true
            ]);
        }

    }
    
    
    /* warnings  Function Start Here */
    public function warnings()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 59)->first();
        $warnings = Warning::orderBy('id','asc')->get();
        return view('admin.approval.warnings', compact('warnings','permcheck'));

    }

    /* viewWarning  Function Start Here */
    public function viewWarning($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 59)->first();
        $warnings = Warning::find($id);
        $copies = Copy::where('letter_number','=',$warnings->letter_number)->get();
        return view('admin.approval.view-warning', compact('copies','warnings','permcheck'));
    }

    /* setWarningStatus  Function Start Here */
    public function setWarningStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $keterangan=Input::get('keterangan');

        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/warnings')->withErrors($v->fails());
        }

        $warnings  = Warning::find($cmd);
        if($warnings){
            $warnings->keterangan=$keterangan;

            $warnings->status=$request->status;
            $warnings->save();
            
            $notification = Notification::where('tag','=','warnings')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }

            return redirect('approvals/warnings')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/warnings')->with([
                'message' => 'Warning not found',
                'message_important'=>true
            ]);
        }

    }
    
    /* resigns  Function Start Here */
    public function resigns()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 60)->first();
        $resigns = Resign::orderBy('id','asc')->get();
        return view('admin.approval.resigns', compact('resigns','permcheck'));

    }

    /* viewResign  Function Start Here */
    public function viewResign($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 60)->first();
        $resigns = Resign::find($id);
        $copies = Copy::where('letter_number','=',$resigns->letter_number)->get();
        return view('admin.approval.view-resign', compact('copies','resigns','permcheck'));
    }

    /* setResignStatus  Function Start Here */
    public function setResignStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $keterangan=Input::get('keterangan');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/resigns')->withErrors($v->fails());
        }

        $resigns  = Resign::find($cmd);
        if($resigns){
            if($request->status=='accepted'){
                $nonaktif= Employee::where('id','=',$resigns->employee_name)->update(array('dol'=>$resigns->resign_date));
            }
            $resigns->status=$request->status;
            $resigns->keterangan=$keterangan;
            $resigns->save();
            
            $employee = Employee::find($resigns->employee_name);

            if($employee){
                $employee_resign = new EmployeeResign();
                $employee_resign->fname = $employee->fname;
                $employee_resign->lname = $employee->lname;
                $employee_resign->employee_code = $employee->employee_code;
                $employee_resign->no_ktp = $employee->no_ktp;
                $employee_resign->company = $employee->company;
                $employee_resign->dol = $employee->dol;
                $employee_resign->reason = $resigns->reason;
                $employee_resign->letter_number = $resigns->letter_number;
                $employee_resign->save();

                $employee->status = 'inactive';
                $employee->save();
            }
            
            $notification = Notification::where('tag','=','resigns')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }

            $refrensi = new ReferenceLetter();
            $refrensi->tag = 'resign';
            $refrensi->id_tag = $cmd;
            $date = date('d');
            $month = date('m');
            $year = date('Y');

            $refrensi->letter_number = $employee->employee_code.'/SK-HRD'.'/'.get_roman_letters($month).'/'.$year;
            $refrensi->save();

            return redirect('approvals/resigns')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/resigns')->with([
                'message' => 'Resign not found',
                'message_important'=>true
            ]);
        }

    }

    
    /* terminations  Function Start Here */
    public function terminations()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 61)->first();
        $terminations = Phk::orderBy('id','asc')->get();
        return view('admin.approval.terminations', compact('terminations','permcheck'));

    }

    /* viewTermination  Function Start Here */
    public function viewTermination($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 61)->first();

        $terminations = Phk::find($id);
        $copies = Copy::where('letter_number','=',$terminations->letter_number)->get();
        $grounds = PhkGround::where('letter_number','=',$terminations->letter_number)->get();
        return view('admin.approval.view-termination', compact('grounds','copies','terminations','permcheck'));
    }

    /* setTerminationStatus  Function Start Here */
    public function setTerminationStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $keterangan=Input::get('keterangan');

        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/terminations')->withErrors($v->fails());
        }

        $terminations  = Phk::find($cmd);
        if($terminations){
            if($request->status=="accepted"){
                $nonaktif= Employee::where('id',$terminations->employee_name)->update(array('dol'=>$terminations->effective_date));
            }
            $terminations->keterangan=$keterangan;

            $terminations->status=$request->status;
            $terminations->save();
              
            $employee = Employee::find($terminations->employee_name);

            if($employee){
                $employee_resign = new EmployeeResign();
                $employee_resign->fname = $employee->fname;
                $employee_resign->lname = $employee->lname;
                $employee_resign->employee_code = $employee->employee_code;
                $employee_resign->no_ktp = $employee->no_ktp;
                $employee_resign->company = $employee->company;
                $employee_resign->dol = $employee->dol;
                $employee_resign->letter_number = $terminations->letter_number;
                $employee_resign->save();
                
                $employee->status = 'inactive';
                $employee->save();
            }

            $notification = Notification::where('tag','=','terminations')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }

            return redirect('approvals/terminations')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/terminations')->with([
                'message' => 'Termination not found',
                'message_important'=>true
            ]);
        }

    }
    
    /* endcontracts  Function Start Here */
    public function endcontracts()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 62)->first();

        $endcontracts = EndContract::orderBy('id','asc')->get();
        return view('admin.approval.endcontracts', compact('endcontracts','permcheck'));

    }

    /* viewEndContract  Function Start Here */
    public function viewEndContract($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 62)->first();

        $endcontracts = EndContract::find($id);
        $recipients = EndContractRecipient::where('end_contract_id','=',$id)->get();
        $concerns = EndContractConcern::where('end_contract_id','=',$id)->get();
        $copies = EndContractCopy::where('end_contract_id','=',$id)->get();
        return view('admin.approval.view-endcontract', compact('concerns','copies','recipients','endcontracts','permcheck'));
    }

    /* setEndContractStatus  Function Start Here */
    public function setEndContractStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $keterangan=Input::get('keterangan');

        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/endcontracts')->withErrors($v->fails());
        }

        $endcontracts  = EndContract::find($cmd);
        if($endcontracts){
            $endcontracts->keterangan=$keterangan;

            $endcontracts->status=$request->status;
            $endcontracts->save();
            
            $notification = Notification::where('tag','=','endcontracts')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }
            $endrecipe= EndContractRecipient::where('end_contract_id',$cmd)->get();
            
            if($request->status=='accepted'){
                foreach($endrecipe as $d){
                    $nonaktif= Employee::where('id',$d->recipients)->update(array('dol'=>$endcontracts->effective_date));

                    $employee = Employee::find($d->recipients);

                    if($employee){
                        $employee_resign = new EmployeeResign();
                        $employee_resign->fname = $employee->fname;
                        $employee_resign->lname = $employee->lname;
                        $employee_resign->employee_code = $employee->employee_code;
                        $employee_resign->no_ktp = $employee->no_ktp;
                        $employee_resign->company = $employee->company;
                        $employee_resign->dol = $employee->dol;
                        $employee_resign->letter_number = $d->letter_number;
                        $employee_resign->save();

                        $employee->status = 'inactive';
                        $employee->save();
                    }
                }
            }
            
            return redirect('approvals/endcontracts')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/contracts')->with([
                'message' => 'End Contract not found',
                'message_important'=>true
            ]);
        }

    }

    /* warrants  Function Start Here */
    public function warrants()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 64)->first();
        $warrant = OvertimeWarrant::orderBy('id','asc')->get();
        return view('admin.approval.overtime-warrants', compact('warrant','permcheck'));

    }

    /* viewWarrant  Function Start Here */
    public function viewWarrant($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 64)->first();
        $employee = Employee::where('role_id','!=','1')->get();
        $overtime_warrant = OvertimeWarrant::find($id);
        return view('admin.approval.view-overtime-warrant', compact('overtime_warrant','employee','permcheck'));
    }

    /* setWarrantStatus  Function Start Here */
    public function setWarrantStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $emp_id=Input::get('employee');
        $keterangan = Input::get('keterangan');

        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/overtime-warrants')->withErrors($v->fails());
        }

        
        $date_from = date('Y-m-d',strtotime(get_date_format_inggris($request->date_from)));
        $date_to = date('Y-m-d',strtotime(get_date_format_inggris($request->date_to). ' +1 day'));

        $overtime_from = strtotime($request->overtime_from);
        $overtime_to = strtotime($request->overtime_to);

        $overtime_hour = ($overtime_to - $overtime_from);

        if ($overtime_hour < 0) {
            $overtime_hour = 0;
        }
        $overtime_hour = $overtime_hour / 60;
        $overtime_hour = $overtime_hour / 60;
        $overtime  = OvertimeWarrant::find($cmd);
        if($overtime){
            
            $overtime->status=$request->status;
            $overtime->keterangan=$keterangan;
            $overtime->save();
            
            $notification = Notification::where('tag','=','overtime/warrants')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->end_date = date('Y-m-d');
                $notification->save();
            }

            if($request->status=='accepted'){
                $begin = new DateTime($date_from);
                $end = new DateTime($date_to);

                $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end); 
                foreach($daterange as $date){
                    Attendance::where('emp_id', $emp_id)->where('date', $date)->update(['overtime' => $overtime_hour]);
                }
            }
            
            return redirect('approvals/overtime-warrants')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/overtime-warrants')->with([
                'message' => 'Warrant not found',
                'message_important'=>true
            ]);
        }

    }

    /* generate tansaction code */
	public function get_kode_transaksi2(){
		$bln = date('m');
		$thn = date('y');
        $query = DB::table('sys_ledger')
                    ->selectRaw('MID(transaction_number,7,5) as id')
                    ->whereRaw('MID(transaction_number,1,6) = "LG'.$thn.''.$bln.'"')
                    ->orderBy('transaction_number', 'asc')
                    ->limit(1)
                    ->get();
		$kode_baru = format_kode_transaksi('LG',$query);
		return $kode_baru;
	}


}