<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\EmployeeRolesPermission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\LegalDocument;
use App\Notification;

class LegalDocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* legalDocument  Function Start Here */
    public function legaldocuments()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 13)->first();
        $legaldocuments = LegalDocument::orderBy('id','asc')->get();
        return view('admin.legaldocument.legaldocuments', compact('legaldocuments','permcheck'));

    }

    /* addLegalDocument  Function Start Here */
    public function addLegalDocument(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'id_document' => 'required',
            'document' => 'required',
            'about' => 'required',
            'due_date' => 'required',
            'file' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('legaldocuments')->withErrors($v->errors());
        }

        $file = Input::file('file');
        $destinationPath_file = public_path() . '/assets/legal_document_files/';
        $target_file = $destinationPath_file . basename($_FILES["file"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        $id_document = Input::get('id_document');
        $document = Input::get('document');
        $about = Input::get('about');
        $due_date = Input::get('due_date');
    
        $tmpdue = explode(' ',$due_date);
        if(sizeof($tmpdue)==3){
            if($tmpdue[1] == 'Januari'){
                $tmpdue[1] = 'January';
            } if($tmpdue[1] == 'Februari'){
                $tmpdue[1] = 'February';
            } if($tmpdue[1] == 'Maret'){
                $tmpdue[1] = 'March';
            } if($tmpdue[1] == 'April'){
                $tmpdue[1] = 'April';
            } if($tmpdue[1] == 'Mei'){
                $tmpdue[1] = 'May';
            } if($tmpdue[1] == 'Juni'){
                $tmpdue[1] = 'June';
            } if($tmpdue[1] == 'Juli'){
                $tmpdue[1] = 'July';
            } if($tmpdue[1] == 'Agustus'){
                $tmpdue[1] = 'August';
            } if($tmpdue[1] == 'September'){
                $tmpdue[1] = 'September';
            } if($tmpdue[1] == 'Oktober'){
                $tmpdue[1] = 'October';
            } if($tmpdue[1] == 'November'){
                $tmpdue[1] = 'November';
            } if($tmpdue[1] == 'Desember'){
                $tmpdue[1] = 'December';
            }
        $due_date = $tmpdue[0].' '.$tmpdue[1].' '.$tmpdue[2];
        }
        $due_date = get_date_format_inggris($due_date);

        if ($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'jpeg' || $imageFileType == 'pdf') {
            $destinationPath = public_path() . '/assets/legal_document_files/';
            $file_name = $file->getClientOriginalName();
            Input::file('file')->move($destinationPath, $file_name);

            $legaldocuments = new LegalDocument();
            $legaldocuments->id_document = $id_document;
            $legaldocuments->document = $document;
            $legaldocuments->about = $about;
            $legaldocuments->due_date = $due_date;
            $legaldocuments->file = $file_name;
            $legaldocuments->save();

            $id_legal = $legaldocuments->id;

            $notification = new Notification();
            $notification->id_tag = $id_legal;
            $notification->tag = 'LegalDocument';
            $notification->title = $document;
            $notification->description = $about;
            $notification->show_date = get_date_format_inggris($due_date.'-30 day');
            $notification->start_date = $due_date;
            $notification->end_date = $due_date;
            $notification->route = 'legaldocuments';
            $notification->save();

            return redirect('legaldocuments')->with([
                'message' => 'Legal Document Added Successfully'
            ]);
        } else {
            return redirect('legaldocuments')->with([
                'message' => 'Upload an Image or PDF',
                'message_important' => true
            ]);
        }

        if ($legaldocuments!='') {
            return redirect('legaldocuments')->with([
                'message' => 'Legal Document Added Successfully'
            ]);

        } else {
            return redirect('legaldocuments')->with([
                'message' => 'Legal Document Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateLegalDocument  Function Start Here */
    public function updateLegalDocument(Request $request)
    {

        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'id_document' => 'required',
            'document' => 'required',
            'about' => 'required',
            'due_date' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('legaldocuments')->withErrors($v->errors());
        }
        $legaldocuments = LegalDocument::find($cmd);
        $id_document = Input::get('id_document');
        $document = Input::get('document');
        $about = Input::get('about');
        $due_date = Input::get('due_date');
        $tmpdue = explode(' ',$due_date);
        if(sizeof($tmpdue)==3){
            if($tmpdue[1] == 'Januari'){
                $tmpdue[1] = 'January';
            } if($tmpdue[1] == 'Februari'){
                $tmpdue[1] = 'February';
            } if($tmpdue[1] == 'Maret'){
                $tmpdue[1] = 'March';
            } if($tmpdue[1] == 'April'){
                $tmpdue[1] = 'April';
            } if($tmpdue[1] == 'Mei'){
                $tmpdue[1] = 'May';
            } if($tmpdue[1] == 'Juni'){
                $tmpdue[1] = 'June';
            } if($tmpdue[1] == 'Juli'){
                $tmpdue[1] = 'July';
            } if($tmpdue[1] == 'Agustus'){
                $tmpdue[1] = 'August';
            } if($tmpdue[1] == 'September'){
                $tmpdue[1] = 'September';
            } if($tmpdue[1] == 'Oktober'){
                $tmpdue[1] = 'October';
            } if($tmpdue[1] == 'November'){
                $tmpdue[1] = 'November';
            } if($tmpdue[1] == 'Desember'){
                $tmpdue[1] = 'December';
            }
        $due_date = $tmpdue[0].' '.$tmpdue[1].' '.$tmpdue[2];
        }
        $due_date = get_date_format_inggris($due_date);

        if ($id_document != $legaldocuments->id_document) {

            $exist = LegalDocument::where('id_document', $id_document)->first();
            if ($exist) {
                return redirect('legaldocuments')->with([
                    'message' => 'Legal Document Already Exist',
                    'message_important' => true
                ]);
            }
        }

        if ($legaldocuments) {
            $legaldocuments->document = $document;
            $legaldocuments->about = $about;
            $legaldocuments->due_date = $due_date;
            $legaldocuments->save();

            $notification = Notification::where('tag','=','LegalDocument')->where('id_tag','=',$cmd)->first();

            if($notification){
                $notification->title = $document;
                $notification->description = $about;
                $notification->show_date = get_date_format_inggris($due_date.'-30 day');
                $notification->start_date = $due_date;
                $notification->end_date = $due_date;
                $notification->save();
            }

            return redirect('legaldocuments')->with([
                'message' => 'Legal Document Updated Successfully'
            ]);

        } else {
            return redirect('legaldocuments')->with([
                'message' => 'Legal Document Not Found',
                'message_important' => true
            ]);
        }
    }


    /* deleteLegalDocument  Function Start Here */
    public function deleteLegalDocument($id)
    {

        $legaldocuments = LegalDocument::find($id);
        if ($legaldocuments) {

            $legaldocuments->delete();

            return redirect('legaldocuments')->with([
                'message' => 'Legal Document Deleted Successfully'
            ]);
        } else {
            return redirect('legaldocuments')->with([
                'message' => 'Legal Document Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewLegalDocument  Function Start Here */
    public function viewLegalDocument($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 10)->first();

        $d = LegalDocument::find($id);
        return view('admin.legaldocument.view-legaldocument', compact('d','permcheck'));
    }

    /* downloadFile  Function Start Here */
    public function downloadFile($id)
    {


        $file = LegalDocument::find($id)->file;
        return response()->download(public_path('assets/legal_document_files/' . $file));
    }
}
