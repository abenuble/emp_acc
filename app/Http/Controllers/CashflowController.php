<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Cashflow;
use App\Cashflow2;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;

date_default_timezone_set(app_config('Timezone'));

class CashflowController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* cashflow  Function Start Here */
    public function cashflow()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 33)->first();
        $cashflow = Cashflow::orderBy('id','asc')->get();
        return view('admin.cashflow.cashflow', compact('cashflow','permcheck'));

    }

    /* setCashflowStatus  Function Start Here */
    public function setCashflowStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('cashflow')->withErrors($v->fails());
        }

        $cashflow  = Cashflow::find($cmd);
        if($cashflow){
            $cashflow->status=$request->status;
            $cashflow->save();

            return redirect('cashflow')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('cashflow')->with([
                'message' => 'Cashflow not found',
                'message_important'=>true
            ]);
        }

    }

    /* addCashflow  Function Start Here */
    public function addCashflow(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'cashflow_code' => 'required',
            'cashflow' => 'required',
            'information' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('cashflow')->withErrors($v->errors());
        }

        $cashflow = Input::get('cashflow');
        $cashflow_code = Input::get('cashflow_code');
        $debit_credit = Input::get('debit_credit');
        $information = Input::get('information');

        //Penomoran id
        $last_number = Cashflow::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }

        $cashflows = new Cashflow();
        $cashflows->id = $number;
        $cashflows->cashflow = $cashflow;
        $cashflows->cashflow_code = $cashflow_code;
        $cashflows->debit_credit = $debit_credit;
        $cashflows->information = $information;
        $cashflows->save();

        $positif = '';
        if($debit_credit == 'debit'){
            $positif = '1';
        } else if($debit_credit == 'credit'){
            $positif = '-1';
        }

        $data_cashflow[] = array(
            'id'				=> $number,
            'nama'				=> $cashflow,
            'kode'  			=> $cashflow_code,
            'positif'	        => $positif,
            'keterangan'		=> $information,
        );
        $insert_cashflow  = Cashflow2::insert($data_cashflow);


        if ($cashflows!='') {
            return redirect('cashflow')->with([
                'message' => 'Cashflow Added Successfully'
            ]);

        } else {
            return redirect('cashflow')->with([
                'message' => 'Cashflow Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateCashflow  Function Start Here */
    public function updateCashflow(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'cashflow_code' => 'required',
            'cashflow' => 'required',
            'information' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('contract')->withErrors($v->errors());
        }
        $cashflows = Cashflow::find($cmd);
        $cashflow = Input::get('cashflow');
        $cashflow_code = Input::get('cashflow_code');
        $debit_credit = Input::get('debit_credit');
        $information = Input::get('information');
        $status = Input::get('status');

        if ($cashflows) {
            $cashflows->cashflow = $cashflow;
            $cashflows->cashflow_code = $cashflow_code;
            $cashflows->debit_credit = $debit_credit;
            $cashflows->information = $information;
            $cashflows->status = $status;
            $cashflows->save();

            $positif = '';
            if($debit_credit == 'debit'){
                $positif = '1';
            } else if($debit_credit == 'credit'){
                $positif = '-1';
            }
            
            $data_cashflow = array(
                'nama'				=> $cashflow,
                'kode'  			=> $cashflow_code,
                'positif'	        => $positif,
                'keterangan'		=> $information,
            );
            DB::table('cashflow')
            ->where('id', $cmd)
            ->update($data_cashflow);

            return redirect('cashflow')->with([
                'message' => 'Cashflow Updated Successfully'
            ]);

        } else {
            return redirect('cashflow')->with([
                'message' => 'Cashflow Not Found',
                'message_important' => true
            ]);
        }
    }


    /* deleteCashflow  Function Start Here */
    public function deleteCashflow($id)
    {

        $cashflows = Cashflow::find($id);
        if ($cashflows) {

            $cashflows->delete();

            return redirect('cashflow')->with([
                'message' => 'Cashflow Deleted Successfully'
            ]);
        } else {
            return redirect('cashflow')->with([
                'message' => 'Cashflow Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewCashflow  Function Start Here */
    public function viewCashflow($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 33)->first();
        $cashflow = Cashflow::find($id);
        return view('admin.cashflow.view-cashflow', compact('cashflow','permcheck'));
    }

    /* download excel function start here */
    public function downloadExcel()
    {
        return response()->download(public_path('assets/template_xls/cashflow.xls'));
    }

    /* import excel function start here */
    public function importExcel()
    {
        // Get current data from company table
        $cashflow_code = Cashflow::lists('cashflow_code')->toArray();

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
			})->get();
            if ($fileType != 'xls'){
                return redirect('cashflow')->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else{
                if(!empty($data) && $data->count()){
                    $insert = array();
                    foreach ($data as $key => $value) {
                        // Skip cashflow_code previously added using in_array
                        if (in_array($value->kode_arus_kas, $cashflow_code)){
                            continue;
                        } else {
                            //Penomoran id
                            $last_number = Cashflow::max('id');
                            if($last_number==''){
                                $number = 1;
                            } else {
                                $number = 1 + $last_number;
                            }

                            $positif = '';
                            if(strtolower($value->debit_kredit) == 'debit' || strtolower($value->debit_kredit) == 'debet'){
                                $positif = '1';
                                $debet_kredit = 'debit';
                            } else if(strtolower($value->debit_kredit) == 'kredit' || strtolower($value->debit_kredit) == 'credit'){
                                $positif = '-1';
                                $debet_kredit = 'credit';
                            }

                            $cashflows = new Cashflow();
                            $cashflows->id = $number;
                            $cashflows->cashflow_code = $value->arus_kas;
                            $cashflows->cashflow = $value->kode_arus_kas;
                            $cashflows->debit_credit = $debit_kredit;
                            $cashflows->information = $value->informasi;
                            $cashflows->save();


                            $data_cashflow[] = array(
                                'id'				=> $number,
                                'nama'				=> $value->arus_kas,
                                'kode'  			=> $value->kode_arus_kas,
                                'positif'	        => $positif,
                                'keterangan'		=> $value->informasi,
                            );
                            
                        }
                    }
                    $insert_cashflow  = Cashflow2::insert($data_cashflow);
                    if($cashflows!=''){
                        return redirect('cashflow')->with([
                            'message' => 'Cashflow Added Successfully'
                        ]);
                    }
                } else {
                    return redirect('cashflow')->with([
                        'message' => 'Cashflow2 Not Found',
                        'message_important' => true
                    ]);
                } 
            }
        }
		return back();
    }


}
