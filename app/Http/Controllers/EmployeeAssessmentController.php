<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AssessmentForm;
use App\AssessmentDimension;
use App\AssessmentAspect;
use App\Attendance;
use App\Company;
use App\Employee;
use App\EmployeeAssessmentReport;
use App\EmployeeAssessment;
use App\EmployeeAssessmentAspect;
use App\EmployeeAssessmentViolation;
use App\EmployeeRolesPermission;
use App\Project;
use App\ScheduleEmployee;
use App\ViolationTypes;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Support\Facades\Input;
use WkPdf;

class EmployeeAssessmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* reports  Function Start Here */
    public function reports()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 45)->first();
        $reports = EmployeeAssessmentReport::orderBy('id','asc')->get();
        return view('admin.employee_assessment.assessment-reports', compact('reports','permcheck'));

    }

    /* employeeAssessments  Function Start Here */
    public function employeeAssessments($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 45)->first();
        $reports = EmployeeAssessmentReport::where('emp_id','=',$id)->first();
        $assessments = EmployeeAssessment::where('emp_id','=',$id)->orderBy('id','asc')->get();
        return view('admin.employee_assessment.assessments', compact('assessments','reports','permcheck'));

    }

    
    /* viewAssessment  Function Start Here */
    public function viewAssessment($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 45)->first();
        $company = Company::all();
        $project = Project::all();
        $employee = Employee::all();
        $assessments = EmployeeAssessment::find($id);
        $assessment_dimension = AssessmentDimension::where('assessment','=', $assessments->assessment_form)->get();
        $assessment_aspect = EmployeeAssessmentAspect::where('emp_assessment_id','=',$assessments->id)->get();
        $assessment_violation = EmployeeAssessmentViolation::where('emp_assessment_id','=',$assessments->id)->first();
        $violation = ViolationTypes::all();
        return view('admin.employee_assessment.view-assessment', compact('assessments','assessment_dimension','assessment_aspect','assessment_violation','violation','company','project','employee','permcheck'));
    }

    public function downloadPdf($id)
    {
        $assessments = EmployeeAssessment::find($id);
        $assessment_dimension = AssessmentDimension::where('assessment','=', $assessments->assessment_form)->get();
        $assessment_aspect = EmployeeAssessmentAspect::where('emp_assessment_id','=',$assessments->id)->get();
        $assessment_violation = EmployeeAssessmentViolation::where('emp_assessment_id','=',$assessments->id)->first();
   
        $pdf=WkPdf::loadView('admin.employee_assessment.pdf-form', compact('assessments','assessment_dimension','assessment_aspect','assessment_violation'));

        return $pdf->stream('EmployeeAssessment.pdf');
    }
    /* addAssessment  Function Start Here */
    public function addAssessment()
    {

        $proj_id = '';
        $comp_id = '';
        $emp_id = '';
        $company = Company::all();


        $assessment = AssessmentForm::find(app_config('FormInternal'));
        $assessment_dimension = AssessmentDimension::where('assessment','=', app_config('FormInternal'))->get();
        $assessment_aspect = AssessmentAspect::where('assessment','=',app_config('FormInternal'))->get();
        $violation = ViolationTypes::all();
        return view('admin.employee_assessment.add-assessment', compact('assessment','assessment_dimension','assessment_aspect','violation','proj_id','comp_id','emp_id','company'));
    }

    
    /* addAssessmentPost Function Start Here */
    public function addAssessmentPost(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'aspect' => 'required',
            'grade' => 'required',
            'weight' => 'required',
            'weighted_grade' => 'required',
            'information' => 'required',
            'additional_note' => 'required',
        ]);

        if ($v->fails()) {
            return redirect('employee-assessments/add')->withErrors($v->errors());
        }

        /*data table employee_assessments */
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $period_from = Input::get('period_from');
        $period_to = Input::get('period_to');
        $period_from = get_date_format_inggris_bulan($period_from);
        $period_to = get_date_format_inggris_bulan($period_to);
        $emp_id = Input::get('emp_id');
        $assessment_form = Input::get('assessment_form');
        $project = Input::get('project');
        $company = Input::get('company');
        $total = Input::get('total');
        $information = Input::get('information');
        $additional_note = Input::get('additional_note');

        /*data table employee_assessment_aspects */
        $aspect = Input::get('aspect');
        $grade = Input::get('grade');
        $weight = Input::get('weight');
        $weighted_grade = Input::get('weighted_grade');
        $violation = Input::get('violation');
        $violation_grade = Input::get('violation_grade');
        $violation_info = ViolationTypes::find($violation);

        /*save table employee_assessments */
        $assessment = new EmployeeAssessment();
        $assessment->date = $date;
        $assessment->period_from = $period_from;
        $assessment->period_to = $period_to;
        $assessment->emp_id = $emp_id;
        $assessment->assessment_form = app_config('FormInternal');
        $assessment->project = $project;
        $assessment->company = $company;
        $assessment->grade = $total;
        $assessment->information = $information;
        $assessment->additional_note = $additional_note;
        $assessment->save();

        /*save table employee_assessment_aspects */
        $emp_assessment_id = $assessment->id;

        $num_elements           = 0;
        
        $sql_data               = array();
        
        while($num_elements<count($grade)) {
            $sql_data[] = array(
                'id'                => "",
                'emp_id'            => $emp_id,
                'emp_assessment_id' => $emp_assessment_id,
                'aspect'            => $aspect[$num_elements],
                'weight'            => $weight[$num_elements],
                'grade'             => $grade[$num_elements],
                'weighted_grade'    => $weighted_grade[$num_elements],
            );
            $num_elements++;
        }

        $assessment_aspect     = EmployeeAssessmentAspect::insert($sql_data);
        if($violation!=0){
            $assessment_violation = new EmployeeAssessmentViolation();
            $assessment_violation->emp_id = $emp_id;
            $assessment_violation->emp_assessment_id = $emp_assessment_id;
            $assessment_violation->violation = $violation_info->violation;
            $assessment_violation->weighted_grade = $violation_info->weight;
            $assessment_violation->save();
        }

        /*save table employee_assessment_reports */

        $assessment_report = EmployeeAssessmentReport::where('emp_id', $emp_id)->first();
        $average = EmployeeAssessment::where('emp_id','=', $emp_id)->avg('grade');
        $difference = $total - $average;

        if($assessment_report){
            $assessment_report->grade = $total;
            $assessment_report->average = $average;
            $assessment_report->difference = $difference;
            $assessment_report->information = $information;
            $assessment_report->save();
        } else {
            $entryreport = new EmployeeAssessmentReport();
            $entryreport->emp_id = $emp_id;
            $entryreport->project = $project;
            $entryreport->company = $company;
            $entryreport->grade = $total;
            $entryreport->average = $average;
            $entryreport->difference = $difference;
            $entryreport->information = $information;
            $entryreport->save();
        }


        if ($assessment!='') {
            return redirect('employee-assessments/reports')->with([
                'message' => 'Assessment Added Successfully'
            ]);

        } else {
            return redirect('employee-assessments/reports')->with([
                'message' => 'Assessment Already Exist',
                'message_important' => true
            ]);
        }

        


    }
    public function updateAssessmentPost(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'date' => 'required',
            'period_from' => 'required',
            'period_to' => 'required',
            'aspect' => 'required',
            'grade' => 'required',
            'weight' => 'required',
            'weighted_grade' => 'required',
            'information' => 'required',
            'additional_note' => 'required',
        ]);
        $emp_asses_id=Input::get('emp_asses_id');

        if ($v->fails()) {
            return redirect('employee-assessments/assessment-aspects/'.$emp_asses_id)->withErrors($v->errors());
        }

        /*data table employee_assessments */
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $period_from = Input::get('period_from');
        $period_to = Input::get('period_to');
        $period_from = get_date_format_inggris_bulan($period_from);
        $period_to = get_date_format_inggris_bulan($period_to);
        $emp_id = Input::get('emp_id');
        $assessment_form = Input::get('assessment_form');
        $project = Input::get('project');
        $company = Input::get('company');
        $total = Input::get('total');
        $information = Input::get('information');
        $additional_note = Input::get('additional_note');
        $emp_asses_id=Input::get('emp_asses_id');
        $violation_asses_id=Input::get('violation_asses_id');
        /*data table employee_assessment_aspects */
        $employeeaspect =Input::get('employeeaspect');
        $aspect = Input::get('aspect');
        $grade = Input::get('grade');
        $weight = Input::get('weight');
        $weighted_grade = Input::get('weighted_grade');
        $violation = Input::get('violation');
        $violation_grade = Input::get('violation_grade');
        $violation_info = ViolationTypes::find($violation);

        /*save table employee_assessments */
        $assessment =  EmployeeAssessment::find($emp_asses_id);
       // $assessment->date = $date;
        //$assessment->period_from = $period_from;
        //$assessment->period_to = $period_to;
        //$assessment->emp_id = $emp_id;
        //$assessment->assessment_form = app_config('FormInternal');
        //$assessment->project = $project;
        //$assessment->company = $company;
        $assessment->grade = $total;
        $assessment->information = $information;
        $assessment->additional_note = $additional_note;
        $assessment->save();

      

        $num_elements           = 0;
        
        $sql_data               = array();
        
        while($num_elements<count($grade)) {
            $assessment_aspect=EmployeeAssessmentAspect::where('id',$employeeaspect[$num_elements])
                ->update(
                    array(
                        'emp_assessment_id' => $emp_asses_id,
                     
                        'grade'             => $grade[$num_elements],
                        'weighted_grade'    =>  $weighted_grade[$num_elements]
                    )
                    );
          
            $num_elements++;
        }

     
        if($violation!=0){
            $assessment_violation = EmployeeAssessmentViolation::find($violation_asses_id);
            if($assessment_violation){
                $assessment_violation->emp_id = $emp_id;
                $assessment_violation->emp_assessment_id = $emp_asses_id;
                $assessment_violation->violation = $violation_info->violation;
                $assessment_violation->weighted_grade = $violation_info->weight;
                $assessment_violation->save();
            } else {
                $assessment_violation = new EmployeeAssessmentViolation();
                $assessment_violation->emp_id = $emp_id;
                $assessment_violation->emp_assessment_id = $emp_asses_id;
                $assessment_violation->violation = $violation_info->violation;
                $assessment_violation->weighted_grade = $violation_info->weight;
                $assessment_violation->save();
            }
        }

        /*save table employee_assessment_reports */

        $assessment_report = EmployeeAssessmentReport::where('emp_id','=', $emp_id)->first();
        $average = EmployeeAssessment::where('id','=', $emp_asses_id)->avg('grade');
        $difference = $total - $average;

        if($assessment_report){
            $assessment_report->emp_id= $emp_id;
            $assessment_report->grade = $total;
            $assessment_report->average = $average;
            $assessment_report->difference = $difference;
            $assessment_report->information = $information;
            $assessment_report->save();
        } else {
            $entryreport = new EmployeeAssessmentReport();
            $entryreport->emp_id = $emp_id;
            $entryreport->project = $project;
            $entryreport->company = $company;
            $entryreport->grade = $total;
            $entryreport->average = $average;
            $entryreport->difference = $difference;
            $entryreport->information = $information;
            $entryreport->save();
        }


        if ($assessment!='') {
            return redirect('employee-assessments/reports')->with([
                'message' => 'Assessment updated Successfully'
            ]);

        } else {
            return redirect('employee-assessments/reports')->with([
                'message' => 'Assessment Update Failed',
                'message_important' => true
            ]);
        }

        


    }
    /* getViolation  Function Start Here */
    public function getViolation(Request $request)
    {
        $vio_id = $request->vio_id;
        if ($vio_id) {
            $violation = ViolationTypes::find($vio_id);
            echo $violation->weight;
        }
    }

    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project . '</option>';
            }
        }
    }

    /* getEmployee Function Start Here */
    public function getEmployee(Request $request)
    {

        $proj_id = $request->proj_id;
        if ($proj_id) {
            echo '<option value="0">Select Employee Code</option>';
            $employee = Employee::where('project', $proj_id)->where('role_id','!=',1)->where('status','=','active')->get();
            foreach ($employee as $d) {
                echo '<option value="' . $d->id . '">' . $d->fname . ' ' . $d->lname . ' (' . $d->employee_code . ')</option>';
            }
        }
    }

    /* getEmployeeName Function Start Here */
    public function getEmployeeName(Request $request)
    {

        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee = Employee::find($emp_id);
            echo $employee->fname . ' ' . $employee->lname;
        }
    }

    /* getDesignation Function Start Here */
    public function getDesignation(Request $request)
    {

        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee = Employee::find($emp_id);
            echo $employee->designation_name->designation;
        }
    }

    

    /* getLocation Function Start Here */
    public function getLocation(Request $request)
    {

        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee = Employee::find($emp_id);
            echo $employee->location;
        }
    }
    /* getPresenceGrade Function Start Here */
    public function getPresenceGrade(Request $request)
    {
        $emp_id = $request->emp_id;
        $month_from = $request->month_from;
        $tmpdue = explode(' ',$month_from);
        if(sizeof($tmpdue)==2){
            if($tmpdue[0] == 'Januari'){
                $tmpdue[0] = 'January';
            } if($tmpdue[0] == 'Februari'){
                $tmpdue[0] = 'February';
            } if($tmpdue[0] == 'Maret'){
                $tmpdue[0] = 'March';
            } if($tmpdue[0] == 'April'){
                $tmpdue[0] = 'April';
            } if($tmpdue[0] == 'Mei'){
                $tmpdue[0] = 'May';
            } if($tmpdue[0] == 'Juni'){
                $tmpdue[0] = 'June';
            } if($tmpdue[0] == 'Juli'){
                $tmpdue[0] = 'July';
            } if($tmpdue[0] == 'Agustus'){
                $tmpdue[0] = 'August';
            } if($tmpdue[0] == 'September'){
                $tmpdue[0] = 'September';
            } if($tmpdue[0] == 'Oktober'){
                $tmpdue[0] = 'October';
            } if($tmpdue[0] == 'November'){
                $tmpdue[0] = 'November';
            } if($tmpdue[0] == 'Desember'){
                $tmpdue[0] = 'December';
            }
        }
        $month_from = $tmpdue[0].' '.$tmpdue[1];
        $month_from = date('Y-m-1', strtotime($month_from));
        $month_to = $request->month_to;
        $tmpdue1 = explode(' ',$month_to);
        if(sizeof($tmpdue1)==2){
            if($tmpdue1[0] == 'Januari'){
                $tmpdue1[0] = 'January';
            } if($tmpdue1[0] == 'Februari'){
                $tmpdue1[0] = 'February';
            } if($tmpdue1[0] == 'Maret'){
                $tmpdue1[0] = 'March';
            } if($tmpdue1[0] == 'April'){
                $tmpdue1[0] = 'April';
            } if($tmpdue1[0] == 'Mei'){
                $tmpdue1[0] = 'May';
            } if($tmpdue1[0] == 'Juni'){
                $tmpdue1[0] = 'June';
            } if($tmpdue1[0] == 'Juli'){
                $tmpdue1[0] = 'July';
            } if($tmpdue1[0] == 'Agustus'){
                $tmpdue1[0] = 'August';
            } if($tmpdue1[0] == 'September'){
                $tmpdue1[0] = 'September';
            } if($tmpdue1[0] == 'Oktober'){
                $tmpdue1[0] = 'October';
            } if($tmpdue1[0] == 'November'){
                $tmpdue1[0] = 'November';
            } if($tmpdue1[0] == 'Desember'){
                $tmpdue1[0] = 'December';
            }
        }
        $month_to = $tmpdue1[0].' '.$tmpdue1[1];
        $month_to = date('Y-m-t', strtotime($month_to));
         
        $attendance_ontime = Attendance::where('emp_id','=',$emp_id)->where('presence_status','=','ontime')->where('date','>=',$month_from)->where('date','<=',$month_to)->count();
        $attendance_late = Attendance::where('emp_id','=',$emp_id)->where('presence_status','=','late')->where('date','>=',$month_from)->where('date','<=',$month_to)->count();
        $effective_day = ScheduleEmployee::where('emp_id','=',$emp_id)->where('starts_at','!=','')->where('date','>=',$month_from)->where('date','<=',$month_to)->count();
        
        // echo $month_from;
        // echo "<br>";
        // echo $month_to;
        // echo "<br>";
        // echo $attendance_ontime;
        // echo "<br>";
        // echo $attendance_late;
        // echo "<br>";
        // echo $effective_day; exit;
        if($effective_day != 0){
            $presensi_grade = (($attendance_ontime*1)+($attendance_late/2))*100/$effective_day;
        } else {
            $presensi_grade = 0;
        }
        if ($emp_id) {
            echo $presensi_grade;
        }
    }


    

}
