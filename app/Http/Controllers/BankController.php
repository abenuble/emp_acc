<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Bank;
use App\Company;
use App\Coa;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;

date_default_timezone_set(app_config('Timezone'));

class BankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* bank  Function Start Here */
    public function bank()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 34)->first();
        $comp_id = '';
        $bank = Bank::orderBy('id','asc')->get();
        $company = Company::where('category','=','Internal')->where('status','=','active')->get();
        return view('admin.bank.bank', compact('bank','company','comp_id','permcheck'));

    }

    /* setBankStatus  Function Start Here */
    public function setBankStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('bank')->withErrors($v->fails());
        }

        $bank  = Bank::find($cmd);
        if($bank){
            $bank->status=$request->status;
            $bank->save();

            return redirect('banks')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('banks')->with([
                'message' => 'Bank not found',
                'message_important'=>true
            ]);
        }

    }

    /* getCoa  Function Start Here */
    public function getCoa(Request $request)
    {
        $comp_id = $request->comp_id;
        if ($comp_id) {
            echo '<option value="0">Select COA</option>';
            $coa = Coa::where('company', $comp_id)->where('sub_header','=','6')->get();
            foreach ($coa as $c) {
                echo '<option value="' . $c->id . '">' . $c->coa . '('.$c->coa_code.')</option>';
            }
        }
    }

    /* addBank  Function Start Here */
    public function addBank(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'company' => 'required',
            'bank' => 'required',
            'branch' => 'required',
            'account_number' => 'required',
            'name' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('banks')->withErrors($v->errors());
        }

        $company = Input::get('company');
        $bank = Input::get('bank');
        $branch = Input::get('branch');
        $account_number = Input::get('account_number');
        $name = Input::get('name');
        $coa_id = Input::get('coa_id');
        //Penomoran id
        $last_number = Bank::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }

        $banks = new Bank();
        $banks->id = $number;
        $banks->company = $company;
        $banks->bank = $bank;
        $banks->branch = $branch;
        $banks->account_number = $account_number;
        $banks->name = $name;
        $banks->coa_id = $coa_id;
        $banks->save();

        if ($banks!='') {
            return redirect('banks')->with([
                'message' => 'Bank Added Successfully'
            ]);

        } else {
            return redirect('banks')->with([
                'message' => 'Bank Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateBank  Function Start Here */
    public function updateBank(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'company' => 'required',
            'bank' => 'required',
            'branch' => 'required',
            'account_number' => 'required',
            'name' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('banks')->withErrors($v->errors());
        }
        $banks = Bank::find($cmd);
        $company = Input::get('company');
        $bank = Input::get('bank');
        $branch = Input::get('branch');
        $account_number = Input::get('account_number');
        $name = Input::get('name');
        $coa_id = Input::get('coa_id');
        $status = Input::get('status');

        if ($banks) {
            $banks->company = $company;
            $banks->bank = $bank;
            $banks->branch = $branch;
            $banks->account_number = $account_number;
            $banks->name = $name;
            $banks->coa_id = $coa_id;
            $banks->status = $status;
            $banks->save();

            return redirect('banks')->with([
                'message' => 'Bank Updated Successfully'
            ]);

        } else {
            return redirect('banks')->with([
                'message' => 'Bank Not Found',
                'message_important' => true
            ]);
        }
    }


    /* deleteBank  Function Start Here */
    public function deleteBank($id)
    {

        $banks = Bank::find($id);
        if ($banks) {

            $banks->delete();

            return redirect('banks')->with([
                'message' => 'Bank Deleted Successfully'
            ]);
        } else {
            return redirect('banks')->with([
                'message' => 'Bank Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewBank  Function Start Here */
    public function viewBank($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 34)->first();
        $comp_id = '';
        $banks = Bank::find($id);
        $coa = Coa::where('status','=','active')->where('company','=',$banks->company)->where('sub_header','=','6')->get();
        $company = Company::where('category','=','Internal')->get();
        return view('admin.bank.view-bank', compact('banks','company','coa','comp_id','permcheck'));
    }

    /* download excel function start here */
    public function downloadExcel()
    {
        return response()->download(public_path('assets/template_xls/bank.xls'));
    }

    /* import excel function start here */
    public function importExcel()
    {
        // Get current data from company table
        $account_number = Bank::lists('account_number')->toArray();

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
			})->get();
            if ($fileType != 'xls'){
                return redirect('banks')->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else{
                if(!empty($data) && $data->count()){
                    $insert = array();
                    foreach ($data as $key => $value) {
                        // Skip account_number previously added using in_array
                        if (in_array($value->nomor_rekening, $account_number)){                            
                            continue;
                        } else {
                            //Penomoran id
                            $last_number = Bank::max('id');
                            if($last_number==''){
                                $number = 1;
                            } else {
                                $number = 1 + $last_number;
                            }
                            $kode_coa = explode(" ",$value->bagan_akun);
                            $company = Company::where('company','=',$value->perusahaan)->first();
                            $coa_code = Coa::where('coa_code','=',$kode_coa[0])->first();

                            if($company==''){
                                return redirect('banks')->with([
                                    'message' => 'Perusahaan Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            } 
                            if($coa_code==''){
                                return redirect('banks')->with([
                                    'message' => 'COA Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            } 
                            if($value->bank=='' || $value->cabang=='' || $value->nomor_rekening=='' || $value->atas_nama_rekening==''){
                                return redirect('banks')->with([
                                    'message' => 'Bank Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            } 
                            
                            $banks = new Bank();
                            $banks->id = $number;
                            $banks->company = $company->id;
                            $banks->bank = $value->bank;
                            $banks->branch = $value->cabang;
                            $banks->account_number = $value->nomor_rekening;
                            $banks->name = $value->atas_nama_rekening;
                            $banks->coa_id = $coa_code->id;
                            $banks->save();
                        
                        }
                    }
                    if($banks!=''){
                        return redirect('banks')->with([
                            'message' => 'Bank Added Successfully'
                        ]);
                    }
                } else{
                    return redirect('banks')->with([
                        'message' => 'Bank Not Found',
                        'message_important' => true
                    ]);
                } 
            }
        }
		return back();
    }



}
