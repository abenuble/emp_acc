<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\AccountingBank;
use App\AccountingBkp;
use App\AccountingBkpDetail;
use App\AccountingBkm;
use App\AccountingBkmDetail;
use App\AccountingBankDetail;
use App\EmployeeRolesPermission;
use App\Bank;
use App\Coa;
use App\Coa2;
use App\Ledger;
use App\Ledger2;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;

class AccountingBankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* contract  Function Start Here */
    public function bank()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 76)->first();
        $proof_bank = AccountingBank::orderBy('id','asc')->get();
        return view('admin.proof_bank.proof_bank', compact('proof_bank','permcheck'));

    }

    /* addBank  Function Start Here */
    public function addBank()
    {

        $coa = Coa::where('sub_header','=','6')->get();
        $bkp = AccountingBkp::where('expenditure_type','=','1')->get();
        $bkm = AccountingBkm::where('income_type','=','1')->get();
        return view('admin.proof_bank.add-proof_bank', compact('coa','bkp','bkm'));

    }

    /* getDetailBkm Function Start Here */
    public function getDetailBkm(Request $request)
    {
        $id = $request->id;
        $bkm_det = AccountingBkm::find($id);

        return $bkm_det;
    }

    /* getDetailBkp Function Start Here */
    public function getDetailBkp(Request $request)
    {
        $id = $request->id;
        $bkp_det = AccountingBkp::find($id);

        return $bkp_det;
    }

    /* addPostBank  Function Start Here */
    public function addPostBank(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'date' => 'required',
            'attachment' => 'required',
            'proof_type' => 'required',
            'bank_coa' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('accountings/bank')->withErrors($v->errors());
        }

        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $date2 = get_date_format_inggris($date);
        $proof_number = $this->get_kode_transaksi();
        $attachment = Input::get('attachment');
        $proof_type = Input::get('proof_type');
        $bank_coa = Input::get('bank_coa');
        // echo $proof_type; exit;
        $coa1 = Input::get('coa1');
        $coa2 = Input::get('coa2');
        $amount = Input::get('amount');
        $in = Input::get('in');

        $total = (int)str_replace(',', '', Input::get('total'));
        
        $note = Input::get('note');

        //Penomoran id
        $last_number_id = AccountingBank::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $proof_bank = new AccountingBank();
        $proof_bank->id = $number_id;
        $proof_bank->date = $date;
        $proof_bank->proof_number = $proof_number;
        $proof_bank->attachment = $attachment;
        $proof_bank->proof_type = $proof_type;
        $proof_bank->bank_coa = $bank_coa;
        $proof_bank->total = $total;
        $proof_bank->note = $note;
        $proof_bank->save();

        $proof_id = $proof_bank->id;
        $proof_bank_detail = new AccountingBankDetail();
        $proof_bank_detail->proof_id = $proof_id;
        $proof_bank_detail->amount = (int)str_replace(',', '', $amount[0]);
        if($in[0]!=''){    
            $proof_bank_detail->in = $in[0];

            $data_buku_besar = array();
            $data_buku_besar2 = array();
            if($proof_type=="bank_in"){
                $proof_bank_detail->coa = $coa1;
                $bkm = AccountingBkm::find($coa1);
                $bkmdet = AccountingBkmDetail::where('bkm','=',$coa1)->get();
                $bank = Bank::find($bkm->income_id);
                // print_r($bkm->income_id); exit;
                foreach($bkmdet as $det){
                    $transaction_number = $this->get_kode_transaksi2();
                    $data_buku_besar[] = array(
                        'company'				=> $bank->company,
                        'period'				=> $date2,
                        'date'  				=> $date,
                        'transaction_number'	=> $transaction_number,
                        'information'			=> $bkm->note,
                        'id_coa_debit'			=> $bank_coa,
                        'id_coa_kredit'			=> $det->coa,
                        'nominal'				=> $det->payment,
                        'customer_id'			=> 0,
                        'supplier_id'			=> 0,
                        'invoice_number'		=> $bkm->bkm_code,
                        'id_cashflow_debit'		=> 0,
                        'id_cashflow_credit'	=> 0,
                        'id_file'				=> 0,
                        'baris_file'			=> 0,
                        'hitung'				=> 0,
                    );
                    $data_buku_besar2[] = array(
                        'branch_id'				=> $bank->company,
                        'periode'				=> $date2,
                        'tanggal'  				=> $date,
                        'no_transaksi'      	=> $transaction_number,
                        'keterangan'			=> $bkm->note,
                        'id_coa_debit'			=> $bank_coa,
                        'id_coa_kredit'			=> $det->coa,
                        'nominal'				=> $det->payment,
                        'customer_id'			=> 0,
                        'supplier_id'			=> 0,
                        'no_invoice'    		=> $bkm->bkm_code,
                        'id_cashflow_debit'		=> 0,
                        'id_cashflow_kredit'	=> 0,
                        'id_file'				=> 0,
                        'baris_file'			=> 0,
                        'hitung'				=> 0,
                    );
                    $coa = Coa2::find($det->coa);
                    
                    if($coa){
                        if($coa->tipe_akun>=5){
                            $data_buku_besar3[] = array(
                                'branch_id'				=> $bank->company,
                                'periode'				=> $date2,
                                'tanggal'  				=> $date,
                                'no_transaksi'      	=> $transaction_number,
                                'keterangan'			=> 'Laba Tahun Berjalan',
                                'id_coa_debit'			=> 0,
                                'id_coa_kredit'			=> 100,
                                'nominal'				=> $det->payment,
                                'customer_id'			=> 0,
                                'supplier_id'			=> 0,
                                'no_invoice'    		=> 0,
                                'id_cashflow_debit'		=> 0,
                                'id_cashflow_kredit'	=> 0,
                                'id_file'				=> 0,
                                'baris_file'			=> 0,
                                'hitung'				=> 0,
                            );
                        }
                    }
                }
                // print_r($data_buku_besar); exit;
                $insert_ledger          = Ledger::insert($data_buku_besar);
                $insert_ledger2         = Ledger2::insert($data_buku_besar2);
                if(isset($data_buku_besar3)){
                    $insert_ledger3         = Ledger2::insert($data_buku_besar3);
                }
            } else if($proof_type=="bank_out"){
                $proof_bank_detail->coa = $coa2;
                $bkp = AccountingBkp::find($coa2);
                $bkpdet = AccountingBkpDetail::where('bkp','=',$coa2)->get();
                $bank = Bank::find($bkp->expenditure_id);
                
                foreach($bkpdet as $det){
                    $transaction_number = $this->get_kode_transaksi2();
                    $data_buku_besar[] = array(
                        'company'				=> $bank->company,
                        'period'				=> $date2,
                        'date'  				=> $date,
                        'transaction_number'	=> $transaction_number,
                        'information'			=> $bkp->note,
                        'id_coa_debit'			=> $det->coa,
                        'id_coa_kredit'			=> $bank_coa,
                        'nominal'				=> $det->payment,
                        'customer_id'			=> 0,
                        'supplier_id'			=> 0,
                        'invoice_number'		=> $bkp->bkp_code,
                        'id_cashflow_debit'		=> 0,
                        'id_cashflow_credit'	=> 0,
                        'id_file'				=> 0,
                        'baris_file'			=> 0,
                        'hitung'				=> 0,
                    );
                    $data_buku_besar2[] = array(
                        'branch_id'				=> $bank->company,
                        'periode'				=> $date2,
                        'tanggal'  				=> $date,
                        'no_transaksi'      	=> $transaction_number,
                        'keterangan'			=> $bkp->note,
                        'id_coa_debit'			=> $det->coa,
                        'id_coa_kredit'			=> $bank_coa,
                        'nominal'				=> $det->payment,
                        'customer_id'			=> 0,
                        'supplier_id'			=> 0,
                        'no_invoice'		    => $bkp->bkp_code,
                        'id_cashflow_debit'		=> 0,
                        'id_cashflow_kredit'	=> 0,
                        'id_file'				=> 0,
                        'baris_file'			=> 0,
                        'hitung'				=> 0,
                    );
                    $coa = Coa2::find($det->coa);
                    
                    if($coa){
                        if($coa->tipe_akun>=5){
                            $data_buku_besar3[] = array(
                                'branch_id'				=> $bank->company,
                                'periode'				=> $date2,
                                'tanggal'  				=> $date,
                                'no_transaksi'      	=> $transaction_number,
                                'keterangan'			=> 'Laba Tahun Berjalan',
                                'id_coa_debit'			=> 100,
                                'id_coa_kredit'			=> 0,
                                'nominal'				=> $det->payment,
                                'customer_id'			=> 0,
                                'supplier_id'			=> 0,
                                'no_invoice'    		=> 0,
                                'id_cashflow_debit'		=> 0,
                                'id_cashflow_kredit'	=> 0,
                                'id_file'				=> 0,
                                'baris_file'			=> 0,
                                'hitung'				=> 0,
                            );
                        }
                    }
                }
                $insert_ledger          = Ledger::insert($data_buku_besar);
                $insert_ledger2         = Ledger2::insert($data_buku_besar2);
                if(isset($data_buku_besar3)){
                    $insert_ledger3         = Ledger2::insert($data_buku_besar3);
                }
            }
        } else {
            $proof_bank_detail->in = 0;
        }
        $proof_bank_detail->save();

        if ($proof_bank!='') {
            return redirect('accountings/bank')->with([
                'message' => language_data('Proof of Bank Added Successfully')
            ]);

        } else {
            return redirect('accountings/bank')->with([
                'message' => language_data('Proof of Bank Already Exist'),
                'message_important' => true
            ]);
        }
    }

    /* viewBank  Function Start Here */
    public function viewBank($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 76)->first();
        $coa = Coa::where('sub_header','=','6')->get();
        $proof_bank = AccountingBank::find($id);
        $detail = AccountingBankDetail::where('proof_id','=',$id)->get();
        $bkp = AccountingBkp::where('expenditure_type','=','1')->get();
        $bkm = AccountingBkm::where('income_type','=','1')->get();
        return view('admin.proof_bank.view-proof_bank', compact('proof_bank','detail','bkp','bkm','coa','permcheck'));
    }

    /* generate tansaction code */
	public function get_kode_transaksi(){
		$bln = date('m');
		$thn = date('y');
        $query = DB::table('sys_accounting_banks')
                    ->selectRaw('MID(proof_number,7,5) as id')
                    ->whereRaw('MID(proof_number,1,6) = "BB'.$thn.''.$bln.'"')
                    ->orderBy('proof_number', 'asc')
                    ->limit(1)
                    ->get();
		$kode_baru = format_kode_transaksi('BB',$query);
		return $kode_baru;
	}

    /* generate tansaction code */
	public function get_kode_transaksi2(){
		$bln = date('m');
		$thn = date('y');
        $query = DB::table('sys_ledger')
                    ->selectRaw('MID(transaction_number,7,5) as id')
                    ->whereRaw('MID(transaction_number,1,6) = "LG'.$thn.''.$bln.'"')
                    ->orderBy('transaction_number', 'asc')
                    ->limit(1)
                    ->get();
		$kode_baru = format_kode_transaksi('LG',$query);
		return $kode_baru;
	}


}
