<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Attendance;
use App\Company;
use App\Designation;
use App\Employee;
use App\Department;
use App\Schedule;
use App\ScheduleComponent;
use App\ScheduleEmployee;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;

date_default_timezone_set(app_config('Timezone'));

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* schedules  Function Start Here */
    public function schedules()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 27)->first();

        $schedules = Schedule::orderBy('id','asc')->get();
        return view('admin.schedule.schedules', compact('schedules','permcheck'));

    }

    /* postCustomSearch  Function Start Here */
    public function postCustomSearch(Request $request)
    {
        $v = \Validator::make($request->all(), [

        ]);

        if ($v->fails()) {
            return redirect('attendance/report')->withErrors($v->errors());
        }

        $emp_id = Input::get('employee');
        $dep_id = Input::get('department');
        $des_id = Input::get('designation');
        $comp_id = Input::get('company');

        $designation = Designation::where('did',$dep_id)->get();
        
        $schedule_query = Employee::where('role_id','!=','1');

        if ($emp_id) {
            $schedule_query->Where('id', $emp_id);
        }

        if ($dep_id) {
            $schedule_query->Where('department', $dep_id);
        }

        if ($des_id) {
            $schedule_query->Where('designation', $des_id);
        }

        if ($comp_id) {
            $schedule_query->Where('company', $comp_id);
        }

        $employee = $schedule_query->get();

        $company = Company::all();
        $department = Department::all();
        return view('admin.schedule.employee-schedules', compact('company', 'employee', 'department', 'emp_id', 'dep_id', 'des_id', 'comp_id', 'designation'));

    }

    /* addSchedule  Function Start Here */
    public function addSchedule(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'schedule' => 'required', 'type' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('schedules')->withErrors($v->errors());
        }

        $schedule_name = Input::get('schedule');
        $type = Input::get('type');
        $repeat = Input::get('repeat');
        $holiday = Input::get('holiday');

        
        $exist = Schedule::where('schedule', $schedule_name)->first();
        if ($exist) {
            return redirect('schedules')->with([
                'message' => 'Schedule Already Exist',
                'message_important' => true
            ]);
        }

        $component = Input::get('component');
        $starts_at = Input::get('start_time');
        $ends_at = Input::get('end_time');
        // var_dump($starts_at);
        // exit;
        
        $component_shift = Input::get('component_shift');
        $starts_at_shift = Input::get('start_time_shift');
        $ends_at_shift = Input::get('end_time_shift');

        $num_elements           = 0;
        $num_elements_holiday   = 0;
        
        $sql_data               = array();
        $sql_data_holiday       = array();
        
        if($type=='fixed'){

            $schedule = new Schedule();
            $schedule->type = $type;
            $schedule->schedule = $schedule_name;
            $schedule->repeat = $repeat;
            $schedule->holiday = $holiday;
            $schedule->save();
    
            $id_schedule = $schedule->id;

            while($num_elements<count($component)) {
                if(isset($starts_at[$num_elements])){
                    $sql_data[] = array(
                        'schedule'      => $id_schedule,
                        'component'     => $component[$num_elements],
                        'OfficeInTime'  => $starts_at[$num_elements],
                        'OfficeOutTime' => $ends_at[$num_elements],
                    );   
                } else {
                    $sql_data[] = array(
                        'schedule'      => $id_schedule,
                        'component'     => $component[$num_elements],
                        'OfficeInTime'  => '',
                        'OfficeOutTime' => '',
                    );
                }
                $num_elements++;
            }
            
            $schedule_component = ScheduleComponent::insert($sql_data);

        } else if($type=='shift'){

            $schedule = new Schedule();
            $schedule->type = $type;
            $schedule->schedule = $schedule_name;
            $schedule->repeat = $repeat;
            $schedule->holiday = $holiday;
            $schedule->save();
    
            $id_schedule = $schedule->id;

            if($repeat=='1'){
                while($num_elements<count($component_shift)) {
                    $sql_data[] = array(
                        'schedule'      => $id_schedule,
                        'component'     => $component_shift[$num_elements],
                        'OfficeInTime'  => $starts_at_shift[$num_elements],
                        'OfficeOutTime' => $ends_at_shift[$num_elements],
                    );
                    $num_elements++;
                }
                
                $schedule_component = ScheduleComponent::insert($sql_data);
    
                // if($holiday=='urutan shift 1'){
                //     $schedule_component = new ScheduleComponent();
                //     $schedule_component->schedule = $id_schedule;
                //     $schedule_component->save();
                // } else if($holiday=='urutan shift 2'){
                //     for($a = 1; $a <= 2; $a++){
                //         $schedule_component = new ScheduleComponent();
                //         $schedule_component->schedule = $id_schedule;
                //         $schedule_component->save();
                //     }
                // }

            } else if($repeat=='2'){
                $a = 0;
                $limit = count($component_shift)*2;
                for ($i = 1; $i <= $limit; $i++){
                    if($i > 1){
                        if ($i % 2== 1){
                            $a++;
                        }
                        if ($a > 2){
                            $a= 0;
                        }
                        $schedule_component = new ScheduleComponent();
                        $schedule_component->schedule = $id_schedule;
                        $schedule_component->component = $component_shift[$a];
                        $schedule_component->OfficeInTime = $starts_at_shift[$a];
                        $schedule_component->OfficeOutTime = $ends_at_shift[$a];
                        $schedule_component->save();
                    }else{
                        $schedule_component = new ScheduleComponent();
                        $schedule_component->schedule = $id_schedule;
                        $schedule_component->component = $component_shift[$a];
                        $schedule_component->OfficeInTime = $starts_at_shift[$a];
                        $schedule_component->OfficeOutTime = $ends_at_shift[$a];
                        $schedule_component->save();
                    }
                    
                }
                
                // if($holiday=='urutan shift 1'){
                //     $schedule_component = new ScheduleComponent();
                //     $schedule_component->schedule = $id_schedule;
                //     $schedule_component->save();
                // } else if($holiday=='urutan shift 2'){
                //     for($a = 1; $a <= 2; $a++){
                //         $schedule_component = new ScheduleComponent();
                //         $schedule_component->schedule = $id_schedule;
                //         $schedule_component->save();
                //     }
                // }
            }


        }

        if ($schedule!='') {
            return redirect('schedules')->with([
                'message' => 'Schedule Added Successfully'
            ]);

        } else {
            return redirect('schedules')->with([
                'message' => 'Schedule Already Exist',
                'message_important' => true
            ]);
        }


    }

    /* setScheduleStatus  Function Start Here */
    public function setScheduleStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('schedules')->withErrors($v->fails());
        }

        $schedule  = Schedule::find($cmd);
        
        $type = $schedule->type;
        $repeat = Input::get('repeat');
        $holiday = Input::get('holiday');
        $status = Input::get('status');

        $component_fixed = Input::get('component_fixed');
        $starts_at_fixed = Input::get('start_time_fixed');
        $ends_at_fixed = Input::get('end_time_fixed');

        $component_shifted = Input::get('component_shifted');
        $starts_at_shifted = Input::get('start_time_shifted');
        $ends_at_shifted = Input::get('end_time_shifted');

        $num_elements           = 0;

        $sql_data               = array();
        // echo "<pre>";
        // print_r($component_fixed);
        // echo "</pre>";
        // exit;

        if($schedule){

            if($type=='fixed'){

            $schedule->type = $type;
            $schedule->repeat = $repeat;
            $schedule->holiday = $holiday;
            $schedule->status = $status;
            $schedule->save();
            
            $schedule_component  = ScheduleComponent::where('schedule', '=', $cmd)->get();

            // echo "<pre>";
            // print_r($component_fixed);
            // print_r($starts_at_fixed);
            // print_r($ends_at_fixed);
            // print_r($schedule_component);
            // echo "</pre>";
            // exit;
            foreach($schedule_component as $s) {
                if(isset($component_fixed) ){
                    for($a=0;$a<sizeof($component_fixed);$a++){
                        // echo "test atas2";
                        // echo $starts_at_fixed[$a];
                        // echo $ends_at_fixed[$a];
                        
                        // echo $s->component;
                        // echo $component_fixed[$a];
                        // exit;
                        if(isset($starts_at_fixed[$a]) && $s->component == $component_fixed[$a]){
                            $s->OfficeInTime = $starts_at_fixed[$a];
                            $s->OfficeOutTime = $ends_at_fixed[$a];

                            // echo "test atas";
                            // exit;
                            $s->save();

                            
                        } 
                    }
                } else {
                    $s->OfficeInTime = '';
                    $s->OfficeOutTime = '';
                    $s->save();
                }
            }
            

        } else if($type=='shift'){

            $schedule->type = $type;
            $schedule->repeat = $repeat;
            $schedule->holiday = $holiday;
            $schedule->status = $status;
            $schedule->save();
    
            $schedule_component  = ScheduleComponent::where('schedule', '=', $cmd)->get();
            // echo "<pre>";
            // print_r($component_shifted);
            // echo "</pre>";
            // exit;

            if($repeat=='1'){
                foreach($schedule_component as $s) {
                    for($a=0;$a<sizeof($component_shifted);$a++){
                        if(isset($starts_at_shifted[$a]) && isset($ends_at_shifted[$a]) && $s->component==$component_shifted[$a]){
                            $s->OfficeInTime = $starts_at_shifted[$a];
                            $s->OfficeOutTime = $ends_at_shifted[$a];
                            $s->save();
                        } else {
                            $s->OfficeInTime = '';
                            $s->OfficeOutTime = '';
                            $s->save();
                        }
                    }
                }
                // if($holiday=='urutan shift 1'){
                //     $schedule_component = new ScheduleComponent();
                //     $schedule_component->schedule = $cmd;
                //     $schedule_component->save();
                // } else if($holiday=='urutan shift 2'){
                //     for($a = 1; $a <= 2; $a++){
                //         $schedule_component = new ScheduleComponent();
                //         $schedule_component->schedule = $cmd;
                //         $schedule_component->save();
                //     }
                // }

            } else if($repeat=='2'){
                $a = 0;
                $limit = count($component_shifted)*2;
                for ($i = 1; $i <= $limit; $i++){
                    if($i > 1){
                        if ($i % 2== 1){
                            $a++;
                        }
                        if ($a > 2){
                            $a= 0;
                        }
                        foreach($schedule_component as $s) {
                            if(isset($starts_at_shifted[$a]) && isset($ends_at_shifted[$a]) && $s->component==$component_shifted[$a]){
                                $s->OfficeInTime = $starts_at_shifted[$a];
                                $s->OfficeOutTime = $ends_at_shifted[$a];
                                $s->save();
                            } else {
                                $s->OfficeInTime = '';
                                $s->OfficeOutTime = '';
                                $s->save();
                            }
                        }
                    } else {
                        foreach($schedule_component as $s) {
                            if(isset($starts_at_shifted[$a]) && isset($ends_at_shifted[$a]) && $s->component==$component_shifted[$a]){
                                $s->OfficeInTime = $starts_at_shifted[$a];
                                $s->OfficeOutTime = $ends_at_shifted[$a];
                                $s->save();
                            } else {
                                $s->OfficeInTime = '';
                                $s->OfficeOutTime = '';
                                $s->save();
                            }
                        }
                        $a++;
                    }
                    
                }
                
                // if($holiday=='urutan shift 1'){
                //     $schedule_component = new ScheduleComponent();
                //     $schedule_component->schedule = $cmd;
                //     $schedule_component->save();
                // } else if($holiday=='urutan shift 2'){
                //     for($a = 1; $a <= 2; $a++){
                //         $schedule_component = new ScheduleComponent();
                //         $schedule_component->schedule = $cmd;
                //         $schedule_component->save();
                //     }
                // }
            }


        }

            return redirect('schedules')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('schedules')->with([
                'message' => 'Schedule not found',
                'message_important'=>true
            ]);
        }

    }

    /* viewSchedule  Function Start Here */
    public function viewSchedule($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 27)->first();

        $schedule = Schedule::find($id);
        $schedule_component = ScheduleComponent::where('schedule', '=', $id)->get();
        return view('admin.schedule.view-schedule', compact('schedule','schedule_component','permcheck'));
    }

    /* viewEmployeeSchedule  Function Start Here */
    public function viewEmployeeSchedule($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 43)->first();
        $emp_id = '';
        $dep_id = '';
        $des_id = '';
        $comp_id = '';

        $schedules = Schedule::all();
        $department = Department::all();
        $company = Company::all();
        $month_now = date('Y');

        $days_before = strtotime(date('Y-m-d'));

        $employee = Employee::find($id);
        $schedule = ScheduleEmployee::where('emp_id','=',$id)->get();
        $schedule_type = Schedule::where('status','=','active')->get();
        return view('admin.schedule.view-employee-schedule', compact('days_before','schedule_type','month_now','schedule','employee','company','comp_id','department', 'date_from','dep_id', 'des_id','permcheck'));
    }

    /* editSchedule  Function Start Here */
    public function editSchedule(Request $request)
    {
        $emp_id=Input::get('emp_id');
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'starts_at'=>'required', 'ends_at'=>'required', 'information'=>'required'
        ]);

        if($v->fails()){
            return redirect('schedules/view-employee/'.$emp_id)->withErrors($v->fails());
        }

        $schedule_employee  = ScheduleEmployee::find($cmd);
        if($schedule_employee){
            $schedule_employee->starts_at=$request->starts_at;
            $schedule_employee->ends_at=$request->ends_at;
            $schedule_employee->information=$request->information;
            $schedule_employee->save();
            

            return redirect('schedules/view-employee/'.$emp_id)->with([
                'message' => 'Schedule updated successfully',
            ]);

        }else{
            return redirect('schedules/view-employee/'.$emp_id)->with([
                'message' => 'Schedule not found',
                'message_important'=>true
            ]);
        }

    }

    /* editScheduleType  Function Start Here */
    public function editScheduleType(Request $request)
    {

        $emp_id=Input::get('emp_id');
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
        ]);

        if($v->fails()){
            return redirect('schedules/view-employee/'.$emp_id)->withErrors($v->fails());
        }
        $schedule = Input::get('schedule');
        $month = Input::get('month');
        $month = get_date_format_inggris_bulan($month);
        $employee = Employee::find($emp_id);
        $schedule_info = Schedule::find($schedule);
        if($employee){
            $employee->schedule=$request->schedule;
            $employee->save();
            if($schedule_info->type=='fixed' && $schedule_info->id!='1'){

                $exist_check = ScheduleEmployee::where('emp_id','=',$emp_id)->where('month','=',$month)->first();
                
                if ($exist_check) {
                    $employee_schedule = ScheduleEmployee::where('emp_id','=',$emp_id)->where('month','=',$month)->get();

                } else {
                    //create next month ScheduleEmployee Save
                    $num_elements_schedule  = 0;
                    $schedule_component_shift = ScheduleComponent::where('schedule','=',$schedule)->get();
                    
                    $list = array();
                    $list_day = array();
                    $month_update = date('Y', strtotime($month));
                    
                    
                    for($m=1;$m<=12;$m++){
                        for($d=1; $d<=31; $d++){
                            $time=mktime(12, 0, 0, $m, $d,  $month);          
                           
                                $list[]=date('Y-m-d', $time);
                                $list_day[]=get_date_day($time);
                            
                        }
                    }
    
                    $sql_data_schedule      = array();
                    for($num_elements_schedule = 0;$num_elements_schedule<count($list);$num_elements_schedule++){
                        $sql_data_schedule[] = array(
                            'id'                => "",
                            'emp_id'            => $emp_id,
                            'date'              => $list[$num_elements_schedule],
                            'day'               => $list_day[$num_elements_schedule],
                            'month'             => $month,
                            'updated_at'        => "",
                            'created_at'        => ""
                        );
                        
                    }
                    $schedule_employee     = ScheduleEmployee::insert($sql_data_schedule);
                }
                
                $employee_schedule_sunday = ScheduleEmployee::where('emp_id','=',$emp_id)->where('day','=','sunday')->where('month','=',$month)->get();
                $schedule_component_sunday = ScheduleComponent::where('schedule','=',$schedule)->where('component','=','sunday')->first();
    
                foreach($employee_schedule_sunday as $es){
                    $es->starts_at = $schedule_component_sunday->OfficeInTime;
                    $es->ends_at = $schedule_component_sunday->OfficeOutTime;
                    $es->save();
                }

                $employee_schedule_monday = ScheduleEmployee::where('emp_id','=',$emp_id)->where('day','=','monday')->where('month','=',$month)->get();
                $schedule_component_monday = ScheduleComponent::where('schedule','=',$schedule)->where('component','=','monday')->first();
    
                foreach($employee_schedule_monday as $es){
                    $es->starts_at = $schedule_component_monday->OfficeInTime;
                    $es->ends_at = $schedule_component_monday->OfficeOutTime;
                    $es->save();
                }

                $employee_schedule_tuesday = ScheduleEmployee::where('emp_id','=',$emp_id)->where('day','=','tuesday')->where('month','=',$month)->get();
                $schedule_component_tuesday = ScheduleComponent::where('schedule','=',$schedule)->where('component','=','tuesday')->first();
    
                foreach($employee_schedule_tuesday as $es){
                    $es->starts_at = $schedule_component_tuesday->OfficeInTime;
                    $es->ends_at = $schedule_component_tuesday->OfficeOutTime;
                    $es->save();
                }

                $employee_schedule_wednesday = ScheduleEmployee::where('emp_id','=',$emp_id)->where('day','=','wednesday')->where('month','=',$month)->get();
                $schedule_component_wednesday = ScheduleComponent::where('schedule','=',$schedule)->where('component','=','wednesday')->first();
    
                foreach($employee_schedule_wednesday as $es){
                    $es->starts_at = $schedule_component_wednesday->OfficeInTime;
                    $es->ends_at = $schedule_component_wednesday->OfficeOutTime;
                    $es->save();
                }

                $employee_schedule_thursday = ScheduleEmployee::where('emp_id','=',$emp_id)->where('day','=','thursday')->where('month','=',$month)->get();
                $schedule_component_thursday = ScheduleComponent::where('schedule','=',$schedule)->where('component','=','thursday')->first();
    
                foreach($employee_schedule_thursday as $es){
                    $es->starts_at = $schedule_component_thursday->OfficeInTime;
                    $es->ends_at = $schedule_component_thursday->OfficeOutTime;
                    $es->save();
                }

                $employee_schedule_friday = ScheduleEmployee::where('emp_id','=',$emp_id)->where('day','=','friday')->where('month','=',$month)->get();
                $schedule_component_friday = ScheduleComponent::where('schedule','=',$schedule)->where('component','=','friday')->first();
    
                foreach($employee_schedule_friday as $es){
                    $es->starts_at = $schedule_component_friday->OfficeInTime;
                    $es->ends_at = $schedule_component_friday->OfficeOutTime;
                    $es->save();
                }

                $employee_schedule_saturday = ScheduleEmployee::where('emp_id','=',$emp_id)->where('day','=','saturday')->where('month','=',$month)->get();
                $schedule_component_saturday = ScheduleComponent::where('schedule','=',$schedule)->where('component','=','saturday')->first();
    
                foreach($employee_schedule_saturday as $es){
                    $es->starts_at = $schedule_component_saturday->OfficeInTime;
                    $es->ends_at = $schedule_component_saturday->OfficeOutTime;
                    $es->save();
                }

                
                
            } else if($schedule_info->type=='shift' && $schedule_info->id!='1'){
                

                $exist_check = ScheduleEmployee::where('emp_id','=',$emp_id)->where('month','=',$month)->first();
                
                if ($exist_check) {
                    $employee_schedule = ScheduleEmployee::where('emp_id','=',$emp_id)->where('month','=',$month)->get();

                } else {
                    //create next month ScheduleEmployee Save
                    $num_elements_schedule  = 0;
                    
                    $list = array();
                    $list_day = array();
                    $month_update = date('Y', strtotime($month));
                    $year = date('Y');

                    for($m=1;$m<=12;$m++){
                        for($d=1; $d<=31; $d++){
                            $time=mktime(12, 0, 0, $m, $d, $month);          
                           
                                $list[]=date('Y-m-d', $time);
                                $list_day[]=get_date_day($time);
                            
                        }
                    }
    
                    $sql_data_schedule      = array();
                    for($num_elements_schedule = 0;$num_elements_schedule<count($list);$num_elements_schedule++){
                        $sql_data_schedule[] = array(
                            'id'                => "",
                            'emp_id'            => $emp_id,
                            'date'              => $list[$num_elements_schedule],
                            'day'               => $list_day[$num_elements_schedule],
                            'month'             => $month,
                            'updated_at'        => "",
                            'created_at'        => ""
                        );
                        
                    }
                    $schedule_employee     = ScheduleEmployee::insert($sql_data_schedule);

                }

                $employee_schedule = ScheduleEmployee::where('emp_id','=',$emp_id)->where('month','=',$month)->get();
                $schedule_component_shift = ScheduleComponent::where('schedule','=',$schedule)->get()->toArray();
                $a = 0;
                foreach($employee_schedule as $es){
                    $es->starts_at = $schedule_component_shift[$a]['OfficeInTime'];
                    $es->ends_at = $schedule_component_shift[$a]['OfficeOutTime'];
                    $a++;
                    if($a==count($schedule_component_shift)){
                        $a = 0;
                    } 
                    $es->save();
                }

                if($schedule_info->holiday=='sabtu dan minggu'){
                    $employee_schedule_sunday = ScheduleEmployee::where('emp_id','=',$emp_id)->where('day','=','sunday')->where('month','=',$month)->get();
        
                    foreach($employee_schedule_sunday as $es){
                        $es->starts_at = '';
                        $es->ends_at = '';
                        $es->save();
                    }

                    $employee_schedule_saturday = ScheduleEmployee::where('emp_id','=',$emp_id)->where('day','=','saturday')->where('month','=',$month)->get();
        
                    foreach($employee_schedule_saturday as $es){
                        $es->starts_at = '';
                        $es->ends_at = '';
                        $es->save();
                    }
                } else if($schedule_info->holiday=='sabtu'){
                    $employee_schedule_saturday = ScheduleEmployee::where('emp_id','=',$emp_id)->where('day','=','saturday')->where('month','=',$month)->get();
        
                    foreach($employee_schedule_saturday as $es){
                        $es->starts_at = '';
                        $es->ends_at = '';
                        $es->save();
                    }
                } else if($schedule_info->holiday=='minggu'){
                    $employee_schedule_sunday = ScheduleEmployee::where('emp_id','=',$emp_id)->where('day','=','sunday')->where('month','=',$month)->get();
        
                    foreach($employee_schedule_sunday as $es){
                        $es->starts_at = '';
                        $es->ends_at = '';
                        $es->save();
                    }
                }

            } else if($schedule_info->id=='1'){
                
                $employee_schedule = ScheduleEmployee::where('emp_id','=',$emp_id)->where('month','=',$month)->get();

                foreach($employee_schedule as $es){
                    $schedule_component = ScheduleComponent::where('schedule','=',$schedule)->where('component','=','Default')->first();
                    $es->starts_at = $schedule_component->OfficeInTime;
                    $es->ends_at = $schedule_component->OfficeOutTime;
                    $es->save();
                }

                $exist_check = ScheduleEmployee::where('emp_id','=',$emp_id)->where('month','=',$month)->first();
                
                if ($exist_check) {
                    $employee_schedule = ScheduleEmployee::where('emp_id','=',$emp_id)->where('month','=',$month)->get();

                } else {
                    //create next month ScheduleEmployee Save
                    $num_elements_schedule  = 0;
                    $schedule_component = ScheduleComponent::where('schedule','=',$schedule)->where('component','=','Default')->first();
                    
                    $list = array();
                    $list_day = array();
                    $month_update = date('Y', strtotime($month));
                    $year = date('Y');
                    
                    for($m=1;$m<=12;$m++){
                    for($d=1; $d<=31; $d++){
                        $time=mktime(12, 0, 0, $m, $d, $month);          
                       
                            $list[]=date('Y-m-d', $time);
                            $list_day[]=get_date_day($time);
                        
                    }
                }
                    $sql_data_schedule      = array();
                    for($num_elements_schedule = 0;$num_elements_schedule<count($list);$num_elements_schedule++){
                        $sql_data_schedule[] = array(
                            'id'                => "",
                            'emp_id'            => $emp_id,
                            'date'              => $list[$num_elements_schedule],
                            'day'               => $list_day[$num_elements_schedule],
                            'starts_at'         => $schedule_component->OfficeInTime,
                            'ends_at'           => $schedule_component->OfficeOutTime,
                            'month'             => $month,
                            'updated_at'        => "",
                            'created_at'        => ""
                        );
                        
                    }
                    $schedule_employee     = ScheduleEmployee::insert($sql_data_schedule);

                }
            }
            
            return redirect('schedules/view-employee/'.$emp_id)->with([
                'message' => 'Schedule updated successfully',
            ]);

        }else{
            return redirect('schedules/view-employee/'.$emp_id)->with([
                'message' => 'Schedule not found',
                'message_important'=>true
            ]);
        }

    }

    /* employeeSchedules  Function Start Here */
    public function employeeSchedules()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 43)->first();
        $employee_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
        $emp_id = '';
        $dep_id = '';
        $des_id = '';
        $comp_id = '';

        $employee = Employee::where('role_id','!=','1')->get();
        $schedules = Schedule::all();
        $department = Department::all();
        $company = Company::all();
        return view('admin.schedule.employee-schedules', compact('company','comp_id','employee','schedules','department', 'emp_id', 'dep_id', 'des_id','permcheck','employee_permission'));

    }

}
