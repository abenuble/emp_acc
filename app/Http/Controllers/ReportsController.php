<?php

namespace App\Http\Controllers;

use App\AwardList;
use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Employee;
use App\EmployeeResign;
use App\EmployeeDependents;
use App\Project;
use App\ProjectItem;
use App\LastEducation;
use App\LeaveType;
use App\Company;
use App\Payment;
use App\Resign;
use App\JobApplicants;
use App\Leave;
use App\Mutation;
use App\Attendance;
use App\Loan;
use App\Payroll;
use App\ProvidentFund;
use App\Procurement;
use App\Designation;
use App\SMSGateway;
use Dompdf\Adapter\CPDF;
use App\PaymentComponent;
use App\EmployeeAssessment;
use Illuminate\Http\Request;
use App\OvertimeWarrant;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use League\Flysystem\Exception;
use Twilio\Rest\Client;
use Carbon\Carbon;
use DateTime;
use DatePeriod;
use DateInterval;
use WkPdf;
use Excel;
class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* payrollSummery  Function Start Here */
    public function payrollSummery()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 84)->first();

        $employees = Employee::where('role_id','!=','1')->where('status', 'active')->get();
        return view('admin.report.payroll-summery', compact('employees','permcheck'));
    }
    public function getProjects()
    {
      

        $employee = Project::all();

        $members_dropdown = array();

        foreach($employee as $emp){
            $members_dropdown[] = array("id" => $emp->id, "text" => $emp->project.'('. $emp->project_number.')');
        }
        return $members_dropdown;
    }
    
    public function getCompanies()
    {
      

        $employee = Company::all();

        $members_dropdown = array();

        foreach($employee as $emp){
            $members_dropdown[] = array("id" => $emp->id, "text" => $emp->company);
        }
        return $members_dropdown;
    }
    public function getLeaveType()
    {
      

        $employee = LeaveType::all();

        $members_dropdown = array();

        foreach($employee as $emp){
            $members_dropdown[] = array("id" => $emp->id, "text" => $emp->leave);
        }
        return $members_dropdown;
    }
    
    public function leavesSummery(Request $request)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 87)->first();
        $thn=input::get('tahun');
        $project = Input::get('share_with_specific');
        $leavetype = Input::get('share_with_specific1');

        $leaves=[];
        $projectchart=[];
        $leavechart=[];
        $ctr=0;
        $projects=Project::all();
        $curYear = date('Y'); 
        if($thn!=null && $thn!="" && $thn!=" "){
            $curYear=$thn;
        }
        $projectt = explode(",", $project);
        $leavetypee = explode(",", $leavetype);
        if((sizeof($projectt)>0 && $projectt[0]!=''&& $projectt[0]!=null) || (sizeof($leavetypee)>0 && $leavetypee[0]!=''&& $leavetypee[0]!=null)){
            $coun=0;
            if(sizeof($projectt)>0 && $projectt[0]!=''&& $projectt[0]!=null){
            foreach($projectt as $p){
                $emp=Employee::where('project','=',$p)->where('role_id','!=',1)->where('status', 'active')->get();

                $tmptotal=0;
                foreach ($emp as $e){
                    if((sizeof($leavetypee)>0 && $leavetypee[0]!=''&& $leavetypee[0]!=null)){
                    $attemp= Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereYear('leave_from','=',$curYear)->sum(DB::raw('DATEDIFF(leave_to, leave_from)'));
                    }
                    else{
                        $attemp= Leave::where('emp_id','=',$e->id)->whereYear('leave_from','=',$curYear)->sum(DB::raw('DATEDIFF(leave_to, leave_from)'));
 
                    }
                    $tmptotal+=$attemp;
                }
                $tmp=array(
                    'namaproyek'=>Project::find($p)->project,
                    'total'=>$tmptotal
               
                );
                $projectchart[$coun]=$tmp;
         
                $coun++;
            }
        }
            $coun=0;
            if(sizeof($leavetypee)>0 && $leavetypee[0]!=''&& $leavetypee[0]!=null){
            foreach($leavetypee as $p){
                if((sizeof($projectt)>0 && $projectt[0]!=''&& $projectt[0]!=null)){
                    $emp=Employee::whereIn('project',$projectt)->where('role_id','!=','1')->where('status', 'active')->get();
                }else{
                $emp=Employee::where('role_id','!=','1')->where('status', 'active')->get();
            }

                $tmptotal=0;
                foreach ($emp as $e){
                    $attemp= Leave::where('emp_id','=',$e->id)->whereYear('leave_from','=',$curYear)->sum(DB::raw('DATEDIFF(leave_to, leave_from)'));
                    $tmptotal+=$attemp;
                }
                $tmp=array(
                    'namaleave'=>LeaveType::find($p)->leave,
                    'total'=>$tmptotal
               
                );
                $leavechart[$coun]=$tmp;
         
                $coun++;
            }
        }
        if((sizeof($projectt)>0 && $projectt[0]!=''&& $projectt[0]!=null)){
            $employees = Employee::where('role_id','!=','1')->where('status', 'active')->whereIn('project',$projectt)->get();
            }
            else{
                $employees = Employee::where('role_id','!=','1')->where('status', 'active')->get();
            }
        }
        else{
        $employees = Employee::where('role_id','!=','1')->where('status', 'active')->get();
        }

        foreach($employees as $e){
            if((sizeof($leavetypee)>0 && $leavetypee[0]!=''&& $leavetypee[0]!=null)){
                $tmp=array(
                    'jan'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','1')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                    'feb'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','2') ->whereYear('leave_from','=',$curYear)->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                    'mar'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','3')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                    'apr'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','4')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                    'may'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','5')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                    'jun'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','6')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                    'jul'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','7')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                    'aug'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','8')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                    'sep'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','9')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                    'oct'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','10')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                    'nov'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','11')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                    'des'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','12') ->whereYear('leave_from','=',$curYear)->sum(DB::raw('DATEDIFF(leave_to, leave_from)'))
                );
            }
            else{
            $tmp=array(
                'jan'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','1')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'feb'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','2') ->whereYear('leave_from','=',$curYear)->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'mar'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','3')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'apr'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','4')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'may'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','5')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'jun'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','6')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'jul'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','7')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'aug'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','8')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'sep'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','9')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'oct'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','10')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'nov'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','11')->whereYear('leave_from','=',$curYear) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'des'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','12') ->whereYear('leave_from','=',$curYear)->sum(DB::raw('DATEDIFF(leave_to, leave_from)'))
            );
        }
            $leaves[$ctr]=$tmp;
         
                $ctr++;
        }
      //  print_r($leaves);
        return view('admin.report.leaves-summery', compact('employees','leaves','projects','curYear','project','projectchart','leavetype','leavechart','permcheck'));
    }
    public function projectSummery(Request $request)
    {   
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 15)->first();
        $data=[];
        $ctr=0;
       $company=input::get('share_with_specific');
       if($company!=null&& $company!="" ){
        $companyy = explode(",", $company);
        foreach($companyy as $c){
            $projectval=Project::where('company','=',$c)->where('status','!=','drafted')->sum('value');
            if($projectval==null){$projectval=0;}
            $tmp=array(
                'company'=> Company::find($c)->company,
                'projectval'=>$projectval
            );
            $data[$ctr]=$tmp;
         
            $ctr++;
        }
       }
       else{
        $comp=Company::where('status','active')->get();
        foreach($comp as $c){
         $projectval=Project::where('company','=',$c->id)->where('status','!=','drafted')->sum('value');
         if($projectval==null){$projectval=0;}
         $tmp=array(
             'company'=> Company::find($c->id)->company,
             'projectval'=>$projectval
         );
       
         $data[$ctr]=$tmp;
      
         $ctr++;
        }

    }
   
        return view('admin.report.project-summery',compact('data','company','permcheck'));
    }


      //ambil daftar employee untuk upah lembur
      public function getEmployeeUpahLembur(Request $request){
        $month = $request->bulan;
        $year = $request->tahun;
        if($month == 'Januari'){
            $month = 1;
        } if($month == 'Februari'){
            $month = 2;
        } if($month == 'Maret'){
            $month = 3;
        } if($month == 'April'){
            $month = 4;
        } if($month == 'Mei'){
            $month = 5;
        } if($month == 'Juni'){
            $month = 6;
        } if($month == 'Juli'){
            $month = 7;
        } if($month == 'Agustus'){
            $month = 8;
        } if($month == 'September'){
            $month = 9;
        } if($month == 'Oktober'){
            $month = 10;
        } if($month == 'November'){
            $month = 11;
        } if($month == 'Desember'){
            $month = 12;
        }
        echo '<option value="0">Nothing Selected</option>';
        if($month!='' && $month!=null && $year!='' && $year!=null){
       $Employee= Employee::whereRaw("status = 'active'")->whereRaw("employee_type = 'Internal'")->whereIn('id', function($query) use ($month,$year)
        {
        $query->select('emp_id')
              ->from('sys_overtime_warrant')
              ->whereRaw("status = 'accepted' AND ((MONTH(date_from) = ".$month." AND YEAR(date_from) =".$year." ) OR  (MONTH(date_to) = ".$month." AND YEAR(date_to) =".$year." )) ");
        })->groupby('id')->distinct()->get();
            foreach ($Employee as $e) {
                echo '<option value="' . $e->id . '">' . $e->fname.' '.$e->lname.'('.$e->employee_code.')' . '</option>';
            }
        }
    }
    
    public function salarySummery(Request $request)
    {
        $curYear= date('Y'); 
        $curMonth= date('M');
        $year=input::get("year");
        $month=input::get("month");
        $project=input::get("project");
        $company=input::get("company");
        $tmpmonth="0";
        $whereother=[];
        if($year==null ||$year=="" || $year=="0"){
            $year=$curYear;
        }
        if($month==null ||$month==""){
            $month=$curMonth;
        }
        if($month!=null &&$month!="" && $month!="0"){
            if($month == 'Januari'){
                $month = 'January';
            } if($month == 'Februari'){
                $month = 'February';
            } if($month == 'Maret'){
                $month = 'March';
            } if($month == 'April'){
                $month = 'April';
            } if($month == 'Mei'){
                $month = 'May';
            } if($month == 'Juni'){
                $month = 'June';
            } if($month == 'Juli'){
                $month = 'July';
            } if($month == 'Agustus'){
                $month = 'August';
            } if($month == 'September'){
                $month = 'September';
            } if($month == 'Oktober'){
                $month = 'October';
            } if($month == 'November'){
                $month = 'November';
            } if($month == 'Desember'){
                $month = 'December';
            }
           $tmpmonth= date_parse($month);
        }
        
  
        if($project!=null &&$project!="" && $project!="0"){
            $whereother['project']=$project;
        }
        if($company!=null &&$company!="" && $company!="0"){
            $whereother['company']=$company;
        }
        $projects=Project::all();
        $companies=Company::all();
        $salaries=PaymentComponent::whereIn('payment', function($query)use ($whereother,$year,$tmpmonth,$month)
        {
            if($month!=null &&$month!="" && $month!="0"){
            $query->select('id')
                  ->from('sys_payments')
                  ->where($whereother)
                 ->where('status','=','approved')
                  ->whereYear(DB::raw("STR_TO_DATE(month, '%Y-%m')"),'=',$year)
                  ->whereMonth(DB::raw("STR_TO_DATE(month, '%Y-%m')"),'=',strval($tmpmonth['month']));}
                  else{
                    $query->select('id')
                    ->from('sys_payments')
                    ->where($whereother)
                    ->where('status','=','approved')
                    ->whereYear(DB::raw("STR_TO_DATE(month, '%Y-%m')"),'=',$year);
                  }
        })->get();
        if($month == 'January'){
            $month = 'Januari';
        } if($month == 'February'){
            $month = 'Februari';
        } if($month == 'March'){
            $month = 'Maret';
        } if($month == 'April'){
            $month = 'April';
        } if($month == 'May'){
            $month = 'Mei';
        } if($month == 'Juni'){
            $month = 'June';
        } if($month == 'July'){
            $month = 'Juli';
        } if($month == 'August'){
            $month = 'Agustus';
        } if($month == 'September'){
            $month = 'September';
        } if($month == 'October'){
            $month = 'Oktober';
        } if($month == 'November'){
            $month = 'November';
        } if($month == 'December'){
            $month = 'Desember';
        }
        return view('admin.report.salary-summery',compact('projects','companies','salaries','project','company','year','month'));
    }
    public function procurementSummery(Request $request)
    {   
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 91)->first();
        $project=input::get('share_with_specific');
        
        if($project!=null &&$project!="" ){
            $whr=[];
            $projectt = explode(",", $project);
            foreach($projectt as $c){
                array_push($whr,$c);
           // $procurement=Procurement::all();}
        }
        $procurement=Procurement::whereNotNull('project')->whereIn('project',$whr)->groupBy('project')->get();
    }
        else{
        $procurement=Procurement::whereNotNull('project')->groupBy('project')->get();
    }
        $totaldiajukan=0;
        $totalrealisasi=0;
        for($i=0;$i<sizeof($procurement);$i++){
       
            $diajukan=ProjectItem::select(DB::raw('SUM(amount) as diajukan'),DB::raw('SUM(amount_bought) as realisasi'))->where('id_project',$procurement[$i]->project)->get();
          // print_r($diajukan);
         
            if(sizeof($diajukan)>0){  
                if($diajukan[0]->diajukan!=null && $diajukan[0]->diajukan!=''){
                 $procurement[$i]->diajukan=$diajukan[0]->diajukan;
                 $totaldiajukan+=$diajukan[0]->diajukan;
                }
                 else{
                    $procurement[$i]->diajukan=0;
                 }
                 if($diajukan[0]->realisasi!=null && $diajukan[0]->realisasi!=''){
                $procurement[$i]->realisasi=$diajukan[0]->realisasi;
              
                $totalrealisasi+=$diajukan[0]->realisasi;}
                else{
                    $procurement[$i]->realisasi=0;
                 }
             
            }
            else{
            $procurement[$i]->diajukan=0;
            $procurement[$i]->realisasi=0;
       
        }
            
        }
       // print_r($procurement);
    
        return view('admin.report.procurement-summery',compact('procurement','project','totaldiajukan','totalrealisasi','permcheck'));
    }
    public function employeeDataSummery(Request $request)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 89)->first();
        $locationq=input::get('location');
        $projectq = Input::get('project');
        $where=[];
        if($locationq!=null &&$locationq!="" &&$locationq!="0"){
            $where['pre_city']=$locationq;
        }
            if($projectq!=null &&$projectq!="" &&$projectq!="0"){
                $where['project']=$projectq;
            }
       $lokasi=Employee::select('pre_city')->distinct()->whereNotNull('pre_city')->where('pre_city','!=',"")->get();
       $project=Project::all();
       $gendermale=Employee::where('status','active')->where('gender','male')->where($where)->count();
       $genderfemale=Employee::where('status','active')->where('gender','female')->where($where)->count();
       $sma=Employee::where('status','active')->where('last_education','3')->where($where)->count();
       $d3=Employee::where('status','active')->where('last_education','5')->where($where)->count();
       $s1=Employee::where('status','active')->where('last_education','7')->where($where)->count();
       $s2=Employee::where('status','active')->where('last_education','8')->where($where)->count();
       $s05=Employee::where('status','active')->where('salary','>=',0)->where('salary','<=',5000000)->where($where)->count();
       $s510=Employee::where('status','active')->where('salary','>',5000000)->where('salary','<=',10000000)->where($where)->count();
       $s1015=Employee::where('status','active')->where('salary','>',10000000)->where('salary','<=',15000000)->where($where)->count();
       $s15=Employee::where('status','active')->where('salary','>',15000000)->where($where)->count();

       $u120=Employee::where('status','active')->where( DB::raw("(YEAR(CURDATE())-YEAR(dob)) "),'>','0' )->where( DB::raw("(YEAR(CURDATE())-YEAR(dob))"),'<=','20' )->where($where)->count();
       $u2130=Employee::where('status','active')->where( DB::raw("(YEAR(CURDATE())-YEAR(dob)) "),'>','20' )->where( DB::raw("(YEAR(CURDATE())-YEAR(dob)) "),'<=','30' )->where($where)->count();
       $u3150=Employee::where('status','active')->where( DB::raw("(YEAR(CURDATE())-YEAR(dob)) "),'>','30' )->where( DB::raw("(YEAR(CURDATE())-YEAR(dob))"),'<=','50' )->where($where)->count();
       $u51=Employee::where('status','active')->where( DB::raw("(YEAR(CURDATE())-YEAR(dob)) "),'>','50' )->where($where)->count();
        return view('admin.report.employee-data-summery',compact('lokasi','project','genderfemale','gendermale', 'sma','d3','s1','s2','locationq','projectq','s05','s510','s1015','s15','u120','u2130','u3150','u51','permcheck'));
    }
    public function employeeMutation(Request $request)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 102)->first();
        $type = '0';
        $mutation_from = '0';
        $mutation_to = '0';
        if(input::get('type')!=''){
            $type = input::get('type');
        }
        
        if(input::get('mutation_from')!=''){
            $mutation_from = Input::get('mutation_from');
            $mutation_from = get_date_format_inggris($mutation_from);
        }
        
        if(input::get('mutation_to')!=''){
            $mutation_to = Input::get('mutation_to');
            $mutation_to = get_date_format_inggris($mutation_to);
        }

        $mutation = Mutation::where('status','=','accepted');

        if($type!='0'){
            $mutation->where('mutation_type','=',$type);
        }
        if($mutation_from!='0'){
            $mutation->where('mutation_date','>=',$mutation_from);
        }
        if($mutation_to!='0'){
            $mutation->where('mutation_date','<=',$mutation_to);
        }
        $mutations = $mutation->get();
        return view('admin.report.employee-mutation',compact('mutations','mutation_from','mutation_to','type','permcheck'));
    }
    public function downloadEmployeeMutation($a,$b,$c){
        $type=$a;
        $mutation_from=$b;
        $mutation_to=$c;
        
        $mutation = Mutation::where('status','=','accepted');
        if($type!='0'){
            $mutation->where('mutation_type','=',$type);
        }
        if($mutation_from!='0'){
            $mutation_from = get_date_format_inggris($mutation_from);
            $mutation->where('mutation_date','>=',$mutation_from);
        }
        if($mutation_to!='0'){
            $mutation_to = get_date_format_inggris($mutation_to);
            $mutation->where('mutation_date','<=',$mutation_to);
        }
        $mutations = $mutation->get();
            
        $data2 = array();
        $ctr = 0;
        foreach($mutations as $q){
            if(isset($q->project_number)){
                $data2[$ctr][language_data('Client Contract Number')] = $q->project_info->project_number;
            }
            if(isset($a->company_name)){
                $data2[$ctr][language_data('Company')] = $q->company_info->company;
            }
            if(isset($q->employee_name)){
                $data2[$ctr][language_data('Employee')] = $q->employee_info->fname.' '.$q->employee_info->lname;
            }
            if(isset($q->company_code_new)){
                $data2[$ctr][language_data('Area Code')] = $q->company_info->company_code.' > '.$q->company_info_new->company_code;
            }
            if(isset($q->payment_type_new)){
                $data2[$ctr][language_data('Payment Type')] = $q->payment_old_info->payroll_name.' > '.$q->payment_new_info->payroll_name;
            }
            if(isset($q->designation_new)){
                $data2[$ctr][language_data('Designation')] = $q->designation_old_info->department_name->department.'/'.$q->designation_old_info->designation.' > '.$q->designation_new_info->department_name->department.'/'.$q->designation_new_info->designation;
            }
            if(isset($q->location_new)){
                $data2[$ctr][language_data('Location')] = $q->location_old.' > '.$q->location_new;
            }
           $ctr++;
        }

        $type = 'xls';
        return Excel::create('Employee Mutation', function($excel) use ($data2) {
            $excel->sheet('mySheet', function($sheet) use ($data2)
            {
                $sheet->fromArray($data2);
            });
        })->download($type);
    }
    public function employeeActive(Request $request)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 103)->first();
        $period_from = '0';
        $period_to = '0';
        if(input::get('period_from')!=''){
            $period_from = Input::get('period_from');
            $period_from = get_date_format_inggris($period_from);
        }
        
        if(input::get('period_to')!=''){
            $period_to = Input::get('period_to');
            $period_to = get_date_format_inggris($period_to);
        }

        // echo $period_from;
        // echo $period_to; exit;
        // DB::enableQueryLog();
        $employee = Employee::where('status','=','active')->where('role_id','!=','1');
        // $employee->where('employee_type','=','Outsourcing');

        if($period_from!='0'){
            $employee->where('doj','>=',$period_from);
        }
        if($period_to!='0'){
            $employee->where('doj','<=',$period_to);
        }
        $employees = $employee->get();
        // $query = DB::getQueryLog();
        // echo "<pre>";
        // print_r($query);
        // print_r($employees); exit;
        return view('admin.report.employee-active',compact('employees','period_from','period_to','permcheck'));
    }
    public function downloadEmployeeActive($a,$b){
        $period_from=$a;
        $period_to=$b;
        
        $employee = Employee::where('status','=','active')->where('role_id','!=','1');
        // $employee->where('employee_type','=','Outsourcing');
        if($period_from!='0'){
            $period_from = get_date_format_inggris($period_from);
            $employee->where('doj','>=',$period_from);
        }
        if($period_to!='0'){
            $period_to = get_date_format_inggris($period_to);
            $employee->where('doj','<=',$period_to);
        }
        $employees = $employee->get()->toArray();

        $data2 = array();
        $ctr = 0;
        foreach($employees as $q){
            // if(isset($q['employee_code'])){
                $data2[$ctr]['NIK']                 = $q['employee_code'];
            // }
            // if(isset($q['fname'])){
                $data2[$ctr]['Nama Karyawan']       = $q['fname'].' '.$q['lname'];
                
            // }
            // if(isset($q['gender'])){
                if($q['gender'] ==  'male'){
                    $data2[$ctr]['Jenis Kelamin']       = 'Laki-laki';
                } else if($q['gender'] ==  'female'){
                    $data2[$ctr]['Jenis Kelamin']       = 'Perempuan';
                }
            // }
            // if(isset($q['no_ktp'])){
                $data2[$ctr]['No. KTP']             = $q['no_ktp'];
                
            // }
            // if(isset($q['birth_place'])){
                $data2[$ctr]['Tempat Lahir']        = $q['birth_place'];
                
            // }
            // if(isset($q['dob'])){
                $data2[$ctr]['Tanggal Lahir']       = $q['dob'];
                
            // }
            // if(isset($q['skck'])){
                $data2[$ctr]['No. SKCK']            = $q['skck'];
                
            // }
            // if(isset($q['per_address'])){
                $data2[$ctr]['Alamat Asal']         = $q['per_address'];
                
            // }
            // if(isset($q['per_city'])){
                $data2[$ctr]['Kota Asal']           = $q['per_city'];
                
            // }
            // if(isset($q['per_district'])){
                $data2[$ctr]['Kecamatan Asal']      = $q['per_district'];
                
            // }
            // if(isset($q['pre_address'])){
                $data2[$ctr]['Alamat Domisili']     = $q['pre_address'];
                
            // }
            // if(isset($q['pre_city'])){
                $data2[$ctr]['Kota Domisili']       = $q['pre_city'];
                
            // }
            // if(isset($q['pre_district'])){
                $data2[$ctr]['Kecamatan Domisili']  = $q['pre_district'];
                
            // }
            // if(isset($q['phone'])){
                $data2[$ctr]['Nomor Telepon']       = $q['phone'];
                
            // }
            // if(isset($q['email'])){
                $data2[$ctr]['Email']               = $q['email'];
            
            // }
            // if(isset($q['religion'])){
                $data2[$ctr]['Agama']               = $q['religion'];
                
            // }
            // if(isset($q['last_education'])){
                $le=LastEducation::find($q['last_education']);
                if($le){
                $data2[$ctr]['Pendidikan Terakhir'] = $le->education;}
                else{  $data2[$ctr]['Pendidikan Terakhir'] = "-";}
                
            // }
            // if(isset($q['project'])){
                $le=Project::find($q['project']);
                if($le){
                $data2[$ctr]['Proyek'] = $le->project;}
                else{  $data2[$ctr]['Proyek'] = "-";}
                
            // }
            // if(isset($q['university'])){
                $data2[$ctr]['Lembaga Institusi']   = $q['university'];
                
            // }
            // if(isset($q['marital_status'])){
                $data2[$ctr]['Status Perkawinan']   = $q['marital_status'];
                
            // }
            // if(isset($q['child'])){
                $data2[$ctr]['Jumlah Tanggungan']   = $q['child'];
                $query_anak = EmployeeDependents::where('emp_id','=', $q['id'])->get()->toArray();
                
                if(isset($query_anak[0]['child_name'])){
                    $data2[$ctr]['Anak 1']   = $query_anak[0]['child_name'];
                
                } else {
                    $data2[$ctr]['Anak 1']   = '';
                }
                if(isset($query_anak[1]['child_name'])){
                    $data2[$ctr]['Anak 2']   = $query_anak[1]['child_name'];
                
                } else {
                    $data2[$ctr]['Anak 2']   = '';
                }
                if(isset($query_anak[2]['child_name'])){
                    $data2[$ctr]['Anak 3']   = $query_anak[2]['child_name'];
                
                } else {
                    $data2[$ctr]['Anak 3']   = '';
                }
            // }
            // if(isset($q['spouse'])){
                $data2[$ctr]['Nama Suami Atau Istri']    = $q['spouse'];
                
            // }
            // if(isset($q['designation'])){
                $des=Designation::find($q['designation']);
                if($des){
                $data2[$ctr]['Jabatan/Posisi']      = $des->designation;}
                else{  $data2[$ctr]['Jabatan/Posisi']      = "-"; }
                
            // }
            // if(isset($q['doj'])){
                $data2[$ctr]['Tanggal Masuk']       = $q['doj'];
                
            // }
            // if(isset($q['location'])){
                $data2[$ctr]['Lokasi']              = $q['location'];
                
            // }
            // if(isset($q['employee_status'])){
                $data2[$ctr]['Status Karyawan']     = $q['employee_status'];
                
            // }
           $ctr++;
       }
            
        $type = 'xls';
        return Excel::create('Employee Active', function($excel) use ($data2) {
            $excel->sheet('mySheet', function($sheet) use ($data2)
            {
                $sheet->fromArray($data2);
            });
        })->download($type);
    }
    public function employeeResign(Request $request)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 104)->first();
        $period_from = '0';
        $period_to = '0';
        if(input::get('period_from')!=''){
            $period_from = Input::get('period_from');
            $period_from = get_date_format_inggris($period_from);
        }
        
        if(input::get('period_to')!=''){
            $period_to = Input::get('period_to');
            $period_to = get_date_format_inggris($period_to);
        }

        $employee = EmployeeResign::where('no_ktp','!=','0');

        if($period_from!='0'){
            $employee->where('dol','>=',$period_from);
        }
        if($period_to!='0'){
            $employee->where('dol','<=',$period_to);
        }
        $employees = $employee->get();
        return view('admin.report.employee-resign',compact('employees','period_from','period_to','permcheck'));
    }
    public function downloadEmployeeResign($a,$b){
        $period_from=$a;
        $period_to=$b;
        
        $employee = EmployeeResign::where('no_ktp','!=','0');
        if($period_from!='0'){
            $period_from = get_date_format_inggris($period_from);
            $employee->where('dol','>=',$period_from);
        }
        if($period_to!='0'){
            $period_to = get_date_format_inggris($period_to);
            $employee->where('dol','<=',$period_to);
        }
        $employees = $employee->get();

        $data2 = array();
        $ctr = 0;
        foreach($employees as $q){
            if(isset($q->employee_info)){
                $data2[$ctr]['NIK'] = $q->employee_info->employee_code;
            }
            if(isset($q->employee_info)){
                $data2[$ctr]['No. KTP'] = $q->employee_info->no_ktp;
            }
            if(isset($q->employee_info)){
                $data2[$ctr][language_data('Name')] = $q->employee_info->fname;
            }
            if(isset($q->company_name)){
                $data2[$ctr][language_data('Company')] = $q->company_name->company;
            }
            if(isset($q->employee_info->designation_name)){
                $data2[$ctr][language_data('Designation')] = $q->employee_info->designation_name->designation;
            }
            if(isset($q->dol)){
                $data2[$ctr][language_data('Date of Join')] = get_date_format($q->employee_info->doj);
            }
            if(isset($q->dol)){
                $data2[$ctr][language_data('Date of Leave')] = get_date_format($q->dol);
            }
           $ctr++;
       }
            
        $type = 'xls';
        return Excel::create('Employee Resign', function($excel) use ($data2) {
            $excel->sheet('mySheet', function($sheet) use ($data2)
            {
                $sheet->fromArray($data2);
            });
        })->download($type);
    }
    public function employeeAccepted(Request $request)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 105)->first();
        $period_from = '0';
        $period_to = '0';
        if(input::get('period_from')!=''){
            $period_from = Input::get('period_from');
            $period_from = get_date_format_inggris($period_from);
        }
        
        if(input::get('period_to')!=''){
            $period_to = Input::get('period_to');
            $period_to = get_date_format_inggris($period_to);
        }


        $employee = Employee::where('status','=','active')->where('role_id','!=','1');
        // $employee->where('employee_type','=','Outsourcing');

        if($period_from!='0'){
            $employee->where('doj','>=',$period_from);
        }
        if($period_to!='0'){
            $employee->where('doj','<=',$period_to);
        }
        $employees = $employee->get();
        return view('admin.report.employee-accepted',compact('employees','period_from','period_to','permcheck'));
    }
    public function downloadEmployeeAccepted($a,$b){
        $period_from=$a;
        $period_to=$b;
        
        $employee = Employee::where('status','=','active')->where('role_id','!=','1');
        // $employee->where('employee_type','=','Outsourcing');
        if($period_from!='0'){
            $period_from = get_date_format_inggris($period_from);
            $employee->where('doj','>=',$period_from);
        }
        if($period_to!='0'){
            $period_to = get_date_format_inggris($period_to);
            $employee->where('doj','<=',$period_to);
        }
        $employees = $employee->get()->toArray();

        $data2 = array();
        $ctr = 0;
        foreach($employees as $q){
            // if(isset($q['employee_code'])){
                $data2[$ctr]['NIK']                 = $q['employee_code'];
            // }
            // if(isset($q['fname'])){
                $data2[$ctr]['Nama Karyawan']       = $q['fname'].' '.$q['lname'];
                
            // }
            // if(isset($q['gender'])){
                if($q['gender'] ==  'male'){
                    $data2[$ctr]['Jenis Kelamin']       = 'Laki-laki';
                } else if($q['gender'] ==  'female'){
                    $data2[$ctr]['Jenis Kelamin']       = 'Perempuan';
                }
            // }
            // if(isset($q['no_ktp'])){
                $data2[$ctr]['No. KTP']             = $q['no_ktp'];
                
            // }
            // if(isset($q['birth_place'])){
                $data2[$ctr]['Tempat Lahir']        = $q['birth_place'];
                
            // }
            // if(isset($q['dob'])){
                $data2[$ctr]['Tanggal Lahir']       = $q['dob'];
                
            // }
            // if(isset($q['skck'])){
                $data2[$ctr]['No. SKCK']            = $q['skck'];
                
            // }
            // if(isset($q['per_address'])){
                $data2[$ctr]['Alamat Asal']         = $q['per_address'];
                
            // }
            // if(isset($q['per_city'])){
                $data2[$ctr]['Kota Asal']           = $q['per_city'];
                
            // }
            // if(isset($q['per_district'])){
                $data2[$ctr]['Kecamatan Asal']      = $q['per_district'];
                
            // }
            // if(isset($q['pre_address'])){
                $data2[$ctr]['Alamat Domisili']     = $q['pre_address'];
                
            // }
            // if(isset($q['pre_city'])){
                $data2[$ctr]['Kota Domisili']       = $q['pre_city'];
                
            // }
            // if(isset($q['pre_district'])){
                $data2[$ctr]['Kecamatan Domisili']  = $q['pre_district'];
                
            // }
            // if(isset($q['phone'])){
                $data2[$ctr]['Nomor Telepon']       = $q['phone'];
                
            // }
            // if(isset($q['email'])){
                $data2[$ctr]['Email']               = $q['email'];
            
            // }
            // if(isset($q['religion'])){
                $data2[$ctr]['Agama']               = $q['religion'];
                
            // }
            // if(isset($q['last_education'])){
                $le=LastEducation::find($q['last_education']);
                if($le){
                $data2[$ctr]['Pendidikan Terakhir'] = $le->education;}
                else{  $data2[$ctr]['Pendidikan Terakhir'] = "-";}
                
            // }
            // if(isset($q['project'])){
                $le=Project::find($q['project']);
                if($le){
                $data2[$ctr]['Proyek'] = $le->project;}
                else{  $data2[$ctr]['Proyek'] = "-";}
                
            // }
            // if(isset($q['company'])){
                $le=Company::find($q['company']);
                if($le){
                $data2[$ctr]['Perusahaan'] = $le->company;}
                else{  $data2[$ctr]['Perusahaan'] = "-";}
                
            // }
            // if(isset($q['university'])){
                $data2[$ctr]['Lembaga Institusi']   = $q['university'];
                
            // }
            // if(isset($q['marital_status'])){
                $data2[$ctr]['Status Perkawinan']   = $q['marital_status'];
                
            // }
            // if(isset($q['designation'])){
                $des=Designation::find($q['designation']);
                if($des){
                $data2[$ctr]['Jabatan/Posisi']      = $des->designation;}
                else{  $data2[$ctr]['Jabatan/Posisi']      = "-"; }
                
            // }
            // if(isset($q['doj'])){
                $data2[$ctr]['Tanggal Masuk']       = $q['doj'];
                
            // }
            // if(isset($q['location'])){
                $data2[$ctr]['Lokasi']              = $q['location'];
                
            // }
           $ctr++;
       }
            
        $type = 'xls';
        return Excel::create('Employee Accepted', function($excel) use ($data2) {
            $excel->sheet('mySheet', function($sheet) use ($data2)
            {
                $sheet->fromArray($data2);
            });
        })->download($type);
    }
    public function attendancesSummery(Request $request)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 86)->first();
        $thn=input::get('tahun');
        $project = Input::get('share_with_specific');

        $projects=Project::all();
        $leaves=[];
        $ctr=0;
        $curYear = date('Y'); 
        if($thn!=null && $thn!="" && $thn!=" "){
            $curYear=$thn;
        }
        if($project!=null && $project!="" && $project!=" " && $project!=0){
            $whr=array();
            $projectt = explode(",", $project);
            $coun=0;
            foreach($projectt as $p){array_push($whr,$p);
                $emp=Employee::where('project','=',$p)->where('role_id','!=',1)->where('status', 'active')->get();
                $tmptotal=0;
                foreach ($emp as $e){
                    $attemp= Attendance::where('emp_id','=',$e->id)->whereYear('date','=',$curYear) ->count();
                    $tmptotal+=$attemp;
                }
                $tmp=array(
                    'namaproyek'=>Project::find($p)->project,
                    'total'=>$tmptotal
               
                );
                $projectchart[$coun]=$tmp;
         
                $coun++;
            }
           // print_r($project);
            $employees = Employee::where('role_id','!=','1')->where('status', 'active')->whereIn('project',$whr)->get();
        }
        else{
        $employees = Employee::where('role_id','!=','1')->where('status', 'active')->get();
        }

        foreach($employees as $e){
            $tmp=array(
                'jan'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','1')->whereYear('date','=',$curYear) ->count(),
                'feb'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','2')->whereYear('date','=',$curYear)  ->count(),
                'mar'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','3')->whereYear('date','=',$curYear)  ->count(),
                'apr'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','4') ->whereYear('date','=',$curYear) ->count(),
                'may'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','5') ->whereYear('date','=',$curYear) ->count(),
                'jun'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','6') ->whereYear('date','=',$curYear) ->count(),
                'jul'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','7')->whereYear('date','=',$curYear) ->count(),
                'aug'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','8') ->whereYear('date','=',$curYear) ->count(),
                'sep'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','9')->whereYear('date','=',$curYear)  ->count(),
                'oct'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','10') ->whereYear('date','=',$curYear) ->count(),
                'nov'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','11')->whereYear('date','=',$curYear)  ->count(),
                'des'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','12') ->whereYear('date','=',$curYear) ->count()
            );
            $leaves[$ctr]=$tmp;
         
                $ctr++;
        }
      return view('admin.report.attendances-summery', compact('employees','leaves','projects','curYear','project','projectchart','permcheck'));
    }
    public function employeeScoreSummery(Request $request)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 88)->first();
        $designation=input::get('designation');
        $project=input::get('project');
        $whereasses=[];
        $whereemp=[];
        if($project!=null && $project!="" && $project!=" " && $project!=0){
            $whereasses['project']= $project;
        }
        if($designation!=null && $designation!="" && $designation!=" " && $designation!=0){
            $whereemp['designation']= $designation;
        }
        $scores=[];
        $ctr=0;
        $employees = Employee::where('role_id','!=','1')->where('status', 'active')->where($whereemp)->get();
        foreach($employees as $e){
            $penilaian=EmployeeAssessment::where('emp_id','=',$e->id)->where($whereasses)->orderBy('date','asc')->first();
            if($penilaian){
                $tmp=array(
                    'emplo'=>$e->fname.' '.$e->lname.'('.$e->employee_code.')',
                    'tanggal'=>$penilaian->date,
                    'grade'=>$penilaian->grade
                );}
                else{
                    $tmp=array(
                        'emplo'=>$e->fname.' '.$e->lname.'('.$e->employee_code.')',
                        'tanggal'=>'None',
                        'grade'=>'0'
                    );
                }
            $scores[$ctr]=$tmp;
         
            $ctr++;
        }
        $tmpskor=$scores;
        $employees=$employees->toArray();
        $price = array();
        foreach ($tmpskor as $key => $row)
        {
            $price[$key] = $row['grade'];
        }
       array_multisort($price, SORT_DESC, $tmpskor);
        $projects=Project::all();
        $designations=Designation::all();
        return view('admin.report.employee-score-summery', compact('employees','scores','projects','designations','project','designation','tmpskor','permcheck'));
    }
    /* getSalaryStatement  Function Start Here */
    public function getSalaryStatement(Request $request)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 92)->first();
        $v = \Validator::make($request->all(), [
            'date_from' => 'required', 'date_to' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('reports/payroll')->withErrors($v->errors());
        }

        $cmd = Input::get('cmd');
        $date_from = Input::get('date_from');
        $date_to = Input::get('date_to');

        $date_from = date('Y-m-01', strtotime($date_from));
        $date_to = date('Y-m-t', strtotime($date_to));

        $payslip = Payroll::where('emp_id', $cmd)->first();

        if ($payslip==''){
            return redirect('reports/payroll')->with([
                'message'=> language_data('User pay transaction data not found'),
                'message_important'=>true
            ]);
        }

        $payroll = Payroll::where('emp_id', $cmd)->whereBetween('payment_date', [$date_from, $date_to])->get();

        $net_salary = Payroll::where('emp_id', $cmd)->whereBetween('payment_date', [$date_from, $date_to])->sum('net_salary');
        $over_time = Payroll::where('emp_id', $cmd)->whereBetween('payment_date', [$date_from, $date_to])->sum('overtime_salary');
        $tax = Payroll::where('emp_id', $cmd)->whereBetween('payment_date', [$date_from, $date_to])->sum('tax');
        $provident_fund = Payroll::where('emp_id', $cmd)->whereBetween('payment_date', [$date_from, $date_to])->sum('provident_fund');
        $loan = Payroll::where('emp_id', $cmd)->whereBetween('payment_date', [$date_from, $date_to])->sum('loan');
        $total_salary = Payroll::where('emp_id', $cmd)->whereBetween('payment_date', [$date_from, $date_to])->sum('total_salary');


        return view('admin.report.employee-salary-statement', compact('payroll', 'payslip', 'cmd', 'date_from', 'date_to', 'net_salary', 'over_time', 'tax', 'provident_fund', 'loan', 'total_salary','permcheck'));


    }

    /* printSalaryStatement  Function Start Here */
    public function printSalaryStatement($id, $date_from, $date_to)
    {
        $date_from=get_date_format_inggris($date_from);
        $date_to=get_date_format_inggris($date_to);

        $payslip = Payroll::where('emp_id', $id)->first();
        $payroll = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->get();

        $net_salary = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('net_salary');
        $over_time = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('overtime_salary');
        $tax = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('tax');
        $provident_fund = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('provident_fund');
        $loan = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('loan');
        $total_salary = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('total_salary');


        return view('admin.report.print-salary-statement', compact('payroll', 'payslip', 'id', 'date_from', 'date_to', 'net_salary', 'over_time', 'tax', 'provident_fund', 'loan', 'total_salary'));
    }

    /* employeeSummery  Function Start Here */
    public function employeeSummery($id)
    {
        $employee = Employee::find($id);
        $loan = Loan::where('emp_id', $id)->get();
        $provident_fund = ProvidentFund::where('emp_id', $id)->get();
        $award = AwardList::where('emp_id', $id)->get();
        $leave = Leave::where('emp_id', $id)->get();

        return view('admin.report.employee-summery', compact('employee', 'provident_fund', 'loan', 'award', 'leave'));
    }


    /* jobApplicants  Function Start Here */
    public function jobApplicants()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 85)->first();
        $job_applicants = JobApplicants::where('status', '!=', 'Unread')->where('status', '!=', 'Rejected')->get();

        return view('admin.report.job-applicants-summery', compact('job_applicants','permcheck'));
    }

    /* sendEmailApplicant  Function Start Here */
    public function sendEmailApplicant(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'email' => 'required', 'subject' => 'required', 'message' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('reports/job-applicants')->withErrors($v->errors());
        }

        $applicant = JobApplicants::find($request->cmd)->name;
        $email = Input::get('email');
        $subject = Input::get('subject');
        $message = Input::get('message');

        $sysEmail = app_config('Email');
        $sysCompany = app_config('AppName');
        $default_gt = app_config('Gateway');

        if ($default_gt == 'default') {

            $mail = new \PHPMailer();

            $mail->setFrom($sysEmail, $sysCompany);
            $mail->addAddress($email, $applicant);     // Add a recipient
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = $subject;
            $mail->Body = $message;

            if (!$mail->send()) {
                return redirect('reports/job-applicants')->with([
                    'message' => language_data('Please check your email setting'),
                    'message_important' => true
                ]);
            } else {
                return redirect('reports/job-applicants')->with([
                    'message' => language_data('Email send successfully')
                ]);
            }

        } else {
            $host = app_config('SMTPHostName');
            $smtp_username = app_config('SMTPUserName');
            $stmp_password = app_config('SMTPPassword');
            $port = app_config('SMTPPort');
            $secure = app_config('SMTPSecure');


            $mail = new \PHPMailer();

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $host;  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $smtp_username;                 // SMTP username
            $mail->Password = $stmp_password;                           // SMTP password
            $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $port;

            $mail->setFrom($sysEmail, $sysCompany);
            $mail->addAddress($email, $applicant);     // Add a recipient
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = $subject;
            $mail->Body = $message;


            if (!$mail->send()) {
                return redirect('reports/job-applicants')->with([
                    'message' => language_data('Please check your email setting'),
                    'message_important' => true
                ]);
            } else {
                return redirect('reports/job-applicants')->with([
                    'message' => language_data('Email send successfully')
                ]);
            }
        }

    }

    /* sendSMSApplicant  Function Start Here */
    public function sendSMSApplicant(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'phone' => 'required', 'message' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('reports/job-applicants')->withErrors($v->errors());
        }

        $phone = Input::get('phone');
        $message = Input::get('message');
        $sender_id=app_config('AppName');

        $sms_gateway = SMSGateway::where('status', 'Active')->first();
        $gateway_name = $sms_gateway->name;

        if ($gateway_name == 'Twilio') {
            $sid = $sms_gateway->user_name;
            $token = $sms_gateway->password;

            try {

                $client = new Client($sid, $token);
                $response = $client->account->messages->create(
                    "$phone",
                    array(
                        'from' => '+19383333659',
                        'body' => $message
                    )
                );

                if ($response->sid!='') {
                    return redirect('reports/job-applicants')->with([
                        'message' => language_data('SMS sent successfully')
                    ]);
                } else {
                    return redirect('reports/job-applicants')->with([
                        'message' => language_data('Please check your Twilio Credentials'),
                        'message_important'=>true
                    ]);
                }
            } catch (Exception $e) {
                return redirect('reports/job-applicants')->with([
                    'message' => language_data('Please check your Twilio Credentials'),
                    'message_important'=>true
                ]);
            }


        } elseif ($gateway_name == 'Route SMS') {

            $sms_url=$sms_gateway->api_link;
            $user=$sms_gateway->user_name;
            $password=$sms_gateway->password;

            $sms_sent_to_user = "$sms_url" . "?type=0" . "&username=$user" . "&password=$password" ."&destination=$phone" . "&source=$sender_id" . "&message=$message" . "&dlr=1";
            $get_sms_status=file_get_contents($sms_sent_to_user);
            $get_sms_status = str_replace("1701", language_data('Success'), $get_sms_status);
            $get_sms_status = str_replace("1709", language_data('User Validation Failed'), $get_sms_status);
            $get_sms_status = str_replace("1025", language_data('Insufficient Credit'), $get_sms_status);
            $get_sms_status = str_replace("1710", language_data('Internal Error'), $get_sms_status);
            $get_sms_status = str_replace("1706", language_data('Invalid receiver'), $get_sms_status);
            $get_sms_status = str_replace("1705", language_data('Invalid SMS'), $get_sms_status);
            $get_sms_status = str_replace("1707", language_data('Invalid sender'), $get_sms_status);
            $get_sms_status = str_replace(",", " ", $get_sms_status);
            $pos = strpos($get_sms_status, language_data('Success'));

            if ($pos === false) {
                return redirect('reports/job-applicants')->with([
                    'message' => language_data('SMS sent successfully')
                ]);
            } else {
                return redirect('reports/job-applicants')->with([
                    'message' => $get_sms_status,
                    'message_important'=>true
                ]);
            }



        } elseif ($gateway_name == 'Bulk SMS') {

            $sms_url=$sms_gateway->api_link;
            $user=$sms_gateway->user_name;
            $password=$sms_gateway->password;


            $url = "$sms_url" . "/eapi/submission/send_sms/2/2.0?username=$user" . "&password=$password" ."&msisdn=$phone" ."&message=".urlencode($message);


            $ret = file_get_contents($url);

            $send = explode("|",$ret);

            if ($send[0]=='0') {
                $get_sms_status= language_data('In progress');
            }elseif($send[0]=='1'){
                $get_sms_status= language_data('Scheduled');
            }elseif($send[0]=='22'){
                $get_sms_status= language_data('Internal Error');
            }elseif($send[0]=='23'){
                $get_sms_status= language_data('Authentication failure');
            }elseif($send[0]=='24'){
                $get_sms_status= language_data('Data validation failed');
            }elseif($send[0]=='25'){
                $get_sms_status= language_data('Insufficient Credit');
            }elseif($send[0]=='26'){
                $get_sms_status= language_data('Upstream credits not available');
            }elseif($send[0]=='27'){
                $get_sms_status= language_data('You have exceeded your daily quota');
            }elseif($send[0]=='28'){
                $get_sms_status= language_data('Upstream quota exceeded');
            }elseif($send[0]=='40'){
                $get_sms_status= language_data('Temporarily unavailable');
            }elseif($send[0]=='201'){
                $get_sms_status= language_data('Maximum batch size exceeded');
            }elseif($send[0]=='200'){
                $get_sms_status= language_data('Success');
            }
            else{
                $get_sms_status= language_data('Failed');
            }

            if($get_sms_status=='Success'){
                return redirect('reports/job-applicants')->with([
                    'message' => language_data('SMS sent successfully')
                ]);
            }else{
                return redirect('reports/job-applicants')->with([
                    'message' => $get_sms_status,
                    'message_important'=>true
                ]);
            }

        } else {
            return redirect('reports/job-applicants')->with([
                'message' => language_data('Gateway information not found'),
                'message_important' => true
            ]);
        }

    }


    /* sendSMSSalaryStatement  Function Start Here */
    public function sendSMSSalaryStatement(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'phone' => 'required', 'message' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('reports/payroll')->withErrors($v->errors());
        }

        $phone = Input::get('phone');
        $message = Input::get('message');

        $sms_gateway = SMSGateway::where('status', 'Active')->first();
        $gateway_name = $sms_gateway->name;

        if ($gateway_name == 'Twilio') {
            $sid = $sms_gateway->user_name;
            $token = $sms_gateway->password;

            try {

                $client = new Client($sid, $token);
                $response = $client->account->messages->create(
                    "$phone",
                    array(
                        'from' => '+15005550006',
                        'body' => $message
                    )
                );

                if ($response->sid!='') {
                    return redirect('reports/payroll')->with([
                        'message' => language_data('SMS sent successfully')
                    ]);
                } else {
                    return redirect('reports/payroll')->with([
                        'message' => language_data('Please check your Twilio Credentials'),
                        'message_important'=>true
                    ]);
                }
            } catch (Exception $e) {
                return redirect('reports/payroll')->with([
                    'message' => language_data('Please check your Twilio Credentials'),
                    'message_important'=>true
                ]);
            }


        } elseif ($gateway_name == 'Route SMS') {

            $sms_url=$sms_gateway->api_link;
            $user=$sms_gateway->user_name;
            $password=$sms_gateway->password;
            $sender_id=app_config('AppName');


            $sms_sent_to_user = "$sms_url" . "?type=0" . "&username=$user" . "&password=$password" ."&destination=$phone" . "&source=$sender_id" . "&message=$message" . "&dlr=1";
            $get_sms_status=file_get_contents($sms_sent_to_user);
            $get_sms_status = str_replace("1701", language_data('Success'), $get_sms_status);
            $get_sms_status = str_replace("1709", language_data('User Validation Failed'), $get_sms_status);
            $get_sms_status = str_replace("1025", language_data('Insufficient Credit'), $get_sms_status);
            $get_sms_status = str_replace("1710", language_data('Internal Error'), $get_sms_status);
            $get_sms_status = str_replace("1706", language_data('Invalid receiver'), $get_sms_status);
            $get_sms_status = str_replace("1705", language_data('Invalid SMS'), $get_sms_status);
            $get_sms_status = str_replace("1707", language_data('Invalid sender'), $get_sms_status);
            $get_sms_status = str_replace(",", " ", $get_sms_status);
            $pos = strpos($get_sms_status, language_data('Success'));

            if ($pos === false) {
                return redirect('reports/payroll')->with([
                    'message' => language_data('SMS sent successfully')
                ]);
            } else {
                return redirect('reports/payroll')->with([
                    'message' => $get_sms_status,
                    'message_important'=>true
                ]);
            }



        } elseif ($gateway_name == 'Bulk SMS') {

            $sms_url=$sms_gateway->api_link;
            $user=$sms_gateway->user_name;
            $password=$sms_gateway->password;

            $url = "$sms_url" . "/eapi/submission/send_sms/2/2.0?username=$user" . "&password=$password" ."&msisdn=$phone" ."&message=".urlencode($message);
            $ret = file_get_contents($url);

            $send = explode("|",$ret);


            if ($send[0]=='0') {
                $get_sms_status= language_data('In progress');
            }elseif($send[0]=='1'){
                $get_sms_status= language_data('Scheduled');
            }elseif($send[0]=='22'){
                $get_sms_status= language_data('Internal Error');
            }elseif($send[0]=='23'){
                $get_sms_status= language_data('Authentication failure');
            }elseif($send[0]=='24'){
                $get_sms_status= language_data('Data validation failed');
            }elseif($send[0]=='25'){
                $get_sms_status= language_data('Insufficient Credit');
            }elseif($send[0]=='26'){
                $get_sms_status= language_data('Upstream credits not available');
            }elseif($send[0]=='27'){
                $get_sms_status= language_data('You have exceeded your daily quota');
            }elseif($send[0]=='28'){
                $get_sms_status= language_data('Upstream quota exceeded');
            }elseif($send[0]=='40'){
                $get_sms_status= language_data('Temporarily unavailable');
            }elseif($send[0]=='201'){
                $get_sms_status= language_data('Maximum batch size exceeded');
            }elseif($send[0]=='200'){
                $get_sms_status= language_data('Success');
            }
            else{
                $get_sms_status= language_data('Failed');
            }

            if($get_sms_status=='Success'){
                return redirect('reports/payroll')->with([
                    'message' => language_data('SMS sent successfully')
                ]);
            }else{
                return redirect('reports/payroll')->with([
                    'message' => $get_sms_status,
                    'message_important'=>true
                ]);
            }

        } else {
            return redirect('reports/payroll')->with([
                'message' => language_data('Gateway information not found'),
                'message_important' => true
            ]);
        }

    }


    /* sendEmailSalaryStatement  Function Start Here */
    public function sendEmailSalaryStatement(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'email' => 'required', 'subject' => 'required', 'message' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('reports/payroll')->withErrors($v->errors());
        }

        $employee = Employee::find($request->cmd)->fname.' '.Employee::find($request->cmd)->lname;
        $email = Input::get('email');
        $subject = Input::get('subject');
        $message = Input::get('message');

        $sysEmail = app_config('Email');
        $sysCompany = app_config('AppName');
        $default_gt = app_config('Gateway');

        if ($default_gt == 'default') {

            $mail = new \PHPMailer();

            $mail->setFrom($sysEmail, $sysCompany);
            $mail->addAddress($email, $employee);     // Add a recipient
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = $subject;
            $mail->Body = $message;

            if (!$mail->send()) {
                return redirect('reports/payroll')->with([
                    'message' => language_data('Please check your email setting'),
                    'message_important' => true
                ]);
            } else {
                return redirect('reports/payroll')->with([
                    'message' => language_data('Email send successfully')
                ]);
            }

        } else {
            $host = app_config('SMTPHostName');
            $smtp_username = app_config('SMTPUserName');
            $stmp_password = app_config('SMTPPassword');
            $port = app_config('SMTPPort');
            $secure = app_config('SMTPSecure');


            $mail = new \PHPMailer();

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $host;  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $smtp_username;                 // SMTP username
            $mail->Password = $stmp_password;                           // SMTP password
            $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $port;

            $mail->setFrom($sysEmail, $sysCompany);
            $mail->addAddress($email, $employee);     // Add a recipient
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = $subject;
            $mail->Body = $message;


            if (!$mail->send()) {
                return redirect('reports/payroll')->with([
                    'message' => language_data('Please check your email setting'),
                    'message_important' => true
                ]);
            } else {
                return redirect('reports/payroll')->with([
                    'message' => language_data('Email send successfully')
                ]);
            }
        }

    }
    public function hitungkonversi($a,$hari){
        $angka = array(1, 8, 1,0);
        $pengkali= array(1.5,2,3,4);
        if($hari=='Saturday' || $hari=='Sunday'){
            $angka = array(8,1,0);
            $pengkali= array(2,3,4);
         
        }
      
        $i = 0;
        $res=0;
        if($a>0){
       
                while ($i <sizeof($angka) && $a>0) {
                    
                    $tmp= (float)($a - $angka[$i]);
               
        if ($tmp >0) {
            if($angka[$i]==0){
                $angka[$i]=(float)$a;
            }
            $res+=(float)(round($pengkali[$i]*$angka[$i],2));
          
            $a = (float)($a - $angka[$i]);
          
        }else{
            $res+=(float)(round($pengkali[$i]*$a,2));
            $a = (float)($a - $angka[$i]);
            
        }
    
      
       
        $i++;
            }
    
        }
        else{
            $res=0;
        }
        return $res;
}
public function downloadPdfDataKaryawan($a,$b,$c,$d,$e,$f,$g,$h,$i,$j,$k,$l,$m,$n,$o){
    $gendermale=$a;
    $genderfemale=$b;
    $sma=$c;
    $d3=$d;
    $s1=$e;
    $s2=$f;
    $s05=$g;
    $s510=$h;
    $s1015=$i;
    $s15=$j;
    $u120=$k;
    $u2130=$l;
    $u3150=$m;
    $u51=$n;
    if($o!=0){
        $project=Project::find($o);
        $project=$project->porject;
    }
    else{
        $project='';
    }
        
  return view('admin.report.print-employee-data-summery',compact('genderfemale','gendermale', 'sma','d3','s1','s2','s05','s510','s1015','s15','u120','u2130','u3150','u51','project'));
  
 // return $pdf->stream('Upah_Lembur.pdf');
  }

  public function downloadPdfLeaves(Request $request){
    $project=input::get('projecte');
    $tahun=input::get('tahune');
    $jenis=input::get('jenise');
    $projectt=array();
    $leavetypee=array();
    if($project!=null && $project!=''){
    $projectt = explode(",", $project);}

    if($leavetypee!=null && $leavetypee!=''){
    $leavetypee = explode(",", $jenis);}
    $projectchart=array();
    $leavechart=array();
    if((sizeof($projectt)>0 && $projectt[0]!='' && $projectt[0]!=null) || (sizeof($leavetypee)>0 && $leavetypee[0]!=''&& $leavetypee[0]!=null)){
        $coun=0;
        if(sizeof($projectt)>0 && $projectt[0]!=''&& $projectt[0]!=null){
        foreach($projectt as $p){
            $emp=Employee::where('project','=',$p)->where('role_id','!=',1)->where('status', 'active')->get();

            $tmptotal=0;
            foreach ($emp as $e){
                if((sizeof($leavetypee)>0 && $leavetypee[0]!=''&& $leavetypee[0]!=null)){
                $attemp= Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereYear('leave_from','=',$tahun)->sum(DB::raw('DATEDIFF(leave_to, leave_from)'));
                }
                else{
                    $attemp= Leave::where('emp_id','=',$e->id)->whereYear('leave_from','=',$tahun)->sum(DB::raw('DATEDIFF(leave_to, leave_from)'));

                }
                $tmptotal+=$attemp;
            }
            $tmp=array(
                'namaproyek'=>Project::find($p)->project,
                'total'=>$tmptotal
           
            );
            $projectchart[$coun]=$tmp;
     
            $coun++;
        }
    }
        $coun=0;
        if(sizeof($leavetypee)>0 && $leavetypee[0]!=''&& $leavetypee[0]!=null){
        foreach($leavetypee as $p){
            if((sizeof($projectt)>0 && $projectt[0]!=''&& $projectt[0]!=null)){
                $emp=Employee::whereIn('project',$projectt)->where('role_id','!=','1')->where('status', 'active')->get();
            }else{
            $emp=Employee::where('role_id','!=','1')->where('status', 'active')->get();
        }

            $tmptotal=0;
            foreach ($emp as $e){
                $attemp= Leave::where('emp_id','=',$e->id)->whereYear('leave_from','=',$tahun)->sum(DB::raw('DATEDIFF(leave_to, leave_from)'));
                $tmptotal+=$attemp;
            }
            $tmp=array(
                'namaleave'=>LeaveType::find($p)->leave,
                'total'=>$tmptotal
           
            );
            $leavechart[$coun]=$tmp;
     
            $coun++;
        }
    }
    if((sizeof($projectt)>0 && $projectt[0]!=''&& $projectt[0]!=null)){
        $employees = Employee::where('role_id','!=','1')->where('status', 'active')->whereIn('project',$projectt)->get();
        }
        else{
            $employees = Employee::where('role_id','!=','1')->where('status', 'active')->get();
        }
    }
    else{
    $employees = Employee::where('role_id','!=','1')->where('status', 'active')->get();
    }
    $leaves=array();
    foreach($employees as $e){
        if((sizeof($leavetypee)>0 && $leavetypee[0]!=''&& $leavetypee[0]!=null)){
            $tmp=array(
                'jan'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','1')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'feb'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','2') ->whereYear('leave_from','=',$tahun)->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'mar'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','3')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'apr'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','4')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'may'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','5')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'jun'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','6')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'jul'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','7')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'aug'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','8')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'sep'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','9')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'oct'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','10')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'nov'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','11')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
                'des'=>Leave::where('emp_id','=',$e->id)->whereIn('ltype_id',$leavetypee)->whereMonth('leave_from','=','12') ->whereYear('leave_from','=',$tahun)->sum(DB::raw('DATEDIFF(leave_to, leave_from)'))
            );
        }
        else{
        $tmp=array(
            'jan'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','1')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
            'feb'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','2') ->whereYear('leave_from','=',$tahun)->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
            'mar'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','3')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
            'apr'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','4')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
            'may'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','5')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
            'jun'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','6')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
            'jul'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','7')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
            'aug'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','8')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
            'sep'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','9')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
            'oct'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','10')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
            'nov'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','11')->whereYear('leave_from','=',$tahun) ->sum(DB::raw('DATEDIFF(leave_to, leave_from)')),
            'des'=>Leave::where('emp_id','=',$e->id)->whereMonth('leave_from','=','12') ->whereYear('leave_from','=',$tahun)->sum(DB::raw('DATEDIFF(leave_to, leave_from)'))
        );
    }
        $leaves[$ctr]=$tmp;
     
            $ctr++;
    }
    $pdf = WkPdf::loadView('admin.report.print-leaves-summery', compact('employees','leaves','tahun','project','projectchart','jenis','leavechart'));
        
    return $pdf->stream('Laporan_Kehadiran-'.$tahun.'pdf');
  }
  public function downloadPdfAttendance(Request $request){
    $project=input::get('projecte');
    $tahun=input::get('tahune');
    $projectchart=array();
    if($project!=null && $project!="" && $project!=" " && $project!=0){
        $whr=array();
        $projectt = explode(",", $project);
        $coun=0;
        foreach($projectt as $p){array_push($whr,$p);
            $emp=Employee::where('project','=',$p)->where('role_id','!=',1)->where('status', 'active')->get();
            $tmptotal=0;
            foreach ($emp as $e){
                $attemp= Attendance::where('emp_id','=',$e->id)->whereYear('date','=',$tahun) ->count();
                $tmptotal+=$attemp;
            }
            $tmp=array(
                'namaproyek'=>Project::find($p)->project,
                'total'=>$tmptotal
           
            );
            $projectchart[$coun]=$tmp;
     
            $coun++;
        }
       // print_r($project);
        $employees = Employee::where('role_id','!=',1)->where('status', 'active')->whereIn('project',$whr)->get();
    }
    else{
    $employees = Employee::where('role_id','!=',1)->where('status', 'active')->get();
    }
    $leaves=[];
    $ctr=0;
    foreach($employees as $e){
        $tmp=array(
            'jan'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','1')->whereYear('date','=',$tahun) ->count(),
            'feb'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','2')->whereYear('date','=',$tahun)  ->count(),
            'mar'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','3')->whereYear('date','=',$tahun)  ->count(),
            'apr'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','4') ->whereYear('date','=',$tahun) ->count(),
            'may'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','5') ->whereYear('date','=',$tahun) ->count(),
            'jun'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','6') ->whereYear('date','=',$tahun) ->count(),
            'jul'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','7')->whereYear('date','=',$tahun) ->count(),
            'aug'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','8') ->whereYear('date','=',$tahun) ->count(),
            'sep'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','9')->whereYear('date','=',$tahun)  ->count(),
            'oct'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','10') ->whereYear('date','=',$tahun) ->count(),
            'nov'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','11')->whereYear('date','=',$tahun)  ->count(),
            'des'=>Attendance::where('emp_id','=',$e->id)->whereMonth('date','=','12') ->whereYear('date','=',$tahun) ->count()
        );
        $leaves[$ctr]=$tmp;
     
            $ctr++;
    }
  $pdf = WkPdf::loadView('admin.report.print-attendances-summery', compact('employees','tahun','projectchart','leaves','project'));
        
  return $pdf->stream('Laporan_Kehadiran-'.$tahun.'pdf');
    
}
public function UpahLembur(Request $request)
{
 
    $role_id = \Auth::user()->role_id;
    $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 93)->first();
    $employee_id = \Auth::user()->id;
$curYear= date('Y'); 
$curMonth= date('M');
$year=input::get("year");
$month=input::get("month");
$employeee=input::get("employee");
$company=input::get("company");
$tmpmonth="0";
$dataemployee=[];
$tableemployee=[];

if($year==null ||$year=="" || $year=="0"){
$year=$curYear;
}

if($month==null ||$month=="" || $month=="0"){
$month= $curMonth;
}
if($month == 'Januari'){
    $month = 'January';
} if($month == 'Februari'){
    $month = 'February';
} if($month == 'Maret'){
    $month = 'March';
} if($month == 'April'){
    $month = 'April';
} if($month == 'Mei'){
    $month = 'May';
} if($month == 'Juni'){
    $month = 'June';
} if($month == 'Juli'){
    $month = 'July';
} if($month == 'Agustus'){
    $month = 'August';
} if($month == 'September'){
    $month = 'September';
} if($month == 'Oktober'){
    $month = 'October';
} if($month == 'November'){
    $month = 'November';
} if($month == 'Desember'){
    $month = 'December';
}

$angkabulan=date_parse($month);
$daterange=$year.'-'.$angkabulan['month'].'-1';

if($employeee!=null &&$employeee!="" && $employeee!="0"){


$dataemployee=Employee::find($employeee);
$tableemployee=OvertimeWarrant::where('emp_id','=',$employeee)->whereRaw("status = 'accepted' AND ((MONTH(date_from) = ".$angkabulan['month']." AND YEAR(date_from) =".$year." ) OR  (MONTH(date_to) = ".$angkabulan['month']." AND YEAR(date_to) =".$year." )) ")->get();
if(count($tableemployee)){
$tmptableemploye=$tableemployee;
$tableemployee=[];
$ctr=0;
if($dataemployee->salary==null || $dataemployee->salary==''){
    $gajipokok=0;
}
else{
    $gajipokok=(float)(round($dataemployee->salary/173,2));
}
foreach ($tmptableemploye as $t){
    $begins = date('Y-m-d',strtotime($t->date_from));
    $ends = date('Y-m-d',strtotime($t->date_to. ' +1 day'));
$begin = new DateTime($begins);
$end = new DateTime($ends);


$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);


foreach($daterange as $date){
    $pagi1=strtotime('05:00:00');
    $pagi2=strtotime('07:00:00');

    $siang1=strtotime('11:30:00');
    $siang2=strtotime('21:00:00');

    $time1 = strtotime($t->overtime_from);
    $time2 = strtotime($t->overtime_to);
    $difference = round(abs($time2 - $time1) / 3600,2);
    $konversi=self::hitungkonversi($difference,$date->format("l"));
    $tarif=(float)(round($gajipokok*$konversi,2));
    $uangmakanpagi=0;
    $uangmakansiang=0;
    if ($time1 >= $pagi1  && $time2<= $pagi2)
            {
               $uangmakanpagi=10000;
            }
           if ($time1 >=$siang1 && $time2<= $siang2)
            {
               $uangmakansiang=15000;
            }
        
    $tmp= array(
        'hari'=>$date->format("l"),
        'tanggal'=>$date->format("Y-m-d"),
        'dari'=>$t->overtime_from,
        'sampai'=>$t->overtime_to,
        'total_jam'=>$difference,
        'konversi'=>$konversi,
        'makan_pagi'=>$uangmakanpagi,
        'makan_siang'=>$uangmakansiang,
        'tarif'=>$tarif,
        'perjam'=>$gajipokok

    );
    $tableemployee[$ctr]=$tmp;
    $ctr++;
}
      
}
}

}



$daterange=$year.'-'.$angkabulan['month'].'-1';
$employee=Employee::whereRaw("status = 'active'")->whereRaw("employee_type = 'Internal'")->whereIn('id', function($query) use ($angkabulan,$year)
{
$query->select('emp_id')
      ->from('sys_overtime_warrant')
      ->whereRaw("status = 'accepted' AND ((MONTH(date_from) = ".$angkabulan['month']." AND YEAR(date_from) =".$year." ) OR  (MONTH(date_to) = ".$angkabulan['month']." AND YEAR(date_to) =".$year." )) ");
})->groupby('id')->distinct()->get();


if($month == 'January'){
    $month = 'Januari';
} if($month == 'February'){
    $month = 'Februari';
} if($month == 'March'){
    $month = 'Maret';
} if($month == 'April'){
    $month = 'April';
} if($month == 'May'){
    $month = 'Mei';
} if($month == 'Juni'){
    $month = 'June';
} if($month == 'July'){
    $month = 'Juli';
} if($month == 'August'){
    $month = 'Agustus';
} if($month == 'September'){
    $month = 'September';
} if($month == 'October'){
    $month = 'Oktober';
} if($month == 'November'){
    $month = 'November';
} if($month == 'December'){
    $month = 'Desember';
}

return view('admin.report.upah-lembur',compact('employee','month','year','employeee','dataemployee','tableemployee','role_id','employee_id','permcheck'));
}
public function PrintUpahLembur($employeee,$bulan,$year)
{

$angkabulan=date_parse($bulan);
$daterange=$year.'-'.$angkabulan['month'].'-1';

$dataemployee=[];
$tableemployee=[];
if($employeee!=null &&$employeee!="" && $employeee!="0"){


$dataemployee=Employee::find($employeee);
$tableemployee=OvertimeWarrant::where('emp_id','=',$employeee)->whereRaw("status = 'accepted' AND ((MONTH(date_from) = ".$angkabulan['month']." AND YEAR(date_from) =".$year." ) OR  (MONTH(date_to) = ".$angkabulan['month']." AND YEAR(date_to) =".$year." )) ")->get();

if(count($tableemployee)){
    $tmptableemploye=$tableemployee;
    $tableemployee=[];
    $ctr=0;
    if($dataemployee->salary==null || $dataemployee->salary==''){
        $gajipokok=0;
    }
    else{
        $gajipokok=(float)(round($dataemployee->salary/173,2));
    }
   foreach ($tmptableemploye as $t){
    $begins = get_date_format_inggris($t->date_from);
    $ends = date('Y-m-d',strtotime(get_date_format_inggris($t->date_to). ' +1 day'));
    $begin = new DateTime($begins);
    $end = new DateTime($ends);


    $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);

  
    foreach($daterange as $date){
        $pagi1=strtotime('05:00:00');
        $pagi2=strtotime('07:00:00');

        $siang1=strtotime('11:30:00');
        $siang2=strtotime('21:00:00');

        $time1 = strtotime($t->overtime_from);
        $time2 = strtotime($t->overtime_to);
        $difference = round(abs($time2 - $time1) / 3600,2);
        $konversi=self::hitungkonversi($difference,$date->format("l"));
        $tarif=(float)(round($gajipokok*$konversi,2));
        $uangmakanpagi=0;
        $uangmakansiang=0;
        if ($time1 >= $pagi1  && $time2<= $pagi2)
        {
           $uangmakanpagi=10000;
        }
       if ($time1 >=$siang1 && $time2<= $siang2)
        {
           $uangmakansiang=15000;
        }
            
        $tmp= array(
            'hari'=>$date->format("l"),
            'tanggal'=>$date->format("Y-m-d"),
            'dari'=>$t->overtime_from,
            'sampai'=>$t->overtime_to,
            'total_jam'=>$difference,
            'konversi'=>$konversi,
            'makan_pagi'=>$uangmakanpagi,
            'makan_siang'=>$uangmakansiang,
            'tarif'=>$tarif,
            'perjam'=>$gajipokok

        );
        $tableemployee[$ctr]=$tmp;
        $ctr++;
    }
          
   }
}

}


$pdf = WkPdf::loadView('admin.report.print-upah-lembur',compact('dataemployee','tableemployee','bulan','year'));

return $pdf->stream('Upah_Lembur.pdf');
}
    /*Version 1.5*/

    /* pdfSalaryStatement  Function Start Here */
    public function pdfSalaryStatement($id, $date_from, $date_to)
    {
        $date_from=get_date_format_inggris($date_from);
        $date_to=get_date_format_inggris($date_to);


        $payslip = Payroll::where('emp_id', $id)->first();
        $payroll = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->get();

        $net_salary = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('net_salary');
        $over_time = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('overtime_salary');
        $tax = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('tax');
        $provident_fund = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('provident_fund');
        $loan = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('loan');
        $total_salary = Payroll::where('emp_id', $id)->whereBetween('payment_date', [$date_from, $date_to])->sum('total_salary');


        $pdf =\WkPdf::loadView('admin.report.pdf-salary-statement', compact('payroll', 'payslip', 'id', 'date_from', 'date_to', 'net_salary', 'over_time', 'tax', 'provident_fund', 'loan', 'total_salary'));
        return $pdf->stream('salary_statement.pdf');


    }



}
