<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\AccountingCash;
use App\AccountingBkp;
use App\AccountingBkpDetail;
use App\AccountingBkm;
use App\AccountingBkmDetail;
use App\AccountingCashDetail;
use App\Cash;
use App\Coa;
use App\Ledger;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;

class AccountingCheckController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    function updateGiro(){

        $id=input::get('id');
        $tipe=input::get('tipe');
        $saldo=input::get('saldo');
        $saldo = str_replace(',', '',  $saldo);
        if($tipe=='bkp'){
            $updategiro=   AccountingBkp::find($id);

        }
        else{
            $updategiro=   AccountingBkm::find($id);

        }
        $updategiro->saldo= $saldo;
        $updategiro->save();
        return redirect('accountings/check')->with([
            'message' => 'Data Updated'
        ]);
    }
   function check(){

        $bkp = AccountingBkp::orderBy('id','asc')->get();
        $bkm = AccountingBkm::orderBy('id','asc')->get();
        return view('admin.proof_giro.proof_giro', compact('bkp','bkm'));
    }
}
