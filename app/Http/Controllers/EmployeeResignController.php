<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\Employee;
use App\EmployeeResign;
use App\EmployeeRolesPermission;
use App\ktpBlocked;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
date_default_timezone_set(app_config('Timezone'));
class EmployeeResignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    /* employeeResigns  Function Start Here */
    public function employeeResigns()
    {
        // echo "masuk"; exit;
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 101)->first();
        $employee_resigns = EmployeeResign::orderBy('id','asc')->get();
        // echo sizeof($employee_resigns); exit;
        return view('admin.employee-resign.employee-resigns', compact('employee_resigns','permcheck'));
    }

    /* blockEmployee  Function Start Here */
    public function blockEmployee($id)
    {
        // echo "masuk"; exit;
        $emp_block = EmployeeResign::find($id);
        if($emp_block){
            
            $exist = ktpBlocked::where('no_ktp', '=', $emp_block->no_ktp)->first();
            if ($exist) {
                return redirect('employee-resigns')->with([
                    'message' => 'Karyawan telah terblokir',
                    'message_important' => true
                ]);
            }
            $ktp_blocked = new ktpBlocked();
            $ktp_blocked->no_ktp = $emp_block->no_ktp;
            $ktp_blocked->save();
            // echo "masuk"; exit;
            return redirect('employee-resigns')->with([
                'message'=>'Karyawan berhasil di blokir'
            ]);

        }else{
            return redirect('employee-resigns')->with([
                'message'=>'Karyawan tidak ditemukan',
                'message_important'=>true
            ]);
        }
    }
}