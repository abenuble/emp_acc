<?php

namespace App\Http\Controllers;

use App\AppConfig;
use App\Attendance;
use App\Classes\permission;
use App\CareerPathEmployee;
use App\CareerPathEmployeeAssessment;
use App\CareerPathEmployeeCompetence;
use App\Company;
use App\ContractRecipient;
use App\Department;
use App\Designation;
use App\Project;
use App\ProjectNeeds;
use App\EmailTemplate;
use App\Employee;
use App\EmployeeDependents;
use App\EmployeeBankAccount;
use App\EmployeeFiles;
use App\EmployeeRoles;
use App\EmployeeRolesPermission;
use App\EmploymentHistory;
use App\EmployeePaymentHistory;
use App\Schedule;
use App\ScheduleComponent;
use App\ScheduleEmployee;
use App\Warning;
use App\Http\Requests;
use App\TaxRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;
use WkPdf;
date_default_timezone_set(app_config('Timezone'));
class ReferenceLetterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    /* allEmployees  Function Start Here */
    public function ReferenceLetter()
    {

        $employee_permission = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 25)->first();
        $check_status=Employee::where('status','active')->where('role_id','!=','1')->get();
        if ($check_status){
            foreach ($check_status as $cs){
                $leave_date=$cs->dol;
                if ($leave_date){
                    if (strtotime($leave_date) < strtotime('now')){
                        $cs->status='inactive';
                        $cs->save();
                    }
                }

            }
        }

        $company = Company::all();
        $designation = Designation::all();
        $projects = Project::all();
        $employees = Employee::where('role_id','!=','1')->get();
        return view('admin.referenceletters.referenceletters', compact('employees','projects','company','designation','employee_permission'));
    }

    public function viewReferenceLetter($id)
    {

        $employee       = Employee::find($id);

        if ($employee) {
            $designation    = Designation::all();
            $department     = Department::all();
            $company        = Company::all();
            $tax            = TaxRules::where('status','active')->get();
            $role           = EmployeeRoles::where('status','Active')->get();

            $dependents     = EmployeeDependents::where('emp_id', '=', $id)->get();
            $bank_accounts  = EmployeeBankAccount::where('emp_id', '=', $id)->first();
            $employee_doc   = EmployeeFiles::where('emp_id', '=', $id)->get();
            $warning        = Warning::where('employee_name', '=', $id)->where('status', '=', 'accepted')->get();
            $employment     = EmploymentHistory::where('emp_id','=',$id)->get();
            $contract       = ContractRecipient::where('recipients','=',$id)->get();
            $salary         = EmployeePaymentHistory::where('emp_id','=',$id)->get();


            return view('admin.referenceletters.view-reference-letter', compact('contract','employment','warning','employee','designation','department','company','dependents','bank_accounts','employee_doc','tax','role','salary'));
        } else {
            return redirect('employees/all')->with([
                'message' => language_data('Employee Not Found'),
                'message_important' => true
            ]);
        }
    }

}
