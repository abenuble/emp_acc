<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\permission;
use App\Mutation;
use App\Company;
use App\Component;
use App\PayrollComponent;
use App\Notification;
use App\Project;
use App\PayrollTypes;
use App\ProjectNeeds;
use App\EmailTemplate;
use App\Employee;
use App\EmployeeRolesPermission;
use Illuminate\Support\Facades\Input;
use DB;
use WkPdf;

class MutationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* contract  Function Start Here */
    public function mutations()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 48)->first();
        $comp_id = '';
        $proj_id = '';
        $emp_id = '';
        $des_id = '';


        $mutations = Mutation::orderBy('id','asc')->get();
        $company = Company::all();
        $employee = Employee::where('role_id','!=','1')->get();
        $project = Project::all();
        $email_template = EmailTemplate::where('table_type','=','sys_mutations')->get();
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        // $last_letter_number = DB::table('sys_mutations')->orderBy('id','asc')->value('letter_number');
        // if(  $last_letter_number ){
        //     $last_number = explode('/', $last_letter_number);
        // }
        // else{
        //     $last_number =0;
        // }
        
        // if($date=='01'){
        //     $number = 01;
        // } else{
        //     $number = 1 + $last_number[0];
        // }
        $last_letter_number = DB::table('sys_mutations')->where('letter_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('letter_number');
        if($last_letter_number==''){
            $number = 1;
        } else{
            $last_number = explode('/', $last_letter_number);
            $number = 1 + $last_number[0];
        }
        $comp = Company::find(\Auth::user()->company);
        return view('admin.mutation.mutations', compact('des_id','comp_id','proj_id','emp_id','month','year','number','mutations','employee','company','comp','project','email_template','permcheck'));

    }

    /* getDesignation  Function Start Here */
    public function getDesignation(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $designation = Employee::where('id', $emp_id)->get();
            foreach ($designation as $d) {
                echo '<option value="' . $d->designation . '">' . $d->designation_name->department_name->department . ' (' . $d->designation_name->designation . ')</option>';
            }
        }
    }

    /* getDesignationNew  Function Start Here */
    public function getDesignationNew(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $designation = Employee::where('id', $emp_id)->first();
            echo '<option value="' . $designation->designation_name->id . '">' . $designation->designation_name->department_name->department . ' (' . $designation->designation_name->designation . ')</option>';
            if($designation->designation_name->career!=0){
                echo '<option value="' . $designation->designation_name->career_info->id . '">' . $designation->designation_name->career_info->department_name->department . ' (' . $designation->designation_name->career_info->designation . ')</option>';
            }
        }
    }

    /* getLocation  Function Start Here */
    public function getLocation(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $location = Employee::select('location')->where('id', $emp_id)->distinct()->get();
            foreach ($location as $l) {
                echo '<option value="' . $l->location . '">' . $l->location . '</option>';
            }
        }
    }

    /* getLocation  Function Start Here */
    public function getLocationNew(Request $request)
    {
        $proj_id = $request->proj_id;
        if ($proj_id) {
            $location = ProjectNeeds::where('id', $proj_id)->distinct()->get();
            foreach ($location as $l) {
                echo '<option value="' . $l->location . '">' . $l->location . '</option>';
            }
        }
    }

    /* getEmployeeCode  Function Start Here */
    public function getEmployeeCode(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee_code = Employee::where('id', $emp_id)->where('status','=','active')->get();
            foreach ($employee_code as $e) {
                echo '<option value="' . $e->employee_code . '">' . $e->employee_code . '</option>';
            }
        }
    }
    
    /* getEmployeeStatus  Function Start Here */
    public function getEmployeeStatus(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee = Employee::where('id', $emp_id)->where('status','=','active')->get();
            foreach ($employee as $e) {
                echo '<option value="' . $e->employee_status . '">' . language_data($e->employee_status) . '</option>';
            }
        }
    }

    /* getEmployeeStatus  Function Start Here */
    public function getPaymentType(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            echo '<option value="0">Silahkan Pilih Tipe Upah</option>';
            $employee = Employee::find($emp_id);
            if($employee->payroll_info){
                echo '<option value="' . $employee->payroll_info->id . '" selected>' . $employee->payroll_info->payroll_name . '</option>';
            }
        }
    }

    /* getEmployeeStatus  Function Start Here */
    public function getPaymentTypeNew(Request $request)
    {
        $payroll = PayrollTypes::get();
        foreach($payroll as $p){
            echo '<option value="' . $p->id . '" selected>' . $p->payroll_name . '</option>';
        }
    }

    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            // echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->where('status','=','opening')->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project_number . '</option>';
            }
        }
    }

    /* getProject Function Start Here */
    public function getCompanyCode(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            $company = Company::find($comp_id);
            echo '<option value="' . $company->id . '" selected>' . $company->company_code . '</option>';
        }
    }


    /* getProject Function Start Here */
    public function getCompanyCodeNew(Request $request)
    {

        $company = Company::orderBy('company_code','asc')->get();
        foreach($company as $s){
            echo '<option value="' . $s->id . '" selected>' . $s->company_code . ' - '. $s->company .'</option>';
        }
    }

    /* getEmployee Function Start Here */
    public function getEmployee(Request $request)
    {

        $proj_id = $request->proj_id;
        if ($proj_id) {
            echo '<option value="0">Select Employee</option>';
            $employee = Employee::where('project', $proj_id)->where('role_id','!=',1)->where('status','=','active')->get();
            foreach ($employee as $e) {
                echo '<option value="' . $e->id . '">' . $e->fname . ' ' . $e->lname . '</option>';
            }
        }
    }

    /* deleteMutation  Function Start Here */
    public function deleteMutation($id)
    {

        $mutations = Mutation::find($id);
        if ($mutations) {
            $mutations->delete();

            return redirect('mutations')->with([
                'message' => 'Mutation Deleted Successfully'
            ]);
        } else {
            return redirect('mutations')->with([
                'message' => 'Mutation Not Found',
                'message_important' => true
            ]);
        }

    }


    /* addMutation  Function Start Here */
    public function addMutation(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'draft_letter' => 'required',
            
            'date' => 'required',
            'mutation_date' => 'required',
            'project_number' => 'required',
            'company_name' => 'required',
            'employee_code' => 'required',
            'employee_name' => 'required',
            'designation_old' => 'required',
            'location_old' => 'required',
            'employee_status_old' => 'required',
            'designation_new' => 'required',
            'employee_status_new' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('mutations')->withErrors($v->errors());
        }
        $tembusan = Input::get('tembusan');

        $draft_letter = Input::get('draft_letter');
        $letter_number = Input::get('letter_number');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $mutation_date = Input::get('mutation_date');
        $mutation_date = get_date_format_inggris($mutation_date);
        $mutation_type = Input::get('mutation_type');
        $project_number = Input::get('project_number');
        $company_name = Input::get('company_name');
        $employee_code = Input::get('employee_code');
        $employee_name = Input::get('employee_name');
        $designation_old = Input::get('designation_old');
        $location_old = Input::get('location_old');
        $employee_status_old = Input::get('employee_status_old');
        $company_code_new = Input::get('company_code_new');
        $payment_type_old = Input::get('payment_type_old');
        $payment_type_new = Input::get('payment_type_new');
        $designation_new = Input::get('designation_new');
        $location_new = Input::get('location_new');
        $employee_status_new = Input::get('employee_status_new');
        //Penomoran id
        $last_number_id = Mutation::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $mutation = new Mutation();
        $mutation->id = $number_id;
        $mutation->tembusan = $tembusan;

        $mutation->draft_letter = $draft_letter;
        $mutation->letter_number = $letter_number;
        $mutation->date = $date;
        $mutation->mutation_date = $mutation_date;
        $mutation->mutation_type = $mutation_type;
        $mutation->project_number = $project_number;
        $mutation->company_name = $company_name;
        $mutation->employee_code = $employee_code;
        $mutation->employee_name = $employee_name;
        $mutation->designation_old = $designation_old;
        $mutation->location_old = $location_old;
        $mutation->employee_status_old = $employee_status_old;
        $mutation->company_code_new = $company_code_new;
        $mutation->payment_type_old = $payment_type_old;
        $mutation->payment_type_new = $payment_type_new;
        $mutation->designation_new = $designation_new;
        $mutation->location_new = $location_new;
        $mutation->employee_status_new = $employee_status_new;
        $mutation->status = 'draft';
        $mutation->save();
        
        $notification = new Notification();
        $notification->id_tag = $mutation->id;
        $notification->tag = 'mutations';
        $notification->title = 'Persetujuan Mutasi';
        $notification->description = 'Persetujuan Mutasi Pegawai '.$employee_code;
        $notification->show_date = date('Y-m-d');
        $notification->start_date = $mutation_date;
        $notification->end_date = $mutation_date;
        $notification->route = 'approvals/mutations';
        $notification->save();

        if ($mutation!='') {
            return redirect('mutations')->with([
                'message' => 'Mutation Added Successfully'
            ]);

        } else {
            return redirect('mutations')->with([
                'message' => 'Mutation Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateMutation  Function Start Here */
    public function updateMutation(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'mutation_date' => 'required',
            'designation_new' => 'required',
            'employee_status_new' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('mutations')->withErrors($v->errors());
        }
        $mutation = Mutation::find($cmd);
        $mutation_date = Input::get('mutation_date');
        $mutation_date = get_date_format_inggris($mutation_date);
        $company_code_new = Input::get('company_code_new');
        $payment_type_new = Input::get('payment_type_new');
        $designation_new = Input::get('designation_new');
        $location_new = Input::get('location_new');
        $employee_status_new = Input::get('employee_status_new');

        if ($mutation) {
            $mutation->mutation_date = $mutation_date;
            $mutation->company_code_new = $company_code_new;
            $mutation->payment_type_new = $payment_type_new;
            $mutation->designation_new = $designation_new;
            $mutation->location_new = $location_new;
            $mutation->employee_status_new = $employee_status_new;
            $mutation->status = 'draft';
            $mutation->save();
            
            $notification = Notification::where('tag','=','mutations')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->start_date = $mutation_date;
                $notification->end_date = $mutation_date;
                $notification->save();
            }

            return redirect('mutations')->with([
                'message' => 'Mutation Updated Successfully'
            ]);

        } else {
            return redirect('mutations')->with([
                'message' => 'Mutation Not Found',
                'message_important' => true
            ]);
        }
    }

    /* setMutationStatus  Function Start Here */
    public function setMutationStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('mutations')->withErrors($v->fails());
        }

        $mutations  = Mutation::find($cmd);
        if($mutations){
            $mutations->status=$request->status;
            $mutations->save();

            return redirect('mutations')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('mutations')->with([
                'message' => 'Mutation not found',
                'message_important'=>true
            ]);
        }

    }

    /* viewMutation  Function Start Here */
    public function viewMutation($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 48)->first();
        $mutations = Mutation::find($id);
        return view('admin.mutation.mutation-view', compact('mutations','permcheck'));
    }

    /* editMutation  Function Start Here */
    public function editMutation($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 48)->first();
        $emp_id = '';
        $proj_id = '';
        $mutations = Mutation::find($id);
        return view('admin.mutation.mutation-edit', compact('emp_id','proj_id','mutations','permcheck'));
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {

        $mutations = Mutation::find($id);
        $draft_letter = EmailTemplate::find($mutations->draft_letter);

        $letter_number = $mutations->letter_number;
        $date = get_date_format_indonesia($mutations->date);
        $mutation_date = get_date_format_indonesia($mutations->mutation_date);
        $project_number = $mutations->project_info->project_number;
        $company_name = $mutations->company_info->company;
        $company_code_old = $mutations->company_info->company_code.'-'.$mutations->company_info->company;
        $company_code_new = $mutations->company_info_new->company_code.'-'.$mutations->company_info->company;
        if($mutations->payment_old_info){
            $payment_type_old = $mutations->payment_old_info->payroll_name;
        } else {
            $payment_type_old = 'Belum Ada';
        }
        $payment_type_new = $mutations->payment_new_info->payroll_name;
        $employee_code = $mutations->employee_code;
        $employee_name = $mutations->employee_info->fname . " " . $mutations->employee_info->lname;
        $designation_old = $mutations->designation_old_info->designation;
        $location_old = $mutations->location_old;
        $employee_status_old = $mutations->employee_status_old;
        $designation_new = $mutations->designation_new_info->designation;
        $location_new = $mutations->location_new;
        $employee_status_new = $mutations->employee_status_new;
        $mutation_type = $mutations->mutation_type;
        $reason = $mutations->keterangan;

        $salary = '';
        $payroll_component  = Component::where('status','=','active')->get();
        $payroll_component_old  = PayrollComponent::where('id_payroll_type','=',$mutations->payment_old_info->id)->get();
        $payroll_component_new  = PayrollComponent::where('id_payroll_type','=',$mutations->payment_new_info->id)->get();
        foreach($payroll_component as $p){
            $lama = 0;
            $baru = 0;
            $payroll_component_old  = PayrollComponent::where('id_payroll_type','=',$mutations->payment_old_info->id)->where('id_component','=',$p->id)->first();
            $payroll_component_new  = PayrollComponent::where('id_payroll_type','=',$mutations->payment_new_info->id)->where('id_component','=',$p->id)->first();
            if($payroll_component_old){
                $lama = number_format($payroll_component_old->value,0);
            }
            if($payroll_component_new){
                $baru = number_format($payroll_component_new->value,0);
            }
            $salary .= 
            '<tr>
                <td>'.$p->component.'</td>
                <td>:</td>
                <td style="text-align: right; border-right: 1px solid black;">'.$lama.'</td>
                <td style="text-align: right;">'.$baru.'</td>                                       
            </tr>'
            ;
        }
            
        $salary_change = 
        
        '<table width="100%" cellpadding="5">
            <thead>
                <tr>
                    <td width="25%" rowspan="2"></td>
                    <td width="5%"></td>
                    <td colspan="2" style="text-align: center"> Perubahan Gaji</td>
                </tr>
                <tr>
                    <td></td>
                    <td width="35%" style="text-align: center; border-right: 1px solid black;"> Saat ini</td>
                    <td width="35%" style="text-align: center"> Baru</td>
                </tr>
            </thead>
            <tbody>'
                .$salary.
            '</tbody>
        </table>';

        
        $data = array(
            'letter_number' => $letter_number,
            'date' => $date,
            'mutation_date' => $mutation_date,
            'project_number' => $project_number,
            'company_name' => $company_name,
            'company_code_old' => $company_code_old,
            'company_code_new' => $company_code_new,
            'payment_type_old' => $payment_type_old,
            'payment_type_new' => $payment_type_new,
            'employee_code' => $employee_code,
            'employee_name' => $employee_name,
            'designation_old' => $designation_old,
            'location_old' => $location_old,
            'employee_status_old' => $employee_status_old,
            'designation_new' => $designation_new,
            'location_new' => $location_new,
            'employee_status_new' => $employee_status_new,
            'salary_change' => $salary_change,
            'mutation_type' => $mutation_type,
            'reason' => $reason
        );
        $template = $draft_letter->message;
        
        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.mutation.pdf-mutations', compact('message','letter_number'));

        return $pdf->stream($letter_number.'.pdf');
    }
}

