<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\permission;
use App\Contract;
use App\ContractRecipient;
use App\Company;
use App\Copy;
use App\Notification;
use App\PayrollTypes;
use App\PayrollComponent;
use App\Project;
use App\ProjectNeeds;
use App\EmailTemplate;
use App\Employee;
use App\EmployeeRolesPermission;
use Illuminate\Support\Facades\Input;
use DB;
use WkPdf;

class ContractController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* contract  Function Start Here */
    public function contracts()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 46)->first();
        $comp_id = '';
        $proj_id = '';


        $contracts = Contract::orderBy('id','asc')->get();
        $company = Company::all();
        $project = Project::all();
        $payroll_types = PayrollTypes::all();
        $email_template = EmailTemplate::where('table_type','=','sys_contracts')->get();
        
        $employee = Employee::where('role_id','!=','1')->get();

        // $members_dropdown = array();

        // foreach($employee as $emp){
        //     $members_dropdown[] = array("id" => $emp->id, "text" => $emp->fname . " " . $emp->lname);
        // }

        return view('admin.contract.contracts', compact('payroll_types','des_id','comp_id','proj_id','contracts','employee','company','project','email_template','permcheck'));

    }

    /* getEmployee Function Start Here */
    public function getEmployee(Request $request)
    {
        $project = $request->proj_id;

        $employee = Employee::where('role_id','!=','1')->where('project','=',$project)->where('status','=','active')->get();

        $members_dropdown = array();

        foreach($employee as $emp){
            $members_dropdown[] = array("id" => $emp->id, "text" => $emp->fname . " " . $emp->lname);
        }
        return $members_dropdown;
    }

    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            // echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->where('status','=','opening')->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project_number . '</option>';
            }
        }
    }

    /* getSalary Function Start Here */
    public function getSalary(Request $request)
    {

        $payroll = $request->payroll_id;
        if ($payroll) {
            $payroll_type = PayrollTypes::find($payroll);
            $salary = $payroll_type->total;
            return number_format($salary,0);
        }
    }

    /* getProjectName Function Start Here */
    public function getProjectName(Request $request)
    {

        $proj_id = $request->proj_id;
        if ($proj_id) {
            $project = Project::where('id', $proj_id)->get();
            foreach ($project as $d) {                
                echo '<option value="' . $d->project . '">' . $d->project . '</option>';
            }
        }
    }

    /* getDesignationName Function Start Here */
    public function getDesignationName(Request $request)
    {

        $id = $request->proj_id;
        if ($id) {
            $project = ProjectNeeds::where('id', '=', $id)->get();
            foreach ($project as $d) {                
                echo '<option value="' . $d->designation_name->id . '">' . $d->designation_name->designation . '</option>';
            }
        }
    }

    /* getLocation  Function Start Here */
    public function getLocation(Request $request)
    {
        $id = $request->proj_id;
        if ($id) {
            $location = ProjectNeeds::where('id', '=', $id)->get();
            foreach ($location as $l) {
                echo '<option value="' . $l->location . '">' . $l->location . '</option>';
            }
        }
    }

    /* deleteContract  Function Start Here */
    public function deleteContract($id)
    {

        $contracts = Contract::find($id);
        if ($contracts) {
            $contracts->delete();

            return redirect('contracts')->with([
                'message' => 'Contract Deleted Successfully'
            ]);
        } else {
            return redirect('contracts')->with([
                'message' => 'Contract Not Found',
                'message_important' => true
            ]);
        }

    }


    /* addContract  Function Start Here */
    public function addContract(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'draft_letter' => 'required',
            'contract_types' => 'required',
            'date' => 'required',
            'project_number' => 'required',
            'project_name' => 'required',
            'company_name' => 'required',
            'payroll_types' => 'required',
            'effective_date' => 'required',
            'end_date' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('contracts')->withErrors($v->errors());
        }

        $draft_letter = Input::get('draft_letter');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $effective_date = Input::get('effective_date');
        $effective_date = get_date_format_inggris($effective_date);
        $end_date = Input::get('end_date');
        $end_date = get_date_format_inggris($end_date);
        $project_number = Input::get('project_number');
        $project_name = Input::get('project_name');
        $company_name = Input::get('company_name');
        $payroll_types = Input::get('payroll_types');        
        $salary = (int)str_replace(',', '', Input::get('salary'));
        $designation = Input::get('designation');
        $location = Input::get('location');
        $contract_types = Input::get('contract_types');
        $share_with = Input::get('share_with');
        $share_with_specific = Input::get('share_with_specific');
        //Penomoran id
        $last_number_id = Contract::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $contract = new Contract();
        $contract->id = $number_id;
        $contract->draft_letter = $draft_letter;
        $contract->date = $date;
        $contract->project_number = $project_number;
        $contract->contract_types = $contract_types;
        $contract->project_name = $project_name;
        $contract->company_name = $company_name;
        $contract->salary = $salary;
        $contract->designation = $designation;
        $contract->location = $location;
        $contract->payroll_types = $payroll_types;
        $contract->effective_date = $effective_date;
        $contract->end_date = $end_date;
        $contract->share_with = $share_with;
        $contract->status = 'draft';
        $contract->save();

        $contract_id = $contract->id;
        
        $notification = new Notification();
        $notification->id_tag = $contract_id;
        $notification->tag = 'contracts';
        $notification->title = 'Persetujuan Kontrak';
        $notification->description = 'Persetujuan Kontrak Pegawai Proyek '.$project_number;
        $notification->show_date = date('Y-m-d');
        $notification->start_date = $effective_date;
        $notification->end_date = $effective_date;
        $notification->route = 'approvals/contracts';
        $notification->save();


        $comp = Company::find(\Auth::user()->company);
        // echo $comp->company;
        // exit;
        $perusahaan = '';
        if($share_with == 'all'){
            $projectidd=Project::where('project','=',$project_name)->first();
            $projectidd= $projectidd->id;
            
            $recipients = Employee::where('payment_type','=',$payroll_types)->where('designation','=',$designation)->where('location','=',$location)->where('project','=',$projectidd)->get()->toArray();
            // $recipients = Employee::where('project','=',$projectidd)->get()->toArray();

            $num_elements = 0;
            
            $sql_data = array();
            //Penomoran surat
            $date = date('d');
            $month = date('m');
            $year = date('Y');
            // $last_letter_number = DB::table('sys_contract_recipients')->orderBy('id','asc')->value('letter_number');
            // $last_number = explode('/', $last_letter_number);
            // if($date=='01'){
            //     $number = 01;
            // } else{
            //     $number = 1 + $last_number[0];
            // }
    
            $perusahaan = 'SSS';
            // if($contract_types=='PKWT1' || $contract_types=='PKWT2'){
            //     $contract_types = 'PKWT';
            // }
            while($num_elements<count($recipients)) {
                // $employee_code = implode(".", $recipients[$num_elements]['employee_code']);
                $sql_data[] = array(
                    'letter_number' => substr($recipients[$num_elements]['employee_code'],-4)."/".$contract_types."-".$perusahaan."/".get_roman_letters($month)."/".$year,
                    'project_number' => $project_number,
                    'contract_id' => $contract_id,
                    'payroll_types' => $payroll_types,
                    'recipients' => $recipients[$num_elements]['id'],
                );
                // $number++;
                $num_elements++;
            }
    
            $contract_recipient = ContractRecipient::insert($sql_data);

        } else if($share_with == 'specific'){
            $recipients = explode(",", $share_with_specific);

            $num_elements = 0;
            
            $sql_data = array();
            //Penomoran surat
            $date = date('d');
            $month = date('m');
            $year = date('Y');
            // $last_letter_number = DB::table('sys_contract_recipients')->orderBy('id','asc')->value('letter_number');
            // if($last_letter_number){
            //     $last_number = explode('/', $last_letter_number);
            // }
            // else{
            //     $last_number=0;
            // }
          

            // if($date=='01'){
            //     $number = 01;
            // } else{
            //     $number = 1 + $last_number[0];
            // }
            $perusahaan='SSS';
            if($contract_types=='PKWT1' || $contract_types=='PKWT2'){
                $contract_types = 'PKWT';
            }
            while($num_elements<count($recipients)) {
                $emp_code = Employee::find($recipients[$num_elements]);
                // $employee_code = implode(".", $emp_code->employee_code);
                $sql_data[] = array(
                    'letter_number' => substr($emp_code->employee_code,-4)."/".$contract_types."-".$perusahaan."/".get_roman_letters($month)."/".$year,
                    'project_number' => $project_number,
                    'contract_id' => $contract_id,
                    'payroll_types' => $payroll_types,
                    'recipients' => $emp_code->id,
                );
                // $number++;
                $num_elements++;
            }
    
            $contract_recipient = ContractRecipient::insert($sql_data);
        }


        if ($contract!='') {
            return redirect('contracts')->with([
                'message' => 'Contract Added Successfully'
            ]);

        } else {
            return redirect('contracts')->with([
                'message' => 'Contract Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateContract  Function Start Here */
    public function updateContract(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'effective_date' => 'required',
            'draft_letter' => 'required',
            'end_date' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('contracts')->withErrors($v->errors());
        }
        $contract = Contract::find($cmd);
        $effective_date = Input::get('effective_date');
        $effective_date = get_date_format_inggris($effective_date);
        $end_date = Input::get('end_date');
        $end_date = get_date_format_inggris($end_date);
        $draft_letter = Input::get('draft_letter');

        if ($contract) {
            $contract->effective_date = $effective_date;
            $contract->end_date = $end_date;
            $contract->draft_letter = $draft_letter;
            $contract->status = 'draft';
            $contract->save();
            
            $notification = Notification::where('tag','=','contracts')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->end_date = $effective_date;
                $notification->end_date = $effective_date;
                $notification->save();
            }

            return redirect('contracts')->with([
                'message' => 'Contract Updated Successfully'
            ]);

        } else {
            return redirect('contracts')->with([
                'message' => 'Contract Not Found',
                'message_important' => true
            ]);
        }
    }

    /* setContractStatus  Function Start Here */
    public function setContractStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('contracts')->withErrors($v->fails());
        }

        $contracts  = Contract::find($cmd);
        if($contracts){
            $contracts->status=$request->status;
            $contracts->save();

            return redirect('contracts')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('contracts')->with([
                'message' => 'Contract not found',
                'message_important'=>true
            ]);
        }

    }

    /* viewContract  Function Start Here */
    public function viewContract($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 46)->first();
        $contracts = Contract::find($id);
        $recipients = ContractRecipient::where('contract_id','=',$id)->get();
        return view('admin.contract.contract-view', compact('recipients','contracts','permcheck'));
    }

    /* editContract  Function Start Here */
    public function editContract($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 46)->first();
        $contracts = Contract::find($id);
        $recipients = ContractRecipient::where('contract_id','=',$id)->get();
        $email_template = EmailTemplate::where('table_type','=','sys_contracts')->get();
        return view('admin.contract.contract-edit', compact('email_template','recipients','contracts','permcheck'));
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {
        $recipients = ContractRecipient::find($id);
        $draft_letter = EmailTemplate::find($recipients->contract_info->draft_letter);
        $payroll_types = $recipients->payroll_info->payroll_name;
        $payroll_component = $recipients->payroll_info->payroll_component;
        if (strpos(strtolower($draft_letter->tplname), 'borongan') === TRUE) {
            $payroll_types = '<table border="1" cellpadding="10" width="100%">';
            $payroll_types .= '<thead>';
            $payroll_types .= '<tr>';
            $payroll_types .= '<td>No</td>';
            $payroll_types .= '<td>Uraian</td>';
            $payroll_types .= '<td>Harga Borongan</td>';
            $payroll_types .= '<td>Keterangan</td>';
            $payroll_types .= '</tr>';
            $payroll_types .= '</thead>';
            $payroll_types .= '<tbody>';
            $no = 1;
            foreach($payroll_component as $component) {
                $payroll_types .= '<tr>';
                $payroll_types .= '<td>'.$no.'</td>';
                $payroll_types .= '<td>'.$component->component_name->component.'</td>';
                $payroll_types .= '<td align="right">'.number_format($component->value,0).'</td>';
                $payroll_types .= '<td>RP / 1 Juta Rod</td>';
                $payroll_types .= '</tr>';
            }
            $payroll_types .= '</tbody>';
            $payroll_types .= '</table><br>';
        }
        if (strpos(strtolower($draft_letter->tplname), 'bulanan') === TRUE) {
            $payroll_types = '<table>';
            foreach($payroll_component as $component) {
                $payroll_types .= '<tr>';
                $payroll_types .= '<td>'.$component->component_name->component.'</td>';
                $payroll_types .= '<td width="1%">:</td>';
                $payroll_types .= '<td><span class="pull-left">Rp.</span><span class="pull-right">'.number_format($component->value,0).',-/Bulan</span></td>';
                $payroll_types .= '</tr>';
            }
            $payroll_types .= '</table><br>';
        }
        if (strpos(strtolower($draft_letter->tplname), 'harian') === TRUE) {
            $payroll_types = 'Rp. '.number_format($recipients->payroll_info->total,0);
        }

        $letter_number = $recipients->letter_number;
        $date = get_date_format_indonesia($recipients->contract_info->date);
        $project_number = $recipients->project_info->project_number;
        $project_name = $recipients->project_info->project;
        $company_name = $recipients->contract_info->company_info->company;
        $employee_name = $recipients->employee_info->fname . " " . $recipients->employee_info->lname;
        $effective_date = get_date_format_indonesia($recipients->contract_info->effective_date);
        $end_date = get_date_format_indonesia($recipients->contract_info->end_date);
        $address = $recipients->employee_info->per_address;
        if($recipients->employee_info->gender=='male'){
            $gender = "Laki-laki";
        } else {
            $gender = "Wanita";
        }
        $last_education = $recipients->employee_info->last_education_info->education;
        $ktp = $recipients->employee_info->no_ktp;
        $phone = $recipients->employee_info->phone;
        $job = $recipients->employee_info->designation_name->designation;
        $date_of_birth = $recipients->employee_info->birth_place . ", " .  $recipients->employee_info->dob;
        $salary = number_format($recipients->employee_info->salary,0);
        $attendance_allowance = $recipients->employee_info->attendance_allowance;

        $data = array(
            'letter_number' => $letter_number,
            'day' => get_date_day($date),
            'date' => $date,
            'project_number' => $project_number,
            'project_name' => $project_name,
            'company_name' => $company_name,
            'payroll_types' => $payroll_types,
            'effective_date' => $effective_date,
            'employee_name' => $employee_name,
            'address' => $address,
            'gender' => $gender,
            'last_education' => $last_education,
            'ktp' => $ktp,
            'phone' => $phone,
            'job' => $job,
            'end_date' => $end_date,
            'date_of_birth' => $date_of_birth,
            'salary' => $salary,
            'attendance_allowance' => $attendance_allowance
        );

        $template = $draft_letter->message;
        
        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.contract.pdf-contract', compact('message','letter_number'));

        return $pdf->stream($letter_number.'.pdf');
    }

    /* downloadAllPdf  Function Start Here */
    public function downloadAllPdf($id)
    {
        $contracts = Contract::find($id);
        $recipients = ContractRecipient::where('contract_id','=', $id)->get();
        $draft_letter = EmailTemplate::find($contracts->draft_letter);
        $template = $draft_letter->message;

        $num_elements = 0;
        
        foreach($recipients as $rec){
            $payroll = $rec->payroll_info->payroll_name;
            $payroll_component = $rec->payroll_info->payroll_component;
            if (strpos(strtolower($draft_letter->tplname), 'borongan') === TRUE) {
                $payroll = '<table border="1" cellpadding="10" width="100%">';
                $payroll .= '<thead>';
                $payroll .= '<tr>';
                $payroll .= '<td>No</td>';
                $payroll .= '<td>Uraian</td>';
                $payroll .= '<td>Harga Borongan</td>';
                $payroll .= '<td>Keterangan</td>';
                $payroll .= '</tr>';
                $payroll .= '</thead>';
                $payroll .= '<tbody>';
                $no = 1;
                foreach($payroll_component as $component) {
                    $payroll .= '<tr>';
                    $payroll .= '<td>'.$no.'</td>';
                    $payroll .= '<td>'.$component->component_name->component.'</td>';
                    $payroll .= '<td align="right">'.number_format($component->value,0).'</td>';
                    $payroll .= '<td>RP / 1 Juta Rod</td>';
                    $payroll .= '</tr>';
                }
                $payroll .= '</tbody>';
                $payroll .= '</table><br>';
            }
            if (strpos(strtolower($draft_letter->tplname), 'bulanan') === TRUE) {
                $payroll = '<table>';
                foreach($payroll_component as $component) {
                    $payroll .= '<tr>';
                    $payroll .= '<td>'.$component->component_name->component.'</td>';
                    $payroll .= '<td width="1%">:</td>';
                    $payroll .= '<td><span class="pull-left">Rp.</span><span class="pull-right">'.number_format($component->value,0).',-/Bulan</span></td>';
                    $payroll .= '</tr>';
                }
                $payroll .= '</table><br>';
            }
            if (strpos(strtolower($draft_letter->tplname), 'harian') === TRUE) {
                $payroll = 'Rp. '.number_format($recipients->payroll_info->total,0);
            }
            $letter_number[] = $rec->letter_number;
            $date[] = get_date_format_indonesia($rec->contract_info->date);
            $project_number[] = $rec->project_info->project_number;
            $project_name[] = $rec->project_info->project;
            $company_name[] = $rec->contract_info->company_info->company;
            $payroll_types[] = $payroll;
            $employee_name[] = $rec->employee_info->fname . " " . $rec->employee_info->lname;
            $effective_date[] = get_date_format_indonesia($rec->contract_info->effective_date);
            $end_date[] = get_date_format_indonesia($rec->contract_info->end_date);
            $address[] = $rec->employee_info->per_address;
            $last_education[] = $rec->employee_info->last_education_info->education;
            if($rec->employee_info->gender=='male'){
                $gender[] = "Laki-laki";
            } else {
                $gender[] = "Wanita";
            }
            $ktp[] = $rec->employee_info->no_ktp;
            $phone[] = $rec->employee_info->phone;
            $job[] = $rec->employee_info->designation_name->designation;
            $date_of_birth[] = $rec->employee_info->birth_place . ", " .  $rec->employee_info->dob;
            $salary[] = number_format($rec->employee_info->salary,0);
            $attendance_allowance[] = $rec->employee_info->attendance_allowance;
        }

        while($num_elements<count($recipients)) {
            $data[$num_elements] = array(
                'letter_number' => $letter_number[$num_elements],
                'day' => get_date_day($date[$num_elements]),
                'date' => $date[$num_elements],
                'project_number' => $project_number[$num_elements],
                'project_name' => $project_name[$num_elements],
                'company_name' => $company_name[$num_elements],
                'payroll_types' => $payroll_types[$num_elements],
                'effective_date' => $effective_date[$num_elements],
                'employee_name' => $employee_name[$num_elements],
                'address' => $address[$num_elements],
                'gender' => $gender[$num_elements],
                'last_education' => $last_education[$num_elements],
                'ktp' => $ktp[$num_elements],
                'phone' => $phone[$num_elements],
                'job' => $job[$num_elements],
                'end_date' => $end_date[$num_elements],
                'date_of_birth' => $date_of_birth[$num_elements],
                'salary' => $salary[$num_elements],
                'attendance_allowance' => $attendance_allowance[$num_elements]
            );
            $message[$num_elements] = array(
                'message' => _render($template, $data[$num_elements]));
            $num_elements++;
        }        

        $pdf=WkPdf::loadView('admin.contract.pdf-all-contract', compact('message','letter_number'));

        return $pdf->stream('contracts.pdf');
    }
}