<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\AccountSetting;
use App\Coa;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;

class AccountSettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* contract  Function Start Here */
    public function AccountSetting()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 80)->first();
        $account_setting = AccountSetting::all();
        $coa = Coa::where('coa_type','=','approximate name')->get();
        return view('admin.setting_account.setting-account', compact('account_setting','coa','permcheck'));

    }

    
    /* updatePostAccountSetting  Function Start Here */
    public function updatePostAccountSetting(Request $request)
    {

        $setting_akun_id = Input::get('setting_akun_id');

		for ($i = 0; $i < sizeof($setting_akun_id); $i++) {
            
            $m_coa_id_debit[] = Input::get('m_coa_id_debit'.($i+1));
            $m_coa_id_kredit[] = Input::get('m_coa_id_kredit'.($i+1));
            // print_r(json_encode($m_coa_id_debit[$i])); exit;

            AccountSetting::where('id', '=', ($i+1))->update(['debit' => json_encode($m_coa_id_debit[$i]), 'credit' => json_encode($m_coa_id_kredit[$i])]);

		}

        if ($setting_akun_id!='') {
            return redirect('accountings/account-setting')->with([
                'message' => 'Account Setting Updated Successfully'
            ]);

        } else {
            return redirect('accountings/account-setting')->with([
                'message' => 'Account Setting Not Found',
                'message_important' => true
            ]);
        }
    }

}
