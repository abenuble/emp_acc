<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Department;
use App\Designation;
use App\Employee;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;

date_default_timezone_set(app_config('Timezone'));

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* departments  Function Start Here */
    public function departments()
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 22)->first();
        $departments = Department::orderBy('id','asc')->get();
        return view('admin.department.departments', compact('departments','permcheck'));

    }

    /* editDepartment  Function Start Here */
    public function editDepartment($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 22)->first();
        $departments = Department::find($id);
        return view('admin.department.edit-department', compact('departments','permcheck'));

    }

    /* addDepartment  Function Start Here */
    public function addDepartment(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'department' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('departments')->withErrors($v->errors());
        }
        $department = Input::get('department');
        $status = Input::get('status');

        $exist = Department::where('department', $department)->first();
        if ($exist) {
            return redirect('departments')->with([
                'message' => 'Division Already Exist',
                'message_important' => true
            ]);
        }

        //Penomoran id
        $last_number = Department::max('id');
        if($last_number==''){
            $number = 1;
        } else {
            $number = 1 + $last_number;
        }
        $departments = new Department();
        $departments->id = $number;
        $departments->department = $department;
        $departments->status = $status;
        $departments->save();

        if ($departments!='') {
            return redirect('departments')->with([
                'message' => 'Division Added Successfully'
            ]);

        } else {
            return redirect('departments')->with([
                'message' => 'Division Already Exist',
                'message_important' => true
            ]);
        }


    }


    /* updateDepartment  Function Start Here */
    public function updateDepartment(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'department' => 'required',
            'status'     => 'required'
        ]);

        if ($v->fails()) {
            return redirect('departments')->withErrors($v->errors());
        }
        $department = Department::find($cmd);
        $department_name = Input::get('department');
        $status = Input::get('status');

        if ($department) {
            $department->department = $department_name;
            $department->status = $status;
            $department->save();

            return redirect('departments')->with([
                'message' => 'Division Added Successfully'
            ]);

        } else {
            return redirect('departments')->with([
                'message' => 'Division Not Found',
                'message_important' => true
            ]);
        }
    }

    /* download excel function start here */
    public function downloadExcel()
    {
        return response()->download(public_path('assets/template_xls/divisions.xls'));

    }

    /* import excel function start here */
    public function importExcel()
    {
        // Get current data from department table
        $department = Department::lists('department')->toArray();
        
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $target_file   = $path . basename($_FILES["import_file"]["name"]);
            $fileType = pathinfo($target_file,PATHINFO_EXTENSION);
			$data = Excel::load($path, function($reader) {
            })->get();
            if ($fileType != 'xls'){
                return redirect('companies')->with([
                    'message' => 'Upload an Excel 97-2003 ',
                    'message_important' => true
                ]);
            } else {
                if(!empty($data) && $data->count()){
                    $insert = array();
                    foreach ($data as $key => $value) {
                        // Skip department previously added using in_array
                        if (in_array($value->divisi, $department)){
                            continue;
                        } else{
                            //Penomoran id
                            $last_number = Department::max('id');
                            if($last_number==''){
                                $number = 1;
                            } else {
                                $number = 1 + $last_number;
                            }
                            if ($value->divisi=='') {
                                return redirect('departments')->with([
                                    'message' => 'Divisi Tidak Ditemukan',
                                    'message_important' => true
                                ]);
                            } else {
                                $departments = new Department();
                                $departments->id = $number;
                                $departments->department = $value->divisi;
                                $departments->save();
                            }
                        }
                    }
                    return redirect('departments')->with([
                        'message' => 'Division Added Successfully'
                    ]);
                } else{
                    return redirect('departments')->with([
                        'message' => 'Division Not Found',
                        'message_important' => true
                    ]);
                } 
            }
			
		}
		return back();
    }

}
