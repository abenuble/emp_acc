<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\EmployeeRolesPermission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Classes\permission;
use App\VehicleTax;
use App\Notification;

class VehicleTaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* vehicletax  Function Start Here */
    public function vehicletax()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 14)->first();
        $vehicletax = VehicleTax::orderBy('id','asc')->get();
        return view('admin.vehicletax.vehicletax', compact('vehicletax','permcheck'));

    }

    /* addVehicleTax  Function Start Here */
    public function addVehicleTax(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'vehicle' => 'required',
            'license' => 'required',
            'tax_due_date' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('vehicletax')->withErrors($v->errors());
        }

        $vehicle = Input::get('vehicle');
        $license = Input::get('license');
        $tax_due_date = Input::get('tax_due_date');
        $tax_due_date = get_date_format_inggris($tax_due_date);

        $vehicletax = new VehicleTax();
        $vehicletax->vehicle = $vehicle;
        $vehicletax->license = $license;
        $vehicletax->tax_due_date = $tax_due_date;
        $vehicletax->save();
        
        $notification = new Notification();
        $notification->id_tag = $vehicletax->id;
        $notification->tag = 'vehicletax';
        $notification->title = $vehicle;
        $notification->description = $license;
        $notification->show_date = date('Y-m-d',strtotime(get_date_format_inggris($tax_due_date).'-30 day'));
        $notification->start_date = $tax_due_date;
        $notification->end_date = $tax_due_date;
        $notification->route = 'vehicletax';
        $notification->save();

        if ($vehicletax!='') {
            return redirect('vehicletax')->with([
                'message' => 'Vehicle Tax Added Successfully'
            ]);

        } else {
            return redirect('vehicletax')->with([
                'message' => 'Vehicle Tax Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateVehicleTax  Function Start Here */
    public function updateVehicleTax(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'vehicle' => 'required',
            'license' => 'required',
            'tax_due_date' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('vehicletax')->withErrors($v->errors());
        }
        $vehicletax = VehicleTax::find($cmd);
        $vehicle = Input::get('vehicle');
        $license = Input::get('license');
        $tax_due_date = Input::get('tax_due_date');
        $tax_due_date = get_date_format_inggris($tax_due_date);

        if ($license != $vehicletax->license) {

            $exist = VehicleTax::where('license', $license)->first();
            if ($exist) {
                return redirect('vehicletax')->with([
                    'message' => 'Vehicle Tax Already Exist',
                    'message_important' => true
                ]);
            }
        }

        if ($vehicletax) {
            $vehicletax->vehicle = $vehicle;
            $vehicletax->license = $license;
            $vehicletax->tax_due_date = $tax_due_date;
            $vehicletax->save();

            $notification = Notification::where('tag','=','vehicletax')->where('id_tag','=',$cmd)->first();

            if($notification){
                $notification->title = $vehicle;
                $notification->description = $license;
                $notification->show_date = date('Y-m-d',strtotime(get_date_format_inggris($tax_due_date).'-30 day'));
                $notification->start_date = $tax_due_date;
                $notification->end_date = $tax_due_date;
                $notification->save();
            }

            return redirect('vehicletax')->with([
                'message' => 'Vehicle Tax Updated Successfully'
            ]);

        } else {
            return redirect('vehicletax')->with([
                'message' => 'Vehicle Tax Not Found',
                'message_important' => true
            ]);
        }
    }


    /* deleteVehicleTax  Function Start Here */
    public function deleteVehicleTax($id)
    {

        $vehicletax = VehicleTax::find($id);
        if ($vehicletax) {

            $vehicletax->delete();

            return redirect('vehicletax')->with([
                'message' => 'Vehicle Tax Deleted Successfully'
            ]);
        } else {
            return redirect('vehicletax')->with([
                'message' => 'Vehicle Tax Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewVehicleTax  Function Start Here */
    public function viewVehicleTax($id)
    {

        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 11)->first();
        $d = VehicleTax::find($id);
        return view('admin.vehicletax.view-vehicletax', compact('d','permcheck'));
    }


}

