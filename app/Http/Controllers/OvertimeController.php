<?php

namespace App\Http\Controllers;

use App\Classes\permission;
use App\EmployeeRolesPermission;
use App\Employee;
use App\OvertimeWarrant;
use App\Notification;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Dompdf\Dompdf;
use Knp\Snappy\Pdf;
use WkPdf;
use App\Company;
use DB;
date_default_timezone_set(app_config('Timezone'));

class OvertimeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* warrants  Function Start Here */
    public function warrants()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 44)->first();
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        $company = Company::find(\Auth::user()->company);
        $last_letter_number = DB::table('sys_overtime_warrant')->orderBy('id','asc')->value('letter_number');
        if($last_letter_number){
            $last_number = explode('/', $last_letter_number);}
            else{ $last_number=0;}
        if($date=='01'){
            $number = 01;
        } else{
            $number = 1 + $last_number[0];
        }

        $emp_id = '';
        $warrant = OvertimeWarrant::orderBy('id','asc')->get();
        $employee = Employee::where('role_id','!=','1')->where('employee_type','Internal')->get();
        return view('admin.overtime.overtime-warrants', compact('warrant','employee','emp_id','number','company','month','year','permcheck'));

    }

    /* addWarrant  Function Start Here */
    public function addWarrant(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'employee' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'overtime_from' => 'required',
            'overtime_to' => 'required',
        ]);

        if ($v->fails()) {
            return redirect('overtime/warrants')->withErrors($v->errors());
        }

        $employee = Input::get('employee');
        $letter_number = Input::get('letter_number');
        $tembusan = Input::get('tembusan');
        $date_from = Input::get('date_from');
        $date_to = Input::get('date_to');
        $date_from = get_date_format_inggris($date_from);
        $date_to = get_date_format_inggris($date_to);

        $employee_info = Employee::find($employee);

        $date_total = (((strtotime($request->date_to))-(strtotime($request->date_from)))/86400)+1;

        $overtime_from = Input::get('overtime_from');
        $overtime_to = Input::get('overtime_to');
        $overtime_from = date('H:i:s', strtotime($overtime_from));
        $overtime_to = date('H:i:s', strtotime($overtime_to));
        
        $overtime_total = (((strtotime($request->overtime_to))-(strtotime($request->overtime_from)))/3600) * $date_total;
        $information = Input::get('information');

        $overtime_warrant = new OvertimeWarrant();
        $overtime_warrant->emp_id = $employee;
        $overtime_warrant->letter_number = $letter_number;
        $overtime_warrant->date_from = $date_from;
        $overtime_warrant->tembusan = $tembusan;
        $overtime_warrant->date_to = $date_to;
        $overtime_warrant->overtime_from = $overtime_from;
        $overtime_warrant->overtime_to = $overtime_to;
        $overtime_warrant->overtime_total = $overtime_total;
        $overtime_warrant->information = $information;
        $overtime_warrant->save();
        
        $notification = new Notification();
        $notification->id_tag = $overtime_warrant->id;
        $notification->tag = 'overtime/warrants';
        $notification->title = 'Surat Perintah Lembur';
        $notification->description = 'Surat Perintah Lembur '.$employee_info->employee_code;
        $notification->show_date = date('Y-m-d');
        $notification->start_date =  $date_from;
        $notification->end_date = $date_from;
        $notification->route = 'approvals/overtime-warrants';
        $notification->save();

        if ($overtime_warrant!='') {
            return redirect('overtime/warrants')->with([
                'message' => 'Warrant Added Successfully'
            ]);

        } else {
            return redirect('overtime/warrants')->with([
                'message' => 'Warrant Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* editWarrant  Function Start Here */
    public function editWarrant(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'employee' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'overtime_from' => 'required',
            'overtime_to' => 'required',
        ]);

        if ($v->fails()) {
            return redirect('overtime/warrants')->withErrors($v->errors());
        }
        $tembusan = Input::get('tembusan');

        $employee = Input::get('employee');
        $date_from = Input::get('date_from');
        $date_to = Input::get('date_to');
        $date_from = get_date_format_inggris($date_from);
        $date_to = get_date_format_inggris($date_to);

        $employee_info = Employee::find($employee);


        $date_total = (((strtotime($request->date_to))-(strtotime($request->date_from)))/86400)+1;
        
        $overtime_from = Input::get('overtime_from');
        $overtime_to = Input::get('overtime_to');
        $overtime_from = date('H:i:s', strtotime($overtime_from));
        $overtime_to = date('H:i:s', strtotime($overtime_to));
        
        $overtime_total = (((strtotime($request->overtime_to))-(strtotime($request->overtime_from)))/3600) * $date_total;
        $information = Input::get('information');
        
        $overtime_warrant = OvertimeWarrant::find($cmd);
        if ($overtime_warrant) {
            $overtime_warrant->emp_id = $employee;
            $overtime_warrant->date_from = $date_from;
            $overtime_warrant->date_to = $date_to;
            $overtime_warrant->tembusan = $tembusan;
            $overtime_warrant->overtime_from = $overtime_from;
            $overtime_warrant->overtime_to = $overtime_to;
            $overtime_warrant->overtime_total = $overtime_total;
            $overtime_warrant->information = $information;
            $overtime_warrant->save();
            
            $notification = Notification::where('tag','=','overtime/warrants')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->start_date = $date_from;
                $notification->end_date = $date_from;
                $notification->save();
            }

            return redirect('overtime/warrants')->with([
                'message' => 'Warrant Updated Successfully'
            ]);

        } else {
            return redirect('overtime/warrants')->with([
                'message' => 'Warrant Not Found',
                'message_important' => true
            ]);
        }
    }

    /* setWarrantStatus  Function Start Here */
    public function setWarrantStatus(Request $request)
    {
        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('approvals/overtime-warrants')->withErrors($v->fails());
        }

        $overtime_warrant  = OvertimeWarrant::find($cmd);
        if($overtime_warrant){
            $overtime_warrant->status=$request->status;
            $overtime_warrant->save();

            return redirect('approvals/overtime-warrants')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('approvals/overtime-warrants')->with([
                'message' => 'Warrant not found',
                'message_important'=>true
            ]);
        }

    }


    /* deleteWarrant  Function Start Here */
    public function deleteWarrant($id)
    {
        $overtime_warrant = OvertimeWarrant::find($id);
        if ($overtime_warrant) {
            $overtime_warrant->delete();

            return redirect('overtime/warrants')->with([
                'message' => 'Warrant Deleted Successfully'
            ]);
        } else {
            return redirect('overtime/warrants')->with([
                'message' => 'Warrant Not Found',
                'message_important' => true
            ]);
        }

    }

    /* viewWarrant  Function Start Here */
    public function viewWarrant($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 44)->first();
        $employee = Employee::where('role_id','!=','1')->get();
        $overtime_warrant = OvertimeWarrant::find($id);
        return view('admin.overtime.view-overtime-warrant', compact('overtime_warrant','employee','permcheck'));
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {
        $overtime_warrant=OvertimeWarrant::find($id);
        $employee = Employee::where('role_id','!=','1')->get();

        $pdf=WkPdf::loadView('admin.overtime.pdf-overtime-warrant',compact('overtime_warrant','employee'));

        return $pdf->stream('overtime_warrants.pdf');


    }


}
