<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\permission;
use App\StatusChange;
use App\Company;
use App\Notification;
use App\Project;
use App\ProjectNeeds;
use App\EmailTemplate;
use App\Employee;
use App\EmployeeRolesPermission;
use Illuminate\Support\Facades\Input;
use DB;
use WkPdf;

class StatusChangeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /* contract  Function Start Here */
    public function status_change()
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 102)->first();
        $comp_id = '';
        $proj_id = '';
        $emp_id = '';
        $des_id = '';


        $status_change = StatusChange::orderBy('id','asc')->get();
        $company = Company::all();
        $employee = Employee::where('role_id','!=','1')->get();
        $project = Project::all();
        $email_template = EmailTemplate::where('table_type','=','sys_status_change')->get();
        //Penomoran surat
        $date = date('d');
        $month = date('m');
        $year = date('Y');
        // $last_letter_number = DB::table('sys_status_change')->orderBy('id','asc')->value('letter_number');
        // if(  $last_letter_number ){
        //     $last_number = explode('/', $last_letter_number);
        // }
        // else{
        //     $last_number =0;
        // }
        
        // if($date=='01'){
        //     $number = 01;
        // } else{
        //     $number = 1 + $last_number[0];
        // }
        $last_letter_number = DB::table('sys_status_change')->where('letter_number','like','%'.get_roman_letters($month).'/'.$year.'%')->orderBy('id','desc')->value('letter_number');
        if($last_letter_number==''){
            $number = 1;
        } else{
            $last_number = explode('/', $last_letter_number);
            $number = 1 + $last_number[0];
        }
        $comp = Company::find(\Auth::user()->company);
        return view('admin.status_change.status_change', compact('des_id','comp_id','proj_id','emp_id','month','year','number','status_change','employee','company','comp','project','email_template','permcheck'));

    }

    /* getDesignation  Function Start Here */
    public function getDesignation(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $designation = Employee::where('id', $emp_id)->get();
            foreach ($designation as $d) {
                echo '<option value="' . $d->designation . '">' . $d->designation_name->department_name->department . ' (' . $d->designation_name->designation . ')</option>';
            }
        }
    }

    /* getDesignationNew  Function Start Here */
    public function getDesignationNew(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $designation = Employee::where('id', $emp_id)->first();
            echo '<option value="' . $designation->designation_name->id . '">' . $designation->designation_name->department_name->department . ' (' . $designation->designation_name->designation . ')</option>';
            if($designation->designation_name->career!=0){
                echo '<option value="' . $designation->designation_name->career_info->id . '">' . $designation->designation_name->career_info->department_name->department . ' (' . $designation->designation_name->career_info->designation . ')</option>';
            }
        }
    }

    /* getLocation  Function Start Here */
    public function getLocation(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $location = Employee::select('location')->where('id', $emp_id)->distinct()->get();
            foreach ($location as $l) {
                echo '<option value="' . $l->location . '">' . $l->location . '</option>';
            }
        }
    }

    /* getLocation  Function Start Here */
    public function getLocationNew(Request $request)
    {
        $proj_id = $request->proj_id;
        if ($proj_id) {
            $location = ProjectNeeds::where('id', $proj_id)->distinct()->get();
            foreach ($location as $l) {
                echo '<option value="' . $l->location . '">' . $l->location . '</option>';
            }
        }
    }

    /* getEmployeeCode  Function Start Here */
    public function getEmployeeCode(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee_code = Employee::where('id', $emp_id)->where('status','=','active')->get();
            foreach ($employee_code as $e) {
                echo '<option value="' . $e->employee_code . '">' . $e->employee_code . '</option>';
            }
        }
    }
    
    /* getEmployeeStatus  Function Start Here */
    public function getEmployeeStatus(Request $request)
    {
        $emp_id = $request->emp_id;
        if ($emp_id) {
            $employee = Employee::where('id', $emp_id)->where('status','=','active')->get();
            foreach ($employee as $e) {
                echo '<option value="' . $e->employee_status . '">' . $e->employee_status . '</option>';
            }
        }
    }

    /* getProject Function Start Here */
    public function getProject(Request $request)
    {

        $comp_id = $request->comp_id;
        if ($comp_id) {
            echo '<option value="0">Select Project</option>';
            $project = Project::where('company', $comp_id)->get();
            foreach ($project as $d) {
                echo '<option value="' . $d->id . '">' . $d->project_number . '</option>';
            }
        }
    }

    /* getEmployee Function Start Here */
    public function getEmployee(Request $request)
    {

        $proj_id = $request->proj_id;
        if ($proj_id) {
            echo '<option value="0">Select Employee</option>';
            $employee = Employee::where('project', $proj_id)->where('role_id','!=',1)->where('status','=','active')->get();
            foreach ($employee as $e) {
                echo '<option value="' . $e->id . '">' . $e->fname . ' ' . $e->lname . '</option>';
            }
        }
    }

    /* deleteStatusChange  Function Start Here */
    public function deleteStatusChange($id)
    {

        $status_change = StatusChange::find($id);
        if ($status_change) {
            $status_change->delete();

            return redirect('status-change')->with([
                'message' => 'Status Change Deleted Successfully'
            ]);
        } else {
            return redirect('status-change')->with([
                'message' => 'Status Change Not Found',
                'message_important' => true
            ]);
        }

    }


    /* addStatusChange  Function Start Here */
    public function addStatusChange(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'draft_letter' => 'required',
            
            'date' => 'required',
            'status_change_date' => 'required',
            'project_number' => 'required',
            'company_name' => 'required',
            'employee_code' => 'required',
            'employee_name' => 'required',
            'designation_old' => 'required',
            'location_old' => 'required',
            'employee_status_old' => 'required',
            'designation_new' => 'required',
            'employee_status_new' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('status-change')->withErrors($v->errors());
        }
        $tembusan = Input::get('tembusan');

        $draft_letter = Input::get('draft_letter');
        $letter_number = Input::get('letter_number');
        $date = Input::get('date');
        $date = get_date_format_inggris($date);
        $status_change_date = Input::get('status_change_date');
        $status_change_date = get_date_format_inggris($status_change_date);
        $project_number = Input::get('project_number');
        $company_name = Input::get('company_name');
        $employee_code = Input::get('employee_code');
        $employee_name = Input::get('employee_name');
        $designation_old = Input::get('designation_old');
        $location_old = Input::get('location_old');
        $employee_status_old = Input::get('employee_status_old');
        $designation_new = Input::get('designation_new');
        $location_new = Input::get('location_new');
        $employee_status_new = Input::get('employee_status_new');
        //Penomoran id
        $last_number_id = StatusChange::max('id');
        if($last_number_id==''){
            $number_id = 1;
        } else {
            $number_id = 1 + $last_number_id;
        }

        $status_change = new StatusChange();
        $status_change->id = $number_id;
        $status_change->tembusan = $tembusan;

        $status_change->draft_letter = $draft_letter;
        $status_change->letter_number = $letter_number;
        $status_change->date = $date;
        $status_change->status_change_date = $status_change_date;
        $status_change->project_number = $project_number;
        $status_change->company_name = $company_name;
        $status_change->employee_code = $employee_code;
        $status_change->employee_name = $employee_name;
        $status_change->designation_old = $designation_old;
        $status_change->location_old = $location_old;
        $status_change->employee_status_old = $employee_status_old;
        $status_change->designation_new = $designation_new;
        $status_change->location_new = $location_new;
        $status_change->employee_status_new = $employee_status_new;
        $status_change->status = 'draft';
        $status_change->save();
        
        $notification = new Notification();
        $notification->id_tag = $status_change->id;
        $notification->tag = 'status_change';
        $notification->title = 'Persetujuan Perubahan Status';
        $notification->description = 'Persetujuan Perubahan Status Pegawai '.$employee_code;
        $notification->show_date = date('Y-m-d');
        $notification->start_date = $status_change_date;
        $notification->end_date = $status_change_date;
        $notification->route = 'approvals/status-change';
        $notification->save();

        if ($status_change!='') {
            return redirect('status-change')->with([
                'message' => 'Status Change Added Successfully'
            ]);

        } else {
            return redirect('status-change')->with([
                'message' => 'Status Change Already Exist',
                'message_important' => true
            ]);
        }

        


    }


    /* updateStatusChange  Function Start Here */
    public function updateStatusChange(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'status_change_date' => 'required',
            'designation_new' => 'required',
            'employee_status_new' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('status-change')->withErrors($v->errors());
        }
        $status_change = StatusChange::find($cmd);
        $status_change_date = Input::get('status_change_date');
        $status_change_date = get_date_format_inggris($status_change_date);
        $designation_new = Input::get('designation_new');
        $location_new = Input::get('location_new');
        $employee_status_new = Input::get('employee_status_new');

        if ($status_change) {
            $status_change->status_change_date = $status_change_date;
            $status_change->designation_new = $designation_new;
            $status_change->location_new = $location_new;
            $status_change->employee_status_new = $employee_status_new;
            $status_change->status = 'draft';
            $status_change->save();
            
            $notification = Notification::where('tag','=','status_change')->where('id_tag','=',$cmd)->first();
        
            if($notification){
                $notification->show_date = date('Y-m-d');
                $notification->start_date = $status_change_date;
                $notification->end_date = $status_change_date;
                $notification->save();
            }

            return redirect('status-change')->with([
                'message' => 'Status Change Updated Successfully'
            ]);

        } else {
            return redirect('status-change')->with([
                'message' => 'Status Change Not Found',
                'message_important' => true
            ]);
        }
    }

    /* setStatusChangeStatus  Function Start Here */
    public function setStatusChangeStatus(Request $request)
    {

        $cmd=Input::get('cmd');
        $v=\Validator::make($request->all(),[
            'status'=>'required'
        ]);

        if($v->fails()){
            return redirect('status-change')->withErrors($v->fails());
        }

        $status_change  = StatusChange::find($cmd);
        if($status_change){
            $status_change->status=$request->status;
            $status_change->save();

            return redirect('status-change')->with([
                'message'=> language_data('Status updated successfully'),
            ]);

        }else{
            return redirect('status-change')->with([
                'message' => 'Status Change not found',
                'message_important'=>true
            ]);
        }

    }

    /* viewStatusChange  Function Start Here */
    public function viewStatusChange($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 102)->first();
        $status_change = StatusChange::find($id);
        return view('admin.status_change.status_change-view', compact('status_change','permcheck'));
    }

    /* editStatusChange  Function Start Here */
    public function editStatusChange($id)
    {
        $role_id = \Auth::user()->role_id;
        $permcheck = EmployeeRolesPermission::where('role_id', $role_id)->where('perm_id', 102)->first();
        $emp_id = '';
        $proj_id = '';
        $status_change = StatusChange::find($id);
        return view('admin.status_change.status_change-edit', compact('emp_id','proj_id','status_change','permcheck'));
    }

    /* downloadPdf  Function Start Here */
    public function downloadPdf($id)
    {

        $status_change = StatusChange::find($id);
        $draft_letter = EmailTemplate::find($status_change->draft_letter);

        $letter_number = $status_change->letter_number;
        $date = get_date_format($status_change->date);
        $status_change_date = get_date_format($status_change->status_change_date);
        $project_number = $status_change->project_info->project_number;
        $company_name = $status_change->company_info->company;
        $employee_code = $status_change->employee_code;
        $employee_name = $status_change->employee_info->fname . " " . $status_change->employee_info->lname;
        $designation_old = $status_change->designation_old_info->designation_old;
        $location_old = $status_change->location_old;
        $employee_status_old = $status_change->employee_status_old;
        $designation_new = $status_change->designation_new_info->designation_new;
        $location_new = $status_change->location_new;
        $employee_status_new = $status_change->employee_status_new;

        
        $data = array(
            'letter_number' => $letter_number,
            'date' => $date,
            'status_change_date' => $status_change_date,
            'project_number' => $project_number,
            'company_name' => $company_name,
            'employee_code' => $employee_code,
            'employee_name' => $employee_name,
            'designation_old' => $designation_old,
            'location_old' => $location_old,
            'employee_status_old' => $employee_status_old,
            'designation_new' => $designation_new,
            'location_new' => $location_new,
            'employee_status_new' => $employee_status_new
        );

        $template = $draft_letter->message;
        
        $message = _render($template, $data);

        $pdf=WkPdf::loadView('admin.status_change.pdf-status_change', compact('message','letter_number'));

        return $pdf->stream($letter_number.'.pdf');
    }
}

