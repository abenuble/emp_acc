<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Login*/
Route::get('/','UserController@login');
Route::post('user/get-login','UserController@getLogin');

/*Forgot Password*/
Route::get('forgot-password','UserController@forgotPassword');
Route::post('forgot-password-token','UserController@forgotPasswordToken');
Route::get('forgot-password-token-code/{token}','UserController@forgotPasswordTokenCode');


/*Logout*/
Route::get('user/logout','UserController@logout');

/*Dashboard*/
Route::get('dashboard','UserController@dashboard');
Route::post('dashboard','UserController@dashboard');
Route::post('dashboard-filtered','UserController@dashboard_filtered');

/*Profile Edit*/
Route::get('user/edit-profile','UserController@editProfile');
Route::post('user/post-user-personal-info','UserController@postUserPersonalInfo');
Route::post('user/update-user-avatar','UserController@updateUserAvatar');
Route::get('user/change-password','UserController@changePassword');
Route::post('user/update-user-password','UserController@updateUserPassword');

/*Designation Module*/
Route::get('designations','DesignationsController@designations');
Route::post('designations/add','DesignationsController@addDesignation');
Route::get('designations/edit/{id}','DesignationsController@editDesignation');
Route::post('designations/update','DesignationsController@updateDesignation');
Route::get('designations/downloadExcel/{type}','DesignationsController@downloadExcel');
Route::post('designations/importExcel','DesignationsController@importExcel');

/*Department Module*/
Route::get('departments','DepartmentController@departments');
Route::post('departments/add','DepartmentController@addDepartment');
Route::get('departments/edit/{id}','DepartmentController@editDepartment');
Route::post('departments/update','DepartmentController@updateDepartment');
Route::get('departments/downloadExcel/{type}','DepartmentController@downloadExcel');
Route::post('departments/importExcel','DepartmentController@importExcel');

/*Subcategory Module*/
Route::get('subcategories','subCategoryController@subcategories');
Route::post('subcategories/add','subCategoryController@addsubCategory');
Route::get('subcategories/edit/{id}','subCategoryController@editsubCategory');
Route::post('subcategories/update','subCategoryController@updatesubCategory');
Route::get('subcategories/downloadExcel/{type}','subCategoryController@downloadExcel');
Route::post('subcategories/importExcel','subCategoryController@importExcel');

/*Career Module*/
Route::get('careers','CareerController@careers');
Route::post('careers/add','CareerController@addCareer');
Route::get('careers/delete/{id}','CareerController@deleteCareer');
Route::get('careers/competence-delete/{id}','CareerController@deleteCareerCompetence');
Route::get('careers/view/{id}', 'CareerController@viewCareer');
Route::post('careers/update','CareerController@updateCareer');
Route::get('careers/downloadExcel/{type}','CareerController@downloadExcel');
Route::post('careers/importExcel','CareerController@importExcel');
Route::post('careers/importExcelCompetent','CareerController@importExcelCompetent');
Route::post('careers/get-designation','CareerController@getDesignation');
Route::post('careers/set-career-status','CareerController@setCareerStatus');
Route::post('careers/set-career-assessment-status','CareerController@setCareerAssessmentStatus');
Route::get('careers/management-employee','CareerController@careersEmployee');
Route::get('careers/view-employee/{id}','CareerController@viewCareerEmployee');
Route::get('careers/download-assessment-document/{id}','CareerController@downloadAssessmentDocument');
Route::post('careers/upload-assessment-document','CareerController@uploadAssessmentDocument');
Route::post('careers/management-employee/post-custom-search','CareerController@postCustomSearch');

/*Companies Module*/
Route::get('company-profile','CompaniesController@companyProfile');
Route::get('cetakWord','CompaniesController@cetakWord');
Route::post('cetakWord','CompaniesController@cetakWord');
Route::get('companies','CompaniesController@companies');
Route::get('companies/edit/{id}','CompaniesController@editCompany');
Route::post('companies/add','CompaniesController@addCompany');
Route::get('companies/delete/{id}','CompaniesController@deleteCompany');
Route::post('companies/update','CompaniesController@updateCompany');
Route::get('companies/view/{id}', 'CompaniesController@viewCompany');
Route::get('companies/downloadExcel/{type}','CompaniesController@downloadExcel');
Route::post('companies/importExcel','CompaniesController@importExcel');
Route::post('company-profile/organizational-structure-upload','CompaniesController@organizationalStructureUpload');
Route::post('company-profile/career-add','CompaniesController@CareerPathAdd');
Route::get('company-profile/career/{id}', 'CompaniesController@viewCareerPath');
Route::get('company-profile/edit-career/{id}', 'CompaniesController@editCareerPath');
Route::post('company-profile/edit-career-post','CompaniesController@editCareerPathPost');
Route::post('company-profile/jobdesc-add','CompaniesController@jobdescAdd');
Route::get('company-profile/jobdesc/{id}', 'CompaniesController@viewJobdesc');
Route::get('company-profile/edit-jobdesc/{id}', 'CompaniesController@editJobdesc');
Route::post('company-profile/edit-jobdesc-post','CompaniesController@editJobdescPost');
Route::post('company-profile/violation-add','CompaniesController@ViolationAdd');
Route::get('company-profile/violation/{id}', 'CompaniesController@viewViolation');
Route::get('company-profile/edit-violation/{id}', 'CompaniesController@editViolation');
Route::post('company-profile/edit-violation-post','CompaniesController@editViolationPost');
Route::post('company-profile/assessment-add','CompaniesController@AssessmentAdd');
Route::get('company-profile/assessment/{id}', 'CompaniesController@viewAssessment');
Route::get('company-profile/edit-assessment/{id}', 'CompaniesController@editAssessment');
Route::post('company-profile/edit-assessment-post','CompaniesController@editAssessmentPost');
Route::post('company-profile/edit-mission','CompaniesController@editMission');
Route::post('company-profile/edit-vision','CompaniesController@editVision');
Route::post('company-profile/edit-slogan','CompaniesController@editSlogan');
Route::post('company-profile/edit-sss','CompaniesController@editSSS');
Route::post('company-profile/edit-yekapepe','CompaniesController@editYekapepe');

/*Components Module*/
Route::get('components','ComponentsController@components');
Route::post('components/add','ComponentsController@addComponent');
Route::get('components/delete/{id}','ComponentsController@deleteComponent');
Route::post('components/update','ComponentsController@updateComponent');
Route::get('components/view/{id}', 'ComponentsController@viewComponent');
Route::post('components/set-component-status','ComponentsController@setComponentStatus');
Route::get('components/downloadExcel/xls','ComponentsController@downloadExcel');
Route::post('components/importExcel','ComponentsController@importExcel');

/*Unit Components Module*/
Route::get('unit-components','UnitComponentsController@unitComponent');
Route::post('unit-components/add','UnitComponentsController@addUnitComponent');
Route::get('unit-components/delete/{id}','UnitComponentsController@deleteUnitComponent');
Route::post('unit-components/update','UnitComponentsController@updateUnitComponent');
Route::get('unit-components/view/{id}', 'UnitComponentsController@viewUnitComponent');
Route::post('unit-components/set-unit-component-status','UnitComponentsController@setUnitComponentStatus');
Route::get('unit-components/downloadExcel/xls','UnitComponentsController@downloadExcel');
Route::post('unit-components/importExcel','UnitComponentsController@importExcel');
/*Payments Module*/
Route::get('payments/edit/{id}', 'PaymentController@editPayment');

Route::get('payments','PaymentController@payments');
Route::post('payments/add','PaymentController@addPayment');
Route::get('payments/delete/{id}','PaymentController@deletePayment');
Route::post('payments/update','PaymentController@updatePayment');
Route::get('payments/view/{id}', 'PaymentController@viewPayment');
Route::post('payments/set-payment-status','PaymentController@setPaymentStatus');
Route::post('payments/get-project','PaymentController@getProject');
Route::post('payments/get-project-number','PaymentController@getProjectNumber');
Route::post('payments/downloadExcel','PaymentController@downloadExcel');
Route::post('payments/importExcel','PaymentController@importExcel');
Route::post('payments/DownloadExcelAll','PaymentController@DownloadExcelAll');
Route::get('payments/print-payslip/{id}','PaymentController@printPayslip');
Route::get('payments/print-all-payslip/{id}','PaymentController@printAllPayslip');
Route::post('payments/DownloadComponent','PaymentController@DownloadComponent');

/* Schedule Module*/
Route::get('schedules','ScheduleController@schedules');
Route::post('schedules/add','ScheduleController@addSchedule');
Route::post('schedules/set-schedule-status','ScheduleController@setScheduleStatus');
Route::get('schedules/view/{id}','ScheduleController@viewSchedule');
Route::get('schedules/view-employee/{id}','ScheduleController@viewEmployeeSchedule');
Route::get('schedules/management-employee','ScheduleController@employeeSchedules');
Route::post('schedules/update','ScheduleController@updateSchedule');
Route::post('schedules/post-custom-search','ScheduleController@postCustomSearch');
Route::get('schedules/delete/{id}','ScheduleController@deleteSchedule');
Route::post('schedules/edit','ScheduleController@editSchedule');
Route::post('schedules/edit-schedule-type','ScheduleController@editScheduleType');

/*Appointments Module*/
Route::get('appointments','AppointmentController@appointments');
Route::post('appointments/add','AppointmentController@addAppointment');
Route::get('appointments/delete/{id}','AppointmentController@deleteAppointment');
Route::post('appointments/update','AppointmentController@updateAppointment');
Route::get('appointments/view/{id}', 'AppointmentController@viewAppointment');
Route::get('appointments/edit/{id}', 'AppointmentController@editAppointment');
Route::post('appointments/get-project','AppointmentController@getProject');
Route::post('appointments/get-designation','AppointmentController@getDesignation');
Route::post('appointments/get-designation-new','AppointmentController@getDesignationNew');
Route::post('appointments/get-location','AppointmentController@getLocation');
Route::post('appointments/get-location-new','AppointmentController@getLocationNew');
Route::post('appointments/get-employee','AppointmentController@getEmployee');
Route::post('appointments/get-employee-code','AppointmentController@getEmployeeCode');
Route::post('appointments/get-employee-status','AppointmentController@getEmployeeStatus');
Route::post('appointments/set-appointment-status','AppointmentController@setAppointmentStatus');
Route::get('appointments/downloadPdf/{id}','AppointmentController@downloadPdf');

/*mutations Module*/
Route::get('mutations','MutationController@mutations');
Route::post('mutations/add','MutationController@addMutation');
Route::get('mutations/delete/{id}','MutationController@deleteMutation');
Route::post('mutations/update','MutationController@updateMutation');
Route::get('mutations/view/{id}', 'MutationController@viewMutation');
Route::get('mutations/edit/{id}', 'MutationController@editMutation');
Route::post('mutations/get-project','MutationController@getProject');
Route::post('mutations/get-company-code','MutationController@getCompanyCode');
Route::post('mutations/get-company-code-new','MutationController@getCompanyCodeNew');
Route::post('mutations/get-designation','MutationController@getDesignation');
Route::post('mutations/get-designation-new','MutationController@getDesignationNew');
Route::post('mutations/get-location','MutationController@getLocation');
Route::post('mutations/get-location-new','MutationController@getLocationNew');
Route::post('mutations/get-employee','MutationController@getEmployee');
Route::post('mutations/get-employee-code','MutationController@getEmployeeCode');
Route::post('mutations/get-employee-status','MutationController@getEmployeeStatus');
Route::post('mutations/get-payment-type','MutationController@getPaymentType');
Route::post('mutations/get-payment-type-new','MutationController@getPaymentTypeNew');
Route::post('mutations/set-mutation-status','MutationController@setMutationStatus');
Route::get('mutations/downloadPdf/{id}','MutationController@downloadPdf');

/*status-change Module*/
Route::get('status-change','StatusChangeController@status_change');
Route::post('status-change/add','StatusChangeController@addStatusChange');
Route::get('status-change/delete/{id}','StatusChangeController@deleteStatusChange');
Route::post('status-change/update','StatusChangeController@updateStatusChange');
Route::get('status-change/view/{id}', 'StatusChangeController@viewStatusChange');
Route::get('status-change/edit/{id}', 'StatusChangeController@editStatusChange');
Route::post('status-change/get-project','StatusChangeController@getProject');
Route::post('status-change/get-designation','StatusChangeController@getDesignation');
Route::post('status-change/get-designation-new','StatusChangeController@getDesignationNew');
Route::post('status-change/get-location','StatusChangeController@getLocation');
Route::post('status-change/get-location-new','StatusChangeController@getLocationNew');
Route::post('status-change/get-employee','StatusChangeController@getEmployee');
Route::post('status-change/get-employee-code','StatusChangeController@getEmployeeCode');
Route::post('status-change/get-employee-status','StatusChangeController@getEmployeeStatus');
Route::post('status-change/set-mutation-status','StatusChangeController@setStatusChangeStatus');
Route::get('status-change/downloadPdf/{id}','StatusChangeController@downloadPdf');

/*warnings Module*/
Route::get('warnings','WarningController@warnings');
Route::post('warnings/add','WarningController@addWarning');
Route::get('warnings/delete/{id}','WarningController@deleteWarning');
Route::post('warnings/update','WarningController@updateWarning');
Route::get('warnings/view/{id}', 'WarningController@viewWarning');
Route::get('warnings/edit/{id}', 'WarningController@editWarning');
Route::post('warnings/get-project','WarningController@getProject');
Route::post('warnings/get-employee','WarningController@getEmployee');
Route::post('warnings/get-employee-warning-type','WarningController@getEmployeeWarningType');
Route::post('warnings/get-employee-code','WarningController@getEmployeeCode');
Route::post('warnings/set-warning-status','WarningController@setWarningStatus');
Route::get('warnings/downloadPdf/{id}','WarningController@downloadPdf');

/*contracts Module*/
Route::get('contracts','ContractController@contracts');
Route::post('contracts/add','ContractController@addContract');
Route::get('contracts/delete/{id}','ContractController@deleteContract');
Route::post('contracts/update','ContractController@updateContract');
Route::get('contracts/view/{id}', 'ContractController@viewContract');
Route::get('contracts/edit/{id}', 'ContractController@editContract');
Route::post('contracts/get-project','ContractController@getProject');
Route::post('contracts/get-designation','ContractController@getDesignationName');
Route::post('contracts/get-salary','ContractController@getSalary');
Route::post('contracts/get-location','ContractController@getLocation');
Route::post('contracts/get-employee','ContractController@getEmployee');
Route::post('contracts/get-project-name','ContractController@getProjectName');
Route::post('contracts/get-salary','ContractController@getSalary');
Route::post('contracts/get-attendance-allowance','ContractController@getAttendanceAllowance');
Route::post('contracts/set-contract-status','ContractController@setContractStatus');
Route::get('contracts/downloadPdf/{id}','ContractController@downloadPdf');
Route::get('contracts/downloadAllPdf/{id}','ContractController@downloadAllPdf');

/*surat-pengantar Module*/
Route::get('surat-pengantar','SuratPengantarController@surat_pengantar');
Route::post('surat-pengantar/add','SuratPengantarController@addSuratPengantar');
Route::get('surat-pengantar/delete/{id}','SuratPengantarController@deleteSuratPengantar');
Route::post('surat-pengantar/update','SuratPengantarController@updateSuratPengantar');
Route::get('surat-pengantar/view/{id}', 'SuratPengantarController@viewSuratPengantar');
Route::get('surat-pengantar/edit/{id}', 'SuratPengantarController@editSuratPengantar');
Route::post('surat-pengantar/get-job-project','SuratPengantarController@getProject');
Route::post('surat-pengantar/get-job-designation','SuratPengantarController@getDesignation');
Route::post('surat-pengantar/get-job-company','SuratPengantarController@getCompany');
Route::post('surat-pengantar/get-job-location','SuratPengantarController@getLocation');
Route::post('surat-pengantar/get-job-employee','SuratPengantarController@getEmployee');
Route::post('surat-pengantar/set-contract-status','SuratPengantarController@setSuratPengantarStatus');
Route::get('surat-pengantar/downloadPdf/{id}','SuratPengantarController@downloadPdf');
Route::get('surat-pengantar/downloadAllPdf/{id}','SuratPengantarController@downloadAllPdf');

/*endcontracts Module*/
Route::get('endcontracts','EndContractController@endcontracts');
Route::post('endcontracts/add','EndContractController@addEndContract');
Route::get('endcontracts/delete/{id}','EndContractController@deleteEndContract');
Route::post('endcontracts/update','EndContractController@updateEndContract');
Route::get('endcontracts/view/{id}', 'EndContractController@viewEndContract');
Route::get('endcontracts/edit/{id}', 'EndContractController@editEndContract');
Route::post('endcontracts/get-project','EndContractController@getProject');
Route::post('endcontracts/get-employee','EndContractController@getEmployee');
Route::post('endcontracts/set-endcontract-status','EndContractController@setEndContractStatus');
Route::get('endcontracts/downloadPdf/{id}','EndContractController@downloadPdf');
Route::get('endcontracts/downloadAllPdf/{id}','EndContractController@downloadAllPdf');

/*resigns Module*/
Route::get('resigns','ResignController@resigns');
Route::post('resigns/add','ResignController@addResign');
Route::get('resigns/delete/{id}','ResignController@deleteResign');
Route::post('resigns/update','ResignController@updateResign');
Route::get('resigns/view/{id}', 'ResignController@viewResign');
Route::get('resigns/view-post', 'ResignController@viewPost');
Route::get('resigns/edit/{id}', 'ResignController@editResign');
Route::post('resigns/get-project','ResignController@getProject');
Route::post('resigns/get-employee','ResignController@getEmployee');
Route::post('resigns/get-employee-code','ResignController@getEmployeeCode');
Route::post('resigns/get-employee-doj','ResignController@getEmployeeDate');
Route::post('resigns/set-resign-status','ResignController@setResignStatus');
Route::get('resigns/downloadPdf/{id}','ResignController@downloadPdf');
Route::get('resigns/download-file-resign-letter/{id}','ResignController@downloadFileResign');
Route::get('resigns/download-file-acceptance-resign-letter/{id}','ResignController@downloadFileAcceptance');
Route::get('resigns/previewPdf/{id}','ResignController@previewPdf');

/*terminations Module*/
Route::get('terminations','PhkController@terminations');
Route::post('terminations/add','PhkController@addTermination');
Route::get('terminations/delete/{id}','PhkController@deleteTermination');
Route::post('terminations/update','PhkController@updateTermination');
Route::get('terminations/view/{id}', 'PhkController@viewTermination');
Route::get('terminations/edit/{id}', 'PhkController@editTermination');
Route::post('terminations/get-project','PhkController@getProject');
Route::post('terminations/get-employee','PhkController@getEmployee');
Route::post('terminations/get-employee-code','PhkController@getEmployeeCode');
Route::post('terminations/set-termination-status','PhkController@setTerminationStatus');
Route::get('terminations/downloadPdf/{id}','PhkController@downloadPdf');

/*Approval Module*/
Route::get('approvals/projects','ApprovalController@projects');
Route::get('approvals/view-project/{id}','ApprovalController@ViewProject');
Route::post('approvals/set-project-status','ApprovalController@setProjectStatus');
Route::get('approvals/expenses','ApprovalController@expenses');
Route::get('approvals/view-expense/{id}','ApprovalController@ViewExpense');
Route::post('approvals/set-expense-status','ApprovalController@setExpenseStatus');
Route::get('approvals/contracts','ApprovalController@contracts');
Route::get('approvals/view-contract/{id}','ApprovalController@ViewContract');
Route::post('approvals/set-contract-status','ApprovalController@setContractStatus');
Route::get('approvals/surat-pengantar','ApprovalController@surat_pengantar');
Route::get('approvals/view-surat-pengantar/{id}','ApprovalController@ViewSuratPengantar');
Route::post('approvals/set-surat-pengantar-status','ApprovalController@setSuratPengantarStatus');
Route::get('approvals/appointments','ApprovalController@appointments');
Route::get('approvals/view-appointment/{id}','ApprovalController@ViewAppointment');
Route::post('approvals/set-appointment-status','ApprovalController@setAppointmentStatus');
Route::get('approvals/leave','ApprovalController@leave');
Route::get('approvals/view-leave/{id}','ApprovalController@ViewLeave');
Route::post('approvals/set-leave-status','ApprovalController@setLeaveStatus');
Route::get('approvals/mutations','ApprovalController@mutations');
Route::get('approvals/view-mutation/{id}','ApprovalController@ViewMutation');
Route::post('approvals/set-mutation-status','ApprovalController@setMutationStatus');
Route::get('approvals/status-change','ApprovalController@status_change');
Route::get('approvals/view-status-change/{id}','ApprovalController@ViewStatusChange');
Route::post('approvals/set-status-change-status','ApprovalController@setStatusChangeStatus');
Route::get('approvals/warnings','ApprovalController@warnings');
Route::get('approvals/view-warning/{id}','ApprovalController@ViewWarning');
Route::post('approvals/set-warning-status','ApprovalController@setWarningStatus');
Route::get('approvals/resigns','ApprovalController@resigns');
Route::get('approvals/view-resign/{id}','ApprovalController@ViewResign');
Route::post('approvals/set-resign-status','ApprovalController@setResignStatus');
Route::get('approvals/terminations','ApprovalController@terminations');
Route::get('approvals/view-termination/{id}','ApprovalController@ViewTermination');
Route::post('approvals/set-termination-status','ApprovalController@setTerminationStatus');
Route::get('approvals/endcontracts','ApprovalController@endcontracts');
Route::get('approvals/view-endcontract/{id}','ApprovalController@ViewEndContract');
Route::post('approvals/set-endcontract-status','ApprovalController@setEndContractStatus');
Route::get('approvals/overtime-warrants','ApprovalController@warrants');
Route::get('approvals/view-overtime-warrant/{id}','ApprovalController@ViewWarrant');
Route::post('approvals/set-overtime-status','ApprovalController@setWarrantStatus');
Route::get('approvals/payments','ApprovalController@payments');
Route::get('approvals/view-payment/{id}','ApprovalController@ViewPayment');
Route::post('approvals/set-payment-status','ApprovalController@setPaymentStatus');
Route::get('approvals/procurements','ApprovalController@procurements');
Route::get('approvals/view-procurement/{id}','ApprovalController@ViewProcurement');
Route::post('approvals/set-procurement-status','ApprovalController@setProcurementStatus');
Route::get('approvals/realizations','ApprovalController@realizations');
Route::get('approvals/view-realization/{id}','ApprovalController@ViewRealization');
Route::post('approvals/set-realization-status','ApprovalController@setRealizationStatus');
Route::get('approvals/payouts','ApprovalController@payouts');
Route::get('approvals/view-payout/{id}','ApprovalController@ViewPayout');
Route::post('approvals/set-payout-status','ApprovalController@setPayoutStatus');
Route::get('approvals/pkb-letters','ApprovalController@pkbletters');
Route::get('approvals/out-letters','ApprovalController@outletters');
Route::get('approvals/view-pkb-letter/{id}','ApprovalController@ViewPkbletter');
Route::post('approvals/set-pkb-letter-status','ApprovalController@setPkbletterStatus');
Route::get('approvals/view-out-letter/{id}','ApprovalController@ViewOutletter');
Route::post('approvals/set-out-letter-status','ApprovalController@setOutletterStatus');
Route::get('approvals/assessments/{id}','ApprovalController@employeeAssessments');
Route::get('approvals/assessment-aspects/{id}','ApprovalController@viewAssessment');
Route::get('approvals/employeeassesment','ApprovalController@Reports');
Route::post("approvals/setEmployeeAssesmentStatus","ApprovalController@setEmployeeAssesmentStatus");

/*legal document Module*/
Route::get('legaldocuments','LegalDocumentController@legaldocuments');
Route::post('legaldocuments/add','LegalDocumentController@addLegalDocument');
Route::get('legaldocuments/delete/{id}','LegalDocumentController@deleteLegalDocument');
Route::post('legaldocuments/update','LegalDocumentController@updateLegalDocument');
Route::get('legaldocuments/view/{id}', 'LegalDocumentController@viewLegalDocument');
Route::get('legaldocuments/download-file/{id}','LegalDocumentController@downloadFile');


/*Vehicle Tax Module*/
Route::get('vehicletax','VehicleTaxController@vehicletax');
Route::post('vehicletax/add','VehicleTaxController@addVehicleTax');
Route::get('vehicletax/delete/{id}','VehicleTaxController@deleteVehicleTax');
Route::post('vehicletax/update','VehicleTaxController@updateVehicleTax');
Route::get('vehicletax/view/{id}', 'VehicleTaxController@viewVehicleTax');

/*Cashflow Module*/
Route::get('cashflow','CashflowController@Cashflow');
Route::post('cashflow/add','CashflowController@addCashflow');
Route::get('cashflow/delete/{id}','CashflowController@deleteCashflow');
Route::post('cashflow/update','CashflowController@updateCashflow');
Route::get('cashflow/view/{id}', 'CashflowController@viewCashflow');
Route::post('cashflow/set-cashflow-status','CashflowController@setCashflowStatus');
Route::get('cashflow/downloadExcel/{type}','CashflowController@downloadExcel');
Route::post('cashflow/importExcel','CashflowController@importExcel');

/*Banks Module*/
Route::get('banks','BankController@Bank');
Route::post('banks/add','BankController@addBank');
Route::get('banks/delete/{id}','BankController@deleteBank');
Route::post('banks/update','BankController@updateBank');
Route::get('banks/view/{id}', 'BankController@viewBank');
Route::post('banks/get-coa','BankController@getCoa');
Route::post('banks/set-bank-status','BankController@setBankStatus');
Route::get('banks/downloadExcel/{type}','BankController@downloadExcel');
Route::post('banks/importExcel','BankController@importExcel');

/*coa Module*/
Route::get('coa','CoaController@Coa');
Route::post('coa/add','CoaController@addCoa');
Route::get('coa/delete/{id}','CoaController@deleteCoa');
Route::post('coa/update','CoaController@updateCoa');
Route::get('coa/view/{id}', 'CoaController@viewCoa');
Route::post('coa/set-coa-status','CoaController@setCoaStatus');
Route::get('coa/downloadExcel/{type}','CoaController@downloadExcel');
Route::post('coa/importExcel','CoaController@importExcel');

/*Cash Module*/
Route::get('cash','CashController@cash');
Route::post('cash/add','CashController@addCash');
Route::get('cash/delete/{id}','CashController@deleteCash');
Route::post('cash/update','CashController@updateCash');
Route::get('cash/view/{id}', 'CashController@viewCash');
Route::post('cash/set-cash-status','CashController@setCashStatus');
Route::post('cash/get-coa','CashController@getCoa');
Route::get('cash/downloadExcel/{type}','CashController@downloadExcel');
Route::post('cash/importExcel','CashController@importExcel');

/*AccountingBkp Module*/
Route::get('accountings/bkp','AccountingBkpController@bkp');
Route::get('accountings/bkp/add','AccountingBkpController@addBkp');
Route::post('accountings/bkp/add-post','AccountingBkpController@addPostBkp');
Route::get('accountings/bkp/delete/{id}','AccountingBkpController@deleteBkp');
Route::post('accountings/bkp/update','AccountingBkpController@updateBkp');
Route::get('accountings/bkp/view/{id}', 'AccountingBkpController@viewBkp');
Route::post('accountings/bkp/set-bkp-status','AccountingBkpController@setBkpStatus');
Route::get('accountings/bkp/downloadExcel/{type}','AccountingBkpController@downloadExcel');
Route::post('accountings/bkp/importExcel','AccountingBkpController@importExcel');
Route::post('accountings/bkp/get-client','AccountingBkpController@getCompany');
Route::post('accountings/bkp/get-procurement','AccountingBkpController@getProcurement');
Route::post('accountings/bkp/get-coa','AccountingBkpController@getCoa');
Route::post('accountings/bkp/get-cash','AccountingBkpController@getCash');
Route::post('accountings/bkp/get-bank','AccountingBkpController@getBank');
Route::post('accountings/bkp/get-detail-procurement','AccountingBkpController@getDetailProcurement');
Route::post('accountings/bkp/get-payment','AccountingBkpController@getPayment');
Route::post('accountings/bkp/get-detail-payment','AccountingBkpController@getDetailPayment');
Route::post('accountings/bkp/get-coa-component','AccountingBkpController@getCoaComponent');

/*AccountingBkm Module*/
Route::get('accountings/bkm','AccountingBkmController@bkm');
Route::get('accountings/bkm/add','AccountingBkmController@addBkm');
Route::post('accountings/bkm/add-post','AccountingBkmController@addPostBkm');
Route::get('accountings/bkm/delete/{id}','AccountingBkmController@deleteBkm');
Route::post('accountings/bkm/update','AccountingBkmController@updateBkm');
Route::get('accountings/bkm/view/{id}', 'AccountingBkmController@viewBkm');
Route::post('accountings/bkm/set-bkm-status','AccountingBkmController@setBkmStatus');
Route::get('accountings/bkm/downloadExcel/{type}','AccountingBkmController@downloadExcel');
Route::post('accountings/bkm/importExcel','AccountingBkmController@importExcel');
Route::post('accountings/bkm/get-client','AccountingBkmController@getCompany');
Route::post('accountings/bkm/get-expense','AccountingBkmController@getExpense');
Route::post('accountings/bkm/get-procure','AccountingBkmController@getProcure');
Route::post('accountings/bkm/get-coa','AccountingBkmController@getCoa');
Route::post('accountings/bkm/get-cash','AccountingBkmController@getCash');
Route::post('accountings/bkm/get-bank','AccountingBkmController@getBank');
Route::post('accountings/bkm/get-detail-expense','AccountingBkmController@getDetailExpense');
Route::post('accountings/bkm/get-detail-procure','AccountingBkmController@getDetailProcure');


/*Journal Manual Module*/
Route::get('accountings/journal-manual','JournalManualController@JournalManual');
Route::get('accountings/journal-manual/add','JournalManualController@addJournalManual');
Route::post('accountings/journal-manual/add-post','JournalManualController@addPostJournalManual');
Route::get('accountings/journal-manual/view/{id}', 'JournalManualController@viewJournalManual');
Route::post('accountings/journal-manual/get-coa','JournalManualController@getCoa');
Route::post('accountings/journal-manual/get-expense-detail','JournalManualController@getExpenseDetail');


/*Account Setting Module*/
Route::get('accountings/account-setting','AccountSettingController@AccountSetting');
Route::post('accountings/account-setting/update-post','AccountSettingController@updatePostAccountSetting');

/*AccountingBank Module*/
Route::get('accountings/bank','AccountingBankController@bank');
Route::get('accountings/bank/add','AccountingBankController@addBank');
Route::post('accountings/bank/add-post','AccountingBankController@addPostBank');
Route::get('accountings/bank/view/{id}', 'AccountingBankController@viewBank');
Route::post('accountings/bank/get-bkm-detail','AccountingBankController@getDetailBkm');
Route::post('accountings/bank/get-bkp-detail','AccountingBankController@getDetailBkp');

/*AccountingCash Module*/
Route::get('accountings/cash','AccountingCashController@cash');
Route::get('accountings/cash/add','AccountingCashController@addCash');
Route::post('accountings/cash/add-post','AccountingCashController@addPostCash');
Route::get('accountings/cash/view/{id}', 'AccountingCashController@viewCash');
Route::post('accountings/cash/get-bkm-detail','AccountingCashController@getDetailBkm');
Route::post('accountings/cash/get-bkp-detail','AccountingCashController@getDetailBkp');

/*AccountingCheck Module*/
Route::get('accountings/check','AccountingCheckController@check');
Route::get('accountings/check/add','AccountingCheckController@addCheck');
Route::post('accountings/update-giro','AccountingCheckController@updateGiro');
Route::post('accountings/check/add-post','AccountingCheckController@addPostCheck');
Route::get('accountings/check/view/{id}', 'AccountingCheckController@viewCheck');

/*Procurement Module*/
Route::get('procurements','ProcurementController@procurements');
Route::post('procurements/add','ProcurementController@addProcurement');
Route::get('procurements/delete/{id}','ProcurementController@deleteProcurement');
Route::post('procurements/update','ProcurementController@updateProcurement');
Route::get('procurements/view/{id}', 'ProcurementController@viewProcurement');
Route::get('procurements/wizard/{id}', 'ProcurementController@wizardProcurement');
Route::get('procurements/wizard2/{id}', 'ProcurementController@wizard2Procurement');
Route::get('procurements/realization/{id}', 'ProcurementController@realizationProcurement');
Route::get('procurements/payout/{id}', 'ProcurementController@payoutProcurement');
Route::post('procurements/set-procurements-status','ProcurementController@setProcurementStatus');
Route::post('procurements/get-project','ProcurementController@getProject');
Route::post('procurements/get-project-item','ProcurementController@getProjectItem');
Route::post('procurements/get-project-item-detail','ProcurementController@getProjectItemDetail');
Route::get('procurements/download-payout-file/{id}','ProcurementController@downloadPayout');
Route::get('procurements/downloadExcel/{type}','ProcurementController@downloadExcel');
Route::post('procurements/importExcel','ProcurementController@importExcel');

Route::get('procurements/print/realization/{id}', 'ProcurementController@printRealizationProcurement');
Route::get('procurements/print/payout/{id}', 'ProcurementController@printPayoutProcurement');
Route::get('procurements/print/procure/{id}', 'ProcurementController@printProcureProcurement');

/*Project Module*/
Route::get('projects','ProjectController@Project');
Route::post('projects/add','ProjectController@addProject');
Route::get('projects/delete/{id}','ProjectController@deleteProject');
Route::post('projects/update','ProjectController@updateProject');
Route::post('projects/updateProjectDetail','ProjectController@updateProjectDetail');
Route::get('projects/view/{id}', 'ProjectController@viewProject');
Route::get('projects/view-expense/{id}', 'ProjectController@viewProjectExpense');
Route::get('projects/view-vehicle/{id}', 'ProjectController@viewProjectVehicle');
Route::get('projects/delete-project-doc/{id}','ProjectController@deleteProjectDoc');
Route::get('projects/download-project-document/{id}','ProjectController@downloadProjectDocument');
Route::post('projects/add-document','ProjectController@addDocument');
Route::post('projects/set-payment-type','ProjectController@setPaymentType');
Route::Post('projects/download-employee-data','ProjectController@downloadExcel');
Route::Post('projects/get-component','ProjectController@getComponent');
Route::Post('projects/add-expense','ProjectController@addExpense');
Route::Post('projects/update-expense','ProjectController@updateExpense');
Route::Post('projects/add-item','ProjectController@addItem');
Route::post('projects/set-vehicle','ProjectController@setVehicle');
Route::get('projects/edit-payment/{id}','ProjectController@editPayment');
Route::get('projects/view-payment/{id}','ProjectController@viewPayment');
Route::get('projects/edit-vehicle/{id}','ProjectController@editVehicle');
Route::post('projects/get-detail','ProjectController@getdetailproject');
Route::post('projects/get-designationname','ProjectController@getdesignationname');
Route::post('projects/get-area-code','ProjectController@getAreaCode');
Route::post('projects/closing','ProjectController@closingProject');
Route::post('projects/downloadExcelProject','ProjectController@downloadExcelProject');
Route::post('projects/downloadExcelProjectExpense','ProjectController@downloadExcelProjectExpense');
Route::get('projects/download-expense/{id}', 'ProjectController@DownloadProjectExpense');
Route::get('projects/download-expense-kwitansi/{id}', 'ProjectController@DownloadProjectExpenseKwitansi');


Route::get('projects/downloadExcel/xls','ProjectController@downloadExcelTemp');
Route::get('projects/downloadExcel/item','ProjectController@downloadExcelItem');
Route::post('projects/importExcelItem/{id}','ProjectController@importExcelItem');
Route::post('projects/importExcel','ProjectController@importExcel');

Route::post('projects/checkexist', 'ProjectController@checkExist');


/*surat refrensi*/
Route::get('references-letters','ReferenceLetterController@ReferenceLetter');
Route::get('reference-letter/view/{id}','ReferenceLetterController@ViewReferenceLetter');

/*Employee Module*/
Route::get('employees/all','EmployeeController@allEmployees');
Route::get('employees/add','EmployeeController@addEmployee');
Route::post('employees/add-employee-post','EmployeeController@addEmployeePost');
Route::post('employees/add-employee-dependents','EmployeeController@addDependents');
Route::get('employees/view/{id}','EmployeeController@viewEmployee');
Route::post('employees/post-employee-personal-info','EmployeeController@postEmployeePersonalInfo');
Route::post('employees/update-employee-avatar','EmployeeController@updateEmployeeAvatar');
Route::post('employee/get-designation','EmployeeController@getDesignation');
Route::post('employee/add-bank-account','EmployeeController@addBankInfo');
Route::get('employee/delete-bank-account/{id}','EmployeeController@deleteBankAccount');
Route::post('employee/add-document','EmployeeController@addDocument');
Route::get('employee/download-employee-document/{id}','EmployeeController@downloadEmployeeDocument');
Route::get('employee/delete-employee-doc/{id}','EmployeeController@deleteEmployeeDoc');
Route::get('employee/delete-employee/{id}','EmployeeController@deleteEmployee');
Route::get('employee/delete-dependent/{id}','EmployeeController@deleteEmployeeDependent');
Route::get('employee/downloadExcel/{type}','EmployeeController@downloadExcel');
Route::post('employee/importExcel','EmployeeController@importExcel');
Route::post('employee/get-department','EmployeeController@getDepartment');
Route::post('employee/get-career','EmployeeController@getCareer');
Route::post('employee/get-project','EmployeeController@getProject');
Route::post('employee/get-employee-type','EmployeeController@getEmployeeType');
Route::Post('employees/download-employee-data','EmployeeController@downloadEmployeeExcel');
Route::Post('employees/download-id-card','EmployeeController@downloadIdCard');
Route::get('employee/download-employee-cuti-document/{id}','EmployeeController@downloadEmployeeCutiDocument');
Route::post('employee/add-cuti','EmployeeController@addCutiDocument');

/*Employee Resign Module*/
Route::get('employee-resigns','EmployeeResignController@employeeResigns');
Route::get('employee-resigns/block/{id}','EmployeeResignController@blockEmployee');

/*Job Application Module*/
Route::get('jobs','JobController@jobs');
Route::post('jobs/post-new-job','JobController@postNewJob');
Route::get('jobs/edit/{id}','JobController@editJob');
Route::post('jobs/job-edit-post','JobController@postEditJob');
Route::post('jobs/add-applicant','JobController@addApplicant');
Route::get('jobs/delete-job/{id}','JobController@deleteJob');
Route::get('jobs/view-applicant/{id}','JobController@viewApplicant');
Route::get('jobs/download-resume/{type}/{id}','JobController@downloadResume');
Route::get('jobs/download-photo/{id}','JobController@downloadPhoto');
Route::get('jobs/download-psychotest/{id}','JobController@downloadPsychotest');
Route::get('jobs/download-interview/{id}','JobController@downloadInterview');
Route::get('jobs/download-healthtest/{id}','JobController@downloadhealthtest');
Route::post('jobs/add-resume','JobController@addResume');
Route::get('jobs/delete-application/{id}','JobController@deleteApplication');
Route::post('jobs/set-applicant-status','JobController@setApplicantStatus');
Route::post('jobs/set-psychotest-status','JobController@setApplicantPsychotestStatus');
Route::post('jobs/set-interview-status','JobController@setApplicantInterviewStatus');
Route::post('jobs/set-healthtest-status','JobController@setApplicantHealthtestStatus');
Route::post('jobs/set-waiting-list-status','JobController@setApplicantWaitingListStatus');
Route::get('jobs/ajax-generalApp','JobController@generalApplicantRetrieve');
Route::get('jobs/general-applicants','JobController@generalApplicants');
Route::post('jobs/add-general-applicant','JobController@addGeneralApplicant');
Route::get('jobs/delete-gen-application/{id}','JobController@deleteGeneralApplication');
Route::get('jobs/view-job-applicant/{id}','JobController@viewJobApplicant');
Route::get('jobs/view-general-applicant/{id}','JobController@viewGeneralApplicant');
Route::get('jobs/view-psychotest-applicant/{id}','JobController@viewPsychotestApplicant');
Route::get('jobs/view-interview-applicant/{id}','JobController@viewInterviewApplicant');
Route::get('jobs/view-healthtest-applicant/{id}','JobController@viewHealthtestApplicant');
Route::get('jobs/view-waiting-applicant/{id}','JobController@viewWaitingApplicant');
Route::get('jobs/downloadExcelGeneral/{type}','JobController@downloadExcelGeneral');
Route::post('jobs/importExcelGeneral','JobController@importExcelGeneral');
Route::get('jobs/downloadExcelPelamar/{type}','JobController@downloadExcelPelamar');
Route::post('jobs/importExcelPekerja','JobController@importExcelPekerja');
Route::post('jobs/update-general-applicant','JobController@updateGeneralApplicant');


/*Setting Module*/
Route::get('settings/general','SettingController@general');
Route::post('settings/post-general-setting','SettingController@postGeneralSetting');
Route::post('settings/post-office-time','SettingController@postOfficeTime');
Route::post('settings/post-expense-title','SettingController@postExpenseTitle');
Route::post('settings/post-leave-type','SettingController@postLeaveType');
Route::post('settings/post-default-schedule','SettingController@postDefaultSchedule');
Route::post('settings/post-default-form','SettingController@postDefaultForm');
Route::post('settings/post-award-name','SettingController@postAwardName');
Route::post('settings/post-job-file-extension','SettingController@postJobFileExtension');
Route::get('settings/localization','SettingController@localization');
Route::post('settings/localization-post','SettingController@localizationPost');
Route::get('settings/language-settings','SettingController@languageSettings');
Route::post('settings/language-settings/add','SettingController@addLanguage');
Route::get('settings/language-settings-translate/{lid}','SettingController@translateLanguage');
Route::post('settings/language-settings-translate-post','SettingController@translateLanguagePost');
Route::get('settings/language-settings-manage/{lid}','SettingController@languageSettingsManage');
Route::post('settings/language-settings-manage-post','SettingController@languageSettingManagePost');
Route::get('settings/language-settings/delete/{lid}','SettingController@deleteLanguage');


/*Language Change*/
Route::get('language/change/{id}','SettingController@languageChange');

/*Expense Settings*/
Route::get('settings/delete-expense/{id}','SettingController@deleteExpense');
Route::post('settings/update-expense-title','SettingController@updateExpenseTitle');

/*Leave Settings*/
Route::get('settings/delete-leave/{id}','SettingController@deleteLeave');
Route::post('settings/update-leave-type','SettingController@updateLeaveType');


/*Award Settings*/
Route::get('settings/delete-award/{id}','SettingController@deleteAward');
Route::post('settings/update-award-name','SettingController@updateAwardName');


/*Award Module*/
Route::get('award','AwardController@award');
Route::post('award/post-new-award','AwardController@postNewAward');
Route::get('award/delete-award/{id}','AwardController@deleteAward');
Route::get('award/edit/{id}','AwardController@editAward');
Route::post('award/award-edit-post','AwardController@postEditAward');


/*Calendar Module*/
Route::get('calendar','CalendarController@calendar');
Route::get('calendar/ajax-event-calendar','CalendarController@eventCalendar');
Route::get('calendar/add','CalendarController@addCalendar');
Route::post('calendar/post-add-calendar','CalendarController@postAddCalendar');
Route::get('calendar/view-calendar/{id}','CalendarController@viewCalendar');
Route::get('calendar/delete-calendar/{id}','CalendarController@deleteCalendar');
Route::post('calendar/post-edit-calendar','CalendarController@postEditCalendar');
Route::get('calendar/day-schedule','CalendarController@calendar');
Route::post('calendar/send-sms','CalendarController@sendSMSNotification');


/*Leave Module*/
Route::get('leave','LeaveController@leave');
Route::get('leave/edit/{id}','LeaveController@viewLeave');
Route::post('leave/post-job-status','LeaveController@postJobStatus');
Route::post('leave/post-new-leave','LeaveController@postNewLeave');
Route::get('leave/delete-leave-application/{id}','LeaveController@deleteLeaveApplication');
Route::post('leave/get-leave-quota','LeaveController@getLeaveQuota');
Route::post('leave/get-remaining-leave','LeaveController@getRemainingLeave');
Route::get('leave/download-file/{id}','LeaveController@downloadFile');

/*Notice Module*/
Route::get('notice-board','NoticeController@noticeBoard');
Route::post('notice-board/post-new-notice','NoticeController@postNewNotice');
Route::get('notice-board/delete-notice/{id}','NoticeController@deleteNotice');
Route::get('notice-board/edit/{id}','NoticeController@editNotice');
Route::post('notice-board/edit-notice-post','NoticeController@postEditNotice');

/*Notification Module*/
Route::get('notifications/all','NotificationController@notifications');

/*Expense Module*/
Route::get('expense','ExpenseController@expense');
Route::post('expense/post-new-expense','ExpenseController@postExpense');
Route::get('expense/download-bill-copy/{id}','ExpenseController@downloadBillCopy');
Route::get('expense/delete-expense/{id}','ExpenseController@deleteExpense');
Route::get('expense/edit/{id}','ExpenseController@editExpense');
Route::post('expense/expense-edit-post','ExpenseController@postEditExpense');


/*Task Module*/
Route::get('task','TaskController@task');
Route::post('task/post-new-task','TaskController@postNewTask');
Route::get('task/edit/{id}','TaskController@editTask');
Route::post('task/task-edit-post','TaskController@postEditTask');
Route::get('task/view/{id}','TaskController@viewTask');
Route::post('task/task-basic-info-post','TaskController@postBasicTaskInfo');
Route::post('task/post-task-comments','TaskController@postTaskComments');
Route::post('task/post-task-files','TaskController@postTaskFiles');
Route::get('task/download-file/{id}','TaskController@downloadTaskFIle');
Route::get('task/delete-task-file/{id}','TaskController@deleteTaskFIle');
Route::get('task/delete-task/{id}','TaskController@deleteTask');

/*Attendance Module */
Route::get('attendance/report','AttendanceController@report');
Route::get('attendance/update','AttendanceController@update');
Route::post('attendance/post-update-attendance','AttendanceController@postUpdateAttendance');
Route::post('attendance/get-designation','AttendanceController@getDesignation');
Route::post('attendance/post-custom-search','AttendanceController@postCustomSearch');
Route::get('attendance/edit/{id}','AttendanceController@editAttendance');
Route::get('attendance/overtime/{id}','AttendanceController@overtimeAttendance');
Route::post('attendance/post-edit-attendance','AttendanceController@postEditAttendance');
Route::post('attendance/edit-leave','AttendanceController@editAttendanceLeave');
Route::post('attendance/post-overtime-attendance','AttendanceController@postOvertimeAttendance');
Route::get('attendance/delete-attendance/{id}','AttendanceController@deleteAttendance');
Route::post('attendance/set-overtime','AttendanceController@postSetOvertime');
Route::get('attendance/view-attendance/{id}','AttendanceController@viewInternal');
Route::post('attendance/post-view-internal-custom-search','AttendanceController@postViewInternalCustomSearch');
Route::get('attendance/downloadExcel/{type}','AttendanceController@downloadExcel');
Route::post('attendance/importExcel','AttendanceController@importExcel');

Route::get('attendance/outsource-report','AttendanceController@reportOutsource');
Route::post('attendance/outsource-post-custom-search','AttendanceController@postOutsourceCustomSearch');
Route::post('attendance/get-company','AttendanceController@getCompany');
Route::post('attendance/get-location','AttendanceController@getLocation');
Route::get('attendance/outsource-update','AttendanceController@updateOutsource');
Route::post('attendance/outsource-update-detail','AttendanceController@updateOutsourceDetail');
Route::post('attendance/post-update-outsource-attendance','AttendanceController@postUpdateOutsourceAttendance');
Route::get('attendance/view-outsource/{id}','AttendanceController@viewOutsource');
Route::get('attendance/overtime-outsource/{id}','AttendanceController@overtimeOutsourceAttendance');
Route::post('attendance/post-overtime-outsource-attendance','AttendanceController@postOvertimeOutsourceAttendance');
Route::get('attendance/outsource-downloadExcel/{type}','AttendanceController@downloadExcelOutsource');
Route::post('attendance/outsource-importExcel','AttendanceController@importExcelOutsource');
Route::post('attendance/get-project','AttendanceController@getProject');
Route::post('attendance/post-view-outsource-custom-search','AttendanceController@postViewOutsourceCustomSearch');


/*Support Ticket Module*/
Route::get('support-tickets/all','SupportTicketController@all');
Route::get('support-tickets/create-new','SupportTicketController@createNew');
Route::get('support-tickets/view-ticket/{id}','SupportTicketController@viewTicket');
Route::get('support-tickets/department','SupportTicketController@department');
Route::get('support-tickets/view-department/{id}','SupportTicketController@viewDepartment');
Route::get('support-tickets/ticket-department/{id}','SupportTicketController@ticketDepartment');
Route::get('support-tickets/ticket-status/{id}','SupportTicketController@ticketStatus');
Route::post('support-tickets/post-department','SupportTicketController@postDepartment');
Route::post('support-tickets/update-department','SupportTicketController@updateDepartment');
Route::post('support-tickets/post-ticket','SupportTicketController@postTicket');
Route::post('support-tickets/ticket-update-department','SupportTicketController@updateTicketDepartment');
Route::post('support-tickets/ticket-update-status','SupportTicketController@updateTicketStatus');
Route::post('support-tickets/replay-ticket','SupportTicketController@replayTicket');
Route::get('support-tickets/delete-ticket/{id}','SupportTicketController@deleteTicket');
Route::get('support-tickets/delete-department/{id}','SupportTicketController@deleteDepartment');
Route::post('support-ticket/basic-info-post','SupportTicketController@postBasicInfo');
Route::post('support-ticket/post-ticket-files','SupportTicketController@postTicketFiles');
Route::get('support-ticket/download-file/{id}','SupportTicketController@downloadTicketFile');
Route::get('support-ticket/delete-ticket-file/{id}','SupportTicketController@deleteTicketFile');

/*Overtime Warrant*/
Route::get('overtime/warrants','OvertimeController@warrants');
Route::post('overtime/add','OvertimeController@addWarrant');
Route::get('overtime/view-warrant/{id}','OvertimeController@viewWarrant');
Route::post('overtime/edit-warrant','OvertimeController@editWarrant');
Route::get('overtime/delete/{id}','OvertimeController@deleteWarrant');
Route::post('overtime/get-employee','OvertimeController@getEmployeeName');
Route::post('overtime/set-warrant-status','OvertimeController@setWarrantStatus');
Route::get('overtime/download-pdf/{id}','OvertimeController@DownloadPDF');

/*Assessment Forms*/
Route::get('assessments/forms','AssessmentController@forms');
Route::post('assessments/add','AssessmentController@addForm');
Route::post('assessments/add-dimension','AssessmentController@addDimension');
Route::get('assessments/view-form/{id}','AssessmentController@viewForm');
Route::post('assessments/edit-form','AssessmentController@editForm');
Route::post('assessments/post-edit-dimension','AssessmentController@postEditDimension');
Route::get('assessments/edit-dimension/{id}','AssessmentController@editDimension');
Route::get('assessments/delete/{id}','AssessmentController@deleteForm');
Route::get('assessments/delete-aspect/{id}','AssessmentController@deleteAspect');
Route::post('assessments/set-form-status','AssessmentController@setFormStatus');
Route::get('assessments/download-pdf/{id}','AssessmentController@DownloadPDF');

/*EmployeeAssessment Forms*/
Route::get('employee-assessments/reports','EmployeeAssessmentController@reports');
Route::get('employee-assessments/assessments/{id}','EmployeeAssessmentController@employeeAssessments');
Route::get('employee-assessments/assessment-aspects/{id}','EmployeeAssessmentController@viewAssessment');
Route::get('employee-assessments/add','EmployeeAssessmentController@addAssessment');
Route::post('employee-assessments/add-post','EmployeeAssessmentController@addAssessmentPost');
Route::post('employee-assessments/update-post','EmployeeAssessmentController@updateAssessmentPost');
Route::post('employee-assessments/get-violation','EmployeeAssessmentController@getViolation');
Route::post('employee-assessments/get-project','EmployeeAssessmentController@getProject');
Route::post('employee-assessments/get-employee-code','EmployeeAssessmentController@getEmployee');
Route::post('employee-assessments/get-employee-name','EmployeeAssessmentController@getEmployeeName');
Route::post('employee-assessments/get-designation','EmployeeAssessmentController@getDesignation');
Route::post('employee-assessments/get-location','EmployeeAssessmentController@getLocation');
Route::post('employee-assessments/get-presence-grade','EmployeeAssessmentController@getPresenceGrade');
Route::get('employee-assessments/downloadPdf/{id}','EmployeeAssessmentController@downloadPdf');

/*Payroll Module*/
Route::get('payroll/employee-salary-list','PayrollController@employeeSalaryList');
Route::get('payroll/employee-salary-edit/{id}','PayrollController@editEmployeeSalary');
Route::post('payroll/edit-employee-salary-post','PayrollController@postEditEmployeeSalary');
Route::get('payroll/make-payment','PayrollController@makePayment');
Route::post('payroll/make-payment/post-custom-search','PayrollController@postCustomSearch');
Route::post('payroll/get-designation','PayrollController@getDesignation');
Route::get('payroll/generate','PayrollController@generatePayslip');
Route::get('payroll/pay-payment/{emp_id}/{date}','PayrollController@payPayment');
Route::post('payroll/pay-payment-post','PayrollController@payPaymentPost');
Route::post('payroll/payslip/post-custom-search','PayrollController@postPayslipCustomSearch');
Route::get('payroll/view-details/{id}','PayrollController@viewDetails');
Route::get('payroll/print-payslip/{id}','PayrollController@printPayslip');
Route::get('payroll/employee-salary-increment','PayrollController@employeeSalaryIncrement');
Route::get('payroll/employee-salary-increment-edit/{id}','PayrollController@editEmployeeSalaryIncrement');
Route::post('payroll/edit-employee-salary-increment-post','PayrollController@postEditEmployeeSalaryIncrement');
Route::get('payroll/payroll-types','PayrollController@payrollType');
Route::post('payroll/add','PayrollController@addPayrollType');
Route::get('payroll/view-payroll-types/{id}','PayrollController@viewPayrollType');
Route::get('payroll/edit-payroll-types/{id}','PayrollController@editPayrollType');
Route::post('payroll/set-payroll-status','PayrollController@setPayrollStatus');
Route::post('payroll/edit-payroll-post','PayrollController@postEditPayroll');

/*Provident Fund*/
Route::get('provident-fund/all','PayrollController@providentFund');
Route::post('provident-fund/post-new-provident-fund','PayrollController@postProvidentFund');
Route::get('provident-fund/view-details/{id}','PayrollController@viewProvidentFund');
Route::post('provident-fund/edit-post','PayrollController@postEditProvidentFund');
Route::get('provident-fund/delete/{id}','PayrollController@deleteProvidentFund');
Route::post('provident-fund/make-payment','PayrollController@makePaymentProvidentFund');
Route::get('provident-fund/view-payslip/{id}','PayrollController@payslipProvidentFund');
Route::get('provident-fund/print-payslip/{id}','PayrollController@printPayslipProvidentFund');

/*Loan Module*/
Route::get('loan/all','PayrollController@loan');
Route::post('loan/post-new-loan','PayrollController@postNewLoan');
Route::get('loan/view-details/{id}','PayrollController@viewDetailsLoan');
Route::post('loan/edit-post','PayrollController@postEditLoan');
Route::get('loan/delete/{id}','PayrollController@deleteLoan');

/*PKB Letter Template Module*/
Route::get('outgoing-letter/pkb-letters','PkbLetterController@pkbLetters');
Route::get('outgoing-letter/pkb-letters/manage/{id}','PkbLetterController@manageTemplate');
Route::post('outgoing-letter/pkb-letters/templates-update','PkbLetterController@updateTemplate');
Route::get('outgoing-letter/pkb-letters/templates-add','PkbLetterController@addTemplate');
Route::post('outgoing-letter/pkb-letters/templates-add-post','PkbLetterController@addTemplatePost');
Route::post('outgoing-letter/pkb-letters/get-variable','PkbLetterController@getVariable');
Route::get('outgoing-letter/pkb-letters/downloadPdf/{id}','PkbLetterController@downloadPdf');

/*OUT Letter Template Module*/
Route::get('outgoing-letter/out-letters','OutLetterController@outLetters');
Route::get('outgoing-letter/out-letters/manage/{id}','OutLetterController@manageTemplate');
Route::post('outgoing-letter/out-letters/templates-update','OutLetterController@updateTemplate');
Route::get('outgoing-letter/out-letters/templates-add','OutLetterController@addTemplate');
Route::post('outgoing-letter/out-letters/templates-add-post','OutLetterController@addTemplatePost');
Route::post('outgoing-letter/out-letters/get-variable','OutLetterController@getVariable');
Route::get('outgoing-letter/out-letters/downloadPdf/{id}','OutLetterController@downloadPdf');

/*Email Template Module*/
Route::get('settings/email-templates','SettingController@emailTemplates');
Route::get('settings/email-template-manage/{id}','SettingController@manageTemplate');
Route::post('settings/email-templates-update','SettingController@updateTemplate');
Route::get('settings/email-templates-add','SettingController@addTemplate');
Route::post('settings/email-templates-add-post','SettingController@addTemplatePost');
Route::post('settings/get-variable','SettingController@getVariable');

/*Tax Rules*/
Route::get('settings/tax-rules','SettingController@taxRules');
Route::post('tax-rules/post-new-tax','SettingController@postNewTax');
Route::get('tax-rules/set-rules/{tid}','SettingController@setRules');
Route::post('tax-rules/post-set-rules','SettingController@postSetRules');
Route::get('tax-rules/delete-tax-rule/{tid}','SettingController@deleteTaxRule');
Route::post('tax-rules/post-update-tax-rules','SettingController@postUpdateTaxRules');

/*SMS Gateways*/
Route::get('settings/sms-gateways','SettingController@smsGateways');
Route::get('settings/sms-gateways-manage/{id}','SettingController@smsGatewayManage');
Route::post('settings/sms-gateway-update','SettingController@smsGatewayUpdate');



/*Reports Module*/
Route::get('reports/employee-data','ReportsController@employeeDataSummery');
Route::post('reports/employee-data','ReportsController@employeeDataSummery');


/*Reports Module*/
Route::post('reports/get-employee-upah-lembur','ReportsController@getEmployeeUpahLembur');

Route::get('reports/employee-mutation','ReportsController@employeeMutation');
Route::post('reports/employee-mutation','ReportsController@employeeMutation');
Route::get('reports/download-excel-employee-mutation/{a}/{b}/{c}','ReportsController@downloadEmployeeMutation');

Route::get('reports/employee-active','ReportsController@employeeActive');
Route::post('reports/employee-active','ReportsController@employeeActive');
Route::get('reports/download-excel-employee-active/{a}/{b}','ReportsController@downloadEmployeeActive');

Route::get('reports/employee-resign','ReportsController@employeeResign');
Route::post('reports/employee-resign','ReportsController@employeeResign');
Route::get('reports/download-excel-employee-resign/{a}/{b}','ReportsController@downloadEmployeeResign');

Route::get('reports/employee-accepted','ReportsController@employeeAccepted');
Route::post('reports/employee-accepted','ReportsController@employeeAccepted');
Route::get('reports/download-excel-employee-accepted/{a}/{b}','ReportsController@downloadEmployeeAccepted');

Route::get('reports/upah-lembur','ReportsController@UpahLembur');
Route::post('reports/upah-lembur','ReportsController@UpahLembur');
Route::get('reports/print-upah-lembur/{id}/{bulan}/{tahun}','ReportsController@PrintUpahLembur');


Route::get('reports/procurement','ReportsController@procurementSummery');
Route::post('reports/procurement','ReportsController@procurementSummery');


Route::get('reports/project','ReportsController@projectSummery');
Route::post('reports/project','ReportsController@projectSummery');

Route::get('reports/leaves','ReportsController@leavesSummery');
Route::post('reports/leaves','ReportsController@leavesSummery');
Route::get('reports/attendances','ReportsController@attendancesSummery');
Route::post('reports/attendances','ReportsController@attendancesSummery');

Route::get('reports/employee-score','ReportsController@employeeScoreSummery');
Route::post('reports/employee-score','ReportsController@employeeScoreSummery');

Route::get('reports/salary','ReportsController@salarySummery');
Route::post('reports/salary','ReportsController@salarySummery');

Route::get('reports/download-pdf-data-karyawan/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}/{j}/{k}/{l}/{m}/{n}/{o}','ReportsController@downloadPdfDataKaryawan');
Route::post('reports/download-pdf-attendances','ReportsController@downloadPdfAttendance');
Route::post('reports/download-pdf-leaves','ReportsController@downloadPdfLeaves');

Route::get('reports/payroll','ReportsController@payrollSummery');
Route::get('reports/get-projects','ReportsController@getProjects');
Route::get('reports/get-leave-type','ReportsController@getLeaveType');
Route::get('reports/get-companies','ReportsController@getCompanies');
Route::post('reports/get-project-attendance','ReportsController@getProjectAttendance');
Route::post('reports/get-salary-statement','ReportsController@getSalaryStatement');
Route::get('reports/print-salary-statement/{id}/{date_from}/{date_to}','ReportsController@printSalaryStatement');
Route::get('reports/employee-summery/{id}','ReportsController@employeeSummery');
Route::get('reports/job-applicants','ReportsController@jobApplicants');
Route::post('reports/send-email-to-applicants','ReportsController@sendEmailApplicant');
Route::post('reports/send-sms-to-applicants','ReportsController@sendSMSApplicant');
Route::post('reports/send-sms-salary-statement','ReportsController@sendSMSSalaryStatement');
Route::post('reports/send-email-salary-statement','ReportsController@sendEmailSalaryStatement');



/*Employee Portal*/

/*Employee Dashboard*/
Route::get('employee/dashboard','UserController@employeeDashboard');

/*Logout*/
Route::get('employee/logout','UserController@logout');

/*Profile Edit*/
Route::get('employee/edit-profile','UserController@editEditProfile');
Route::post('employee/post-employee-personal-info','UserController@postEmployeePersonalInfo');
Route::post('employee/update-employee-avatar','UserController@updateEmployeeAvatar');
Route::get('employee/change-password','UserController@changeEmployeePassword');
Route::post('employee/update-employee-password','UserController@updateEmployeePassword');

/*Employee Calendar*/
Route::get('employee/calendar','EmployeePortalController@calendar');
Route::get('employee/calendar/ajax-event-calendar','EmployeePortalController@eventCalendar');

/*Employee Award*/
Route::get('employee/award','EmployeePortalController@award');

/*Employee Leave*/
Route::get('employee/leave','EmployeePortalController@leave');
Route::post('employee/leave/post-new-leave','EmployeePortalController@postNewLeave');

/*Employee Notice Board*/
Route::get('employee/notice-board','EmployeePortalController@noticeBoard');

/*Employee Expense*/
Route::get('employee/expense','EmployeePortalController@expense');
Route::post('employee/expense/post-new-expense','EmployeePortalController@postExpense');

/*Employee Support Ticket*/
Route::get('employee/support-tickets/all','EmployeePortalController@allSupportTickets');
Route::get('employee/support-tickets/create-new','EmployeePortalController@createNewTicket');
Route::post('employee/support-tickets/post-ticket','EmployeePortalController@postTicket');
Route::get('employee/support-tickets/view-ticket/{id}','EmployeePortalController@viewTicket');
Route::post('employee/support-tickets/replay-ticket','EmployeePortalController@replayTicket');
Route::post('employee/support-ticket/post-ticket-files','EmployeePortalController@postTicketFiles');
Route::get('employee/support-ticket/download-file/{id}','EmployeePortalController@downloadTicketFile');

/*Employee Payroll*/
Route::get('employee/payslip','EmployeePortalController@paySlip');
Route::get('employee/payslip/view/{id}','EmployeePortalController@viewPaySlip');
Route::get('employee/payslip/print-payslip/{id}','EmployeePortalController@printPaySlip');

/*Employee Task*/
Route::get('employee/task','EmployeePortalController@task');
Route::get('employee/task/view/{id}','EmployeePortalController@viewTask');
Route::post('employee/task/post-task-comments','EmployeePortalController@postTaskComments');
Route::post('employee/task/post-task-files','EmployeePortalController@postTaskFiles');
Route::get('employee/task/download-file/{id}','EmployeePortalController@downloadTaskFIle');


/*Apply Job*/
Route::get('apply-job','UserController@applyJob');
Route::get('apply-job/details/{id}','UserController@applyJobDetails');
Route::post('apply-job/post-applicant-resume','UserController@postApplicantResume');
Route::post('apply-job/post-general-applicant-resume','UserController@postGeneralApplicantResume');
Route::get('apply-job/checkktp','UserController@getKTP');

/*Clock In, Out*/
Route::post('employee/attendance/set_clocking','EmployeePortalController@setClocking');

/*Loan*/
Route::get('employee/loan/all','EmployeePortalController@allLoan');
Route::post('employee/loan/post-new-loan','EmployeePortalController@postNewLoan');
Route::get('employee/loan/view-details/{id}','EmployeePortalController@viewLoanDetails');
Route::post('employee/loan/edit-post','EmployeePortalController@postEditLoan');



/*Version 1.5*/

/*For Admin Portal*/

Route::get('payroll/download-pdf/{id}','PayrollController@downloadPdf');
Route::get('reports/pdf-salary-statement/{id}/{date_from}/{date_to}','ReportsController@pdfSalaryStatement');

/*Training*/

Route::get('training/trainers','TrainingController@trainers');
Route::post('training/post-new-trainer','TrainingController@postNewTrainer');
Route::get('training/delete-trainer/{id}','TrainingController@deleteTrainer');
Route::get('training/view-trainers-info/{id}','TrainingController@viewTrainersInfo');
Route::post('training/post-trainer-update-info','TrainingController@postTrainerUpdateInfo');
Route::get('training/employee-training','TrainingController@employeeTraining');
Route::post('training/post-new-training','TrainingController@postNewTraining');
Route::get('training/delete-employee-training/{id}','TrainingController@deleteEmployeeTraining');
Route::get('training/view-employee-training/{id}','TrainingController@viewEmployeeTraining');
Route::post('training/post-employee-training-info','TrainingController@postEmployeeTrainingInfo');
Route::get('training/training-needs-assessment','TrainingController@trainingNeedsAssessment');
Route::post('training/post-new-training-needs-assessment','TrainingController@postNewTrainingNeedsAssessment');
Route::get('training/delete-training-needs-assessment/{id}','TrainingController@deleteTrainingNeedsAssessment');
Route::get('training/view-training-needs-assessment/{id}','TrainingController@viewTrainingNeedsAssessment');
Route::get('training/view-training-needs-assessment/{id}','TrainingController@viewTrainingNeedsAssessment');
Route::post('training/post-training-needs-assessment-update','TrainingController@postTrainingNeedsAssessmentUpdate');
Route::get('training/training-events','TrainingController@trainingEvents');
Route::post('training/post-new-training-event','TrainingController@postNewTrainingEvent');
Route::get('training/delete-training-event/{id}','TrainingController@deleteTrainingEvent');
Route::get('training/view-training-events/{id}','TrainingController@viewTrainingEvent');
Route::post('training/post-training-events-update','TrainingController@postTrainingEventUpdate');
Route::get('training/evaluations','TrainingController@TrainingEvaluations');
Route::post('training/post-training-evaluations','TrainingController@postTrainingEvaluations');
Route::post('training/update-training-evaluations','TrainingController@updateTrainingEvaluations');
Route::get('training/delete-training-evaluations/{id}','TrainingController@deleteTrainingEvaluations');

/*For Employee Role management*/

Route::get('employees/roles','EmployeeController@employeeRoles');
Route::post('employees/add-roles','EmployeeController@addEmployeeRoles');
Route::post('employees/update-role','EmployeeController@updateEmployeeRoles');
Route::get('employees/set-roles/{id}','EmployeeController@setEmployeeRoles');
Route::post('employees/update-employee-set-roles','EmployeeController@updateEmployeeSetRoles');
Route::get('employees/delete-roles/{id}','EmployeeController@deleteEmployeeRoles');

/*Permission Check*/
Route::get('permission-error','UserController@permissionError');


Route::get('attendance/get-all-pdf-report','AttendanceController@getAllPdfReport');
Route::get('attendance/get-all-pdf-report-outsource','AttendanceController@getAllPdfReportOutsource');
Route::get('attendance/get-pdf-report/{date}/{emp_id?}/{dep_id?}/{des_id?}','AttendanceController@getPdfReport');


/*
|--------------------------------------------------------------------------
| Disable Menu For specific Employee
|--------------------------------------------------------------------------
|
| You can show hide Admin Menu/Employee Menu for specific Employee
|
*/
Route::get('settings/disable-menus','SettingController@disableMenus');
Route::get('settings/disable-menus-manage/{id}','SettingController@disableMenusManage');
Route::post('settings/disable-menus-post','SettingController@disableMenusManagePost');

/*Employee Portal*/
Route::get('employee/attendance','EmployeePortalController@attendance');
Route::get('employee/training','EmployeePortalController@training');
Route::get('employee/training/view/{id}','EmployeePortalController@viewTraining');

/*Update Application*/
Route::get('update','UserController@updateApplication');