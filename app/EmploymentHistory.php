<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmploymentHistory extends Model
{
    protected $table = 'sys_employment_histories';

    /* project_info Start Here */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','project');
    }

    /* employee_info Start Here */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','emp_id');
    }
    /* designation_info Start Here */
    public function designation_info()
    {
        return $this->hasOne('App\Designation','id','designation');
    }
    /* department_info Start Here */
    public function department_info()
    {
        return $this->hasOne('App\Department','id','department');
    }

}
