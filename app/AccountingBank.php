<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingBank extends Model
{
    protected $table = 'sys_accounting_banks';
}
