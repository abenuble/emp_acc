<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procurement extends Model
{
    protected $table = 'sys_procurements';

    
    /* company  Function Start Here */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','company');
    }

    /* project  Function Start Here */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','project');
    }
}
