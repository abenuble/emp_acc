<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $table = 'sys_designation';
    protected $fillable = ['did','designation'];

    /* department_name  Function Start Here */
    public function department_name()
    {
        return $this->hasOne('App\Department','id','did');
    }

    /*  Function Start Here */
    public function career_info()
    {
        return $this->hasOne('App\Designation','id','career');
    }

    /*  Function Start Here */
    public function career_path_info()
    {
        return $this->hasOne('App\CareerPathEmployee','designation','career');
    }

    /*  Function Start Here */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','designation','designation');
    }


}
