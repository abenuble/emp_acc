<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseDetail extends Model
{
    protected $table='sys_expense_details';

    /* expense_info  Function Start Here */
    public function expense_info()
    {
        return $this->hasOne('App\Expense','id','expense_id');
    }
}
