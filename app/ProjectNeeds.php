<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectNeeds extends Model
{
    protected $table = 'sys_project_needs';

    /* project Function Start Here */
    public function project_name()
    {
        return $this->hasOne('App\Project','id','id');
    }

    /* designation Function Start Here */
    public function designation_name()
    {
        return $this->hasOne('App\Designation','id','id_designation');
    }

    /* payment Function Start Here */
    public function payment_name()
    {
        return $this->hasOne('App\Payment','id','id_payment');
    }

}
