<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractRecipient extends Model
{
    protected $table = 'sys_contract_recipients';

    /* payroll_info Start Here */
    public function payroll_info()
    {
        return $this->hasOne('App\PayrollTypes','id','payroll_types');
    }
    
    /* contract_info Start Here */
    public function contract_info()
    {
        return $this->hasOne('App\Contract','id','contract_id');
    }

    /* project_info Start Here */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','project_number');
    }

    /* employee_info Start Here */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','recipients');
    }
}
