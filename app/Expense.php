<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $table='sys_expense';

    /* project_id  Function Start Here */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','project');
    }
    /* company_id  Function Start Here */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','company');
    }

}
