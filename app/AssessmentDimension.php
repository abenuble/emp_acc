<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssessmentDimension extends Model
{
    protected $table='sys_assessment_dimension';

    /* leave assessment_info Start Here */
    public function assessment_info()
    {
        return $this->hasOne('App\AssessmentForm','id','assessment');
    }
    /* leave assessment_info Start Here */
    public function aspect_info()
    {
        return $this->hasMany('App\AssessmentAspect','dimension','id');
    }
}
