<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingBankDetail extends Model
{
    protected $table = 'sys_accounting_bank_details';
    /* bank_info Start Here */
    public function bank_info()
    {
        return $this->hasOne('App\AccountingBank','id','proof_id');
    }
}
