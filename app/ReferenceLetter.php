<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferenceLetter extends Model
{
    protected $table = 'sys_reference_letter';

}
