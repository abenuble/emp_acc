<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProfileJobDesc extends Model
{
    protected $table = 'sys_company_profile_job_description';

    /* division_info Function Start Here */
    public function division_info()
    {
        return $this->hasOne('App\Division','id','division');
    }

    /* position_info  Function Start Here */
    public function position_info()
    {
        return $this->hasOne('App\Position','id','position');
    }

}
