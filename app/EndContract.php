<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EndContract extends Model
{
    protected $table = 'sys_end_contracts';

    /* project_info Start Here */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','project_number');
    }

    /* company_info Start Here */
    public function company_info()
    {
        return $this->hasOne('App\Company','id','company_name');
    }

    /* draft_letter_info Start Here */
    public function draft_letter_info()
    {
        return $this->hasOne('App\EmailTemplate','id','draft_letter');
    }


}
