<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectVehicle extends Model
{
    protected $table = 'sys_project_vehicles';

    /* project_info Function Start Here */
    public function project_info()
    {
        return $this->hasOne('App\Project','id','id_project');
    }
    /* employee_info Function Start Here */
    public function employee_info()
    {
        return $this->hasOne('App\Employee','id','emp_id');
    }
}
