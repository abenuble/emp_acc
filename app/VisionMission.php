<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisionMission extends Model
{
    protected $table = 'sys_vision_mission_slogan';
}
