<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeFiles extends Model
{
    protected $table='sys_employee_files';

    /* employee  Function Start Here */
    public function employee_name()
    {
        return $this->hasOne('App\Employee','employee_code','emp_id');
    }
}
