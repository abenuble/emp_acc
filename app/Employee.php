<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contract;
class Employee extends Model
{
    protected $table = 'sys_employee';

    /* designation  Function Start Here */
    public function designation_name()
    {
        return $this->hasOne('App\Designation','id','designation');
    }
    /* department  Function Start Here */
    public function department_name()
    {
        return $this->hasOne('App\Department','id','department');
    }

    /* company  Function Start Here */
    public function company_name()
    {
        return $this->hasOne('App\Company','id','company');
    }


    /* project  Function Start Here */
    public function payroll_info()
    {
        return $this->hasOne('App\PayrollTypes','id','payment_type');
    }

    /* project  Function Start Here */
    public function project_name()
    {
        return $this->hasOne('App\Project','id','project');
    }

    /* last education  Function Start Here */
    public function last_education_info()
    {
        return $this->hasOne('App\LastEducation','id','last_education');
    }

    /* schedule_info  Function Start Here */
    public function schedule_info()
    {
        return $this->hasOne('App\Schedule','id','schedule');
    }

    /* vehicle  Function Start Here */
    public function vehicle_info()
    {
        return $this->hasOne('App\ProjectVehicle','id','vehicle');
    }
    public function bank_info()
    {
        return $this->hasOne('App\EmployeeBankAccount','emp_id','id');
    }

   
    public static function fiind($a,$b)
    {
        return Contract::whereIn('id', function($query) use($a,$b){
            $query->select('project_number')
            ->from('sys_contract_recipients')
            ->where('recipients', $a)
            ;
        })->where('status','accepted')->where('project_number',$b)->orderBy('id','asc')->first();
    }



}
