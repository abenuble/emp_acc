<?php

return array(


    'pdf' => array(
        'enabled' => true,
        'binary'  => public_path() . '/assets/wkhtmltox/bin/wkhtmltopdf',
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary'  => public_path() . '/assets/wkhtmltox/bin/wkhtmltoimage',
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),


);
